
module evt_delay_bufferTb();

   parameter EVT_WIDTH = 64;
   parameter VC_WIDTH = 5;
   parameter SIZE_WIDTH = 5;

   bit clk, reset;
   
   //input output ports 
   //Input signals
   logic [EVT_WIDTH-1:0] 	     us_evt_i;
   logic [VC_WIDTH-1:0] 	     us_vc_i;
   logic [SIZE_WIDTH-1:0] 	     us_size_i;
   logic 			     us_valid_i;
   logic 			     ds_ready_i;

   //Output signals
   logic 			     us_ready_o;
   logic [EVT_WIDTH-1:0] 	     ds_evt_o;
   logic [VC_WIDTH-1:0] 	     ds_vc_o;
   logic [SIZE_WIDTH-1:0] 	     ds_size_o;
   logic 			     ds_valid_o;


   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   evt_delay_buffer evt_delay_buffer1 (
				       .clk(clk),
				       .reset(reset),
				       .us_evt_i(us_evt_i),
				       .us_vc_i(us_vc_i),
				       .us_size_i(us_size_i),
				       .us_valid_i(us_valid_i),
				       .ds_ready_i(ds_ready_i),
				       .us_ready_o(us_ready_o),
				       .ds_evt_o(ds_evt_o),
				       .ds_vc_o(ds_vc_o),
				       .ds_size_o(ds_size_o),
				       .ds_valid_o(ds_valid_o)
				       );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports
      reset = 1'b1;
      //us_evt_i = '0;
      //us_vc_i = '0;
      //us_size_i = '0;
      us_valid_i = '0;
      ds_ready_i = '0;

      ##5;
      reset = 1'b0;
      ds_ready_i = 1'b0;      
      ##5;
      us_valid_i = 1'b1;
      ##5;
      ds_ready_i = 1'b1;
      
      #500 $finish;
   end 

   always_ff @(posedge clk) begin : REG_ASSIGN
      if(us_valid_i & us_ready_o) begin
	 us_evt_i <= us_evt_i + 1;
	 us_vc_i <= us_vc_i + 1;
	 us_size_i <= us_size_i + 1;
      end

      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset
	 us_evt_i <= 'd1;
	 us_vc_i <= 'd2;
	 us_size_i <= 'd3;
      end
   end : REG_ASSIGN
endmodule
