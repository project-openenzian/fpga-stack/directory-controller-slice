/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-11-10
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */

// Round robin arbiter.
// Arbitrates between output of 16/32 DCUs. 
// 64 DCUs not supported currently.
// Has 2 pipelined stages
// for 32 inputs:
//   stage 0 is 4 8-ip combinational rr arb.
//   stage 1 is 1 4-ip combinational rr arb, ips from stage 0.
// for 16 inputs:
//   stage 0 is 4 4-ip combinational rr arb.
//   stage 1 is 1 4-ip combinational rr arb, ips from stage 0.
// 64 inputs currently not supported. 

`ifndef DCS_RR_ARB_SV
`define DCS_RR_ARB_SV

import eci_dcs_defs::*;

module dcs_rr_arb #
  (
   parameter ID_WIDTH = 5,
   parameter DATA_WIDTH = 64
   )
   (
    input logic 				     clk,
    input logic 				     reset,

    input logic [DS_NUM_DCU_IDX-1:0][ID_WIDTH-1:0]   us_id_i,
    input logic [DS_NUM_DCU_IDX-1:0][DATA_WIDTH-1:0] us_data_i,
    input logic [DS_NUM_DCU_IDX-1:0] 		     us_valid_i,
    output logic [DS_NUM_DCU_IDX-1:0] 		     us_ready_o,

    output logic [ID_WIDTH-1:0] 		     ds_id_o,
    output logic [DATA_WIDTH-1:0] 		     ds_data_o,
    output logic 				     ds_valid_o,
    input logic 				     ds_ready_i
    );

   initial begin
      if((DS_NUM_DCU_IDX != 32) & (DS_NUM_DCU_IDX != 16)) begin
	 $error("Error: DS_NUM_DCU_IDX %d is not supported. Needs enhancement. (instance %m)");
	 $finish;
      end
   end

   localparam STAGE0_NUM_INST = 4; 
   localparam STAGE0_NUM_IN = (DS_NUM_DCU_IDX == 32)? 8 : 4;
   localparam STAGE1_NUM_INST = 1;
   localparam STAGE1_NUM_IN = STAGE0_NUM_INST; // In of stage 1 same width as out of stage 0.
   localparam PIPE_DATA_WIDTH = ID_WIDTH+DATA_WIDTH;

   // Stage 0 arb signals.
   logic [STAGE0_NUM_INST-1:0][STAGE0_NUM_IN-1:0][ID_WIDTH-1:0] s0_us_id_i;
   logic [STAGE0_NUM_INST-1:0][STAGE0_NUM_IN-1:0][DATA_WIDTH-1:0] s0_us_data_i;
   logic [STAGE0_NUM_INST-1:0][STAGE0_NUM_IN-1:0] 		  s0_us_valid_i;
   logic [STAGE0_NUM_INST-1:0][STAGE0_NUM_IN-1:0] 		  s0_us_ready_o;
   logic [STAGE0_NUM_INST-1:0][ID_WIDTH-1:0] 			  s0_ds_id_o;
   logic [STAGE0_NUM_INST-1:0][DATA_WIDTH-1:0] 			  s0_ds_data_o;
   logic [STAGE0_NUM_INST-1:0] 					  s0_ds_valid_o;
   logic [STAGE0_NUM_INST-1:0] 					  s0_ds_ready_i;
   // Stage 0 pipeline signals.
   logic [STAGE0_NUM_INST-1:0][PIPE_DATA_WIDTH-1:0] 		  s0_pipe_us_data;
   logic [STAGE0_NUM_INST-1:0] 					  s0_pipe_us_valid;
   logic [STAGE0_NUM_INST-1:0] 					  s0_pipe_us_ready;
   logic [STAGE0_NUM_INST-1:0][PIPE_DATA_WIDTH-1:0] 		  s0_pipe_ds_data;
   logic [STAGE0_NUM_INST-1:0] 					  s0_pipe_ds_valid;
   logic [STAGE0_NUM_INST-1:0] 					  s0_pipe_ds_ready;
   // Stage 1 Arb signals.
   logic [STAGE1_NUM_IN-1:0][ID_WIDTH-1:0] 			  s1_us_id_i;
   logic [STAGE1_NUM_IN-1:0][DATA_WIDTH-1:0] 			  s1_us_data_i;
   logic [STAGE1_NUM_IN-1:0] 					  s1_us_valid_i;
   logic [STAGE1_NUM_IN-1:0] 					  s1_us_ready_o;
   logic [ID_WIDTH-1:0] 					  s1_ds_id_o;
   logic [DATA_WIDTH-1:0] 					  s1_ds_data_o;
   logic 							  s1_ds_valid_o;
   logic 							  s1_ds_ready_i;
   // Stage 1 pipeline signals.
   logic [PIPE_DATA_WIDTH-1:0] 					  s1_pipe_us_data;
   logic 							  s1_pipe_us_valid;
   logic 							  s1_pipe_us_ready;
   logic [PIPE_DATA_WIDTH-1:0] 					  s1_pipe_ds_data;
   logic 							  s1_pipe_ds_valid;
   logic 							  s1_pipe_ds_ready;
   
   // Stage 0
   // (DS_NUM_DCU_IDX == 32) ? 4 8-ip axis_comb_rr_arb
   // (DS_NUM_DCU_IDX == 16) ? 4 4-ip axis_comb_rr_arb
   genvar i;
   generate
      for( i = 0; i < STAGE0_NUM_INST; i++) begin : STAGE0
	 always_comb begin : OP_ASSIGN
	    us_ready_o[((i+1)*STAGE0_NUM_IN)-1:(i*STAGE0_NUM_IN)] = s0_us_ready_o[i];
	 end : OP_ASSIGN
	 
	 always_comb begin : S0_RR_IP_ASSIGN
	    s0_us_id_i[i] = us_id_i[((i+1)*STAGE0_NUM_IN)-1:(i*STAGE0_NUM_IN)];
	    s0_us_data_i[i] = us_data_i[((i+1)*STAGE0_NUM_IN)-1:(i*STAGE0_NUM_IN)];
	    s0_us_valid_i[i] = us_valid_i[((i+1)*STAGE0_NUM_IN)-1:(i*STAGE0_NUM_IN)];
	    s0_ds_ready_i[i] = s0_pipe_us_ready[i];
	 end : S0_RR_IP_ASSIGN
	 
	 axis_comb_rr_arb #
		      (
		       .ID_WIDTH(ID_WIDTH),
		       .DATA_WIDTH(DATA_WIDTH),
		       .NUM_IN(STAGE0_NUM_IN) //8
			)
	 stg0_rr_arb
		      (
		       .clk(clk),
		       .reset(reset),
		       .us_id_i(s0_us_id_i[i]),
		       .us_data_i(s0_us_data_i[i]),
		       .us_valid_i(s0_us_valid_i[i]),
		       .us_ready_o(s0_us_ready_o[i]),
		       .ds_id_o(s0_ds_id_o[i]),
		       .ds_data_o(s0_ds_data_o[i]),
		       .ds_valid_o(s0_ds_valid_o[i]),
		       .ds_ready_i(s0_ds_ready_i[i])
		       );

	 always_comb begin : S0_PIPE_IP_ASSIGN
	    s0_pipe_us_data[i] = {s0_ds_id_o[i], s0_ds_data_o[i]};
	    s0_pipe_us_valid[i] = s0_ds_valid_o[i];
	    s0_pipe_ds_ready[i] = s1_us_ready_o[i];
	 end : S0_PIPE_IP_ASSIGN
	 // Add pipeline stage to output.
	 axis_pipeline_stage #
	   (
	    .DATA_WIDTH(PIPE_DATA_WIDTH) 
	     )
	 stg0_pipe_1
	   (
	    .clk(clk),
	    .reset(reset),
	    .us_data(s0_pipe_us_data[i]),
	    .us_valid(s0_pipe_us_valid[i]),
	    .us_ready(s0_pipe_us_ready[i]),
	    .ds_data(s0_pipe_ds_data[i]),
	    .ds_valid(s0_pipe_ds_valid[i]),
	    .ds_ready(s0_pipe_ds_ready[i])
	    );
      end : STAGE0
   endgenerate

   // Stage 1
   // (DS_NUM_DCU_IDX == 32) ? 1 4-ip axis_comb_rr_arb
   // (DS_NUM_DCU_IDX == 16) ? 1 4-ip axis_comb_rr_arb
   always_comb begin : S1_RR_IP_ASSIGN
      {s1_us_id_i[0],s1_us_data_i[0]} = s0_pipe_ds_data[0];
      {s1_us_id_i[1],s1_us_data_i[1]} = s0_pipe_ds_data[1];
      {s1_us_id_i[2],s1_us_data_i[2]} = s0_pipe_ds_data[2];
      {s1_us_id_i[3],s1_us_data_i[3]} = s0_pipe_ds_data[3];
      s1_us_valid_i = s0_pipe_ds_valid;
      s1_ds_ready_i = s1_pipe_us_ready;
   end : S1_RR_IP_ASSIGN
   axis_comb_rr_arb #
     (
      .ID_WIDTH(ID_WIDTH),
      .DATA_WIDTH(DATA_WIDTH),
      .NUM_IN(STAGE1_NUM_IN)//4
      )
   stg1_rr_arb
     (
      .clk(clk),
      .reset(reset),
      .us_id_i(s1_us_id_i),
      .us_data_i(s1_us_data_i),
      .us_valid_i(s1_us_valid_i),
      .us_ready_o(s1_us_ready_o),
      .ds_id_o(s1_ds_id_o),
      .ds_data_o(s1_ds_data_o),
      .ds_valid_o(s1_ds_valid_o),
      .ds_ready_i(s1_ds_ready_i)
      );

   always_comb begin : S1_PIPE_IP_ASSIGN
      s1_pipe_us_data = {s1_ds_id_o, s1_ds_data_o};
      s1_pipe_us_valid = s1_ds_valid_o;
      s1_pipe_ds_ready = ds_ready_i;
   end : S1_PIPE_IP_ASSIGN
   // Add pipeline stage to output.
   axis_pipeline_stage #
     (
      .DATA_WIDTH(PIPE_DATA_WIDTH) 
      )
   stg0_pipe_1
     (
      .clk(clk),
      .reset(reset),
      .us_data(s1_pipe_us_data),
      .us_valid(s1_pipe_us_valid),
      .us_ready(s1_pipe_us_ready),
      .ds_data(s1_pipe_ds_data),
      .ds_valid(s1_pipe_ds_valid),
      .ds_ready(s1_pipe_ds_ready)
      );

   always_comb begin : OP_ASSIGN_2
      {ds_id_o, ds_data_o} = s1_pipe_ds_data;
      ds_valid_o = s1_pipe_ds_valid;
   end : OP_ASSIGN_2
   
endmodule // dcs_rr_arb

`endif
