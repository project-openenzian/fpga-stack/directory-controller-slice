import eci_cmd_defs::*;
import eci_dcs_defs::*;

module cli_tput_load_genTb();

   parameter PERF_REGS_WIDTH = 32;

   //input output ports 
   //Input signals
   logic 				 clk;
   logic 				 reset;
   logic [DS_ADDR_WIDTH-1:0] 		 odd_al_addr_i;
   logic 				 odd_al_addr_valid_i;
   logic 				 odd_lcl_fwd_wod_pkt_ready_i;
   logic [ECI_WORD_WIDTH-1:0] 		 odd_lcl_rsp_wod_hdr_i;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 odd_lcl_rsp_wod_pkt_size_i;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] odd_lcl_rsp_wod_pkt_vc_i;
   logic 				 odd_lcl_rsp_wod_pkt_valid_i;
   logic 				 odd_lcl_rsp_wod_pkt_ready_i;

   //Output signals
   logic 				 odd_al_addr_ready_o;
   logic [ECI_WORD_WIDTH-1:0] 		 odd_lcl_fwd_wod_hdr_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 odd_lcl_fwd_wod_pkt_size_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] odd_lcl_fwd_wod_pkt_vc_o;
   logic 				 odd_lcl_fwd_wod_pkt_valid_o;
   logic 				 odd_lcl_rsp_wod_pkt_ready_o;
   logic [ECI_WORD_WIDTH-1:0] 		 odd_lcl_rsp_wod_hdr_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 odd_lcl_rsp_wod_pkt_size_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] odd_lcl_rsp_wod_pkt_vc_o;
   logic 				 odd_lcl_rsp_wod_pkt_valid_o;
   logic 				 odd_tr_fin_valid_o;

   logic [DS_ADDR_WIDTH-1:0] 		 even_al_addr_i;
   logic 				 even_al_addr_valid_i;
   logic 				 even_lcl_fwd_wod_pkt_ready_i;
   logic [ECI_WORD_WIDTH-1:0] 		 even_lcl_rsp_wod_hdr_i;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 even_lcl_rsp_wod_pkt_size_i;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] even_lcl_rsp_wod_pkt_vc_i;
   logic 				 even_lcl_rsp_wod_pkt_valid_i;
   logic 				 even_lcl_rsp_wod_pkt_ready_i;

   //Output signals
   logic 				 even_al_addr_ready_o;
   logic [ECI_WORD_WIDTH-1:0] 		 even_lcl_fwd_wod_hdr_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 even_lcl_fwd_wod_pkt_size_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] even_lcl_fwd_wod_pkt_vc_o;
   logic 				 even_lcl_fwd_wod_pkt_valid_o;
   logic 				 even_lcl_rsp_wod_pkt_ready_o;
   logic [ECI_WORD_WIDTH-1:0] 		 even_lcl_rsp_wod_hdr_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 even_lcl_rsp_wod_pkt_size_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] even_lcl_rsp_wod_pkt_vc_o;
   logic 				 even_lcl_rsp_wod_pkt_valid_o;
   logic 				 even_tr_fin_valid_o;

   logic 				 perf_done_o;
   logic [PERF_REGS_WIDTH-1:0] 		 perf_cyc_st_to_done_o;
   logic [PERF_REGS_WIDTH-1:0] 		 perf_num_tr_done_o;
   

   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   perf_gen_seq_aliased #
     (
      .NUM_REQS_TO_WAIT(10),
      .NUM_ADDR_TO_ISSUE(20),
      .PERF_REGS_WIDTH(32)
      )
   gen_seq_al_addr_1
     (
      .clk			(clk),
      .reset			(reset),
      .odd_wait_req_valid_i	(1'b1),
      .even_wait_req_valid_i	(1'b1),
      .odd_al_addr_o		(odd_al_addr_i),
      .odd_al_addr_valid_o	(odd_al_addr_valid_i),
      .odd_al_addr_ready_i	(odd_al_addr_ready_o),
      .even_al_addr_o		(even_al_addr_i),
      .even_al_addr_valid_o	(even_al_addr_valid_i),
      .even_al_addr_ready_i	(even_al_addr_ready_o),
      .odd_wait_rsp_valid_i	(odd_tr_fin_valid_o),
      .even_wait_rsp_valid_i	(even_tr_fin_valid_o),
      .done_o			(perf_done_o),
      .perf_cyc_st_to_done_o	(perf_cyc_st_to_done_o),
      .perf_num_tr_done_o	(perf_num_tr_done_o)
      );
   
   //instantiation
   cli_tput_load_gen #
     (
      .PERF_REGS_WIDTH(PERF_REGS_WIDTH)
      )
     cli_tput_load_gen_odd
       (
	.clk(clk),
	.reset(reset),
	.al_addr_i			(odd_al_addr_i),
	.al_addr_valid_i		(odd_al_addr_valid_i),
	.al_addr_ready_o		(odd_al_addr_ready_o),
	.lcl_fwd_wod_hdr_o		(odd_lcl_fwd_wod_hdr_o),
	.lcl_fwd_wod_pkt_size_o		(odd_lcl_fwd_wod_pkt_size_o),
	.lcl_fwd_wod_pkt_vc_o		(odd_lcl_fwd_wod_pkt_vc_o),
	.lcl_fwd_wod_pkt_valid_o	(odd_lcl_fwd_wod_pkt_valid_o),
	.lcl_fwd_wod_pkt_ready_i	(odd_lcl_fwd_wod_pkt_ready_i),
	.lcl_rsp_wod_hdr_i		(odd_lcl_rsp_wod_hdr_i),
	.lcl_rsp_wod_pkt_size_i		(odd_lcl_rsp_wod_pkt_size_i),
	.lcl_rsp_wod_pkt_vc_i		(odd_lcl_rsp_wod_pkt_vc_i),
	.lcl_rsp_wod_pkt_valid_i	(odd_lcl_rsp_wod_pkt_valid_i),
	.lcl_rsp_wod_pkt_ready_o	(odd_lcl_rsp_wod_pkt_ready_o),
	.lcl_rsp_wod_hdr_o		(odd_lcl_rsp_wod_hdr_o),
	.lcl_rsp_wod_pkt_size_o		(odd_lcl_rsp_wod_pkt_size_o),
	.lcl_rsp_wod_pkt_vc_o		(odd_lcl_rsp_wod_pkt_vc_o),
	.lcl_rsp_wod_pkt_valid_o	(odd_lcl_rsp_wod_pkt_valid_o),
	.lcl_rsp_wod_pkt_ready_i	(odd_lcl_rsp_wod_pkt_ready_i),
	.tr_fin_valid_o			(odd_tr_fin_valid_o)
	);//instantiation completed 

   cli_tput_load_gen #
     (
      .PERF_REGS_WIDTH(PERF_REGS_WIDTH)
      )
     cli_tput_load_gen_even
       (
	.clk(clk),
	.reset(reset),
	.al_addr_i			(even_al_addr_i),
	.al_addr_valid_i		(even_al_addr_valid_i),
	.al_addr_ready_o		(even_al_addr_ready_o),
	.lcl_fwd_wod_hdr_o		(even_lcl_fwd_wod_hdr_o),
	.lcl_fwd_wod_pkt_size_o		(even_lcl_fwd_wod_pkt_size_o),
	.lcl_fwd_wod_pkt_vc_o		(even_lcl_fwd_wod_pkt_vc_o),
	.lcl_fwd_wod_pkt_valid_o	(even_lcl_fwd_wod_pkt_valid_o),
	.lcl_fwd_wod_pkt_ready_i	(even_lcl_fwd_wod_pkt_ready_i),
	.lcl_rsp_wod_hdr_i		(even_lcl_rsp_wod_hdr_i),
	.lcl_rsp_wod_pkt_size_i		(even_lcl_rsp_wod_pkt_size_i),
	.lcl_rsp_wod_pkt_vc_i		(even_lcl_rsp_wod_pkt_vc_i),
	.lcl_rsp_wod_pkt_valid_i	(even_lcl_rsp_wod_pkt_valid_i),
	.lcl_rsp_wod_pkt_ready_o	(even_lcl_rsp_wod_pkt_ready_o),
	.lcl_rsp_wod_hdr_o		(even_lcl_rsp_wod_hdr_o),
	.lcl_rsp_wod_pkt_size_o		(even_lcl_rsp_wod_pkt_size_o),
	.lcl_rsp_wod_pkt_vc_o		(even_lcl_rsp_wod_pkt_vc_o),
	.lcl_rsp_wod_pkt_valid_o	(even_lcl_rsp_wod_pkt_valid_o),
	.lcl_rsp_wod_pkt_ready_i	(even_lcl_rsp_wod_pkt_ready_i),
	.tr_fin_valid_o			(even_tr_fin_valid_o)
	);//instantiation completed 

   
   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();
      // Initialize Input Ports 
      clk = '0;
      reset = '1;
      odd_lcl_rsp_wod_pkt_ready_i = '0;
      even_lcl_rsp_wod_pkt_ready_i = '0;
      ##5;
      reset = 1'b0;
      odd_lcl_rsp_wod_pkt_ready_i = '1;
      even_lcl_rsp_wod_pkt_ready_i = '1;
      wait(perf_done_o);
      ##1;
      #500;
      $display("PASS: Odd ch issues odd LCI and odd Unlocks");
      $display("PASS: Even ch issues even LCI and even Unlocks");
      $display("Success: all tests pass");
      $display("Number of Transactions: %0d", perf_num_tr_done_o);
      $display("Number of cycles taken: %0d", perf_cyc_st_to_done_o);
      $finish;
   end

   always_comb begin : ODD_LCIA
      odd_lcl_rsp_wod_hdr_i = eci_cmd_defs::lcl_gen_lcia(odd_lcl_fwd_wod_hdr_o);
      odd_lcl_rsp_wod_pkt_size_i = 'd1;
      odd_lcl_rsp_wod_pkt_vc_i = VC_LCL_RESP_WO_DATA_E; //18
      odd_lcl_rsp_wod_pkt_valid_i = odd_lcl_fwd_wod_pkt_valid_o;
      odd_lcl_fwd_wod_pkt_ready_i = odd_lcl_rsp_wod_pkt_ready_o;
   end : ODD_LCIA

   always_comb begin : EVEN_LCIA
      even_lcl_rsp_wod_hdr_i = eci_cmd_defs::lcl_gen_lcia(even_lcl_fwd_wod_hdr_o);
      even_lcl_rsp_wod_pkt_size_i = 'd1;
      even_lcl_rsp_wod_pkt_vc_i = VC_LCL_RESP_WO_DATA_O; //19
      even_lcl_rsp_wod_pkt_valid_i = even_lcl_fwd_wod_pkt_valid_o;
      even_lcl_fwd_wod_pkt_ready_i = even_lcl_rsp_wod_pkt_ready_o;
   end : EVEN_LCIA

   // Test: all odd unlocks must have odd CL indices.
   always_ff @(posedge clk) begin : ASSERTIONS
      if(odd_lcl_fwd_wod_pkt_valid_o & odd_lcl_fwd_wod_pkt_ready_i) begin
	 if(odd_lcl_fwd_wod_hdr_o[ECI_CL_ADDR_LSB] !== 1'b1) begin
	    $fatal(1, "LCI from odd ch does not have odd index.");
	 end
      end
      if(odd_lcl_rsp_wod_pkt_valid_o & odd_lcl_rsp_wod_pkt_ready_i) begin
	 if(odd_lcl_rsp_wod_hdr_o[ECI_CL_ADDR_LSB] !== 1'b1) begin
	    $fatal(1, "Unlock from odd ch does not have odd index.");
	 end
      end

      if(even_lcl_fwd_wod_pkt_valid_o & even_lcl_fwd_wod_pkt_ready_i) begin
	 if(even_lcl_fwd_wod_hdr_o[ECI_CL_ADDR_LSB] !== 1'b0) begin
	    $fatal(1, "LCI from even ch does not have even index.");
	 end
      end
      if(even_lcl_rsp_wod_pkt_valid_o & even_lcl_rsp_wod_pkt_ready_i) begin
	 if(even_lcl_rsp_wod_hdr_o[ECI_CL_ADDR_LSB] !== 1'b0) begin
	    $fatal(1, "Unlock from even ch does not have even index.");
	 end
      end

   end : ASSERTIONS

endmodule
