/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-08-19
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */

`ifndef WR_DATA_PATH_SV
`define WR_DATA_PATH_SV

import eci_cmd_defs::*;
import eci_dcs_defs::*;

// Instantiates dp_gen_path for write data path.

module wr_data_path
  (
   input logic 						 clk,
   input logic 						 reset,
   // ECI packet for response with data. (VC 4 or 5). (header + data).
   input logic [ECI_PACKET_SIZE-1:0][ECI_WORD_WIDTH-1:0] rsp_wd_pkt_i,
   input logic [ECI_PACKET_SIZE_WIDTH-1:0] 		 rsp_wd_pkt_size_i,
   input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 		 rsp_wd_pkt_vc_i,
   input logic 						 rsp_wd_pkt_valid_i,
   output logic 					 rsp_wd_pkt_ready_o,
   // Output ECI header only for response with data
   // to be routed to the DCUs.
   // DCU_IDX is for routing the ECI header, size, VC
   // to appropriate DCU.
   output logic [DS_DCU_IDX_WIDTH-1:0] 			 rsp_wd_dcu_idx_o,
   output logic [ECI_WORD_WIDTH-1:0] 			 rsp_wd_hdr_o,
   output logic [ECI_PACKET_SIZE_WIDTH-1:0] 		 rsp_wd_pkt_size_o,
   output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 		 rsp_wd_pkt_vc_o,
   output logic 					 rsp_wd_pkt_valid_o,
   input logic 						 rsp_wd_pkt_ready_i,
   // Input Write descriptors: Request from DCUs. 
   // no data, only descriptor.
   input logic [MAX_DCU_ID_WIDTH-1:0] 			 wr_req_id_i, //7b
   input logic [DS_ADDR_WIDTH-1:0] 			 wr_req_addr_i, //38b
   input logic [ECI_CL_SIZE_BYTES-1:0] 			 wr_req_strb_i,
   input logic 						 wr_req_valid_i,
   output logic 					 wr_req_ready_o,
   // Output write request: descriptor + data i/f.
   // to byte addressable memory.
   output logic [MAX_DCU_ID_WIDTH-1:0] 			 wr_req_id_o, //7b
   output logic [DS_ADDR_WIDTH-1:0] 			 wr_req_addr_o, //38b
   output logic [ECI_CL_SIZE_BYTES-1:0] 		 wr_req_strb_o,
   output logic [ECI_CL_WIDTH-1:0] 			 wr_req_data_o,
   output logic 					 wr_req_valid_o,
   input logic 						 wr_req_ready_i
   );

   localparam STORE_HDR_WIDTH = (ECI_WORD_WIDTH + ECI_PACKET_SIZE_WIDTH + ECI_LCL_TOT_NUM_VCS_WIDTH);
   localparam RETRIEVE_HDR_WIDTH = (MAX_DCU_ID_WIDTH + DS_ADDR_WIDTH + ECI_CL_SIZE_BYTES);
   localparam GEN_MAP_ECID_TO_WRD = 1;

   typedef union packed {
      logic [STORE_HDR_WIDTH-1:0] flat;
      struct packed{
	 logic [ECI_WORD_WIDTH-1:0] eci_hdr;
	 logic [ECI_PACKET_SIZE_WIDTH-1:0] eci_pkt_size;
	 logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] eci_vc;
      } parts;
   } str_hdr_t;

   typedef union packed {
      logic [RETRIEVE_HDR_WIDTH-1:0] flat;
      struct packed {
	 logic [MAX_DCU_ID_WIDTH-1:0] id;
	 logic [DS_ADDR_WIDTH-1:0]    addr;
	 logic [ECI_CL_SIZE_BYTES-1:0] strb;
      } parts;
   } rtr_hdr_t;

   // casting input ECI header.
   logic [ECI_WORD_WIDTH-1:0] rsp_wd_pkt_hdr;
   eci_word_t rsp_wd_hdr_c;
   logic [ECI_DMASK_WIDTH-1:0] rsp_wd_pkt_hdr_dmask;
   logic [ECI_ADDR_WIDTH-1:0]  rsp_wd_pkt_hdr_addr;
   ds_cl_addr_t ds_cl_addr_c;

   // DP Gen path signals.
   logic [DS_DCU_ID_WIDTH-1:0]    w_st_dcu_id_i;
   logic [ECI_DMASK_WIDTH-1:0]	  w_st_dmask_i;
   logic [STORE_HDR_WIDTH-1:0]	  w_st_hdr_i;
   str_hdr_t                      w_st_hdr_c;
   logic [ECI_CL_WIDTH-1:0]	  w_st_data_i;
   logic			  w_st_valid_i;
   logic			  w_st_ready_o;
   logic [DS_DCU_IDX_WIDTH-1:0]   w_sth_dcu_idx_o;
   logic [STORE_HDR_WIDTH-1:0]	  w_sth_hdr_o;
   str_hdr_t                      w_sth_hdr_c;
   logic			  w_sth_valid_o;
   logic			  w_sth_ready_i;
   logic [DS_DCU_ID_WIDTH-1:0]	  w_rth_dcu_id_i;
   logic [RETRIEVE_HDR_WIDTH-1:0] w_rth_hdr_i;
   rtr_hdr_t                      w_rth_hdr_c;
   logic			  w_rth_valid_i;
   logic			  w_rth_ready_o;
   logic [RETRIEVE_HDR_WIDTH-1:0] w_rt_hdr_o;
   rtr_hdr_t                      w_rt_hdr_c;
   logic [ECI_CL_WIDTH-1:0]	  w_rt_data_o;
   logic			  w_rt_valid_o;
   logic			  w_rt_ready_i;

   always_comb begin : CAST_ECI_HDR
      rsp_wd_pkt_hdr = rsp_wd_pkt_i[0];
      rsp_wd_hdr_c = eci_word_t'(rsp_wd_pkt_hdr);
      rsp_wd_pkt_hdr_dmask = rsp_wd_hdr_c.generic_cmd.dmask;
      rsp_wd_pkt_hdr_addr = rsp_wd_pkt_hdr[ECI_ADDR_WIDTH-1:0];
      ds_cl_addr_c = ds_cl_addr_t'(rsp_wd_pkt_hdr_addr);
   end : CAST_ECI_HDR

   always_comb begin : OUT_ASSIGN
      rsp_wd_pkt_ready_o = w_st_ready_o;
      rsp_wd_dcu_idx_o   = w_sth_dcu_idx_o;
      rsp_wd_hdr_o       = w_sth_hdr_c.parts.eci_hdr;
      rsp_wd_pkt_size_o  = w_sth_hdr_c.parts.eci_pkt_size;
      rsp_wd_pkt_vc_o    = w_sth_hdr_c.parts.eci_vc;
      rsp_wd_pkt_valid_o = w_sth_valid_o;
      wr_req_ready_o     = w_rth_ready_o;
      wr_req_id_o        = w_rt_hdr_c.parts.id; //7b
      wr_req_addr_o      = w_rt_hdr_c.parts.addr; //38b
      wr_req_strb_o      = w_rt_hdr_c.parts.strb;
      wr_req_data_o      = w_rt_data_o;
      wr_req_valid_o     = w_rt_valid_o;
   end : OUT_ASSIGN

   always_comb begin : WR_DP_GEN_PATH_IP_ASSIGN
      w_st_dcu_id_i			= ds_cl_addr_c.dcu_id;
      w_st_dmask_i			= rsp_wd_pkt_hdr_dmask;
      w_st_hdr_c.parts.eci_hdr		= rsp_wd_pkt_hdr;
      w_st_hdr_c.parts.eci_pkt_size	= rsp_wd_pkt_size_i;
      w_st_hdr_c.parts.eci_vc		= rsp_wd_pkt_vc_i;
      w_st_hdr_i			= w_st_hdr_c.flat;
      w_st_data_i			= rsp_wd_pkt_i[ECI_PACKET_SIZE-1:1];
      w_st_valid_i			= rsp_wd_pkt_valid_i;
      w_sth_ready_i			= rsp_wd_pkt_ready_i;
      w_rth_dcu_id_i			= f_maxdcuid_2_dcu_id(wr_req_id_i);
      w_rth_hdr_c.parts.id		= wr_req_id_i;
      w_rth_hdr_c.parts.addr		= wr_req_addr_i;
      w_rth_hdr_c.parts.strb		= wr_req_strb_i;
      w_rth_hdr_i			= w_rth_hdr_c.flat;
      w_rth_valid_i			= wr_req_valid_i;
      w_rt_ready_i			= wr_req_ready_i;
      w_sth_hdr_c.flat			= w_sth_hdr_o;
      w_rt_hdr_c.flat			= w_rt_hdr_o;
   end : WR_DP_GEN_PATH_IP_ASSIGN

   dp_gen_path #
     (
      .STORE_HDR_WIDTH(STORE_HDR_WIDTH),
      .RETRIEVE_HDR_WIDTH(RETRIEVE_HDR_WIDTH),
      .GEN_MAP_ECID_TO_WRD(GEN_MAP_ECID_TO_WRD)
       )
   wr_dp_gen_path
     (
      .clk		(clk),
      .reset		(reset),
      // Store hdr+data ip i/f.
      .st_dcu_id_i	(w_st_dcu_id_i),
      .st_dmask_i	(w_st_dmask_i),
      .st_hdr_i		(w_st_hdr_i),
      .st_data_i	(w_st_data_i),
      .st_valid_i	(w_st_valid_i),
      .st_ready_o	(w_st_ready_o),
      // Store hdr op i/f.
      .sth_dcu_idx_o	(w_sth_dcu_idx_o),
      .sth_hdr_o	(w_sth_hdr_o),
      .sth_valid_o	(w_sth_valid_o),
      .sth_ready_i	(w_sth_ready_i),
      // Retrieve hdr ip i/f.
      .rth_dcu_id_i	(w_rth_dcu_id_i),
      .rth_hdr_i	(w_rth_hdr_i),
      .rth_valid_i	(w_rth_valid_i),
      .rth_ready_o	(w_rth_ready_o),
      // retrieve hdr+data op i/f.
      .rt_hdr_o		(w_rt_hdr_o),
      .rt_data_o	(w_rt_data_o),
      .rt_valid_o	(w_rt_valid_o),
      .rt_ready_i	(w_rt_ready_i)
      );

endmodule // wr_data_path

`endif
