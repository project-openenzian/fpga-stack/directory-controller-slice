import eci_cmd_defs::*;

module seq_read_load_genTb();

   parameter PRINT_EVERY_X_TR = 1000;

   //input output ports 
   //Input signals
   logic 					   clk;
   logic 					   reset;
   logic [ECI_CL_INDEX_WIDTH-1:0] 		   st_cl_idx_i;
   logic [63:0] 				   num_cls_i;
   logic [63:0] 				   cl_idx_stride_i;
   logic 					   tput_latency_bar_i;
   logic 					   en_i;
   logic 					   lo_wod_pkt_ready_i;
   logic 					   hi_wd_pkt_ready_i;
   logic [ECI_WORD_WIDTH-1:0] 			   lo_wod_hdr_i;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   lo_wod_pkt_size_i;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 		   lo_wod_pkt_vc_i;
   logic 					   lo_wod_pkt_valid_i;
   logic [ECI_PACKET_SIZE-1:0][ECI_WORD_WIDTH-1:0] hi_wd_pkt_i;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   hi_wd_pkt_size_i;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 		   hi_wd_pkt_vc_i;
   logic 					   hi_wd_pkt_valid_i;

   //Output signals
   logic [ECI_WORD_WIDTH-1:0] 			   lo_wod_hdr_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   lo_wod_pkt_size_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 		   lo_wod_pkt_vc_o;
   logic 					   lo_wod_pkt_valid_o;
   logic [ECI_PACKET_SIZE-1:0][ECI_WORD_WIDTH-1:0] hi_wd_pkt_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   hi_wd_pkt_size_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 		   hi_wd_pkt_vc_o;
   logic 					   hi_wd_pkt_valid_o;
   logic 					   lo_wod_pkt_ready_o;
   logic 					   hi_wd_pkt_ready_o;
   logic [63:0] 				   tput_timer_o;
   logic [63:0] 				   num_rsp_recd_o;


   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   seq_read_load_gen seq_read_load_gen1 (
					 .clk(clk),
					 .reset(reset),
					 .st_cl_idx_i(st_cl_idx_i),
					 .num_cls_i(num_cls_i),
					 .cl_idx_stride_i(cl_idx_stride_i),
					 .tput_latency_bar_i(tput_latency_bar_i),
					 .en_i(en_i),
					 .lo_wod_pkt_ready_i(lo_wod_pkt_ready_i),
					 .hi_wd_pkt_ready_i(hi_wd_pkt_ready_i),
					 .lo_wod_hdr_i(lo_wod_hdr_i),
					 .lo_wod_pkt_size_i(lo_wod_pkt_size_i),
					 .lo_wod_pkt_vc_i(lo_wod_pkt_vc_i),
					 .lo_wod_pkt_valid_i(lo_wod_pkt_valid_i),
					 .hi_wd_pkt_i(hi_wd_pkt_i),
					 .hi_wd_pkt_size_i(hi_wd_pkt_size_i),
					 .hi_wd_pkt_vc_i(hi_wd_pkt_vc_i),
					 .hi_wd_pkt_valid_i(hi_wd_pkt_valid_i),
					 .lo_wod_hdr_o(lo_wod_hdr_o),
					 .lo_wod_pkt_size_o(lo_wod_pkt_size_o),
					 .lo_wod_pkt_vc_o(lo_wod_pkt_vc_o),
					 .lo_wod_pkt_valid_o(lo_wod_pkt_valid_o),
					 .hi_wd_pkt_o(hi_wd_pkt_o),
					 .hi_wd_pkt_size_o(hi_wd_pkt_size_o),
					 .hi_wd_pkt_vc_o(hi_wd_pkt_vc_o),
					 .hi_wd_pkt_valid_o(hi_wd_pkt_valid_o),
					 .lo_wod_pkt_ready_o(lo_wod_pkt_ready_o),
					 .hi_wd_pkt_ready_o(hi_wd_pkt_ready_o),
					 .tput_timer_o(tput_timer_o),
					 .num_rsp_recd_o(num_rsp_recd_o)
					 );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports 
      clk = '0;
      reset = '1;
      st_cl_idx_i = '0;
      num_cls_i = '0;
      cl_idx_stride_i = '0;
      tput_latency_bar_i = '0;
      en_i = '0;
      lo_wod_pkt_ready_i = '0;
      hi_wd_pkt_ready_i = '0;
      lo_wod_hdr_i = '0;
      lo_wod_pkt_size_i = '0;
      lo_wod_pkt_vc_i = '0;
      lo_wod_pkt_valid_i = '0;
      //hi_wd_pkt_i = '0;
      hi_wd_pkt_size_i = '0;
      hi_wd_pkt_vc_i = '0;
      hi_wd_pkt_valid_i = '0;

      ##5;
      reset = 1'b0;
      st_cl_idx_i = '0;
      num_cls_i = 'd5;
      cl_idx_stride_i = 'd1;
      tput_latency_bar_i = '0;
      en_i = '0;
      lo_wod_pkt_ready_i = '1;
      ##2;
      en_i = 1'b1;      
      ##1;
      en_i = 1'b0;
      ##5;
      //hi_wd_pkt_i = 'd100;
      hi_wd_pkt_size_i = 'd17;
      hi_wd_pkt_vc_i = VC_RESP_W_DATA_E;
      hi_wd_pkt_valid_i = 1'b1;
      #500 $finish;
   end 

   always_ff @(posedge clk) begin : REG_ASSIGN
      if(hi_wd_pkt_valid_i & hi_wd_pkt_ready_o) begin
	 hi_wd_pkt_i <= hi_wd_pkt_i + 1;
      end
	 
      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset
	 hi_wd_pkt_i <= 'd1;
      end
   end : REG_ASSIGN

endmodule
