
module axi_rd_clTb();

   parameter DESC_ID_WIDTH = 6;
   parameter DESC_ADDR_WIDTH = 40;
   parameter CL_WIDTH = 1024;
   parameter AXI_ID_WIDTH = DESC_ID_WIDTH;
   parameter AXI_ADDR_WIDTH = DESC_ADDR_WIDTH;
   parameter AXI_DATA_WIDTH = 512;

   //input output ports 
   //Input signals
   logic 		      clk;
   logic 		      reset;
   logic [DESC_ID_WIDTH-1:0]  rd_req_id_i;
   logic [DESC_ADDR_WIDTH-1:0] rd_req_addr_i;
   logic 		       rd_req_valid_i;
   logic 		       rd_rsp_ready_i;
   logic 		       p_axi_arready;
   logic [AXI_ID_WIDTH-1:0]    p_axi_rid;
   logic [AXI_DATA_WIDTH-1:0]  p_axi_rdata;
   logic [1:0] 		       p_axi_rresp;
   logic 		       p_axi_rlast;
   logic 		       p_axi_rvalid;

   //Output signals
   logic 		       rd_req_ready_o;
   logic [DESC_ID_WIDTH-1:0]   rd_rsp_id_o;
   logic [CL_WIDTH-1:0]        rd_rsp_data_o;
   logic 		       rd_rsp_valid_o;
   logic [AXI_ID_WIDTH-1:0]    p_axi_arid;
   logic [AXI_ADDR_WIDTH-1:0]  p_axi_araddr;
   logic [7:0] 		       p_axi_arlen;
   logic [2:0] 		       p_axi_arsize;
   logic [1:0] 		       p_axi_arburst;
   logic 		       p_axi_arlock;
   logic [3:0] 		       p_axi_arcache;
   logic [2:0] 		       p_axi_arprot;
   logic 		       p_axi_arvalid;
   logic 		       p_axi_rready;


   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   axi_rd_cl axi_rd_cl1 (
			 .clk(clk),
			 .reset(reset),
			 .rd_req_id_i(rd_req_id_i),
			 .rd_req_addr_i(rd_req_addr_i),
			 .rd_req_valid_i(rd_req_valid_i),
			 .rd_rsp_ready_i(rd_rsp_ready_i),
			 .p_axi_arready(p_axi_arready),
			 .p_axi_rid(p_axi_rid),
			 .p_axi_rdata(p_axi_rdata),
			 .p_axi_rresp(p_axi_rresp),
			 .p_axi_rlast(p_axi_rlast),
			 .p_axi_rvalid(p_axi_rvalid),
			 .rd_req_ready_o(rd_req_ready_o),
			 .rd_rsp_id_o(rd_rsp_id_o),
			 .rd_rsp_data_o(rd_rsp_data_o),
			 .rd_rsp_valid_o(rd_rsp_valid_o),
			 .p_axi_arid(p_axi_arid),
			 .p_axi_araddr(p_axi_araddr),
			 .p_axi_arlen(p_axi_arlen),
			 .p_axi_arsize(p_axi_arsize),
			 .p_axi_arburst(p_axi_arburst),
			 .p_axi_arlock(p_axi_arlock),
			 .p_axi_arcache(p_axi_arcache),
			 .p_axi_arprot(p_axi_arprot),
			 .p_axi_arvalid(p_axi_arvalid),
			 .p_axi_rready(p_axi_rready)
			 );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports 
      clk = '0;
      reset = '1;
      rd_req_id_i = '0;
      rd_req_addr_i = '0;
      rd_req_valid_i = '0;
      rd_rsp_ready_i = '0;
      p_axi_arready = '0;
      p_axi_rid = '0;
      p_axi_rdata = '0;
      p_axi_rresp = '0;
      p_axi_rlast = '0;
      p_axi_rvalid = '0;

      ##5;
      reset = 1'b0;
      p_axi_arready = 1'b1;
      
      ##5;
      send_rreq(
		.id_i('d5),
		.addr_i('d128)
		);
      
      
      // downstream is not ready yet.
      // recd rd request, sending 2 beats. 
      send_rdata(
		 .id_i('d5),
		 .data_i('d2),
		 .rlast_i(1'b0),
		 .rresp_i('0)
		 );
      
      send_rdata(
		 .id_i('d5),
		 .data_i('d3),
		 .rlast_i(1'b0),
		 .rresp_i('0)
		 );

      // downstream should be valid now.
      assert(rd_rsp_valid_o == 1'b1) else $fatal(1, "downstream is not valid yet");
      assert(rd_rsp_data_o[1023:512] == 'd3) else $fatal(1, "Expected upper bits do not match actual");
      assert(rd_rsp_data_o[511:0] == 'd2) else $fatal(1, "Expected lower bits do not match actual");
      assert(rd_rsp_id_o == 'd5) else $fatal(1, "IDs do not match");
      

      ##5;
      // accept downstream data. 
      rd_rsp_ready_i = 1'b1;

      // Send new request.
      send_rreq(
		.id_i('d10),
		.addr_i('d256)
		);
      

      // send only 1 beat with rlast. 
      send_rdata(
		 .id_i('d10),
		 .data_i('d4),
		 .rlast_i(1'b1),
		 .rresp_i('0)
		 );
      
      #500 $finish;
   end

   task static send_rreq(
			 input logic [DESC_ID_WIDTH-1:0]   id_i,
			 input logic [DESC_ADDR_WIDTH-1:0] addr_i
			 );
      rd_req_id_i = id_i;
      rd_req_addr_i = addr_i;
      rd_req_valid_i = 1'b1;
      wait(rd_req_valid_i & rd_req_ready_o);
      ##1;
      rd_req_valid_i = 1'b0;
   endtask // send_rreq
   
   task static send_rdata(
			  input logic [AXI_ID_WIDTH-1:0]   id_i,
			  input logic [AXI_DATA_WIDTH-1:0] data_i,
			  input logic 			   rlast_i, 
			  input logic [1:0] 		   rresp_i
			  );
      p_axi_rid = id_i;
      p_axi_rdata = data_i;
      p_axi_rlast = rlast_i;
      p_axi_rresp = rresp_i;
      p_axi_rvalid = 1'b1;
      wait(p_axi_rvalid & p_axi_rready);
      ##1;
      p_axi_rvalid = 1'b0;
      ##1;
   endtask //send_rdata

   

endmodule
