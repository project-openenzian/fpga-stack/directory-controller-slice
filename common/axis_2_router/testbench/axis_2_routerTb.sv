
module axis_2_routerTb();

   parameter DATA_WIDTH = 64;

   //input output ports 
   //Input signals
   logic 		  clk;
   logic 		  reset;
   logic [DATA_WIDTH-1:0] us_data_i;
   logic 		  us_sel_down_upbar_i;
   logic 		  us_valid_i;
   logic 		  ds_up_ready_i;
   logic 		  ds_down_ready_i;

   //Output signals
   logic 		  us_ready_o;
   logic [DATA_WIDTH-1:0] ds_up_data_o;
   logic 		  ds_up_valid_o;
   logic [DATA_WIDTH-1:0] ds_down_data_o;
   logic 		  ds_down_valid_o;


   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   axis_2_router
     axis_2_router1 (
		     .clk(clk),
		     .reset(reset),
		     .us_data_i(us_data_i),
		     .us_sel_down_upbar_i(us_sel_down_upbar_i),
		     .us_valid_i(us_valid_i),
		     .ds_up_ready_i(ds_up_ready_i),
		     .ds_down_ready_i(ds_down_ready_i),
		     .us_ready_o(us_ready_o),
		     .ds_up_data_o(ds_up_data_o),
		     .ds_up_valid_o(ds_up_valid_o),
		     .ds_down_data_o(ds_down_data_o),
		     .ds_down_valid_o(ds_down_valid_o)
		     );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports 
      clk = '0;
      reset = '1;
      //us_data_i = '0;
      us_sel_down_upbar_i = '0;
      //us_valid_i = '0;
      ds_up_ready_i = '0;
      ds_down_ready_i = '0;

      ##5;
      reset = 1'b0;
      ##5;

      ##2;
      ds_up_ready_i = 1'b0;
      ds_down_ready_i = 1'b1;
      ##1;
      ds_up_ready_i = 1'b1;
      
      ##2;
      us_sel_down_upbar_i = 1'b1;
      ##1;
      us_sel_down_upbar_i = 1'b0;
      ##1;
      us_sel_down_upbar_i = 1'b1;

      #500 $finish;
   end

   always_ff @(posedge clk) begin : REG_ASSIGN
      if(us_valid_i == 1'b0) begin
	 us_valid_i <= 1'b1;
	 us_data_i <= 'd1;
      end
      if(us_valid_i <= us_ready_o) begin
	 us_data_i <= us_data_i + 1'b1;
      end

      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset
	 us_data_i <= '0;
	 us_valid_i <= 1'b0;
      end
   end : REG_ASSIGN
   

endmodule
