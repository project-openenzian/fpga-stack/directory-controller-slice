#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
      $XILINX_VIVADO/data/verilog/src/glbl.v \
      ../rtl/eci_dcs_defs.sv \
      ../rtl/eci_cmd_defs.sv \
      ../testbench/dp_gateTb.sv \
      ../rtl/map_ecid_to_wrd.sv \
      ../rtl/dp_gate.sv


xelab -debug typical -incremental -L xpm worklib.dp_gateTb worklib.glbl -s worklib.dp_gateTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.dp_gateTb 
xsim -R worklib.dp_gateTb 
