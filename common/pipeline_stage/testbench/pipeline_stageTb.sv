
module pipeline_stageTb();

   parameter NUM_STAGE = 5;
   parameter DATA_WIDTH = 64;

   //input output ports 
   //Input signals
   logic 		  clk;
   logic [DATA_WIDTH-1:0] data_i;
   logic 		  reset;
   

   //Output signals
   logic [DATA_WIDTH-1:0] data_o;
   logic [DATA_WIDTH-1:0] exp_data;
   logic 		  check;
   

   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   pipeline_stage pipeline_stage1 (
				   .clk(clk),
				   .data_i(data_i),
				   .data_o(data_o)
				   );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports,
      // reset and start sending data.
      // after NUM_STAGES, the output
      // should be available.
      clk = '0;
      reset = 1'b1;
      check = 0;
      ##5;
      reset = 1'b0;
      ##NUM_STAGE;
      check = 1'b1;
      #500 $finish;
   end

   always_ff @(posedge clk) begin : REG_ASSIGN
      data_i <= data_i + 1;
      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset 
	 data_i <= '0;
      end
   end : REG_ASSIGN

   always_ff @(posedge clk) begin : CHECK_RES
      if(check == 1'b1) begin
	 assert(data_o === exp_data) else 
	   $fatal(1,"Actual data %d does not match expected %d", data_o, exp_data);
	 exp_data <= exp_data + 1;
      end
      if(reset) begin
	 exp_data <= '0;
      end
   end : CHECK_RES

endmodule
