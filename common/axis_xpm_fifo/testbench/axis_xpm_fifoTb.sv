
module axis_xpm_fifoTb();

   parameter DATA_WIDTH = 64;
   parameter FIFO_DEPTH = 32;

   //input output ports 
   //Input signals
   logic 		  clk;
   logic 		  reset;
   logic [DATA_WIDTH-1:0] us_data;
   logic 		  us_valid;
   logic 		  ds_ready;

   //Output signals
   logic 		  us_ready;
   logic [DATA_WIDTH-1:0] ds_data;
   logic 		  ds_valid;
   logic [31:0] 	  ff_sideband_o;


   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   axis_xpm_fifo axis_xpm_fifo1 (
				 .clk(clk),
				 .reset(reset),
				 .us_data(us_data),
				 .us_valid(us_valid),
				 .ds_ready(ds_ready),
				 .us_ready(us_ready),
				 .ds_data(ds_data),
				 .ds_valid(ds_valid),
				 .ff_sideband_o(ff_sideband_o)
				 );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports 
      clk = '0;
      reset = '1;
      //us_data = '0;
      us_valid = '0;
      ds_ready = '0;

      ##5;
      reset = 1'b0;
      us_valid = 1'b1;
      ##32;
      ds_ready = 1'b1;
      ##5;
      us_valid = 1'b0;

      #500 $finish;
   end

   always_ff @(posedge clk) begin : REG_ASSIGN
      if(us_valid & us_ready) begin
	 us_data <= us_data + 'd1;
      end
      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset 
	 us_data <= 'd1;
      end
   end : REG_ASSIGN

endmodule
