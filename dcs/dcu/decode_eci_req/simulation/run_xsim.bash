#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
$XILINX_VIVADO/data/verilog/src/glbl.v \
      ../rtl/eci_cc_defs.sv \
      ../rtl/eci_cmd_defs.sv \
      ../rtl/eci_dirc_defs.sv \
      ../testbench/decode_eci_reqTb.sv \
      ../rtl/decode_eci_req.sv

xelab -debug typical -incremental -L xpm worklib.decode_eci_reqTb worklib.glbl -s worklib.decode_eci_reqTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.decode_eci_reqTb 
xsim -R worklib.decode_eci_reqTb 
