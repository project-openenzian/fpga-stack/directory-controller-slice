// When testing, run following scenarios
// NUM_SETS_PER_DCU = 64/128/256
// TSR_OP_REGISTER = 1/0
module dcu_tsuTb();

   parameter NUM_SETS_PER_DCU = 256;
   parameter logic TSR_OP_REGISTER = 1'b1;
   parameter NUM_WAYS_PER_SET = 16; 
   parameter NUM_TSR_PER_DCU  = (NUM_SETS_PER_DCU == 256) ? 4 : (NUM_SETS_PER_DCU == 128) ? 2 : 1;
   parameter SET_WIDTH = $clog2(NUM_SETS_PER_DCU); 
   parameter WAY_WIDTH = $clog2(NUM_WAYS_PER_SET);  

   parameter logic [17:0] INVALID_TAG = '1;
   parameter logic [17:0] INVALID_STATE = '0;
   

   //input output ports 
   //Input signals
   logic 		 clk;
   logic 		 reset;
   logic [17:0] 	 wr_tag_i;
   logic [17:0] 	 wr_stt_i;
   logic [SET_WIDTH-1:0] wr_set_i;
   logic [WAY_WIDTH-1:0] wr_way_i;
   logic 		 wr_en_i;
   logic [SET_WIDTH-1:0] rd_tag_set_i;
   logic [17:0] 	 rd_tag_to_cmp_i;
   logic 		 rd_tag_en_i;

   //Output signals
   logic [17:0] 	 rd_tag_stt_o;
   logic [WAY_WIDTH-1:0] rd_tag_way_o;
   logic 		 rd_tag_hit_o;
   logic 		 rd_tag_set_full_o;
   logic 		 rd_tag_nxm_o;
   logic 		 tsu_busy_o;

   // Misc signals
   logic [SET_WIDTH-1:0] my_set;

   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   dcu_tsu 
     #(
       .NUM_SETS_PER_DCU(NUM_SETS_PER_DCU),
       .TSR_OP_REGISTER(TSR_OP_REGISTER)
       )
   dcu_tsu1 (
	     .clk(clk),
	     .reset(reset),
	     .wr_tag_i(wr_tag_i),
	     .wr_stt_i(wr_stt_i),
	     .wr_set_i(wr_set_i),
	     .wr_way_i(wr_way_i),
	     .wr_en_i(wr_en_i),
	     .rd_tag_set_i(rd_tag_set_i),
	     .rd_tag_to_cmp_i(rd_tag_to_cmp_i),
	     .rd_tag_en_i(rd_tag_en_i),
	     .rd_tag_stt_o(rd_tag_stt_o),
	     .rd_tag_way_o(rd_tag_way_o),
	     .rd_tag_hit_o(rd_tag_hit_o),
	     .rd_tag_set_full_o(rd_tag_set_full_o),
	     .rd_tag_nxm_o(rd_tag_nxm_o),
	     .tsu_busy_o(tsu_busy_o)
	     );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports 
      clk = '0;
      reset = '1;
      wr_tag_i = '0;
      wr_stt_i = '0;
      wr_set_i = '0;
      wr_way_i = '0;
      wr_en_i = '0;
      rd_tag_set_i = '0;
      rd_tag_to_cmp_i = '0;
      rd_tag_en_i = '0;
      ##5;
      reset = 1'b0;
      wait(tsu_busy_o == 1'b0);
      ##1;

      // Tests on following operations.
      // read the tag and state. 
      //   Hit
      //     hit in each of 16 ways.
      //   Miss
      //     Full
      //     Not Full
      //       Empty way: 0-16 ways.
      // write tag and state
      //   Add a new tag and state.
      //   Update a previously written tag.
      // nxm signal.
      // Test over all sets. 
      for(integer n = 0; n < NUM_SETS_PER_DCU; n = n+1) begin
	 my_set = n;
	 
	 // Test NXM.
	 test_wr_rd(
		    .tag_i(INVALID_TAG),
		    .stt_i('d100),
		    .set_i(my_set),
		    .way_i('d0)
		    );

	 // Write from way 0 to 16 for set 5
	 // Tests Hit on all 16 ways,
	 // After this set 5 is full.
	 // Test: write tag and state -> add a new tag and state.
	 // read tag and state : hit in each of 16 ways. 
	 for(integer i=0; i<NUM_WAYS_PER_SET; i++) begin
	    test_wr_rd(
      		       .tag_i((i+1)*10),
      		       .stt_i((i+1)*100),
      		       .set_i(my_set),
      		       .way_i(i)
      		       );
	 end
	 
	 // Test: read tag and state -> miss. 
	 rd_tag(
		.set_i(my_set),
		.tag_i('d15)
		);
	 assert(rd_tag_hit_o == 1'b0) else $error("Expected miss but got hit.");
	 assert(rd_tag_set_full_o == 1'b1) else $error("Expected set to be full but it is not.");

	 // Test: Miss, not full: all 0 to 16 ways. 
	 for(integer i=0; i<NUM_WAYS_PER_SET; i++) begin
	    // Update state to invalid for each tag in set 5.
	    // This should open up an empty spot in the set. 
	    test_rd_wr(
		       .tag_i((i+1)*10),
		       .set_i(my_set),
		       .stt_i(INVALID_STATE)
		       );
	    // check if empty spot is created.
	    // by adding removed tag back in set 5 and new new state.
	    test_rd_wr(
		       .tag_i((i+1)*10),
		       .set_i(my_set),
		       .stt_i((i+1)*100 + 1)
		       );
	 end
      end
      
      $display("Tests: Completed, all pass.");
      #500 $finish;
      
   end // initial begin

   // Generic wr tag and state, works for all configurations. 
   task static wr_tag_stt(
			  input logic [17:0] 	      tag_i,
			  input logic [17:0] 	      stt_i,
			  input logic [SET_WIDTH-1:0] set_i,
			  input logic [WAY_WIDTH-1:0] way_i
			  );

      wr_tag_i = tag_i;
      wr_stt_i = stt_i;
      wr_set_i = set_i;
      wr_way_i = way_i;
      wr_en_i = 1'b1;
      ##1;
      wr_en_i = 1'b0;
      #1;
      wait(tsu_busy_o == 1'b0);
      ##1;
   endtask //wr_tag_stt

   task static rd_tag(
		      input logic [SET_WIDTH-1:0] set_i,
		      input logic [17:0] 	  tag_i
		      );
      rd_tag_set_i = set_i;
      rd_tag_to_cmp_i = tag_i;
      rd_tag_en_i = 1'b1;
      ##1;
      rd_tag_en_i =1'b0;
      #1;
      wait(tsu_busy_o == 1'b0);
      ##1;
   endtask //rd_tag

   task static test_wr_rd(
			  input logic [17:0] 	      tag_i,
			  input logic [17:0] 	      stt_i,
			  input logic [SET_WIDTH-1:0] set_i,
			  input logic [WAY_WIDTH-1:0] way_i
			  );
      $display("test_wr_rd: tag %d, stt %d, set %d, way %d", tag_i, stt_i, set_i, way_i);
      wr_tag_stt(
		 .tag_i(tag_i),
		 .stt_i(stt_i),
		 .set_i(set_i),
		 .way_i(way_i)
		 );
      rd_tag(
	     .set_i(set_i),
	     .tag_i(tag_i)
	     );
      if(tag_i == INVALID_TAG) begin
	 assert(rd_tag_nxm_o == 1'b1) else $error("NXM Expected 1, Actual 0");
	 assert(rd_tag_hit_o == 1'b0) else $error("Hit Expected: 0, Actual 1");
      end else begin
	 assert(rd_tag_nxm_o == 1'b0) else $error("NXM Expected 0, Actual 1");
	 if(stt_i == INVALID_STATE) begin
	    assert(rd_tag_hit_o == 1'b0) else $error("Hit Expected: 0, Actual 1");
	 end else begin
	    // write state is valid. 
	    assert(rd_tag_hit_o == 1'b1) else $error("Hit Expected: 1, Actual 0");
	    assert(rd_tag_way_o == way_i) else $error("Way Expected: %d, Actual %d", way_i, rd_tag_way_o);
	    assert(rd_tag_stt_o == stt_i) else $fatal(1, "Stt Expected: %d, Actual %d", stt_i, rd_tag_stt_o);
	 end
      end
   endtask // test_wr_rd

   // Technically there should always be a hit way or a free way available. 
   task static test_rd_wr(
			  input logic [17:0] 	      tag_i,
			  input logic [17:0] 	      stt_i,
			  input logic [SET_WIDTH-1:0] set_i
			  );
      rd_tag(
	     .set_i(set_i),
	     .tag_i(tag_i)
	     );

      // can be hit or miss.
      // if miss and full then error out.
      if(rd_tag_hit_o == 1'b1) begin
	 // way returned is the way to write.
	 test_wr_rd(
		    .tag_i(tag_i),
		    .stt_i(stt_i),
		    .set_i(set_i),
		    .way_i(rd_tag_way_o)
		    );
      end else begin
	 // miss. 
	 if(rd_tag_set_full_o == 1'b1) begin
	    $fatal(1, "Ideally a set should never be full before writing");
	 end else begin
	    // way returned is empty way.
	    test_wr_rd(
		       .tag_i(tag_i),
		       .stt_i(stt_i),
		       .set_i(set_i),
		       .way_i(rd_tag_way_o)
		       );
	 end
      end
      
   endtask //test_rd_wr
   
   
    

endmodule
