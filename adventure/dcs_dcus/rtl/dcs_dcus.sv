`ifndef DCS_DCUS_SV
`define DCS_DCUS_SV

module dcs_dcus  #
  (
   parameter PERF_REGS_WIDTH = 64,
   parameter SYNTH_PERF_REGS = 0   // 0, 1
   )
   (
    input logic 			     clk,
    input logic 			     reset,
    // Input ECI events.
    // ECI packet for request without data. (VC 6 or 7) (only header).
    input logic [ECI_WORD_WIDTH-1:0] 	     req_wod_hdr_i,
    input logic [ECI_PACKET_SIZE_WIDTH-1:0]  req_wod_pkt_size_i,
    input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0]  req_wod_pkt_vc_i,
    input logic 			     req_wod_pkt_valid_i,
    output logic 			     req_wod_pkt_ready_o,

    // ECI packet for response without data.(VC 10 or 11). (only header).
    input logic [ECI_WORD_WIDTH-1:0] 	     rsp_wod_hdr_i,
    input logic [ECI_PACKET_SIZE_WIDTH-1:0]  rsp_wod_pkt_size_i,
    input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0]  rsp_wod_pkt_vc_i,
    input logic 			     rsp_wod_pkt_valid_i,
    output logic 			     rsp_wod_pkt_ready_o,

    // ECI packet for response with data. (VC 4 or 5). (only header).
    input logic [ECI_WORD_WIDTH-1:0] 	     rsp_wd_hdr_i,
    input logic [ECI_PACKET_SIZE_WIDTH-1:0]  rsp_wd_pkt_size_i,
    input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0]  rsp_wd_pkt_vc_i,
    input logic 			     rsp_wd_pkt_valid_i,
    output logic 			     rsp_wd_pkt_ready_o,

    // Output ECI events, only header.
    // VC # indicates VC to route to.
    output logic [ECI_WORD_WIDTH-1:0] 	     eci_rsp_hdr_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] eci_rsp_size_o,
    output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] eci_rsp_vc_o,
    output logic 			     eci_rsp_valid_o,
    input logic 			     eci_rsp_ready_i,

    // Output Read descriptors
    // Read descriptors: Request and response.
    // no data, only descriptor.
    output logic [MAX_DCU_ID_WIDTH-1:0]      rd_req_id_o,
    output logic [DS_ADDR_WIDTH-1:0] 	     rd_req_addr_o,
    output logic 			     rd_req_valid_o,
    input logic 			     rd_req_ready_i,

    input logic [MAX_DCU_ID_WIDTH-1:0] 	     rd_rsp_id_i,
    input logic 			     rd_rsp_valid_i,
    output logic 			     rd_rsp_ready_o,

    // Write descriptors: Request and response.
    // no data, only descriptor.
    output logic [MAX_DCU_ID_WIDTH-1:0]      wr_req_id_o,
    output logic [DS_ADDR_WIDTH-1:0] 	     wr_req_addr_o,
    output logic [ECI_CL_SIZE_BYTES-1:0]     wr_req_strb_o,
    output logic 			     wr_req_valid_o,
    input logic 			     wr_req_ready_i,

    input logic [MAX_DCU_ID_WIDTH-1:0] 	     wr_rsp_id_i,
    input logic [1:0] 			     wr_rsp_bresp_i,
    input logic 			     wr_rsp_valid_i,
    output logic 			     wr_rsp_ready_o
    );

   typedef struct packed {
      logic [ECI_WORD_WIDTH-1:0]        hdr;
      logic [ECI_PACKET_SIZE_WIDTH-1:0] size;
      logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] vc;
      logic				valid;
      logic				ready;
   } eci_hdr_if_t;

   typedef struct packed {
      logic [ECI_PACKET_SIZE-1:0][ECI_WORD_WIDTH-1:0] pkt;
      logic [ECI_PACKET_SIZE_WIDTH-1:0] 	      size;
      logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	      vc;
      logic					      valid;
      logic					      ready;
   } eci_pkt_if_t;

   typedef struct packed {
      logic [MAX_DCU_ID_WIDTH-1:0] id;
      logic [DS_ADDR_WIDTH-1:0]    addr;
      logic 			   valid;
      logic 			   ready;
   } rd_req_ctrl_if_t;

   typedef struct packed {
      logic [MAX_DCU_ID_WIDTH-1:0] id;
      logic 			   valid;
      logic 			   ready;
   } rd_rsp_ctrl_if_t;

   typedef logic [ECI_CL_WIDTH-1:0] rd_rsp_data_if_t;

   typedef struct packed {
      logic [MAX_DCU_ID_WIDTH-1:0]  id;
      logic [DS_ADDR_WIDTH-1:0]     addr;
      logic [ECI_CL_SIZE_BYTES-1:0] strb; 
      logic 			    valid;
      logic 			    ready;
   } wr_req_ctrl_if_t;

   typedef logic [ECI_CL_WIDTH-1:0] wr_req_data_if_t;

   typedef struct packed {
      logic [MAX_DCU_ID_WIDTH-1:0]  id;
      logic [1:0] 		    bresp;
      logic 			    valid;
      logic 			    ready;
   } wr_rsp_ctrl_if_t;

   // DCU specific signals.
   eci_hdr_if_t [DS_NUM_DCU_IDX-1:0] dc_req_wod_i, dc_rsp_wod_i, dc_rsp_wd_hdr_i, dc_eci_rsp_hdr_o;
   rd_req_ctrl_if_t [DS_NUM_DCU_IDX-1:0] dc_rd_req_o;
   rd_rsp_ctrl_if_t [DS_NUM_DCU_IDX-1:0] dc_rd_rsp_ctrl_i;
   wr_req_ctrl_if_t [DS_NUM_DCU_IDX-1:0] dc_wr_req_ctrl_o;
   wr_rsp_ctrl_if_t [DS_NUM_DCU_IDX-1:0] dc_wr_rsp_ctrl_i;

   

   genvar dcu_iter;
   generate
      for( dcu_iter = 0; dcu_iter < DS_NUM_DCU_IDX; dcu_iter++) begin : DCUS
	 always_comb begin : DCUS_IP_ASSIGN
	    // Input ECI events.
	    // ECI packet for request without data. (VC 6 or 7) (only header).
	    dc_req_wod_i[dcu_iter].hdr = ;
	    dc_req_wod_i[dcu_iter].size = ;
	    dc_req_wod_i[dcu_iter].vc = ;
	    dc_req_wod_i[dcu_iter].valid = ;
	    // ECI packet for response without data.(VC 10 or 11). (only header).
	    dc_rsp_wod_i[dcu_iter].hdr = ;
	    dc_rsp_wod_i[dcu_iter].size = ;
	    dc_rsp_wod_i[dcu_iter].vc = ;
	    dc_rsp_wod_i[dcu_iter].valid = ;
	    // ECI packet for response with data. (VC 4 or 5). (only header).
	    dc_rsp_wd_hdr_i[dcu_iter].hdr = ;
	    dc_rsp_wd_hdr_i[dcu_iter].size = ;
	    dc_rsp_wd_hdr_i[dcu_iter].vc = ;
	    dc_rsp_wd_hdr_i[dcu_iter].valid = ; 
	    // Output ECI events, only header.
	    // VC # indicates VC to route to.
	    dc_eci_rsp_hdr_o[dcu_iter].hdr = ;
	    dc_eci_rsp_hdr_o[dcu_iter].size = ;
	    dc_eci_rsp_hdr_o[dcu_iter].vc = ;
	    dc_eci_rsp_hdr_o[dcu_iter].valid = ; 
	    // Output Read descriptors
	    // Read descriptors: Request and response.
	    // no data, only descriptor.
	    dc_rd_req_o[dcu_iter].ready = ;
	    dc_rd_rsp_ctrl_i[dcu_iter].id = ;
	    dc_rd_rsp_ctrl_i[dcu_iter].valid = ; 
	    // Write descriptors: Request and response.
	    // no data, only descriptor.
	    dc_wr_req_ctrl_o[dcu_iter].ready = ;
	    dc_wr_rsp_ctrl_i[dcu_iter].id = ;
	    dc_wr_rsp_ctrl_i[dcu_iter].bresp = ;
	    dc_wr_rsp_ctrl_i[dcu_iter].valid = ; 
	 end : DCUS_IP_ASSIGN
	 dcu_top #(
		   .PERF_REGS_WIDTH(PERF_REGS_WIDTH),
		   .SYNTH_PERF_REGS(SYNTH_PERF_REGS)
		    )
	 dcu_inst(
		  .clk(clk),
		  .reset(reset),
		  // Input ECI events.
		  // ECI packet for request without data. (VC 6 or 7) (only header).
		  .req_wod_hdr_i	(dc_req_wod_i[dcu_iter].hdr),
		  .req_wod_pkt_size_i	(dc_req_wod_i[dcu_iter].size),
		  .req_wod_pkt_vc_i	(dc_req_wod_i[dcu_iter].vc),
		  .req_wod_pkt_valid_i	(dc_req_wod_i[dcu_iter].valid),
		  .req_wod_pkt_ready_o	(dc_req_wod_i[dcu_iter].ready),
		  // ECI packet for response without data.(VC 10 or 11). (only header).
		  .rsp_wod_hdr_i	(dc_rsp_wod_i[dcu_iter].hdr),
		  .rsp_wod_pkt_size_i	(dc_rsp_wod_i[dcu_iter].size),
		  .rsp_wod_pkt_vc_i	(dc_rsp_wod_i[dcu_iter].vc),
		  .rsp_wod_pkt_valid_i	(dc_rsp_wod_i[dcu_iter].valid),
		  .rsp_wod_pkt_ready_o	(dc_rsp_wod_i[dcu_iter].ready),
		  // ECI packet for response with data. (VC 4 or 5). (only header).
		  .rsp_wd_hdr_i		(dc_rsp_wd_hdr_i[dcu_iter].hdr),
		  .rsp_wd_pkt_size_i	(dc_rsp_wd_hdr_i[dcu_iter].size),
		  .rsp_wd_pkt_vc_i	(dc_rsp_wd_hdr_i[dcu_iter].vc),
		  .rsp_wd_pkt_valid_i	(dc_rsp_wd_hdr_i[dcu_iter].valid),
		  .rsp_wd_pkt_ready_o	(dc_rsp_wd_hdr_i[dcu_iter].ready),
		  // Output ECI events, only header.
		  // VC # indicates VC to route to.
		  .eci_rsp_hdr_o	(dc_eci_rsp_hdr_o[dcu_iter].hdr),
		  .eci_rsp_size_o	(dc_eci_rsp_hdr_o[dcu_iter].size),
		  .eci_rsp_vc_o		(dc_eci_rsp_hdr_o[dcu_iter].vc),
		  .eci_rsp_valid_o	(dc_eci_rsp_hdr_o[dcu_iter].valid),
		  .eci_rsp_ready_i	(dc_eci_rsp_hdr_o[dcu_iter].ready),
		  // Output Read descriptors
		  // Read descriptors: Request and response.
		  // no data, only descriptor.
		  .rd_req_id_o		(dc_rd_req_o[dcu_iter].id),
		  .rd_req_addr_o	(dc_rd_req_o[dcu_iter].addr),
		  .rd_req_valid_o	(dc_rd_req_o[dcu_iter].valid),
		  .rd_req_ready_i	(dc_rd_req_o[dcu_iter].ready),
		  .rd_rsp_id_i		(dc_rd_rsp_ctrl_i[dcu_iter].id),
		  .rd_rsp_valid_i	(dc_rd_rsp_ctrl_i[dcu_iter].valid),
		  .rd_rsp_ready_o	(dc_rd_rsp_ctrl_i[dcu_iter].),
		  // Write descriptors: Request and response.
		  // no data, only descriptor.
		  .wr_req_id_o		(dc_wr_req_ctrl_o[dcu_iter].id),
		  .wr_req_addr_o	(dc_wr_req_ctrl_o[dcu_iter].addr),
		  .wr_req_strb_o	(dc_wr_req_ctrl_o[dcu_iter].strb),
		  .wr_req_valid_o	(dc_wr_req_ctrl_o[dcu_iter].valid),
		  .wr_req_ready_i	(dc_wr_req_ctrl_o[dcu_iter].ready),
		  .wr_rsp_id_i		(dc_wr_rsp_ctrl_i[dcu_iter].id),
		  .wr_rsp_bresp_i	(dc_wr_rsp_ctrl_i[dcu_iter].bresp),
		  .wr_rsp_valid_i	(dc_wr_rsp_ctrl_i[dcu_iter].valid),
		  .wr_rsp_ready_o	(dc_wr_rsp_ctrl_i[dcu_iter].ready),

		  // Performance Counters.
		  .prf_no_req_pkt_rdy_o(),        // Not connected on purpose. 
		  .prf_no_req_pkt_stl_o(),	  // Not connected on purpose. 
		  .prf_no_rsp_pkt_sent_o(),	  // Not connected on purpose. 
		  .prf_no_rd_req_o(),		  // Not connected on purpose. 
		  .prf_no_rd_rsp_o(),		  // Not connected on purpose. 
		  .prf_no_wr_req_o(),		  // Not connected on purpose. 
		  .prf_no_wr_rsp_o(),		  // Not connected on purpose. 
		  .prf_no_cyc_bw_req_vld_rdy_o(), // Not connected on purpose. 

		  // Debug interface.
		  .dbg_eci_req_hdr_o(),     // Not connected on purpose. 
		  .dbg_eci_req_vc_o(),	    // Not connected on purpose. 
		  .dbg_eci_req_size_o(),    // Not connected on purpose. 
		  .dbg_eci_req_ready_o(),   // Not connected on purpose. 
		  .dbg_eci_req_stalled_o(), // Not connected on purpose. 
		  .dbg_eci_rsp_hdr_o(),	    // Not connected on purpose. 
		  .dbg_eci_rsp_vc_o(),	    // Not connected on purpose. 
		  .dbg_eci_rsp_size_o(),    // Not connected on purpose. 
		  .dbg_eci_rsp_valid_o(),   // Not connected on purpose. 
		  .dbg_req_cc_enc_o(),	    // Not connected on purpose. 
		  .dbg_rdda_o(),	    // Not connected on purpose. 
		  .dbg_wdda_o(),	    // Not connected on purpose. 
		  .dbg_cc_active_req_o(),   // Not connected on purpose. 
		  .dbg_cc_present_state_o(),// Not connected on purpose. 
		  .dbg_cc_next_state_o(),   // Not connected on purpose. 
		  .dbg_cc_next_action_o(),  // Not connected on purpose. 
		  .dbg_err_code_o(),	    // Not connected on purpose. 
		  .dbg_error_o(),	    // Not connected on purpose. 
		  .dbg_valid_o()	    // Not connected on purpose. 
		  );
      end : DCUS
   endgenerate


endmodule // dcs_dcus

`endif
