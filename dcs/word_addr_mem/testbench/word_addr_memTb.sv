
module word_addr_memTb();

   parameter ID_WIDTH = 6;
   parameter ADDR_WIDTH = 10;
   parameter DATA_WIDTH = 1024;

   //input output ports 
   //Input signals
   logic 		  clk;
   logic 		  reset;
   logic [ID_WIDTH-1:0]   rd_req_id_i;
   logic [ADDR_WIDTH-1:0] rd_req_addr_i;
   logic 		  rd_req_valid_i;
   logic 		  rd_rsp_ready_i;
   logic [ID_WIDTH-1:0]   wr_req_id_i;
   logic [ADDR_WIDTH-1:0] wr_req_addr_i;
   logic [DATA_WIDTH-1:0] wr_req_data_i;
   logic 		  wr_req_valid_i;
   logic 		  wr_rsp_ready_i;

   //Output signals
   logic 		  rd_req_ready_o;
   logic [ID_WIDTH-1:0]   rd_rsp_id_o;
   logic [DATA_WIDTH-1:0] rd_rsp_data_o;
   logic 		  rd_rsp_valid_o;
   logic 		  wr_req_ready_o;
   logic [ID_WIDTH-1:0]   wr_rsp_id_o;
   logic [1:0] 		  wr_rsp_bresp_o;
   logic 		  wr_rsp_valid_o;


   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   word_addr_mem word_addr_mem1 (
				 .clk(clk),
				 .reset(reset),
				 .rd_req_id_i(rd_req_id_i),
				 .rd_req_addr_i(rd_req_addr_i),
				 .rd_req_valid_i(rd_req_valid_i),
				 .rd_rsp_ready_i(rd_rsp_ready_i),
				 .wr_req_id_i(wr_req_id_i),
				 .wr_req_addr_i(wr_req_addr_i),
				 .wr_req_data_i(wr_req_data_i),
				 .wr_req_valid_i(wr_req_valid_i),
				 .wr_rsp_ready_i(wr_rsp_ready_i),
				 .rd_req_ready_o(rd_req_ready_o),
				 .rd_rsp_id_o(rd_rsp_id_o),
				 .rd_rsp_data_o(rd_rsp_data_o),
				 .rd_rsp_valid_o(rd_rsp_valid_o),
				 .wr_req_ready_o(wr_req_ready_o),
				 .wr_rsp_id_o(wr_rsp_id_o),
				 .wr_rsp_bresp_o(wr_rsp_bresp_o),
				 .wr_rsp_valid_o(wr_rsp_valid_o)
				 );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports 
      clk = '0;
      reset = '1;
      rd_req_id_i = '0;
      rd_req_addr_i = '0;
      rd_req_valid_i = '0;
      rd_rsp_ready_i = '0;
      wr_req_id_i = '0;
      wr_req_addr_i = '0;
      wr_req_data_i = '0;
      wr_req_valid_i = '0;
      wr_rsp_ready_i = '0;

      ##5;
      reset = 1'b0;
      ##5;
      write(.id_i(5), .addr_i('d0), .data_i('d125));
      read(.id_i(6), .addr_i('d0));
      //assert(rd_rsp_id == 'd6) else $error("ID does not match");
      //assert(rd_rsp_data_o == 'd125) else $error("read data is different from written data for addr 0");
      ##1;
      write(.id_i(7), .addr_i('d1), .data_i('d256));
      read(.id_i(8), .addr_i('d1));
      //assert(rd_rsp_id == 'd8) else $error("ID does not match");
      //assert(rd_rsp_data_o == 'd256) else $error("read data is different from written data for addr 0");
      

      #500 $finish;
   end

   // send write request, wait for wr req hs.
   // then wr resp channel is ready, wait for wr rsp hs.
   task static write(
		     input logic [ID_WIDTH-1:0]   id_i,
		     input logic [ADDR_WIDTH-1:0] addr_i,
		     input logic [DATA_WIDTH-1:0] data_i
		     );
      wr_req_id_i = id_i;
      wr_req_addr_i = addr_i;
      wr_req_data_i = data_i;
      wr_req_valid_i = 1'b1;
      wr_rsp_ready_i = 1'b0;
      wait(wr_req_valid_i & wr_req_ready_o);
      ##1;
      wr_req_valid_i = 1'b0;
      wr_rsp_ready_i = 1'b1;
      wait(wr_rsp_valid_o & wr_rsp_ready_i);
      ##1;
      wr_rsp_ready_i = 1'b0;
   endtask //write


   // send read request, wait for rd req hs.
   // then rd resp channel is ready, wait for hs.
   task static read(
		    input logic [ID_WIDTH-1:0] 	 id_i,
		    input logic [ADDR_WIDTH-1:0] addr_i
		    );
      rd_req_id_i = id_i;
      rd_req_addr_i = addr_i;
      rd_req_valid_i = 1'b1;
      rd_rsp_ready_i = 1'b0;
      wait(rd_req_valid_i & rd_req_ready_o);
      ##1;
      rd_req_valid_i = 1'b0;
      rd_rsp_ready_i = 1'b1;
      wait(rd_rsp_valid_o & rd_rsp_ready_i);
      ##1;
      rd_rsp_ready_i = 1'b0;
   endtask //read
endmodule
