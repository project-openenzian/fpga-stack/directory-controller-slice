#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
$XILINX_VIVADO/data/verilog/src/glbl.v \
../testbench/map_ecid_to_wrdTb.sv \
../rtl/map_ecid_to_wrd.sv 

xelab -debug typical -incremental -L xpm worklib.map_ecid_to_wrdTb worklib.glbl -s worklib.map_ecid_to_wrdTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.map_ecid_to_wrdTb 
xsim -R worklib.map_ecid_to_wrdTb 
