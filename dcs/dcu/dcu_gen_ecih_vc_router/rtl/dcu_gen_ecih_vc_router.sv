/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-01-28
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */


`ifndef DCU_GEN_ECIH_VC_ROUTER_SV
`define DCU_GEN_ECIH_VC_ROUTER_SV

 `define DCU_GEN_ECIH_VC_ROUTER_SV
/*
 * Module Description:
 *  Module to route ECI header (no payload) to one of two output channels.
 *  An input "vc number" signal is used to route the input stream to one of the two output streams.
 *  
 *  Intended to be used as a stage in routing. 
 * 
 * Routing based on select signal 
 *  parameter VC_BOUNDARY is used to select the output channel. If input VC number is >= VC_BOUNDARY 
 *  output channel 1 is selected otherwise output channel 0 is selected
 * 
 * Flow control 
 *  For a valid input, only one of the output stream is valid. Module does not accept a new input 
 *  till a handshake is received on the previously selected output channel 
 *
 * Input Output Description:
 *  Input channel - ECI header, ECI packet size, VC number, valid, ready
 *  Output channel0 - ECI header, ECI packet size, VC number, valid, ready
 *  Output channel1 - ECI header, ECI packet size, VC number, valid, ready
 *
 * Architecture Description:
 *  The VC number is used to generate a select signal for the generic axis_2_router module. 
 *
 * Modifiable Parameters:
 *   VC_BOUNDARY - Necessary to set to right value to route properly 
 *
 * Modules Used:
 *  axis_2_router
 *
 * Notes:
 *  II=1
 *
 */

import eci_cmd_defs::*;
module dcu_gen_ecih_vc_router #
  (
   // VCs above 4 goto channel 1, else goto channel 0
   parameter VC_BOUNDARY = 4
   )
   (
    input logic 			     clk,
    input logic 			     reset,
    // Input ECI header (no payload)
    input logic [ECI_WORD_WIDTH-1:0] 	     eci_hdr_i,
    input logic [ECI_PACKET_SIZE_WIDTH-1:0]  eci_pkt_size_i,
    input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0]  eci_pkt_vc_i,
    input logic 			     eci_pkt_valid_i,
    output logic 			     eci_pkt_ready_o,
    // Output ECI header channel 0 (no payload)
    output logic [ECI_WORD_WIDTH-1:0] 	     eci_hdr0_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] eci_pkt0_size_o,
    output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] eci_pkt0_vc_o,
    output logic 			     eci_pkt0_valid_o,
    input logic 			     eci_pkt0_ready_i,
    // Output ECI header channel 1 (no payload)
    output logic [ECI_WORD_WIDTH-1:0] 	     eci_hdr1_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] eci_pkt1_size_o,
    output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] eci_pkt1_vc_o,
    output logic 			     eci_pkt1_valid_o,
    input logic 			     eci_pkt1_ready_i
    );

   parameter AXIS_RTR_DATA_WIDTH = ECI_WORD_WIDTH + ECI_PACKET_SIZE_WIDTH + ECI_LCL_TOT_NUM_VCS_WIDTH;
   logic sel_down_upbar;

   // If input VC number is less than boundary, send to down channel (channel 0).
   // otherwise if VC number is greater than or equal to the boundary, send to
   // up channel (channel 1).
   always_comb begin : SEL_SIGNAL
      sel_down_upbar = 1'b0;
      if(eci_pkt_vc_i < VC_BOUNDARY) begin
	 sel_down_upbar = 1'b1;
      end else begin
	 sel_down_upbar = 1'b0;
      end
   end : SEL_SIGNAL

   // Router based on select signal.
   // if select is 1 send to down channel, if select is 0 send to up channel.
   axis_2_router #
     (
      .DATA_WIDTH(AXIS_RTR_DATA_WIDTH)
       )
   ecih_vc_rtr_inst
     (
      .clk			(clk),
      .reset			(reset),
      .us_data_i		({eci_pkt_vc_i, eci_pkt_size_i, eci_hdr_i}),
      .us_sel_down_upbar_i	(sel_down_upbar),
      .us_valid_i		(eci_pkt_valid_i),
      .us_ready_o		(eci_pkt_ready_o),
      .ds_up_data_o		({eci_pkt1_vc_o, eci_pkt1_size_o, eci_hdr1_o}),
      .ds_up_valid_o		(eci_pkt1_valid_o),
      .ds_up_ready_i		(eci_pkt1_ready_i),
      .ds_down_data_o		({eci_pkt0_vc_o, eci_pkt0_size_o, eci_hdr0_o}),
      .ds_down_valid_o		(eci_pkt0_valid_o),
      .ds_down_ready_i		(eci_pkt0_ready_i)
      );
   
endmodule // dcu_gen_ecih_vc_router
`endif
