#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
      $XILINX_VIVADO/data/verilog/src/glbl.v \
      ../rtl/eci_cmd_defs.sv \
      ../rtl/eci_cc_defs.sv \
      ../rtl/eci_dcs_defs.sv \
      ../rtl/eci_dirc_defs.sv \
      ../perf_sim_tb/perf_simTb.sv \
      ../testbench/word_addr_mem.sv \
      ../rtl/wr_trmgr.sv \
      ../rtl/dcu_controller.sv \
      ../rtl/rd_data_path.sv \
      ../rtl/dcu_gen_ecih_vc_router.sv \
      ../rtl/dcu_tsu.sv \
      ../rtl/rd_trmgr.sv \
      ../rtl/gen_out_header.sv \
      ../rtl/dp_data_store.sv \
      ../rtl/decode_eci_req.sv \
      ../rtl/eci_cc_table.sv \
      ../rtl/dcu.sv \
      ../rtl/axis_2_router.sv \
      ../rtl/dp_gen_path.sv \
      ../rtl/map_ecid_to_wrd.sv \
      ../rtl/wr_data_path.sv \
      ../rtl/dp_gate.sv \
      ../rtl/dcu_to_vc_router.sv \
      ../rtl/dcs_single_dcu.sv \
      ../rtl/dcu_top.sv \
      ../rtl/dp_mem.sv \
      ../rtl/dp_wr_ser.sv \
      ../rtl/axis_pipeline_stage.sv \
      ../rtl/tag_state_ram.sv \
      ../rtl/ram_tdp.sv \
      ../rtl/arb_3_ecih.sv \
      ../../perf_sim_modules/seq_read_load_gen/rtl/seq_read_load_gen.sv \
      ../../perf_sim_modules/evt_delay_buffer/rtl/evt_delay_buffer.sv


xelab -debug typical -incremental -L xpm worklib.perf_simTb worklib.glbl -s worklib.perf_simTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.perf_simTb 
xsim -R worklib.perf_simTb 
