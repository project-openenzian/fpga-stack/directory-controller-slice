#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
      $XILINX_VIVADO/data/verilog/src/glbl.v \
      ../rtl/eci_cmd_defs.sv \
      ../rtl/eci_dcs_defs.sv \
      ../testbench/cli_lat_load_genTb.sv \
      ../rtl/cli_lat_load_gen.sv \
      ../rtl/axis_pipeline_stage.sv \
      ../testbench/perf_gen_seq_aliased.sv \
      ../rtl/axis_xpm_fifo.sv 

xelab -debug typical -incremental -L xpm worklib.cli_lat_load_genTb worklib.glbl -s worklib.cli_lat_load_genTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.cli_lat_load_genTb 
xsim -gui worklib.cli_lat_load_genTb 
