/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-09-20
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */

`ifndef DCS_SV
`define DCS_SV

import eci_cmd_defs::*;
import eci_dcs_defs::*;
import eci_dirc_defs::*; // DCU specific defs. 
import eci_cc_defs::*;   // CC specific defs to support dirc debug interface.


// This module splits incoming and outgoing ECI
// packets into control and data path.
// Control path is connected to dcs_dcus.
// There are two data path, one for read and another for write. 

// Debug and performance counters are not currently
// exposed to software. This will eventually have
// to be connected to CPU.

module dcs #
  (
   parameter PERF_REGS_WIDTH = 64,
   parameter SYNTH_PERF_REGS = 0   //0, 1
   )
   (
    input logic 					   clk,
    input logic 					   reset,
    
    // Input ECI header only events.
    // Request without data: VC 6 or 7.
    input logic [ECI_WORD_WIDTH-1:0] 			   req_wod_hdr_i,
    input logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   req_wod_pkt_size_i,
    input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	   req_wod_pkt_vc_i,
    input logic 					   req_wod_pkt_valid_i,
    output logic 					   req_wod_pkt_ready_o,

    // Response without data: VC 10 or 11.
    input logic [ECI_WORD_WIDTH-1:0] 			   rsp_wod_hdr_i,
    input logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   rsp_wod_pkt_size_i,
    input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	   rsp_wod_pkt_vc_i,
    input logic 					   rsp_wod_pkt_valid_i,
    output logic 					   rsp_wod_pkt_ready_o,

    // Input ECI header + data events.
    // Response with data: VC 4 or 5.
    input logic [ECI_PACKET_SIZE-1:0][ECI_WORD_WIDTH-1:0]  rsp_wd_pkt_i,
    input logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   rsp_wd_pkt_size_i,
    input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	   rsp_wd_pkt_vc_i,
    input logic 					   rsp_wd_pkt_valid_i,
    output logic 					   rsp_wd_pkt_ready_o,

    // LCL packet for clean, clean invalidates. (VC 16 or 17) (only header).
    input logic [ECI_WORD_WIDTH-1:0] 			   lcl_fwd_wod_hdr_i,
    input logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   lcl_fwd_wod_pkt_size_i,
    input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	   lcl_fwd_wod_pkt_vc_i,
    input logic 					   lcl_fwd_wod_pkt_valid_i,
    output logic 					   lcl_fwd_wod_pkt_ready_o,

    // LCL packet for response without data (unlock) (VC 18 or 19) (only header).
    input logic [ECI_WORD_WIDTH-1:0] 			   lcl_rsp_wod_hdr_i,
    input logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   lcl_rsp_wod_pkt_size_i,
    input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	   lcl_rsp_wod_pkt_vc_i,
    input logic 					   lcl_rsp_wod_pkt_valid_i,
    output logic 					   lcl_rsp_wod_pkt_ready_o,

    // Output ECI header only events.
    // Response without data: VC 10 or 11.
    output logic [ECI_WORD_WIDTH-1:0] 			   rsp_wod_hdr_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   rsp_wod_pkt_size_o,
    output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	   rsp_wod_pkt_vc_o,
    output logic 					   rsp_wod_pkt_valid_o,
    input logic 					   rsp_wod_pkt_ready_i,

    // Output ECI header + data events.
    // Response with data: VC 4 or 5.
    output logic [ECI_PACKET_SIZE-1:0][ECI_WORD_WIDTH-1:0] rsp_wd_pkt_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   rsp_wd_pkt_size_o,
    output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	   rsp_wd_pkt_vc_o,
    output logic 					   rsp_wd_pkt_valid_o,
    input logic 					   rsp_wd_pkt_ready_i,

    // Output ECI Fwd without data headers: VC 8 or 9.
    output logic [ECI_WORD_WIDTH-1:0] 			   fwd_wod_hdr_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   fwd_wod_pkt_size_o,
    output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	   fwd_wod_pkt_vc_o,
    output logic 					   fwd_wod_pkt_valid_o,
    input logic 					   fwd_wod_pkt_ready_i,

    // Output lcl rsp without data headers: VC 18 or 19.
    output logic [ECI_WORD_WIDTH-1:0] 			   lcl_rsp_wod_hdr_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   lcl_rsp_wod_pkt_size_o,
    output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	   lcl_rsp_wod_pkt_vc_o,
    output logic 					   lcl_rsp_wod_pkt_valid_o,
    input logic 					   lcl_rsp_wod_pkt_ready_i,

    // Output Read descriptors
    // Read descriptors: Request and response.
    output logic [MAX_DCU_ID_WIDTH-1:0] 		   rd_req_id_o,
    output logic [DS_ADDR_WIDTH-1:0] 			   rd_req_addr_o,
    output logic 					   rd_req_valid_o,
    input logic 					   rd_req_ready_i,

    input logic [MAX_DCU_ID_WIDTH-1:0] 			   rd_rsp_id_i,
    input logic [ECI_CL_WIDTH-1:0] 			   rd_rsp_data_i,
    input logic 					   rd_rsp_valid_i,
    output logic 					   rd_rsp_ready_o,

    // Write descriptors: Request and response.
    output logic [MAX_DCU_ID_WIDTH-1:0] 		   wr_req_id_o,
    output logic [DS_ADDR_WIDTH-1:0] 			   wr_req_addr_o,
    output logic [ECI_CL_WIDTH-1:0] 			   wr_req_data_o,
    output logic [ECI_CL_SIZE_BYTES-1:0] 		   wr_req_strb_o, 
    output logic 					   wr_req_valid_o,
    input logic 					   wr_req_ready_i,

    input logic [MAX_DCU_ID_WIDTH-1:0] 			   wr_rsp_id_i,
    input logic [1:0] 					   wr_rsp_bresp_i,
    input logic 					   wr_rsp_valid_i,
    output logic 					   wr_rsp_ready_o,

    // Tracing
    output logic		tracing_valid[2],
    output logic		tracing_error[2],
    output logic [39:0] tracing_cli[2],
    output logic [6:0]	tracing_state[2],
    output logic [3:0]	tracing_action[2],
    output logic [4:0]	tracing_request[2]    
    );

   localparam ECI_HDR_PKT_WIDTH = ECI_WORD_WIDTH + ECI_PACKET_SIZE_WIDTH + 
				  ECI_LCL_TOT_NUM_VCS_WIDTH;
   
   typedef struct packed {
      logic [ECI_WORD_WIDTH-1:0]        hdr;
      logic [ECI_PACKET_SIZE_WIDTH-1:0] size;
      logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] vc;
      logic				valid;
      logic				ready;
   } eci_hdr_if_t;

   typedef struct packed {
      logic [ECI_PACKET_SIZE-1:0][ECI_WORD_WIDTH-1:0] pkt;
      logic [ECI_PACKET_SIZE_WIDTH-1:0] 	      size;
      logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	      vc;
      logic					      valid;
      logic					      ready;
   } eci_pkt_if_t;

   typedef struct packed {
      logic [MAX_DCU_ID_WIDTH-1:0]		   id;
      logic [DS_ADDR_WIDTH-1:0]			   addr;
      logic					   valid;
      logic					   ready;
   } rd_req_ctrl_if_t;

   typedef struct packed {
      logic [MAX_DCU_ID_WIDTH-1:0] id;
      logic 			   valid;
      logic 			   ready;
   } rd_rsp_ctrl_if_t;

   typedef logic [ECI_CL_WIDTH-1:0] rd_rsp_data_if_t;

   typedef struct packed {
      logic [MAX_DCU_ID_WIDTH-1:0]  id;
      logic [DS_ADDR_WIDTH-1:0]     addr;
      logic [ECI_CL_SIZE_BYTES-1:0] strb; 
      logic 			    valid;
      logic 			    ready;
   } wr_req_ctrl_if_t;

   typedef logic [ECI_CL_WIDTH-1:0] wr_req_data_if_t;

   typedef struct packed {
      logic [MAX_DCU_ID_WIDTH-1:0]  id;
      logic [1:0] 		    bresp;
      logic 			    valid;
      logic 			    ready;
   } wr_rsp_ctrl_if_t;

   typedef struct packed {
      logic [PERF_REGS_WIDTH-1:0] no_req_pkt_rdy;
      logic [PERF_REGS_WIDTH-1:0] no_req_pkt_stl;
      logic [PERF_REGS_WIDTH-1:0] no_rsp_pkt_sent;
      logic [PERF_REGS_WIDTH-1:0] no_rd_req;
      logic [PERF_REGS_WIDTH-1:0] no_rd_rsp;
      logic [PERF_REGS_WIDTH-1:0] no_wr_req;
      logic [PERF_REGS_WIDTH-1:0] no_wr_rsp;
      logic [PERF_REGS_WIDTH-1:0] no_cyc_bw_req_vld_rdy;
   } perf_counters_if_t;

   typedef struct packed {
      // Debug interface.
      logic [ECI_WORD_WIDTH-1:0]        eci_req_hdr;
      logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] eci_req_vc;
      logic [ECI_PACKET_SIZE_WIDTH-1:0] eci_req_size;
      logic 				eci_req_ready;
      logic 				eci_req_stalled;
      logic [ECI_WORD_WIDTH-1:0] 	eci_rsp_hdr;
      logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] eci_rsp_vc;
      logic [ECI_PACKET_SIZE_WIDTH-1:0] eci_rsp_size;
      logic 				eci_rsp_valid;
      cc_req_t				req_cc_enc;
      logic 				rdda;
      logic 				wdda;
      cc_req_t				cc_active_req;
      cc_state_t			cc_present_state;
      cc_state_t			cc_next_state;
      cc_action_t			cc_next_action;
      dirc_err_t			err_code;
      logic 				error;
      logic 				valid;
   } dbg_if_t;

   typedef logic [DS_DCU_IDX_WIDTH-1:0] dcu_idx_t;
   typedef logic [DS_DCU_ID_WIDTH-1:0] 	dcu_id_t;

   // DCS_DCU specific signals.   
   eci_hdr_if_t dc_req_wod_i, dc_rsp_wod_i, dc_rsp_wd_hdr_i, dc_lcl_fwd_wod_i, dc_lcl_rsp_wod_i;
   eci_hdr_if_t dc_rsp_wod_o, dc_rsp_wd_hdr_o, dc_fwd_wod_o, dc_lcl_rsp_wod_o;
   rd_req_ctrl_if_t dc_rd_req_o;
   rd_rsp_ctrl_if_t dc_rd_rsp_ctrl_i;
   wr_req_ctrl_if_t dc_wr_req_ctrl_o;
   wr_rsp_ctrl_if_t dc_wr_rsp_ctrl_i;
   // Read data path specific signals.
   rd_rsp_ctrl_if_t rdp_rsp_mem_ctrl_i;
   rd_rsp_data_if_t rdp_rsp_mem_data_i;
   rd_rsp_ctrl_if_t rdp_rsp_dcu_ctrl_o;
   dcu_idx_t rdp_rsp_dcu_idx_o;
   eci_hdr_if_t rdp_rsp_wd_hdr_i;
   eci_pkt_if_t rdp_rsp_wd_pkt_o;
   // Write data path specific signals.
   eci_pkt_if_t wdp_rsp_wd_pkt_i;
   dcu_idx_t wdp_rsp_wd_dcu_idx_o;
   eci_hdr_if_t wdp_rsp_wd_hdr_o;
   wr_req_ctrl_if_t wdp_req_dcu_ctrl_i;
   wr_req_ctrl_if_t wdp_req_mem_ctrl_o;
   wr_req_data_if_t wdp_req_mem_data_o;
   
   always_comb begin : OUT_ASSIGN
      // Request without data: VC 6 or 7.
      req_wod_pkt_ready_o = dc_req_wod_i.ready;
      // Response without data: VC 10 or 11.
      rsp_wod_pkt_ready_o = dc_rsp_wod_i.ready;
      // Response with data: VC 4 or 5.
      rsp_wd_pkt_ready_o  = wdp_rsp_wd_pkt_i.ready;
      // lcl clean clean inv VC 16 or 17.
      lcl_fwd_wod_pkt_ready_o = dc_lcl_fwd_wod_i.ready;
      // lcl rsp wod unlock VC 18 or 19.
      lcl_rsp_wod_pkt_ready_o = dc_lcl_rsp_wod_i.ready;
      // Output ECI header only events.
      // Response without data: VC 10 or 11.
      rsp_wod_hdr_o		= dc_rsp_wod_o.hdr;
      rsp_wod_pkt_size_o	= dc_rsp_wod_o.size;
      rsp_wod_pkt_vc_o		= dc_rsp_wod_o.vc;
      rsp_wod_pkt_valid_o	= dc_rsp_wod_o.valid;
      // Output ECI header + data events.
      // Response with data: VC 4 or 5.
      rsp_wd_pkt_o		= rdp_rsp_wd_pkt_o.pkt;
      rsp_wd_pkt_size_o		= rdp_rsp_wd_pkt_o.size;
      rsp_wd_pkt_vc_o		= rdp_rsp_wd_pkt_o.vc;
      rsp_wd_pkt_valid_o	= rdp_rsp_wd_pkt_o.valid;
      // Output fwd_wod : VC 8 or 9.
      fwd_wod_hdr_o             = dc_fwd_wod_o.hdr;
      fwd_wod_pkt_size_o        = dc_fwd_wod_o.size;
      fwd_wod_pkt_vc_o          = dc_fwd_wod_o.vc;
      fwd_wod_pkt_valid_o       = dc_fwd_wod_o.valid;
      // output lcl_rsp: VC 18 or 19.
      lcl_rsp_wod_hdr_o         = dc_lcl_rsp_wod_o.hdr;
      lcl_rsp_wod_pkt_size_o    = dc_lcl_rsp_wod_o.size;
      lcl_rsp_wod_pkt_vc_o      = dc_lcl_rsp_wod_o.vc;
      lcl_rsp_wod_pkt_valid_o   = dc_lcl_rsp_wod_o.valid;
      // Output Read descriptors
      // Read descriptors: Request and response.
      rd_req_id_o		= dc_rd_req_o.id;
      rd_req_addr_o		= dc_rd_req_o.addr;
      rd_req_valid_o		= dc_rd_req_o.valid;
      rd_rsp_ready_o		= rdp_rsp_mem_ctrl_i.ready;
      // Write descriptors: Request and response.
      wr_req_id_o		= wdp_req_mem_ctrl_o.id;
      wr_req_addr_o		= wdp_req_mem_ctrl_o.addr;
      wr_req_data_o		= wdp_req_mem_data_o;
      wr_req_strb_o		= wdp_req_mem_ctrl_o.strb; 
      wr_req_valid_o		= wdp_req_mem_ctrl_o.valid;
      wr_rsp_ready_o		= dc_wr_rsp_ctrl_i.ready;
   end : OUT_ASSIGN

   // DCS_DCUs
   always_comb begin : DCS_DCUS_IP_ASSIGN
      // Input ECI header only events.
      // req_wod
      dc_req_wod_i.hdr		= req_wod_hdr_i;
      dc_req_wod_i.size		= req_wod_pkt_size_i;
      dc_req_wod_i.vc		= req_wod_pkt_vc_i;
      dc_req_wod_i.valid	= req_wod_pkt_valid_i;
      // rsp_wod
      dc_rsp_wod_i.hdr		= rsp_wod_hdr_i;
      dc_rsp_wod_i.size		= rsp_wod_pkt_size_i;
      dc_rsp_wod_i.vc		= rsp_wod_pkt_vc_i;
      dc_rsp_wod_i.valid	= rsp_wod_pkt_valid_i;
      // Input ECI header + data events but
      // only header is input here.
      // rsp_wd
      dc_rsp_wd_hdr_i.hdr	= wdp_rsp_wd_hdr_o.hdr;
      dc_rsp_wd_hdr_i.size	= wdp_rsp_wd_hdr_o.size;
      dc_rsp_wd_hdr_i.vc	= wdp_rsp_wd_hdr_o.vc;
      dc_rsp_wd_hdr_i.valid	= wdp_rsp_wd_hdr_o.valid;
      // lcl clean, inv header
      dc_lcl_fwd_wod_i.hdr	= lcl_fwd_wod_hdr_i;
      dc_lcl_fwd_wod_i.size	= lcl_fwd_wod_pkt_size_i;
      dc_lcl_fwd_wod_i.vc	= lcl_fwd_wod_pkt_vc_i;
      dc_lcl_fwd_wod_i.valid	= lcl_fwd_wod_pkt_valid_i;
      // lcl rsp wod unlock header.
      dc_lcl_rsp_wod_i.hdr	= lcl_rsp_wod_hdr_i;
      dc_lcl_rsp_wod_i.size	= lcl_rsp_wod_pkt_size_i;
      dc_lcl_rsp_wod_i.vc	= lcl_rsp_wod_pkt_vc_i;
      dc_lcl_rsp_wod_i.valid	= lcl_rsp_wod_pkt_valid_i;
      // Output ECI header only events.
      dc_rsp_wod_o.ready        = rsp_wod_pkt_ready_i;
      dc_rsp_wd_hdr_o.ready     = rdp_rsp_wd_hdr_i.ready;
      dc_fwd_wod_o.ready        = fwd_wod_pkt_ready_i;
      dc_lcl_rsp_wod_o.ready    = lcl_rsp_wod_pkt_ready_i;
      // Output read descriptors
      // Read descriptors: Request.
      dc_rd_req_o.ready		= rd_req_ready_i;
      // Read descriptors: Response control
      // no data here.
      dc_rd_rsp_ctrl_i.id	= rdp_rsp_dcu_ctrl_o.id;
      dc_rd_rsp_ctrl_i.valid	= rdp_rsp_dcu_ctrl_o.valid; 
      // Output write descriptors
      // Write requests only control, no data here.
      dc_wr_req_ctrl_o.ready	= wdp_req_dcu_ctrl_i.ready;
      // Write response.
      dc_wr_rsp_ctrl_i.id	= wr_rsp_id_i;
      dc_wr_rsp_ctrl_i.bresp	= wr_rsp_bresp_i;
      dc_wr_rsp_ctrl_i.valid	= wr_rsp_valid_i; 
   end : DCS_DCUS_IP_ASSIGN

   dcs_dcus #
     (
      .PERF_REGS_WIDTH(PERF_REGS_WIDTH),
      .SYNTH_PERF_REGS(SYNTH_PERF_REGS)
       )
   dcs_dcus_inst
     (
      .clk(clk),
      .reset(reset),
      // Input ECI events.
      // ECI packet for request without data. VC 6 or 7, only header.
      .req_wod_hdr_i		(dc_req_wod_i.hdr),
      .req_wod_pkt_size_i	(dc_req_wod_i.size),
      .req_wod_pkt_vc_i		(dc_req_wod_i.vc),
      .req_wod_pkt_valid_i	(dc_req_wod_i.valid),
      .req_wod_pkt_ready_o	(dc_req_wod_i.ready),
      // ECI packet for response without data. VC 10 or 11, only header.
      .rsp_wod_hdr_i		(dc_rsp_wod_i.hdr),
      .rsp_wod_pkt_size_i	(dc_rsp_wod_i.size),
      .rsp_wod_pkt_vc_i		(dc_rsp_wod_i.vc),
      .rsp_wod_pkt_valid_i	(dc_rsp_wod_i.valid),
      .rsp_wod_pkt_ready_o	(dc_rsp_wod_i.ready),
      // ECI packet for response with data. VC 4 or 5, only header
      .rsp_wd_hdr_i		(dc_rsp_wd_hdr_i.hdr),
      .rsp_wd_pkt_size_i	(dc_rsp_wd_hdr_i.size),
      .rsp_wd_pkt_vc_i		(dc_rsp_wd_hdr_i.vc),
      .rsp_wd_pkt_valid_i	(dc_rsp_wd_hdr_i.valid),
      .rsp_wd_pkt_ready_o	(dc_rsp_wd_hdr_i.ready),
      // lcl packet for clean, clean invalidate VC 16 or 17.
      .lcl_fwd_wod_hdr_i	(dc_lcl_fwd_wod_i.hdr),
      .lcl_fwd_wod_pkt_size_i	(dc_lcl_fwd_wod_i.size),
      .lcl_fwd_wod_pkt_vc_i	(dc_lcl_fwd_wod_i.vc),
      .lcl_fwd_wod_pkt_valid_i	(dc_lcl_fwd_wod_i.valid),
      .lcl_fwd_wod_pkt_ready_o	(dc_lcl_fwd_wod_i.ready),
      // lcl packet for rsp wod unlock VC 18 or 19.
      .lcl_rsp_wod_hdr_i	(dc_lcl_rsp_wod_i.hdr),
      .lcl_rsp_wod_pkt_size_i	(dc_lcl_rsp_wod_i.size),
      .lcl_rsp_wod_pkt_vc_i	(dc_lcl_rsp_wod_i.vc),
      .lcl_rsp_wod_pkt_valid_i	(dc_lcl_rsp_wod_i.valid),
      .lcl_rsp_wod_pkt_ready_o	(dc_lcl_rsp_wod_i.ready),
      // Output ECI header only events.
      // Response without data: VC 10 or 11.
      .rsp_wod_hdr_o		(dc_rsp_wod_o.hdr),
      .rsp_wod_pkt_size_o	(dc_rsp_wod_o.size),
      .rsp_wod_pkt_vc_o		(dc_rsp_wod_o.vc),
      .rsp_wod_pkt_valid_o	(dc_rsp_wod_o.valid),
      .rsp_wod_pkt_ready_i	(dc_rsp_wod_o.ready),
      // Output ECI header + data events but only header.
      // Response with data headers: VC 4 or 5. only header.
      .rsp_wd_hdr_o		(dc_rsp_wd_hdr_o.hdr),
      .rsp_wd_pkt_size_o	(dc_rsp_wd_hdr_o.size),
      .rsp_wd_pkt_vc_o		(dc_rsp_wd_hdr_o.vc),
      .rsp_wd_pkt_valid_o	(dc_rsp_wd_hdr_o.valid),
      .rsp_wd_pkt_ready_i	(dc_rsp_wd_hdr_o.ready),
      // Output ECI fwd VC 8,9
      .fwd_wod_hdr_o		(dc_fwd_wod_o.hdr),
      .fwd_wod_pkt_size_o	(dc_fwd_wod_o.size),
      .fwd_wod_pkt_vc_o		(dc_fwd_wod_o.vc),
      .fwd_wod_pkt_valid_o	(dc_fwd_wod_o.valid),
      .fwd_wod_pkt_ready_i	(dc_fwd_wod_o.ready),
      // output local response VC 18,19
      .lcl_rsp_wod_hdr_o	(dc_lcl_rsp_wod_o.hdr),
      .lcl_rsp_wod_pkt_size_o	(dc_lcl_rsp_wod_o.size),
      .lcl_rsp_wod_pkt_vc_o	(dc_lcl_rsp_wod_o.vc),
      .lcl_rsp_wod_pkt_valid_o	(dc_lcl_rsp_wod_o.valid),
      .lcl_rsp_wod_pkt_ready_i	(dc_lcl_rsp_wod_o.ready),
      // Output read descriptors
      // Read descriptors: Request.
      .rd_req_id_o	(dc_rd_req_o.id),
      .rd_req_addr_o	(dc_rd_req_o.addr),
      .rd_req_valid_o	(dc_rd_req_o.valid),
      .rd_req_ready_i	(dc_rd_req_o.ready),
      // Read descriptors: Response control
      // no data here.
      .rd_rsp_id_i	(dc_rd_rsp_ctrl_i.id),
      .rd_rsp_valid_i	(dc_rd_rsp_ctrl_i.valid),
      .rd_rsp_ready_o	(dc_rd_rsp_ctrl_i.ready),
      // Output write descriptors
      // Write requests only control, no data here.
      .wr_req_id_o	(dc_wr_req_ctrl_o.id),
      .wr_req_addr_o	(dc_wr_req_ctrl_o.addr),
      .wr_req_strb_o	(dc_wr_req_ctrl_o.strb),
      .wr_req_valid_o	(dc_wr_req_ctrl_o.valid),
      .wr_req_ready_i	(dc_wr_req_ctrl_o.ready),
      // Write response.
      .wr_rsp_id_i	(dc_wr_rsp_ctrl_i.id),
      .wr_rsp_bresp_i	(dc_wr_rsp_ctrl_i.bresp),
      .wr_rsp_valid_i	(dc_wr_rsp_ctrl_i.valid),
      .wr_rsp_ready_o	(dc_wr_rsp_ctrl_i.ready),
    // Tracing
        .tracing_valid      (tracing_valid),
        .tracing_error      (tracing_error),
        .tracing_cli        (tracing_cli),
        .tracing_state      (tracing_state),
        .tracing_action     (tracing_action),
        .tracing_request    (tracing_request)
      );
   
   // Read from byte addressable mem data path.
   always_comb begin : RDP_IP_ASSIGN
      // Read resposne + data from byte addressable memory.
      rdp_rsp_mem_ctrl_i.id	= rd_rsp_id_i;
      rdp_rsp_mem_data_i	= rd_rsp_data_i;
      rdp_rsp_mem_ctrl_i.valid	= rd_rsp_valid_i;
      // read response only routed to DCU.
      rdp_rsp_dcu_ctrl_o.ready	= dc_rd_rsp_ctrl_i.ready;
      // VC 4,5 Input - header only from DCU
      rdp_rsp_wd_hdr_i.hdr	= dc_rsp_wd_hdr_o.hdr;
      rdp_rsp_wd_hdr_i.size	= dc_rsp_wd_hdr_o.size;
      rdp_rsp_wd_hdr_i.vc	= dc_rsp_wd_hdr_o.vc;
      rdp_rsp_wd_hdr_i.valid	= dc_rsp_wd_hdr_o.valid;
      // VC 4,5 Output header + data to TX to CPU.
      rdp_rsp_wd_pkt_o.ready	= rsp_wd_pkt_ready_i;
   end : RDP_IP_ASSIGN
   
   rd_data_path 
     dcs_rd_data_path 
       (
	.clk			(clk),
	.reset			(reset),
	// Read resposne + data from byte addressable memory.
	.rd_rsp_id_i		(rdp_rsp_mem_ctrl_i.id),
	.rd_rsp_data_i		(rdp_rsp_mem_data_i),
	.rd_rsp_valid_i		(rdp_rsp_mem_ctrl_i.valid),
	.rd_rsp_ready_o		(rdp_rsp_mem_ctrl_i.ready),
	// read response only (no data) routed to DCU.
	// IDX Not connected as IDX is identified internally. 
	.rd_rsp_dcu_idx_o	(), // Not connected. 
	.rd_rsp_id_o		(rdp_rsp_dcu_ctrl_o.id),
	.rd_rsp_valid_o		(rdp_rsp_dcu_ctrl_o.valid),
	.rd_rsp_ready_i		(rdp_rsp_dcu_ctrl_o.ready),
	// VC 4,5 Input - header only from DCU
	.rsp_wd_hdr_i		(rdp_rsp_wd_hdr_i.hdr),
	.rsp_wd_pkt_size_i	(rdp_rsp_wd_hdr_i.size),
	.rsp_wd_pkt_vc_i	(rdp_rsp_wd_hdr_i.vc),
	.rsp_wd_pkt_valid_i	(rdp_rsp_wd_hdr_i.valid),
	.rsp_wd_pkt_ready_o	(rdp_rsp_wd_hdr_i.ready),
	// VC 4,5 Output header + data to TX to CPU.
	.rsp_wd_pkt_o		(rdp_rsp_wd_pkt_o.pkt),
	.rsp_wd_pkt_size_o	(rdp_rsp_wd_pkt_o.size),
	.rsp_wd_pkt_vc_o	(rdp_rsp_wd_pkt_o.vc),
	.rsp_wd_pkt_valid_o	(rdp_rsp_wd_pkt_o.valid),
	.rsp_wd_pkt_ready_i	(rdp_rsp_wd_pkt_o.ready)
	);

   // write to byte addressable mem data path.
   always_comb begin : WDP_IP_ASSIGN
      // ECI packet for response with data 
      // (VC 4 or 5). (header + data) from CPU. 
      wdp_rsp_wd_pkt_i.pkt	= rsp_wd_pkt_i;
      wdp_rsp_wd_pkt_i.size	= rsp_wd_pkt_size_i;
      wdp_rsp_wd_pkt_i.vc	= rsp_wd_pkt_vc_i;
      wdp_rsp_wd_pkt_i.valid	= rsp_wd_pkt_valid_i;
      // Output ECI header only for response with data
      // to be routed to the DCUs.
      wdp_rsp_wd_hdr_o.ready	= dc_rsp_wd_hdr_i.ready;
      // Input Write descriptors: Request from DCUs. 
      // no data, only descriptor.
      wdp_req_dcu_ctrl_i.id	= dc_wr_req_ctrl_o.id;
      wdp_req_dcu_ctrl_i.addr	= dc_wr_req_ctrl_o.addr;
      wdp_req_dcu_ctrl_i.strb	= dc_wr_req_ctrl_o.strb;
      wdp_req_dcu_ctrl_i.valid	= dc_wr_req_ctrl_o.valid;
      // Output write request: descriptor + data i/f.
      // to byte addressable memory.
      wdp_req_mem_ctrl_o.ready	= wr_req_ready_i;
   end : WDP_IP_ASSIGN
   
   wr_data_path 
     dcs_wr_data_path 
       (
	.clk			(clk),
	.reset			(reset),
	// ECI packet for response with data 
	// (VC 4 or 5). (header + data) from CPU. 
	.rsp_wd_pkt_i		(wdp_rsp_wd_pkt_i.pkt),
	.rsp_wd_pkt_size_i	(wdp_rsp_wd_pkt_i.size),
	.rsp_wd_pkt_vc_i	(wdp_rsp_wd_pkt_i.vc),
	.rsp_wd_pkt_valid_i	(wdp_rsp_wd_pkt_i.valid),
	.rsp_wd_pkt_ready_o	(wdp_rsp_wd_pkt_i.ready),
	// Output ECI header only for response with data
	// to be routed to the DCUs.
	// DCU_IDX is for routing the ECI header, size, VC
	// to appropriate DCU.
	.rsp_wd_dcu_idx_o	(), // Not connected, routed internally
	.rsp_wd_hdr_o		(wdp_rsp_wd_hdr_o.hdr),
	.rsp_wd_pkt_size_o	(wdp_rsp_wd_hdr_o.size),
	.rsp_wd_pkt_vc_o	(wdp_rsp_wd_hdr_o.vc),
	.rsp_wd_pkt_valid_o	(wdp_rsp_wd_hdr_o.valid),
	.rsp_wd_pkt_ready_i	(wdp_rsp_wd_hdr_o.ready),
	// Input Write descriptors: Request from DCUs. 
	// no data, only descriptor.
	.wr_req_id_i		(wdp_req_dcu_ctrl_i.id),
	.wr_req_addr_i		(wdp_req_dcu_ctrl_i.addr),
	.wr_req_strb_i		(wdp_req_dcu_ctrl_i.strb),
	.wr_req_valid_i		(wdp_req_dcu_ctrl_i.valid),
	.wr_req_ready_o		(wdp_req_dcu_ctrl_i.ready),
	// Output write request: descriptor + data i/f.
	// to byte addressable memory.
	.wr_req_id_o		(wdp_req_mem_ctrl_o.id),
	.wr_req_addr_o		(wdp_req_mem_ctrl_o.addr),
	.wr_req_strb_o		(wdp_req_mem_ctrl_o.strb),
	.wr_req_data_o		(wdp_req_mem_data_o),
	.wr_req_valid_o		(wdp_req_mem_ctrl_o.valid),
	.wr_req_ready_i		(wdp_req_mem_ctrl_o.ready)
	);

endmodule // dcs


`endif


