#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
      $XILINX_VIVADO/data/verilog/src/glbl.v \
      ../../../eci_dcs_defs/rtl/eci_dcs_defs.sv \
      ../rtl/eci_cmd_defs.sv \
      ../rtl/eci_dirc_defs.sv \
      ../rtl/eci_cc_defs.sv \
      ../testbench/dcuTb.sv \
      ../testbench/word_addr_mem.sv \
      ../rtl/wr_trmgr.sv \
      ../rtl/dcu_tsu.sv \
      ../rtl/rd_trmgr.sv \
      ../rtl/eci_trmgr.sv \
      ../rtl/gen_out_header.sv \
      ../rtl/decode_eci_req.sv \
      ../rtl/eci_cc_table.sv \
      ../rtl/dcu.sv \
      ../rtl/tag_state_ram.sv \
      ../rtl/dcu_controller.sv \
      ../rtl/ram_tdp.sv 

# if you want debug messages, uncomment
# `define DEBUG 1 in the test bench file. 
xelab -debug typical -incremental -L xpm worklib.dcuTb worklib.glbl -s worklib.dcuTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.dcuTb 
xsim -R worklib.dcuTb
