/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-02-02
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */


`ifndef MAP_ECID_TO_WRD_SV
 `define MAP_ECID_TO_WRD_SV

/*
 This module maps payload from ECI packet to downstream write interface.

 CL address is 40 bits (33 bits CL index + 7 byte offset). 
 SCL address is 35 bits (35 bits SCL index + 5 bytes offset).
 
 Given a CL index, it can map to 4 SCL indices. 
 SCL_idx
 SCL_idx + 1
 SCL_idx + 2
 SCL_idx + 3
 
 Each ECI packet with data can be further subdivided into 4 slots, each slot can carry a sub cache line.
 the dmask bits in the header of ECI packet determines the size of the ECI packet. 
 Example:
   dmask 0101, 0110 - ECI packet contains 1 header + 2 slots of data.
   dmask 1011, 1110 - ECI packet contains 1 header + 3 slots of data. 
   dmask 1111       - ECI packet contains 1 header + 4 slots of data. 
 
 Dmask also determines the SCL index of the data that is contained in the slots. 
 Example:
   dmask 0101 - eci_slot 0 contains data for SCL_idx, 
                eci_slot 1 contains data for SCL_idx + 2.
 
   dmask 0110 - eci_slot 0 contains data for SCL_idx + 1, 
                eci_slot 1 contains data for SCL_idx + 2
 
   dmask 1011 - eci_slot 0 contains data for SCL_idx,
                eci_slot 1 contains data for SCL_idx + 1,
                eci_slot 2 contains data for SCL_idx + 3

   dmask 1110 - eci_slot 0 contains data for SCL_idx + 1,
                eci_slot 1 contains data for SCL_idx + 2,
                eci_slot 2 contains data for SCL_idx + 3
 
   dmask 1111 - eci_slot 0 contains data for SCL_idx,
    	        eci_slot 1 contains data for SCL_idx + 1,
 	        eci_slot 2 contains data for SCL_idx + 2,
                eci_slot 3 contains data for SCL_idx + 3.

 This module maps the data from eci slot to the write slots that correspond to each SCL idx.
 The output write data is also split into 4 slot,
  
 wr_slot_o[0] - The write data for SCL_idx.
 wr_slot_o[1] - The write data for SCL_idx + 1
 wr_slot_o[2] - The write data for SCL_idx + 2
 wr_slot_o[3] - The write data for SCL_idx + 3.
 
 
 Map
 * X -> dont care + byte strobe = '0;
 * Not X -> byte strobe is all 1 for all bytes in slot. 

 dmask write_slot_o[3] write_slot_o[2] write_slot_o[1] write_slot_o[0]
         SCL_idx + 3     SCL_idx + 2     SCL_idx + 1     SCL_idx + 0
 
 0000     X                  X                X              X    
 0001     X                  X                X            eci_slot[0]
 0010     X                  X            eci_slot[0]        X
 0011     X                  X            eci_slot[1]      eci_slot[0]
 0100     X               eci_slot[0]         X              X
 0101     X               eci_slot[1]         X            eci_slot[0]
 0110     X               eci_slot[1]     eci_slot[0]        X
 0111     X               eci_slot[2]     eci_slot[1]      eci_slot[0] 
 1000   eci_slot[0]          X                X              X
 1001   eci_slot[1]          X                X            eci_slot[0]
 1010   eci_slot[1]          X            eci_slot[0]        X
 1011   eci_slot[2]          X            eci_slot[1]      eci_slot[0]
 1100   eci_slot[1]       eci_slot[0]         X              X
 1101   eci_slot[2]       eci_slot[1]         X            eci_slot[0]
 1110   eci_slot[2]       eci_slot[1]     eci_slot[0]        X
 1111   eci_slot[3]       eci_slot[2]     eci_slot[1]      eci_slot[0] 
 
 */

module map_ecid_to_wrd #
  (
   // do not change dmask_width. 
   parameter DMASK_WIDTH = 4,
   // 4 slots in a CL. 
   parameter NUM_SLOTS = DMASK_WIDTH,
   // same as width of SCL. 
   parameter SLOT_WIDTH = 256,
   // 32 bits per slot. 
   parameter BYTE_STRB_WIDTH = SLOT_WIDTH/8
   )
   (
    // Input data from ECI packet. 
    input logic [NUM_SLOTS-1:0][SLOT_WIDTH-1:0]       eci_slot_i,
    input logic [DMASK_WIDTH-1:0] 		      dmask_i,
    
    // output write data + strobe that can be sent to AXI channel.
    output logic [NUM_SLOTS-1:0][SLOT_WIDTH-1:0]      wr_slot_o,
    output logic [NUM_SLOTS-1:0][BYTE_STRB_WIDTH-1:0] wrd_byte_strobe_o
    );


   always_comb begin : DMASK_MAPPER
      // default
      wr_slot_o[0] = eci_slot_i[0];
      wr_slot_o[1] = eci_slot_i[1];
      wr_slot_o[2] = eci_slot_i[2];
      wr_slot_o[3] = eci_slot_i[3];

      set_wr_byte_strobe(
			 .wr_slot_0(dmask_i[0]),
			 .wr_slot_1(dmask_i[1]),
			 .wr_slot_2(dmask_i[2]),
			 .wr_slot_3(dmask_i[3])
			 );
      
      // wr_slot_o[0] is always eci_slot_i[0].
      
      // wr_slot_o[1]
      if(dmask_i[1] == 1 && dmask_i[0] == 0) begin
	 wr_slot_o[1] = eci_slot_i[0];
      end

      // wr_slot_o[2], wr_slot_o[3].
      case(dmask_i)
	4'b0100: begin
	   wr_slot_o[2] = eci_slot_i[0];
	end
	4'b0101, 4'b0110: begin
	   wr_slot_o[2] = eci_slot_i[1];
	end
	4'b1000: begin
	   wr_slot_o[3] = eci_slot_i[0];
	end
	4'b1001, 4'b1010: begin
	   wr_slot_o[3] = eci_slot_i[1];
	end
	4'b1011: begin
	   wr_slot_o[3] = eci_slot_i[2];
	end
	4'b1100: begin
	   wr_slot_o[2] = eci_slot_i[0];
	   wr_slot_o[3] = eci_slot_i[1];
	end
	4'b1101: begin
	   wr_slot_o[2] = eci_slot_i[1];
	   wr_slot_o[3] = eci_slot_i[2];
	end
	4'b1110: begin
	   wr_slot_o[2] = eci_slot_i[1];
	   wr_slot_o[3] = eci_slot_i[2];
	end
	default: begin
	   wr_slot_o[2] = eci_slot_i[2];
	   wr_slot_o[3] = eci_slot_i[3];
	end
      endcase // case (dmask_i)

   end : DMASK_MAPPER

   task static set_wr_byte_strobe(
				  input logic wr_slot_0,
				  input logic wr_slot_1, 
				  input logic wr_slot_2, 
				  input logic wr_slot_3 
				  );
      // assign byte strobe values for each write slot.
      // all bytes within a write slot will have the same strobe value. 
      wrd_byte_strobe_o[0] = {BYTE_STRB_WIDTH{wr_slot_0}};
      wrd_byte_strobe_o[1] = {BYTE_STRB_WIDTH{wr_slot_1}};
      wrd_byte_strobe_o[2] = {BYTE_STRB_WIDTH{wr_slot_2}};
      wrd_byte_strobe_o[3] = {BYTE_STRB_WIDTH{wr_slot_3}};
   endtask //set_wr_byte_strobe
    
endmodule // map_ecid_to_wrd

`endif
