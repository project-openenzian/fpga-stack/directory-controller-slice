#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import csv
import pandas as pd 

total_xs = 0
total_num_states = 0
total_num_events = 0
state_xs = {}
event_xs = {}
x_matrix = []

with open('ECI_CC.csv', newline='') as csvfile:
    cc_rom = csv.reader(csvfile, delimiter=',')

    #ignore header
    next(cc_rom)
    
    for line in cc_rom:
        total_num_states = total_num_states + 1
        state = line[0].replace(" ","")
        c_st_num_x = 0
        x_list = []
        for i in range(1, len(line)):
            e = line[i].replace(" ","")
            if(e == 'X'):
                x_list.append(1)
                total_xs = total_xs + 1
                c_st_num_x = c_st_num_x + 1
            elif (e == "Stall"):
                x_list.append(2)
            else:
                x_list.append(0)
        x_matrix.append(x_list)
        state_xs[state] = c_st_num_x

with open('ECI_CC.csv', newline='') as csvfile:
    cc_rom_2 = csv.DictReader(csvfile, delimiter=",")
    for line in cc_rom_2:
        for key,val in line.items():
            k = key.replace(" ","")
            v = val.replace(" ","")
            if (k == "CurrentStateHS:RS"):
                continue
            if(k not in event_xs):
                event_xs[k] = 0
            if(v == "X"):
                event_xs[k] = event_xs[k] + 1
                    

    
total_num_events = len(event_xs.keys())
for k,v in event_xs.items():
    event_xs[k] = event_xs[k]/total_num_states * 100

for k,v in state_xs.items():
    state_xs[k] = state_xs[k]/total_num_events * 100 

#print(total_xs)
#print(total_num_states)
#print(state_xs)
#print(event_xs)

#plt.bar(state_xs.keys(), state_xs.values(), 0.5, color='g')
#plt.xticks(rotation=90)
#plt.show()

#pd.Series(state_xs).sort_values(ascending=False).plot.bar()
#plt.ylabel("Percentage of Xs (%)")
#plt.xlabel("States")
#plt.show()

#pd.Series(event_xs).sort_values(ascending=False).plot.bar()
#plt.ylabel("Percentage of Xs (%)")
#plt.xlabel("Events")
#plt.show()



# plt.xlim(0, total_num_events)
# plt.ylim(0, total_num_states)
# plt.grid()
# for y in range (0, total_num_states):
#     for x in range(0, total_num_events):
#         if(x_matrix[y][x] == 1):
#             plt.plot(x, y, marker="x", color = "red")
#         elif(x_matrix[y][x] == 2):
#             plt.plot(x, y, marker=".", color = "blue")
#         else:
#             plt.plot(x, y, marker=".", color = "green")

# plt.ylabel("state enumerations")
# plt.xlabel("event enumerations")
# plt.title("Next state: Allowed(Green)/Not allowed(Red)/Stalled (Blue)")
# plt.show()

def get_a_na_s(str_val):
    str_val = str_val.replace(" " ,"");
    if(str_val == "X"):
        return 0
    else:
        return 1

    
cc_df = pd.read_csv("ECI_CC.csv", index_col = 0)
#print(evt_list)
cc_df =  cc_df.applymap(get_a_na_s)
cc_df["num_allwd_stt"] = cc_df.sum(axis=1)
cc_df.loc["num_allwd_evt"] = cc_df.sum()
print(cc_df.tail())

# Sort states based on num_allwd_stt
s_cc_df = cc_df.sort_values("num_allwd_stt")
s_cc_df = s_cc_df.sort_values("num_allwd_evt", axis=1)
row_idx = cc_df.index
stt_list = list(row_idx)
stt_list.remove("num_allwd_evt")
evt_list = list(s_cc_df.columns)
evt_list.remove("num_allwd_stt")
print(s_cc_df)
print(stt_list)

plt.matshow(s_cc_df.loc[stt_list][evt_list])
plt.ylabel("state enum (sorted ascending)")
plt.xlabel("event enum (sorted ascending)")
plt.title("valid next state (yellow) vs Not allowed(purple)")
plt.show()
