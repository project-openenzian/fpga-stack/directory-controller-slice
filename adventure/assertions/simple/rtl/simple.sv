`ifndef SIMPLE_SV
`define SIMPLE_SV

module simple #
  (
   parameter DCU_ID=5
   )
   (
    input logic        clk,
    input logic        reset,
    input logic        wr_en_i,
    input logic [4:0]  dcu_id_i,
    output logic [4:0] dcu_id_o
    );

   logic [4:0] dcu_id_reg = '0, dcu_id_next;
   assign dcu_id_o	= dcu_id_reg;
   always_comb begin : CONTROLLER
      dcu_id_next	= dcu_id_reg;
      if(wr_en_i) begin
	 dcu_id_next = dcu_id_i;
      end
   end : CONTROLLER

   always_ff @(posedge clk) begin : REG_ASSIGN
      dcu_id_reg <= dcu_id_next;

      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset 
	 dcu_id_reg	<= '0;

      end
   end : REG_ASSIGN

   
   // always_ff @(posedge clk) begin : ASSERTIONS
   //    if(wr_en_i) begin
   // 	 assert(dcu_id_i === DCU_ID) else 
   // 	   $fatal(1,"DCU_ID does not match");
   //    end
   // end : ASSERTIONS

`include "simple_assertions.svh"
   
endmodule // simple

`endif
