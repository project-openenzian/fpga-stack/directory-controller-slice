#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
      $XILINX_VIVADO/data/verilog/src/glbl.v \
      ../rtl/eci_cmd_defs.sv \
      ../testbench/vc_hilo_routerTb.sv \
      ../rtl/vc_hilo_router.sv \
      ../rtl/axis_pipeline_stage.sv 

xelab -debug typical -incremental -L xpm worklib.vc_hilo_routerTb worklib.glbl -s worklib.vc_hilo_routerTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.vc_hilo_routerTb 
xsim -R worklib.vc_hilo_routerTb 
