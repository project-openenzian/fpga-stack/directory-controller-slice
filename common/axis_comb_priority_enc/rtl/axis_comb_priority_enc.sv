/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-08-04
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */

`ifndef AXIS_COMB_PRIORITY_ENC_SV
`define AXIS_COMB_PRIORITY_ENC_SV

/*
 * Module Description:
 *  N to 1 Priority Mux. 
 *  N input channels with ch 0 having highest priority 
 *  and ch N-1 with lowest priority. 
 *  More than 1 input channel can be valid at a time, 
 *  output is chosen based on priority. 
 *  Fully combinational.
 *
 * Input Output Description:
 *  N input channels with data, valid ready flow control. 
 *  1 output channel with data, valid ready flow control. 
 *  
 *
 * Architecture Description:
 *  Generate select signal based on priority. 
 *  Assign downstream data based on select signal.
 *
 * Modifiable Parameters:
 *  ID_WIDTH - width of ID sent along with data. 
 *  DATA_WIDTH - width of data in input and output channels. 
 *  NUM_IN - Number of input channels. 
 *
 * Non-modifiable Parameters:
 *  None.
 * 
 * Modules Used:
 *  None.
 *
 * Notes:
 *  Input channels wait on valid before asserting ready. 
 *  Output channels do not wait on ready before asserting valid. 
 *
 */

module axis_comb_priority_enc #
  (
   parameter ID_WIDTH = 5,
   parameter DATA_WIDTH = 64,
   parameter NUM_IN = 4
   )
   (
    input logic [NUM_IN-1:0][ID_WIDTH-1:0]   us_id_i,
    input logic [NUM_IN-1:0][DATA_WIDTH-1:0] us_data_i,
    input logic [NUM_IN-1:0] 		     us_valid_i,
    output logic [NUM_IN-1:0] 		     us_ready_o,

    output logic [ID_WIDTH-1:0] 	     ds_id_o,
    output logic [DATA_WIDTH-1:0] 	     ds_data_o,
    output logic 			     ds_valid_o,
    input logic 			     ds_ready_i
    );
   localparam SEL_WIDTH = $clog2(NUM_IN);

   logic [SEL_WIDTH-1:0] 		     p_sel;

   // assign downstream data based on priority select signal.
   // upstream ready is sent only to selected channel, 
   always_comb begin : OUT_ASSIGN
      ds_id_o = us_id_i[p_sel];
      ds_data_o = us_data_i[p_sel];
      ds_valid_o = us_valid_i[p_sel];
      us_ready_o = '0;
      us_ready_o[p_sel] = ds_ready_i;
   end : OUT_ASSIGN

   // Generate select signal based on priority
   // assigned to output channels.
   // Ch 0 highest priority.
   // Ch N-1 lowest priority. 
   always_comb begin : GET_SEL_VAL
      p_sel = '0;
      for( integer i=NUM_IN-1; i>=0; i=i-1 ) begin
	 if(us_valid_i[i] == 1'b1) begin
	    p_sel = SEL_WIDTH'(i);
	 end
      end 
   end : GET_SEL_VAL
endmodule // axis_comb_priority_enc

`endif
