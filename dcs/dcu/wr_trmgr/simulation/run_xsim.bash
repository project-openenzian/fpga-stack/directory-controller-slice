#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
$XILINX_VIVADO/data/verilog/src/glbl.v \
../testbench/wr_trmgrTb.sv \
../rtl/wr_trmgr.sv 

xelab -debug typical -incremental -L xpm worklib.wr_trmgrTb worklib.glbl -s worklib.wr_trmgrTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.wr_trmgrTb 
xsim -R worklib.wr_trmgrTb 
