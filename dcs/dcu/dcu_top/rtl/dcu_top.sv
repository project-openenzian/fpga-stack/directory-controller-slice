/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-08-15
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */

`ifndef DCU_TOP_SV
`define DCU_TOP_SV

//
//
//                                      ':+o:'
//                                     :osssss/'
//                                    +ssssssss+'
//                                   -ssssssssss/
//                                   /ssssssssss+
//                         '...''    :ssssssssss/     ''''
//                     .:+ssssssso/- 'osss//ssso' -/osssssso+:'
//                    :ssssssssssssso:'+ss. os+':osssssssssssss-
//                    'ossssssssssssss+'.:  ..'+sssssssssssssso'
//                     '/ssssssssso.''.' ://: '..'.osssssssss+'
//                       ./osssssssso/' /ssss/ '/ssssssssss+.
//                          .-:///:-.'' '/oo/' ''.://///:.
//                               .:+oso'    '  -so+:.
//                             -osssso' :o 'oo:'-ssss+-
//                           '+sssssss+oss''sssosssssss/
//                           /sssssssssss+  osssssssssss:
//                          'ssssssssssso'  .osssssssssso
//                          'ssssssssss/'    '+ssssssssso
//                           /ssssss+:'        ./ossssss:
//                            ---..               '.----
//
//
// '''''''       ''               ''''''''       '           ''           '       '
// o+:::::'      :o-     //       :::::+s:      's.          +o'         .s:     -o
// o-            :o+/'   //           -o.       's.         /+/+         .s/+.   -o
// o/----.       :+ -o-  //          /+'        's.        :o' +:        .s'.+:  -o
// o:....'       :+  '//'//        '+:          's.       .o.  'o-       .s'  :+.-o
// o-            :+    -oo/       -o.           's.      'o+/////o.      .s'   .++o
// ++/////-      :/     '//      -o+//////'     'o.      /:      :+      .o      :o

import eci_cmd_defs::*;  // ECI specific defs.
import eci_dcs_defs::*;  // DCS defs at a higher level than DCU. 
import eci_dirc_defs::*; // DCU specific defs. 
import eci_cc_defs::*;   // CC specific defs to support dirc debug interface.

// Module that instantiates a DCU and arbitrates between
// incoming VC channels to choose one VC to send to DCU.
//
// Figure below shows architecture.
//
// arb_4_ecih is the rotating priority arbiter.
// it changes priority with each handshake.
// if the VC with priority does not have any data in current
// cycle, a VC that has data will be chosen so cycles are not
// lost.
// It also has a stall feature, where if the DCU cannot handle
// a VC in this cycle, the arbiter switches priority without
// sending a ready to the currently selected VC. 
//
// Pipeline registers are added to all incoming and outgoing
// channels (except debug and performance register channels).
// These pipeline registers have registered valid and ready
// signals and so provide isolation for the DCU_TOP unit.
//
// DCU top does not accept any data, only headers. All
// data goes through data channel.

//                   Pipeline Register
//                   ▲
//                   │
//                   │
//                   │
//
//     req_wod_hdr   │        ┌─────┐                   DCU_TOP
//            ───────┼───────►│     │
//                   │        │     │      ┌────────────────────────────────────┐
//                            │     │      │                                    │
//                            │     │      │                                    │       │ rd_req_desc
//     rsp_wod_hdr   │        │     │      │                                    ├───────┼───────►
//            ───────┼───────►│     │      │                                    │       │
//                   │        │     │      │                                    │
//                            │     │      │                                    │       │  rd_rsp_desc(no_data)
//                            │     │      │                                    │◄──────┼────────
//     rsp_wd_hdr    │        │     ├─────►│                DCU                 │       │
//   (no data)───────┼───────►│     │      │                                    │
//                   │        │     │◄─────│ Stall                              │
//                            │     │      │                                    │       │ wr_req_desc(no_data)
// lcl_mfwd_wod_hdr  │        │     │      │                                    ├───────┼───────►
//            ───────┼───────►│     │      │                                    │       │
//                   │        │     │      │                                    │
//                            └─────┘      │                                    │       │  wr_rsp_desc
//                     Rotating Priority   │                                    │◄──────┼────────
//                     Arbiter             └──────────────────┬────┬─────┬──────┘       │
//                                                            │    │     │
//                   │                                        │    │     │
//            ◄──────┼────────────────────────────────────────┘    │     │
//                   │ eci_rsp _hdr + VC# (no data)                │     │
//                                                                 │     │
//                                                                 ▼     ▼
//                                                      Debug Signals    Performance
//                                                      (not registered) Registers

module dcu_top #
  (
   parameter PERF_REGS_WIDTH = 64,
   parameter SYNTH_PERF_REGS = 1   // 0, 1
   )
   (
    input logic 				 clk,
    input logic 				 reset,
    // Input ECI events.
    // ECI packet for request without data. (VC 6 or 7) (only header).
    input logic [ECI_WORD_WIDTH-1:0] 		 req_wod_hdr_i,
    input logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 req_wod_pkt_size_i,
    input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0]  req_wod_pkt_vc_i,
    input logic 				 req_wod_pkt_valid_i,
    output logic 				 req_wod_pkt_ready_o,

    // ECI packet for response without data.(VC 10 or 11). (only header).
    input logic [ECI_WORD_WIDTH-1:0] 		 rsp_wod_hdr_i,
    input logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 rsp_wod_pkt_size_i,
    input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0]  rsp_wod_pkt_vc_i,
    input logic 				 rsp_wod_pkt_valid_i,
    output logic 				 rsp_wod_pkt_ready_o,

    // ECI packet for response with data. (VC 4 or 5). (only header).
    input logic [ECI_WORD_WIDTH-1:0] 		 rsp_wd_hdr_i,
    input logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 rsp_wd_pkt_size_i,
    input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0]  rsp_wd_pkt_vc_i,
    input logic 				 rsp_wd_pkt_valid_i,
    output logic 				 rsp_wd_pkt_ready_o,

    // LCL packet for clean, clean invalidates. (VC 16 or 17). (only header).
    input logic [ECI_WORD_WIDTH-1:0] 		 lcl_fwd_wod_hdr_i,
    input logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 lcl_fwd_wod_pkt_size_i,
    input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0]  lcl_fwd_wod_pkt_vc_i,
    input logic 				 lcl_fwd_wod_pkt_valid_i,
    output logic 				 lcl_fwd_wod_pkt_ready_o,

    // LCL packet for mem resp (unlock). (VC 18 or 19).
    input logic [ECI_WORD_WIDTH-1:0] 		 lcl_rsp_wod_hdr_i,
    input logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 lcl_rsp_wod_pkt_size_i,
    input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0]  lcl_rsp_wod_pkt_vc_i,
    input logic 				 lcl_rsp_wod_pkt_valid_i,
    output logic 				 lcl_rsp_wod_pkt_ready_o,

    // Output ECI events, only header.
    // VC # indicates VC to route to.
    output logic [ECI_WORD_WIDTH-1:0] 		 eci_rsp_hdr_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 eci_rsp_size_o,
    output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] eci_rsp_vc_o,
    output logic 				 eci_rsp_valid_o,
    input logic 				 eci_rsp_ready_i,

    // Output Read descriptors
    // Read descriptors: Request and response.
    // no data, only descriptor.
    output logic [MAX_DCU_ID_WIDTH-1:0] 	 rd_req_id_o,
    output logic [DS_ADDR_WIDTH-1:0] 		 rd_req_addr_o,
    output logic 				 rd_req_valid_o,
    input logic 				 rd_req_ready_i,

    input logic [MAX_DCU_ID_WIDTH-1:0] 		 rd_rsp_id_i,
    input logic 				 rd_rsp_valid_i,
    output logic 				 rd_rsp_ready_o,

    // Write descriptors: Request and response.
    // no data, only descriptor.
    output logic [MAX_DCU_ID_WIDTH-1:0] 	 wr_req_id_o,
    output logic [DS_ADDR_WIDTH-1:0] 		 wr_req_addr_o,
    output logic [ECI_CL_SIZE_BYTES-1:0] 	 wr_req_strb_o,
    output logic 				 wr_req_valid_o,
    input logic 				 wr_req_ready_i,

    input logic [MAX_DCU_ID_WIDTH-1:0] 		 wr_rsp_id_i,
    input logic [1:0] 				 wr_rsp_bresp_i,
    input logic 				 wr_rsp_valid_i,
    output logic 				 wr_rsp_ready_o,

    // Performance Counters.
    output logic [PERF_REGS_WIDTH-1:0] 		 prf_no_req_pkt_rdy_o,
    output logic [PERF_REGS_WIDTH-1:0] 		 prf_no_req_pkt_stl_o,
    output logic [PERF_REGS_WIDTH-1:0] 		 prf_no_rsp_pkt_sent_o,
    output logic [PERF_REGS_WIDTH-1:0] 		 prf_no_rd_req_o,
    output logic [PERF_REGS_WIDTH-1:0] 		 prf_no_rd_rsp_o,
    output logic [PERF_REGS_WIDTH-1:0] 		 prf_no_wr_req_o,
    output logic [PERF_REGS_WIDTH-1:0] 		 prf_no_wr_rsp_o,
    output logic [PERF_REGS_WIDTH-1:0] 		 prf_no_cyc_bw_req_vld_rdy_o,

    // Debug interface.
    output logic [ECI_WORD_WIDTH-1:0] 		 dbg_eci_req_hdr_o,
    output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] dbg_eci_req_vc_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 dbg_eci_req_size_o,
    output logic 				 dbg_eci_req_ready_o,
    output logic 				 dbg_eci_req_stalled_o,
    output logic [ECI_WORD_WIDTH-1:0] 		 dbg_eci_rsp_hdr_o,
    output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] dbg_eci_rsp_vc_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 dbg_eci_rsp_size_o,
    output logic 				 dbg_eci_rsp_valid_o,
    output 					 cc_req_t dbg_req_cc_enc_o,
    output logic 				 dbg_rdda_o,
    output logic 				 dbg_wdda_o,
    output logic 				 dbg_stall_prot_o,
    output logic 				 dbg_stall_rd_busy_o,
    output logic 				 dbg_stall_wr_busy_o,
    output logic 				 dbg_stall_eci_tr_busy_o,
    output logic 				 dbg_stall_rtg_full_o,
    output logic 				 dbg_stall_wait_op_hs_o,
    output 					 cc_req_t dbg_cc_active_req_o,
    output 					 cc_state_t dbg_cc_present_state_o,
    output 					 cc_state_t dbg_cc_next_state_o,
    output 					 cc_action_t dbg_cc_next_action_o,
    output 					 dirc_err_t dbg_err_code_o,
    output logic 				 dbg_error_o,
    output logic 				 dbg_valid_o
    );

   localparam PIPE_ECI_HDR_WIDTH = ECI_WORD_WIDTH+ECI_PACKET_SIZE_WIDTH+ECI_LCL_TOT_NUM_VCS_WIDTH;

   typedef struct packed {
      logic [ECI_WORD_WIDTH-1:0]        hdr;
      logic [ECI_PACKET_SIZE_WIDTH-1:0] size;
      logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] vc;
      logic				valid;
      logic				ready;
      logic				stall;
   } eci_hdr_if_t;

   eci_hdr_if_t req_wod_p_i, req_wod_p_o;
   eci_hdr_if_t rsp_wod_p_i, rsp_wod_p_o;
   eci_hdr_if_t rsp_wd_p_i, rsp_wd_p_o;
   eci_hdr_if_t lcl_fwd_wod_p_i, lcl_fwd_wod_p_o;
   eci_hdr_if_t lcl_rsp_wod_p_i, lcl_rsp_wod_p_o;
   

   eci_hdr_if_t arb_req_wod_i, arb_rsp_wod_i, arb_rsp_wd_i;
   eci_hdr_if_t arb_lcl_fwd_wod_i, arb_lcl_rsp_wod_i;
   eci_hdr_if_t arb_eci_evt;
   eci_hdr_if_t dc_i, dc_o;

   // DCU signals
   // Read descriptors: request, response.
   logic [MAX_DCU_ID_WIDTH-1:0] 	dc_rd_req_id_o;
   logic [DS_ADDR_WIDTH-1:0] 		dc_rd_req_addr_o;
   logic				dc_rd_req_valid_o;
   logic				dc_rd_req_ready_i;
   logic [MAX_DCU_ID_WIDTH-1:0] 	dc_rd_rsp_id_i;
   logic				dc_rd_rsp_valid_i;
   logic				dc_rd_rsp_ready_o;
   // Write descriptors: request, response.
   logic [MAX_DCU_ID_WIDTH-1:0] 	dc_wr_req_id_o;
   logic [DS_ADDR_WIDTH-1:0] 		dc_wr_req_addr_o;
   logic [ECI_CL_SIZE_BYTES-1:0] 	dc_wr_req_strb_o;
   logic				dc_wr_req_valid_o;
   logic				dc_wr_req_ready_i;
   logic [MAX_DCU_ID_WIDTH-1:0] 	dc_wr_rsp_id_i;
   logic [1:0] 				dc_wr_rsp_bresp_i;
   logic				dc_wr_rsp_valid_i;
   logic				dc_wr_rsp_ready_o;
   // Performance counters.
   logic [PERF_REGS_WIDTH-1:0] 		dc_prf_no_req_pkt_rdy_o;
   logic [PERF_REGS_WIDTH-1:0] 		dc_prf_no_req_pkt_stl_o;
   logic [PERF_REGS_WIDTH-1:0] 		dc_prf_no_rsp_pkt_sent_o;
   logic [PERF_REGS_WIDTH-1:0] 		dc_prf_no_rd_req_o;
   logic [PERF_REGS_WIDTH-1:0] 		dc_prf_no_rd_rsp_o;
   logic [PERF_REGS_WIDTH-1:0] 		dc_prf_no_wr_req_o;
   logic [PERF_REGS_WIDTH-1:0] 		dc_prf_no_wr_rsp_o;
   logic [PERF_REGS_WIDTH-1:0] 		dc_prf_no_cyc_bw_req_vld_rdy_o;

   // Debug interface
   logic [ECI_WORD_WIDTH-1:0] 		dc_dbg_eci_req_hdr_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	dc_dbg_eci_req_vc_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	dc_dbg_eci_req_size_o;
   logic				dc_dbg_eci_req_ready_o;
   logic				dc_dbg_eci_req_stalled_o;
   logic [ECI_WORD_WIDTH-1:0] 		dc_dbg_eci_rsp_hdr_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	dc_dbg_eci_rsp_vc_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	dc_dbg_eci_rsp_size_o;
   logic				dc_dbg_eci_rsp_valid_o;
   logic 				dc_dbg_stall_prot_o;
   logic 				dc_dbg_stall_rd_busy_o;
   logic 				dc_dbg_stall_wr_busy_o;
   logic 				dc_dbg_stall_eci_tr_busy_o;
   logic 				dc_dbg_stall_rtg_full_o;
   logic 				dc_dbg_stall_wait_op_hs_o;
   cc_req_t                             dc_dbg_req_cc_enc_o;
   logic				dc_dbg_rdda_o;
   logic				dc_dbg_wdda_o;
   cc_req_t                             dc_dbg_cc_active_req_o;
   cc_state_t                           dc_dbg_cc_present_state_o;
   cc_state_t                           dc_dbg_cc_next_state_o;
   cc_action_t                          dc_dbg_cc_next_action_o;
   dirc_err_t                           dc_dbg_err_code_o;
   logic				dc_dbg_error_o;
   logic				dc_dbg_valid_o;

   // Pipeline signals.
   // Input of pipeline stage of rd response desc
   logic [MAX_DCU_ID_WIDTH-1:0] 	rd_rsp_id_p_i;
   logic				rd_rsp_valid_p_i;
   logic				rd_rsp_ready_p_o;
   // Output of pipeline stage of rd response desc.
   logic [MAX_DCU_ID_WIDTH-1:0] 	rd_rsp_id_p_o;
   logic				rd_rsp_valid_p_o;
   logic				rd_rsp_ready_p_i;
   // Input of pipeline stage of wr response desc
   logic [MAX_DCU_ID_WIDTH-1:0] 	wr_rsp_id_p_i;
   logic [1:0] 				wr_rsp_bresp_p_i;
   logic				wr_rsp_valid_p_i;
   logic				wr_rsp_ready_p_o;
   // Output of pipeline stage of wr response desc
   logic [MAX_DCU_ID_WIDTH-1:0] 	wr_rsp_id_p_o;
   logic [1:0] 				wr_rsp_bresp_p_o;
   logic				wr_rsp_valid_p_o;
   logic				wr_rsp_ready_p_i;
   // Input of pipeline stage of rd request desc
   logic [MAX_DCU_ID_WIDTH-1:0] 	rd_req_id_p_i;
   logic [DS_ADDR_WIDTH-1:0] 		rd_req_addr_p_i;
   logic				rd_req_valid_p_i;
   logic				rd_req_ready_p_o;
   // Output of pipeline stage of rd request desc
   logic [MAX_DCU_ID_WIDTH-1:0] 	rd_req_id_p_o;
   logic [DS_ADDR_WIDTH-1:0] 		rd_req_addr_p_o;
   logic				rd_req_valid_p_o;
   logic				rd_req_ready_p_i;
   // Input of pipeline stage of wr request desc
   logic [MAX_DCU_ID_WIDTH-1:0] 	wr_req_id_p_i;
   logic [DS_ADDR_WIDTH-1:0] 		wr_req_addr_p_i;
   logic [ECI_CL_SIZE_BYTES-1:0] 	wr_req_strb_p_i;
   logic				wr_req_valid_p_i;
   logic				wr_req_ready_p_o;
   // Output of pipeline stage of wr request desc
   logic [MAX_DCU_ID_WIDTH-1:0] 	wr_req_id_p_o;
   logic [DS_ADDR_WIDTH-1:0] 		wr_req_addr_p_o;
   logic [ECI_CL_SIZE_BYTES-1:0] 	wr_req_strb_p_o;
   logic				wr_req_valid_p_o;
   logic				wr_req_ready_p_i;
   // Input of pipeline stage of eci response
   logic [ECI_WORD_WIDTH-1:0] 		eci_rsp_hdr_p_i;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	eci_rsp_size_p_i;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	eci_rsp_vc_p_i;
   logic				eci_rsp_valid_p_i;
   logic				eci_rsp_ready_p_o;
   // Output of pipeline stage of eci response
   logic [ECI_WORD_WIDTH-1:0] 		eci_rsp_hdr_p_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	eci_rsp_size_p_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	eci_rsp_vc_p_o;
   logic				eci_rsp_valid_p_o;
   logic				eci_rsp_ready_p_i;

   // Assign Outputs.
   // Output Performance registers.
   always_comb begin : PERF_REG_OP_ASSIGN
      // All signals come from DCU.
      prf_no_req_pkt_rdy_o  = dc_prf_no_req_pkt_rdy_o;
      prf_no_req_pkt_stl_o  = dc_prf_no_req_pkt_stl_o;
      prf_no_rsp_pkt_sent_o = dc_prf_no_rsp_pkt_sent_o;
      prf_no_rd_req_o       = dc_prf_no_rd_req_o;
      prf_no_rd_rsp_o       = dc_prf_no_rd_rsp_o;
      prf_no_wr_req_o       = dc_prf_no_wr_req_o;
      prf_no_wr_rsp_o       = dc_prf_no_wr_rsp_o;
      prf_no_cyc_bw_req_vld_rdy_o = dc_prf_no_cyc_bw_req_vld_rdy_o;
   end : PERF_REG_OP_ASSIGN

   // Output Debug I/F.
   always_comb begin : DEBUG_IF_OP_ASSIGN
      // All signals come from DCU.
      dbg_eci_req_hdr_o		= dc_dbg_eci_req_hdr_o;
      dbg_eci_req_vc_o		= dc_dbg_eci_req_vc_o;
      dbg_eci_req_size_o	= dc_dbg_eci_req_size_o;
      dbg_eci_req_ready_o	= dc_dbg_eci_req_ready_o;
      dbg_eci_req_stalled_o	= dc_dbg_eci_req_stalled_o;
      dbg_eci_rsp_hdr_o		= dc_dbg_eci_rsp_hdr_o;
      dbg_eci_rsp_vc_o		= dc_dbg_eci_rsp_vc_o;
      dbg_eci_rsp_size_o	= dc_dbg_eci_rsp_size_o;
      dbg_eci_rsp_valid_o	= dc_dbg_eci_rsp_valid_o;
      dbg_stall_prot_o          = dc_dbg_stall_prot_o;
      dbg_stall_rd_busy_o       = dc_dbg_stall_rd_busy_o;
      dbg_stall_wr_busy_o       = dc_dbg_stall_wr_busy_o;
      dbg_stall_eci_tr_busy_o   = dc_dbg_stall_eci_tr_busy_o;
      dbg_stall_rtg_full_o      = dc_dbg_stall_rtg_full_o;
      dbg_stall_wait_op_hs_o    = dc_dbg_stall_wait_op_hs_o;
      dbg_req_cc_enc_o		= dc_dbg_req_cc_enc_o;
      dbg_rdda_o		= dc_dbg_rdda_o;
      dbg_wdda_o		= dc_dbg_wdda_o;
      dbg_cc_active_req_o	= dc_dbg_cc_active_req_o;
      dbg_cc_present_state_o	= dc_dbg_cc_present_state_o;
      dbg_cc_next_state_o	= dc_dbg_cc_next_state_o;
      dbg_cc_next_action_o	= dc_dbg_cc_next_action_o;
      dbg_err_code_o		= dc_dbg_err_code_o;
      dbg_error_o		= dc_dbg_error_o;
      dbg_valid_o		= dc_dbg_valid_o;
   end : DEBUG_IF_OP_ASSIGN

   always_comb begin : OP_ASSIGN
      // ECI packet for request without data. (VC 6 or 7) (only header).
      req_wod_pkt_ready_o = req_wod_p_i.ready;
      // ECI packet for response without data.(VC 10 or 11). (only header).
      rsp_wod_pkt_ready_o = rsp_wod_p_i.ready;
      // ECI packet for response with data. (VC 4 or 5). (only header).
      rsp_wd_pkt_ready_o  = rsp_wd_p_i.ready;
      // LCL packet for forwards without data. (VC 16 or 17). (only header).
      lcl_fwd_wod_pkt_ready_o = lcl_fwd_wod_p_i.ready;
      // LCL packet for response without data. (VC 18 or 19).
      lcl_rsp_wod_pkt_ready_o = lcl_rsp_wod_p_i.ready;
      // Output ECI events, only header.
      eci_rsp_hdr_o       = eci_rsp_hdr_p_o;
      eci_rsp_size_o      = eci_rsp_size_p_o;
      eci_rsp_vc_o        = eci_rsp_vc_p_o;
      eci_rsp_valid_o     = eci_rsp_valid_p_o;
      // Read descriptors requests
      rd_req_id_o         = rd_req_id_p_o;
      rd_req_addr_o       = rd_req_addr_p_o;
      rd_req_valid_o      = rd_req_valid_p_o;
      // Read descriptors response
      rd_rsp_ready_o      = rd_rsp_ready_p_o;
      // Write descriptors: Request
      wr_req_id_o         = wr_req_id_p_o;
      wr_req_addr_o       = wr_req_addr_p_o;
      wr_req_strb_o       = wr_req_strb_p_o;
      wr_req_valid_o      = wr_req_valid_p_o;
      // Write descriptors: Response
      wr_rsp_ready_o      = wr_rsp_ready_p_o;
   end : OP_ASSIGN

   // Input pipeline stages - All input I/Fs has
   // an input pipeline stage.
   // Required to avoid head-of-line blocking.
   // req_wod input pipeline stage.
   always_comb begin : REQ_WOD_PIPE_IP_ASSIGN
      req_wod_p_i.hdr   = req_wod_hdr_i;
      req_wod_p_i.size  = req_wod_pkt_size_i;
      req_wod_p_i.vc    = req_wod_pkt_vc_i;
      req_wod_p_i.valid = req_wod_pkt_valid_i;
      req_wod_p_o.ready = arb_req_wod_i.ready;
   end : REQ_WOD_PIPE_IP_ASSIGN
   axis_pipeline_stage #
     (
      .DATA_WIDTH(PIPE_ECI_HDR_WIDTH)
       )
   req_wod_pipe_inst
     (
      .clk(clk),
      .reset(reset),
      .us_data({req_wod_p_i.hdr, req_wod_p_i.size, req_wod_p_i.vc}),
      .us_valid(req_wod_p_i.valid),
      .us_ready(req_wod_p_i.ready),
      .ds_data({req_wod_p_o.hdr, req_wod_p_o.size, req_wod_p_o.vc}),
      .ds_valid(req_wod_p_o.valid),
      .ds_ready(req_wod_p_o.ready)
      );

   // rsp_wod input pipeline stage.
   always_comb begin : RSP_WOD_PIPE_IP_ASSIGN
      rsp_wod_p_i.hdr   = rsp_wod_hdr_i;
      rsp_wod_p_i.size  = rsp_wod_pkt_size_i;
      rsp_wod_p_i.vc    = rsp_wod_pkt_vc_i;
      rsp_wod_p_i.valid = rsp_wod_pkt_valid_i;
      rsp_wod_p_o.ready = arb_rsp_wod_i.ready;
   end : RSP_WOD_PIPE_IP_ASSIGN
   axis_pipeline_stage #
     (
      .DATA_WIDTH(PIPE_ECI_HDR_WIDTH)
      )
   rsp_wod_pipe_inst
     (
      .clk(clk),
      .reset(reset),
      .us_data({rsp_wod_p_i.hdr, rsp_wod_p_i.size, rsp_wod_p_i.vc}),
      .us_valid(rsp_wod_p_i.valid),
      .us_ready(rsp_wod_p_i.ready),
      .ds_data({rsp_wod_p_o.hdr, rsp_wod_p_o.size, rsp_wod_p_o.vc}),
      .ds_valid(rsp_wod_p_o.valid),
      .ds_ready(rsp_wod_p_o.ready)
      );

   // rsp_wd input pipeline stage.
   always_comb begin : RSP_WD_PIPE_IP_ASSIGN
      rsp_wd_p_i.hdr   = rsp_wd_hdr_i;
      rsp_wd_p_i.size  = rsp_wd_pkt_size_i;
      rsp_wd_p_i.vc    = rsp_wd_pkt_vc_i;
      rsp_wd_p_i.valid = rsp_wd_pkt_valid_i;
      rsp_wd_p_o.ready = arb_rsp_wd_i.ready;
   end : RSP_WD_PIPE_IP_ASSIGN
   axis_pipeline_stage #
     (
      .DATA_WIDTH(PIPE_ECI_HDR_WIDTH)
      )
   rsp_wd_pipe_inst
     (
      .clk(clk),
      .reset(reset),
      .us_data({rsp_wd_p_i.hdr, rsp_wd_p_i.size, rsp_wd_p_i.vc}),
      .us_valid(rsp_wd_p_i.valid),
      .us_ready(rsp_wd_p_i.ready),
      .ds_data({rsp_wd_p_o.hdr, rsp_wd_p_o.size, rsp_wd_p_o.vc}),
      .ds_valid(rsp_wd_p_o.valid),
      .ds_ready(rsp_wd_p_o.ready)
      );

   // lcl_fwd_wod input pipeline stage.
   always_comb begin : LCL_FWD_WOD_PIPE_IP_ASSIGN
      lcl_fwd_wod_p_i.hdr   = lcl_fwd_wod_hdr_i;
      lcl_fwd_wod_p_i.size  = lcl_fwd_wod_pkt_size_i;
      lcl_fwd_wod_p_i.vc    = lcl_fwd_wod_pkt_vc_i;
      lcl_fwd_wod_p_i.valid = lcl_fwd_wod_pkt_valid_i;
      lcl_fwd_wod_p_o.ready = arb_lcl_fwd_wod_i.ready;
   end : LCL_FWD_WOD_PIPE_IP_ASSIGN
   axis_pipeline_stage #
     (
      .DATA_WIDTH(PIPE_ECI_HDR_WIDTH)
       )
   lcl_fwd_wod_pipe_inst
     (
      .clk(clk),
      .reset(reset),
      .us_data({lcl_fwd_wod_p_i.hdr, lcl_fwd_wod_p_i.size, lcl_fwd_wod_p_i.vc}),
      .us_valid(lcl_fwd_wod_p_i.valid),
      .us_ready(lcl_fwd_wod_p_i.ready),
      .ds_data({lcl_fwd_wod_p_o.hdr, lcl_fwd_wod_p_o.size, lcl_fwd_wod_p_o.vc}),
      .ds_valid(lcl_fwd_wod_p_o.valid),
      .ds_ready(lcl_fwd_wod_p_o.ready)
      );

   // lcl_rsp_wod input pipeline stage.
   always_comb begin : LCL_RSP_WOD_PIPE_IP_ASSIGN
      lcl_rsp_wod_p_i.hdr   = lcl_rsp_wod_hdr_i;
      lcl_rsp_wod_p_i.size  = lcl_rsp_wod_pkt_size_i;
      lcl_rsp_wod_p_i.vc    = lcl_rsp_wod_pkt_vc_i;
      lcl_rsp_wod_p_i.valid = lcl_rsp_wod_pkt_valid_i;
      lcl_rsp_wod_p_o.ready = arb_lcl_rsp_wod_i.ready;
   end : LCL_RSP_WOD_PIPE_IP_ASSIGN
   axis_pipeline_stage #
     (
      .DATA_WIDTH(PIPE_ECI_HDR_WIDTH)
       )
   lcl_rsp_wod_pipe_inst
     (
      .clk(clk),
      .reset(reset),
      .us_data({lcl_rsp_wod_p_i.hdr, lcl_rsp_wod_p_i.size, lcl_rsp_wod_p_i.vc}),
      .us_valid(lcl_rsp_wod_p_i.valid),
      .us_ready(lcl_rsp_wod_p_i.ready),
      .ds_data({lcl_rsp_wod_p_o.hdr, lcl_rsp_wod_p_o.size, lcl_rsp_wod_p_o.vc}),
      .ds_valid(lcl_rsp_wod_p_o.valid),
      .ds_ready(lcl_rsp_wod_p_o.ready)
      );


   // Read descriptor response rd_rsp input pipeline stage.
   always_comb begin : RD_RSP_PIPE_IP_ASSIGN
      rd_rsp_id_p_i = rd_rsp_id_i;
      rd_rsp_valid_p_i = rd_rsp_valid_i;
      rd_rsp_ready_p_i = dc_rd_rsp_ready_o;
   end : RD_RSP_PIPE_IP_ASSIGN
   axis_pipeline_stage #
     (
      .DATA_WIDTH(7) //rd_rsp_id_i
      )
   rd_rsp_pipe_inst
     (
      .clk(clk),
      .reset(reset),
      .us_data(rd_rsp_id_p_i),
      .us_valid(rd_rsp_valid_p_i),
      .us_ready(rd_rsp_ready_p_o),
      .ds_data(rd_rsp_id_p_o),
      .ds_valid(rd_rsp_valid_p_o),
      .ds_ready(rd_rsp_ready_p_i)
      );

   // Write descriptor response wr_rsp input pipeline stage.
   always_comb begin : WR_RSP_PIPE_IP_ASSIGN
      wr_rsp_id_p_i    = wr_rsp_id_i;
      wr_rsp_bresp_p_i = wr_rsp_bresp_i;
      wr_rsp_valid_p_i = wr_rsp_valid_i;
      wr_rsp_ready_p_i = dc_wr_rsp_ready_o;
   end : WR_RSP_PIPE_IP_ASSIGN
   axis_pipeline_stage #
     (
      .DATA_WIDTH(7+2) // wr_rsp_id_i, wr_rsp_bresp_i
       )
   wr_rsp_pipe_inst
     (
      .clk(clk),
      .reset(reset),
      .us_data({wr_rsp_id_p_i, wr_rsp_bresp_p_i}),
      .us_valid(wr_rsp_valid_p_i),
      .us_ready(wr_rsp_ready_p_o),
      .ds_data({wr_rsp_id_p_o, wr_rsp_bresp_p_o}),
      .ds_valid(wr_rsp_valid_p_o),
      .ds_ready(wr_rsp_ready_p_i)
      );

   // Arbitrate between req wod VC, rsp wd, rsp wod headers to choose one.
   // Only header not data.
   always_comb begin : IP_EVT_ARB_IP_ASSIGN
      arb_req_wod_i.hdr   = req_wod_p_o.hdr;
      arb_req_wod_i.size  = req_wod_p_o.size;
      arb_req_wod_i.vc    = req_wod_p_o.vc;
      arb_req_wod_i.valid = req_wod_p_o.valid;

      arb_rsp_wod_i.hdr   = rsp_wod_p_o.hdr;
      arb_rsp_wod_i.size  = rsp_wod_p_o.size;
      arb_rsp_wod_i.vc    = rsp_wod_p_o.vc;
      arb_rsp_wod_i.valid = rsp_wod_p_o.valid;

      arb_rsp_wd_i.hdr    = rsp_wd_p_o.hdr;
      arb_rsp_wd_i.size   = rsp_wd_p_o.size;
      arb_rsp_wd_i.vc     = rsp_wd_p_o.vc;
      arb_rsp_wd_i.valid  = rsp_wd_p_o.valid;

      arb_lcl_fwd_wod_i.hdr   = lcl_fwd_wod_p_o.hdr;
      arb_lcl_fwd_wod_i.size  = lcl_fwd_wod_p_o.size;
      arb_lcl_fwd_wod_i.vc    = lcl_fwd_wod_p_o.vc;
      arb_lcl_fwd_wod_i.valid = lcl_fwd_wod_p_o.valid;

      arb_lcl_rsp_wod_i.hdr   = lcl_rsp_wod_p_o.hdr;
      arb_lcl_rsp_wod_i.size  = lcl_rsp_wod_p_o.size;
      arb_lcl_rsp_wod_i.vc    = lcl_rsp_wod_p_o.vc;
      arb_lcl_rsp_wod_i.valid = lcl_rsp_wod_p_o.valid;

      arb_eci_evt.ready   = dc_i.ready;
      arb_eci_evt.stall   = dc_i.stall;
   end : IP_EVT_ARB_IP_ASSIGN

   arb_4_ecih
     ip_evts_arb1
       (
	.clk(clk),
	.reset(reset),
	// Input ECI header 1
	.eci_hdr1_i		(arb_req_wod_i.hdr),
	.eci_pkt1_size_i	(arb_req_wod_i.size),
	.eci_pkt1_vc_i		(arb_req_wod_i.vc),
	.eci_pkt1_valid_i	(arb_req_wod_i.valid),
	.eci_pkt1_ready_o	(arb_req_wod_i.ready),
	.eci_pkt1_stall_o	(),// not connected on purpose
	// Input ECI header 2
	.eci_hdr2_i		(arb_rsp_wd_i.hdr),
	.eci_pkt2_size_i	(arb_rsp_wd_i.size),
	.eci_pkt2_vc_i		(arb_rsp_wd_i.vc),
	.eci_pkt2_valid_i	(arb_rsp_wd_i.valid),
	.eci_pkt2_ready_o	(arb_rsp_wd_i.ready),
	.eci_pkt2_stall_o	(), // not connected on purpose
	// Input ECI header 3
	.eci_hdr3_i		(arb_rsp_wod_i.hdr),
	.eci_pkt3_size_i	(arb_rsp_wod_i.size),
	.eci_pkt3_vc_i		(arb_rsp_wod_i.vc),
	.eci_pkt3_valid_i	(arb_rsp_wod_i.valid),
	.eci_pkt3_ready_o	(arb_rsp_wod_i.ready),
	.eci_pkt3_stall_o	(), // not connected on purpose
	// Input LCL header 4
	.eci_hdr4_i		(arb_lcl_fwd_wod_i.hdr),
	.eci_pkt4_size_i	(arb_lcl_fwd_wod_i.size),
	.eci_pkt4_vc_i		(arb_lcl_fwd_wod_i.vc),
	.eci_pkt4_valid_i	(arb_lcl_fwd_wod_i.valid),
	.eci_pkt4_ready_o	(arb_lcl_fwd_wod_i.ready),
	.eci_pkt4_stall_o	(),// not connected on purpose
	// Input LCL header 5
	.eci_hdr5_i		(arb_lcl_rsp_wod_i.hdr),
	.eci_pkt5_size_i	(arb_lcl_rsp_wod_i.size),
	.eci_pkt5_vc_i		(arb_lcl_rsp_wod_i.vc),
	.eci_pkt5_valid_i	(arb_lcl_rsp_wod_i.valid),
	.eci_pkt5_ready_o	(arb_lcl_rsp_wod_i.ready),
	.eci_pkt5_stall_o	(),// not connected on purpose
	// Arbitrated ECI header
	.eci_hdr_o		(arb_eci_evt.hdr),
	.eci_pkt_size_o		(arb_eci_evt.size),
	.eci_pkt_vc_o		(arb_eci_evt.vc),
	.eci_pkt_valid_o	(arb_eci_evt.valid),
	.eci_pkt_ready_i	(arb_eci_evt.ready),
	.eci_pkt_stall_i	(arb_eci_evt.stall)
	);

   // Directory Controller Unit.
   always_comb begin : DCU_IP_ASSIGN
      // Input ECI header I/F.
      dc_i.hdr		= arb_eci_evt.hdr;
      dc_i.size		= arb_eci_evt.size;
      dc_i.vc		= arb_eci_evt.vc;
      dc_i.valid	= arb_eci_evt.valid;
      // Output ECI header I/F.
      dc_o.ready	= eci_rsp_ready_p_o;
      // Read desc I/F.
      dc_rd_req_ready_i = rd_req_ready_p_o;
      dc_rd_rsp_id_i	= rd_rsp_id_p_o;
      dc_rd_rsp_valid_i = rd_rsp_valid_p_o;
      // Write desc I/F.
      dc_wr_req_ready_i = wr_req_ready_p_o;
      dc_wr_rsp_id_i	= wr_rsp_id_p_o;
      dc_wr_rsp_bresp_i = wr_rsp_bresp_p_o;
      dc_wr_rsp_valid_i = wr_rsp_valid_p_o;
   end : DCU_IP_ASSIGN

   dcu #
     (
      .PERF_REGS_WIDTH(PERF_REGS_WIDTH),
      .SYNTH_PERF_REGS(SYNTH_PERF_REGS)
      )
   core_dcu_inst
     (
      .clk                      (clk),
      .reset                    (reset),
      // Input ECI header.
      .eci_req_hdr_i		(dc_i.hdr),
      .eci_req_vc_i		(dc_i.vc),
      .eci_req_size_i		(dc_i.size),
      .eci_req_valid_i		(dc_i.valid),
      .eci_req_ready_o		(dc_i.ready),
      .eci_req_stall_o		(dc_i.stall),
      // Output ECI header.
      .eci_rsp_hdr_o		(dc_o.hdr),
      .eci_rsp_vc_o		(dc_o.vc),
      .eci_rsp_size_o		(dc_o.size),
      .eci_rsp_valid_o		(dc_o.valid),
      .eci_rsp_ready_i		(dc_o.ready),
      // Read Desc I/F.
      .rd_req_id_o		(dc_rd_req_id_o),
      .rd_req_addr_o		(dc_rd_req_addr_o),
      .rd_req_valid_o		(dc_rd_req_valid_o),
      .rd_req_ready_i		(dc_rd_req_ready_i),
      .rd_rsp_id_i		(dc_rd_rsp_id_i),
      .rd_rsp_valid_i		(dc_rd_rsp_valid_i),
      .rd_rsp_ready_o		(dc_rd_rsp_ready_o),
      // Write Desc I/F.
      .wr_req_id_o		(dc_wr_req_id_o),
      .wr_req_addr_o		(dc_wr_req_addr_o),
      .wr_req_strb_o		(dc_wr_req_strb_o),
      .wr_req_valid_o		(dc_wr_req_valid_o),
      .wr_req_ready_i		(dc_wr_req_ready_i),
      .wr_rsp_id_i		(dc_wr_rsp_id_i),
      .wr_rsp_bresp_i		(dc_wr_rsp_bresp_i),
      .wr_rsp_valid_i		(dc_wr_rsp_valid_i),
      .wr_rsp_ready_o		(dc_wr_rsp_ready_o),
      // Performance registers.
      .prf_no_req_pkt_rdy_o	(dc_prf_no_req_pkt_rdy_o),
      .prf_no_req_pkt_stl_o	(dc_prf_no_req_pkt_stl_o),
      .prf_no_rsp_pkt_sent_o	(dc_prf_no_rsp_pkt_sent_o),
      .prf_no_rd_req_o		(dc_prf_no_rd_req_o),
      .prf_no_rd_rsp_o		(dc_prf_no_rd_rsp_o),
      .prf_no_wr_req_o		(dc_prf_no_wr_req_o),
      .prf_no_wr_rsp_o		(dc_prf_no_wr_rsp_o),
      .prf_no_cyc_bw_req_vld_rdy_o (dc_prf_no_cyc_bw_req_vld_rdy_o),
      // Debug I/F.
      .dbg_eci_req_hdr_o	(dc_dbg_eci_req_hdr_o),
      .dbg_eci_req_vc_o		(dc_dbg_eci_req_vc_o),
      .dbg_eci_req_size_o	(dc_dbg_eci_req_size_o),
      .dbg_eci_req_ready_o	(dc_dbg_eci_req_ready_o),
      .dbg_eci_req_stalled_o	(dc_dbg_eci_req_stalled_o),
      .dbg_eci_rsp_hdr_o	(dc_dbg_eci_rsp_hdr_o),
      .dbg_eci_rsp_vc_o		(dc_dbg_eci_rsp_vc_o),
      .dbg_eci_rsp_size_o	(dc_dbg_eci_rsp_size_o),
      .dbg_eci_rsp_valid_o	(dc_dbg_eci_rsp_valid_o),
      .dbg_stall_prot_o         (dc_dbg_stall_prot_o),
      .dbg_stall_rd_busy_o      (dc_dbg_stall_rd_busy_o),
      .dbg_stall_wr_busy_o      (dc_dbg_stall_wr_busy_o),
      .dbg_stall_eci_tr_busy_o  (dc_dbg_stall_eci_tr_busy_o),
      .dbg_stall_rtg_full_o     (dc_dbg_stall_rtg_full_o),
      .dbg_stall_wait_op_hs_o   (dc_dbg_stall_wait_op_hs_o),
      .dbg_req_cc_enc_o		(dc_dbg_req_cc_enc_o),
      .dbg_rdda_o		(dc_dbg_rdda_o),
      .dbg_wdda_o		(dc_dbg_wdda_o),
      .dbg_cc_active_req_o	(dc_dbg_cc_active_req_o),
      .dbg_cc_present_state_o	(dc_dbg_cc_present_state_o),
      .dbg_cc_next_state_o	(dc_dbg_cc_next_state_o),
      .dbg_cc_next_action_o	(dc_dbg_cc_next_action_o),
      .dbg_err_code_o		(dc_dbg_err_code_o),
      .dbg_error_o		(dc_dbg_error_o),
      .dbg_valid_o		(dc_dbg_valid_o)
      );

   // Output Pipeline stage
   // read desc requests and write desc requests
   // go through an output pipeline stage.
   always_comb begin : RD_REQ_PIPE_IP_ASSIGN
      rd_req_id_p_i    = dc_rd_req_id_o;
      rd_req_addr_p_i  = dc_rd_req_addr_o;
      rd_req_valid_p_i = dc_rd_req_valid_o;
      rd_req_ready_p_i = rd_req_ready_i;
   end : RD_REQ_PIPE_IP_ASSIGN
   axis_pipeline_stage #
     (
      .DATA_WIDTH(7+38) //rd_req_id_o, rd_req_addr_o
       )
   rd_req_pipe_inst
     (
      .clk(clk),
      .reset(reset),
      .us_data({rd_req_id_p_i, rd_req_addr_p_i}),
      .us_valid(rd_req_valid_p_i),
      .us_ready(rd_req_ready_p_o),
      .ds_data({rd_req_id_p_o, rd_req_addr_p_o}),
      .ds_valid(rd_req_valid_p_o),
      .ds_ready(rd_req_ready_p_i)
      );

   // write desc requests output pipeline stage.
   always_comb begin : WR_REQ_PIPE_IP_ASSIGN
      wr_req_id_p_i    = dc_wr_req_id_o;
      wr_req_addr_p_i  = dc_wr_req_addr_o;
      wr_req_strb_p_i  = dc_wr_req_strb_o;
      wr_req_valid_p_i = dc_wr_req_valid_o;
      wr_req_ready_p_i = wr_req_ready_i;
   end : WR_REQ_PIPE_IP_ASSIGN
   axis_pipeline_stage #
     (
      .DATA_WIDTH(7+38+ECI_CL_SIZE_BYTES) //wr_req_id_o,wr_req_addr_o,wr_req_strb_o
      )
   wr_req_pipe_inst
     (
      .clk(clk),
      .reset(reset),
      .us_data({wr_req_id_p_i, wr_req_addr_p_i, wr_req_strb_p_i}),
      .us_valid(wr_req_valid_p_i),
      .us_ready(wr_req_ready_p_o),
      .ds_data({wr_req_id_p_o, wr_req_addr_p_o, wr_req_strb_p_o}),
      .ds_valid(wr_req_valid_p_o),
      .ds_ready(wr_req_ready_p_i)
      );

   // ECI response events output pipeline stage.
   always_comb begin : ECI_RSP_PIPE_IP_ASSIGN
      eci_rsp_hdr_p_i   = dc_o.hdr;
      eci_rsp_vc_p_i    = dc_o.vc;
      eci_rsp_size_p_i  = dc_o.size;
      eci_rsp_valid_p_i = dc_o.valid;
      eci_rsp_ready_p_i = eci_rsp_ready_i;
   end : ECI_RSP_PIPE_IP_ASSIGN
   axis_pipeline_stage #
     (
      .DATA_WIDTH(PIPE_ECI_HDR_WIDTH)
       )
   eci_rsp_pipe_inst
     (
      .clk(clk),
      .reset(reset),
      .us_data({eci_rsp_hdr_p_i,eci_rsp_size_p_i,eci_rsp_vc_p_i}),
      .us_valid(eci_rsp_valid_p_i),
      .us_ready(eci_rsp_ready_p_o),
      .ds_data({eci_rsp_hdr_p_o, eci_rsp_size_p_o, eci_rsp_vc_p_o}),
      .ds_valid(eci_rsp_valid_p_o),
      .ds_ready(eci_rsp_ready_p_i)
      );

endmodule // dcu_top
`endif
