/*
Steps:
1. Create a vivado project
2. Add the source rtl, testbench file
3. From IP catalog, choose AXI Verifiction IP
4. Change parameters of IP as per need
5. Leave the default module name which is axi_vip_0
6. Add the following lines to the code 
7. NOTE: if asserting reset, it should be atleast 16 cycles, assert it for 20 cycles to be safe

Backdoor write and read
1. Note addresses has to be aligned, if data is 32 bytes wide
   the lower 5 bits of addrress has to be 0
      slv_agent.mem_model.backdoor_memory_write  (
						  .addr (40'h1111_0000),
						  .payload (wr_data_i[1]),
						  .strb ({32{1'b1}})
						  );
      

      test_read = slv_agent.mem_model.backdoor_memory_read(.addr(40'h1111_0000));
*/

import axi_vip_0_pkg::*;

module axi_wr_clTb();

   parameter DESC_ID_WIDTH = 6;
   parameter DESC_ADDR_WIDTH = 40;
   parameter DESC_DATA_WIDTH = 1024;
   parameter DESC_STRB_WIDTH = DESC_DATA_WIDTH/8;
   parameter AXI_ID_WIDTH   = DESC_ID_WIDTH;
   parameter AXI_ADDR_WIDTH = DESC_ADDR_WIDTH;
   parameter AXI_DATA_WIDTH = 512;
   parameter AXI_STRB_WIDTH = AXI_DATA_WIDTH/8;

   //input output ports 
   //Input signals
   bit clk, reset;
   
   logic [DESC_ID_WIDTH-1:0]   wr_req_id_i;
   logic [DESC_ADDR_WIDTH-1:0] wr_req_addr_i;
   logic [1:0][511:0] 	       wr_req_data_i;
   //logic [DESC_DATA_WIDTH-1:0] wr_req_data_i;
   logic [DESC_STRB_WIDTH-1:0] wr_req_strb_i;
   logic 		       wr_req_valid_i;
   logic 		       wr_rsp_ready_i;
   logic 		       p_axi_awready;
   logic 		       p_axi_wready;
   logic [AXI_ID_WIDTH-1:0]    p_axi_bid;
   logic [1:0] 		       p_axi_bresp;
   logic 		       p_axi_bvalid;

   //Output signals
   logic 		       wr_req_ready_o;
   logic [DESC_ID_WIDTH-1:0]   wr_rsp_id_o;
   logic [1:0] 		       wr_rsp_bresp_o;
   logic 		       wr_rsp_valid_o;
   logic [AXI_ID_WIDTH-1:0]    p_axi_awid;
   logic [AXI_ADDR_WIDTH-1:0]  p_axi_awaddr;
   logic [7:0] 		       p_axi_awlen;
   logic [2:0] 		       p_axi_awsize;
   logic [1:0] 		       p_axi_awburst;
   logic 		       p_axi_awlock;
   logic [3:0] 		       p_axi_awcache;
   logic [2:0] 		       p_axi_awprot;
   logic 		       p_axi_awvalid;
   logic [AXI_DATA_WIDTH-1:0]  p_axi_wdata;
   logic [AXI_STRB_WIDTH-1:0]  p_axi_wstrb;
   logic 		       p_axi_wlast;
   logic 		       p_axi_wvalid;
   logic 		       p_axi_bready;
   
   logic [AXI_DATA_WIDTH-1:0]  test_read;

   // AXI Slave agent
   axi_vip_0_slv_mem_t slv_agent;

   // Start the verification component
   initial begin
      slv_agent = new("slv_agent",axi_wr_clTb.axi_vip_inst.inst.IF);
      slv_agent.start_slave();
   end

   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   axi_wr_cl axi_wr_cl1 (
			 .clk(clk),
			 .reset(reset),
			 .wr_req_id_i(wr_req_id_i),
			 .wr_req_addr_i(wr_req_addr_i),
			 .wr_req_data_i(wr_req_data_i),
			 .wr_req_strb_i(wr_req_strb_i),
			 .wr_req_valid_i(wr_req_valid_i),
			 .wr_rsp_ready_i(wr_rsp_ready_i),
			 .p_axi_awready(p_axi_awready),
			 .p_axi_wready(p_axi_wready),
			 .p_axi_bid(p_axi_bid),
			 .p_axi_bresp(p_axi_bresp),
			 .p_axi_bvalid(p_axi_bvalid),
			 .wr_req_ready_o(wr_req_ready_o),
			 .wr_rsp_id_o(wr_rsp_id_o),
			 .wr_rsp_bresp_o(wr_rsp_bresp_o),
			 .wr_rsp_valid_o(wr_rsp_valid_o),
			 .p_axi_awid(p_axi_awid),
			 .p_axi_awaddr(p_axi_awaddr),
			 .p_axi_awlen(p_axi_awlen),
			 .p_axi_awsize(p_axi_awsize),
			 .p_axi_awburst(p_axi_awburst),
			 .p_axi_awlock(p_axi_awlock),
			 .p_axi_awcache(p_axi_awcache),
			 .p_axi_awprot(p_axi_awprot),
			 .p_axi_awvalid(p_axi_awvalid),
			 .p_axi_wdata(p_axi_wdata),
			 .p_axi_wstrb(p_axi_wstrb),
			 .p_axi_wlast(p_axi_wlast),
			 .p_axi_wvalid(p_axi_wvalid),
			 .p_axi_bready(p_axi_bready)
			 );//instantiation completed 

   axi_vip_0
     axi_vip_inst
       (
	.aclk(clk),
	.aresetn(~reset),
	.s_axi_awid	(p_axi_awid    ),
	.s_axi_awaddr	(p_axi_awaddr  ),
	.s_axi_awlen	(p_axi_awlen   ),
	.s_axi_awsize	(p_axi_awsize  ),
	.s_axi_awburst	(p_axi_awburst ),
	.s_axi_awcache  (p_axi_awcache),
	.s_axi_awprot   (p_axi_awprot),
	.s_axi_awregion ('0),
	.s_axi_awqos    ('0),
	.s_axi_awvalid	(p_axi_awvalid ),
	.s_axi_awready	(p_axi_awready ),
	.s_axi_wdata	(p_axi_wdata   ),
	.s_axi_wstrb	(p_axi_wstrb   ),
	.s_axi_wlast	(p_axi_wlast   ),
	.s_axi_wvalid	(p_axi_wvalid  ),
	.s_axi_wready	(p_axi_wready  ),
	.s_axi_bid	(p_axi_bid     ),
	.s_axi_bresp	(p_axi_bresp   ),
	.s_axi_bvalid	(p_axi_bvalid  ),
	.s_axi_bready	(p_axi_bready  )
	);

   
   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports
      clk = '0;
      reset = '1;
      
      wr_req_id_i = '0;
      wr_req_addr_i = '0;
      wr_req_data_i = '0;
      wr_req_strb_i = '0;
      wr_req_valid_i = '0;
      wr_rsp_ready_i = '0;
      ##20;
      reset = 1'b0;

      ##5;
      wr_req_id_i = 'd5;
      wr_req_addr_i = {34'd1, 6'd0};
      wr_req_data_i[0] = {128{4'b1}};
      wr_req_data_i[1] = {128{4'd2}};
      wr_req_strb_i    = '1;
      wr_rsp_ready_i   = '1;
      ##1;
      wr_req_valid_i = 1'b1;
      wait(wr_req_valid_i & wr_req_ready_o);
      ##1;
      wr_req_valid_i = 1'b0;
      
      // wait till writing completes. 
      wait(wr_rsp_valid_o & wr_rsp_ready_i);
      ##1;

      // Aligning the address to 64 byte word boundary 
      for( integer i=0; i<64*2; i=i+64 ) begin
	 test_read = slv_agent.mem_model.backdoor_memory_read(.addr(wr_req_addr_i + i));
	 if(test_read != wr_req_data_i[i]) begin
	    $error("Error: Read does not match write (instance %m)");
	    $finish;
	 end else begin
	    $display("Success write matches read = %h", test_read);
	 end
      end 

      // reset the values in idx 1, 2 through backdoor write.
      // idx 1 has value 10(repeated), idx 2 has value 11(repeated).
      reset_my_cl();

      // strobes for idx 1 (beat 1) is all 0s and idx 2 is all 1s.
      // idx 1 should not be overwritten, idx 2 should be overwritten with 6.
      // idx 1 should have 10(repeated), idx 2 should have 6(repeated).
      write_req(
		.id_i('d10),
		.addr_i({34'd1, 6'd0}),
		.data_i({ {128{4'd6}}, {128{4'd5}} }),
		.strb_i({ {64{1'b1 }}, {64{1'b0} } })
		);
      
      compare_read_data(
			.exp_idx1_data_i({128{4'd10}}),
			.exp_idx2_data_i({128{4'd6}})
			);

      #500 $finish;
   end 

   task static reset_my_cl();
      // 64 Byte aligned address.
      // Prefilling these addresses with some values.
      // Data is 512 bits, strobe would be 64 bits. 
      slv_agent.mem_model.backdoor_memory_write(
						.addr({34'd1, 6'd0}),
						.payload({128{4'd10}}),
						.strb({64{1'b1}})
						);
      
      // Writing to idx 2
      slv_agent.mem_model.backdoor_memory_write(
						.addr({34'd2, 6'd0}),
						.payload({128{4'd11}}),
						.strb({64{1'b1}})
						);
   endtask //reset_my_cl

   task static write_req(
			 input logic [DESC_ID_WIDTH-1:0]   id_i,
			 input logic [DESC_ADDR_WIDTH-1:0] addr_i,
			 input logic [DESC_DATA_WIDTH-1:0] data_i,
			 input logic [DESC_STRB_WIDTH-1:0] strb_i
			 );
      wr_req_id_i = id_i;
      wr_req_addr_i = {addr_i[DESC_ADDR_WIDTH-1:6], 6'd0};
      wr_req_data_i = data_i;
      wr_req_strb_i = strb_i;
      wr_req_valid_i = 1'b1;
      wait(wr_req_valid_i & wr_req_ready_o);
      ##1;
      wr_req_valid_i = 1'b0;
      // wait till writing completes.
      wait(wr_rsp_valid_o & wr_rsp_ready_i);
      ##1;
   endtask //write_req

   task static compare_read_data(
				 input logic [AXI_DATA_WIDTH-1:0] exp_idx1_data_i,
				 input logic [AXI_DATA_WIDTH-1:0] exp_idx2_data_i
				 );
      // read idx1.
      test_read = slv_agent.mem_model.backdoor_memory_read(.addr({36'd1,6'd0}));
      if(test_read != exp_idx1_data_i) begin
	 $error("Error: Read does not match write (instance %m)");
	 $finish;
      end else begin
	 $display("Success write matches read = %h", test_read);
      end

      // read idx2.
      test_read = slv_agent.mem_model.backdoor_memory_read(.addr({36'd2,6'd0}));
      if(test_read != exp_idx2_data_i) begin
	 $error("Error: Read does not match write (instance %m)");
	 $finish;
      end else begin
	 $display("Success write matches read = %h", test_read);
      end

   endtask //compare_read_data
   
endmodule
