/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-11-22
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */

`ifndef CL_INV_LOOPBACK_SV
`define CL_INV_LOOPBACK_SV

import eci_cmd_defs::*;
import eci_dcs_defs::*;

module cl_inv_loopback #
  (
   parameter NUM_IN_STAGES = 32,
   parameter PERF_REGS_WIDTH = 32
   )
   (
    input logic 				 clk,
    input logic 				 reset,
    // Sampled addresses to invalidate.
    input logic [DS_ADDR_WIDTH-1:0] 		 rd_req_addr_i,
    input logic 				 rd_req_valid_i,
    // Output LCI VC 16 or 17
    output logic [ECI_WORD_WIDTH-1:0] 		 lcl_fwd_wod_hdr_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 lcl_fwd_wod_pkt_size_o,
    output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] lcl_fwd_wod_pkt_vc_o,
    output logic 				 lcl_fwd_wod_pkt_valid_o,
    input logic 				 lcl_fwd_wod_pkt_ready_i,
    // Input LCIA VC 18 or 19.
    input logic [ECI_WORD_WIDTH-1:0] 		 lcl_rsp_wod_hdr_i,
    input logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 lcl_rsp_wod_pkt_size_i,
    input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0]  lcl_rsp_wod_pkt_vc_i,
    input logic 				 lcl_rsp_wod_pkt_valid_i,
    output logic 				 lcl_rsp_wod_pkt_ready_o,
    // Output unlock VC 18 or 19.
    output logic [ECI_WORD_WIDTH-1:0] 		 lcl_rsp_wod_hdr_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 lcl_rsp_wod_pkt_size_o,
    output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] lcl_rsp_wod_pkt_vc_o,
    output logic 				 lcl_rsp_wod_pkt_valid_o,
    input logic 				 lcl_rsp_wod_pkt_ready_i
    );

   localparam CL_IDX_ODD = 1'b1;
   localparam CL_IDX_EVEN = 1'b0;
   
   // Pipe stage signals.
   logic [DS_ADDR_WIDTH-1:0] 			addr_us_data;
   logic 					addr_us_valid;
   logic 					addr_us_ready;
   logic [DS_ADDR_WIDTH-1:0] 			addr_ds_data;
   logic 					addr_ds_valid;
   logic 					addr_ds_ready;
   // LCIA pipe signals
   logic [DS_ADDR_WIDTH-1:0] 			lcia_addr_us_data;
   logic 					lcia_addr_us_valid;
   logic 					lcia_addr_us_ready;
   logic [DS_ADDR_WIDTH-1:0] 			lcia_addr_ds_data;
   logic 					lcia_addr_ds_valid;
   logic 					lcia_addr_ds_ready;
   // cast incoming lcia
   eci_word_t lcia_cmd;
   eci_word_t unlock_cmd;
   // Perf reg signals.
   logic [PERF_REGS_WIDTH-1:0] 			num_valid_rd_req_reg = '0;
   logic [PERF_REGS_WIDTH-1:0] 			num_lci_issued_reg = '0;
      
   always_comb begin : OUT_ASSIGN
      // Generate and send LCI.
      lcl_fwd_wod_hdr_o = gen_lci(addr_ds_data);
      lcl_fwd_wod_pkt_size_o = 'd1;
      // Odd CL -> Even VC and vice versa.
      if(addr_ds_data[ECI_CL_ADDR_LSB] == CL_IDX_ODD) begin
	 lcl_fwd_wod_pkt_vc_o = VC_LCL_FWD_WO_DATA_E; //16
      end else begin
	 lcl_fwd_wod_pkt_vc_o = VC_LCL_FWD_WO_DATA_O; //17
      end
      lcl_fwd_wod_pkt_valid_o = addr_ds_valid;
      // LCIA ready.
      lcl_rsp_wod_pkt_ready_o = lcia_addr_us_ready;
      // Generate unlock, assign to output. 
      unlock_cmd.eci_word = '0;
      unlock_cmd.lcl_unlock.opcode = LCL_CMD_MRSP_UNLOCK;
      unlock_cmd.lcl_unlock.address[DS_ADDR_WIDTH-1:0] = lcia_addr_ds_data;
      lcl_rsp_wod_hdr_o = unlock_cmd.eci_word;
      lcl_rsp_wod_pkt_size_o = 'd1;
      // Odd cl - even VC.
      if(lcia_addr_ds_data[ECI_CL_ADDR_LSB] == CL_IDX_ODD) begin
	 lcl_rsp_wod_pkt_vc_o = VC_LCL_RESP_WO_DATA_E; //18
      end else begin
	 lcl_rsp_wod_pkt_vc_o = VC_LCL_RESP_WO_DATA_O; //19
      end
      lcl_rsp_wod_pkt_valid_o = lcia_addr_ds_valid;
      // Perf
   end : OUT_ASSIGN

   // Delay between seeing rd addresses and issuing
   // cleans. 
   // The input addresses are sampled,
   // there is no valid ready flow control at input.
   // any valid addresses when us_ready is low is
   // dropped.
   always_comb begin : PIP_IP_ASSIGN
      addr_us_data = rd_req_addr_i;
      addr_us_valid = rd_req_valid_i;
      addr_ds_ready = lcl_fwd_wod_pkt_ready_i;
   end : PIP_IP_ASSIGN
   vr_pipe_stage #
     (
      .DATA_WIDTH(DS_ADDR_WIDTH),
      .NUM_STAGES(NUM_IN_STAGES)
       )
   addr_vr_pipe1
     (
      .clk(clk),
      .reset(reset),
      .us_data(addr_us_data),
      .us_valid(addr_us_valid),
      .us_ready(addr_us_ready),
      .ds_data(addr_ds_data),
      .ds_valid(addr_ds_valid),
      .ds_ready(addr_ds_ready)
      );

   // Generate Unlock when receiving LCIA.
   // Decode LCIA
   assign lcia_cmd = eci_word_t'(lcl_rsp_wod_hdr_i);
   always_comb begin : LCIA_PIP_IP_ASSIGN
      lcia_addr_us_data = lcia_cmd.lcl_clean_inv_ack.address[DS_ADDR_WIDTH-1:0];
      lcia_addr_us_valid = lcl_rsp_wod_pkt_valid_i;
      lcia_addr_ds_ready = lcl_rsp_wod_pkt_ready_i;
   end : LCIA_PIP_IP_ASSIGN
   axis_pipeline_stage #
     (
      .DATA_WIDTH(DS_ADDR_WIDTH)
      )
   lcia_pipe
     (
      .clk(clk),
      .reset(reset),
      .us_data(lcia_addr_us_data),
      .us_valid(lcia_addr_us_valid),
      .us_ready(lcia_addr_us_ready),
      .ds_data(lcia_addr_ds_data),
      .ds_valid(lcia_addr_ds_valid),
      .ds_ready(lcia_addr_ds_ready)
      );
   // Generate unlock and assign to output.

   always_ff @(posedge clk) begin : REG_ASSIGN
      if(rd_req_valid_i) begin
	 num_valid_rd_req_reg <= num_valid_rd_req_reg + 'd1;
      end
      if(lcl_fwd_wod_pkt_valid_o & lcl_fwd_wod_pkt_ready_i) begin
	 num_lci_issued_reg <= num_lci_issued_reg + 'd1;
      end
      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset 
	 num_valid_rd_req_reg <= '0;
	 num_lci_issued_reg <= '0;
      end
   end : REG_ASSIGN
   
   function automatic [ECI_WORD_WIDTH-1:0] gen_lci
     (
      input logic [DS_ADDR_WIDTH-1:0] addr_i
      );
      eci_word_t eci_cmd;
      eci_cmd.eci_word = '0;
      eci_cmd.lcl_clean_inv.opcode = LCL_CMD_MFWD_CLEAN_INV;
      eci_cmd.lcl_clean_inv.hreq_id = '0;
      eci_cmd.lcl_clean_inv.dmask = '1;
      eci_cmd.lcl_clean_inv.ns = 1'b1;
      eci_cmd.lcl_clean_inv.rnode = eci_nodeid_t'(ECI_FPGA_NODE_ID);
      eci_cmd.lcl_clean_inv.address = addr_i;
      return(eci_cmd.eci_word);
   endfunction : gen_lci
   
endmodule // cl_inv_loopback

`endif
