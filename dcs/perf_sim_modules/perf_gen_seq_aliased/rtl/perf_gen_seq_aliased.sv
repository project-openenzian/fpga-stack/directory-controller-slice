/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-11-26
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */

`ifndef PERF_GEN_SEQ_ALIASED_SV
`define PERF_GEN_SEQ_ALIASED_SV

import eci_cmd_defs::*;
import eci_dcs_defs::*;

// Module waits for certain number of
// upgrades to happen before it starts
// issuing aliased addresses sweeping
// a pre determined range.
// once addresses are issued, the module
// waits for rsps to be received for all
// addresses issued and then marks complete.
//
// it records time taken for issuing addresses
// and receiving responses.
module perf_gen_seq_aliased #
  (
   parameter NUM_REQS_TO_WAIT = 65536,
   parameter NUM_ADDR_TO_ISSUE = 65536,
   parameter PERF_REGS_WIDTH = 32
   )
   (
    input logic 		       clk,
    input logic 		       reset,
    // Wait for number of requests.
    input logic 		       odd_wait_req_valid_i,
    input logic 		       even_wait_req_valid_i,
    // Output aliased addresses
    // odd cl indices.
    output logic [DS_ADDR_WIDTH-1:0]   odd_al_addr_o,
    output logic 		       odd_al_addr_valid_o,
    input logic 		       odd_al_addr_ready_i,
    // Output aliased addresses even
    // even cl indices.
    output logic [DS_ADDR_WIDTH-1:0]   even_al_addr_o,
    output logic 		       even_al_addr_valid_o,
    input logic 		       even_al_addr_ready_i,
    // Once all addresses are launched
    // wait for rsps to be received for
    // all addresses.
    input logic 		       odd_wait_rsp_valid_i,
    input logic 		       even_wait_rsp_valid_i,
    output logic 		       done_o,
    // Number of cycles betweeen launching
    // addresses and receiving responses
    // for each address.
    output logic [PERF_REGS_WIDTH-1:0] perf_cyc_st_to_done_o,
    output logic [PERF_REGS_WIDTH-1:0] perf_num_tr_done_o
    );

   localparam BYTES_PER_CL = 128;

   typedef struct packed {
      logic [DS_ADDR_WIDTH-1:0] addr;
      logic			valid;
      logic			ready;
   } al_addr_if_t;

   // controller signals.
   enum {WAIT_REQ, LAUNCH, WAIT_RSP, DONE} state_reg, state_next;
   logic [PERF_REGS_WIDTH-1:0] wait_req_counter_reg = '0;
   logic [PERF_REGS_WIDTH-1:0] wait_rsp_counter_reg = '0;
   logic [PERF_REGS_WIDTH-1:0] cyc_launch_to_done_reg = '0;
   logic [ECI_ADDR_WIDTH-1:0]  un_al_addr_counter_reg = '0, un_al_addr_counter_next;
   logic [PERF_REGS_WIDTH-1:0] num_addr_issued_reg = '0, num_addr_issued_next;
   logic		       wait_req_counter_en;
   logic		       wait_rsp_counter_en;
   logic		       wait_req_counter_clr;
   logic		       wait_rsp_counter_clr;
   logic		       launch_valid;
   logic		       launch_hs;
   logic		       odd_hs, even_hs;
   logic		       cl_idx_odd, cl_idx_even;
   logic		       done_sig;
   logic		       wip_sig;
   // unaliased to aliased converter signals.
   eci_cl_addr_t un_al_addr_c;
   eci_cl_addr_t al_addr_c;
   //Output pipeline stage signals.
   al_addr_if_t odd_cl_addr_us;
   al_addr_if_t odd_cl_addr_ds;
   al_addr_if_t even_cl_addr_us;
   al_addr_if_t even_cl_addr_ds;

   always_comb begin : OUT_ASSIGN
      odd_al_addr_o        = odd_cl_addr_ds.addr;
      odd_al_addr_valid_o  = odd_cl_addr_ds.valid;
      even_al_addr_o       = even_cl_addr_ds.addr;
      even_al_addr_valid_o = even_cl_addr_ds.valid;
      done_o               = done_sig;
      perf_cyc_st_to_done_o = cyc_launch_to_done_reg;
      perf_num_tr_done_o = wait_rsp_counter_reg;
   end : OUT_ASSIGN

   assign odd_hs  = odd_cl_addr_us.valid & odd_cl_addr_us.ready;
   assign even_hs = even_cl_addr_us.valid & even_cl_addr_us.ready;
   assign launch_hs = odd_hs | even_hs;
   always_comb begin : CONTROLLER
      state_next = state_reg;
      un_al_addr_counter_next = un_al_addr_counter_reg;
      num_addr_issued_next = num_addr_issued_reg;
      wait_req_counter_en  = 1'b0;
      wait_req_counter_clr = 1'b0;
      wait_rsp_counter_en  = 1'b0;
      wait_rsp_counter_clr = 1'b0;
      launch_valid = 1'b0;
      done_sig = 1'b0;
      wip_sig = 1'b0;
      case(state_reg)
	WAIT_REQ: begin
	   // Wait for a specific number of events
	   // before issuing aliased addresses.
	   wait_req_counter_en = 1'b1;
	   if(wait_req_counter_reg == NUM_REQS_TO_WAIT) begin
	      state_next = LAUNCH;
	   end
	end
	LAUNCH: begin
	   // the address would be broadcasted to both
	   // odd and even output channels but only
	   // one of them would be valid at a time.
	   // a handshake on either of the output channels
	   // means that the data is consumed.
	   // also start accepting response events.
	   launch_valid = 1'b1;
	   wait_rsp_counter_en = 1'b1;
	   wip_sig = 1'b1;
	   if(launch_hs) begin
	      un_al_addr_counter_next = un_al_addr_counter_reg + BYTES_PER_CL;
	      num_addr_issued_next = num_addr_issued_reg + 'd1;
	   end
	   if(num_addr_issued_reg == NUM_ADDR_TO_ISSUE) begin
	      state_next = WAIT_RSP;
	   end
	end
	WAIT_RSP: begin
	   // Wait till rsps are received for
	   // all issued addresses.
	   wait_rsp_counter_en = 1'b1;
	   wip_sig = 1'b1;
	   if(wait_rsp_counter_reg == NUM_ADDR_TO_ISSUE) begin
	      // all addresses issued have been accounted for.
	      state_next = DONE;
	   end
	end
	DONE: begin
	   done_sig = 1'b1;
	   state_next = DONE;
	end
      endcase
   end : CONTROLLER

   // alias the address and determine if
   // aliased cl idx is odd or even.
   always_comb begin : ALIAS_ADDR
      cl_idx_even = 1'b0;
      cl_idx_odd = 1'b0;
      un_al_addr_c = eci_cl_addr_t'(un_al_addr_counter_reg);
      al_addr_c = '0;
      al_addr_c.parts.cl_index = eci_cmd_defs::eci_alias_cache_line_index
				 (
				  .cli(un_al_addr_c.parts.cl_index)
				  );
      if(al_addr_c.parts.cl_index[0] == 1'b1) begin
	 cl_idx_odd = 1'b1;
      end else begin
	 cl_idx_even = 1'b1;
      end
   end : ALIAS_ADDR

   // Output aliased address pipeline stages.
   // Odd cl indices.
   always_comb begin : ODD_PIPE_IP_ASSIGN
      odd_cl_addr_us.addr  = al_addr_c.flat[DS_ADDR_WIDTH-1:0];
      odd_cl_addr_us.valid = launch_valid & cl_idx_odd;
      odd_cl_addr_ds.ready = odd_al_addr_ready_i;
   end : ODD_PIPE_IP_ASSIGN
   axis_pipeline_stage #
     (
      .DATA_WIDTH(DS_ADDR_WIDTH)
       )
   odd_cl_addr_pipe
     (
      .clk(clk),
      .reset(reset),
      .us_data (odd_cl_addr_us.addr),
      .us_valid(odd_cl_addr_us.valid),
      .us_ready(odd_cl_addr_us.ready),
      .ds_data (odd_cl_addr_ds.addr),
      .ds_valid(odd_cl_addr_ds.valid),
      .ds_ready(odd_cl_addr_ds.ready)
      );

   // Output aliased address pipeline stages.
   // even cl indices.
   always_comb begin : EVEN_PIPE_IP_ASSIGN
      even_cl_addr_us.addr  = al_addr_c.flat[DS_ADDR_WIDTH-1:0];
      even_cl_addr_us.valid = launch_valid & cl_idx_even;
      even_cl_addr_ds.ready = even_al_addr_ready_i;
   end : EVEN_PIPE_IP_ASSIGN
   axis_pipeline_stage #
     (
      .DATA_WIDTH(DS_ADDR_WIDTH)
      )
   even_cl_addr_pipe
     (
      .clk(clk),
      .reset(reset),
      .us_data (even_cl_addr_us.addr),
      .us_valid(even_cl_addr_us.valid),
      .us_ready(even_cl_addr_us.ready),
      .ds_data (even_cl_addr_ds.addr),
      .ds_valid(even_cl_addr_ds.valid),
      .ds_ready(even_cl_addr_ds.ready)
      );

   // wait for req events to happen before launch.
   always_ff @(posedge clk) begin : WAIT_REQ_COUNTER
      if(wait_req_counter_en) begin
	 if(odd_wait_req_valid_i & even_wait_req_valid_i) begin
	    wait_req_counter_reg <= wait_req_counter_reg + 'd2;
	 end else if (odd_wait_req_valid_i | even_wait_req_valid_i) begin
	    wait_req_counter_reg <= wait_req_counter_reg + 'd1;
	 end else begin
	    wait_req_counter_reg <= wait_req_counter_reg;
	 end
      end
      if(wait_req_counter_clr) begin
	 wait_req_counter_reg <= '0;
      end
      if( reset ) begin
	 wait_req_counter_reg <= '0;
      end
   end : WAIT_REQ_COUNTER

   // Num rsps received.
   always_ff @(posedge clk) begin : WAIT_RSP_COUNTER
      if(wait_rsp_counter_en) begin
	 if(odd_wait_rsp_valid_i & even_wait_rsp_valid_i) begin
	    wait_rsp_counter_reg <= wait_rsp_counter_reg + 'd2;
	 end else if (odd_wait_rsp_valid_i) begin
	    wait_rsp_counter_reg <= wait_rsp_counter_reg + 'd1;
	 end else if(even_wait_rsp_valid_i) begin
	    wait_rsp_counter_reg <= wait_rsp_counter_reg + 'd1;
	 end else begin
	    wait_rsp_counter_reg <= wait_rsp_counter_reg;
	 end
      end
      if(wait_rsp_counter_clr) begin
	 wait_rsp_counter_reg <= '0;
      end
      if( reset ) begin
	 wait_rsp_counter_reg <= '0;
      end
   end : WAIT_RSP_COUNTER

   // Number of cycles between launch and complete.
   always_ff @(posedge clk) begin : WIP_COUNTER
      if(wip_sig) begin
	 cyc_launch_to_done_reg <= cyc_launch_to_done_reg + 'd1;
      end
      if( reset ) begin
	 cyc_launch_to_done_reg	<= '0;
      end
   end : WIP_COUNTER

   always_ff @(posedge clk) begin : REG_ASSIGN
      state_reg <= state_next;
      un_al_addr_counter_reg	<= un_al_addr_counter_next;
      num_addr_issued_reg <= num_addr_issued_next;
      if( reset ) begin
	 un_al_addr_counter_reg <= '0;
	 num_addr_issued_reg <= 1'b0;
	 state_reg <= WAIT_REQ;
      end
   end : REG_ASSIGN
endmodule

`endif
