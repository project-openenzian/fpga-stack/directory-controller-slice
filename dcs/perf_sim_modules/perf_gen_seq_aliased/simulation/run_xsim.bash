#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
      $XILINX_VIVADO/data/verilog/src/glbl.v \
      ../rtl/eci_cmd_defs.sv \
      ../rtl/eci_dcs_defs.sv \
      ../testbench/perf_gen_seq_aliasedTb.sv \
      ../rtl/perf_gen_seq_aliased.sv \
      ../rtl/axis_pipeline_stage.sv 

xelab -debug typical -incremental -L xpm worklib.perf_gen_seq_aliasedTb worklib.glbl -s worklib.perf_gen_seq_aliasedTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.perf_gen_seq_aliasedTb 
xsim -R worklib.perf_gen_seq_aliasedTb 
