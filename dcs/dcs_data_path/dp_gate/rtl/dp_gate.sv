/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-07-27
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */

`ifndef DP_GATE_SV
`define DP_GATE_SV

import eci_cmd_defs::*;
import eci_dcs_defs::*;

/*
 * Module Description:
 * 1. Splits the incoming header+data channel into 
 *    separate header and data valid ready channels. 
 * 2. Ensures each DCU has only 1 on-going data operation
 *    by gating the incoming header+data channel until a 
 *    "retrieved" signal is received from the data store.
 * 3. If incoming hdr+data is ECI data, option available 
 *    to map the ECI data to correct word address using
 *    dmask.
 * 
 *
 * Input Output Description:
 * 
 *  Input Header+data i/f
 *  * Header + 1024 bit data 
 *  * optional dmask if ECI data is to be mapped to 
 *    its word address.
 *  * DCU_ID from CL address corresponding to hdr, data.
 *  * valid ready flow controlled. 
 * 
 *  Output  header i/f.
 *  * hdr to be sent to the DCUs 
 *  * DCU_IDX corresponding to the DCU in current DC_SLICE 
 *    NOTE: DCU_IDX is different from DCU_ID. 
 *  * valid ready flow control.
 * 
 *  Output data i/f.
 *  * Data to be sent to data store. 
 *  * DCU_IDX corresponding to this data. 
 *  * valid ready flow control. 
 *  
 *  Clear DCU_IDX i/f.
 *  * DCU_IDX that was retrieved freeing up
 *    new transactions corresponding to the DCU_IDX. 
 *
 * Architecture Description:
 * For each input hdr+data, it takes 2 cycles to consume it. 
 * This is fine because we expect to recieved 1 hdr+data every 
 * 3 cycles at this point. 
 * In first cycle, a check is done if the DCU corresponding to 
 * this transaction has an on-going operation or not. 
 * In the second cycle, the transaction is launched (provided 
 * the first stage allows). 
 *
 * Modifiable Parameters:
 * HDR_WIDTH = width of header. 
 * GEN_MAP_ECID_TO_WRD (0/1) - disable/enable mapping ECI data 
 * to word address
 * 
 *
 * Non-modifiable Parameters:
 * None.
 * 
 * Modules Used:
 * map_ecid_to_wrd
 *
 * Notes:
 * No registers in valid ready flow control.
 *
 */

module dp_gate #
  (
   // Instantiate map_ecid_to_wrd module or not.
   parameter HDR_WIDTH = 64,
   parameter GEN_MAP_ECID_TO_WRD = 1
   )
   (
    input logic 			clk,
    input logic 			reset,
    // Header info + data i/f.
    input logic [DS_DCU_ID_WIDTH-1:0] 	hd_dcu_id_i,
    input logic [ECI_DMASK_WIDTH-1:0] 	hd_dmask_i,
    input logic [HDR_WIDTH-1:0] 	hd_hdr_i,
    input logic [ECI_CL_WIDTH-1:0] 	hd_data_i,
    input logic 			hd_valid_i,
    output logic 			hd_ready_o,
    // Output header i/f.
    output logic [DS_DCU_IDX_WIDTH-1:0] h_dcu_idx_o,
    output logic [HDR_WIDTH-1:0] 	h_hdr_o,
    output logic 			h_valid_o,
    input logic 			h_ready_i,
    // Output data i/f.
    output logic [DS_DCU_IDX_WIDTH-1:0] d_dcu_idx_o,
    output logic [ECI_CL_WIDTH-1:0] 	d_data_o,
    output logic 			d_valid_o,
    input logic 			d_ready_i,
    // clear signal to free up dbuf idx.
    input logic [DS_DCU_IDX_WIDTH-1:0] 	c_dcu_idx_i,
    input logic 			c_dcu_idx_valid_i
    );

   typedef logic [DS_DCU_IDX_WIDTH-1:0] dcu_idx_t;
   typedef logic [DS_DCU_ID_WIDTH-1:0] 	dcu_id_t;

   dcu_id_t  hdr_dcu_id;
   dcu_idx_t curr_dcu_idx;
   logic dcu_in_use;
   logic h_hs, d_hs;
   logic hd_ready;
   
   // DCU IDX valid ram.
   // 1 bit for each dcu idx.
   // 1 indicates op in progress, 0 indicates free.
   logic [DS_NUM_DCU_IDX-1:0] dcu_idx_valid_ram;
   logic 		      dcu_idx_valid_ram_we;
   
   // ECI data Map module signals.
   logic [ECI_CL_WIDTH-1:0]    m_eci_data_i;
   logic [ECI_DMASK_WIDTH-1:0] m_eci_dmask_i;
   logic [ECI_CL_WIDTH-1:0]    m_eci_data_o;

   // Registers.
   enum  {COMPARE,LAUNCH} state_reg, state_next;
   logic h_valid_reg = '0, h_valid_next;
   logic d_valid_reg = '0, d_valid_next;

   assign hdr_dcu_id = hd_dcu_id_i;
   assign curr_dcu_idx = f_dcuid_2_dcuidx(.dcu_id_i(hdr_dcu_id));
   assign dcu_in_use = dcu_idx_valid_ram[curr_dcu_idx];
   assign h_hs = h_valid_o & h_ready_i;
   assign d_hs = d_valid_o & d_ready_i;

   // Output assign.
   assign h_dcu_idx_o = curr_dcu_idx;
   assign h_hdr_o = hd_hdr_i;
   assign h_valid_o = h_valid_reg;
   assign d_dcu_idx_o = curr_dcu_idx;
   assign d_data_o = m_eci_data_o;
   assign d_valid_o = d_valid_reg;
   assign hd_ready_o = hd_ready;
   
   // map_ecid_to_wrd module needs to be
   // instantiated for write requests, no
   // need to instantiate for read response.
   // hence a parameter to instantiate or not.
   generate
      if(GEN_MAP_ECID_TO_WRD == 1) begin
	 // Map data from ECI packet to appropriate
	 // byte address depending on the dmask.
	 // combinational circuit.
	 always_comb begin : MAP_ASSIGN
	    m_eci_data_i = hd_data_i;
	    m_eci_dmask_i = hd_dmask_i;
	 end : MAP_ASSIGN
	 map_ecid_to_wrd #
	   (
	    .DMASK_WIDTH(ECI_DMASK_WIDTH),
	    .SLOT_WIDTH(ECI_SCL_DATA_WIDTH)
	    )
	 ecid_to_wrd_i1
	   (
	    .eci_slot_i(m_eci_data_i),
	    .dmask_i(m_eci_dmask_i),
	    .wr_slot_o(m_eci_data_o),
	    .wrd_byte_strobe_o() // not connected on purpose.
	    );
      end else begin
	 // No need to instantiate map module.
	 always_comb begin : NO_MAP_ASSIGN
	    m_eci_data_i = hd_data_i;
	    m_eci_dmask_i = hd_dmask_i;
	    m_eci_data_o = m_eci_data_i;
	 end : NO_MAP_ASSIGN
      end
   endgenerate

   // State machine with 2 states COMPARE and LAUNCH.
   //
   // COMPARE state:
   // when input hdr and data is recd, the
   // valid ram is checked to see if corresponding
   // dcu has an operation in progress. (dcu in use).
   // If the DCU is in use, wait for clear signal,
   // which will be eventually recd when the operation
   // completes.
   // If DCU is not in use, split the hdr and data
   // into corresponding op channels and launch them
   // after marking the DCU to be in use.
   //
   // LAUNCH state:
   // wait for handshakes in either or both of the
   // op i/fs. when hanshakes are recd in both op
   // i/fs, send ready to ip i/f and start COMPARING
   // again.
   always_comb begin : CONTROLLER
      state_next = state_reg;
      h_valid_next = h_valid_reg;
      d_valid_next = d_valid_reg;
      dcu_idx_valid_ram_we = 1'b0;
      hd_ready = 1'b0;
      case(state_reg)
	COMPARE: begin
	   //assert(h_valid_o === 1'b0) else 
	   //  $error("HDR valid should be 0 (instance %m)");
	   //assert(d_valid_o === 1'b0) else 
	   //  $error("Data valid should be 0 (instance %m)");
	   if(hd_valid_i == 1'b1) begin
	      if(dcu_in_use) begin
		 state_next = COMPARE;
	      end else begin
		 state_next = LAUNCH;
		 h_valid_next = 1'b1;
		 d_valid_next = 1'b1;
		 dcu_idx_valid_ram_we = 1'b1;
	      end
	   end
	end
	LAUNCH: begin
	   if(h_hs & d_hs) begin
	      // Both op ifs have handshakes this cycle.
	      // send ready to input. 
	      h_valid_next = 1'b0;
	      d_valid_next = 1'b0;
	      hd_ready = 1'b1;
	      state_next = COMPARE;
	   end else if(h_hs) begin
	      // only hdr i/f has handshake this cycle.
	      // check if handshake already happened in
	      // data i/f.
	      h_valid_next = 1'b0;
	      if(~d_valid_o) begin
		 // data i/f hs has happened before.
		 // send ready to ip.
		 hd_ready = 1'b1;
		 state_next = COMPARE;
	      end // else wait for hs in data i/f.
	   end else if (d_hs) begin
	      // hs happens in data i/f this cycle.
	      // check if hs already happened in
	      // hdr i/f.
	      d_valid_next = 1'b0;
	      if(~h_valid_o) begin
		 // handshake already happened in
		 // hdr i/f before, send ready to ip.
		 hd_ready = 1'b1;
		 state_next = COMPARE;
	      end // else wait for hs in hdr i/f.
	   end
	end
      endcase
   end : CONTROLLER

   // DCU IDX Valid RAM.
   // If clear signal is recd for an index, clear it.
   // if write signal is recd for an index, write it.
   //    write index is always got from ip hdr.
   //if reset all valid bits are set to 0s.
   always_ff @(posedge clk) begin : DCU_IDX_VALID_RAM_FF
      if(c_dcu_idx_valid_i) begin
	 dcu_idx_valid_ram[c_dcu_idx_i] <= 1'b0;
      end
      if(dcu_idx_valid_ram_we) begin
	 dcu_idx_valid_ram[curr_dcu_idx] <= 1'b1;
      end
      if(reset) begin
	 dcu_idx_valid_ram <= '0;
      end
   end : DCU_IDX_VALID_RAM_FF

   always_ff @(posedge clk) begin : REG_ASSIGN
      state_reg  <= state_next;
      h_valid_reg	<= h_valid_next;
      d_valid_reg	<= d_valid_next;
      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset
	 state_reg <= COMPARE;
	 h_valid_reg	<= '0;
	 d_valid_reg	<= '0;
      end
   end : REG_ASSIGN
   
endmodule // dp_gate


`endif
