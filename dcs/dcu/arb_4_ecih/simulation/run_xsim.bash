#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
      $XILINX_VIVADO/data/verilog/src/glbl.v \
      ../rtl/eci_cmd_defs.sv \
      ../testbench/arb_4_ecihTb.sv \
      ../rtl/arb_4_ecih.sv \
      ../rtl/arb_2_ecih.sv \
      ../rtl/arb_3_ecih.sv

xelab -debug typical -incremental -L xpm worklib.arb_4_ecihTb worklib.glbl -s worklib.arb_4_ecihTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.arb_4_ecihTb 
xsim -gui worklib.arb_4_ecihTb 
