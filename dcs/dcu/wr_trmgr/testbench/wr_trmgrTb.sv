
module wr_trmgrTb();

   parameter ID_WIDTH = 7;
   parameter META_DATA_WIDTH = 64;
   parameter ADDR_WIDTH = 38;
   parameter DATA_WIDTH = 1024;
   parameter DMASK_WIDTH = 4;
   parameter DATA_STRB_WIDTH = (DATA_WIDTH/8);

   localparam NUM_SCLS = DMASK_WIDTH;
   localparam SCL_WIDTH = 256;
   localparam SCL_BSTRB_WIDTH = SCL_WIDTH/8;

   //input output ports 
   //Input signals
   logic 		       clk;
   logic 		       reset;
   logic [ID_WIDTH-1:0]        desc_id_i;
   logic [META_DATA_WIDTH-1:0] desc_meta_i;
   logic [ADDR_WIDTH-1:0]      desc_addr_i;
   logic [DMASK_WIDTH-1:0]     desc_dmask_i;
   logic 		       desc_valid_i;
   logic 		       req_ready_i;
   logic [ID_WIDTH-1:0]        rsp_id_i;
   logic [1:0] 		       rsp_bresp_i;
   logic 		       rsp_valid_i;
   logic 		       desc_ready_i;

   //Output signals
   logic 		       desc_ready_o;
   logic [ID_WIDTH-1:0]        req_id_o;
   logic [ADDR_WIDTH-1:0]      req_addr_o;
   logic [DATA_STRB_WIDTH-1:0] req_strb_o;
   logic 		       req_valid_o;
   logic 		       rsp_ready_o;
   logic [ID_WIDTH-1:0]        desc_id_o;
   logic [META_DATA_WIDTH-1:0] desc_meta_o;
   logic [1:0] 		       desc_bresp_o;
   logic 		       desc_valid_o;


   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   wr_trmgr #
     (
      .ID_WIDTH(ID_WIDTH),
      .META_DATA_WIDTH(META_DATA_WIDTH),
      .ADDR_WIDTH(ADDR_WIDTH),
      .DATA_WIDTH(DATA_WIDTH),
      .DMASK_WIDTH(DMASK_WIDTH),
      .DATA_STRB_WIDTH(DATA_STRB_WIDTH)
      )
   wr_trmgr1 (
	      .clk(clk),
	      .reset(reset),
	      
	      .desc_id_i(desc_id_i),
	      .desc_meta_i(desc_meta_i),
	      .desc_addr_i(desc_addr_i),
	      .desc_dmask_i(desc_dmask_i),
	      .desc_valid_i(desc_valid_i),
	      .desc_ready_o(desc_ready_o),

	      .req_id_o(req_id_o),
	      .req_addr_o(req_addr_o),
	      .req_strb_o(req_strb_o),
	      .req_valid_o(req_valid_o),
	      .req_ready_i(req_ready_i),
	      
	      .rsp_id_i(rsp_id_i),
	      .rsp_bresp_i(rsp_bresp_i),
	      .rsp_valid_i(rsp_valid_i),
	      .rsp_ready_o(rsp_ready_o),
	      
	      .desc_id_o(desc_id_o),
	      .desc_meta_o(desc_meta_o),
	      .desc_bresp_o(desc_bresp_o),
	      .desc_valid_o(desc_valid_o),
	      .desc_ready_i(desc_ready_i)
	      );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports 
      clk = '0;
      reset = '1;
      desc_id_i = '0;
      desc_meta_i = '0;
      desc_addr_i = '0;
      desc_dmask_i = '0;
      desc_valid_i = '0;
      req_ready_i = '0;
      rsp_id_i = '0;
      rsp_bresp_i = '0;
      rsp_valid_i = '0;
      desc_ready_i = '0;

      ##5;
      reset = 1'b0;

      ##5;
      // Initiate write transaction.
      desc_id_i = 'd25;
      desc_meta_i = 'd64;
      desc_addr_i = 'd72;
      desc_dmask_i = '1;
      desc_valid_i = 1'b1;
      req_ready_i = 1'b1;
      desc_ready_i = 1'b1;
      wait(desc_valid_i & desc_ready_o);
      #1;
      assert(req_id_o == desc_id_i) else $error("req_id_o should be %d", desc_id_i);
      assert(req_addr_o == desc_addr_i) else $error("req_addr_o does not match desc_addr_i");
      assert(rsp_ready_o == 1'b0) else $error("Req accept mode rsp_ready_o should be 0");
      assert(desc_valid_o == 1'b0) else $error("Req accept mode desc_valid_o should be 0");
      assert(req_strb_o == get_strb(desc_dmask_i)) else $error("Dmask error");
      ##1;
      desc_valid_i = 1'b0;

      // get write response.
      rsp_id_i = desc_id_i;
      rsp_bresp_i = 2'b01;
      rsp_valid_i = 1'b1;
      wait(rsp_valid_i & rsp_ready_o);
      #1;
      assert(desc_id_o == rsp_id_i) else $error("desc_id_o does not match desc_id_i");
      assert(desc_meta_o == desc_meta_i) else $error("desc_meta_o does not match desc_meta_i");
      assert(desc_ready_o == 1'b0) else $error("Rsp accept mode should not be ready to accept input descriptors");
      assert(req_valid_o == 1'b0) else $error("Rsp accept mode should not issue write requests");
      assert(desc_bresp_o == rsp_bresp_i) else $error("Error: bresp does not match (instance %m)");
      ##1;
      rsp_valid_i = 1'b0;

      #500 $finish;
   end 

   function automatic [DATA_STRB_WIDTH-1:0] get_strb
     (
      input logic [DMASK_WIDTH-1:0] dmask_i
      );
      return({{SCL_BSTRB_WIDTH{dmask_i[3]}},{SCL_BSTRB_WIDTH{dmask_i[2]}},{SCL_BSTRB_WIDTH{dmask_i[1]}},{SCL_BSTRB_WIDTH{dmask_i[0]}}});
   endfunction : get_strb
endmodule
