import eci_cmd_defs::*;
import eci_dcs_defs::*;

module dp_gateTb();

   parameter GEN_MAP_ECID_TO_WRD = 1;

   //input output ports 
   //Input signals
   logic 			clk;
   logic 			reset;
   logic [DS_DCU_ID_WIDTH-1:0] 	hd_dcu_id_i;
   logic [ECI_DMASK_WIDTH-1:0] 	hd_dmask_i;
   logic [ECI_WORD_WIDTH-1:0] 	hd_hdr_i;
   logic [ECI_CL_WIDTH-1:0] 	hd_data_i;
   logic 			hd_valid_i;
   logic 			h_ready_i;
   logic 			d_ready_i;
   logic [DS_DCU_IDX_WIDTH-1:0] c_dcu_idx_i;
   logic 			c_dcu_idx_valid_i;

   //Output signals
   logic 			hd_ready_o;
   logic [DS_DCU_IDX_WIDTH-1:0] h_dcu_idx_o;
   logic [ECI_WORD_WIDTH-1:0] 	h_hdr_o;
   logic 			h_valid_o;
   logic [DS_DCU_IDX_WIDTH-1:0] d_dcu_idx_o;
   logic [ECI_CL_WIDTH-1:0] 	d_data_o;
   logic 			d_valid_o;

   eci_word_t   hd_hdr_c;
   ds_cl_addr_t hd_addr_c;
   eci_dmask_t  hd_dmask;
   logic [ECI_CL_WIDTH-1:0] 	    exp_data;
   logic [DS_DBUF_IDX_WIDTH-1:0]    c_id;

   always_comb begin : E_HDR_ASSIGN
      hd_hdr_c = '0;
      hd_hdr_c.generic_cmd.dmask = hd_dmask;
      hd_hdr_c.eci_word[ECI_ADDR_WIDTH-1:0] = ECI_ADDR_WIDTH'(hd_addr_c);
   end : E_HDR_ASSIGN

   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking // cb

   generate
      if(GEN_MAP_ECID_TO_WRD == 1) begin
	 map_ecid_to_wrd #
	   (
	    .DMASK_WIDTH(ECI_DMASK_WIDTH),
	    .SLOT_WIDTH(ECI_SCL_DATA_WIDTH)
	    )
	 ecid_to_wrd_test
	   (
	    .eci_slot_i(hd_data_i),
	    .dmask_i(hd_dmask),
	    .wr_slot_o(exp_data),
	    .wrd_byte_strobe_o() // not connected on purpose.
	    );
      end else begin
	 assign exp_data = hd_data_i;
      end
   endgenerate

   //instantiation
   always_comb begin : DP_GATE_IP_ASSIGN
      hd_dmask_i = hd_hdr_c.generic_cmd.dmask;
      hd_hdr_i = hd_hdr_c.eci_word;
      hd_dcu_id_i = hd_addr_c.dcu_id;
   end : DP_GATE_IP_ASSIGN
   dp_gate dp_gate1 (
		     .clk(clk),
		     .reset(reset),
		     .hd_dcu_id_i(hd_dcu_id_i),
		     .hd_dmask_i(hd_dmask_i),
		     .hd_hdr_i(hd_hdr_i),
		     .hd_data_i(hd_data_i),
		     .hd_valid_i(hd_valid_i),
		     .hd_ready_o(hd_ready_o),
		     .h_dcu_idx_o(h_dcu_idx_o),
		     .h_hdr_o(h_hdr_o),
		     .h_valid_o(h_valid_o),
		     .h_ready_i(h_ready_i),
		     .d_dcu_idx_o(d_dcu_idx_o),
		     .d_data_o(d_data_o),
		     .d_valid_o(d_valid_o),
		     .d_ready_i(d_ready_i),
		     .c_dcu_idx_i(c_dcu_idx_i),
		     .c_dcu_idx_valid_i(c_dcu_idx_valid_i)
		     );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports 
      clk = '0;
      reset = '1;
      //hd_dcu_id_i = '0;
      //hd_dmask_i = '0;
      //hd_hdr_i = '0;
      hd_data_i = '0;
      hd_valid_i = '0;
      h_ready_i = '0;
      d_ready_i = '0;
      c_dcu_idx_i = '0;
      c_dcu_idx_valid_i = '0;
      hd_addr_c = '0;
      hd_dmask = '1;
      ##5;
      reset = 1'b0;
      h_ready_i = '1;
      d_ready_i = '1;
      ##1;
      
      assert(dp_gate1.dcu_idx_valid_ram === '0) else
	$fatal(1,"valid ram should be all 0 after reset %d.", dp_gate1.dcu_idx_valid_ram);
      ##1;
      
      hd_addr_c.tag = $urandom_range(1,11);
      hd_addr_c.set_within_dcu = $urandom_range(11,20);
      hd_addr_c.byte_offset = 'd0;

            // Launch 2 packets to same DCU back to back
      // second packet should be stalled till clear
      // signal is receievd.
      launch_e_tr(.dcu_id_i(DS_DCU_ID_WIDTH'('d0)));
      launch_e_tr_when_full(.dcu_id_i('d0));
      clear_tr(.dcu_id_i('d0));


      // Check handshakes signals.
      test_vr_hs(.dcu_id_i('d0));
      
      // Fill all slots in DBUF.
      // Checks performed.
      //  1. Output header same as input header.
      //  2. Output data is mapped correctly to input data.
      //  3. buf idx is indeed marked as valid.
      //  5. buf idx sent in hdr and data ch match.
      // State machine checks:
      //  1. when all slots are full, new transactions
      //     should not be launched.
      //  2. when all slots are not full, if a dcu id
      //     has an on-going wr, it should not launch.
      // Valid ready flow control:
      //  1. each of ready signals go down make
      //     sure it performs as expected. 

      // Fill the data buffer.
      // Make sure each slot has a unique DCU ID.
      // Assuming sending to Even DCU slice.
      // (only even DCU IDs).
      for( integer i=0; i<DS_NUM_DCU; i=i+2 ) begin
	 launch_e_tr(.dcu_id_i(DS_DCU_ID_WIDTH'(i)));
      end 

      // all slots in data buffer is filled.
      assert(&dp_gate1.dcu_idx_valid_ram === 1'b1) else 
	$fatal(1,"All slots should be full.");
      
      // All slots are full, new transaction
      // should be launched only if one
      // of the slots become free. 
      launch_e_tr_when_full(.dcu_id_i('d0));

      ##5;
      $display("All tests completed");
      $display("IMPORTANT: flip GEN_MAP_ECID_TO_WRD parameter and run test again");
      
      #500 $finish;
   end 

   task static launch_e_tr_no_wait(
				   input logic [DS_DCU_ID_WIDTH-1:0] dcu_id_i
				   );
      hd_addr_c.dcu_id = dcu_id_i;
      hd_data_i = ECI_CL_WIDTH'($urandom_range(100,300));
      hd_valid_i = 1'b1;
   endtask //launch_e_tr_no_wait

   task static launch_e_tr(
			   input logic [DS_DCU_ID_WIDTH-1:0] dcu_id_i
			   );
      launch_e_tr_no_wait(.dcu_id_i(dcu_id_i));
      wait(hd_valid_i & hd_ready_o);
      #1;
      assert(h_hdr_o === hd_hdr_i) else 
	$fatal(1,"Hdr mismatch: Expected %h, Got %h", 
	       hd_hdr_i, h_hdr_o);
      assert(d_data_o === exp_data) else 
	$fatal(1,"Data mismatch: Expected %d, Got %d", 
	       exp_data, d_data_o);
      assert(d_dcu_idx_o === f_dcuid_2_dcuidx(.dcu_id_i(dcu_id_i))) 
	else $fatal(1, "DCU IDX %d does not match expected %d",
		    d_dcu_idx_o, f_dcuid_2_dcuidx(.dcu_id_i(dcu_id_i)));
      assert(dp_gate1.dcu_idx_valid_ram[d_dcu_idx_o] === 1'b1) else
	$fatal(1,"Buf idx %d is not marked valid", d_dcu_idx_o);
      assert(h_dcu_idx_o === d_dcu_idx_o) else 
	$fatal(1,"Hdr buf idx %d does not match data buf idx %d",
	       h_dcu_idx_o, d_dcu_idx_o);
      ##1;
      hd_valid_i = 1'b0;
      ##1;
   endtask //launch_e_tr

   task static launch_e_tr_when_full(
				     input logic [DS_DCU_ID_WIDTH-1:0] dcu_id_i
				     );
      // new entry should not be allowed till
      // clear signal is recd.
      // launch new transaction, wait for a bit
      // then send clear signal to check if the
      // new transaction is consumed.
      launch_e_tr_no_wait(.dcu_id_i(dcu_id_i));
      for( integer i=0; i<5; i=i+1 ) begin
	 assert(hd_ready_o !== 1'b1) else 
	   $fatal(1,"ready should not be asserted.");
	 ##1;
      end
      clear_tr(.dcu_id_i(dcu_id_i));
      wait(hd_valid_i & hd_ready_o);
      ##1;
      hd_valid_i = 1'b0;
   endtask //launch_e_tr_when_full

   task static clear_tr(
			input logic [DS_DCU_ID_WIDTH-1:0] dcu_id_i
			);
      c_dcu_idx_i = f_dcuid_2_dcuidx(.dcu_id_i(dcu_id_i)) ;
      c_dcu_idx_valid_i = 1'b1;
      ##1;
      c_dcu_idx_valid_i = 1'b0;
      assert(dp_gate1.dcu_idx_valid_ram[c_dcu_idx_i] === 1'b0) 
	else $fatal(1, "Expected DCU IDX %d to be cleared", c_dcu_idx_i);
      #1;
   endtask //clear_tr

   task static test_vr_hs(
			  input logic [DS_DCU_ID_WIDTH-1:0] dcu_id_i
			  );
      h_ready_i = 1'b0;
      launch_e_tr_no_wait(.dcu_id_i(dcu_id_i));
      for( integer i=0; i<5; i=i+1 ) begin
	 assert(hd_ready_o !== 1'b1) else 
	   $fatal(1,"ready should not be asserted.");
	 ##1;
      end
      h_ready_i = 1'b1;
      wait(hd_valid_i & hd_ready_o);
      ##1;
      hd_valid_i = 1'b0;
      ##1;
      clear_tr(.dcu_id_i(dcu_id_i));
      ##5;
      
      d_ready_i = 1'b0;
      launch_e_tr_no_wait(.dcu_id_i(dcu_id_i));
      for( integer i=0; i<5; i=i+1 ) begin
	 assert(hd_ready_o !== 1'b1) else 
	   $fatal(1,"ready should not be asserted.");
	 ##1;
      end
      d_ready_i = 1'b1;
      wait(hd_valid_i & hd_ready_o);
      ##1;
      hd_valid_i = 1'b0;
      ##1;
      clear_tr(.dcu_id_i(dcu_id_i));
      ##5;
      
      h_ready_i = 1'b0;
      d_ready_i = 1'b0;
      launch_e_tr_no_wait(.dcu_id_i('d0));
      for( integer i=0; i<5; i=i+1 ) begin
	 assert(hd_ready_o !== 1'b1) else 
	   $fatal(1,"ready should not be asserted.");
	 ##1;
      end
      h_ready_i = 1'b1;
      d_ready_i = 1'b1;
      wait(hd_valid_i & hd_ready_o);
      ##1;
      hd_valid_i = 1'b0;
      ##1;
      clear_tr(.dcu_id_i(dcu_id_i));
   endtask //test_vr_hs
endmodule
