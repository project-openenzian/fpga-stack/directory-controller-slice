import eci_cmd_defs::*;
import eci_dcs_defs::*;

`timescale 1ps/1ps
parameter CLK_PERIOD = 20; // 20ps.
parameter SYS_CLK_PERIOD = 3100; // 3100 ps or 3.1ns. 

// Parameters to vary for tput.
//
// Delay if there is an arbitration module
// ARB_SIM_DELAY 0 means no arbitration delays.
parameter ARB_SIM_DELAY = 0;
// This parameter is for modeling DCU delay.
// DCU[0] would have a delay of DCU_SIM_DELAY[0]
// DCU[1] would have a delay of DCU_SIM_DELAY[1]
// DCU[8] would round back to DCU_SIM_DELAY[0]
// DCU_SIM_DELAY[0] is right most value. 
parameter integer DCU_SIM_DELAY [7:0] = 
		  {
		   //3,6,1,5,1,4,2,6
		   //3,3,3,3,3,3,3,3
		   //1,1,1,1,1,1,1,1
		   //5,5,5,5,5,5,5,5
		   //20,20,20,20,20,20,20,20
		   3,20,0,25,16,12,5,18
		   };

module dp_gen_pathTb();

   parameter STORE_HDR_WIDTH = 64;
   parameter RETRIEVE_HDR_WIDTH = 64;
   parameter GEN_MAP_ECID_TO_WRD = 1;

   //input output ports 
   //Input signals
   logic 			  clk;
   logic 			  reset;
   logic [DS_DCU_ID_WIDTH-1:0] 	  st_dcu_id_i;
   logic [ECI_DMASK_WIDTH-1:0] 	  st_dmask_i;
   logic [STORE_HDR_WIDTH-1:0] 	  st_hdr_i;
   logic [ECI_CL_WIDTH-1:0] 	  st_data_i;
   logic 			  st_valid_i;
   logic 			  sth_ready_i;
   logic [DS_DCU_ID_WIDTH-1:0] 	  rth_dcu_id_i;
   logic [RETRIEVE_HDR_WIDTH-1:0] rth_hdr_i;
   logic 			  rth_valid_i;
   logic 			  rt_ready_i;

   //Output signals
   logic 			  st_ready_o;
   logic [DS_DCU_IDX_WIDTH-1:0]   sth_dcu_idx_o;
   logic [STORE_HDR_WIDTH-1:0] 	  sth_hdr_o;
   logic 			  sth_valid_o;
   logic 			  rth_ready_o;
   logic [RETRIEVE_HDR_WIDTH-1:0] rt_hdr_o;
   logic [ECI_CL_WIDTH-1:0] 	  rt_data_o;
   logic 			  rt_valid_o;

   logic [DS_DCU_IDX_WIDTH-1:0]   ds_req_dcu_idx_i;
   logic [STORE_HDR_WIDTH-1:0] 	  ds_req_hdr_i;
   logic 			  ds_req_valid_i;
   logic 			  ds_req_ready_o;
   logic [DS_DCU_ID_WIDTH-1:0] 	  ds_rsp_dcu_id_o;
   logic [RETRIEVE_HDR_WIDTH-1:0] ds_rsp_hdr_o;
   logic 			  ds_rsp_valid_o;
   logic 			  ds_rsp_ready_i;

   logic [RETRIEVE_HDR_WIDTH-1:0] p_rt_hdr_o;
   logic [ECI_CL_WIDTH-1:0] 	  p_rt_data_o;
   logic 			  p_rt_valid_o;

   logic [DS_DCU_ID_WIDTH-1:0] 	  p_st_dcu_id_i;
   logic [ECI_DMASK_WIDTH-1:0] 	  p_st_dmask_i;
   logic [STORE_HDR_WIDTH-1:0] 	  p_st_hdr_i;
   logic [ECI_CL_WIDTH-1:0] 	  p_st_data_i;
   logic 			  p_st_valid_i;
   logic 			  p_st_ready_o;

   // Tput analysis time variables.
   realtime 			  time_st;
   realtime 			  time_end;
   realtime 			  time_sim_clk_period;
   realtime 			  time_sys_clk_period;
   real 			  tput_GiBps;
   bit 				  disp_tput_flag;
   
   // Signals to simulate arb delay
   logic [(DS_DCU_IDX_WIDTH+STORE_HDR_WIDTH)-1:0] arb_us_data_i;
   logic 					  arb_us_valid_i;
   logic 					  arb_us_ready_o;
   logic [(DS_DCU_IDX_WIDTH+STORE_HDR_WIDTH)-1:0] arb_ds_data_o;
   logic 					  arb_ds_valid_o;
   logic 					  arb_ds_ready_i;
   
   integer 			  num_pkts_recd;
   integer 			  num_pkts_sent;
   
   //Clock block comment if not needed
   always #(CLK_PERIOD/2) clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking // cb

   axis_pipeline_stage #
     (
      .DATA_WIDTH(DS_DCU_ID_WIDTH + ECI_DMASK_WIDTH + STORE_HDR_WIDTH + ECI_CL_WIDTH)
      )
   last_stage_pl1
     (
      .clk(clk),
      .reset(reset),
      .us_data({st_dcu_id_i, st_dmask_i, st_hdr_i, st_data_i}),
      .us_valid(st_valid_i),
      .us_ready(st_ready_o),
      .ds_data({p_st_dcu_id_i, p_st_dmask_i, p_st_hdr_i, p_st_data_i}),
      .ds_valid(p_st_valid_i),
      .ds_ready(p_st_ready_o)
      );

   always_comb begin : SIM_ARB_DELAY_IP_ASSIGN
      arb_us_data_i = {sth_dcu_idx_o, sth_hdr_o};
      arb_us_valid_i = sth_valid_o;
      arb_ds_ready_i = ds_req_ready_o;
   end : SIM_ARB_DELAY_IP_ASSIGN
   simple_delay_module #
     (
      .DATA_WIDTH(DS_DCU_IDX_WIDTH+STORE_HDR_WIDTH),
      .SIM_DELAY(ARB_SIM_DELAY)
       )
   simulate_arb_delay
     (
      .clk(clk),
      .reset(reset),
      .us_data_i (arb_us_data_i),
      .us_valid_i(arb_us_valid_i),
      .us_ready_o(arb_us_ready_o),
      .ds_data_o (arb_ds_data_o),
      .ds_valid_o(arb_ds_valid_o),
      .ds_ready_i(arb_ds_ready_i)
      );
   
   always_comb begin : DC_SLICE_SIM_IP_ASSIGN
      ds_req_dcu_idx_i = arb_ds_data_o[(DS_DCU_IDX_WIDTH+STORE_HDR_WIDTH-1):STORE_HDR_WIDTH];
      ds_req_hdr_i = arb_ds_data_o[STORE_HDR_WIDTH-1:0];
      ds_req_valid_i = arb_ds_valid_o;
      ds_rsp_ready_i = rth_ready_o;
   end : DC_SLICE_SIM_IP_ASSIGN

   dc_slice_sim #
     (
      .STORE_HDR_WIDTH(STORE_HDR_WIDTH),
      .RETRIEVE_HDR_WIDTH(RETRIEVE_HDR_WIDTH),
      .DCU_SIM_DELAY(DCU_SIM_DELAY)
       )
   dc_slice_inst
     (
      .clk(clk),
      .reset(reset),
      .req_dcu_idx_i(ds_req_dcu_idx_i),
      .req_hdr_i(ds_req_hdr_i),
      .req_valid_i(ds_req_valid_i),
      .req_ready_o(ds_req_ready_o),
      .rsp_dcu_id_o(ds_rsp_dcu_id_o),
      .rsp_hdr_o(ds_rsp_hdr_o),
      .rsp_valid_o(ds_rsp_valid_o),
      .rsp_ready_i(ds_rsp_ready_i)
      );

   // Send store header to retrieve it as well.
   always_comb begin : DP_GEN_PATH_IP_ASSIGN
      rth_dcu_id_i = ds_rsp_dcu_id_o;
      rth_hdr_i = ds_rsp_hdr_o;
      rth_valid_i = ds_rsp_valid_o;
      sth_ready_i = arb_us_ready_o;
   end : DP_GEN_PATH_IP_ASSIGN

   dp_gen_path #
     (
      .STORE_HDR_WIDTH(STORE_HDR_WIDTH),
      .RETRIEVE_HDR_WIDTH(RETRIEVE_HDR_WIDTH),
      .GEN_MAP_ECID_TO_WRD(GEN_MAP_ECID_TO_WRD)
      )
   dp_gen_path1 
     (
      .clk(clk),
      .reset(reset),
      // Store hdr+data ip i/f.
      .st_dcu_id_i(p_st_dcu_id_i),
      .st_dmask_i(p_st_dmask_i),
      .st_hdr_i(p_st_hdr_i),
      .st_data_i(p_st_data_i),
      .st_valid_i(p_st_valid_i),
      .st_ready_o(p_st_ready_o),
      // Store hdr op i/f.
      .sth_dcu_idx_o(sth_dcu_idx_o),
      .sth_hdr_o(sth_hdr_o),
      .sth_valid_o(sth_valid_o),
      .sth_ready_i(sth_ready_i),
      // Retrieve hdr ip i/f.
      .rth_dcu_id_i(rth_dcu_id_i),
      .rth_hdr_i(rth_hdr_i),
      .rth_valid_i(rth_valid_i),
      .rth_ready_o(rth_ready_o),
      // retrieve hdr+data op i/f.
      .rt_hdr_o(rt_hdr_o),
      .rt_data_o(rt_data_o),
      .rt_valid_o(rt_valid_o),
      .rt_ready_i(rt_ready_i)
      );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();
      
      //https://stackoverflow.com/questions/27522426/calculation-of-simulation-time-in-verilog
      //$printtimescale();
      $timeformat(-12, 0, " ps", 10);
      time_sim_clk_period = CLK_PERIOD;
      time_sys_clk_period = SYS_CLK_PERIOD;
      
      // Initialize Input Ports 
      clk = '0;
      reset = '1;
      st_dcu_id_i = '0;
      st_dmask_i = '0;
      st_hdr_i = '0;
      st_data_i = '0;
      st_valid_i = '0;
      rt_ready_i = '0;


      // In each of the test cases below, the data is always hdr*10.
      // this is done to check if correct data retrieved matches the
      // header.
      
      // Test case 1: Send a packet to each of the DCU sequentially.
      ##5;
      reset = 1'b0;
      ##5;
      $display("\nSending a packet to each of %0d DCUs", DS_NUM_DCU_IDX);
      for( integer i=0; i<DS_NUM_DCU; i=i+2 ) begin
	 st_dcu_id_i = i;
	 st_dmask_i = '1;
	 st_hdr_i = (i+1)*10;
	 st_data_i = (i+1)*100;
	 st_valid_i = '1;
	 rt_ready_i = '1;
	 wait(st_valid_i & st_ready_o);
	 ##1;
	 //st_valid_i = 1'b0;
      end
      st_valid_i = 1'b0;
      #5000;

      // Test case 2: serial test case.
      // Send DS_NUM_DCU_IDX packets to same DCU.
      // reseting so throughput numbers would be shown.
      reset = 1'b1;
      ##5;
      reset = 1'b0;
      st_dcu_id_i = 'd10; // DCU_ID of 10 means DCU_IDX of 5
      $display("\nSending %0d packets to the same DCU[%0d]", DS_NUM_DCU_IDX, f_dcuid_2_dcuidx(st_dcu_id_i) );
      for( integer i=0; i<DS_NUM_DCU; i=i+2 ) begin
	 //st_dcu_id_i = 0;
	 st_dmask_i = '1;
	 st_hdr_i = (i+1)*10;
	 st_data_i = (i+1)*100;
	 st_valid_i = '1;
	 rt_ready_i = '1;
	 wait(st_valid_i & st_ready_o);
	 ##1;
      end
      st_valid_i = 1'b0;
      #5000;

      // Test case 3: serial test case
      // Send DS_NUM_DCU_IDX packets to same DCU.
      // reseting so throughput numbers would be shown.
      reset = 1'b1;
      ##5;
      reset = 1'b0;
      st_dcu_id_i = 'd8; // DCU_ID of 8 means DCU_IDX of 4
      $display("\nSending %0d packets to the same DCU[%0d]", DS_NUM_DCU_IDX, f_dcuid_2_dcuidx(st_dcu_id_i) );
      for( integer i=0; i<DS_NUM_DCU; i=i+2 ) begin
	 //st_dcu_id_i = 0;
	 st_dmask_i = '1;
	 st_hdr_i = (i+1)*10;
	 st_data_i = (i+1)*100;
	 st_valid_i = '1;
	 rt_ready_i = '1;
	 wait(st_valid_i & st_ready_o);
	 ##1;
      end
      st_valid_i = 1'b0;
      #5000;

      // Test case 4: random test case. 
      // Send DS_NUM_DCU_IDX packets to random DCUs.
      // reseting so throughput numbers would be shown.
      reset = 1'b1;
      ##5;
      reset = 1'b0;
      $display("\nSending %0d packets to random DCUs", DS_NUM_DCU_IDX);
      for( integer i=0; i<DS_NUM_DCU; i=i+2 ) begin
	 st_dcu_id_i = $urandom_range(0, DS_NUM_DCU-1);
	 $display("Sending to DCU_IDX[%0d]", f_dcuid_2_dcuidx(st_dcu_id_i));
	 st_dmask_i = '1;
	 st_hdr_i = (i+1)*10;
	 st_data_i = (i+1)*100;
	 st_valid_i = '1;
	 rt_ready_i = '1;
	 wait(st_valid_i & st_ready_o);
	 ##1;
      end
      st_valid_i = 1'b0;
      #5000 $finish;

   end

   always_ff @(posedge clk) begin : TEST
      if(rt_valid_o & rt_ready_i) begin
	 assert(rt_hdr_o*10 === rt_data_o) else 
	   $fatal(1,"Expected %0d, Got %0d",rt_hdr_o*10, rt_data_o);
	 if(time_st == 0) begin
	    time_st = $realtime;
	 end

	 // Calculate tput after receiving all packets from
	 // DS_NUM_DCU_IDX DCUs. Each packet is 128 Bytes. 
	 if(num_pkts_recd == DS_NUM_DCU_IDX-1) begin
	    time_end = $realtime;
	    // (32x128Bytes/time_in_ps) * (10^12ps/1024^3Gi)  GiB/s
	    tput_GiBps = (DS_NUM_DCU_IDX*128)/((time_end-time_st)*time_sys_clk_period/time_sim_clk_period) * (10**12 / 1024**3);
	    if(disp_tput_flag == 1'b0) begin
	       $display("Throughput: %f GiB/s\n", tput_GiBps);
	       disp_tput_flag <= 1'b1;
	    end
	    
	    num_pkts_recd <= num_pkts_recd;
	 end else begin
	    num_pkts_recd <= num_pkts_recd + 1;
	 end
	 
      end
      if(st_valid_i & st_ready_o) begin
	 //$display("Sending Hdr: %d, Data: %d", st_hdr_i, st_data_i);
	 //$display("Time %t", $realtime);
	 //$display("Simulation Delay for DCU_ID %0d is %0d", st_dcu_id_i,
		//  dc_slice_inst.DCU_SIM_DELAY[st_dcu_id_i[DS_DCU_ID_WIDTH-1:1]]);
      end

      if(reset) begin
	 time_st <= '0;
	 time_end <= '0;
	 num_pkts_recd <= '0;
	 num_pkts_sent <= '0;
	 disp_tput_flag <= 0;
      end
   end : TEST


endmodule
