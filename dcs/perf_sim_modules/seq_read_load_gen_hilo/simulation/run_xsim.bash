#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
      $XILINX_VIVADO/data/verilog/src/glbl.v \
      ../rtl/eci_cmd_defs.sv \
      ../testbench/seq_read_load_genTb.sv \
      ../rtl/seq_read_load_gen.sv 

xelab -debug typical -incremental -L xpm worklib.seq_read_load_genTb worklib.glbl -s worklib.seq_read_load_genTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.seq_read_load_genTb 
xsim -gui worklib.seq_read_load_genTb 
