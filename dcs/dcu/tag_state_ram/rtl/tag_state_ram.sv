/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-05-28
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */

`ifndef TAG_STATE_RAM_SV
`define TAG_STATE_RAM_SV

/*
 * Module Description:
 *  Tag State RAM that implements 1 BRAM to hold tag and corresponding states. 
 *  The module optimizes memory access for three operations
 *    1. writing tag and state. 
 *    2. reading multiple tags in parallel to get hit miss or random gstate. 
 *    3. reading state corresponding to a tag. 
 *
 * Input Output Description:
 *   Three interfaces, check comments on the interfaces for more details. 
 *
 * Architecture Description:
 *  1 BRAM 1kx36 + MUXes at input of BRAM to choose operation. 
 *
 * Modifiable Parameters:
 *  None. 
 *
 * Non-modifiable Parameters:
 *  None. 
 * 
 * Modules Used:
 *  ram_tdp - instantiates a true dual port BRAM, the contents 
 *  of BRAM can be initialized upon reset. 
 *
 * Notes:
 *  any operation when ram_tdp is busy will be ignored. 
 *
 */



// BRAM Address space.    
//                                  1kx36 BRAM REGIONS
//                             35                        0
//                             ┌─────────────┬────────────┐
//                           0 │             │            │
//                             │   ROW_WAY_1 │ ROW_WAY_0  │
//                             │   18 bits   │  18 bits   │
//                             │             │            │
//                             │             │            │
//                             │       LO TAG REGION      │
//                             │             │            │
//                             │             │            │
//                        255  ├─────────────┼────────────┤
//                        256  │             │            │
//                             │             │            │
//                             │             │            │
//    TAG, HI, SET_ID  ───────►│//////////// │\\\\\\\\\\\\│
//    (TAG of SET_ID)          │       HI TAG│REGION      │
//    (1 tag per ROW_WAY)      │             │            │
//                             │             │            │
//                             │             │            │
//                        511  ├─────────────┼────────────┤
//                        512  │             │            │
//                             │             │            │
//                             │             │            │
//                             │      LO STATE REGION     │
//                             │             │            │
//                             │             │            │
//                             │             │            │
//                        767  ├─────────────┼────────────┤
//                        768  │             │            │
//                             │             │            │
// STATE, HI, SET_ID  ────────►│//////////// │\\\\\\\\\\\\│
// (STATES of SET_ID)          │             │            │
// (1 state per ROW_WAY)       │      HI STATE REGION     │
// states correspond to        │             │            │
// 2 tags with same set id     │             │            │
// in HI TAG region            │             │            │
//                       1023  └─────────────┴────────────┘
//
//
// * The lower half of BRAM has TAG information and upper half holds their STATES. 
// * each TAG and STATE region is further divided into LO/HI regions for access in parallel. 
// * 4 Regions, 2 regions for TAG (LO/HI) and 2 regions for STATE(LO/HI).
// *   SET_ID, WAY_ID is used to build the word address of a way. 
// *   SET_ID provides the row index within a region. 
// *   WAY_ID provides information on which region (LO/HI) as well as 
//     the word offset (which of the 2 ways in a row to read). 
// * 
// * For a given set, 2 ways are stored in the LO region and 2 ways are 
//   stored in the HI region at the same set_id. This allows 4 ways to be 
// * read in parallel for tag matching. 

/*
 * Detailed notes on operations:
 *  
 * 3 operations: Only one operation possible at a time. 
 *  1. Write tag and state (1st priority).
 *  2. Read tags (2nd priority): Read 4 tags for given {set, set_row} in parallel. 
 *  3. Read state (3rd priority). Read 1 state corresponding to a given {set, set_row}, way_id. 
 * 
 * 1 True dual port BRAM, reading and writing possible using both ports at the sametime.
 * BRAM configuration is 1kx36.
 * The 10 bit address space is split as follows
 *  MSB   (bit 9)     : differentiates between tag(0) and state(1) regions.
 *  MSB-1 (bit 8)     : splits both tag and state regions into HI(1) and LO(0).
 *  MSB-2:0 (bits 7:0): indicates the row within a region (set_id)
 *  Since tag and state words are both 18-bits wide, we can fit 2 words per row.
 * 
 * ┌───────────┬─────────────┬─────────┬─────────────┐
 * │ Tag/State │ HI/LO Region│ Set ID  │ Row_Way 0/1 │
 * │ 1 bit     │ 1 bit       │ 8 bits  │ 1 bit       │
 * └───────────┴─────────────┴─────────┴─────────────┘
 *  
 * │                                   │
 * │----BRAM-Address-(10-bits)---------│
 * │                                   │
 * 
 * BRAM Ports and their use.
 * Write:
 *  PORT A - write tag. 
 *  PORT B - write state. 
 * Read Tags:
 *  PORT A - read LO TAG region for 2 tags. 
 *  PORT B - read HI TAG region for 2 tags. 
 * Read State:
 *  PORT B - read state from STATE LO and HI regions. 
 *
 *
 * Write operation:
 *  18-bit input wr_tag_i and wr_state_i are written into the tag region and state region. 
 *  wr_Way_id_i gives two information 
 *   1. Write should happen to LO/HI region of TAG and STATE region. 
 *   2. Since 2 way slots are available per row, which row_way should be written to. 
 *  wr_set_id_i gives the row within TAG/STATE LO/HI region to be written to. 
 *  writing the invalid state will also mark the tag as invalid. 
 *  an all 1 tag is invalid tag.
 * In the BRAM - Port A is used to write tag and port B is used to write state in 1 cycles. 
 *
 * Read Tag operation:
 *  rd_tag_set_id_i refers to row within the TAG HI/LO region that is to be read to get tags.
 *  In BRAM - port A is used to read LO TAG region, port B is used to read HI TAG region.
 *  This results in 2 rows or 4 tags being read which are then output. 
 *  1 cycle to read 2 rows or 4 tags.
 * 
 * Read state operation 
 *   rd_stt_set_id_i indicates the row within the STATE HI/LO region to be read. 
 *   rd_stt_way_id_i gives 2 information 
 *    1. whether LO STATE or HI STATE region has to be read. 
 *    2. row_way which gives which of the two ways in the row is to be read. 
 * 
 * In general:
 *  Set ID gives: {set, set_row (row within set)}
 *  Way ID gives: {LO/HI region, row_way_0/1}
 */

module tag_state_ram #
  (
   parameter logic [17:0] INVALID_STATE = '0,
   parameter logic [17:0] INVALID_TAG = '1,
   // Add additional pipeline stage to output.
   // increases latency but improves timing.
   // set to 0 if timing issues are acceptable.
   parameter logic 	  OP_REGISTER = 1'b1
   )
   (
    input logic 	clk,
    input logic 	reset,

    // Write Tag, State
    // if write state is invalid, the tag written would be INVALID_TAG and not wr_tag_i.
    // set_id points to the row within a tag/state hi/lo region.
    // way_id provides HI/LO region and ROW_WAY to write to.
    input logic [17:0] 	wr_tag_i,
    input logic [17:0] 	wr_stt_i,
    input logic [7:0] 	wr_set_id_i,
    input logic [1:0] 	wr_way_id_i,
    input logic 	wr_en_i,

    // Read Tags and compare.
    //  Read 4 tags, 2 from HI region, 2 from LO region.
    //  set_id specifies the index within the HI/LO regions to read from.
    //  upon enable, the 4 tags are compared with the tag rd_tag_to_cmp_i.
    //  if there is a hit, the hit {HI/LO region, ROW_WAY_0/1} is returned in way_id.
    //  if there is a miss, and one of the ways is empty, the empty way_id is returned.
    //  if there is a miss and none of the ways are empty, 
    //   a random way is returned and set_id_full_o is 1.
    input logic [7:0] 	rd_tag_set_id_i,
    input logic [17:0] 	rd_tag_to_cmp_i,
    input logic 	rd_tag_en_i,
    output logic [1:0] 	rd_way_id_o,
    output logic 	hit_o,
    output logic 	set_id_full_o,
    // if the tag that is being compared is invalid, this signal will be set. 
    output logic 	rd_tag_nxm_o,
   
    // Read State
    //  set_id gives the row within the SET, HI/LO region to read.
    //  way_id gives whether region is HI or LO and the row_way. 
    input logic [7:0] 	rd_stt_set_id_i,
    input logic [1:0] 	rd_stt_way_id_i,
    input logic 	rd_stt_en_i,
   
    // Output read state.
    // 2 bit way id and 18 bit state.
    // way id gives LO/HI region, ROW_WAY_0/1.
    output logic [19:0] way_id_stt_o,

    // Tag State Ram busy
    // any enables when busy is ignored and cant be trusted. 
    output logic 	tsr_busy_o
    );

   localparam BRAM_ADDR_WIDTH  = 10;
   localparam BRAM_DATA_WIDTH  = 36;
   localparam BRAM_BYTE_WIDTH  = 9;
   localparam BRAM_WSTRB_WIDTH = BRAM_DATA_WIDTH/BRAM_BYTE_WIDTH;
   localparam WAY_ID_WIDTH = 2;
   localparam WAY_WIDTH = BRAM_DATA_WIDTH/2; //18
   
   localparam logic TAG = 1'b0;
   localparam logic STATE = ~TAG;
   localparam logic LO_REGION = 1'b0;
   localparam logic HI_REGION = ~LO_REGION;
   localparam logic ROW_WAY_0 = 1'b0;
   localparam logic ROW_WAY_1 = ~ROW_WAY_0;
   localparam logic ENABLE = 1'b1;
   localparam logic DISABLE = ~ENABLE;
   localparam logic HIT = 1'b1;
   localparam logic MISS = ~HIT;
   localparam logic FREE = 1'b1;
   localparam logic USED = ~FREE;
   localparam logic [BRAM_WSTRB_WIDTH-1:0] WE_ROW_WAY_0 = 4'b0011;
   localparam logic [BRAM_WSTRB_WIDTH-1:0] WE_ROW_WAY_1 = 4'b1100;
   localparam logic [BRAM_WSTRB_WIDTH-1:0] WE_DISABLE   = 4'b0000;
   
   // BRAM address 
   typedef union packed {
      logic [BRAM_ADDR_WIDTH-1:0] row;           // 10 BRAM row address. 
      struct packed{
	 logic tag_or_stt;                       // 1 tag or state region.
	 logic lo_or_hi;                         // 1 lo or hi region within tag or state region
	 logic [BRAM_ADDR_WIDTH-3:0] region_row; // 8 row within a region. 
      } fields;
   } bram_addr_t;

   typedef logic [BRAM_DATA_WIDTH-1:0] bram_data_t;

   // BRAM port interface.
   typedef struct packed{
      // Port A.
      logic [BRAM_ADDR_WIDTH-1:0]  addra;
      logic [BRAM_DATA_WIDTH-1:0]  dina;
      logic 			   ena;
      logic [BRAM_WSTRB_WIDTH-1:0] wea;
      logic [BRAM_DATA_WIDTH-1:0]  douta;
      // Port B.
      logic [BRAM_ADDR_WIDTH-1:0]  addrb;
      logic [BRAM_DATA_WIDTH-1:0]  dinb;
      logic 			   enb;
      logic [BRAM_WSTRB_WIDTH-1:0] web;
      logic [BRAM_DATA_WIDTH-1:0]  doutb;
   }bram_port_if_t;

   // Way ID fields. 
   typedef union packed {
      logic [WAY_ID_WIDTH-1:0] way_id;
      struct packed{
	 logic lo_or_hi;  // 1 lo or hi regions
	 logic row_way;   // 1 Row way 0 or 1. 
      }fields;
   } way_id_t;

   // Registered output signal (pipeline stage)
   logic [1:0] rd_way_id_reg	= '0, rd_way_id_next;
   logic       hit_reg		= '0, hit_next;
   logic       set_id_full_reg	= '0, set_id_full_next;
   logic       rd_tag_nxm_reg	= '0, rd_tag_nxm_next;
   logic [19:0] way_id_stt_reg	= '0, way_id_stt_next;
   logic 	tsr_busy_reg = '0, tsr_busy_next;
   
   // Local signals.
   bram_port_if_t tsr;
   logic tsr_busy;
   bram_addr_t wr_addra, wr_addrb;
   bram_data_t wr_dina, wr_dinb;
   logic [BRAM_WSTRB_WIDTH-1:0] wr_wea, wr_web;
   bram_addr_t rd_tag_addra, rd_tag_addrb;
   bram_addr_t rd_stt_addrb;
   way_id_t wr_way_id_c, rd_stt_way_id_c;
   logic [WAY_WIDTH-1:0] wr_tag_act;
   logic [3:0] 	hit_miss_way;
   logic [3:0] 	free_used_way;
   logic [3:0][1:0] rd_way_id;
   logic 	    any_hit;
   logic 	    any_free;
   logic [1:0] 	    hit_way_id;
   logic [1:0] 	    free_way_id;
   
   // Registers
   logic rd_stt_lo_or_hi_reg = LO_REGION, rd_stt_lo_or_hi_next;
   logic rd_stt_row_way_0_or_1_reg = ROW_WAY_0, rd_stt_row_way_0_or_1_next;
   logic [WAY_WIDTH-1:0] tag_to_cmp_reg = '0, tag_to_cmp_next;

   // Generate output register stage.
   // Adds 1 cycle latency for each read operation
   // of TSR.
   // Done to meet timing, set OP_REGISTER to 1'b0 
   // if timing issues are acceptable.
   generate
      if(OP_REGISTER == 1'b1) begin
	 always_comb begin : ASSIGN_REG_OP
	    // Assign output signals to registers. 
	    rd_way_id_o = rd_way_id_reg;
	    hit_o = hit_reg;
	    set_id_full_o = set_id_full_reg;
	    rd_tag_nxm_o = rd_tag_nxm_reg;
	    way_id_stt_o = way_id_stt_reg;
	    tsr_busy_o = tsr_busy | tsr_busy_reg;
	 end : ASSIGN_REG_OP
      end else begin
	 always_comb begin : ASSIGN_NOREG_OP
	    // Do not register output signals. 
	    rd_way_id_o = rd_way_id_next;
	    hit_o = hit_next;
	    set_id_full_o = set_id_full_next;
	    rd_tag_nxm_o = rd_tag_nxm_next;
	    way_id_stt_o = way_id_stt_next;
	    tsr_busy_o = tsr_busy;
	 end : ASSIGN_NOREG_OP
      end
   endgenerate
   
   always_comb begin : OUT_ASSIGN
      // default op next signal values. 
      rd_way_id_next = rd_way_id_reg;
      hit_next = hit_reg;
      set_id_full_next = set_id_full_reg;
      rd_tag_nxm_next = rd_tag_nxm_reg;
      way_id_stt_next = way_id_stt_reg;
      tsr_busy_next = tsr_busy_reg;


      // Assign op next signals.
      // TSR will be busy for 1 cycle after reading is enabled.
      // This is to allow data to be registered in the output
      // pipeline stage.
      tsr_busy_next = tsr_busy | rd_stt_en_i | rd_tag_en_i;
      rd_tag_nxm_next = (tag_to_cmp_reg == INVALID_TAG) ? 1'b1 : 1'b0;
      
      // Port A reads LO TAG region, lower 18 bits is ROW_WAY_0 and upper 18 bits is  ROW_WAY_1.
      // Port B reads HI TAG region, lower 18 bits is ROW_WAY_0, higher 18 bits is ROW_WAY_1.
      // idx 0: LO_REGION, ROW_WAY_0
      // idx 1: LO_REGION, ROW_WAY_1
      // idx 2: HI_REGION, ROW_WAY_0
      // idx 3: HI_REGION, ROW_WAY_1
      hit_miss_way[0] = (tsr.douta[WAY_WIDTH-1:0]        == tag_to_cmp_reg) ? HIT : MISS;
      hit_miss_way[1] = (tsr.douta[WAY_WIDTH+:WAY_WIDTH] == tag_to_cmp_reg) ? HIT : MISS;
      hit_miss_way[2] = (tsr.doutb[WAY_WIDTH-1:0]        == tag_to_cmp_reg) ? HIT : MISS;
      hit_miss_way[3] = (tsr.doutb[WAY_WIDTH+:WAY_WIDTH] == tag_to_cmp_reg) ? HIT : MISS;

      free_used_way[0] = (tsr.douta[WAY_WIDTH-1:0]        == INVALID_TAG) ? FREE : USED;
      free_used_way[1] = (tsr.douta[WAY_WIDTH+:WAY_WIDTH] == INVALID_TAG) ? FREE : USED;
      free_used_way[2] = (tsr.doutb[WAY_WIDTH-1:0]        == INVALID_TAG) ? FREE : USED;
      free_used_way[3] = (tsr.doutb[WAY_WIDTH+:WAY_WIDTH] == INVALID_TAG) ? FREE : USED;

      rd_way_id[0] = {LO_REGION,ROW_WAY_0};
      rd_way_id[1] = {LO_REGION,ROW_WAY_1};
      rd_way_id[2] = {HI_REGION,ROW_WAY_0};
      rd_way_id[3] = {HI_REGION,ROW_WAY_1};

      any_hit  = |hit_miss_way;
      any_free = |free_used_way;

      hit_way_id = {LO_REGION,ROW_WAY_0}; // default.
      for(integer k=0; k<4; k++) begin
	 if(hit_miss_way[k] == HIT) begin
	    hit_way_id = rd_way_id[k];
	 end
      end

      free_way_id = {LO_REGION,ROW_WAY_0}; // default.
      for(integer k=0; k<4; k++) begin
	 if(free_used_way[k] == FREE) begin
	    free_way_id = rd_way_id[k];
	 end
      end

      // Setting output way_id.
      // if there is a hit rd_way_id_next should equals hit way_id.
      // if there is a miss and there is a free way_id, rd_way_id_next equals free way_id.
      // if there is a miss and there are no free way_id, rd_way_id_next should be a random way_id.
      hit_next = any_hit;
      set_id_full_next = ~any_free;
      //rd_way_id_next = {LO_REGION,ROW_WAY_0};
      if(any_hit) begin
	 rd_way_id_next = hit_way_id;
      end else begin
	 // miss.
	 // free way must be available
	 // if free ways are not available,
	 // default value would be assigned.
	 rd_way_id_next = free_way_id;
      end
      
      // State is read in port B.
      // select the right way based on input requested ROW_WAY. 
      way_id_stt_next = {
		      rd_stt_lo_or_hi_reg,
		      rd_stt_row_way_0_or_1_reg,
		      (rd_stt_row_way_0_or_1_reg == ROW_WAY_0) ? 
		      tsr.doutb[WAY_WIDTH-1:0] :
		      tsr.doutb[WAY_WIDTH+:WAY_WIDTH]
		      };
   end : OUT_ASSIGN

   // Write tag and state operation: BRAM signals. 
   always_comb begin : BRAM_WRITE_SIGNALS
      // Write address. 
      // Port A is used to write tag and Port B is used to write state.
      //  tag and state BRAM address differs only in MSB bit which indicates TAG/STATE region.
      //  way_id gives lo/hi region and row_way to write to.
      //  set_id gives the row within a region to write to.
      wr_addra.row = '0;
      wr_addrb.row = '0;
      wr_way_id_c.way_id = wr_way_id_i;

      // Tag 
      wr_addra.fields.tag_or_stt = TAG;
      wr_addra.fields.lo_or_hi   = wr_way_id_c.fields.lo_or_hi;
      wr_addra.fields.region_row = wr_set_id_i;
      
      // State
      wr_addrb.fields.tag_or_stt = STATE;
      wr_addrb.fields.lo_or_hi   = wr_way_id_c.fields.lo_or_hi;
      wr_addrb.fields.region_row = wr_set_id_i;

      // if writing invalid state, mark the tag as invalid as well.
      if(wr_stt_i == INVALID_STATE) begin
	 wr_tag_act = INVALID_TAG;
      end else begin
	 wr_tag_act = wr_tag_i;
      end

      if(wr_way_id_c.fields.row_way == ROW_WAY_0) begin
	 // write Tag and state only to ROW_WAY_0
	 wr_dina = {18'b0, wr_tag_act};
	 wr_wea  = WE_ROW_WAY_0;
	 wr_dinb = {18'b0, wr_stt_i};
	 wr_web  = WE_ROW_WAY_0;
      end else begin
	 // write Tag and state only to ROW_WAY_1
	 wr_dina = {wr_tag_act, 18'b0};
	 wr_wea  = WE_ROW_WAY_1;
	 wr_dinb = {wr_stt_i, 18'b0};
	 wr_web  = WE_ROW_WAY_1;
      end
   end : BRAM_WRITE_SIGNALS

   // Read tag operation: BRAM signals.
   always_comb begin : BRAM_READ_TAG_SIGNALS
      // Read Tag address.
      // Both ports are used to read tag information.
      // Port A is for reading LO tag region and Port B for HI tag region.
      // Both ports read the same tag row in their corresponding regions.
      // a tag row contains 2 tag words, so 4 tag words are read in parallel. 
      rd_tag_addra.row = '0;
      rd_tag_addrb.row = '0;

      rd_tag_addra.fields.tag_or_stt = TAG;
      rd_tag_addra.fields.lo_or_hi   = LO_REGION;
      rd_tag_addra.fields.region_row = rd_tag_set_id_i;
      
      rd_tag_addrb.fields.tag_or_stt = TAG;
      rd_tag_addrb.fields.lo_or_hi   = HI_REGION;
      rd_tag_addrb.fields.region_row = rd_tag_set_id_i;

      if(rd_tag_en_i) begin
	 tag_to_cmp_next = rd_tag_to_cmp_i;
      end else begin
	 tag_to_cmp_next = tag_to_cmp_reg;
      end
   end : BRAM_READ_TAG_SIGNALS

   // Read state operation: BRAM signals
   //  Read from port B.
   //  Read the STATE region.
   //   within state region, way_id gives HI/LO region as well as ROW_WAY to read from.
   //   the row within the state region is given by the input set_id. 
   always_comb begin : BRAM_READ_STT_SIGNALS
      // Read state address.
      // Only Port B is used to read state. (no specific reason, port A also can be used).
      rd_stt_addrb.row = '0;
      rd_stt_way_id_c.way_id = rd_stt_way_id_i;
      
      rd_stt_addrb.fields.tag_or_stt = STATE;
      rd_stt_addrb.fields.lo_or_hi   = rd_stt_way_id_c.fields.lo_or_hi;
      rd_stt_addrb.fields.region_row = rd_stt_set_id_i;

      // Store these signals to send in output.
      if(rd_stt_en_i) begin
	 rd_stt_lo_or_hi_next       = rd_stt_way_id_c.fields.lo_or_hi;
	 rd_stt_row_way_0_or_1_next = rd_stt_way_id_c.fields.row_way;
      end else begin
	 rd_stt_lo_or_hi_next       = rd_stt_lo_or_hi_reg;
	 rd_stt_row_way_0_or_1_next = rd_stt_row_way_0_or_1_reg;
      end
   end : BRAM_READ_STT_SIGNALS

   // BRAM enable signals. 
   // 1st priority write tag and state.
   // 2nd priority read tags and compare.
   // 3rd priority read state. 
   always_comb begin : TSR_IN_MUX
      // Default values: Write tag and state. 
      tsr.addra = wr_addra.row;
      tsr.dina  = wr_dina;
      tsr.ena   = DISABLE;
      tsr.wea   = WE_DISABLE;

      tsr.addrb = wr_addrb.row;
      tsr.dinb  = wr_dinb;
      tsr.enb   = DISABLE;
      tsr.web   = WE_DISABLE;
      
      if(wr_en_i) begin
	 // by default write signals are connected to TSR ports, only needs enabling.
	 tsr.ena = ENABLE;
	 tsr.wea = wr_wea;
	 tsr.enb = ENABLE;
	 tsr.web = wr_web;
      end else if(rd_tag_en_i) begin
	 // Connect read tag signals to the port A, B
	 tsr.addra = rd_tag_addra.row;
	 tsr.ena   = ENABLE;
	 tsr.wea   = WE_DISABLE;

	 tsr.addrb = rd_tag_addrb.row;
	 tsr.enb   = ENABLE;
	 tsr.web   = WE_DISABLE;
      end else if(rd_stt_en_i)begin
	 // Read the state through port B.
	 tsr.ena = DISABLE;
	 tsr.wea = WE_DISABLE;

	 tsr.addrb = rd_stt_addrb.row;
	 tsr.enb   = ENABLE;
	 tsr.web   = WE_DISABLE;
      end
   end : TSR_IN_MUX

   // True Dual port BRAM 1kx36. 
   ram_tdp #
     (
      // default invalid TAG value, 2 tags per row. 
      .PORTA_RESET_VALUE({2{INVALID_TAG}}),
      // default invalid state value. 2 states per row.
      .PORTB_RESET_VALUE({2{INVALID_STATE}})
      )
   tsr0 (
	 .clk		(clk),
	 .reset		(reset),
	 .addra		(tsr.addra),
	 .dina		(tsr.dina),
	 .ena		(tsr.ena),
	 .wea		(tsr.wea),
	 .douta		(tsr.douta),
	 .addrb		(tsr.addrb),
	 .dinb		(tsr.dinb),
	 .enb		(tsr.enb),
	 .web		(tsr.web),
	 .doutb		(tsr.doutb),
	 .busy_o	(tsr_busy)
	 );

   always_ff @(posedge clk) begin : REG_ASSIGN
      rd_way_id_reg		<= rd_way_id_next;
      hit_reg			<= hit_next;
      set_id_full_reg		<= set_id_full_next;
      rd_tag_nxm_reg		<= rd_tag_nxm_next;
      way_id_stt_reg		<= way_id_stt_next;
      tsr_busy_reg		<= tsr_busy_next;
      rd_stt_lo_or_hi_reg	<= rd_stt_lo_or_hi_next;
      rd_stt_row_way_0_or_1_reg <= rd_stt_row_way_0_or_1_next;
      tag_to_cmp_reg <= tag_to_cmp_next;
      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset
	 rd_way_id_reg		<= '0;
	 hit_reg		<= '0;
	 set_id_full_reg	<= '0;
	 rd_tag_nxm_reg		<= '0;
	 way_id_stt_reg		<= '0;
	 tsr_busy_reg		<= '0;
	 rd_stt_lo_or_hi_reg	   <= LO_REGION;
	 rd_stt_row_way_0_or_1_reg <= ROW_WAY_0;
	 tag_to_cmp_reg <= '0;
      end
   end : REG_ASSIGN
   
endmodule // tag_state_ram

`endif
