
module map_ecid_to_wrdTb();

   parameter DMASK_WIDTH = 4;
   parameter NUM_SLOTS = DMASK_WIDTH;
   parameter SLOT_WIDTH = 256;
   parameter BYTE_STRB_WIDTH = SLOT_WIDTH/8;

   bit clk, reset;

   //input output ports 
   //Input signals
   logic [NUM_SLOTS-1:0][SLOT_WIDTH-1:0]       eci_slot_i;
   logic [DMASK_WIDTH-1:0] 		       dmask_i;

   //Output signals
   logic [NUM_SLOTS-1:0][SLOT_WIDTH-1:0]       wr_slot_o;
   logic [NUM_SLOTS-1:0][BYTE_STRB_WIDTH-1:0]  wrd_byte_strobe_o;


   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   map_ecid_to_wrd map_ecid_to_wrd1 (
				     .eci_slot_i(eci_slot_i),
				     .dmask_i(dmask_i),
				     .wr_slot_o(wr_slot_o),
				     .wrd_byte_strobe_o(wrd_byte_strobe_o)
				     );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();
      // Initialize Input Ports 
      eci_slot_i = '0;
      dmask_i = '0;
      check_op(
	       .exp_wr_slot_0(eci_slot_i[0]),
	       .exp_wr_slot_1(eci_slot_i[1]),
	       .exp_wr_slot_2(eci_slot_i[2]),
	       .exp_wr_slot_3(eci_slot_i[3])
	       );
      incr_dmask(); // 1
      check_op(
      	       .exp_wr_slot_0(eci_slot_i[0]),
	       .exp_wr_slot_1(eci_slot_i[1]),
	       .exp_wr_slot_2(eci_slot_i[2]),
	       .exp_wr_slot_3(eci_slot_i[3])
	       );
      incr_dmask(); // 2
      check_op(
      	       .exp_wr_slot_0(eci_slot_i[0]),
	       .exp_wr_slot_1(eci_slot_i[0]),
	       .exp_wr_slot_2(eci_slot_i[2]),
	       .exp_wr_slot_3(eci_slot_i[3])
	       );
      incr_dmask(); // 3
      check_op(
      	       .exp_wr_slot_0(eci_slot_i[0]),
	       .exp_wr_slot_1(eci_slot_i[1]),
	       .exp_wr_slot_2(eci_slot_i[2]),
	       .exp_wr_slot_3(eci_slot_i[3])
	       );
      incr_dmask(); // 4
      check_op(
      	       .exp_wr_slot_0(eci_slot_i[0]),
	       .exp_wr_slot_1(eci_slot_i[1]),
	       .exp_wr_slot_2(eci_slot_i[0]),
	       .exp_wr_slot_3(eci_slot_i[3])
	       );
      incr_dmask(); // 5
      check_op(
      	       .exp_wr_slot_0(eci_slot_i[0]),
	       .exp_wr_slot_1(eci_slot_i[1]),
	       .exp_wr_slot_2(eci_slot_i[1]),
	       .exp_wr_slot_3(eci_slot_i[3])
	       );
      incr_dmask(); // 6
      check_op(
      	       .exp_wr_slot_0(eci_slot_i[0]),
	       .exp_wr_slot_1(eci_slot_i[0]),
	       .exp_wr_slot_2(eci_slot_i[1]),
	       .exp_wr_slot_3(eci_slot_i[3])
	       );
      incr_dmask(); // 7
      check_op(
      	       .exp_wr_slot_0(eci_slot_i[0]),
	       .exp_wr_slot_1(eci_slot_i[1]),
	       .exp_wr_slot_2(eci_slot_i[2]),
	       .exp_wr_slot_3(eci_slot_i[3])
	       );
      incr_dmask(); // 8
      check_op(
      	       .exp_wr_slot_0(eci_slot_i[0]),
	       .exp_wr_slot_1(eci_slot_i[1]),
	       .exp_wr_slot_2(eci_slot_i[2]),
	       .exp_wr_slot_3(eci_slot_i[0])
	       );
      incr_dmask(); // 9
      check_op(
      	       .exp_wr_slot_0(eci_slot_i[0]),
	       .exp_wr_slot_1(eci_slot_i[1]),
	       .exp_wr_slot_2(eci_slot_i[2]),
	       .exp_wr_slot_3(eci_slot_i[1])
	       );
      incr_dmask(); // 10
      check_op(
      	       .exp_wr_slot_0(eci_slot_i[0]),
	       .exp_wr_slot_1(eci_slot_i[0]),
	       .exp_wr_slot_2(eci_slot_i[2]),
	       .exp_wr_slot_3(eci_slot_i[1])
	       );
      incr_dmask(); // 11
      check_op(
      	       .exp_wr_slot_0(eci_slot_i[0]),
	       .exp_wr_slot_1(eci_slot_i[1]),
	       .exp_wr_slot_2(eci_slot_i[2]),
	       .exp_wr_slot_3(eci_slot_i[2])
	       );
      incr_dmask(); // 12
      check_op(
      	       .exp_wr_slot_0(eci_slot_i[0]),
	       .exp_wr_slot_1(eci_slot_i[1]),
	       .exp_wr_slot_2(eci_slot_i[0]),
	       .exp_wr_slot_3(eci_slot_i[1])
	       );
      incr_dmask(); // 13
      check_op(
      	       .exp_wr_slot_0(eci_slot_i[0]),
	       .exp_wr_slot_1(eci_slot_i[1]),
	       .exp_wr_slot_2(eci_slot_i[1]),
	       .exp_wr_slot_3(eci_slot_i[2])
	       );
      incr_dmask(); // 14
      check_op(
      	       .exp_wr_slot_0(eci_slot_i[0]),
	       .exp_wr_slot_1(eci_slot_i[0]),
	       .exp_wr_slot_2(eci_slot_i[1]),
	       .exp_wr_slot_3(eci_slot_i[2])
	       );
      incr_dmask(); // 15
      check_op(
      	       .exp_wr_slot_0(eci_slot_i[0]),
	       .exp_wr_slot_1(eci_slot_i[1]),
	       .exp_wr_slot_2(eci_slot_i[2]),
	       .exp_wr_slot_3(eci_slot_i[3])
	       );
      $display("Success");
      #500 $finish;
   end 

   task static incr_dmask();
      dmask_i = dmask_i + 1;
      $display("Dmask: %d", dmask_i);
      #1;
   endtask //incr_dmask

   task static check_op(
			input logic [SLOT_WIDTH-1:0] exp_wr_slot_0,
			input logic [SLOT_WIDTH-1:0] exp_wr_slot_1,
			input logic [SLOT_WIDTH-1:0] exp_wr_slot_2,
			input logic [SLOT_WIDTH-1:0] exp_wr_slot_3
			);
      #1;
      if(exp_wr_slot_0 != wr_slot_o[0]) begin
	 $fatal(1, "Write slot 0 contents dont match exp = %h, act = %h", exp_wr_slot_0, wr_slot_o[0]);	 
      end
      if(exp_wr_slot_1 != wr_slot_o[1]) begin
	 $fatal(1, "Write slot 1 contents dont match exp = %h, act = %h", exp_wr_slot_1, wr_slot_o[1]);	 
      end
      if(exp_wr_slot_2 != wr_slot_o[2]) begin
	 $fatal(1, "Write slot 2 contents dont match exp = %h, act = %h", exp_wr_slot_2, wr_slot_o[2]);	 
      end
      if(exp_wr_slot_3 != wr_slot_o[3]) begin
	 $fatal(1, "Write slot 3 contents dont match exp = %h, act = %h", exp_wr_slot_3, wr_slot_o[3]);
      end
      if(|wrd_byte_strobe_o[0] != dmask_i[0]) begin
	 $fatal(1, "byte strobe 0 does not match");
      end
      if(|wrd_byte_strobe_o[1] != dmask_i[1]) begin
	 $fatal(1, "byte strobe 1 does not match");
      end
      if(|wrd_byte_strobe_o[2] != dmask_i[2]) begin
	 $fatal(1, "byte strobe 2 does not match");
      end
      if(|wrd_byte_strobe_o[3] != dmask_i[3]) begin
	 $fatal(1, "byte strobe 3 does not match");
      end
      #1;
   endtask //check_op
   
endmodule
