#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
$XILINX_VIVADO/data/verilog/src/glbl.v \
      ../rtl/eci_cmd_defs.sv \
      ../rtl/eci_dcs_defs.sv \
      ../testbench/dcu_sim_wrTb.sv \
      ../rtl/dcu_sim_wr.sv

xelab -debug typical -incremental -L xpm worklib.dcu_sim_wrTb worklib.glbl -s worklib.dcu_sim_wrTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.dcu_sim_wrTb 
xsim -gui worklib.dcu_sim_wrTb 
