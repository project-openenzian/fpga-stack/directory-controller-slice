#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
      $XILINX_VIVADO/data/verilog/src/glbl.v \
      ../rtl/axis_pipeline_stage.sv \
      ../testbench/vr_pipe_stageTb.sv \
      ../rtl/vr_pipe_stage.sv 

xelab -debug typical -incremental -L xpm worklib.vr_pipe_stageTb worklib.glbl -s worklib.vr_pipe_stageTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.vr_pipe_stageTb 
xsim -gui worklib.vr_pipe_stageTb 
