
module eci_trmgrTb();

   parameter DATA_WIDTH = 64;

   //input output ports 
   //Input signals
   logic			  clk;
   logic			  reset;
   logic [DATA_WIDTH-1:0] 	  alloc_data_i;
   logic			  alloc_en_i;
   logic			  free_en_i;

   //Output signals
   logic [DATA_WIDTH-1:0] 	  alloc_data_o;
   logic 			  alloc_valid_o;


   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   eci_trmgr eci_trmgr1 (
			 .clk(clk),
			 .reset(reset),
			 .alloc_data_i(alloc_data_i),
			 .alloc_en_i(alloc_en_i),
			 .free_en_i(free_en_i),
			 .alloc_data_o(alloc_data_o),
			 .alloc_valid_o(alloc_valid_o)
			 );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports 
      clk = '0;
      reset = '1;
      alloc_data_i = '0;
      alloc_en_i = '0;
      free_en_i = '0;

      ##5;
      reset = 1'b0;

      ##5;
      allocate(.data_i('1));
      assert(alloc_data_o === alloc_data_i) else
	$fatal(1,"Allocated data not showing in output.");
      assert(alloc_valid_o === 1'b1) else
	$fatal(1,"Allocated data should be valid now.");

      ##5;
      free();
      assert(alloc_valid_o === 1'b0) else
	$fatal(1,"Allocated data should be invalid now.");

      allocate(.data_i('d256));
      assert(alloc_data_o === alloc_data_i) else
	$fatal(1,"Allocated data not showing in output.");
      assert(alloc_valid_o === 1'b1) else
	$fatal(1,"Allocated data should be valid now.");

      ##5;
      free();
      assert(alloc_valid_o === 1'b0) else
	$fatal(1,"Allocated data should be invalid now.");

      $display("Success, all tests passed.");
      #500 $finish;
   end

   task static allocate(
			input logic [DATA_WIDTH-1:0] data_i
			);
      alloc_data_i = data_i;
      alloc_en_i = 1'b1;
      ##1;
      alloc_en_i = 1'b0;
      #1;
   endtask //allocate

   task static free();
      free_en_i = 1'b1;
      ##1;
      free_en_i = 1'b0;
      #1;
   endtask //free
endmodule
