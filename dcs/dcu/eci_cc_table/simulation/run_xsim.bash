#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
      $XILINX_VIVADO/data/verilog/src/glbl.v \
      ../rtl/eci_cc_defs.sv \
      ../testbench/eci_cc_tableTb.sv \
      ../rtl/eci_cc_table.sv 

xelab -debug typical -incremental -L xpm worklib.eci_cc_tableTb worklib.glbl -s worklib.eci_cc_tableTb

#Top open GUI replace -R with -gui
#xsim -t source_xsim_run.tcl -R worklib.eci_cc_tableTb 
xsim -gui worklib.eci_cc_tableTb 
