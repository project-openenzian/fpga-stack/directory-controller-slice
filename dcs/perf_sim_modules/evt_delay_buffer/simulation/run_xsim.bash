#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
$XILINX_VIVADO/data/verilog/src/glbl.v \
      ../testbench/evt_delay_bufferTb.sv \
      ../rtl/axis_pipeline_stage.sv \
      ../rtl/evt_delay_buffer.sv 

xelab -debug typical -incremental -L xpm worklib.evt_delay_bufferTb worklib.glbl -s worklib.evt_delay_bufferTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.evt_delay_bufferTb 
xsim -gui worklib.evt_delay_bufferTb 
