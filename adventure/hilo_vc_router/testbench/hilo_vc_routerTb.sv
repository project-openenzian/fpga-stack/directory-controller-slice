import eci_cmd_defs::*;

module hilo_vc_routerTb();


   //input output ports 
   //Input signals
   logic 				    clk;
   logic 				    reset;
   logic [ECI_WORD_WIDTH-1:0] 		    ecih_hdr_i;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	    ecih_pkt_size_i;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	    ecih_pkt_vc_i;
   logic 				    ecih_pkt_valid_i;
   logic 				    req_wod_pkt_ready_i;
   logic 				    rsp_wod_pkt_ready_i;
   logic 				    rsp_wd_pkt_ready_i;

   //Output signals
   logic 				    ecih_pkt_ready_o;
   logic [ECI_WORD_WIDTH-1:0] 		    req_wod_hdr_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	    req_wod_pkt_size_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	    req_wod_pkt_vc_o;
   logic 				    req_wod_pkt_valid_o;
   logic [ECI_WORD_WIDTH-1:0] 		    rsp_wod_hdr_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	    rsp_wod_pkt_size_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	    rsp_wod_pkt_vc_o;
   logic 				    rsp_wod_pkt_valid_o;
   logic [ECI_WORD_WIDTH-1:0] 		    rsp_wd_hdr_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	    rsp_wd_pkt_size_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	    rsp_wd_pkt_vc_o;
   logic 				    rsp_wd_pkt_valid_o;

   typedef enum logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] {VC_RSP_WD_ODD = 4, VC_RSP_WD_EVEN = 5, 
						   VC_REQ_WOD_ODD = 6, VC_REQ_WOD_EVEN = 7,
						   VC_RSP_WOD_ODD = 10, VC_RSP_WOD_EVEN = 11} vc_t;
   
   vc_t my_vc;
   
   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   assign ecih_pkt_vc_i = my_vc;
   //instantiation
   hilo_vc_router hilo_vc_router1 (
				   .clk(clk),
				   .reset(reset),
				   .ecih_hdr_i(ecih_hdr_i),
				   .ecih_pkt_size_i(ecih_pkt_size_i),
				   .ecih_pkt_vc_i(ecih_pkt_vc_i),
				   .ecih_pkt_valid_i(ecih_pkt_valid_i),
				   .req_wod_pkt_ready_i(req_wod_pkt_ready_i),
				   .rsp_wod_pkt_ready_i(rsp_wod_pkt_ready_i),
				   .rsp_wd_pkt_ready_i(rsp_wd_pkt_ready_i),
				   .ecih_pkt_ready_o(ecih_pkt_ready_o),
				   .req_wod_hdr_o(req_wod_hdr_o),
				   .req_wod_pkt_size_o(req_wod_pkt_size_o),
				   .req_wod_pkt_vc_o(req_wod_pkt_vc_o),
				   .req_wod_pkt_valid_o(req_wod_pkt_valid_o),
				   .rsp_wod_hdr_o(rsp_wod_hdr_o),
				   .rsp_wod_pkt_size_o(rsp_wod_pkt_size_o),
				   .rsp_wod_pkt_vc_o(rsp_wod_pkt_vc_o),
				   .rsp_wod_pkt_valid_o(rsp_wod_pkt_valid_o),
				   .rsp_wd_hdr_o(rsp_wd_hdr_o),
				   .rsp_wd_pkt_size_o(rsp_wd_pkt_size_o),
				   .rsp_wd_pkt_vc_o(rsp_wd_pkt_vc_o),
				   .rsp_wd_pkt_valid_o(rsp_wd_pkt_valid_o)
				   );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports 
      clk = '0;
      reset = '1;
      ecih_hdr_i = '0;
      ecih_pkt_size_i = '0;
      //ecih_pkt_vc_i = '0;
      if(randomize(my_vc) == 0) begin
	 $fatal(1, "Randomize Failed");
      end
      ecih_pkt_valid_i = '0;
      req_wod_pkt_ready_i = '0;
      rsp_wod_pkt_ready_i = '0;
      rsp_wd_pkt_ready_i = '0;

      ##5;
      reset = 1'b0;
      req_wod_pkt_ready_i = '1;
      rsp_wod_pkt_ready_i = '1;
      rsp_wd_pkt_ready_i = '1;

      ##5;

      repeat(10) begin
	 launch_random_wait();
      end

      $display("Success: All tests pass");
      #500 $finish;
   end

   always_ff @(posedge clk) begin : CHECK_RESULTS
      if(req_wod_pkt_valid_o & req_wod_pkt_ready_i) begin
	 assert((req_wod_pkt_vc_o === VC_REQ_WOD_ODD) | 
		(req_wod_pkt_vc_o === VC_REQ_WOD_EVEN)) else
	   $fatal(1,"VC %0d does not match VC_REQ_WOD VCs", req_wod_pkt_vc_o);
	 assert(rsp_wod_pkt_valid_o === 1'b0) else
	   $fatal(1,"Rsp wod should not be valid.");
	 assert(rsp_wd_pkt_valid_o === 1'b0) else
	   $fatal(1,"Rsp wd should not be valid.");
      end

      if(rsp_wod_pkt_valid_o & rsp_wod_pkt_ready_i) begin
	 assert((rsp_wod_pkt_vc_o === VC_RSP_WOD_ODD) | 
		(rsp_wod_pkt_vc_o === VC_RSP_WOD_EVEN)) else
	   $fatal(1,"VC %0d does not match VC_REQ_WOD VCs", rsp_wod_pkt_vc_o);
	 assert(req_wod_pkt_valid_o === 1'b0) else
	   $fatal(1,"Req wod should not be valid.");
	 assert(rsp_wd_pkt_valid_o === 1'b0) else
	   $fatal(1,"Rsp wd should not be valid.");
      end

      if(rsp_wd_pkt_valid_o & rsp_wd_pkt_ready_i) begin
	 assert((rsp_wd_pkt_vc_o === VC_RSP_WD_ODD) | 
		(rsp_wd_pkt_vc_o === VC_RSP_WD_EVEN)) else
	   $fatal(1,"VC %0d does not match VC_REQ_WOD VCs", rsp_wd_pkt_vc_o);
	 assert(req_wod_pkt_valid_o === 1'b0) else
	   $fatal(1,"Req wod should not be valid.");
	 assert(rsp_wod_pkt_valid_o === 1'b0) else
	   $fatal(1,"Rsp wod should not be valid.");
      end
   end : CHECK_RESULTS

   task static launch_random_wait();
      launch_random();
      wait(ecih_pkt_valid_i & ecih_pkt_ready_o);
      ##1;
      ecih_pkt_valid_i = 1'b0;
      ##1;
   endtask //launch_random

   task static launch_random();
      ecih_hdr_i = $urandom_range(10,200);
      ecih_pkt_size_i = $urandom_range(1, 17);
      if(randomize(my_vc) == 0) begin
	 $fatal(1, "Randomize failed");
      end
      ecih_pkt_valid_i = 1'b1;
   endtask //launch_random
endmodule
