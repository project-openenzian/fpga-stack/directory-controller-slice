`ifndef SIMPLE_DELAY_MODULE_SV
 `define SIMPLE_DELAY_MODULE_SV


// simulate a delay of SIM_DELAY cycles
// between each input and output.
// Not pipelined this means there will be
// SIM_DELAY cycles before consuming the next
// input.

module simple_delay_module #
  (
   parameter DATA_WIDTH = 64,
   parameter SIM_DELAY = 5
   
   ) 
   (
    input logic 		  clk,
    input logic 		  reset,
    input logic [DATA_WIDTH-1:0]  us_data_i,
    input logic 		  us_valid_i,
    output logic 		  us_ready_o,
    output logic [DATA_WIDTH-1:0] ds_data_o,
    output logic 		  ds_valid_o,
    input logic 		  ds_ready_i
    );

   logic [63:0] 		  counter_reg = '0, counter_next;
   logic 			  en_vr;

   always_comb begin : OP_ASSIGN
      ds_data_o = us_data_i;
      ds_valid_o = us_valid_i & en_vr;
      us_ready_o =  ds_ready_i & en_vr;
   end : OP_ASSIGN

   // counter is initially at 0.
   // if input is valid, start counter.
   // keep counter running till SIM_DELAY 
   // count is reached. Once count is
   // reached, output becomes valid and
   // downstream ready is fed into input.
   // Counter is reset upon handshake in
   // request i/f.
   always_comb begin : CONTROLLER
      counter_next	= counter_reg;
      en_vr = 1'b0;
      
      if(counter_reg == SIM_DELAY) begin
	 en_vr = 1'b1;
      end
 
      if(us_valid_i & us_ready_o) begin
	 counter_next = '0;
      end else if(us_valid_i & ~en_vr) begin
	 counter_next = counter_reg + 1;
      end 
   end : CONTROLLER

   always_ff @(posedge clk) begin : REG_ASSIGN
      counter_reg	<= counter_next;
      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset 
	 counter_reg	<= '0;
      end
   end : REG_ASSIGN

endmodule //simple_delay_module
`endif
