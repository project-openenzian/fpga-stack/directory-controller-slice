
module simpleTb();

   parameter DCU_ID=5;

   //input output ports 
   //Input signals
   logic        clk;
   logic        reset;
   logic        wr_en_i;
   logic [4:0] 	dcu_id_i;

   //Output signals
   logic [4:0] 	dcu_id_o;


   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   simple simple1 (
		   .clk(clk),
		   .reset(reset),
		   .wr_en_i(wr_en_i),
		   .dcu_id_i(dcu_id_i),
		   .dcu_id_o(dcu_id_o)
		   );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports 
      clk = '0;
      reset = '1;
      wr_en_i = '0;
      dcu_id_i = '0;

      ##5;
      reset = 1'b0;
      ##5;
      wr_en_i = 1'b1;
      ##1;
      wr_en_i = 1'b0;
      

      #500 $finish;
   end 

endmodule
