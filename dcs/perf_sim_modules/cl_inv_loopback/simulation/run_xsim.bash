#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
      $XILINX_VIVADO/data/verilog/src/glbl.v \
      ../rtl/eci_cmd_defs.sv \
      ../rtl/eci_dcs_defs.sv \
      ../testbench/cl_inv_loopbackTb.sv \
      ../rtl/axis_pipeline_stage.sv \
      ../rtl/cl_inv_loopback.sv \
      ../rtl/vr_pipe_stage.sv 

xelab -debug typical -incremental -L xpm worklib.cl_inv_loopbackTb worklib.glbl -s worklib.cl_inv_loopbackTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.cl_inv_loopbackTb 
xsim -gui worklib.cl_inv_loopbackTb 
