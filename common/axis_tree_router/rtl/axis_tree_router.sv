/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-01-27
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */

`ifndef AXIS_TREE_ROUTER_SV
`define AXIS_TREE_ROUTER_SV

module axis_tree_router #
  (
   parameter DATA_WIDTH = 64,
   parameter NUM_OUT = 4,
   parameter SEL_WIDTH = $clog2(NUM_OUT)
   )
   (
    input logic 			       clk,
    input logic 			       reset,
    // Input channel.
    input logic [DATA_WIDTH-1:0] 	       us_data_i,
    input logic [SEL_WIDTH-1:0] 	       us_sel_ch_i,
    input logic 			       us_valid_i,
    output logic 			       us_ready_o,
    // Output channels.
    output logic [NUM_OUT-1:0][DATA_WIDTH-1:0] ds_data_o,
    output logic [NUM_OUT-1:0] 		       ds_valid_o,
    input logic [NUM_OUT-1:0] 		       ds_ready_i
    );

   localparam NUM_STAGES = $clog2(NUM_OUT);
   
   typedef struct packed {
      logic [DATA_WIDTH-1:0] data;
      logic [SEL_WIDTH-1:0]  sel_word;
      logic 		     sel_bit;
      logic 		     valid;
      logic 		     ready;
   } str_t;

   // NUM_STAGES to account for input and output stages.
   str_t stage_str [(NUM_STAGES + 1)-1:0][NUM_OUT-1:0];

   initial begin
      if((NUM_OUT % 2) != 0) begin
	 $error("Error: The number of outputs should be a multiple of 2 (instance %m)");
	 $finish;
      end
   end

   always_comb begin : FIRST_LAST_ASSIGN
      // First input stage. 
      stage_str[0][0].data = us_data_i;
      stage_str[0][0].sel_word = us_sel_ch_i;
      stage_str[0][0].valid = us_valid_i;
      us_ready_o = stage_str[0][0].ready;
      // Last stage. 
      for( integer i=0; i<NUM_OUT; i=i+1 ) begin
	 ds_data_o[i] = stage_str[NUM_STAGES][i].data;
	 ds_valid_o[i] = stage_str[NUM_STAGES][i].valid;
	 stage_str[NUM_STAGES][i].ready = ds_ready_i[i];
      end 
   end : FIRST_LAST_ASSIGN


   // The select signals are also passed along with the data to the subsequent stages.
   // the select bit for each subsequent stage is got from the output of the previous stage.
   // The MSB of the select word is used to select the output of the first stage.
   // The MSB-1 bit of select word is used to select the output of second stage and so on.
   genvar i,j;
   generate
      for( i = 0; i < NUM_STAGES; i = i+1 ) begin : STAGE
	 for( j = 0; j < (2**i); j=j+1 ) begin : INST_WITHIN_STAGE
	    
	    assign stage_str[i][j].sel_bit = stage_str[i][j].sel_word[(SEL_WIDTH-1)-i];
	    
	    axis_2_router #
			(
			 .DATA_WIDTH(DATA_WIDTH + SEL_WIDTH)
			 )
	    axis_2_rtr_inst
			(
			 .clk			(clk),
			 .reset			(reset),
			 .us_data_i		({
						  stage_str[i][j].sel_word,
						  stage_str[i][j].data
						  }
						 ),
			 .us_sel_down_upbar_i	(stage_str[i][j].sel_bit),
			 .us_valid_i		(stage_str[i][j].valid),
			 .us_ready_o		(stage_str[i][j].ready),
			 .ds_up_data_o		({
						  stage_str[i+1][(j*2)].sel_word,
						  stage_str[i+1][(j*2)].data
						  }
						 ),
			 .ds_up_valid_o		(stage_str[i+1][(j*2)].valid),
			 .ds_up_ready_i		(stage_str[i+1][(j*2)].ready),
			 .ds_down_data_o	({
						  stage_str[i+1][(j*2) + 1].sel_word,
						  stage_str[i+1][(j*2) + 1].data
						  }
						 ),
			 .ds_down_valid_o	(stage_str[i+1][(j*2) + 1].valid),
			 .ds_down_ready_i	(stage_str[i+1][(j*2) + 1].ready)
			 );
	 end : INST_WITHIN_STAGE
      end : STAGE
   endgenerate
   
endmodule // axis_tree_router
`endif
