#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sys
import re

def read_sim_res(sim_filepath):
    fh1 = open(sim_filepath,'r')
    num_cls_read = []
    tput = []
    
    while True:
        line = fh1.readline()
        if not line:
            break
        m = re.search(r"Num resp recd\s+=\s+(\d+)", line)
        n = re.search(r"Tput\s+=\s+(\d+.\d+)\s+GBytes/s", line)
        if m:
            if(int(m.group(1)) != 0):
                num_cls_read.append(int(m.group(1)))
        if n:
            tput.append(float(n.group(1)))
        #print("Line{}:{}".format(count, line.strip()))
    
    #completed reading the file.
    fh1.close()
    #print(len(tput))
    #print(len(num_cls_read))
    df = pd.DataFrame({'num_cls_read':num_cls_read, 'tput_GBps':tput})
    return(df)


if(len(sys.argv) < 2):
    print("Simulation results to be plotted, should be sent as input\n")
    exit(-1)

sim_res_file = sys.argv[1];
sim_res_df = pd.DataFrame();
sim_res_df = read_sim_res(sim_res_file)
print(sim_res_df.head())
sim_res_df.plot.scatter(x="num_cls_read", y="tput_GBps")
plt.axhline(y=sim_res_df['tput_GBps'].min(), color = 'r', linestyle='-')
plt.annotate('%0.2f' % sim_res_df['tput_GBps'].min(), xy=(1, sim_res_df['tput_GBps'].min()))
plt.show()


