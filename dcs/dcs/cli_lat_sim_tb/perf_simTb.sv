/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-08-25
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */

`ifndef PERF_SIMTB_SV
 `define PERF_SIMTB_SV

import eci_cmd_defs::*;
import eci_dcs_defs::*;

// Connects seq_read_load_gen to a single DC Slice.
// seq_read_load_gen issues continuous R13 messages
// given a starting address, stride and number of CLs.
// A DC slice takes only odd/even cache lines, so
// the stride is always 2. 
// A DC slice also has only limited directory and
// Each DCU can store DS_NUM_SETS_PER_DCU * DS_NUM_WAYS_PER_SET
// and there are DS_NUM_DCU_IDX DCUs. This gives maximum
// number of cacheable CLs. 
module perf_simTb();

   // round trip latency = 500ns.
   localparam CPU_DIRC_LATENCY = 500/2;
   localparam CLOCK_FREQ = 3.1;
   localparam NUM_LATENCY_STAGES = int'(CPU_DIRC_LATENCY/CLOCK_FREQ);
   localparam STRIDE = 2**(1);
   // Since there is only 1 DCU, it can accommodate
   // Number of sets per DCU * Number of ways per set
   // cache lines. anything above will result in TSU FULL error. 
   localparam MAX_NUM_CLS = DS_NUM_DCU_IDX * DS_NUM_SETS_PER_DCU * DS_NUM_WAYS_PER_SET;
   localparam PRINT_EVERY_X_TR = 1000;

   parameter PERF_REGS_WIDTH = 64;
   parameter SYNTH_PERF_REGS = 1;

   typedef struct packed {
      logic [ECI_WORD_WIDTH-1:0] hdr;
      logic [ECI_PACKET_SIZE_WIDTH-1:0] size;
      logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] vc;
      logic				valid;
      logic				ready;
   } eci_hdr_if_t;

   typedef struct packed {
      logic [ECI_PACKET_SIZE-1:0][ECI_WORD_WIDTH-1:0] pkt;
      logic [ECI_PACKET_SIZE_WIDTH-1:0]		      size;
      logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0]		      vc;
      logic					      valid;
      logic					      ready;
   } eci_pkt_if_t;

   //input output ports
   //Input signals

   logic					      clk;
   logic					      reset;

   logic [ECI_CL_INDEX_WIDTH-1:0]		      st_cl_idx_i;
   logic [63:0]					      num_cls_i;
   logic [63:0]					      cl_idx_stride_i;
   logic					      tput_latency_bar_i;
   logic					      en_i;
   logic [63:0]					      num_rsp_recd_o;

   logic [MAX_DCU_ID_WIDTH-1:0] 		      rd_req_id_o;
   logic [DS_ADDR_WIDTH-1:0] 			      rd_req_addr_o;
   logic 					      rd_req_valid_o;
   logic 					      rd_rsp_ready_o;
   logic [MAX_DCU_ID_WIDTH-1:0] 		      wr_req_id_o;
   logic [DS_ADDR_WIDTH-1:0] 			      wr_req_addr_o;
   logic [ECI_CL_WIDTH-1:0] 			      wr_req_data_o;
   logic [ECI_CL_SIZE_BYTES-1:0] 		      wr_req_strb_o;
   logic 					      wr_req_valid_o;
   logic 					      wr_rsp_ready_o;
   logic 					      rd_req_ready_i;
   logic [MAX_DCU_ID_WIDTH-1:0] 		      rd_rsp_id_i;
   logic [ECI_CL_WIDTH-1:0] 			      rd_rsp_data_i;
   logic 					      rd_rsp_valid_i;
   logic 					      wr_req_ready_i;
   logic [MAX_DCU_ID_WIDTH-1:0] 		      wr_rsp_id_i;
   logic [1:0] 					      wr_rsp_bresp_i;
   logic 					      wr_rsp_valid_i;

   // CLI Loopback signals.
   logic [DS_ADDR_WIDTH-1:0] 			      cli_rd_req_addr_i;
   logic 					      cli_rd_req_valid_i;
   eci_hdr_if_t cli_lcl_fwd_wod_o;
   eci_hdr_if_t cli_lcl_rsp_wod_i;
   eci_hdr_if_t cli_lcl_rsp_wod_o;
   logic 					      cli_done;
   
   // C_ means CPU initiated (from CPU to DIRC).
   // F_ means FPGA initiated (from DirC to CPU).
   eci_hdr_if_t c_us_req_wod, c_ds_req_wod;
   eci_hdr_if_t c_us_rsp_wod, c_ds_rsp_wod;
   eci_pkt_if_t c_us_rsp_wd, c_ds_rsp_wd;
   eci_hdr_if_t f_us_fwd_wod, f_ds_fwd_wod;
   eci_hdr_if_t f_us_rsp_wod, f_ds_rsp_wod;
   eci_pkt_if_t f_us_rsp_wd, f_ds_rsp_wd;

   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   // Generate read requests to send to DirC. 
   seq_read_load_gen  #
     (
      .PRINT_EVERY_X_TR(PRINT_EVERY_X_TR)
      )
     seq_read_load_gen1 (
			 .clk			(clk),
			 .reset			(reset),
			 .st_cl_idx_i		(st_cl_idx_i),
			 .num_cls_i		(num_cls_i),
			 .cl_idx_stride_i	(cl_idx_stride_i),
			 .tput_latency_bar_i	(tput_latency_bar_i),
			 .en_i			(en_i),

			 // Input from Dirc. 
			 .fwd_wod_hdr_i		(f_ds_fwd_wod.hdr),
			 .fwd_wod_pkt_size_i	(f_ds_fwd_wod.size),
			 .fwd_wod_pkt_vc_i	(f_ds_fwd_wod.vc),
			 .fwd_wod_pkt_valid_i	(f_ds_fwd_wod.valid),
			 .fwd_wod_pkt_ready_o	(f_ds_fwd_wod.ready),

			 .rsp_wod_hdr_i		(f_ds_rsp_wod.hdr),
			 .rsp_wod_pkt_size_i	(f_ds_rsp_wod.size),
			 .rsp_wod_pkt_vc_i	(f_ds_rsp_wod.vc),
			 .rsp_wod_pkt_valid_i	(f_ds_rsp_wod.valid),
			 .rsp_wod_pkt_ready_o	(f_ds_rsp_wod.ready),

			 .rsp_wd_pkt_i		(f_ds_rsp_wd.pkt),
			 .rsp_wd_pkt_size_i	(f_ds_rsp_wd.size),
			 .rsp_wd_pkt_vc_i	(f_ds_rsp_wd.vc),
			 .rsp_wd_pkt_valid_i	(f_ds_rsp_wd.valid),
			 .rsp_wd_pkt_ready_o	(f_ds_rsp_wd.ready),

			 // Output to Dirc. 
			 .req_wod_hdr_o		(c_us_req_wod.hdr),
			 .req_wod_pkt_size_o	(c_us_req_wod.size),
			 .req_wod_pkt_vc_o	(c_us_req_wod.vc),
			 .req_wod_pkt_valid_o	(c_us_req_wod.valid),
			 .req_wod_pkt_ready_i	(c_us_req_wod.ready),

			 .rsp_wod_hdr_o		(c_us_rsp_wod.hdr),
			 .rsp_wod_pkt_size_o	(c_us_rsp_wod.size),
			 .rsp_wod_pkt_vc_o	(c_us_rsp_wod.vc),
			 .rsp_wod_pkt_valid_o	(c_us_rsp_wod.valid),
			 .rsp_wod_pkt_ready_i	(c_us_rsp_wod.ready),

			 .rsp_wd_pkt_o		(c_us_rsp_wd.pkt),
			 .rsp_wd_pkt_size_o	(c_us_rsp_wd.size),
			 .rsp_wd_pkt_vc_o	(c_us_rsp_wd.vc),
			 .rsp_wd_pkt_valid_o	(c_us_rsp_wd.valid),
			 .rsp_wd_pkt_ready_i	(c_us_rsp_wd.ready),
     
			 .tput_timer_o		(),
			 .num_rsp_recd_o	(num_rsp_recd_o)
			 );

   // ECI latency pipeline. 
   // CPU to FPGA: REQ wod pipeline. 
   evt_delay_buffer #
     (
      .EVT_WIDTH(ECI_WORD_WIDTH),
      .VC_WIDTH(ECI_LCL_TOT_NUM_VCS_WIDTH),
      .SIZE_WIDTH(ECI_PACKET_SIZE_WIDTH),
      .NUM_STAGES(NUM_LATENCY_STAGES)
       )
   req_wod_to_dirc_lat
     (
      .clk		(clk),
      .reset		(reset),
      .us_evt_i		(c_us_req_wod.hdr),
      .us_vc_i		(c_us_req_wod.vc),
      .us_size_i	(c_us_req_wod.size),
      .us_valid_i	(c_us_req_wod.valid),
      .us_ready_o	(c_us_req_wod.ready),
      .ds_evt_o		(c_ds_req_wod.hdr),
      .ds_vc_o		(c_ds_req_wod.vc),
      .ds_size_o	(c_ds_req_wod.size),
      .ds_valid_o	(c_ds_req_wod.valid),
      .ds_ready_i	(c_ds_req_wod.ready)
      );

   // CPU to FPGA: RSP wod pipeline. 
   evt_delay_buffer #
     (
      .EVT_WIDTH(ECI_WORD_WIDTH),
      .VC_WIDTH(ECI_LCL_TOT_NUM_VCS_WIDTH),
      .SIZE_WIDTH(ECI_PACKET_SIZE_WIDTH),
      .NUM_STAGES(NUM_LATENCY_STAGES)
       )
   rsp_wod_to_dirc_lat
     (
      .clk		(clk),
      .reset		(reset),
      .us_evt_i		(c_us_rsp_wod.hdr),
      .us_vc_i		(c_us_rsp_wod.vc),
      .us_size_i	(c_us_rsp_wod.size),
      .us_valid_i	(c_us_rsp_wod.valid),
      .us_ready_o	(c_us_rsp_wod.ready),
      .ds_evt_o		(c_ds_rsp_wod.hdr),
      .ds_vc_o		(c_ds_rsp_wod.vc),
      .ds_size_o	(c_ds_rsp_wod.size),
      .ds_valid_o	(c_ds_rsp_wod.valid),
      .ds_ready_i	(c_ds_rsp_wod.ready)
      );

   // CPU to FPGA: RSP wd pipeline.
   evt_delay_buffer #
     (
      .EVT_WIDTH(ECI_PACKET_SIZE*ECI_WORD_WIDTH),
      .VC_WIDTH(ECI_LCL_TOT_NUM_VCS_WIDTH),
      .SIZE_WIDTH(ECI_PACKET_SIZE_WIDTH),
      .NUM_STAGES(NUM_LATENCY_STAGES)
       )
   rsp_wd_to_dirc_lat
     (
      .clk		(clk),
      .reset		(reset),
      .us_evt_i		(c_us_rsp_wd.pkt),
      .us_vc_i		(c_us_rsp_wd.vc),
      .us_size_i	(c_us_rsp_wd.size),
      .us_valid_i	(c_us_rsp_wd.valid),
      .us_ready_o	(c_us_rsp_wd.ready),
      .ds_evt_o		(c_ds_rsp_wd.pkt),
      .ds_vc_o		(c_ds_rsp_wd.vc),
      .ds_size_o	(c_ds_rsp_wd.size),
      .ds_valid_o	(c_ds_rsp_wd.valid),
      .ds_ready_i	(c_ds_rsp_wd.ready)
      );

   // FPGA to CPU: FWD wod pipeline.
   evt_delay_buffer #
     (
      .EVT_WIDTH(ECI_WORD_WIDTH),
      .VC_WIDTH(ECI_LCL_TOT_NUM_VCS_WIDTH),
      .SIZE_WIDTH(ECI_PACKET_SIZE_WIDTH),
      .NUM_STAGES(NUM_LATENCY_STAGES)
       )
   fwd_wod_frm_dirc_lat
     (
      .clk		(clk),
      .reset		(reset),
      .us_evt_i		(f_us_fwd_wod.hdr),
      .us_vc_i		(f_us_fwd_wod.vc),
      .us_size_i	(f_us_fwd_wod.size),
      .us_valid_i	(f_us_fwd_wod.valid),
      .us_ready_o	(f_us_fwd_wod.ready),
      .ds_evt_o		(f_ds_fwd_wod.hdr),
      .ds_vc_o		(f_ds_fwd_wod.vc),
      .ds_size_o	(f_ds_fwd_wod.size),
      .ds_valid_o	(f_ds_fwd_wod.valid),
      .ds_ready_i	(f_ds_fwd_wod.ready)
      );

   // FPGA to CPU: RSP wod pipeline.
   evt_delay_buffer #
     (
      .EVT_WIDTH(ECI_WORD_WIDTH),
      .VC_WIDTH(ECI_LCL_TOT_NUM_VCS_WIDTH),
      .SIZE_WIDTH(ECI_PACKET_SIZE_WIDTH),
      .NUM_STAGES(NUM_LATENCY_STAGES)
       )
   rsp_wod_frm_dirc_lat
     (
      .clk		(clk),
      .reset		(reset),
      .us_evt_i		(f_us_rsp_wod.hdr),
      .us_vc_i		(f_us_rsp_wod.vc),
      .us_size_i	(f_us_rsp_wod.size),
      .us_valid_i	(f_us_rsp_wod.valid),
      .us_ready_o	(f_us_rsp_wod.ready),
      .ds_evt_o		(f_ds_rsp_wod.hdr),
      .ds_vc_o		(f_ds_rsp_wod.vc),
      .ds_size_o	(f_ds_rsp_wod.size),
      .ds_valid_o	(f_ds_rsp_wod.valid),
      .ds_ready_i	(f_ds_rsp_wod.ready)
      );

   // FPGA to CPU: RSP wd pipeline.
   evt_delay_buffer #
     (
      .EVT_WIDTH(ECI_PACKET_SIZE*ECI_WORD_WIDTH),
      .VC_WIDTH(ECI_LCL_TOT_NUM_VCS_WIDTH),
      .SIZE_WIDTH(ECI_PACKET_SIZE_WIDTH),
      .NUM_STAGES(NUM_LATENCY_STAGES)
       )
   rsp_wd_frm_dirc_lat
     (
      .clk		(clk),
      .reset		(reset),
      .us_evt_i		(f_us_rsp_wd.pkt),
      .us_vc_i		(f_us_rsp_wd.vc),
      .us_size_i	(f_us_rsp_wd.size),
      .us_valid_i	(f_us_rsp_wd.valid),
      .us_ready_o	(f_us_rsp_wd.ready),
      .ds_evt_o		(f_ds_rsp_wd.pkt),
      .ds_vc_o		(f_ds_rsp_wd.vc),
      .ds_size_o	(f_ds_rsp_wd.size),
      .ds_valid_o	(f_ds_rsp_wd.valid),
      .ds_ready_i	(f_ds_rsp_wd.ready)
      );

   // DCS.
   dcs#
     (
      .PERF_REGS_WIDTH(PERF_REGS_WIDTH),
      .SYNTH_PERF_REGS(SYNTH_PERF_REGS)
      )
   dcs1
     (
      .clk(clk),
      .reset(reset),
      // Input ECI events.
      // ECI packet for request without data. (VC 6 or 7) (only header).
      .req_wod_hdr_i	        (c_ds_req_wod.hdr),
      .req_wod_pkt_size_i	(c_ds_req_wod.size),
      .req_wod_pkt_vc_i	        (c_ds_req_wod.vc),
      .req_wod_pkt_valid_i	(c_ds_req_wod.valid),
      .req_wod_pkt_ready_o	(c_ds_req_wod.ready),
      // ECI packet for response without data.(VC 10 or 11). (only header).
      .rsp_wod_hdr_i	        (c_ds_rsp_wod.hdr),
      .rsp_wod_pkt_size_i	(c_ds_rsp_wod.size),
      .rsp_wod_pkt_vc_i	        (c_ds_rsp_wod.vc),
      .rsp_wod_pkt_valid_i	(c_ds_rsp_wod.valid),
      .rsp_wod_pkt_ready_o	(c_ds_rsp_wod.ready),
      // ECI packet for response with data. (VC 4 or 5). (header + data).
      .rsp_wd_pkt_i	        (c_ds_rsp_wd.pkt),
      .rsp_wd_pkt_size_i	(c_ds_rsp_wd.size),
      .rsp_wd_pkt_vc_i	        (c_ds_rsp_wd.vc),
      .rsp_wd_pkt_valid_i	(c_ds_rsp_wd.valid),
      .rsp_wd_pkt_ready_o	(c_ds_rsp_wd.ready),
      // Lcl clean, clean inv fwd requests: VC 16 or 17.
      .lcl_fwd_wod_hdr_i	(cli_lcl_fwd_wod_o.hdr),
      .lcl_fwd_wod_pkt_size_i	(cli_lcl_fwd_wod_o.size),
      .lcl_fwd_wod_pkt_vc_i	(cli_lcl_fwd_wod_o.vc),
      .lcl_fwd_wod_pkt_valid_i	(cli_lcl_fwd_wod_o.valid),
      .lcl_fwd_wod_pkt_ready_o	(cli_lcl_fwd_wod_o.ready),
      // lcl rsp_wod unlock interface: VC 18 or 19.
      .lcl_rsp_wod_hdr_i	(cli_lcl_rsp_wod_o.hdr),
      .lcl_rsp_wod_pkt_size_i	(cli_lcl_rsp_wod_o.size),
      .lcl_rsp_wod_pkt_vc_i	(cli_lcl_rsp_wod_o.vc),
      .lcl_rsp_wod_pkt_valid_i	(cli_lcl_rsp_wod_o.valid),
      .lcl_rsp_wod_pkt_ready_o	(cli_lcl_rsp_wod_o.ready),
      // Output ECI events. (rsp without data, rsp with data).
      // VC 10,11
      .rsp_wod_hdr_o	        (f_us_rsp_wod.hdr),
      .rsp_wod_pkt_size_o	(f_us_rsp_wod.size),
      .rsp_wod_pkt_vc_o	        (f_us_rsp_wod.vc),
      .rsp_wod_pkt_valid_o	(f_us_rsp_wod.valid),
      .rsp_wod_pkt_ready_i	(f_us_rsp_wod.ready),
      // VC 5,4
      .rsp_wd_pkt_o	        (f_us_rsp_wd.pkt),
      .rsp_wd_pkt_size_o	(f_us_rsp_wd.size),
      .rsp_wd_pkt_vc_o	        (f_us_rsp_wd.vc),
      .rsp_wd_pkt_valid_o	(f_us_rsp_wd.valid),
      .rsp_wd_pkt_ready_i	(f_us_rsp_wd.ready),
      // VC 8,9
      .fwd_wod_hdr_o		(f_us_fwd_wod.hdr),
      .fwd_wod_pkt_size_o	(f_us_fwd_wod.size),
      .fwd_wod_pkt_vc_o		(f_us_fwd_wod.vc),
      .fwd_wod_pkt_valid_o	(f_us_fwd_wod.valid),
      .fwd_wod_pkt_ready_i	(f_us_fwd_wod.ready),
      // lcl clean, clean inv responses: VC 18 or 19.
      .lcl_rsp_wod_hdr_o	(cli_lcl_rsp_wod_i.hdr),
      .lcl_rsp_wod_pkt_size_o	(cli_lcl_rsp_wod_i.size),
      .lcl_rsp_wod_pkt_vc_o	(cli_lcl_rsp_wod_i.vc),
      .lcl_rsp_wod_pkt_valid_o	(cli_lcl_rsp_wod_i.valid),
      .lcl_rsp_wod_pkt_ready_i	(cli_lcl_rsp_wod_i.ready),
      // Output Read descriptors
      // Read descriptors: Request and response.
      .rd_req_id_o	(rd_req_id_o),
      .rd_req_addr_o	(rd_req_addr_o),
      .rd_req_valid_o	(rd_req_valid_o),
      .rd_req_ready_i	(rd_req_ready_i),
      .rd_rsp_id_i	(rd_rsp_id_i),
      .rd_rsp_data_i	(rd_rsp_data_i),
      .rd_rsp_valid_i	(rd_rsp_valid_i),
      .rd_rsp_ready_o	(rd_rsp_ready_o),
      // Write descriptors: Request and response.
      .wr_req_id_o	(wr_req_id_o),
      .wr_req_addr_o	(wr_req_addr_o),
      .wr_req_data_o	(wr_req_data_o),
      .wr_req_strb_o	(wr_req_strb_o),
      .wr_req_valid_o	(wr_req_valid_o),
      .wr_req_ready_i	(wr_req_ready_i),
      .wr_rsp_id_i	(wr_rsp_id_i),
      .wr_rsp_bresp_i	(wr_rsp_bresp_i),
      .wr_rsp_valid_i	(wr_rsp_valid_i),
      .wr_rsp_ready_o	(wr_rsp_ready_o)
      );//instantiation completed 

   // Memory.
   word_addr_mem #
     (
      .ID_WIDTH(MAX_DCU_ID_WIDTH),
      .ADDR_WIDTH(10),
      .DATA_WIDTH(ECI_CL_WIDTH)
       )
   mem_inst_1
     (
      .clk(clk),
      .reset(reset),

      .rd_req_id_i(rd_req_id_o),
      .rd_req_addr_i(rd_req_addr_o[9+7:7]),
      .rd_req_valid_i(rd_req_valid_o),
      .rd_req_ready_o(rd_req_ready_i),
      
      .wr_req_id_i(wr_req_id_o),
      .wr_req_addr_i(wr_req_addr_o[9+7:7]),
      .wr_req_data_i(wr_req_data_o),
      .wr_req_valid_i(wr_req_valid_o),
      .wr_req_ready_o(wr_req_ready_i),
      
      .rd_rsp_id_o(rd_rsp_id_i),
      .rd_rsp_data_o(rd_rsp_data_i),
      .rd_rsp_valid_o(rd_rsp_valid_i),
      .rd_rsp_ready_i(rd_rsp_ready_o),
      
      .wr_rsp_id_o(wr_rsp_id_i),
      .wr_rsp_bresp_o(wr_rsp_bresp_i),
      .wr_rsp_valid_o(wr_rsp_valid_i),
      .wr_rsp_ready_i(wr_rsp_ready_o)
      );

   // Invalidate module.
   // sample recd rd requests and continuously
   // send invalidates.
   always_comb begin : CLI_LPBK_IP_ASSIGN
      cli_rd_req_addr_i = rd_req_addr_o;
      cli_rd_req_valid_i = rd_req_valid_o & rd_req_ready_i;
   end : CLI_LPBK_IP_ASSIGN
   cli_lat_load_gen #
     (
      .PERF_REGS_WIDTH(32),
      .WAIT_NUM_REQS(64),
      .FIFO_DEPTH(16)
      )
   cli_lat_1
     (
      .clk			(clk),
      .reset			(reset),
      .rd_req_addr_i		(cli_rd_req_addr_i),
      .rd_req_valid_i		(cli_rd_req_valid_i),
      // output clean, clean inv req. (VC 16 or 17).
      .lcl_fwd_wod_hdr_o	(cli_lcl_fwd_wod_o.hdr),
      .lcl_fwd_wod_pkt_size_o	(cli_lcl_fwd_wod_o.size),
      .lcl_fwd_wod_pkt_vc_o	(cli_lcl_fwd_wod_o.vc),
      .lcl_fwd_wod_pkt_valid_o	(cli_lcl_fwd_wod_o.valid),
      .lcl_fwd_wod_pkt_ready_i	(cli_lcl_fwd_wod_o.ready),
      // input clean, clean inv ack. (VC 18 or 19).
      .lcl_rsp_wod_hdr_i	(cli_lcl_rsp_wod_i.hdr),
      .lcl_rsp_wod_pkt_size_i	(cli_lcl_rsp_wod_i.size),
      .lcl_rsp_wod_pkt_vc_i	(cli_lcl_rsp_wod_i.vc),
      .lcl_rsp_wod_pkt_valid_i	(cli_lcl_rsp_wod_i.valid),
      .lcl_rsp_wod_pkt_ready_o	(cli_lcl_rsp_wod_i.ready),
      // output unlock. VC 18 or 19.
      .lcl_rsp_wod_hdr_o	(cli_lcl_rsp_wod_o.hdr),
      .lcl_rsp_wod_pkt_size_o	(cli_lcl_rsp_wod_o.size),
      .lcl_rsp_wod_pkt_vc_o	(cli_lcl_rsp_wod_o.vc),
      .lcl_rsp_wod_pkt_valid_o	(cli_lcl_rsp_wod_o.valid),
      .lcl_rsp_wod_pkt_ready_i	(cli_lcl_rsp_wod_o.ready),
      .done_o                   (cli_done)
      );

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports
      clk = '0;
      reset = '1;
      st_cl_idx_i = '0;
      num_cls_i = '0;
      cl_idx_stride_i = '0;
      tput_latency_bar_i = '0;
      en_i = '0;
      ##5;
      reset = 1'b0;
      ##5;
      st_cl_idx_i = 'd1;
      num_cls_i = MAX_NUM_CLS;
      cl_idx_stride_i = STRIDE;
      tput_latency_bar_i = '0;
      en_i = '1;
      ##1;
      en_i = '0;
      ##5;

      wait(num_rsp_recd_o == num_cls_i);
      ##1;
      #500 $finish;
   end

   always @(cli_done) begin
      if(cli_done) begin
	 $display("Min latency: %0d cycles", perf_simTb.cli_lat_1.min_latency_reg);
	 $display("Max latency: %0d cycles", perf_simTb.cli_lat_1.max_latency_reg);
	 #500 $finish;
      end
   end
   // Check if num_cls_i doesnt cross MAX_NUM_CLS
   // Since there is only 1 DCU, it can accommodate
   // Number of sets per DCU * Number of ways per set
   // cache lines. anything above will result in TSU FULL error. 
   always_ff @(posedge clk) begin : CHECK_NUM_CLS_I
      assert(num_cls_i <= MAX_NUM_CLS) else
	$fatal(1,"One DCU cannot hold more than %0d Cls, reduce num_cls_i", MAX_NUM_CLS);
   end : CHECK_NUM_CLS_I
endmodule //perf_simTb
`endif
