import eci_cmd_defs::*;

module vc_hilo_routerTb();


   //input output ports 
   //Input signals
   logic 				    clk;
   logic 				    reset;
   logic [ECI_WORD_WIDTH-1:0] 		    ecih_hdr_i;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	    ecih_pkt_size_i;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	    ecih_pkt_vc_i;
   logic 				    ecih_pkt_valid_i;
   logic 				    lo_wod_pkt_ready_i;
   logic 				    hi_wd_pkt_ready_i;

   //Output signals
   logic 				    ecih_pkt_ready_o;
   logic [ECI_WORD_WIDTH-1:0] 		    lo_wod_hdr_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	    lo_wod_pkt_size_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	    lo_wod_pkt_vc_o;
   logic 				    lo_wod_pkt_valid_o;
   logic [ECI_WORD_WIDTH-1:0] 		    hi_wd_hdr_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	    hi_wd_pkt_size_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	    hi_wd_pkt_vc_o;
   logic 				    hi_wd_pkt_valid_o;

   typedef enum logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] {HI2=2, HI3=3, HI4=4, HI5=5,
						   LO6=6, LO7=7, LO8=8, LO9=9,
						   LO10=10, LO11=11
						   } vc_t;

   vc_t my_vc;

   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   assign ecih_pkt_vc_i = my_vc;
   
   //instantiation
   vc_hilo_router vc_hilo_router1 (
				   .clk(clk),
				   .reset(reset),
				   .ecih_hdr_i(ecih_hdr_i),
				   .ecih_pkt_size_i(ecih_pkt_size_i),
				   .ecih_pkt_vc_i(ecih_pkt_vc_i),
				   .ecih_pkt_valid_i(ecih_pkt_valid_i),
				   .lo_wod_pkt_ready_i(lo_wod_pkt_ready_i),
				   .hi_wd_pkt_ready_i(hi_wd_pkt_ready_i),
				   .ecih_pkt_ready_o(ecih_pkt_ready_o),
				   .lo_wod_hdr_o(lo_wod_hdr_o),
				   .lo_wod_pkt_size_o(lo_wod_pkt_size_o),
				   .lo_wod_pkt_vc_o(lo_wod_pkt_vc_o),
				   .lo_wod_pkt_valid_o(lo_wod_pkt_valid_o),
				   .hi_wd_hdr_o(hi_wd_hdr_o),
				   .hi_wd_pkt_size_o(hi_wd_pkt_size_o),
				   .hi_wd_pkt_vc_o(hi_wd_pkt_vc_o),
				   .hi_wd_pkt_valid_o(hi_wd_pkt_valid_o)
				   );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports 
      clk = '0;
      reset = '1;
      ecih_hdr_i = '0;
      ecih_pkt_size_i = '0;
      //ecih_pkt_vc_i = '0;
      if(randomize(my_vc) === 0) begin
	 $fatal(1, "Randomize failed");
      end
      ecih_pkt_valid_i = '0;
      lo_wod_pkt_ready_i = '0;
      hi_wd_pkt_ready_i = '0;

      ##5;
      reset = 1'b0;
      lo_wod_pkt_ready_i = '1;
      hi_wd_pkt_ready_i = '1;

      ##5;
      repeat(10) begin
	 launch_random_wait();
      end

      $display("Success: All tests pass");
      #500 $finish;
   end 

   always_ff @(posedge clk) begin : CHECK_RESULTS
      if(lo_wod_pkt_valid_o & lo_wod_pkt_ready_i) begin
	 assert(
		(lo_wod_pkt_vc_o === LO6) |
		(lo_wod_pkt_vc_o === LO7) |
		(lo_wod_pkt_vc_o === LO8) |
		(lo_wod_pkt_vc_o === LO9) |
		(lo_wod_pkt_vc_o === LO10) |
		(lo_wod_pkt_vc_o === LO11)
		) else
	   $fatal(1,"VC does not match any of the LO vcs.");
	 assert(hi_wd_pkt_valid_o === 1'b0) else
	   $fatal(1, "Hi channel should not be valid");
      end
      if(hi_wd_pkt_valid_o & hi_wd_pkt_ready_i) begin
	 assert(
		(hi_wd_pkt_vc_o === HI2) |
		(hi_wd_pkt_vc_o === HI3) |
		(hi_wd_pkt_vc_o === HI4) |
		(hi_wd_pkt_vc_o === HI5)
		) else
	   $fatal(1,"VC does not match any of the HI vcs.");
	 assert(lo_wod_pkt_valid_o === 1'b0) else
	   $fatal(1, "Hi channel should not be valid");
      end
   end : CHECK_RESULTS
   
   task static launch_random_wait();
      launch_random();
      wait(ecih_pkt_valid_i & ecih_pkt_ready_o);
      ##1;
      ecih_pkt_valid_i = 1'b0;
      ##1;
   endtask //launch_random

   task static launch_random();
      ecih_hdr_i = $urandom_range(10,200);
      ecih_pkt_size_i = $urandom_range(1, 17);
      if(randomize(my_vc) == 0) begin
	 $fatal(1, "Randomize failed");
      end
      ecih_pkt_valid_i = 1'b1;
   endtask //launch_random
endmodule
