#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
      $XILINX_VIVADO/data/verilog/src/glbl.v \
      ../rtl/eci_cmd_defs.sv \
      ../rtl/eci_dcs_defs.sv \
      ../testbench/wr_data_pathTb.sv \
      ../rtl/dp_data_store.sv \
      ../rtl/dp_gen_path.sv \
      ../rtl/map_ecid_to_wrd.sv \
      ../rtl/wr_data_path.sv \
      ../rtl/dp_gate.sv \
      ../rtl/dp_mem.sv \
      ../rtl/dp_wr_ser.sv \
      ../rtl/vr_pipe_stage.sv \
      ../rtl/axis_pipeline_stage.sv 

xelab -debug typical -incremental -L xpm worklib.wr_data_pathTb worklib.glbl -s worklib.wr_data_pathTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.wr_data_pathTb 
xsim -gui worklib.wr_data_pathTb 
