/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-09-15
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */

`ifndef HILO_VC_ROUTER_SV
`define HILO_VC_ROUTER_SV

import eci_cmd_defs::*;

// Module takes a ECI header and routes
// to to appropriate VC channel.
// Incoming header passes through a pipeline stage.

module hilo_vc_router 
  (
   input logic 				    clk,
   input logic 				    reset,
   // Input ECI header + VC number.
   input logic [ECI_WORD_WIDTH-1:0] 	    ecih_hdr_i,
   input logic [ECI_PACKET_SIZE_WIDTH-1:0]  ecih_pkt_size_i,
   input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0]  ecih_pkt_vc_i,
   input logic 				    ecih_pkt_valid_i,
   output logic 			    ecih_pkt_ready_o,
   // Output ECI header for req without data: VC 6/7.
   output logic [ECI_WORD_WIDTH-1:0] 	    req_wod_hdr_o,
   output logic [ECI_PACKET_SIZE_WIDTH-1:0] req_wod_pkt_size_o,
   output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] req_wod_pkt_vc_o,
   output logic 			    req_wod_pkt_valid_o,
   input logic 				    req_wod_pkt_ready_i,
   // Output ECI header for rsp without data: VC 10/11
   output logic [ECI_WORD_WIDTH-1:0] 	    rsp_wod_hdr_o,
   output logic [ECI_PACKET_SIZE_WIDTH-1:0] rsp_wod_pkt_size_o,
   output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] rsp_wod_pkt_vc_o,
   output logic 			    rsp_wod_pkt_valid_o,
   input logic 				    rsp_wod_pkt_ready_i,
   // Output ECI header for rsp with data: VC 4/5.
   output logic [ECI_WORD_WIDTH-1:0] 	    rsp_wd_hdr_o,
   output logic [ECI_PACKET_SIZE_WIDTH-1:0] rsp_wd_pkt_size_o,
   output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] rsp_wd_pkt_vc_o,
   output logic 			    rsp_wd_pkt_valid_o,
   input logic 				    rsp_wd_pkt_ready_i
   );

   localparam PIPE_DATA_WIDTH = ECI_WORD_WIDTH + ECI_PACKET_SIZE_WIDTH + ECI_LCL_TOT_NUM_VCS_WIDTH;
   
   typedef struct packed {
      logic [ECI_WORD_WIDTH-1:0]        hdr;
      logic [ECI_PACKET_SIZE_WIDTH-1:0] size;
      logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] vc;
      logic				valid;
      logic				ready;
   } eci_hdr_if_t;

   eci_hdr_if_t ecih_p_i, ecih_p_o;
   logic req_wod_hs, rsp_wod_hs, rsp_wd_hs;

   always_comb begin : MISC_ASSIGN
      req_wod_hs = req_wod_pkt_valid_o & req_wod_pkt_ready_i;
      rsp_wod_hs = rsp_wod_pkt_valid_o & rsp_wod_pkt_ready_i;
      rsp_wd_hs  = rsp_wd_pkt_valid_o  & rsp_wd_pkt_ready_i;
   end : MISC_ASSIGN

   always_comb begin : OUT_ASSIGN
      ecih_pkt_ready_o	 = ecih_p_i.ready;
      // Since only one output channel will be
      // valid at a time, broadcast ecih from
      // output of pipeline stage to all output
      // channel.s
      req_wod_hdr_o	 = ecih_p_o.hdr;
      req_wod_pkt_size_o = ecih_p_o.size;
      req_wod_pkt_vc_o	 = ecih_p_o.vc;
      rsp_wod_hdr_o	 = ecih_p_o.hdr;
      rsp_wod_pkt_size_o = ecih_p_o.size;
      rsp_wod_pkt_vc_o	 = ecih_p_o.vc;
      rsp_wd_hdr_o	 = ecih_p_o.hdr;
      rsp_wd_pkt_size_o  = ecih_p_o.size;
      rsp_wd_pkt_vc_o	 = ecih_p_o.vc;
   end : OUT_ASSIGN

   // The input upstream channel feeds into a pipeline stage of II = 1.
   // This is done to make chaining of multiple of these units together as it
   // registers the output ready and valid signals.
   //
   // Only one of the downstream channels can be valid at a time and when
   // a downstream handshake happens it will happen only on the valid channel.
   // so the output of this pipeline stage is consumed when a handshake happens
   // on any of the output channels. 
   always_comb begin : PIPE_IP_ASSIGN
      ecih_p_i.hdr = ecih_hdr_i;
      ecih_p_i.size = ecih_pkt_size_i;
      ecih_p_i.vc = ecih_pkt_vc_i;
      ecih_p_i.valid = ecih_pkt_valid_i;
      // handshake in any output channel
      // consumes the output of pipeline stage.
      ecih_p_o.ready = req_wod_hs | rsp_wod_hs | rsp_wd_hs;
   end : PIPE_IP_ASSIGN
   axis_pipeline_stage #
     (
      .DATA_WIDTH(PIPE_DATA_WIDTH)
      )
   rtr_in_stage
     (
      .clk(clk),
      .reset(reset),
      .us_data({ecih_p_i.hdr, ecih_p_i.size, ecih_p_i.vc}),
      .us_valid(ecih_p_i.valid),
      .us_ready(ecih_p_i.ready),
      .ds_data({ecih_p_o.hdr, ecih_p_o.size, ecih_p_o.vc}),
      .ds_valid(ecih_p_o.valid),
      .ds_ready(ecih_p_o.ready)
      );

   // When upstream handshake happens, the data would be registered in the
   // pipeline stage next cycle along with the select VC signal.
   // Based on this select signal enable only one of the output channels.
   always_comb begin : CONTROLLER
      req_wod_pkt_valid_o = 1'b0;
      rsp_wod_pkt_valid_o = 1'b0;
      rsp_wd_pkt_valid_o = 1'b0;
      if(ecih_p_o.vc < 'd6) begin
	 // VC 4/5, rsp with data.
	 req_wod_pkt_valid_o = 1'b0;
	 rsp_wod_pkt_valid_o = 1'b0;
	 rsp_wd_pkt_valid_o = ecih_p_o.valid;
      end else if(ecih_p_o.vc < 'd10) begin
	 // VC 6/7, req without data.
	 req_wod_pkt_valid_o = ecih_p_o.valid;
	 rsp_wod_pkt_valid_o = 1'b0;
	 rsp_wd_pkt_valid_o = 1'b0;
      end else begin
	 // VC 10/11, rsp without data.
	 req_wod_pkt_valid_o = 1'b0;
	 rsp_wod_pkt_valid_o = ecih_p_o.valid;
	 rsp_wd_pkt_valid_o = 1'b0;
      end
   end : CONTROLLER
endmodule // hilo_vc_router
`endif
