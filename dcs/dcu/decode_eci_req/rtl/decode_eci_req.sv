/*
 * Systems Group, D-INFK, ETH Zurich
 *
 * Author  : A.Ramdas
 * Date    : 2021-12-13
 * Project : Enzian
 *
 */

`ifndef DECODE_ECI_REQ_SV
 `define DECODE_ECI_REQ_SV

import eci_cmd_defs::*;
import eci_cc_defs::*;
import eci_dirc_defs::*;

/*
 * Module Description:
 *  Take input eci event header and VC# to decode the ECI event recd.
 *  The VC along with the opcode field in the header determines the event.
 *
 *  The event is considered invalid if either VC is not defined or if opcode
 *  within the VC is not defined.
 *
 *  Currently supports only 3 VCs
 *    VC request w/o data.
 *    VC response w/o data.
 *    VC response with data.
 *
 *
 * Input Description:
 *  Header of ECI event.
 *  VC number of ECI event.
 *
 * Output Description:
 *
 *  eci_req_trid_o            : Transaction ID from header.
 *  eci_req_addr_o	      : CL address (index + byte offset).
 *  eci_req_dmask_o	      : dmask from header.
 *  eci_req_cl_idx_oe_o	      : Is CL index odd or even.
 *  eci_req_cc_enc_o	      : ECI event encoded according to CC definitions.
 *  eci_req_invalid_o	      : 1 if unable to decode the eci event.
 *  eci_req_crts_rtg_ntry_o   : 1 if ECI event can potentially create an RTG entry.
 *                            : 1 for R12, R13, RR, RW.
 *  eci_rsp_2_prv_snt_req_o   : 1 if ECI event is a ressponse to previously sent request.
 *                            : 1 for A32d, A31d, A22, A21, A11.
 *
 * Architecture Description:
 *  Simple combinational decoder using case statements.
 *
 * Modifiable Parameters:
 *  None.
 *
 * Non-modifiable Parameters:
 *  None.
 *
 * Modules Used:
 *  None.
 *
 * Notes:
 *  Combinational.
 *
 */


// IMPORTANT
// IF ADDING VC_REQ_W_DATA MAKE SURE RETRY BUFFERS SIMILAR TO THOSE OF VC_REQ_WO_DATA ARE ADDED.
// BECAUSE STATE MACHINE WOULD DELAY THESE EVENTS FOR LATER.

module decode_eci_req
  (
   input logic				   clk,
   input logic				   reset,

   // Input ECI events.
   input logic [ECI_WORD_WIDTH-1:0]	   eci_req_hdr_i,
   input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] eci_req_vc_i,

   // Output decoded ECI event.
   output				   eci_hreqid_t eci_req_trid_o,
   output				   eci_address_t eci_req_addr_o,
   output				   eci_dmask_t eci_req_dmask_o,
   // is CL index odd or even
   output logic				   eci_req_cl_idx_oe_o,
   output				   cc_req_t eci_req_cc_enc_o,
   // 1 if unable to decode the request
   output logic				   eci_req_invalid_o,
   // ECI event can potentially create an RTG entry
   // 1 for R12, R13, RR, RW
   output logic				   eci_req_crts_rtg_ntry_o,
   // ECI event is a ressponse to previously sent request
   // 1 for A32d, A31d, A22, A21, A11
   output logic				   eci_rsp_2_prv_snt_req_o
   );

   eci_word_t eci_req_hdr;

   assign eci_req_hdr = eci_word_t'(eci_req_hdr_i);

   always_comb begin : DECODER
      // Based on VC and opcode determine the incoming ECI event.
      // extract relevant fields from ECI and encode the CC equivalent of the event.

      case(eci_req_vc_i)
	VC_REQ_WO_DATA_E, VC_REQ_WO_DATA_O : begin
	   // RR, R12, R13, R23
	   case(eci_req_hdr.generic_cmd.opcode)

	     // RR
	     ECI_CMD_MREQ_RLDT: begin
		latch_eci_data(
			       // rreq_id is 5 bits hreq_id is 6 bits and we choose the
			       // bigger width always
			       .trid_i(eci_hreqid_t'({1'b0, eci_req_hdr.mreq_0to10.rreq_id})),
			       .addr_i(eci_req_hdr.mreq_0to10.address),
			       .dmask_i(eci_req_hdr.mreq_0to10.dmask),
			       .enc_req_i(RR),
			       .req_valid_i(1'b1)
			       );
	     end

	     // R12
	     ECI_CMD_MREQ_RLDI, ECI_CMD_MREQ_RLDD: begin
		latch_eci_data(
			       // rreq_id is 5 bits hreq_id is 6 bits and we choose the
			       // bigger width always
			       .trid_i(eci_hreqid_t'({1'b0, eci_req_hdr.mreq_0to10.rreq_id})),
			       .addr_i(eci_req_hdr.mreq_0to10.address),
			       .dmask_i(eci_req_hdr.mreq_0to10.dmask),
			       .enc_req_i(R12),
			       .req_valid_i(1'b1)
			       );
	     end

	     // R13
	     ECI_CMD_MREQ_RLDX: begin
		// RLDD can be treated as R12 or R13, RLDD can accept RA2 as well as RA3.
		latch_eci_data(
			       // rreq_id is 5 bits hreq_id is 6 bits and we choose the
			       // bigger width always
			       .trid_i(eci_hreqid_t'({1'b0, eci_req_hdr.mreq_0to10.rreq_id})),
			       .addr_i(eci_req_hdr.mreq_0to10.address),
			       .dmask_i(eci_req_hdr.mreq_0to10.dmask),
			       .enc_req_i(R13),
			       .req_valid_i(1'b1)
			       );
	     end

	     //R23
	     ECI_CMD_MREQ_RC2D_S: begin
		latch_eci_data(
			       // rreq_id is 5 bits hreq_id is 6 bits and we choose the
			       // bigger width always
			       .trid_i(eci_hreqid_t'({1'b0, eci_req_hdr.mreq_0to10.rreq_id})),
			       .addr_i(eci_req_hdr.mreq_0to10.address),
			       .dmask_i(eci_req_hdr.mreq_0to10.dmask),
			       .enc_req_i(R23),
			       .req_valid_i(1'b1)
			       );
	     end

	     // Opcode is not defined for given VC, invalid request.
	     default: begin
		//$error("Error: Does not support opcode %b in VC_REQ_WO_DATA VCs(instance %m)", eci_req_hdr.generic_cmd.opcode);
		latch_eci_data(
			       .trid_i('0),
			       .addr_i('0),
			       .dmask_i('0),
			       .enc_req_i(UNKNOWN_REQ),
			       .req_valid_i(1'b0)
			       );
	     end

	   endcase // case (eci_req_hdr.generic_cmd.opcode)
	end // VC_REQ_WO_DATA_E, VC_REQ_WO_DATA_O


	VC_RESP_WO_DATA_E, VC_RESP_WO_DATA_O : begin
	   // VICS, VICC.N, VICD.N (V21, V32, V31)
	   // VICDHI_N, HAKD_N, HAKN_S, HAKS, HAKV, HAKI (A31, A21, A32, A22, A11, A11)
	   // Note: HAKV and HAKI are both A11 it is not a mistake
	   case(eci_req_hdr.generic_cmd.opcode)

	     //V21
	     ECI_CMD_MRSP_VICS: begin
		latch_eci_data(
			       //has no transaction ID, defaulting to 0
			       .trid_i('0),
			       .addr_i(eci_req_hdr.vics.address),
			       .dmask_i(eci_req_hdr.vics.dmask),
			       .enc_req_i(V21),
			       .req_valid_i(1'b1)
			       );
	     end

	     //V32
	     ECI_CMD_MRSP_VICC: begin
		latch_eci_data(
			       //has no transaction ID, defaulting to 0
			       .trid_i('0),
			       .addr_i(eci_req_hdr.vicc.address),
			       .dmask_i(eci_req_hdr.vicc.dmask),
			       .enc_req_i(V32),
			       .req_valid_i(1'b1)
			       );
	     end

	     //V31
	     ECI_CMD_MRSP_VICD: begin
		latch_eci_data(
			       //has no transaction ID, defaulting to 0
			       .trid_i('0),
			       .addr_i(eci_req_hdr.vicd.address),
			       .dmask_i(eci_req_hdr.vicd.dmask),
			       .enc_req_i(V31),
			       .req_valid_i(1'b1)
			       );
	     end

	     //A31
	     ECI_CMD_MRSP_VICDHI: begin
		latch_eci_data(
			       .trid_i(eci_req_hdr.mrsp_3to8.hreq_id),
			       .addr_i(eci_req_hdr.mrsp_3to8.address),
			       .dmask_i(eci_req_hdr.mrsp_3to8.dmask),
			       .enc_req_i(A31),
			       .req_valid_i(1'b1)
			       );
	     end

	     // A21
	     ECI_CMD_MRSP_HAKD: begin
		latch_eci_data(
			       .trid_i(eci_req_hdr.mrsp_3to8.hreq_id),
			       .addr_i(eci_req_hdr.mrsp_3to8.address),
			       .dmask_i(eci_req_hdr.mrsp_3to8.dmask),
			       .enc_req_i(A21),
			       .req_valid_i(1'b1)
			       );
	     end

	     //A32
	     ECI_CMD_MRSP_HAKN_S: begin
		latch_eci_data(
			       .trid_i(eci_req_hdr.mrsp_3to8.hreq_id),
			       .addr_i(eci_req_hdr.mrsp_3to8.address),
			       .dmask_i(eci_req_hdr.mrsp_3to8.dmask),
			       .enc_req_i(A32),
			       .req_valid_i(1'b1)
			       );
	     end

	     //A22
	     ECI_CMD_MRSP_HAKS: begin
		latch_eci_data(
			       .trid_i(eci_req_hdr.mrsp_3to8.hreq_id),
			       .addr_i(eci_req_hdr.mrsp_3to8.address),
			       .dmask_i(eci_req_hdr.mrsp_3to8.dmask),
			       .enc_req_i(A22),
			       .req_valid_i(1'b1)
			       );
	     end


	     // A11, as is HAKI
	     ECI_CMD_MRSP_HAKV: begin
		latch_eci_data(
			       .trid_i(eci_req_hdr.mrsp_3to8.hreq_id),
			       .addr_i(eci_req_hdr.mrsp_3to8.address),
			       .dmask_i(eci_req_hdr.mrsp_3to8.dmask),
			       .enc_req_i(A11),
			       .req_valid_i(1'b1)
			       );
	     end

	     // A11, as is HAKV
	     ECI_CMD_MRSP_HAKI: begin
		latch_eci_data(
			       .trid_i(eci_req_hdr.mrsp_3to8.hreq_id),
			       .addr_i(eci_req_hdr.mrsp_3to8.address),
			       .dmask_i(eci_req_hdr.mrsp_3to8.dmask),
			       .enc_req_i(A11),
			       .req_valid_i(1'b1)
			       );
	     end

	     // Opcode not defined for this VC, invalid event.
	     default: begin
		//$error("Error: Does not support opcode %b in VC_RESP_WO_DATA VCs(instance %m)", eci_req_hdr.generic_cmd.opcode);
		latch_eci_data(
			       .trid_i('0),
			       .addr_i('0),
			       .dmask_i('0),
			       .enc_req_i(UNKNOWN_REQ),
			       .req_valid_i(1'b0)
			       );

	     end

	   endcase // case (eci_req_hdr.generic_cmd.opcode)
	end // VC_RESP_WO_DATA_E, VC_RESP_WO_DATA_O

	VC_RESP_W_DATA_E, VC_RESP_W_DATA_O : begin
	   // VICD, VICC, (V31d, V32d)
	   // VICDHI, HAKD (A31d, A32d)
	   case(eci_req_hdr.generic_cmd.opcode)

	     //V32d
	     ECI_CMD_MRSP_VICC: begin
		latch_eci_data(
			       //has no transaction ID, defaulting to 0
			       .trid_i('0),
			       .addr_i(eci_req_hdr.vicc.address),
			       .dmask_i(eci_req_hdr.vicc.dmask),
			       .enc_req_i(V32d),
			       .req_valid_i(1'b1)
			       );
	     end

	     //V31d
	     ECI_CMD_MRSP_VICD: begin
		latch_eci_data(
			       //has no transaction ID, defaulting to 0
			       .trid_i('0),
			       .addr_i(eci_req_hdr.vicd.address),
			       .dmask_i(eci_req_hdr.vicd.dmask),
			       .enc_req_i(V31d),
			       .req_valid_i(1'b1)
			       );
	     end

	     //A31d
	     ECI_CMD_MRSP_VICDHI: begin
		latch_eci_data(
			       .trid_i(eci_req_hdr.mrsp_3to8.hreq_id),
			       .addr_i(eci_req_hdr.mrsp_3to8.address),
			       .dmask_i(eci_req_hdr.mrsp_3to8.dmask),
			       .enc_req_i(A31d),
			       .req_valid_i(1'b1)
			       );
	     end

	     //A32d
	     ECI_CMD_MRSP_HAKD: begin
		latch_eci_data(
			       .trid_i(eci_req_hdr.mrsp_3to8.hreq_id),
			       .addr_i(eci_req_hdr.mrsp_3to8.address),
			       .dmask_i(eci_req_hdr.mrsp_3to8.dmask),
			       .enc_req_i(A32d),
			       .req_valid_i(1'b1)
			       );
	     end

	     // Opcode not defined for this VC, invalid event.
	     default: begin
		// $error("Error: Does not support opcode %b in VC_RESP_W_DATA VCs(instance %m)", eci_req_hdr.generic_cmd.opcode);
		latch_eci_data(
			       .trid_i('0),
			       .addr_i('0),
			       .dmask_i('0),
			       .enc_req_i(UNKNOWN_REQ),
			       .req_valid_i(1'b0)
			       );
	     end

	   endcase // case (eci_req_hdr.generic_cmd.opcode)
	end // case: VC_RESP_W_DATA_E, VC_RESP_W_DATA_O

	VC_LCL_FWD_WO_DATA_E, VC_LCL_FWD_WO_DATA_O: begin
	   // LCL_CMD_MWD_CLEAN, LCL_CMD_MWD_CLEAN_INV
	   case(eci_req_hdr.generic_cmd.opcode)
	     LCL_CMD_MFWD_CLEAN: begin
		latch_eci_data(
			       .trid_i(eci_req_hdr.lcl_mfwd_generic.hreq_id),
			       .addr_i(eci_req_hdr.lcl_mfwd_generic.address),
			       .dmask_i(eci_req_hdr.lcl_mfwd_generic.dmask),
			       .enc_req_i(LC),
			       .req_valid_i(1'b1)
			       );
	     end
	     LCL_CMD_MFWD_CLEAN_INV: begin
		latch_eci_data(
			       .trid_i(eci_req_hdr.lcl_mfwd_generic.hreq_id),
			       .addr_i(eci_req_hdr.lcl_mfwd_generic.address),
			       .dmask_i(eci_req_hdr.lcl_mfwd_generic.dmask),
			       .enc_req_i(LCI),
			       .req_valid_i(1'b1)
			       );
	     end
	     // opcode not defined for this VC, invalid event.
	     default: begin
		// $error("Error: Does not support opcode %b in VC_LCL_FWD_WO_DATA VCs(instance %m)", eci_req_hdr.generic_cmd.opcode);
		latch_eci_data(
			       .trid_i('0),
			       .addr_i('0),
			       .dmask_i('0),
			       .enc_req_i(UNKNOWN_REQ),
			       .req_valid_i(1'b0)
			       );
	     end
	   endcase // case (eci_req_hdr.generic_cmd.opcode)
	end // case: VC_LCL_FWD_WO_DATA_E, VC_LCL_FWD_WO_DATA_O

	VC_LCL_RESP_WO_DATA_E, VC_LCL_RESP_WO_DATA_O: begin
	   // LCL_CMD_MRSP_UNLOCK
	   case(eci_req_hdr.generic_cmd.opcode)
	     LCL_CMD_MRSP_UNLOCK: begin
		latch_eci_data(
			       .trid_i('0),
			       .addr_i(eci_req_hdr.lcl_unlock.address),
			       .dmask_i('0),
			       .enc_req_i(UL),
			       .req_valid_i(1'b1)
			       );
	     end
	     // opcode not defined for this VC, invalid event.
	     default: begin
		latch_eci_data(
			       .trid_i('0),
			       .addr_i('0),
			       .dmask_i('0),
			       .enc_req_i(UNKNOWN_REQ),
			       .req_valid_i(1'b0)
			       );
	     end
	   endcase // case (eci_req_hdr.generic_cmd.opcode)
	end

	// Add new VCs here

	// VC itself is not defined, invalid event.
	default: begin
	   //$error("Error: VC %d not defined (instance %m)", eci_req_vc_i);
	   latch_eci_data(
			  .trid_i('0),
			  .addr_i('0),
			  .dmask_i('0),
			  .enc_req_i(UNKNOWN_REQ),
			  .req_valid_i(1'b0)
			  );

	end

      endcase // case (eci_req_vc_i)
   end : DECODER


   // Assign decoded values to output signals.
   task static latch_eci_data (
			       input	   eci_hreqid_t trid_i,
			       input	   eci_address_t addr_i,
			       input	   eci_dmask_t dmask_i,
			       input	   cc_req_t enc_req_i,
			       input logic req_valid_i
			       );

      eci_cl_addr_t cl_addr;
      eci_req_trid_o = trid_i;
      eci_req_addr_o = {addr_i[ECI_ADDR_WIDTH-1:7], 7'b0000000}; // lower 7 bits are always 0, we can ignore fillo bits (5th and 6th bit) and send always 0s in a response
      eci_req_dmask_o = dmask_i;
      eci_req_cc_enc_o = enc_req_i;
      eci_req_invalid_o = ~req_valid_i;
      // cl index is odd then 1 even then 0
      cl_addr = eci_cl_addr_t'(addr_i);
      eci_req_cl_idx_oe_o = (cl_addr.parts.cl_index[0] == 1'b1) ? 1'b1 : 1'b0;
      // If you are adding new entries to eci_cc_defs make sure the auto generation script is updated
      // does event create new RTG entry
      eci_req_crts_rtg_ntry_o = eci_cc_defs::cc_req_creates_new_rtg_entry(.cc_req_i(enc_req_i));
      //is event a response to previously sent request
      eci_rsp_2_prv_snt_req_o = eci_cc_defs::cc_evt_rsp2req(.cc_evt_i(enc_req_i));
   endtask //latch_eci_data

endmodule // decode_eci_req

`endif
