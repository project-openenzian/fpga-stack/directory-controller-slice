component dcs_single_dcu_2_axi is
port (
  clk, reset : in std_logic;

  -- Input ECI events.
  -- ECI packet for request without data. (VC 6 or 7) (only header).
  -- ECI packet for response without data.(VC 10 or 11). (only header).
  lo_wod_hdr_i       : in std_logic_vector(63 downto 0);
  lo_wod_pkt_size_i  : in std_logic_vector( 4 downto 0);
  lo_wod_pkt_vc_i    : in std_logic_vector( 4 downto 0);
  lo_wod_pkt_valid_i : in std_logic;
  lo_wod_pkt_ready_o : out std_logic;

  -- ECI packet for response with data. (VC 4 or 5). (header + data).
  hi_wd_pkt_i        : in std_logic_vector(17*64-1 downto 0);
  hi_wd_pkt_size_i   : in std_logic_vector( 4 downto 0);
  hi_wd_pkt_vc_i     : in std_logic_vector( 4 downto 0);
  hi_wd_pkt_valid_i  : in std_logic;
  hi_wd_pkt_ready_o  : out std_logic;


  -- Output ECI events. (rsp without data, rsp with data).
  -- VC 10,11
  lo_wod_hdr_o       : out std_logic_vector(63 downto 0);
  lo_wod_pkt_size_o  : out std_logic_vector( 4 downto 0);
  lo_wod_pkt_vc_o    : out std_logic_vector( 4 downto 0);
  lo_wod_pkt_valid_o : out std_logic;
  lo_wod_pkt_ready_i : in std_logic;

  -- Responses with data (VC 5 or 4)
  -- header+payload
  hi_wd_pkt_o       : out std_logic_vector(17*64-1 downto 0);
  hi_wd_pkt_size_o  : out std_logic_vector( 4 downto 0);
  hi_wd_pkt_vc_o    : out std_logic_vector( 4 downto 0);
  hi_wd_pkt_valid_o : out std_logic;
  hi_wd_pkt_ready_i : in std_logic;

  -- Primary AXI rd/wr i/f.
  p_axi_arid    : out std_logic_vector( 6 downto 0);
  p_axi_araddr  : out std_logic_vector(37 downto 0);
  p_axi_arlen   : out std_logic_vector( 7 downto 0);
  p_axi_arsize  : out std_logic_vector( 2 downto 0);
  p_axi_arburst : out std_logic_vector( 1 downto 0);
  p_axi_arlock  : out std_logic;
  p_axi_arcache : out std_logic_vector( 3 downto 0);
  p_axi_arprot  : out std_logic_vector( 2 downto 0);
  p_axi_arvalid : out std_logic;
  p_axi_arready : in std_logic;
  p_axi_rid     : in std_logic_vector( 6 downto 0);
  p_axi_rdata   : in std_logic_vector(511 downto 0);
  p_axi_rresp   : in std_logic_vector( 1 downto 0);
  p_axi_rlast   : in std_logic;
  p_axi_rvalid  : in std_logic;
  p_axi_rready  : out std_logic;

  p_axi_awid    : out std_logic_vector ( 6 downto 0);
  p_axi_awaddr  : out std_logic_vector (37 downto 0);
  p_axi_awlen   : out std_logic_vector ( 7 downto 0);
  p_axi_awsize  : out std_logic_vector ( 2 downto 0);
  p_axi_awburst : out std_logic_vector ( 1 downto 0);
  p_axi_awlock  : out std_logic;
  p_axi_awcache : out std_logic_vector ( 3 downto 0);
  p_axi_awprot  : out std_logic_vector ( 2 downto 0);
  p_axi_awvalid : out std_logic;
  p_axi_awready : in std_logic ;
  p_axi_wdata   : out std_logic_vector (511 downto 0);
  p_axi_wstrb   : out std_logic_vector (63 downto 0);
  p_axi_wlast   : out std_logic;
  p_axi_wvalid  : out std_logic;
  p_axi_wready  : in std_logic;
  p_axi_bid     : in std_logic_vector( 6 downto 0);
  p_axi_bresp   : in std_logic_vector( 1 downto 0);
  p_axi_bvalid  : in std_logic;
  p_axi_bready  : out std_logic
);
end component;
