`ifndef AXIS_XPM_FIFO_SV
`define AXIS_XPM_FIFO_SV

module axis_xpm_fifo #
  (
   parameter DATA_WIDTH = 64,
   parameter FIFO_DEPTH = 16,
   parameter SIDE_BAND_WIDTH = 2
   )
   (
    input logic 		       clk,
    input logic 		       reset,
    // Incoming upstream data. 
    input logic [DATA_WIDTH-1:0]       us_data,
    input logic 		       us_valid,
    output logic 		       us_ready,
    // Outgoing downstream data.
    output logic [DATA_WIDTH-1:0]      ds_data,
    output logic 		       ds_valid,
    input logic 		       ds_ready,
    // sideband signals.
    // always valid.
    output logic [SIDE_BAND_WIDTH-1:0] ff_sideband_o   
    );

   typedef struct 		  packed {
      // format - logic abc;
      logic 			  full;  // bit 1
      logic 			  empty; // bit 0
   } sideband_t;
   
   // FIFO signals.
   logic 			  almost_empty;
   logic 			  almost_full;
   logic 			  data_valid;
   logic 			  dbiterr;
   logic [DATA_WIDTH-1:0] 	  dout;
   logic 			  empty;
   logic 			  full;
   logic 			  overflow;
   logic 			  prog_empty;
   logic 			  prog_full;
   logic 			  rd_data_count;
   logic 			  rd_rst_busy;
   logic 			  sbiterr;
   logic 			  underflow;
   logic 			  wr_ack;
   logic 			  wr_data_count;
   logic 			  wr_rst_busy;
   logic [DATA_WIDTH-1:0] 	  din;
   logic 			  injectdbiterr;
   logic 			  injectsbiterr;
   logic 			  rd_en;
   logic 			  sleep;
   logic 			  wr_en;
   sideband_t ff_sideband;

   initial begin
      if((FIFO_DEPTH < 16)) begin
	 $error("Error: FIFO depth should be atleast 16 (instance %m)");
	 $finish;
      end
   end

   always_comb begin : OP_ASSIGN
      us_ready = ~full & ~wr_rst_busy & ~rd_rst_busy;
      ds_data = dout;
      ds_valid = ~empty & ~wr_rst_busy & ~rd_rst_busy;
      ff_sideband.empty = empty;
      ff_sideband.full = full;
      ff_sideband_o = '0;
      ff_sideband_o[1:0] = ff_sideband;
   end : OP_ASSIGN
   
   always_comb begin : FIFO_IP_ASSIGN
      din = us_data;
      rd_en = ds_valid & ds_ready;
      wr_en = us_valid & us_ready;
      injectdbiterr = '0;
      injectsbiterr = '0;
      sleep = '0;
   end : FIFO_IP_ASSIGN
   xpm_fifo_sync #(
		   //.CASCADE_HEIGHT(0),        // DECIMAL
		   .DOUT_RESET_VALUE("0"),    // String
		   .ECC_MODE("no_ecc"),       // String
		   .FIFO_MEMORY_TYPE("auto"), // String
		   .FIFO_READ_LATENCY(1),     // DECIMAL
		   .FIFO_WRITE_DEPTH(FIFO_DEPTH),   // DECIMAL
		   .FULL_RESET_VALUE(0),      // DECIMAL
		   .PROG_EMPTY_THRESH(10),    // DECIMAL
		   .PROG_FULL_THRESH(10),     // DECIMAL
		   .RD_DATA_COUNT_WIDTH(1),   // DECIMAL
		   .READ_DATA_WIDTH(DATA_WIDTH),      // DECIMAL
		   .READ_MODE("fwft"),         // String
		   //.SIM_ASSERT_CHK(0),        // DECIMAL; 0=disable simulation messages, 1=enable simulation messages
		   .USE_ADV_FEATURES("0707"), // String
		   .WAKEUP_TIME(0),           // DECIMAL
		   .WRITE_DATA_WIDTH(DATA_WIDTH),     // DECIMAL
		   .WR_DATA_COUNT_WIDTH(1)    // DECIMAL
		   )
   xpm_fifo_sync_inst (
		       .almost_empty(almost_empty),
		       .almost_full(almost_full),
		       .data_valid(data_valid),
		       .dbiterr(dbiterr),
		       .dout(dout),
		       .empty(empty),
		       .full(full),
		       .overflow(overflow),
		       .prog_empty(prog_empty),
		       .prog_full(prog_full),
		       .rd_data_count(rd_data_count),
		       .rd_rst_busy(rd_rst_busy),
		       .sbiterr(sbiterr),
		       .underflow(underflow),
		       .wr_ack(wr_ack),
		       .wr_data_count(wr_data_count),
		       .wr_rst_busy(wr_rst_busy),
		       .din(din),
		       .injectdbiterr(injectdbiterr),
		       .injectsbiterr(injectsbiterr),
		       .rd_en(rd_en),
		       .rst(reset),
		       .sleep(sleep),
		       .wr_clk(clk),
		       .wr_en(wr_en)
		       );


endmodule // axis_xpm_fifo

`endif
