/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-11-13
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */

`ifndef ECI_TRMGR_SV
`define ECI_TRMGR_SV

// Store information to start an ECI transaction.
// and retrieve later when transaction is completed.
// only 1 on-going transaction allowed.
module eci_trmgr #
  (
   parameter DATA_WIDTH = 64
   )
   (
    input logic			  clk,
    input logic			  reset,

    // store data and
    // mark transaction as on-going.
    input logic [DATA_WIDTH-1:0]  alloc_data_i,
    input logic			  alloc_en_i,

    // Mark transaction complete.
    input logic			  free_en_i,

    // provide stored data and whether
    // a transaction is on-going.
    output logic [DATA_WIDTH-1:0] alloc_data_o,
    output logic		  alloc_valid_o
    );

   logic [DATA_WIDTH-1:0]	 data_reg = '0, data_next;
   logic			 valid_reg = 1'b0, valid_next;

   always_comb begin : OUT_ASSIGN
      alloc_data_o = data_reg;
      alloc_valid_o = valid_reg;
   end : OUT_ASSIGN

   always_comb begin : CONTROLLER
      data_next = data_reg;
      valid_next = valid_reg;
      if(free_en_i) begin
	 valid_next = 1'b0;
      end else if(alloc_en_i) begin
	 valid_next = 1'b1;
	 data_next = alloc_data_i;
      end
   end : CONTROLLER

   always_ff @(posedge clk) begin : REG_ASSIGN
      data_reg	<= data_next;
      valid_reg <= valid_next;
      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset
	 //data_reg <= '0;
	 valid_reg <= '0;
      end
   end : REG_ASSIGN

endmodule // eci_trmgr
`endif
