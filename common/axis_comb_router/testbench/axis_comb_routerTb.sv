
module axis_comb_routerTb();

   parameter DATA_WIDTH = 64;
   parameter NUM_OUT = 32;
   parameter SEL_WIDTH = $clog2(NUM_OUT);

   bit clk, reset;
   
   //input output ports 
   //Input signals
   logic [DATA_WIDTH-1:0] 	       us_data_i;
   logic [SEL_WIDTH-1:0] 	       us_sel_i;
   logic 			       us_valid_i;
   logic [NUM_OUT-1:0] 		       ds_ready_i;

   //Output signals
   logic 			       us_ready_o;
   logic [NUM_OUT-1:0][DATA_WIDTH-1:0] ds_data_o;
   logic [NUM_OUT-1:0] 		       ds_valid_o;


   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   axis_comb_router #
     (
      .NUM_OUT(NUM_OUT)
      )
   axis_comb_router1 (
		      .us_data_i(us_data_i),
		      .us_sel_i(us_sel_i),
		      .us_valid_i(us_valid_i),
		      .ds_ready_i(ds_ready_i),
		      .us_ready_o(us_ready_o),
		      .ds_data_o(ds_data_o),
		      .ds_valid_o(ds_valid_o)
		      );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();
      clk = '0;
      reset = '0;
      // Initialize Input Ports 
      us_data_i = '0;
      us_sel_i = '0;
      us_valid_i = '0;
      ds_ready_i = '0;
      for( integer i=0; i<NUM_OUT; i=i+1 ) begin
	 us_data_i = DATA_WIDTH'(i);
	 us_sel_i = SEL_WIDTH'(i);
	 us_valid_i = 1'b1;
	 ##1;
	 ds_ready_i[i] = 1'b1;
	 wait(us_valid_i & us_ready_o);
	 ##1;
	 us_valid_i = 1'b0;
	 ds_ready_i[i] = 1'b0;
      end
      ##1;
      us_valid_i = 1'b0;

      #500 $finish;
   end 

endmodule
