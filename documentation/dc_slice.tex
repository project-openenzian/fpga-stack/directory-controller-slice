
\section{DC Slice}
\label{sec:dcs}


The \gls{dc} comprises of two \glspl{dcs} where each \gls{dcs} has
a number of \glspl{dcu}. Each \gls{dcu} has a unique \gls{dcu-id} and
is identified by its \gls{dcu-idx} within a \gls{dcs} \textcolor{red}
{Add link to picture here.}. The \gls{dcs}
receives coherence messages of different types from multiple sources.
Each coherence message is associated with a \gls{cl} address, and part
of the \gls{cl} address is the ID of the \gls{dcu} responsible for handling it.
The different types of interfaces exposed by a \gls{dcs} to recieve
coherence messages are as follows: An interface to connect to \gls{eci}
to exchange coherence messages with the CPU, a second interface that
exposes read, write descriptor interface to be converted to \gls{axi}
and connected to the memory, and finally a local interface for applications
on the \gls{fpga} to interact with the coherence protocol through \gls{dcs}.
Each interface is made up of one or more channels with valid-ready
flow control. These interfaces are shown in \autoref{fig:dcs_if}.


\begin{figure}[htbp]
  \centering
  \includegraphics[scale=0.5]{figures/dcs_if}
  \caption{DCS Interface}
  \label{fig:dcs_if}
\end{figure}


The interface to \gls{eci} consists of three incoming and three outgoing
channels. The incoming channels are request-without-data,
response-without-data and response-with-data-channels.
The request-without-data channel receives upgrade requests from the CPU
and both response channels recieve voluntary downgrades as well as
forward-downgrade acknowledgements from the CPU. In contrast, messages
are sent to the CPU via outgoing channels namely forward-without-data,
response-without-data and response-with-data.The forward channel is
used to issue forward downgrade requests to the CPU. The response
channels are used to issue responses to upgrade requests from the
CPU.

In general, all \gls{eci} channels have a 64-bit header (of which the
\gls{cl} address is a part). Each channel is also assigned a \gls{vc}
number (not shown in figure) to distinguish between them.
Channels with data additionally have
up-to 128-bytes of payload. The size of the payload can vary between
1 and 4 sub-cache-lines (each sub-cache-line being 32 bytes), and is
indicated by the \emph{dmask}-field in its header. The dmask-field
also identifies the exact byte-address to which the sub-cache-line
is to be written to. This allows for data that are smaller than
a \gls{cl} to be compressed, thereby reducing the amount of
data flowing through the interconnect. 

The read descriptor interface has an outgoing read request channel and
incoming read response channel. The read request channel is used to
initiate read transactions by issuing the \gls{cl} byte-address to read
along and tagging it with a transaction identifier. An open transaction
is completed when response data, tagged with the transaction identifier,
is received through the read response channel.

The write descriptor interface also has outgoing write request and
incoming write response channels. The write request channel initiates
a write transaction by tagging it with a transaction identifier and
issuing the \gls{cl} byte-address to write to along with write data
and byte-enable strobe signals. Similar to read transactions, write
transactions are also terminated with a matching transaction identifier
is recieved in the write response channel along with information
indicating if the write completed successfully.

It is to be noted that in the current design, write requests
are at the granularity of sub-cache-lines whereas reads
are always at the granularity full of \glspl{cl}.
This is because reading sub-cache-lines is only an
optimization which we decided not to implement. 


Finally, the simplified local interface has an incoming forward-without-data
channel for the \gls{fpga} application to issue clean or clean-invalidate requests,
an outgoing response-without-data channel containing acknowledgements for
previously issued clean, clean-invalidate reqeusts, and an incoming
response-without-data channel for the \gls{fpga} application to unlock previously
locked \glspl{cl}. All channels have a 64-bit header with opcode indicating
the type of oepration and the \gls{cl} address embedded into it. The local
channels also have distinct \gls{vc} numbers associated with each channel. 
There are no data channels in the current iteration of the local interface. 


\subsection{DCS Architecture: Control and Data-Paths}
In order to simplify routing to the numerous \glspl{dcu}, we split information
from these channels into control and data. Control information in
a channel is any information except \gls{cl} data. This includes the 64-bit
headers in \gls{eci} and local interfaces, as well as read/write response transaction
IDs from the memory interface. The \emph{control-path} 
routes control information to the \glspl{dcu} where as \emph{data-path}
can be used to store and retrieve data as shown in \autoref{fig:dcs_internals}.

\begin{figure}[htbp]
  \centering
  \includegraphics[scale=0.5]{figures/dcs_internals}
  \caption{DC Slice control and data-path}
  \label{fig:dcs_internals}
\end{figure}

The \gls{dcs} has two data-paths, the write-data-path
to hold data that is to be written to the memory and the
read-data-path to hold data read from the memory. 
Each \gls{dcu} in a \gls{dcs} has one slot in the data-path buffer
(indexed by the \gls{dcu-idx}) to store and retrieve data. 

On the write-data-path, we allow only responses
(with data) from the CPU to be written to the memory through
the \gls{dcs}. This means that if an \gls{fpga}
application wants to write to the memory, it has to interact
with the \gls{dcs} through its local interface channels and
have its own data-path outside the \gls{dcs}. This design
choice is made to modularize the \gls{dcs}, as
not all \gls{fpga} applications would require writing to
memory.

When CPU data arrives in the response-with-data \gls{eci}
channel, it gets written into a slot (identified by the
\gls{dcu-idx}, extracted from the \gls{cl} address in the
header) in the write-data-path buffer provided the slot is empty. 
If the slot is not empty, the response-with-data \gls{eci}
channel gets blocked till the previously stored
data in the slot is retrieved.
Once data is stored, the header is routed to the \gls{dcu}
(again using the \gls{dcu-idx}) through the
control-path. The \gls{dcu} would eventually issue write request
control-signals. The write request would be tagged
with the \gls{dcu-id} as the transaction ID, and write
strobe signals are generated from the dmask-field in the header.
The stored data is then retrieved by indexing the buffer
with \gls{dcu-idx} from the transaction ID, before issuing
a write request to memory. 

On the read-data-path, the read response is stored in the
data-path buffer. Similar to write transactions, read
transactions are also tagged with the \gls{dcu-id}. Thus
the transaction ID from the read response is used to
identify the index to store data. Once read response data
is stored, the control signals (transaction ID in this case)
is routed to the appropriate \gls{dcu}. Eventually the
\gls{dcu} generates a 64-bit header (as a response to
what caused the read request), the \gls{cl} address of
which is used to retrieve the data before sending it
through an outgoing data channel. In the current design,
the read-data-path is used for serving upgrade requests
from the CPU. \gls{fpga} applications that require reading
the memory must interact with the \gls{dcs} through the
local interface and have its own data-path outside the
\gls{dcs}.

\subsubsection{Architecture of Control-Path}
\label{sec:control_path_arch}
As shown in \autoref{fig:dcs_internals}, each
incoming interface channel has routing logic
to route control signals to the \glspl{dcu}.
Control signals output from \glspl{dcu} are
arbitrated to choose one set of signals for
an outgoing interface channel. The arbitration
for each outgoing channel is independent of
others i.e. the read request can be from one
\gls{dcu} and the write request can be from a
different \gls{dcu}.

\subsubsection{Architecture of Data-Path}
\label{sec:data_path_arch}

In general, the data-path has to ensure that stored data
is not overwritten before it is retrieved. The data-path
achieves this by blocking any writes to the buffer where
this rule is violated. Since each slot corresponds to a
single \gls{dcu}, this also means that each \gls{dcu}
can have only one outstanding operation on a data-path. 

The read and write data-paths are similar in all cases
except that writes can happen in sub-cache-line
granularity and reads are always \gls{cl} granularity.
Incoming data from \gls{eci} response-with-data channel
into the write-data-path can be compressed and would
have to be decompressed using the dmask-field i.e.
the sub-cache-lines would have to be mapped to the
correct byte-address they should be written to. This
operation is not required in the read-data-path. 

The architecture of a generic data-path is shown in
\autoref{dcs_data_path}. Control and data are passed into
the gate-keeping module (DP\_GATE) which ensures that a \gls{dcu}
has only one ongoing operation in the data-path. When
a slot is available, the gate-keeping module sends the
control signals to the \glspl{dcu} and writes the data
into a data-store. A module that maps compressed \gls{eci}
data to its byte-address (based on dmask-field) can
be optionally instantiated within the gate-keeping module.

\begin{figure}[htbp]
  \centering
  \includegraphics[scale=0.5]{figures/dcs_data_path}
  \caption{DC Slice data-path}
  \label{dcs_data_path}
\end{figure}

The decompressed \gls{cl} size (1024-bit) data
is then split into two chunks of 512-bits each
by a serializer module (DP\_WR\_SER) before writing into the
data-store (DP\_STORE). This is done to reduce the size of the
bus inorder to ease routing and reduce congestion.

Depending on the number of \glspl{dcu} in a
\gls{dcs}, the data-store has 16/32/64 \gls{cl}
sized slots.The data-store uses the \gls{dcu-idx}
to store \gls{cl} sized data in 2 cycles and retrieve
the same in 1 cycle. Since storing data happens in 2
cycles and retrieving data happens in 1 cycle,
there are pipeline registers present at the
retrieve interface to ensure data is stored before
retrieval.  Upon retrieval, the \gls{dcu-idx} is
sent by the data-store to the gate-keeping module indicating
that the slot for the \gls{dcu} has been freed.

\subsection{Back of envelope calculation}
\label{sec:boecalc}
\begin{itemize}
\item What is number of outstanding requests required to
  saturate ECI bandwidth for DDR operations?
\item This also gives the depth of data buffer in data-path.
\item Say odd/even channel with 2 links has 8GiB/s throughput.
\item Say latency of DDR is 300ns. 
\item 1 request is ${2^7}$ Bytes.
\item 8GiB/s is ${2^{33}}$ B/s.
\item ${2^{33}}$ B/s is ${2^{33-7}}$ requests/s = ${2^{26}}$ requests/s.
\item Bandwidth delay product gives the number of outstanding requests to be issued. 
\item Bandwidth delay product = ${2^{26} * 300 * 10^{-9}}$
\item Number of outstanding requests ~ 19 outsanding requests. 
\item Hence, 16/32/64 should be reasonable depth in data-store per \gls{dcu} slice to fill ECI bandwidth. 
\end{itemize}


With 32 \glspl{dcu} per \gls{dcu} slice (by extension, the buffer has 32 slots, one per \gls{dcu}),
what is expected performance?
\begin{itemize}
\item We can have 32 outstanding requests maximum.
\item DDR latency = 300ns (100 cycles) 
\item ${Number of outstanding requests = Total number of requests/s * Latency in s}$
\item ${32 = X * 300 x 10^-9}$
\item ${32x10^9/300 = 1.0667E8 ~ 1x10^8 requests/second.}$
\item 1 request = 128 Bytes 
\item ${10^8 requests = 10^8x128 bytes = 12.8 GB/s = 11.9 GiB/s }$
\item Expected performance is 12 GiB/s.
\end{itemize}
\subsection{Verification of Generic Data-Path}
\label{sec:dcs_data_path_verif}
\begin{figure}[htbp]
  \centering
  \includegraphics[scale=0.5]{figures/dcs_data_path_verif}
  \caption{DC Slice data-path verification}
  \label{dcs_data_path_verif}
\end{figure}

\autoref{dcs_data_path_verif} shows the architecture of the simulation testbench
built to simulate the performance of the data-path. The \glspl{dcu} are modeled by
a valid-ready delay module that accepts a header and sends the same header out
after a programmable delay. Each \gls{dcu} is modeled with a different random delay between the
input and output headers. The delays vary between 0 and 25 cycles.

The DP\_GATE module splits the incoming header and data into separate channels.
The header is then routed to appropriate \gls{dcu} based on the \gls{dcu-idx}. The data gets
written into DP\_STORE at the location pointed to by the \gls{dcu-idx}.

The outputs of the \glspl{dcu} become valid at different points of time depending on
the programmed delays and a priority arbiter selects one of the outputs based on
priority with \gls{dcu-idx} 0 having the highest priority and \gls{dcu-idx} ${K-1}$ with lowest
priority. The chosen header is then sent to the retrieve interface of the data store,
to retrieve the data.

In order to verify that the data retrieved corresponds to the original header, each header
is unique and the data corresponding to the header is the header multiplied by a factor.
When data is retrieved, the data is compared with the header to make sure the right value
is retrieved.

Different test cases are run and their throughput is measured. The test cases are
\begin{itemize}
\item TC1: Issuing one header+data packet to each \gls{dcu} sequentially, with total of ${K}$ packets issued. This is the best case scenario where we can expect the highest throughput. 
\item TC2: Issuing ${K}$ packets to the same \gls{dcu}, effectively serializing the transactions. 
  \begin{itemize}
  \item TC2.1: Best case is when the \gls{dcu} is modeled by a small delay. 
  \item TC2.2: Worst case is when the \gls{dcu} is modeled by a large delay. 
  \end{itemize}
  
\item TC3: Issuing ${K}$ packets to random \glspl{dcu}. Collisions might occur which can cause
  head of line blocking. 
\end{itemize}

In this particular scenario, ${K=32}$ with 32 \glspl{dcu} per DC\_Slice. The delays
modeled in each \gls{dcu} is shown in \autoref{dcu_delay_values}.
\begin{figure}[htbp]
  \centering
  \includegraphics[scale=0.5]{figures/dcu_delay_values}
  \caption{Delays modeled in each of 32 \glspl{dcu}.}
  \label{dcu_delay_values}
\end{figure}

The throughput values are
\begin{itemize}
\item TC1: 12.06 GiB/s. (matches expectations)
\item TC2.1 (Delay = 0 cycles): 7.94 GiB/s.
\item TC2.2 (Delay = 25 cycles): 1.32 GiB/s.
\item TC2: 9.18 GiB/s.
\end{itemize}

For TC1, the performance matches what is expected, there is no head-of-line blocking.
For TC2.1 and TC2.2, the operations are effectively serialized and performance varies
greatly depending on the delay in the \gls{dcu}.
The TC3, there head-of-line blocking occurs randomly which causes the throughput to
decrease. 



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
