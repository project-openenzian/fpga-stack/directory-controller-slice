`ifndef AXIS_PIPELINE_STAGE_SV
`define AXIS_PIPELINE_STAGE_SV

// Initiation Interval = 1 (no delay)

/*
 * There are 2 sets of data and valid registers - primary and expansion 
 * Primary registers hold the data that has been captured from the input
 * Expansion catches the overflow in the data when primary is filled and downstream is not ready yet
 * 
 * When expansion registers have no valid data, module  is ready to receive upstream data 
 * when upstream data is latched in primary registers and downstream is ready, the data will 
 * flow to downstream next cycle. New inputs will be latched next cycle in primary registers
 * When upstream data is latched in primary registers and downstream is not ready
 * The data in primary registers will be overwritten by current inputs and hence the data  
 * needs to be copied to expansion registers. 
 * When expansion register has valid data, the module will not accept any new inputs and wait 
 * for output handshake to happen. It provides data from expansion registers to output and resets
 * the valid in expannsion register. 
 */
module axis_pipeline_stage #
  (
   parameter DATA_WIDTH = 32
   )
   (
    input logic 		      clk,
    input logic 		      reset,
    // Incoming Upstream data 
    input logic [(DATA_WIDTH - 1):0]  us_data,
    input logic 		      us_valid,
    output logic 		      us_ready,
    // Outgoing downstream data 
    output logic [(DATA_WIDTH - 1):0] ds_data,
    output logic 		      ds_valid,
    input logic 		      ds_ready
    );

   // Primary registers 
   logic [(DATA_WIDTH - 1):0] 	      prim_data_reg	= '0, prim_data_next;
   logic 			      prim_valid_reg	= '0, prim_valid_next;
   // Expansion registers
   logic [(DATA_WIDTH - 1):0] 	      exp_data_reg = '0, exp_data_next;
   logic 			      exp_valid_reg = '0, exp_valid_next;

   // ready as long as there is nothing in the expansion register
   assign us_ready	= ~exp_valid_reg;
   assign ds_data	= (exp_valid_reg == 1'b1) ? exp_data_reg : prim_data_reg;
   assign ds_valid	= prim_valid_reg | exp_valid_reg;

   always_comb begin : S1_M1_II1
      prim_data_next	= prim_data_reg;
      prim_valid_next	= prim_valid_reg;
      exp_data_next	= exp_data_reg;
      exp_valid_next = exp_valid_reg;

      if(us_ready) begin
	 // accept upstream data if ready is high
	 prim_data_next = us_data;
	 prim_valid_next = us_valid;
	 if(ds_ready == 1'b0) begin
	    // when ds is not ready, accept data into expansion reg until it is valid
	    exp_data_next = prim_data_reg;
	    exp_valid_next = prim_valid_reg;
	 end
      end
      
      // when ds becomes ready the expansion reg data is accepted and we must clear the valid register
      if(ds_ready) begin
	 exp_valid_next = 1'b0;
      end
   end : S1_M1_II1

   always_ff @(posedge clk) begin : REG_ASSIGN
      prim_data_reg	<= prim_data_next;
      prim_valid_reg	<= prim_valid_next;
      exp_data_reg	<= exp_data_next;
      exp_valid_reg <= exp_valid_next;
      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset 
	 prim_data_reg	<= '0;
	 prim_valid_reg <= '0;
	 exp_data_reg <= '0;
	 exp_valid_reg <= '0;
      end
   end : REG_ASSIGN
endmodule // axis_pipeline_stage

`endif
