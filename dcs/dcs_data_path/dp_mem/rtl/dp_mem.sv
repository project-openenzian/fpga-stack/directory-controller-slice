/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-07-14
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */

`ifndef DP_MEM_SV
`define DP_MEM_SV

// Memory for data path - distributed RAM.
// To store and retrieve CLs.
//
// A CL is written in 2 cycles
// with CL/2 bits every cycle.
//
// Reading a CL is 1 cycle. 
//
// View of memory to store 32 CLs.
// (ie RD_NUM_REGS = 32).
// width - 512 bits
// depth - 64 regs.
//
// For writing memory is viewed as
//  width - 512
//  depth - 64 (wr addr is 6 bits).
//  to write a cl, it takes 2 cycles
//
// For reading memory is viewed as
//  width - 1024
//  depth - 32 (rd addr is 5 bits).
//  reading a cl takes 1 cycle.
//
// Internally the rd addr is
// suffixed with an additional bit
// to identify the registers to read.

module dp_mem #
  (
   parameter WR_NUM_REGS = 64,
   parameter RD_NUM_REGS = WR_NUM_REGS/2, // 32 dont modify
   parameter WR_IDX_WIDTH = $clog2(WR_NUM_REGS), //6 dont modify
   parameter RD_CL_IDX_WIDTH = $clog2(RD_NUM_REGS), //5 dont modify
   parameter RD_DATA_WIDTH = 1024,
   parameter WR_DATA_WIDTH = 512
   )
   (
    input logic 		      clk,
    // write interface.
    // write 512 bits every cycle.
    // 5 bit address. 
    input logic [WR_IDX_WIDTH-1:0]    wr_addr_i,
    input logic [WR_DATA_WIDTH-1:0]   wr_data_i,
    input logic 		      wr_en_i,
    // read interface.
    // read 1024 bits every cycle.
    // 4 bit address. 
    input logic [RD_CL_IDX_WIDTH-1:0] rd_addr_i,
    input logic 		      rd_en_i,
    output logic [RD_DATA_WIDTH-1:0]  rd_data_o
    );
   
   localparam NUM_REGS = WR_NUM_REGS;
   localparam REG_WIDTH = 512;
   localparam REG_IDX_WIDTH = WR_IDX_WIDTH;

   logic [REG_IDX_WIDTH-1:0] 		rd_addr_lo;
   logic [REG_IDX_WIDTH-1:0] 		rd_addr_hi;

   initial begin
      if(RD_NUM_REGS != WR_NUM_REGS/2) begin
	 $error("Error: Module requires RD_NUM_REGS to be half of WR_NUM_REGS (instance %m)");
	 $finish;
      end
      if(WR_DATA_WIDTH != RD_DATA_WIDTH/2) begin
	 $error("Error: Module requires WR_DATA_WIDTH to be half of RD_DATA_WIDTH (instance %m)");
	 $finish;
      end
   end

   (* ram_style = "distributed" *) logic [REG_WIDTH-1:0] data_ram_reg [NUM_REGS-1:0];

   always_ff @(posedge clk) begin : WRITE_LOGIC
      if(wr_en_i) begin
	 data_ram_reg[wr_addr_i] <= wr_data_i;
      end
   end : WRITE_LOGIC

   assign rd_addr_lo = {rd_addr_i, 1'b0};
   assign rd_addr_hi = {rd_addr_i, 1'b1};
   always_ff @(posedge clk) begin : READ_LOGIC
      if(rd_en_i) begin
	 rd_data_o[RD_DATA_WIDTH-1:WR_DATA_WIDTH] <= data_ram_reg[rd_addr_hi];
	 rd_data_o[WR_DATA_WIDTH-1:0] <= data_ram_reg[rd_addr_lo];
      end
   end : READ_LOGIC
   
endmodule // dp_mem

`endif
