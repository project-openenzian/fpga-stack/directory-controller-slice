#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
$XILINX_VIVADO/data/verilog/src/glbl.v \
../testbench/tag_state_ramTb.sv \
../rtl/tag_state_ram.sv \
../rtl/ram_tdp.sv 

xelab -debug typical -incremental -L xpm worklib.tag_state_ramTb worklib.glbl -s worklib.tag_state_ramTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.tag_state_ramTb 
xsim -R worklib.tag_state_ramTb
