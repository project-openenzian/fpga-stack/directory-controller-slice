
module ram_tdpTb();

   localparam ADDR_WIDTH = 10;
   localparam DATA_WIDTH = 36;
   
   parameter [35:0] RESET_VALUE = '1;

   //input output ports 
   //Input signals
   logic 	clk;
   logic 	reset;
   logic [9:0] 	addra;
   logic [35:0] dina;
   logic 	ena;
   logic [3:0] 	wea;
   logic [9:0] 	addrb;
   logic [35:0] dinb;
   logic 	enb;
   logic [3:0] 	web;

   //Output signals
   logic [35:0] douta;
   logic [35:0] doutb;
   logic 	busy_o;


   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   ram_tdp ram_tdp1 (
		     .clk(clk),
		     .reset(reset),
		     .addra(addra),
		     .dina(dina),
		     .ena(ena),
		     .wea(wea),
		     .addrb(addrb),
		     .dinb(dinb),
		     .enb(enb),
		     .web(web),
		     .douta(douta),
		     .doutb(doutb),
		     .busy_o(busy_o)
		     );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports 
      clk = '0;
      reset = '0;
      addra = '0;
      dina = '0;
      ena = '0;
      wea = '0;
      addrb = '0;
      dinb = '0;
      enb = '0;
      web = '0;
      ##5;
      reset = 1'b1;
      ##5;
      reset = 1'b0;
      wait(busy_o == 1'b0);
      // Test dual port write and read
      wr_dual(.pa_addr(0), .pb_addr(1), .pa_din('d10), .pb_din('d20));
      rd_dual(.pa_addr(0), .pb_addr(1));
      assert(douta == 'd10) else $error("Port A %d output is wrong", douta);
      assert(doutb == 'd20) else $error("Port B %d output is wrong", doutb);
      // Test reset values.
      rd_dual(.pa_addr(2), .pb_addr(3));
      assert(douta == '1) else $error("Port A %b output is wrong", douta);
      assert(doutb == '1) else $error("Port B %b output is wrong", doutb);
      ##5;
      // write through one port and read from the other. 
      sweep_wr_porta();
      ##1;
      sweep_rd_portb();
      
      #500 $finish;
   end 


   task static wr_dual(
		       input logic [ADDR_WIDTH-1:0] pa_addr,
		       input logic [DATA_WIDTH-1:0] pa_din,
		       input logic [ADDR_WIDTH-1:0] pb_addr,
		       input logic [DATA_WIDTH-1:0] pb_din
		       );
      addra = pa_addr;
      dina  = pa_din;
      addrb = pb_addr;
      dinb  = pb_din;
      ena = 1'b1;
      wea = '1;
      enb = 1'b1;
      web = '1;
      ##1;
      ena = 1'b0;
      wea = '0;
      enb = 1'b0;
      web = '0;
      #1;
   endtask //wr_dual

   task static rd_dual(
		       input logic [ADDR_WIDTH-1:0] pa_addr,
		       input logic [ADDR_WIDTH-1:0] pb_addr
		       );
      addra = pa_addr;
      addrb = pb_addr;
      ena = 1'b1;
      wea = '0;
      enb = 1'b1;
      web = '0;
      ##1;
      ena = 1'b0;
      wea = '0;
      enb = 1'b0;
      web = '0;
      #1;
   endtask //rd_dual

   task static sweep_wr_porta();
      for( logic[9:0] i=0; i<1023; i=i+1 ) begin
	 addra = i;
	 dina = {25'd0, i};
	 ena = 1'b1;
	 wea = '1;
	 enb = 1'b0;
	 web = '0;
	 ##1;
      end
      ena = 1'b0;
      wea = '0;
      enb = 1'b0;
      web = '0;
      #1;
   endtask //sweep_wr_porta

   task static sweep_rd_portb();
      for( logic[9:0] i=0; i<1023; i=i+1 ) begin
	 addrb = i;
	 ena = 1'b0;
	 wea = '0;
	 enb = 1'b1;
	 web = '0;
	 ##1;
	 #1;
	 assert(doutb == {25'd0, i}) else $error("Data doutb %d does not match expected %d\n", doutb, i);
      end
      ena = 1'b0;
      wea = '0;
      enb = 1'b0;
      web = '0;
      #1;
   endtask //sweep_rd_portb
   
endmodule
