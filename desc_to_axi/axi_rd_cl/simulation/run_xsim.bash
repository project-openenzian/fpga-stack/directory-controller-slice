#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
$XILINX_VIVADO/data/verilog/src/glbl.v \
../testbench/axi_rd_clTb.sv \
../rtl/axi_rd_cl.sv 

xelab -debug typical -incremental -L xpm worklib.axi_rd_clTb worklib.glbl -s worklib.axi_rd_clTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.axi_rd_clTb 
xsim -gui worklib.axi_rd_clTb 
