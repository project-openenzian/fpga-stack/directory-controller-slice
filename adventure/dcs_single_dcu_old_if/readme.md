## Single DCU connected to a DC slice.

### Important
A single DCU is connected in a DC slice only to
verify in implementation if the read and write
datapaths work without issues. There are limitations
on the CL indices that can be issued to this module
for it to reliably work. This is described here. 

A DC slice is associated with either an odd or even
CL index but not both. A DCU within a DC slice is
also associated with a DCU_ID and only services
cachelines indices corresponding to this ID. If
a DCU recieves request for a cacheline containing
a different DCU ID, there might be errors.

A single DCU has a limited number of sets in its
directory. This limits the maximum number of cachelines
allowed to be cached on the CPU before the directory
gets full.

These limit the CL indices that can be issued for
this module to the following scenarios.
(Assume the DCU_ID is 0 for this DCU)

| Number of sets per DCU | Start CL index | CL index stride | Max number of CL indices|
| :--------------------: | :------------: | :-------------: | :----------------------:|
| **64**                 | 0              | 32              | 1024                    |
| **128**                | 0              | 64              | 2048                    |
| **256**                | 0              | 128             | 4092                    |

If CL indices other than the ones confirming with
the table are issued, there would be errors. 

**NOTE:** You can change the number of sets per DCU
in **../eci_dcs_defs/rtl/eci_dcs_defs.sv**

### Description
A single DCU with part of the directory is instantiated
in a DC slice with separate read and write data paths.
The architecture is shown in figure. 

![Image](figures/dcs_single_dcu.png)

VC channels **without data** from the CPU, namely
req_wod and rsp_wod are directly connected to
the DCU. The VC channels **with data** rsp_wd is
connected to the write data path, which stores
the data and sends only the header to the DCU.
Once the rsp_wd_hdr is processed by the DCU to
generate the control signals for the write request,
the control signals are sent to the write data path
to retrieve the stored data and generate the
write request control and data packet to be sent
to the byte addressable memory. Write responses
from the byte addressable memory do not carry
cache line data and are connected directly to
the DCU.

Similarly, read resposnes from byte addressable
memory are connected to the read data path, where
the data is stored and the control signals are
sent to the DCU. Once the DCU processes the data
and generates a rsp_wd header, the header is sent
to the read data path to retrieve the stored data
and send an rsp_wd header + data packet to the CPU.


### Testing
There are two types of testing done,
 * Correctness testing.
 * Performance measurement.

#### Testing for correctness
The read and write data paths are tested to ensure
data is stored and retrieved correctly. Finer tests
on the DCU are performed at the DCU level and not
here.

 * Testbench: ./testbench/dcs_single_dcuTb.sv
 * Simulation: ./simulation/run_xsim.bash


How to run:
 * cd to simulation folder.
 * ./run_xsim.bash
 * This has been tested on Vivado 2018.2


#### Performance measurement
The DC Slice is connected to a module
that mimics CPU in issuing contiguous reads to
issue read exclusive to CL indices given a
starting CL index, stride and number of CLs.
Throughput is measured by this module once
the responses from the DC slice start rolling
in.

 * Testbench: ./perf_sim_tb/perf_simTb.sv
 * Simulation: ./perf_sim_run/run_xsim.bash

How to run:
 * cd ./perf_sim_run
 * ./run_xsim.bash > ./results.txt
 * You can change the number of sets per DCU
   in ../eci_dcs_defs/rtl/eci_dcs_defs.sv

Plotting results
 * ../perf_sim_modules/plot/plot_tput.py ./results.txt  

#### Performance result analysis.
 * Performance results are stored in ./perf_sim_results/
 * one result file per number of sets per DCU configuration.

#### Performance 256 sets per DCU.
![dcs_single_dcu_256_perf.png](figures/dcs_single_dcu_256_perf.png "Performance 256 sets/DCU.")

#### Performance 128 sets per DCU.
![dcs_single_dcu_128_perf.png](figures/dcs_single_dcu_128_perf.png "Performance 128 sets/DCU.")

#### Performance 64 sets per DCU.
![dcs_single_dcu_64_perf.png](figures/dcs_single_dcu_64_perf.png "Performance 64 sets/DCU.")

The reason why performance with 128 and 64 sets per
DCU has an inflection point is due to directory lookup
terminating early as the directory gets filled up.

The directory has 16 way per set. Directory lookup
starts with the first way and continues to the last.
However when the directory gets filled up, it gets
filled up with the last way first continuing to the
first, essentially in reverse order as lookup.

For example:
In the 128 sets/DCU and 16 ways directory lookup takes
2 cycles maximum, with first 8 (1-8) ways looked up in a cycle
followed by the last 8 ways (9-16).
The first 128x8 (1024) CLs, fills up ways ways 9-16 of
all sets. Looking up the first 1024 CLs takes 2 cycles each.
The next 128x8 CLs fill up ways 1-8, which takes only 1
cycle to look up. Thus we have slightly improved performance
when look up terminates early. 
