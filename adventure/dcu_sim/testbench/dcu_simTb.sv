import eci_dcs_defs::*;

module dcu_simTb();

   parameter STORE_HDR_WIDTH = 64;
   parameter RETRIEVE_HDR_WIDTH = 64;
   parameter SIM_DELAY = 5;

   //input output ports 
   //Input signals
   logic 				 clk;
   logic 				 reset;
   logic [DS_DCU_IDX_WIDTH-1:0] 	 req_dcu_idx_i;
   logic [STORE_HDR_WIDTH-1:0] 		 req_hdr_i;
   logic 				 req_valid_i;
   logic 				 rsp_ready_i;

   //Output signals
   logic 				 req_ready_o;
   logic [DS_DCU_ID_WIDTH-1:0] 		 rsp_dcu_id_o;
   logic [RETRIEVE_HDR_WIDTH-1:0] 	 rsp_hdr_o;
   logic 				 rsp_valid_o;


   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   dcu_sim dcu_sim1 (
		     .clk(clk),
		     .reset(reset),
		     .req_dcu_idx_i(req_dcu_idx_i),
		     .req_hdr_i(req_hdr_i),
		     .req_valid_i(req_valid_i),
		     .rsp_ready_i(rsp_ready_i),
		     .req_ready_o(req_ready_o),
		     .rsp_dcu_id_o(rsp_dcu_id_o),
		     .rsp_hdr_o(rsp_hdr_o),
		     .rsp_valid_o(rsp_valid_o)
		     );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports 
      clk = '0;
      reset = '1;
      req_dcu_idx_i = '0;
      req_hdr_i = '0;
      req_valid_i = '0;
      rsp_ready_i = '0;
      ##5;
      reset = 1'b0;
      req_dcu_idx_i = 'd5;
      req_hdr_i = 'd64;
      req_valid_i = '1;
      rsp_ready_i = '1;
      wait(req_valid_i & req_ready_o);
      ##1;
      req_dcu_idx_i = 'd6;
      req_hdr_i = 'd128;
      req_valid_i = '1;
      wait(req_valid_i & req_ready_o);
      ##1;
      req_valid_i = 1'b0;
      
      #500 $finish;
   end 

endmodule
