import eci_cmd_defs::*;

module dc_to_vc_routerTb();

   //input output ports 
   //Input signals
   logic 				    clk;
   logic 				    reset;
   logic [ECI_WORD_WIDTH-1:0] 		    ecih_hdr_i;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	    ecih_pkt_size_i;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	    ecih_pkt_vc_i;
   logic 				    ecih_pkt_valid_i;
   logic 				    rsp_wod_pkt_ready_i;
   logic 				    rsp_wd_pkt_ready_i;
   logic 				    fwd_wod_pkt_ready_i;
   logic 				    lcl_rsp_wod_pkt_ready_i;

   //Output signals
   logic 				    ecih_pkt_ready_o;
   logic [ECI_WORD_WIDTH-1:0] 		    rsp_wod_hdr_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	    rsp_wod_pkt_size_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	    rsp_wod_pkt_vc_o;
   logic 				    rsp_wod_pkt_valid_o;
   logic [ECI_WORD_WIDTH-1:0] 		    rsp_wd_hdr_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	    rsp_wd_pkt_size_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	    rsp_wd_pkt_vc_o;
   logic 				    rsp_wd_pkt_valid_o;
   logic [ECI_WORD_WIDTH-1:0] 		    fwd_wod_hdr_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	    fwd_wod_pkt_size_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	    fwd_wod_pkt_vc_o;
   logic 				    fwd_wod_pkt_valid_o;
   logic [ECI_WORD_WIDTH-1:0] 		    lcl_rsp_wod_hdr_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	    lcl_rsp_wod_pkt_size_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	    lcl_rsp_wod_pkt_vc_o;
   logic 				    lcl_rsp_wod_pkt_valid_o;


   typedef enum logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] {
						   VC_RESP_W_DATA_E=4, //4
						   VC_RESP_W_DATA_O=5, //5
						   VC_FWD_WO_DATA_E=8, //8
						   VC_FWD_WO_DATA_O=9, //9
						   VC_RESP_WO_DATA_E=10, //10
						   VC_RESP_WO_DATA_O=11, //11
						   VC_LCL_RESP_WO_DATA_E=18, //18
						   C_LCL_RESP_WO_DATA_O=19 //19
						   } vc_t;

   vc_t my_vc;

   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   assign ecih_pkt_vc_i = my_vc;
   
   //instantiation
   dc_to_vc_router dc_to_vc_router1 (
				     .clk(clk),
				     .reset(reset),
				     .ecih_hdr_i(ecih_hdr_i),
				     .ecih_pkt_size_i(ecih_pkt_size_i),
				     .ecih_pkt_vc_i(ecih_pkt_vc_i),
				     .ecih_pkt_valid_i(ecih_pkt_valid_i),
				     .rsp_wod_pkt_ready_i(rsp_wod_pkt_ready_i),
				     .rsp_wd_pkt_ready_i(rsp_wd_pkt_ready_i),
				     .fwd_wod_pkt_ready_i(fwd_wod_pkt_ready_i),
				     .lcl_rsp_wod_pkt_ready_i(lcl_rsp_wod_pkt_ready_i),
				     .ecih_pkt_ready_o(ecih_pkt_ready_o),
				     .rsp_wod_hdr_o(rsp_wod_hdr_o),
				     .rsp_wod_pkt_size_o(rsp_wod_pkt_size_o),
				     .rsp_wod_pkt_vc_o(rsp_wod_pkt_vc_o),
				     .rsp_wod_pkt_valid_o(rsp_wod_pkt_valid_o),
				     .rsp_wd_hdr_o(rsp_wd_hdr_o),
				     .rsp_wd_pkt_size_o(rsp_wd_pkt_size_o),
				     .rsp_wd_pkt_vc_o(rsp_wd_pkt_vc_o),
				     .rsp_wd_pkt_valid_o(rsp_wd_pkt_valid_o),
				     .fwd_wod_hdr_o(fwd_wod_hdr_o),
				     .fwd_wod_pkt_size_o(fwd_wod_pkt_size_o),
				     .fwd_wod_pkt_vc_o(fwd_wod_pkt_vc_o),
				     .fwd_wod_pkt_valid_o(fwd_wod_pkt_valid_o),
				     .lcl_rsp_wod_hdr_o(lcl_rsp_wod_hdr_o),
				     .lcl_rsp_wod_pkt_size_o(lcl_rsp_wod_pkt_size_o),
				     .lcl_rsp_wod_pkt_vc_o(lcl_rsp_wod_pkt_vc_o),
				     .lcl_rsp_wod_pkt_valid_o(lcl_rsp_wod_pkt_valid_o)
				     );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports 
      clk = '0;
      reset = '0;
      ecih_hdr_i = '0;
      ecih_pkt_size_i = '0;
      //ecih_pkt_vc_i = '0;
      if(randomize(my_vc) === 0) begin
	 $fatal(1, "Randomize failed");
      end
      ecih_pkt_valid_i    = '0;
      rsp_wod_pkt_ready_i = '0;
      rsp_wd_pkt_ready_i  = '0;
      fwd_wod_pkt_ready_i = '0;
      lcl_rsp_wod_pkt_ready_i = '0;
      ##5;
      reset = 1'b0;
      rsp_wod_pkt_ready_i = '1;
      rsp_wd_pkt_ready_i  = '1;
      fwd_wod_pkt_ready_i = '1;
      lcl_rsp_wod_pkt_ready_i = '1;
      ##5;
      repeat(50) begin
	 launch_random_wait();
      end
      $display("Success: All tests pass");
      #500 $finish;
   end 

   always_ff @(posedge clk) begin : CHECK_RESULTS
      // rsp_wd is selected, VC should be 4,5
      // rsp_wod, fwd_wod, lcl_rsp_wod should not be valid.
      if(rsp_wd_pkt_valid_o & rsp_wd_pkt_ready_i) begin
	 assert(
		(rsp_wd_pkt_vc_o === VC_RESP_W_DATA_E) |
		(rsp_wd_pkt_vc_o === VC_RESP_W_DATA_O)
		) else
	   $fatal(1,"VC does not match any of the %0d,%0d vcs.",VC_RESP_W_DATA_E,VC_RESP_W_DATA_O);
	 assert(rsp_wod_pkt_valid_o === 1'b0) else
	   $fatal(1, "rsp_wod channel should not be valid");
	 assert(fwd_wod_pkt_valid_o === 1'b0) else
	   $fatal(1, "fwd_wod channel should not be valid");
	 assert(lcl_rsp_wod_pkt_valid_o === 1'b0) else
	   $fatal(1, "lcl_rsp_wod channel should not be valid");
      end

      // fwd_wod is selected, VC should be 8,9
      // rsp_wod, rsp_wd, lcl_rsp_wod should not be valid.
      if(fwd_wod_pkt_valid_o & fwd_wod_pkt_ready_i) begin
	 assert(
		(fwd_wod_pkt_vc_o === VC_FWD_WO_DATA_E) |
		(fwd_wod_pkt_vc_o === VC_FWD_WO_DATA_O)
		) else
	   $fatal(1,"VC does not match any of the 8,9 vcs.");
	 assert(rsp_wod_pkt_valid_o === 1'b0) else
	   $fatal(1, "rsp_wod channel should not be valid");
	 assert(rsp_wd_pkt_valid_o === 1'b0) else
	   $fatal(1, "rsp_wd channel should not be valid");
	 assert(lcl_rsp_wod_pkt_valid_o === 1'b0) else
	   $fatal(1, "lcl_rsp_wod channel should not be valid");
      end

      // rsp_wod is selected, VC should be 10, 11
      // rsp_wd, fwd_wod, lcl_rsp_wod should not be valid.
      if(rsp_wod_pkt_valid_o & rsp_wod_pkt_ready_i) begin
	 assert(
		(rsp_wod_pkt_vc_o === VC_RESP_WO_DATA_E) |
		(rsp_wod_pkt_vc_o === VC_RESP_WO_DATA_O)
		) else
	   $fatal(1,"VC does not match any of the 10,11 vcs.");
	 assert(rsp_wd_pkt_valid_o === 1'b0) else
	   $fatal(1, "rsp_wd channel should not be valid");
	 assert(fwd_wod_pkt_valid_o === 1'b0) else
	   $fatal(1, "fwd_wod channel should not be valid");
	 assert(lcl_rsp_wod_pkt_valid_o === 1'b0) else
	   $fatal(1, "lcl_rsp_wod channel should not be valid");
      end

      // lcl_rsp_wod is selected, VC should be 10, 11
      // rsp_wd, fwd_wod, rsp_wod should not be valid.
      if(lcl_rsp_wod_pkt_valid_o & lcl_rsp_wod_pkt_ready_i) begin
	 assert(
		(lcl_rsp_wod_pkt_vc_o === VC_LCL_RESP_WO_DATA_E) |
		(lcl_rsp_wod_pkt_vc_o === VC_LCL_RESP_WO_DATA_O)
		) else
	   $fatal(1,"VC does not match any of the 18,19 vcs.");
	 assert(rsp_wd_pkt_valid_o === 1'b0) else
	   $fatal(1, "rsp_wd channel should not be valid");
	 assert(fwd_wod_pkt_valid_o === 1'b0) else
	   $fatal(1, "fwd_wod channel should not be valid");
	 assert(rsp_wod_pkt_valid_o === 1'b0) else
	   $fatal(1, "rsp_wod channel should not be valid");
      end

   end : CHECK_RESULTS
   
   task static launch_random_wait();
      launch_random();
      wait(ecih_pkt_valid_i & ecih_pkt_ready_o);
      ##1;
      ecih_pkt_valid_i = 1'b0;
      ##1;
   endtask //launch_random

   task static launch_random();
      ecih_hdr_i = $urandom_range(10,200);
      ecih_pkt_size_i = $urandom_range(1, 17);
      if(randomize(my_vc) == 0) begin
	 $fatal(1, "Randomize failed");
      end
      ecih_pkt_valid_i = 1'b1;
   endtask //launch_random
endmodule
