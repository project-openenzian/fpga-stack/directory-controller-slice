#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
      $XILINX_VIVADO/data/verilog/src/glbl.v \
      ../rtl/eci_dcs_defs.sv \
      ../../../common/axis_pipeline_stage/rtl/axis_pipeline_stage.sv \
      ../testbench/dcu_simTb.sv \
      ../rtl/dcu_sim.sv


xelab -debug typical -incremental -L xpm worklib.dcu_simTb worklib.glbl -s worklib.dcu_simTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.dcu_simTb 
xsim -gui worklib.dcu_simTb 
