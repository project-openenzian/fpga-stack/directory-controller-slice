import eci_cmd_defs::*;
import eci_cc_defs::*;
import eci_dirc_defs::*;

module decode_eci_reqTb();


   //input output ports
   //Input signals
   logic				   clk;
   logic				   reset;
   logic [ECI_WORD_WIDTH-1:0]		   eci_req_hdr_i;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0]	   eci_req_vc_i;

   eci_hreqid_t trid_i;
   eci_address_t addr_i;
   eci_dmask_t dmask_i;
   cc_req_t enc_req_i;

   //Output signals
   eci_hreqid_t eci_req_trid_o;
   eci_address_t eci_req_addr_o;
   eci_dmask_t eci_req_dmask_o;
   logic				   eci_req_cl_idx_oe_o;
   cc_req_t eci_req_cc_enc_o;
   logic				   eci_req_invalid_o;
   logic				   eci_req_crts_rtg_ntry_o;
   logic				   eci_rsp_2_prv_snt_req_o;

   // Address casted to CL address type.
   eci_cl_addr_t addr_cl_casted;

   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   decode_eci_req decode_eci_req1 (
				   .clk(clk),
				   .reset(reset),
				   // Inputs
				   .eci_req_hdr_i(eci_req_hdr_i),
				   .eci_req_vc_i(eci_req_vc_i),
				   // Outputs
				   .eci_req_trid_o(eci_req_trid_o),
				   .eci_req_addr_o(eci_req_addr_o),
				   .eci_req_dmask_o(eci_req_dmask_o),
				   .eci_req_cl_idx_oe_o(eci_req_cl_idx_oe_o),
				   .eci_req_cc_enc_o(eci_req_cc_enc_o),
				   .eci_req_invalid_o(eci_req_invalid_o),
				   .eci_req_crts_rtg_ntry_o(eci_req_crts_rtg_ntry_o),
				   .eci_rsp_2_prv_snt_req_o(eci_rsp_2_prv_snt_req_o)
				   );//instantiation completed

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports
      clk = '0;
      reset = '1;
      eci_req_hdr_i = '0;
      eci_req_vc_i = '0;
      addr_cl_casted.flat = '0;
      ##5;
      reset = 1'b0;
      ##5;

      // Send unknown command to check if invalid cmd is detected
      // VC = 0/1 are IO VCs are not handled in dirc
      // so sending a request to those VCs
      send_cmd(
	       // Dummy request
	       .my_req_i('0),
	       // IO request VC#
	       .my_vc_i('0)
	       );
      ##1;
      assert(eci_req_invalid_o == 1'b1) else $fatal(1,"invalid signal should be set");

      ##5;
      // Send valid RLDD and check
      // Sending odd cl index
      gen_aligned_cl_addr(.cl_index_i(ECI_CL_INDEX_WIDTH'('d1)));
      send_rldd(
		.my_id_i(eci_id_t'('d5)),
		.my_dmask_i(eci_dmask_t'('1)),
		.my_addr_i(eci_address_t'(addr_cl_casted.flat))
		);
      ##1;
      assert(eci_req_trid_o == 6'd5) else $fatal(1,"trid does not match");
      assert(eci_req_addr_o == addr_cl_casted.flat) else $fatal(1,"address does not match");
      assert(|eci_req_dmask_o == 1'b1) else $fatal(1,"dmask does not match");
      assert(eci_req_cl_idx_oe_o == 1'b1) else $fatal(1,"Odd even indicator fails");
      assert(eci_req_cc_enc_o == R12) else $fatal(1,"CC Encoding does not match");
      assert(eci_req_invalid_o == 1'b0) else $fatal(1,"Invalid does not match");

      // Adding support for RLDX it should be supported now
      send_cmd(
	       // Dummy request
	       .my_req_i(64'h2807ec0000000000),
	       // IO request VC#
	       .my_vc_i('d7)
	       );
      assert(eci_req_invalid_o == 1'b0) else $fatal(1,"RLDX is not considered valid command");

      ##1;
      // Send LC for odd cl index.
      gen_aligned_cl_addr(.cl_index_i(ECI_CL_INDEX_WIDTH'('d1)));
      send_lc(
	      .my_id_i('d15),
	      .my_dmask_i('1),
	      .my_ns_i(1'b0),
	      .my_addr_i(eci_address_t'(addr_cl_casted.flat))
	      );
      assert(eci_req_trid_o == 'd15) else $fatal(1,"trid does not match");
      assert(eci_req_addr_o == addr_cl_casted.flat) else $fatal(1,"address does not match");
      assert(|eci_req_dmask_o == 1'b1) else $fatal(1,"dmask does not match");
      assert(eci_req_cl_idx_oe_o == 1'b1) else $fatal(1,"Odd even indicator fails");
      assert(eci_req_cc_enc_o == LC) else $fatal(1,"CC Encoding does not match");
      assert(eci_req_invalid_o == 1'b0) else $fatal(1,"Invalid does not match");

      ##1;
      // Send LCI for even cl index.
      gen_aligned_cl_addr(.cl_index_i(ECI_CL_INDEX_WIDTH'('d0)));
      send_lci(
	      .my_id_i('d16),
	      .my_dmask_i('1),
	      .my_ns_i(1'b0),
	      .my_addr_i(eci_address_t'(addr_cl_casted.flat))
	      );
      assert(eci_req_trid_o == 'd16) else $fatal(1,"trid does not match");
      assert(eci_req_addr_o == addr_cl_casted.flat) else $fatal(1,"address does not match");
      assert(|eci_req_dmask_o == 1'b1) else $fatal(1,"dmask does not match");
      assert(eci_req_cl_idx_oe_o == 1'b0) else $fatal(1,"Odd even indicator fails");
      assert(eci_req_cc_enc_o == LCI) else $fatal(1,"CC Encoding does not match");
      assert(eci_req_invalid_o == 1'b0) else $fatal(1,"Invalid does not match");

      $display("Success, all tests pass");
      #500 $finish;
   end

   // Tasks and functions below

   // Generate CL address given CL index.
   task static gen_aligned_cl_addr(
				   input logic [ECI_CL_INDEX_WIDTH-1:0] cl_index_i
				   );
      addr_cl_casted.parts.cl_index = cl_index_i;
      addr_cl_casted.parts.byte_offset = '0;
   endtask //gen_cl_addr

   // Generate RLDD request.
   function automatic eci_word_t gen_rldd
     (
      input eci_id_t rreq_id_i,
      input eci_dmask_t dmask_i,
      input eci_address_t addr_i
      );
      eci_word_t my_req;
      my_req.eci_word = '0;
      my_req.mreq_load.opcode = ECI_CMD_MREQ_RLDD;
      my_req.mreq_load.rreq_id = rreq_id_i;
      my_req.mreq_load.dmask = dmask_i;
      my_req.mreq_load.address = addr_i;
      return(my_req);
   endfunction : gen_rldd

   // generate local clean request.
   function automatic eci_word_t gen_lc
     (
      input	  eci_hreqid_t my_id_i,
      input	  eci_dmask_t my_dmask_i,
      input logic my_ns_i,
      input	  eci_address_t my_addr_i
      );
      eci_word_t my_req;
      my_req.eci_word = '0;
      my_req.lcl_mfwd_generic.opcode = LCL_CMD_MFWD_CLEAN;
      my_req.lcl_mfwd_generic.hreq_id = my_id_i;
      my_req.lcl_mfwd_generic.dmask = my_dmask_i;
      my_req.lcl_mfwd_generic.ns = my_ns_i;
      my_req.lcl_mfwd_generic.address = my_addr_i;
      return(my_req);
   endfunction : gen_lc

   // generate local clean inv request.
   function automatic eci_word_t gen_lci
     (
      input	  eci_hreqid_t my_id_i,
      input	  eci_dmask_t my_dmask_i,
      input logic my_ns_i,
      input	  eci_address_t my_addr_i
      );
      eci_word_t my_req;
      my_req.eci_word = '0;
      my_req.lcl_mfwd_generic.opcode = LCL_CMD_MFWD_CLEAN_INV;
      my_req.lcl_mfwd_generic.hreq_id = my_id_i;
      my_req.lcl_mfwd_generic.dmask = my_dmask_i;
      my_req.lcl_mfwd_generic.ns = my_ns_i;
      my_req.lcl_mfwd_generic.address = my_addr_i;
      return(my_req);
   endfunction : gen_lci

   // generate and send local clean request.
   task static send_lc(
		       input eci_hreqid_t my_id_i,
		       input eci_dmask_t my_dmask_i,
		       input logic my_ns_i,
		       input eci_address_t my_addr_i
			  );
      eci_word_t my_req;
      eci_cl_addr_t my_addr;
      eci_vc_size_t my_vc;
      my_req = gen_lc(
		      .my_id_i(my_id_i),
		      .my_dmask_i(my_dmask_i),
		      .my_ns_i(my_ns_i),
		      .my_addr_i(my_addr_i)
		      );
      // calculate VC # from odd/even idx.
      my_addr = eci_cl_addr_t'(my_addr_i);
      my_vc = (my_addr.parts.cl_index[0] == 1'b1) ? VC_LCL_FWD_WO_DATA_E : VC_LCL_FWD_WO_DATA_O;
      send_cmd(
	       .my_req_i(my_req),
	       .my_vc_i(my_vc)
	       );
   endtask //send_lc

   // generate and send local clean invalidate request.
   task static send_lci(
		       input eci_hreqid_t my_id_i,
		       input eci_dmask_t my_dmask_i,
		       input logic my_ns_i,
		       input eci_address_t my_addr_i
			  );
      eci_word_t my_req;
      eci_cl_addr_t my_addr;
      eci_vc_size_t my_vc;
      my_req = gen_lci(
		      .my_id_i(my_id_i),
		      .my_dmask_i(my_dmask_i),
		      .my_ns_i(my_ns_i),
		      .my_addr_i(my_addr_i)
		      );
      // calculate VC # from odd/even idx.
      my_addr = eci_cl_addr_t'(my_addr_i);
      my_vc = (my_addr.parts.cl_index[0] == 1'b1) ? VC_LCL_FWD_WO_DATA_E : VC_LCL_FWD_WO_DATA_O;
      send_cmd(
	       .my_req_i(my_req),
	       .my_vc_i(my_vc)
	       );
   endtask //send_lci

   // Generate RLDD and send it to DUT.
   task static send_rldd(
			 input eci_hreqid_t my_id_i,
			 input eci_dmask_t  my_dmask_i,
			 input eci_address_t my_addr_i
			 );
      // To send RLDD we need to
      // 1. generate the rldd header
      // 2. generate the VC
      // 3. send this with enable signal

      eci_word_t my_req;
      eci_cl_addr_t my_addr;
      eci_vc_size_t my_vc;

      // remote id is 5 bits eci_id_t
      eci_id_t my_rid;
      // casting 6 bits to 5 bits
      my_rid = eci_id_t'(my_id_i);

      // Generate Header:
      my_req = gen_rldd(.rreq_id_i(my_rid),
			.dmask_i(my_dmask_i),
			.addr_i(my_addr_i)
			);

      // Generate VC:
      //  RLDD, depending on the address is REQ_WO_DATA_E or REQ_WO_DATA_O
      //  ODD CL INDEX goes to EVEN VC
      //  Casting address eci_address_t to eci_cl_addr_t and get the VC
      my_addr = eci_cl_addr_t'(my_addr_i);
      my_vc = (my_addr.parts.cl_index[0] == 1'b1) ? VC_REQ_WO_DATA_E : VC_REQ_WO_DATA_O;

      // Send RLDD.
      send_cmd(.my_req_i(my_req),
	       .my_vc_i(my_vc)
	       );

   endtask //send_rldd

   // Send an ECI request to the DUT.
   task static send_cmd(
			input eci_word_t my_req_i,
			input eci_vc_size_t my_vc_i
			);
      // requires input header and VC
      eci_req_hdr_i = my_req_i;
      eci_req_vc_i = my_vc_i;
      ##1;
   endtask // send_cmd

endmodule
