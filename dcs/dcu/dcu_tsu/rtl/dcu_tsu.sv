`ifndef DCU_TSU_SV
`define DCU_TSU_SV



// Summary of the configuration. 
// ┌─────────────────────┬────────────────────┬─────────────────────┐
// │                     │                    │                     │
// │ Sets/DCU = 64       │ Sets/DCU = 128     │  Sets/DCU = 256     │
// ├─────────────────────┼────────────────────┼─────────────────────┤
// │                     │                    │                     │
// │ # bram/DCU = 1      │ # bram/DCU = 2     │  # bram/DCU = 4     │
// │                     │                    │                     │
// │ set width = 6 bits  │ set width = 7 bits │  set width = 8 bits │
// │                     │                    │                     │
// │ # set_row/set = 4   │ # set_row/set = 2  │  # set_row/set = 1  │
// │                     │                    │                     │
// │ # ways/set_row = 4  │ # ways/set_row = 4 │  # ways/set_row = 4 │
// │                     │                    │                     │
// │ set_id (8 bits) =   │ set_id (8 bits) =  │  set_id (8 bits) =  │
// │ {set, set_row[1:0]} │ {set, set_row[0]}  │  {set}              │
// │                     │                    │                     │
// │ way =               │ way =              │  way =              │
// │ {set_row[1:0],      │ {tsr[0], set_row[0]│  {tsr[1:0],         │
// │  way_id[1:0]}       │  , way_id[1:0]}    │  way_id[1:0]}       │
// │                     │                    │                     │
// │                     │                    │                     │
// │ way_id[1:0] =       │ way_id[1:0] =      │ way_id[1:0] =       │
// │  {HI/LO(1b),        │ {HI/LO(1b),        │  {HI/LO(1b),        │
// │   ROW_WAY(1b)}      │  ROW_WAY(1b)}      │   ROW_WAY(1b)}      │
// │                     │                    │                     │
// └─────────────────────┴────────────────────┴─────────────────────┘

/*
 *
 * TSR: tag state ram.
 *  each TSR is 1 BRAM. 
 * 
 * set: Identifies the set index.
 * A DCU is configured to have 64/128/256 sets. 
 * Each set will have 16 ways. 
 * In a 64  set/DCU configuration, we have 1 TSR/DCU. 
 * In a 128 set/DCU configuration, we have 2 TSR/DCU.
 * In a 256 set/DCU configuration, we have 4 TSR/DCU.
 * 
 * Set_row: A Set has 16 ways, A row in TSR can hold 2 ways. 
 * Using a true dual port RAM we can access 2 TSR rows in parallel.
 * the set_row identifies which row ofset of the set to be accessed. 
 * These 4 ways are identified by a set_row. 
 * Ex. In 64 sets/DCU, 8 rows are needed to store 16 ways of a set.
 *  8 rows can be split into 4 set_row, each set row accessing 4 ways
 *  in parallel. Thus there are 4 set_row per set. 
 * Note: set_row is only the offset. 
 * In 64  sets/DCU, we have 4 set_row per set. 
 * In 128 sets/DCU, we have 2 set_row per set.
 * In 256 sets/DCU, we have 1 set_row per set.
 * 
 * set_id: {set, set_row} the full address into the TSR to access 
 * 4 ways of a set_row is called a set_id. 
 * set_id is always 8 bits.
 * Ex. In 64 sets/DCU, 6 bits are required to identify the set (2^6=64).
 * There are 4 set_rows which need 2 bits and hence the set id is 
 * given by set_id (8bits) = {set(6bits), set_row(2bits)}
 * In 128 sets/DCU we have 
 * set_id (8bits) = {set(7bits), set_row(1bit)}
 * (check table below for configuration info). 
 * 
 * way_id: Accessing a single TSR in a cycle reads a set_row
 * and each set_row has 4 ways. way_id identifes one of the 4 ways. 
 * 
 * tsr_id: ID for a TSR. 
 * In 64  sets/DCU, we have 1 TSR with ID 0.
 * In 128 sets/DCU, we have 2 TSR with ID 0, 1.
 * In 256 sets/DCU, we have 4 TSR with ID 0, 1, 2, 3.
 * 
 * way: Identifies 1 of the 16 ways in a set. 
 * Always 4 bits wide. 
 * In a 64 set/DCU scenario, to identify a way, we need 
 *  2 bits to identify one set_row of the 4 set_rows per set, 
 *  and 2 more bits to identify one of the 4 ways in a set row. 
 * In a 128 set/DCU, we need 1 bit to identify one of the two TSR,
 *  1 bit to identify 1 of the 2 set_row per set and, 
 *  2 bits to identify 1 of the 4 ways per set row. 
 * In a 256 set/DCU, we need 2 bits to identify 1 of the 4 TSR,
 *  2 bits to identify 1 of the 4 ways. 
 * 64  set/DCU - way = {set_row[1:0], way_id[1:0]}. 
 * 128 set/DCU - way = {tsr_id[0], set_row[0], way_id[1:0]}.
 * 256 set/DCU - way = {tsr_id[1:0], way_id[1:0]}.
 * 
 * way_id (2 bits):
 * Each row (36b) in a single TSR can hold two ways (18b)
 * these ways are called row_way_0 and row_way_1.
 * LSB of way_id gives the row_way. 
 * A TSR has 2 regions (HI/LO) for both tag and state. 
 * MSB of way_id gives the region.
 * 
 */

/*
 * Output:
 * Read tag and state.
 *  Upon hit returns:
 *   1. hit way.
 *   2. state stored in hit way 
 *   which is the valid state of tag read.
 * 
 *  Upon miss and not full returns:
 *   1. free way to write to.
 *   2. state store in free way 
 *   which is not a valid state. 
 *   might not be S1__1 (invalid) state.
 * 
 *  Upon miss and full returns:
 *   1. random way: dont write in this way.
 *   2. state returned is not a 
 *   valid state. it need not be 
 *   S1__1 (invalid) state. 
 * 
 */

module dcu_tsu #
  (
   parameter NUM_SETS_PER_DCU = 64, // can be 64, 128 or 256.
   // this is because 1 TSR can support a maximum of 256 sets in current configuration.
   parameter NUM_WAYS_PER_SET = 16, // Do not modify. 
   parameter NUM_TSR_PER_DCU  = (NUM_SETS_PER_DCU == 256) ? 4 : (NUM_SETS_PER_DCU == 128) ? 2 : 1,
   parameter SET_WIDTH = $clog2(NUM_SETS_PER_DCU), // 6 (64 sets), 7 (128 sets), 8(256 sets) 
   parameter WAY_WIDTH = $clog2(NUM_WAYS_PER_SET),  // 4 always
   // Add a pipeline stage to output of TSR to meet timing.
   // introduces 1 cycle latency, replace with 0 if not needed.
   parameter logic TSR_OP_REGISTER = 1'b1
   )
   (
    input logic 		 clk,
    input logic 		 reset,

    // Write tag and state.
    input logic [17:0] 		 wr_tag_i,
    input logic [17:0] 		 wr_stt_i,
    input logic [SET_WIDTH-1:0]  wr_set_i,
    input logic [WAY_WIDTH-1:0]  wr_way_i,
    input logic 		 wr_en_i,

    // Compare tag in a set to get matching way. 
    input logic [SET_WIDTH-1:0]  rd_tag_set_i,
    input logic [17:0] 		 rd_tag_to_cmp_i,
    input logic 		 rd_tag_en_i,
    output logic [17:0] 	 rd_tag_stt_o,
    output logic [WAY_WIDTH-1:0] rd_tag_way_o,
    output logic 		 rd_tag_hit_o,
    output logic 		 rd_tag_set_full_o,
    // do not trust rest of rd tag results if
    // bit is set. give highest priority. 
    output logic 		 rd_tag_nxm_o,

    // Module is busy all inputs will be ignored.
    output logic 		 tsu_busy_o
    );

   localparam SET_ROW_WIDTH = 3;
   localparam logic [SET_ROW_WIDTH-1:0] SET_ROW_LIMIT = (NUM_SETS_PER_DCU == 64)  ? 'd4 :
			                  (NUM_SETS_PER_DCU == 128) ? 'd2 : 'd1;
   localparam logic [SET_ROW_WIDTH-1:0] FIRST_SET_ROW = 'd1;
   localparam MAX_NUM_TSR = 4;
   localparam NUM_TSR_WIDTH = $clog2(MAX_NUM_TSR);
   
			    
   
   initial begin
      if(! (NUM_SETS_PER_DCU inside {64, 128, 256}) ) begin
	 $error("Error: Number of sets per DCU can be 64, 128 or 256 (instance %m)");
	 $finish;
      end
      if(NUM_WAYS_PER_SET != 16) begin
	 $error("Error: NUM_WAYS_PER_SET has to be 16 (instance %m)");
	 $finish;
      end
   end

   typedef struct packed {
      logic [17:0] wr_tag;
      logic [17:0] wr_stt;
      logic [7:0]  wr_set_id;
      logic [1:0]  wr_way_id;
      logic 	   wr_en;
      logic [7:0]  rd_tag_set_id;
      logic [17:0] rd_tag_to_cmp;
      logic 	   rd_tag_en;
      logic [1:0]  rd_way_id;
      logic 	   hit;
      logic 	   set_id_full;
      logic 	   rd_tag_nxm;
      logic [7:0]  rd_stt_set_id;
      logic [1:0]  rd_stt_way_id;
      logic 	   rd_stt_en;
      logic [19:0] way_id_stt;
      logic 	   tsr_busy;
   } tsr_if_t;

   tsr_if_t tsr[NUM_TSR_PER_DCU-1:0];
   logic 	   cur_sr_full;
   logic 	   cur_sr_hit;
   logic 	   cur_sr_nxm;
   logic [3:0] 	   cur_sr_way;
   logic [1:0] 	   cur_hit_tsr_id;
   logic [1:0] 	   cur_hit_way_id;
   logic [1:0] 	   cur_nfull_tsr_id;
   logic [1:0] 	   cur_nfull_way_id;
   logic 	   any_tsr_busy;
   logic 	   c_rd_stt_en;
   
   
   // registers.
   enum 	   {IDLE, WAIT_TSR_BUSY_RD_CMP_TAG, RD_CMP_TAG, WAIT_TSR_BUSY_RD_STT, RD_STT} state_reg, state_next;
   logic [SET_WIDTH-1:0] rd_tag_set_reg = '0, rd_tag_set_next;
   logic [17:0] 	 rd_tag_to_cmp_reg = '0, rd_tag_to_cmp_next;
   logic [SET_ROW_WIDTH-1:0] set_row_reg = '0, set_row_next;
   logic 		     rd_tag_hit_reg = '0, rd_tag_hit_next;
   logic 		     rd_tag_set_full_reg = '0, rd_tag_set_full_next;
   logic 		     rd_tag_nxm_reg = '0, rd_tag_nxm_next;
   logic [WAY_WIDTH-1:0]     rd_tag_way_reg	= '0, rd_tag_way_next;
   logic [SET_WIDTH-1:0]     rd_stt_set;
   logic [WAY_WIDTH-1:0]     rd_stt_way;
   logic [SET_ROW_WIDTH-1:0] cur_set_row, prv_set_row;
   logic [NUM_TSR_WIDTH-1:0] hit_tsr_id;
   
   
   // Controller signal
   logic 	   c_wr_en;
   logic 	   c_rd_tag_en;
   logic 	   c_latch_rd_tag;
   logic 	   c_inc_set_row;
   logic 	   c_clr_set_row;
   logic 	   c_busy;

   // Output signals. 
   assign tsu_busy_o = any_tsr_busy | c_busy;
   assign rd_tag_way_o = rd_tag_way_reg;
   assign rd_tag_hit_o = rd_tag_hit_reg;
   assign rd_tag_set_full_o = rd_tag_set_full_reg;
   assign rd_tag_nxm_o = rd_tag_nxm_reg;

   
   // controller is busy if not in IDLE state. 
   assign c_busy = (state_reg == IDLE) ? 1'b0 : 1'b1;

   // the stored "hit" set and way is also used to read the state.
   assign rd_stt_set = rd_tag_set_reg;
   assign rd_stt_way = rd_tag_way_reg;
   assign cur_set_row = set_row_reg;
   assign prv_set_row = set_row_reg - 1;
   
   genvar i;
   generate
      for( i = 0; i < NUM_TSR_PER_DCU; i++) begin : TSR_GEN
	 tag_state_ram #
	       (
		.OP_REGISTER(TSR_OP_REGISTER)
		)
	       tsr_i
	       (
		.clk(clk),
		.reset(reset),
		// Write tag and state i/f
		.wr_tag_i        (tsr[i].wr_tag   ),
		.wr_stt_i        (tsr[i].wr_stt   ),
		.wr_set_id_i     (tsr[i].wr_set_id),
		.wr_way_id_i     (tsr[i].wr_way_id),
		.wr_en_i         (tsr[i].wr_en    ),
		// Read tags i/f
		.rd_tag_set_id_i (tsr[i].rd_tag_set_id),
		.rd_tag_to_cmp_i (tsr[i].rd_tag_to_cmp),
		.rd_tag_en_i     (tsr[i].rd_tag_en    ),
		.rd_way_id_o     (tsr[i].rd_way_id    ),
		.hit_o           (tsr[i].hit          ),
		.set_id_full_o   (tsr[i].set_id_full  ),
		.rd_tag_nxm_o    (tsr[i].rd_tag_nxm   ),
		// Read state i/f
		.rd_stt_set_id_i (tsr[i].rd_stt_set_id), 
		.rd_stt_way_id_i (tsr[i].rd_stt_way_id), 
		.rd_stt_en_i     (tsr[i].rd_stt_en    ), 
		.way_id_stt_o    (tsr[i].way_id_stt   ), 
		// busy 
		.tsr_busy_o      (tsr[i].tsr_busy)
		);
      end : TSR_GEN
   endgenerate
   
   generate
      always_comb begin : TSR_SIGNALS
	 rd_tag_to_cmp_next = rd_tag_to_cmp_reg;
	 rd_tag_set_next = rd_tag_set_reg;
	 for( int j=0; j<NUM_TSR_PER_DCU; j=j+1 ) begin

	    // Write tag, state interface:
	    // Broadcast tag and state to all TSRs.
	    // wr_way[1:0] is thw wr_way_id.
	    tsr[j].wr_tag    = wr_tag_i;
	    tsr[j].wr_stt    = wr_stt_i;
	    tsr[j].wr_way_id = wr_way_i[1:0];

	    if(NUM_SETS_PER_DCU == 64) begin
	       // wr_set_i (6 bits) indicates the set to write to,
	       // wr_way_i[3:2] indicates set_row to write this. 
	       // set_id (8 bits) = {wr_set_i (6bits), wr_way_i[3:2] (2bits)}
	       // since there is only 1 TSR, broadcast the write enable.
	       tsr[j].wr_set_id = {wr_set_i, wr_way_i[3:2]};
	       tsr[j].wr_en     = c_wr_en;
	    end else if (NUM_SETS_PER_DCU == 128) begin
	       // wr_set_i (7 bits) indicates the set to write to,
	       // wr_way_i[3] indicates which TSR to write to.
	       // wr_way_i[2] indicates which set_row to write to.
	       // set_id (8bits) = {wr_set_i (7bits)}, wr_way_i[2] (1bit)}.
	       // Broadcast the tag, stt info to both TSRs but enable only the
	       // tsr that matches the value in wr_way_i[3].
	       // there are 2 tsrs indexed by "j" lsb of j should match
	       // wr_way_i[3].
	       tsr[j].wr_set_id = {wr_set_i, wr_way_i[2]};
	       tsr[j].wr_en     = (j[0] == wr_way_i[3]) ? c_wr_en : 1'b0;
	    end else begin
	       // NUM_SETS_PER_DCU == 256, there are 4 TSRs. 
	       // wr_set_i (8 bits) indicates the set to write to,
	       // wr_way_i[3:2] indicates which TSR to write to.
	       // set_id(8bits) = wr_set_i(8bits),
	       // TSR index j[1:0] should match wr_way_i[3:2] for write enable.
	       tsr[j].wr_set_id = wr_set_i;
	       tsr[j].wr_en = (j[1:0] == wr_way_i[3:2]) ?  c_wr_en : 1'b0;
	    end
	    
	    // Read tag input interface.
	    // Broadcast tag to compare to all TSRs.
	    // The controller decides whether input tag or 
	    // stored tag is compared.
	    // Read all TSRs in parallel. 
	    if(c_latch_rd_tag) begin
	       rd_tag_to_cmp_next   = rd_tag_to_cmp_i;
	       rd_tag_set_next      = rd_tag_set_i;
	       tsr[j].rd_tag_to_cmp = rd_tag_to_cmp_i;
	    end else begin
	       tsr[j].rd_tag_to_cmp = rd_tag_to_cmp_reg;
	    end
	    tsr[j].rd_tag_en = c_rd_tag_en;
	    //set id.
	    if(NUM_SETS_PER_DCU == 64) begin
	       // Only 1 TSR. 
	       // rd_tag_set_i is 6 bits.
	       // in this configuration each set has 4 set_rows.
	       // the current set_row is stored in cur_set_row. 
	       // rd_tag_set_id(8) = {rd_tag_set_i(6b),cur_set_row(2b)}
	       tsr[j].rd_tag_set_id = {rd_tag_set_i, cur_set_row[1:0]};
	    end else if (NUM_SETS_PER_DCU == 128) begin
	       // 2 TSRs.
	       // rd_tag_set_i is 7 bits.
	       // each set has 2 TSR set_rows in this config.
	       // current set row is stored in cur_set_row[0].
	       // rd_tag_set_id(8b) = {rd_tag_set_i(7b), cur_set_row[0](1b)}
	       tsr[j].rd_tag_set_id = {rd_tag_set_i, cur_set_row[0]};
	    end else begin
	       // 4 TSRs.
	       // rd_tag_set_i is 8 bits.
	       // each set has only 1 row in this config.
	       // there are no set rows
	       // rd_tag_set_id(8b) = rd_tag_set_i
	       tsr[j].rd_tag_set_id = rd_tag_set_i;
	    end
	    
	    // Read state:
	    // State is read only when there is a hit.
	    // Addressing for reading a state is similar 
	    // to write tag, state scenario. 
	    tsr[j].rd_stt_way_id = rd_stt_way[1:0];
	    if(NUM_SETS_PER_DCU == 64) begin
	       tsr[j].rd_stt_set_id = {rd_stt_set, rd_stt_way[3:2]};
	       tsr[j].rd_stt_en = c_rd_stt_en;
	    end else if (NUM_SETS_PER_DCU == 128) begin
	       tsr[j].rd_stt_set_id = {rd_stt_set, rd_stt_way[2]};
	       tsr[j].rd_stt_en = (j[0] == rd_stt_way[3]) ? c_rd_stt_en : 1'b0;
	    end else begin
	       tsr[j].rd_stt_set_id = rd_stt_set;
	       tsr[j].rd_stt_en = (j[1:0] == rd_stt_way[3:2]) ? c_rd_stt_en : 1'b0;
	    end
	    
	 end // for all TSRs.
	 
      end : TSR_SIGNALS
   endgenerate

   // Results aggregate for read tag and read state.
   // Read tag results aggregate.
   //  A hit is atleast one of TSRs for current set_row 
   //  has a hit.
   //  A current set row full if all TSRs currently are full.
   //  A tsu is full when all TSRs are full.
   //  even one tsr gives nxm, nxm should be set.
   always_comb begin : RD_RES_AGG
      any_tsr_busy = 1'b0;
      cur_sr_full = 1'b1;
      cur_sr_hit = 1'b0;
      cur_sr_nxm = 1'b0;
      cur_hit_tsr_id = '0;
      cur_hit_way_id = '0;
      cur_nfull_tsr_id = '0;
      cur_nfull_way_id = '0;

      for( integer j=0; j<NUM_TSR_PER_DCU; j=j+1 ) begin
	 any_tsr_busy = any_tsr_busy | tsr[j].tsr_busy;
	 cur_sr_hit   = cur_sr_hit   | tsr[j].hit;
	 cur_sr_full  = cur_sr_full  & tsr[j].set_id_full;
	 cur_sr_nxm   = cur_sr_nxm   | tsr[j].rd_tag_nxm;

	 if(tsr[j].hit) begin
            cur_hit_tsr_id = j[1:0];
            cur_hit_way_id = tsr[j].rd_way_id;
	 end

	 if(~tsr[j].set_id_full) begin
            cur_nfull_tsr_id = j[1:0];
            cur_nfull_way_id = tsr[j].rd_way_id;
	 end

      end 
   end : RD_RES_AGG

   generate
      // Read TAG way information: Generate based on output of TSRs.
      // Generate way information based on output of TSRs.
      // Read state results aggregate:
      //  state is read only when there is a hit. 
      //  1 TSR scenario: the output state is output from the TSR.
      //  2,4 TSR scenario: Results from the TSR 
      //  that has s hit will be muxed to output.
      
      always_comb begin : GEN_WAY
	 cur_sr_way = '0;
	 rd_tag_stt_o = tsr[0].way_id_stt[17:0];
	 // Generate different hardware for different NUM_SETS_PER_DCU.
	 //   how way is configured depends on this parameter.
	 //   check summary table for more information. 
	 // If there is a hit, choose hit way. 
	 // If there is a miss,
	 //   if not full, choose empty way.
	 //   if full, choose way 0 by default.
	 // Results for a set row are available 1 cycle later
	 // so these are results for prv_set_row.
	 if(NUM_SETS_PER_DCU == 64) begin
	    if(cur_sr_hit) begin
	       cur_sr_way = {prv_set_row[1:0], cur_hit_way_id};
	    end else begin
	       if(~cur_sr_full) begin
		  cur_sr_way = {prv_set_row[1:0], cur_nfull_way_id};
	       end else begin
		  cur_sr_way = '0;
	       end
	    end
	    rd_tag_stt_o = tsr[0].way_id_stt[17:0];
	 end else if(NUM_SETS_PER_DCU == 128) begin
	    if(cur_sr_hit) begin
	       cur_sr_way = {cur_hit_tsr_id[0], prv_set_row[0], cur_hit_way_id};
	    end else begin
	       if(~cur_sr_full) begin
		  cur_sr_way = {cur_nfull_tsr_id[0], prv_set_row[0], cur_nfull_way_id};
	       end else begin
		  cur_sr_way = '0;
	       end
	    end
	    // rd_stt_way[3] indicates which TSR is hit. 
	    rd_tag_stt_o = tsr[rd_stt_way[3]].way_id_stt[17:0];
	 end else begin
	    // NUM_SETS_PER_DCU = 256
	    if(cur_sr_hit) begin
	       cur_sr_way = {cur_hit_tsr_id[1:0], cur_hit_way_id};
	    end else begin
	       if(~cur_sr_full) begin
		  cur_sr_way = {cur_nfull_tsr_id[1:0], cur_nfull_way_id};
	       end else begin
		  cur_sr_way = '0;
	       end
	    end
	    // rd_stt_way[3:2] indicates which TSR is hit. 
	    rd_tag_stt_o = tsr[rd_stt_way[3:2]].way_id_stt[17:0];
	 end      
      end : GEN_WAY
   endgenerate

   
   // Initially start with IDLE scenario where it waits for
   // all TSRs to be ready before accepting inputs.
   // cannot read and write to TSRs at the same time and hence
   // controller prioritizes write over read.
   // Writing tag and state takes only 1 cycle and controller
   // can continue sampling inputs after this operation is completed.
   always_comb begin : CONTROLLER
      state_next = state_reg;
      c_wr_en = 1'b0;
      c_latch_rd_tag = 1'b0;
      c_rd_tag_en = 1'b0;
      c_inc_set_row = 1'b0;
      c_clr_set_row = 1'b0;
      rd_tag_hit_next	   = rd_tag_hit_reg;
      rd_tag_set_full_next = rd_tag_set_full_reg;
      rd_tag_nxm_next	   = rd_tag_nxm_reg;
      rd_tag_way_next	   = rd_tag_way_reg;
      c_rd_stt_en = 1'b0;
      
      case(state_reg)
	IDLE: begin
	   if(any_tsr_busy == 1'b1) begin
	      // Ignore all inputs. 
	      state_next = IDLE;
	   end else if(wr_en_i) begin
	      c_wr_en = 1'b1;
	      state_next = IDLE;
	   end else if (rd_tag_en_i) begin
	      // latch the tag to compare and
	      // read set_row 0.
	      // incr the set_row_reg anticipating next read.
	      // then go to comparing and reading tag. 
	      c_rd_tag_en = 1'b1;
	      c_latch_rd_tag = 1'b1;
	      c_inc_set_row = 1'b1;
	      if(TSR_OP_REGISTER == 1'b1) begin
		 state_next = WAIT_TSR_BUSY_RD_CMP_TAG;
	      end else begin
		 state_next = RD_CMP_TAG;
	      end
	   end
	end //IDLE
	
	WAIT_TSR_BUSY_RD_CMP_TAG: begin
	   if(any_tsr_busy == 1'b1) begin
	      // wait till tsrs become not busy.
	      state_next = WAIT_TSR_BUSY_RD_CMP_TAG;
	   end else begin
	      state_next = RD_CMP_TAG;
	   end
	end
	
	RD_CMP_TAG : begin
	   if(cur_set_row == FIRST_SET_ROW) begin
	      // reading the first set row, latch fresh results.
	      rd_tag_hit_next = (cur_sr_nxm == 1'b0) ? cur_sr_hit : 1'b0;
	      rd_tag_set_full_next = cur_sr_full;
	      rd_tag_nxm_next = cur_sr_nxm;
	      rd_tag_way_next = cur_sr_way;
	   end else begin
	      // the previous set_row that was read had a miss and
	      // results from new set_row are available.

	      // Current set row can be hit or miss, provided
	      // a non existant tag is not compared. 
	      rd_tag_hit_next = (cur_sr_nxm == 1'b0) ? cur_sr_hit : 1'b0;

	      // if an invalid tag was compared in previous set row, nxm would
	      // be set to 1 and that value should be retained. 
	      rd_tag_nxm_next = (rd_tag_nxm_reg == 1'b1) ? rd_tag_nxm_reg : cur_sr_nxm;

	      // if previous set row had ane empty way, then there
	      // are empty ways available in this set. if not
	      // latch full result of current set_row. 
	      rd_tag_set_full_next = (rd_tag_set_full_reg == 1'b0) ? rd_tag_set_full_reg : cur_sr_full;
	      
	      // Latching way that is returned after reading current set row.
	      // Upon hit, the way returned would be hit way and has to be latched.
	      // if miss and current set row is not full, an empty way would be returned,
	      // latch it.
	      // if miss and current set row is full, no need to update the way latched
	      // in the previous set_row iteration. 
	      if(cur_sr_hit) begin
		 rd_tag_way_next = cur_sr_way;
	      end else begin
		 if(~cur_sr_full) begin
		    rd_tag_way_next = cur_sr_way;
		 end else begin
		    rd_tag_way_next = rd_tag_way_reg;
		 end
	      end
	   end //if(cur_set_row == FIRST_SET_ROW)
	   
	   // State machine progression:
	   // if there is a hit in the current set row,
	   // then the hit way is latched in this cycle,
	   // no need to read the next set_row, 
	   // continue reading the state for the hit way.
	   //
	   // if current set row is a miss, check
	   // if all set_rows are read: 
	   //  If all set_rows are read then the tag 
	   //  is a miss and hit signal would be 0. 
	   //  full and way signals are also latched. 
	   //  No need to read state go to idle.
	   // if all set_rows are not read:
	   //  read the next set_row.
	   if(cur_sr_nxm == 1'b1) begin
	      // comparing a tag for non existant memory,
	      // do not trust rest of results, go to idle. 
	      c_clr_set_row = 1'b1;
	      state_next = IDLE;
	   end else if(cur_sr_hit == 1'b1) begin
	      // hit:
	      // set_row_reg information already 
	      // stored in hit way. clear it.
	      c_clr_set_row = 1'b1;
	      state_next = RD_STT;
	   end else begin
	      // miss.
	      if(cur_set_row == SET_ROW_LIMIT) begin
		 // Read all set_rows but still a miss.
		 // No need to read state.
		 c_clr_set_row = 1'b1;
		 state_next = IDLE;
	      end else begin
		 c_inc_set_row = 1'b1;
		 c_rd_tag_en = 1'b1;
		 if(TSR_OP_REGISTER == 1'b1) begin
		    state_next = WAIT_TSR_BUSY_RD_CMP_TAG;
		 end else begin
		    state_next = RD_CMP_TAG;
		 end
	      end
	   end
	end // RD_CMP_TAG

	RD_STT: begin
	   // read state information.
	   // wait for TSR to become not busy
	   c_rd_stt_en = 1'b1;
	   if(TSR_OP_REGISTER == 1'b1) begin
	      state_next = WAIT_TSR_BUSY_RD_STT;
	   end else begin
	      state_next = IDLE;
	   end
	end // RD_STT

	WAIT_TSR_BUSY_RD_STT: begin
	   if(any_tsr_busy == 1'b1) begin
	      // wait till tsrs become not busy.
	      state_next = WAIT_TSR_BUSY_RD_STT;
	   end else begin
	      // read state completed.
	      state_next = IDLE;
	   end
	end
      endcase
   end : CONTROLLER

   always_comb begin : SET_ROW_COUNTER
      if(c_clr_set_row == 1'b1) begin
	 set_row_next = '0;
      end else if(c_inc_set_row == 1'b1) begin
	 set_row_next = set_row_reg + 1'b1;
      end else begin
	 set_row_next = set_row_reg;
      end
   end : SET_ROW_COUNTER

   always_ff @(posedge clk) begin : REG_ASSIGN
      state_reg			<= state_next;
      rd_tag_set_reg		<= rd_tag_set_next;
      rd_tag_to_cmp_reg		<= rd_tag_to_cmp_next;
      set_row_reg		<= set_row_next;
      rd_tag_hit_reg		<= rd_tag_hit_next;
      rd_tag_set_full_reg	<= rd_tag_set_full_next;
      rd_tag_nxm_reg		<= rd_tag_nxm_next;
      rd_tag_way_reg		<= rd_tag_way_next;
      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset
	 state_reg		<= IDLE;
	 rd_tag_set_reg		<= '0;
	 rd_tag_to_cmp_reg	<= '0;
	 set_row_reg		<= '0;
	 rd_tag_hit_reg		<= '0;
	 rd_tag_set_full_reg	<= '0;
	 rd_tag_nxm_reg		<= '0;
	 rd_tag_way_reg		<= '0;
      end
   end : REG_ASSIGN
endmodule // dcu_tsu

`endif
