
module axis_comb_rr_arbTb();

   parameter ID_WIDTH = 5;
   parameter DATA_WIDTH = 64;
   parameter NUM_IN = 4;

   //input output ports 
   //Input signals
   logic 			     clk;
   logic 			     reset;
   logic [NUM_IN-1:0][ID_WIDTH-1:0]  us_id_i;
   logic [NUM_IN-1:0][DATA_WIDTH-1:0] us_data_i;
   logic [NUM_IN-1:0] 		      us_valid_i;
   logic 			      ds_ready_i;

   //Output signals
   logic [NUM_IN-1:0] 		      us_ready_o;
   logic [ID_WIDTH-1:0] 	      ds_id_o;
   logic [DATA_WIDTH-1:0] 	      ds_data_o;
   logic 			      ds_valid_o;


   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   axis_comb_rr_arb axis_comb_rr_arb1 (
				       .clk(clk),
				       .reset(reset),
				       .us_id_i(us_id_i),
				       .us_data_i(us_data_i),
				       .us_valid_i(us_valid_i),
				       .ds_ready_i(ds_ready_i),
				       .us_ready_o(us_ready_o),
				       .ds_id_o(ds_id_o),
				       .ds_data_o(ds_data_o),
				       .ds_valid_o(ds_valid_o)
				       );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports 
      clk = '0;
      reset = '0;
      us_id_i = '0;
      us_data_i = '0;
      //us_valid_i = '0;
      ds_ready_i = '0;

      for( integer i=0; i<NUM_IN; i=i+1 ) begin
	 us_data_i[i] = i+1;
      end
      ##5;
      reset = 1;
      ##5;
      reset = 1'b0;
      ds_ready_i = 1'b1;
      #500 $finish;
   end

   always_ff @(posedge clk) begin : REG_ASSIGN
      for( integer i=0; i<NUM_IN; i=i+1 ) begin
	 if(us_valid_i[i] & us_ready_o[i]) begin
	    us_valid_i[i] <= 1'b0;
	 end
      end 

      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset
	 us_valid_i <= '1;
      end
   end : REG_ASSIGN

endmodule
