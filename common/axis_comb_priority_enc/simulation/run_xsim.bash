#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
$XILINX_VIVADO/data/verilog/src/glbl.v \
../testbench/axis_comb_priority_encTb.sv \
../rtl/axis_comb_priority_enc.sv 

xelab -debug typical -incremental -L xpm worklib.axis_comb_priority_encTb worklib.glbl -s worklib.axis_comb_priority_encTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.axis_comb_priority_encTb 
xsim -gui worklib.axis_comb_priority_encTb 
