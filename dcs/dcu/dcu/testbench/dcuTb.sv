import eci_cmd_defs::*;
import eci_dirc_defs::*;
import eci_cc_defs::*;
import eci_dcs_defs::*;

// Uncomment following line to allow debug messages.
//`define DEBUG 1

module dcuTb();

   parameter PERF_REGS_WIDTH = 64;
   parameter SYNTH_PERF_REGS = 1;
   parameter NUM_SETS_PER_DCU = DS_NUM_SETS_PER_DCU;
   localparam NUM_WAYS_PER_SET = DS_NUM_WAYS_PER_SET;

   //input output ports 
   //Input signals
   logic 			     clk;
   logic 			     reset;
   logic [ECI_WORD_WIDTH-1:0] 	     eci_req_hdr_i;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] eci_req_vc_i;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] eci_req_size_i;
   logic 			     eci_req_valid_i;
   logic 			     eci_rsp_ready_i;
   logic 			     rd_req_ready_i;
   logic [6:0] 			     rd_rsp_id_i;
   logic 			     rd_rsp_valid_i;
   logic 			     wr_req_ready_i;
   logic [6:0] 			     wr_rsp_id_i;
   logic [1:0] 			     wr_rsp_bresp_i;
   logic 			     wr_rsp_valid_i;

   //Output signals
   logic 			     eci_req_ready_o;
   logic 			     eci_req_stall_o;
   logic [ECI_WORD_WIDTH-1:0] 	     eci_rsp_hdr_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] eci_rsp_vc_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] eci_rsp_size_o;
   logic 			     eci_rsp_valid_o;
   logic [6:0] 			     rd_req_id_o;
   logic [37:0] 		     rd_req_addr_o;
   logic 			     rd_req_valid_o;
   logic 			     rd_rsp_ready_o;
   logic [6:0] 			     wr_req_id_o;
   logic [37:0] 		     wr_req_addr_o;
   logic [ECI_CL_SIZE_BYTES-1:0]     wr_req_strb_o;
   logic 			     wr_req_valid_o;
   logic 			     wr_rsp_ready_o;
   logic [PERF_REGS_WIDTH-1:0] 	     prf_no_req_pkt_rdy_o;
   logic [PERF_REGS_WIDTH-1:0] 	     prf_no_req_pkt_stl_o;
   logic [PERF_REGS_WIDTH-1:0] 	     prf_no_rsp_pkt_sent_o;
   logic [PERF_REGS_WIDTH-1:0] 	     prf_no_rd_req_o;
   logic [PERF_REGS_WIDTH-1:0] 	     prf_no_rd_rsp_o;
   logic [PERF_REGS_WIDTH-1:0] 	     prf_no_wr_req_o;
   logic [PERF_REGS_WIDTH-1:0] 	     prf_no_wr_rsp_o;
   logic [PERF_REGS_WIDTH-1:0] 	     prf_no_cyc_bw_req_vld_rdy_o;
   logic [ECI_WORD_WIDTH-1:0] 	     dbg_eci_req_hdr_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] dbg_eci_req_vc_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] dbg_eci_req_size_o;
   logic 			     dbg_eci_req_ready_o;
   logic 			     dbg_eci_req_stalled_o;
   logic [ECI_WORD_WIDTH-1:0] 	     dbg_eci_rsp_hdr_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] dbg_eci_rsp_vc_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] dbg_eci_rsp_size_o;
   logic 			     dbg_eci_rsp_valid_o;
   cc_req_t dbg_req_cc_enc_o;
   logic 			     dbg_rdda_o;
   logic 			     dbg_wdda_o;
   logic 			     dbg_stall_prot_o;
   logic 			     dbg_stall_rd_busy_o;
   logic 			     dbg_stall_wr_busy_o;
   logic 			     dbg_stall_eci_tr_busy_o;
   logic 			     dbg_stall_rtg_full_o;
   logic 			     dbg_stall_wait_op_hs_o;
   cc_req_t dbg_cc_active_req_o;
   cc_state_t dbg_cc_present_state_o;
   cc_state_t dbg_cc_next_state_o;
   cc_action_t dbg_cc_next_action_o;
   dirc_err_t dbg_err_code_o;
   logic 			     dbg_error_o;
   logic 			     dbg_valid_o;

   logic [ECI_PACKET_SIZE-1:0][ECI_WORD_WIDTH-1:0] eci_req_pkt_i;
   logic [ECI_PACKET_SIZE-1:0][ECI_WORD_WIDTH-1:0] eci_rsp_pkt_o;
   eci_word_t exp_resp_header;
   eci_word_t act_resp_header;
   eci_word_t lc_lci_store_header;
   eci_word_t fwd_store_header;
   eci_hreqid_t my_hreq_id;
   eci_dmask_t my_dmask;
   logic my_ns;
   eci_cl_addr_t my_addr;
   eci_cl_data_t my_data;
   eci_cl_data_t my_rd_data;
   ds_cl_addr_t my_addr_rtg_casted;
   ds_cl_addr_t test_my_addr_c;

   assign eci_req_hdr_i = eci_req_pkt_i[0];
   assign eci_rsp_pkt_o[0] = eci_rsp_hdr_o;
   assign test_my_addr_c = ds_cl_addr_t'(my_addr);
   
   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   dcu #
     (
      .PERF_REGS_WIDTH(PERF_REGS_WIDTH),
      .SYNTH_PERF_REGS(SYNTH_PERF_REGS)
      )
   dcu1 (
	 .clk(clk),
	 .reset(reset),
	 .eci_req_hdr_i(eci_req_hdr_i),
	 .eci_req_vc_i(eci_req_vc_i),
	 .eci_req_size_i(eci_req_size_i),
	 .eci_req_valid_i(eci_req_valid_i),
	 .eci_rsp_ready_i(eci_rsp_ready_i),
	 .rd_req_ready_i(rd_req_ready_i),
	 .rd_rsp_id_i(rd_rsp_id_i),
	 .rd_rsp_valid_i(rd_rsp_valid_i),
	 .wr_req_ready_i(wr_req_ready_i),
	 .wr_rsp_id_i(wr_rsp_id_i),
	 .wr_rsp_bresp_i(wr_rsp_bresp_i),
	 .wr_rsp_valid_i(wr_rsp_valid_i),
	 .eci_req_ready_o(eci_req_ready_o),
	 .eci_req_stall_o(eci_req_stall_o),
	 .eci_rsp_hdr_o(eci_rsp_hdr_o),
	 .eci_rsp_vc_o(eci_rsp_vc_o),
	 .eci_rsp_size_o(eci_rsp_size_o),
	 .eci_rsp_valid_o(eci_rsp_valid_o),
	 .rd_req_id_o(rd_req_id_o),
	 .rd_req_addr_o(rd_req_addr_o),
	 .rd_req_valid_o(rd_req_valid_o),
	 .rd_rsp_ready_o(rd_rsp_ready_o),
	 .wr_req_id_o(wr_req_id_o),
	 .wr_req_addr_o(wr_req_addr_o),
	 .wr_req_strb_o(wr_req_strb_o),
	 .wr_req_valid_o(wr_req_valid_o),
	 .wr_rsp_ready_o(wr_rsp_ready_o),
	 .prf_no_req_pkt_rdy_o(prf_no_req_pkt_rdy_o),
	 .prf_no_req_pkt_stl_o(prf_no_req_pkt_stl_o),
	 .prf_no_rsp_pkt_sent_o(prf_no_rsp_pkt_sent_o),
	 .prf_no_rd_req_o(prf_no_rd_req_o),
	 .prf_no_rd_rsp_o(prf_no_rd_rsp_o),
	 .prf_no_wr_req_o(prf_no_wr_req_o),
	 .prf_no_wr_rsp_o(prf_no_wr_rsp_o),
	 .prf_no_cyc_bw_req_vld_rdy_o(prf_no_cyc_bw_req_vld_rdy_o),
	 .dbg_eci_req_hdr_o(dbg_eci_req_hdr_o),
	 .dbg_eci_req_vc_o(dbg_eci_req_vc_o),
	 .dbg_eci_req_size_o(dbg_eci_req_size_o),
	 .dbg_eci_req_ready_o(dbg_eci_req_ready_o),
	 .dbg_eci_req_stalled_o(dbg_eci_req_stalled_o),
	 .dbg_eci_rsp_hdr_o(dbg_eci_rsp_hdr_o),
	 .dbg_eci_rsp_vc_o(dbg_eci_rsp_vc_o),
	 .dbg_eci_rsp_size_o(dbg_eci_rsp_size_o),
	 .dbg_eci_rsp_valid_o(dbg_eci_rsp_valid_o),
	 .dbg_req_cc_enc_o(dbg_req_cc_enc_o),
	 .dbg_rdda_o(dbg_rdda_o),
	 .dbg_wdda_o(dbg_wdda_o),
	 .dbg_stall_prot_o(dbg_stall_prot_o),
	 .dbg_stall_rd_busy_o(dbg_stall_rd_busy_o),
	 .dbg_stall_wr_busy_o(dbg_stall_wr_busy_o),
	 .dbg_stall_eci_tr_busy_o(dbg_stall_eci_tr_busy_o),
	 .dbg_stall_rtg_full_o(dbg_stall_rtg_full_o),
	 .dbg_stall_wait_op_hs_o(dbg_stall_wait_op_hs_o),
	 .dbg_cc_active_req_o(dbg_cc_active_req_o),
	 .dbg_cc_present_state_o(dbg_cc_present_state_o),
	 .dbg_cc_next_state_o(dbg_cc_next_state_o),
	 .dbg_cc_next_action_o(dbg_cc_next_action_o),
	 .dbg_err_code_o(dbg_err_code_o),
	 .dbg_error_o(dbg_error_o),
	 .dbg_valid_o(dbg_valid_o)
	 );

   word_addr_mem #
     (
      .ID_WIDTH('d7),
      .ADDR_WIDTH(10),
      .DATA_WIDTH(ECI_CL_WIDTH)
      )
   mem_inst_1
     (
      .clk(clk),
      .reset(reset),

      .rd_req_id_i(rd_req_id_o),
      .rd_req_addr_i(rd_req_addr_o[9:0]),
      .rd_req_valid_i(rd_req_valid_o),
      .rd_req_ready_o(rd_req_ready_i),
      
      .wr_req_id_i(wr_req_id_o),
      .wr_req_addr_i(wr_req_addr_o[9:0]),
      .wr_req_data_i('0), // only header no data. 
      .wr_req_valid_i(wr_req_valid_o),
      .wr_req_ready_o(wr_req_ready_i),
      
      .rd_rsp_id_o(rd_rsp_id_i),
      .rd_rsp_data_o(), //unused
      .rd_rsp_valid_o(rd_rsp_valid_i),
      .rd_rsp_ready_i(rd_rsp_ready_o),
      
      .wr_rsp_id_o(wr_rsp_id_i),
      .wr_rsp_bresp_o(wr_rsp_bresp_i),
      .wr_rsp_valid_o(wr_rsp_valid_i),
      .wr_rsp_ready_i(wr_rsp_ready_o)
      );
  

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();
      // Initialize Input Ports 
      clk = '0;
      reset = '0;
      eci_req_pkt_i = '0;
      eci_req_vc_i = '0;
      eci_req_size_i = '0;
      eci_req_valid_i = '0;
      eci_rsp_ready_i = '0;

      my_hreq_id = '0;
      my_dmask = '0;
      my_addr = '0;
      my_data = '0;
      my_rd_data = '0;
      my_ns = 1'b1;
      
      reset_system();
      //always ready to accept rsp.
      eci_rsp_ready_i = 1'b1;
      ##5;

      // Remember when writing test cases:
      // DCU_ID should be 0 in all CL addresses.
      
      test_r13_ra3_v31d_rr_rra();
      test_r12_ra2_v21();
      test_r13_ra3_v31();
      test_r13_ra3_v21_v32d_rr_rra();
      test_stall_tsu_full();
      reset_system();
      test_r13_reset_r13_v31();
      test_protocol_stall();
      reset_system();
      test_r12_ra2_r23_ra3_v31();
      test_nxm();
      assert(dbg_error_o === 1'b1) else 
      	$fatal(1,"DCU should be in error state here.");
      reset_system();
      test_lci_lcia_ul();
      test_lci_f31_v32d_a11_v21_ul();
      test_lci_f31_a31d_lcia_ul();
      test_lci_f21_a21_lcia_ul();
      test_lci_f21_a11_lcia_v21_ul();
      test_lc_lca_ul();
      test_lc_lca_v21_ul();
      test_lc_f32_a32d_lca_ul();
      test_lc_f32_v21_v32_a11_ul();
      test_lci_f21_lci();
      test_lci_stall_tsu_full();
      
      
      ##1;
      $display("All tests pass");
      #500 $finish;
   end 

   // This DCU should always receive only addresses
   // corresponding to DCU_ID 0. ie this DCU has 
   // DCU_ID 0.
   always_ff @(posedge clk) begin : REG_ASSIGN
      assert(test_my_addr_c.dcu_id === '0) else 
	$fatal(1, "Test case is wrong the dcu_id in my_addr should always be 0.");
      assert(my_ns === 1'b1) else
	$fatal(1,"NS bit should always be 1, DCU needs to be enhanced otherwise.");
   end : REG_ASSIGN
   
   //---------------------------------------------------------------
   // Tasks and functions below
   // Tasks and functions below
   task static reset_system();
      $display("Resetting system");
      ##5;
      reset = 1'b1;
      ##20;
      reset = 1'b0;
   endtask // reset_system

   task static test_lci_stall_tsu_full();
      $display("test test_lci_stall_tsu_full in progress.");
      // Bug encountered in real life
      //
      // Bug: lci was not creating an RTG entry before
      // but now it does. if there is a miss in the directory,
      // and the directory has no empty spot available lci should
      // get stalled. Formerly LCI did not create a new entry so
      // a response was sent without disturbing contents of RTG.
      // but now LCI even when there is a miss creates entry for
      // locking the CL. Since LCI is not marked as an event that
      // creates an entry it updates a previously existing entry
      // which causes problem later when an ECI message arrives
      // for overwritten entry.
      //
      // Make a set full, then issue lci.
      //
      // All 16 ways of set 1 are filled with
      // tags 1 to 16. 
      make_set1_full_r12();
      //
      // send LCI for a tag that is not present in set 1.
      // for ex: tag 17.
      //
      my_addr_rtg_casted = '0;
      my_addr_rtg_casted.set_within_dcu = 1; // Set is always 1.
      my_dmask = '1;
      my_addr_rtg_casted.tag = 17;
      my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
      my_hreq_id = $urandom_range(0,63);
      send_lci(
	       .my_id_i(eci_hreqid_t'(my_hreq_id)),
	       .my_dmask_i(my_dmask),
	       .my_ns_i(my_ns),
	       .my_addr_i(my_addr.flat)
	       );
      //
      // LCI should have been stalled but with the bug,
      // it would have overwritten one of the tags.
      // sending voluntary downgrade to all previously
      // updated tags and this will cause dcu to error out.
      //
      // without the bug, the lci would be stalled and issuing
      // downgrades will not cause the issue.
      make_set1_empty_v21();
      //
      // now send lci and get lcia.
      my_addr_rtg_casted = '0;
      my_addr_rtg_casted.set_within_dcu = 1; // Set is always 1.
      my_dmask = '1;
      my_addr_rtg_casted.tag = 17;
      my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
      my_hreq_id = $urandom_range(0,63);
      send_lci(
	       .my_id_i(eci_hreqid_t'(my_hreq_id)),
	       .my_dmask_i(my_dmask),
	       .my_ns_i(my_ns),
	       .my_addr_i(my_addr.flat)
	       );
      // no need to wait as when lci handshake
      // happens lcia (rsp_valid) handshake also
      // happens. 
      //wait(eci_rsp_valid_o & eci_rsp_ready_i);
      // Receive LCIA
      exp_resp_header = eci_cmd_defs::lcl_gen_lcia(.req_i(eci_req_pkt_i[0]));
      act_resp_header = eci_word_t'(eci_rsp_pkt_o[0]);
      if(act_resp_header.eci_word !== exp_resp_header.eci_word) begin
	 $error("Error: The response LCIA does not match expected LCIA");
	 $finish;
      end
      assert((eci_rsp_vc_o === VC_LCL_RESP_WO_DATA_E) |
	     (eci_rsp_vc_o === VC_LCL_RESP_WO_DATA_O))else
	$fatal(1,"LCIA not sent to correct VC.");
      assert(eci_rsp_size_o === 'd1) else
	$fatal(1,"LCIA size should be 1.");
      ##1; // wait for output valid signal to go low.
      assert(dcuTb.dcu1.cc_rom_inst.next_state_o == s1pUL__1) else 
	$fatal(1, "test_lci_lcia_ul expected state does not match actual");
      // CL should be locked here.
      ##10;
      test_unlock_locked_rs_inv(
			 .my_id_i(my_hreq_id),
			 .my_dmask_i(my_dmask),
			 .my_addr_i(my_addr.flat)
			 );
      assert(dcuTb.dcu1.cc_rom_inst.next_state_o == s1__1) else 
	$fatal(1, "Address is not invalid but should be.");
      $display("PASS: test_lci_stall_tsu_full\n");
   endtask //test_lci_stall_tsu_full

   task static test_lci_f21_lci();
      $display("test test_lci_f21_lci in progress.");
      // Reproducing bug encountered when testing on FPGA.
      //
      // Bug: A CLI for a shared CL causes F21 to be issued.
      // when F21 is in progress if anohter CLI arrives for a
      // different CL, it sends LCIA for the address that has
      // downgrade in progress.
      //
      // Upgrade cl(A) to shared then issue lci(A).
      // this will cause F21(A) to be issued.
      // when F21(A) is in progress, issue lci(B)
      // where B is in invalid state.
      // This should cause lcia(B) to be issued and
      // B should be locked. Unlock B and transactions
      // with B are completed.
      // Going back to transaction on cl(A), send
      // A21(A) should cause LCIA(A) to be issued.
      // Unlock(A) and make sure reads can proceed.
      my_addr_rtg_casted = '0;
      my_addr_rtg_casted.set_within_dcu = $urandom_range(0, 8092);
      my_addr_rtg_casted.dcu_id = 'd0; // DCU ID should be always 0.
      my_addr_rtg_casted.tag = $urandom_range(100, 5000);
      my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
      my_hreq_id = $urandom_range(0,63);
      my_dmask = '1;
      test_r12_ra2(
		   .my_id_i(my_hreq_id),
		   .my_dmask_i(my_dmask),
		   .my_addr_i(my_addr.flat)
		   );
      // CL is in state 2.
      // issue lci.
      my_ns = 1'b1; // NS hardcoded to 1 in DCU. 
      send_lci(
	       .my_id_i(eci_hreqid_t'(my_hreq_id)),
	       .my_dmask_i(my_dmask),
	       .my_ns_i(my_ns),
	       .my_addr_i(my_addr.flat)
	       );
      // Store LCI header. 
      lc_lci_store_header = eci_req_hdr_i;
      // no need to wait as when lci handshake
      // happens lcia (rsp_valid) handshake also
      // happens. 
      //wait(eci_rsp_valid_o & eci_rsp_ready_i);
      //
      // Check if F21 is issued.
      //  HREQ ID should be the DCU ID which is 0s in this case.
      exp_resp_header = eci_cmd_defs::eci_gen_sinv_h
			(
			 .hreq_id_i('0),
			 .dmask_i(my_dmask),
			 .ns_i(my_ns),
			 .addr_i(my_addr.flat)
			 );
      act_resp_header = eci_word_t'(eci_rsp_pkt_o[0]);
      if(act_resp_header.eci_word !== exp_resp_header.eci_word) begin
	 $error("Error: F21 is not issued.");
	 $finish;
      end
      assert((eci_rsp_vc_o === VC_FWD_WO_DATA_E) |
	     (eci_rsp_vc_o === VC_FWD_WO_DATA_O))else
	$fatal(1,"F21 not sent to correct VC.");
      assert(eci_rsp_size_o === 'd1) else
	$fatal(1,"F21 size should be 1.");
      fwd_store_header = act_resp_header;
      ##1;
      ##10;
      // An ECI transaction is in progress.
      // Issue LCI for a different CL that is in invalid state.
      // LCI(B)
      test_lci_lcia_ul();
      // Send A21 to retire the original request.
      // this will cause LCIA to be issued.
      // A21(A)
      send_A21(
	       .fwd_req_i(fwd_store_header)
	       );
      // check if LCIA is received for the first LCI 
      // that caused the forward downgrade. 
      exp_resp_header = eci_cmd_defs::lcl_gen_lcia(.req_i(lc_lci_store_header));
      act_resp_header = eci_word_t'(eci_rsp_pkt_o[0]);
      if(act_resp_header.eci_word !== exp_resp_header.eci_word) begin
	 $error("Error: LCIA does not match: Expected %0h, Actual %0h",
		exp_resp_header, act_resp_header);
	 $finish;
      end
      assert((eci_rsp_vc_o === VC_LCL_RESP_WO_DATA_E) |
	     (eci_rsp_vc_o === VC_LCL_RESP_WO_DATA_O))else
	$fatal(1,"LCIA not sent to correct VC.");
      assert(eci_rsp_size_o === 'd1) else
	$fatal(1,"LCIA size should be 1.");
      ##1;
      assert(dcuTb.dcu1.cc_rom_inst.next_state_o == s1pUL__1) else 
	$fatal(1, "Final state is not s1pUL__1.");
      // CL should be locked here. Send unlock for address
      // that got locked now.
      ##10;
      my_addr.flat = act_resp_header[ECI_ADDR_WIDTH-1:0];
      my_hreq_id = $urandom_range(0,63);
      my_dmask = '1;
      test_unlock_locked_rs_inv(
			 .my_id_i(my_hreq_id),
			 .my_dmask_i(my_dmask),
			 .my_addr_i(my_addr.flat)
			 );
      assert(dcuTb.dcu1.cc_rom_inst.next_state_o == s1__1) else 
	$fatal(1, "Address is not invalid but should be.");
      $display("PASS: test_lci_f21_lci");
   endtask //test_lci_f21_lci

   task static test_lc_f32_v21_v32_a11_ul();
      $display("test_lc_f32_v21_v32_a11_ul in progress.");
      my_addr_rtg_casted = '0;
      my_addr_rtg_casted.set_within_dcu = $urandom_range(0, 8092);
      my_addr_rtg_casted.dcu_id = 'd0; // DCU ID should be always 0.
      my_addr_rtg_casted.tag = $urandom_range(100, 5000);
      my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
      my_hreq_id = $urandom_range(0,63);
      my_dmask = '1;
      test_r13_ra3(
		   .my_id_i(my_hreq_id),
		   .my_dmask_i(my_dmask),
		   .my_addr_i(my_addr.flat)
		   );
      // CL is in state 3
      // issue lc.
      my_ns = 1'b1; // NS hardcoded to 1 in DCU. 
      send_lc(
	       .my_id_i(eci_hreqid_t'(my_hreq_id)),
	       .my_dmask_i(my_dmask),
	       .my_ns_i(my_ns),
	       .my_addr_i(my_addr.flat)
	       );
      // Store LC header. 
      lc_lci_store_header = eci_req_hdr_i;
      // no need to wait as when lci handshake
      // happens lcia (rsp_valid) handshake also
      // happens. 
      //wait(eci_rsp_valid_o & eci_rsp_ready_i);
      //
      // check if F32 is issued.
      //  HREQ ID should be the DCU ID which is 0s in this case.
      exp_resp_header = eci_cmd_defs::eci_gen_fldrs_eh
			(
			 .hreq_id_i('0),
			 .dmask_i(my_dmask),
			 .ns_i(my_ns),
			 .addr_i(my_addr.flat)
			 );
      fwd_store_header = exp_resp_header;
      act_resp_header = eci_word_t'(eci_rsp_pkt_o[0]);
      if(act_resp_header.eci_word !== exp_resp_header.eci_word) begin
	 $error("Error: F32 is not issued.");
	 $finish;
      end
      assert((eci_rsp_vc_o === VC_FWD_WO_DATA_E) |
	     (eci_rsp_vc_o === VC_FWD_WO_DATA_O))else
	$fatal(1,"F32 not sent to correct VC.");
      assert(eci_rsp_size_o === 'd1) else
	$fatal(1,"F32 size should be 1.");
      ##1;
      ##10;
      // Issue V21
      send_v21(
	       .my_addr_i(my_addr.flat),
	       .my_dmask_i(my_dmask)
	       );
      ##1;
      // LCA should not be sent here.
      // Issue V32
      send_v32(
	       .my_addr_i(my_addr.flat),
	       .my_dmask_i(my_dmask),
	       .my_data_i(my_data)
	       );
      // LCA should be received here.
      //wait(eci_rsp_valid_o & eci_rsp_ready_i);
      //##1;
      // check if LCA is received.
      exp_resp_header = eci_cmd_defs::lcl_gen_lca(.req_i(lc_lci_store_header));
      act_resp_header = eci_word_t'(eci_rsp_pkt_o[0]);
      if(act_resp_header.eci_word !== exp_resp_header.eci_word) begin
	 $error("Error: LCA does not match: Expected %0h, Actual %0h",
		exp_resp_header, act_resp_header);
	 $finish;
      end
      assert((eci_rsp_vc_o === VC_LCL_RESP_WO_DATA_E) |
	     (eci_rsp_vc_o === VC_LCL_RESP_WO_DATA_O))else
	$fatal(1,"LCA not sent to correct VC.");
      assert(eci_rsp_size_o === 'd1) else
	$fatal(1,"LCA size should be 1.");
      ##1;
      // Issue A11
      send_A11(
	       .fwd_req_i(fwd_store_header)
	       );
      // CL should be locked here and remote state is invalid.
      ##10;
      test_unlock_locked_rs_inv(
			 .my_id_i(my_hreq_id),
			 .my_dmask_i(my_dmask),
			 .my_addr_i(my_addr.flat)
			 );
      assert(dcuTb.dcu1.cc_rom_inst.next_state_o == s1__1) else 
	$fatal(1, "Address is not invalid but should be.");
      $display("PASS: test_lc_f32_v21_v32_a11_ul\n");
   endtask //test_lc_f32_v21_v32_a11_ul

   task static test_lc_lca_ul();
      $display("test_lc_lca_ul in progress.");
      // CL in state 2 is already clean.
      my_addr_rtg_casted = '0;
      my_addr_rtg_casted.set_within_dcu = $urandom_range(0, 8092);
      my_addr_rtg_casted.dcu_id = 'd0; // DCU ID should be always 0.
      my_addr_rtg_casted.tag = $urandom_range(100, 5000);
      my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
      my_hreq_id = $urandom_range(0,63);
      my_dmask = '1;
      test_r12_ra2(
		   .my_id_i(my_hreq_id),
		   .my_dmask_i(my_dmask),
		   .my_addr_i(my_addr.flat)
		   );
      // CL is in state 2.
      // send LC.
      my_ns = 1'b1; // NS hardcoded to 1 in DCU. 
      send_lc(
	       .my_id_i(eci_hreqid_t'(my_hreq_id)),
	       .my_dmask_i(my_dmask),
	       .my_ns_i(my_ns),
	       .my_addr_i(my_addr.flat)
	       );
      // Store LC header. 
      lc_lci_store_header = eci_req_hdr_i;
      // no need to wait as when lci handshake
      // happens lcia (rsp_valid) handshake also
      // happens. 
      //wait(eci_rsp_valid_o & eci_rsp_ready_i);
      // check if LCA is received.
      exp_resp_header = eci_cmd_defs::lcl_gen_lca(.req_i(lc_lci_store_header));
      act_resp_header = eci_word_t'(eci_rsp_pkt_o[0]);
      if(act_resp_header.eci_word !== exp_resp_header.eci_word) begin
	 $error("Error: LCA does not match: Expected %0h, Actual %0h",
		exp_resp_header, act_resp_header);
	 $finish;
      end
      assert((eci_rsp_vc_o === VC_LCL_RESP_WO_DATA_E) |
	     (eci_rsp_vc_o === VC_LCL_RESP_WO_DATA_O))else
	$fatal(1,"LCA not sent to correct VC.");
      assert(eci_rsp_size_o === 'd1) else
	$fatal(1,"LCA size should be 1.");
      ##1;
      // CL is in shared state in CPU.
      // CL should also be locked for requests.
      ##10;
      test_unlock_locked_rs_shared(
				   .my_id_i(my_hreq_id),
				   .my_dmask_i(my_dmask),
				   .my_addr_i(my_addr.flat)
				   );
      // CL should be exclusive here, bring to invalid.
      ##10;
      send_v31(
	       .my_addr_i(my_addr.flat),
	       .my_dmask_i(my_dmask),
	       .my_data_i('0)
	       );
      assert(dcuTb.dcu1.cc_rom_inst.next_state_o == s1__1) else 
	$fatal(1, "Address is not invalid but should be.");
      $display("PASS: test_lc_lca_ul\n");
   endtask //test_lc_lca_ul
   
   task static test_lc_lca_v21_ul();
      $display("test_lc_lca_v21_ul in progress.");
      // bring CL to state 2, is already clean.
      my_addr_rtg_casted = '0;
      my_addr_rtg_casted.set_within_dcu = $urandom_range(0, 8092);
      my_addr_rtg_casted.dcu_id = 'd0; // DCU ID should be always 0.
      my_addr_rtg_casted.tag = $urandom_range(100, 5000);
      my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
      my_hreq_id = $urandom_range(0,63);
      my_dmask = '1;
      test_r12_ra2(
		   .my_id_i(my_hreq_id),
		   .my_dmask_i(my_dmask),
		   .my_addr_i(my_addr.flat)
		   );
      // CL is in state 2.
      // send LC.
      my_ns = 1'b1; // NS hardcoded to 1 in DCU. 
      send_lc(
	       .my_id_i(eci_hreqid_t'(my_hreq_id)),
	       .my_dmask_i(my_dmask),
	       .my_ns_i(my_ns),
	       .my_addr_i(my_addr.flat)
	       );
      // Store LC header. 
      lc_lci_store_header = eci_req_hdr_i;
      // no need to wait as when lci handshake
      // happens lcia (rsp_valid) handshake also
      // happens. 
      //wait(eci_rsp_valid_o & eci_rsp_ready_i);
      // check if LCA is received.
      exp_resp_header = eci_cmd_defs::lcl_gen_lca(.req_i(lc_lci_store_header));
      act_resp_header = eci_word_t'(eci_rsp_pkt_o[0]);
      if(act_resp_header.eci_word !== exp_resp_header.eci_word) begin
	 $error("Error: LCA does not match: Expected %0h, Actual %0h",
		exp_resp_header, act_resp_header);
	 $finish;
      end
      assert((eci_rsp_vc_o === VC_LCL_RESP_WO_DATA_E) |
	     (eci_rsp_vc_o === VC_LCL_RESP_WO_DATA_O))else
	$fatal(1,"LCA not sent to correct VC.");
      assert(eci_rsp_size_o === 'd1) else
	$fatal(1,"LCA size should be 1.");
      ##1;
      // CL is in shared state in CPU.
      // CL should be locked here but only for requests.
      // send_v21 to bring state to invalid and will not be stalled.
      ##5;
      send_v21(
	       .my_addr_i(my_addr.flat),
	       .my_dmask_i(my_dmask)
	       );     
      // CL should be invalid state in CPU
      // but locked by the FPGA.
      ##10;
      test_unlock_locked_rs_inv(
			 .my_id_i(my_hreq_id),
			 .my_dmask_i(my_dmask),
			 .my_addr_i(my_addr.flat)
			 );
      $display("PASS: test_lc_lca_v21_ul\n");
   endtask //test_lc_lca_v21_ul

   task static test_lc_f32_a32d_lca_ul();
      $display("test_lc_f32_a32d_lca_ul in progress.");
      my_addr_rtg_casted = '0;
      my_addr_rtg_casted.set_within_dcu = $urandom_range(0, 8092);
      my_addr_rtg_casted.dcu_id = 'd0; // DCU ID should be always 0.
      my_addr_rtg_casted.tag = $urandom_range(100, 5000);
      my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
      my_hreq_id = $urandom_range(0,63);
      my_dmask = '1;
      test_r13_ra3(
		   .my_id_i(my_hreq_id),
		   .my_dmask_i(my_dmask),
		   .my_addr_i(my_addr.flat)
		   );
      // CL is in state 3
      // issue lc.
      my_ns = 1'b1; // NS hardcoded to 1 in DCU. 
      send_lc(
	       .my_id_i(eci_hreqid_t'(my_hreq_id)),
	       .my_dmask_i(my_dmask),
	       .my_ns_i(my_ns),
	       .my_addr_i(my_addr.flat)
	       );
      // Store LC header. 
      lc_lci_store_header = eci_req_hdr_i;
      // no need to wait as when lci handshake
      // happens lcia (rsp_valid) handshake also
      // happens. 
      //wait(eci_rsp_valid_o & eci_rsp_ready_i);
      //
      // check if F32 is issued.
      //  HREQ ID should be the DCU ID which is 0s in this case.
      exp_resp_header = eci_cmd_defs::eci_gen_fldrs_eh
			(
			 .hreq_id_i('0),
			 .dmask_i(my_dmask),
			 .ns_i(my_ns),
			 .addr_i(my_addr.flat)
			 );
      act_resp_header = eci_word_t'(eci_rsp_pkt_o[0]);
      if(act_resp_header.eci_word !== exp_resp_header.eci_word) begin
	 $error("Error: F32 is not issued.");
	 $finish;
      end
      assert((eci_rsp_vc_o === VC_FWD_WO_DATA_E) |
	     (eci_rsp_vc_o === VC_FWD_WO_DATA_O))else
	$fatal(1,"F32 not sent to correct VC.");
      assert(eci_rsp_size_o === 'd1) else
	$fatal(1,"F32 size should be 1.");
      ##1;
      ##10;
      // issue A32d
      send_A32d(
		.fwd_req_i(act_resp_header),
		.my_data_i('0)
		);
      // A32d
      wait(eci_rsp_valid_o & eci_rsp_ready_i);
      ##1;
      // check if LCA is received.
      exp_resp_header = eci_cmd_defs::lcl_gen_lca(.req_i(lc_lci_store_header));
      act_resp_header = eci_word_t'(eci_rsp_pkt_o[0]);
      if(act_resp_header.eci_word !== exp_resp_header.eci_word) begin
	 $error("Error: LCA does not match: Expected %0h, Actual %0h",
		exp_resp_header, act_resp_header);
	 $finish;
      end
      assert((eci_rsp_vc_o === VC_LCL_RESP_WO_DATA_E) |
	     (eci_rsp_vc_o === VC_LCL_RESP_WO_DATA_O))else
	$fatal(1,"LCA not sent to correct VC.");
      assert(eci_rsp_size_o === 'd1) else
	$fatal(1,"LCA size should be 1.");
      ##1;
      // CL is in shared and locked.
      // unlock it upgrade to exclusive.
      ##10;
      test_unlock_locked_rs_shared(
				   .my_id_i(my_hreq_id),
				   .my_dmask_i(my_dmask),
				   .my_addr_i(my_addr.flat)
				   );
      // CL should be exclusive here, bring to invalid.
      ##10;
      send_v31(
	       .my_addr_i(my_addr.flat),
	       .my_dmask_i(my_dmask),
	       .my_data_i('0)
	       );
      assert(dcuTb.dcu1.cc_rom_inst.next_state_o == s1__1) else 
	$fatal(1, "Address is not invalid but should be.");
      $display("PASS: test_lc_f32_a32d_lca_ul\n");
   endtask //test_lc_f32_a32d_lca_ul
   
   task static test_lci_f21_a11_lcia_v21_ul();
      $display("test_lci_f21_a11_lcia_v21_ul in progress");
      my_addr_rtg_casted = '0;
      my_addr_rtg_casted.set_within_dcu = $urandom_range(0, 8092);
      my_addr_rtg_casted.dcu_id = 'd0; // DCU ID should be always 0.
      my_addr_rtg_casted.tag = $urandom_range(100, 5000);
      my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
      my_hreq_id = $urandom_range(0,63);
      my_dmask = '1;
      test_r12_ra2(
		   .my_id_i(my_hreq_id),
		   .my_dmask_i(my_dmask),
		   .my_addr_i(my_addr.flat)
		   );
      // CL is in state 2.
      // issue lci.
      my_ns = 1'b1; // NS hardcoded to 1 in DCU. 
      send_lci(
	       .my_id_i(eci_hreqid_t'(my_hreq_id)),
	       .my_dmask_i(my_dmask),
	       .my_ns_i(my_ns),
	       .my_addr_i(my_addr.flat)
	       );
      // Store LCI header. 
      lc_lci_store_header = eci_req_hdr_i;
      // no need to wait as when lci handshake
      // happens lcia (rsp_valid) handshake also
      // happens. 
      //wait(eci_rsp_valid_o & eci_rsp_ready_i);
      //
      // Check if F21 is issued.
      //  HREQ ID should be the DCU ID which is 0s in this case.
      exp_resp_header = eci_cmd_defs::eci_gen_sinv_h
			(
			 .hreq_id_i('0),
			 .dmask_i(my_dmask),
			 .ns_i(my_ns),
			 .addr_i(my_addr.flat)
			 );
      act_resp_header = eci_word_t'(eci_rsp_pkt_o[0]);
      if(act_resp_header.eci_word !== exp_resp_header.eci_word) begin
	 $error("Error: F21 is not issued.");
	 $finish;
      end
      assert((eci_rsp_vc_o === VC_FWD_WO_DATA_E) |
	     (eci_rsp_vc_o === VC_FWD_WO_DATA_O))else
	$fatal(1,"F21 not sent to correct VC.");
      assert(eci_rsp_size_o === 'd1) else
	$fatal(1,"F21 size should be 1.");
      ##1;
      ##10;
      // Issue A11
      send_A11(
	       .fwd_req_i(act_resp_header)
	       );
      // check if LCIA is received.
      exp_resp_header = eci_cmd_defs::lcl_gen_lcia(.req_i(lc_lci_store_header));
      act_resp_header = eci_word_t'(eci_rsp_pkt_o[0]);
      if(act_resp_header.eci_word !== exp_resp_header.eci_word) begin
	 $error("Error: LCIA does not match: Expected %0h, Actual %0h",
		exp_resp_header, act_resp_header);
	 $finish;
      end
      assert((eci_rsp_vc_o === VC_LCL_RESP_WO_DATA_E) |
	     (eci_rsp_vc_o === VC_LCL_RESP_WO_DATA_O))else
	$fatal(1,"LCIA not sent to correct VC.");
      assert(eci_rsp_size_o === 'd1) else
	$fatal(1,"LCIA size should be 1.");
      ##1;
      ##10;
      // send V21 to bring CL to invalid.
      send_v21(
	       .my_addr_i(my_addr.flat),
	       .my_dmask_i(my_dmask)
	       );
      assert(dcuTb.dcu1.cc_rom_inst.next_state_o == s1pUL__1) else 
	$fatal(1, "Final state is not s1pUL__1.");
      // CL should be locked here.
      ##10;
      test_unlock_locked_rs_inv(
			 .my_id_i(my_hreq_id),
			 .my_dmask_i(my_dmask),
			 .my_addr_i(my_addr.flat)
			 );
      assert(dcuTb.dcu1.cc_rom_inst.next_state_o == s1__1) else 
	$fatal(1, "Address is not invalid but should be.");
      $display("PASS: test_lci_f21_a11_lcia_v21_ul\n");
   endtask //test_lci_f21_a11_lcia_v21_ul
   
   task static test_lci_f21_a21_lcia_ul();
      $display("test_lci_f21_a21_lcia_ul in progress");
      my_addr_rtg_casted = '0;
      my_addr_rtg_casted.set_within_dcu = $urandom_range(0, 8092);
      my_addr_rtg_casted.dcu_id = 'd0; // DCU ID should be always 0.
      my_addr_rtg_casted.tag = $urandom_range(100, 5000);
      my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
      my_hreq_id = $urandom_range(0,63);
      my_dmask = '1;
      test_r12_ra2(
		   .my_id_i(my_hreq_id),
		   .my_dmask_i(my_dmask),
		   .my_addr_i(my_addr.flat)
		   );
      // CL is in state 2.
      // issue lci.
      my_ns = 1'b1; // NS hardcoded to 1 in DCU. 
      send_lci(
	       .my_id_i(eci_hreqid_t'(my_hreq_id)),
	       .my_dmask_i(my_dmask),
	       .my_ns_i(my_ns),
	       .my_addr_i(my_addr.flat)
	       );
      // Store LCI header. 
      lc_lci_store_header = eci_req_hdr_i;
      // no need to wait as when lci handshake
      // happens lcia (rsp_valid) handshake also
      // happens. 
      //wait(eci_rsp_valid_o & eci_rsp_ready_i);
      //
      // Check if F21 is issued.
      //  HREQ ID should be the DCU ID which is 0s in this case.
      exp_resp_header = eci_cmd_defs::eci_gen_sinv_h
			(
			 .hreq_id_i('0),
			 .dmask_i(my_dmask),
			 .ns_i(my_ns),
			 .addr_i(my_addr.flat)
			 );
      act_resp_header = eci_word_t'(eci_rsp_pkt_o[0]);
      if(act_resp_header.eci_word !== exp_resp_header.eci_word) begin
	 $error("Error: F21 is not issued.");
	 $finish;
      end
      assert((eci_rsp_vc_o === VC_FWD_WO_DATA_E) |
	     (eci_rsp_vc_o === VC_FWD_WO_DATA_O))else
	$fatal(1,"F21 not sent to correct VC.");
      assert(eci_rsp_size_o === 'd1) else
	$fatal(1,"F21 size should be 1.");
      ##1;
      ##10;
      // Issue A21
      send_A21(
	       .fwd_req_i(act_resp_header)
	       );
      // check if LCIA is received.
      exp_resp_header = eci_cmd_defs::lcl_gen_lcia(.req_i(lc_lci_store_header));
      act_resp_header = eci_word_t'(eci_rsp_pkt_o[0]);
      if(act_resp_header.eci_word !== exp_resp_header.eci_word) begin
	 $error("Error: LCIA does not match: Expected %0h, Actual %0h",
		exp_resp_header, act_resp_header);
	 $finish;
      end
      assert((eci_rsp_vc_o === VC_LCL_RESP_WO_DATA_E) |
	     (eci_rsp_vc_o === VC_LCL_RESP_WO_DATA_O))else
	$fatal(1,"LCIA not sent to correct VC.");
      assert(eci_rsp_size_o === 'd1) else
	$fatal(1,"LCIA size should be 1.");
      ##1;
      assert(dcuTb.dcu1.cc_rom_inst.next_state_o == s1pUL__1) else 
	$fatal(1, "Final state is not s1pUL__1.");
      // CL should be locked here.
      ##10;
      test_unlock_locked_rs_inv(
			 .my_id_i(my_hreq_id),
			 .my_dmask_i(my_dmask),
			 .my_addr_i(my_addr.flat)
			 );
      assert(dcuTb.dcu1.cc_rom_inst.next_state_o == s1__1) else 
	$fatal(1, "Address is not invalid but should be.");
      $display("PASS: test_lci_f21_a21_lcia_ul\n");
   endtask //test_lci_f21_a21_lcia_ul
   
   task static test_lci_f31_v32d_a11_v21_ul();
      $display("test_lci_f31_v32d_a11_v21_ul in progress");
      my_addr_rtg_casted = '0;
      my_addr_rtg_casted.set_within_dcu = $urandom_range(0, 8092);
      my_addr_rtg_casted.dcu_id = 'd0; // DCU ID should be always 0.
      my_addr_rtg_casted.tag = $urandom_range(100, 5000);
      my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
      my_hreq_id = $urandom_range(0,63);
      my_dmask = '1;
      test_r13_ra3(
		   .my_id_i(my_hreq_id),
		   .my_dmask_i(my_dmask),
		   .my_addr_i(my_addr.flat)
		   );
      // CL is in state 3.
      // issue lci.
      my_ns = 1'b1; // NS hardcoded to 1 in DCU. 
      send_lci(
	       .my_id_i(eci_hreqid_t'(my_hreq_id)),
	       .my_dmask_i(my_dmask),
	       .my_ns_i(my_ns),
	       .my_addr_i(my_addr.flat)
	       );
      // Store LCI header. 
      lc_lci_store_header = eci_req_hdr_i;
      // no need to wait as when lci handshake
      // happens lcia (rsp_valid) handshake also
      // happens. 
      //wait(eci_rsp_valid_o & eci_rsp_ready_i);
      //
      // Check if F31 is issued.
      //  HREQ ID should be the DCU ID which is 0s in this case.
      exp_resp_header = eci_cmd_defs::eci_gen_fevx_eh
			    (
			     .hreq_id_i('0),
			     .dmask_i(my_dmask),
			     .ns_i(my_ns),
			     .addr_i(my_addr.flat)
			     );
      act_resp_header = eci_word_t'(eci_rsp_pkt_o[0]);
      if(act_resp_header.eci_word !== exp_resp_header.eci_word) begin
	 $error("Error: F31 doesnt match: Expected: %0h, Actual: %0h",
		exp_resp_header, act_resp_header);
	 $finish;
      end
      assert((eci_rsp_vc_o === VC_FWD_WO_DATA_E) |
	     (eci_rsp_vc_o === VC_FWD_WO_DATA_O))else
	$fatal(1,"F31 not sent to correct VC.");
      assert(eci_rsp_size_o === 'd1) else
	$fatal(1,"F31 size should be 1.");
      ##1;
      // F31 is issued by DCU.
      // Issue A31d as response after some
      // cycles (mimicing ECI latency).
      ##10;
      // Issue V32d 
      send_v32d(
		.my_addr_i(my_addr.flat),
		.my_dmask_i(my_dmask),
		.my_data_i(my_data)
		);
      // Issue A11
      send_A11(
	       .fwd_req_i(act_resp_header)
	       );
      //wait(eci_rsp_valid_o & eci_rsp_ready_i);
      //##1;
      // Check if LCIA is received.
      exp_resp_header = eci_cmd_defs::lcl_gen_lcia(.req_i(lc_lci_store_header));
      act_resp_header = eci_word_t'(eci_rsp_pkt_o[0]);
      if(act_resp_header.eci_word !== exp_resp_header.eci_word) begin
	 $error("Error: LCIA does not match: Expected %0h, Actual %0h",
		exp_resp_header, act_resp_header);
	 $finish;
      end
      assert((eci_rsp_vc_o === VC_LCL_RESP_WO_DATA_E) |
	     (eci_rsp_vc_o === VC_LCL_RESP_WO_DATA_O))else
	$fatal(1,"LCIA not sent to correct VC.");
      assert(eci_rsp_size_o === 'd1) else
	$fatal(1,"LCIA size should be 1.");
      ##1;
      // Send V21 to finish the transaction.
      send_v21(
	       .my_addr_i(my_addr.flat),
	       .my_dmask_i(my_dmask)
	       );
      // This address should be locked.
      test_unlock_locked_rs_inv(
			 .my_id_i(my_hreq_id),
			 .my_dmask_i(my_dmask),
			 .my_addr_i(my_addr.flat)
			 );
      assert(dcuTb.dcu1.cc_rom_inst.next_state_o == s1__1) else 
	$fatal(1, "Address is not invalid but should be.");
      $display("PASS: test_lci_f31_v32d_a11_v21_ul\n");
   endtask //test_lci_f31_v32d_a11_v21_ul
   
   task static test_lci_f31_a31d_lcia_ul();
      $display("test_lci_f31_a31d_lcia_ul in progress");
      my_addr_rtg_casted = '0;
      my_addr_rtg_casted.set_within_dcu = $urandom_range(0, 8092);
      my_addr_rtg_casted.dcu_id = 'd0; // DCU ID should be always 0.
      my_addr_rtg_casted.tag = $urandom_range(100, 5000);
      my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
      my_hreq_id = $urandom_range(0,63);
      my_dmask = '1;
      test_r13_ra3(
		   .my_id_i(my_hreq_id),
		   .my_dmask_i(my_dmask),
		   .my_addr_i(my_addr.flat)
		   );
      // CL is in state 3.
      // issue lci.
      my_ns = 1'b1; // NS hardcoded to 1 in DCU. 
      send_lci(
	       .my_id_i(eci_hreqid_t'(my_hreq_id)),
	       .my_dmask_i(my_dmask),
	       .my_ns_i(my_ns),
	       .my_addr_i(my_addr.flat)
	       );
      // Store LCI header. 
      lc_lci_store_header = eci_req_hdr_i;
      // no need to wait as when lci handshake
      // happens lcia (rsp_valid) handshake also
      // happens. 
      //wait(eci_rsp_valid_o & eci_rsp_ready_i);
      //
      // Check if F31 is issued.
      //  HREQ ID should be the DCU ID which is 0s in this case.
      // 
      exp_resp_header = eci_cmd_defs::eci_gen_fevx_eh
			    (
			     .hreq_id_i('0),
			     .dmask_i(my_dmask),
			     .ns_i(my_ns),
			     .addr_i(my_addr.flat)
			     );
      act_resp_header = eci_word_t'(eci_rsp_pkt_o[0]);
      if(act_resp_header.eci_word !== exp_resp_header.eci_word) begin
	 $error("Error: F31 is not issued.");
	 $finish;
      end
      assert((eci_rsp_vc_o === VC_FWD_WO_DATA_E) |
	     (eci_rsp_vc_o === VC_FWD_WO_DATA_O))else
	$fatal(1,"F31 not sent to correct VC.");
      assert(eci_rsp_size_o === 'd1) else
	$fatal(1,"F31 size should be 1.");
      ##1;
      // F31 is issued by DCU.
      // Issue A31d as response after some
      // cycles (mimicing ECI latency).
      ##10;
      // halting accepting LCIA response temporarily.
      // send_a31d has a delay enough to consume the
      // handshake in eci_rsp channel. 
      send_A31d(
		.fwd_req_i(act_resp_header),
		.my_data_i('0)
		);
      ##1;
      wait(eci_rsp_valid_o & eci_rsp_ready_i);
      //##1;
      // Check if LCIA is received.
      exp_resp_header = eci_cmd_defs::lcl_gen_lcia(.req_i(lc_lci_store_header));
      act_resp_header = eci_word_t'(eci_rsp_pkt_o[0]);
      if(act_resp_header.eci_word !== exp_resp_header.eci_word) begin
	 $error("Error: LCIA does not match: Expected %0h, Actual %0h",
		exp_resp_header, act_resp_header);
	 $finish;
      end
      assert((eci_rsp_vc_o === VC_LCL_RESP_WO_DATA_E) |
	     (eci_rsp_vc_o === VC_LCL_RESP_WO_DATA_O))else
	$fatal(1,"LCIA not sent to correct VC.");
      assert(eci_rsp_size_o === 'd1) else
	$fatal(1,"LCIA size should be 1.");
      ##1;

      // CL should be locked here.
      ##10;
      test_unlock_locked_rs_inv(
			 .my_id_i(my_hreq_id),
			 .my_dmask_i(my_dmask),
			 .my_addr_i(my_addr.flat)
			 );

      $display("PASS: test_lci_f31_a31d_lcia_ul\n");
   endtask //test_lci_f31_a31d_lcia_ul
   
   task static test_lci_lcia_ul();
      //CL should be in invalid state to start with.
      $display("test_lci_lcia_ul in progress");
      my_hreq_id = $urandom_range(0,63);
      my_dmask = '1;
      my_ns = 1'b1;
      my_addr_rtg_casted = '0;
      my_addr_rtg_casted.set_within_dcu = $urandom_range(0,8091);;
      my_addr_rtg_casted.dcu_id = 'd0; // should always be 0.
      my_addr_rtg_casted.tag = $urandom_range(100, 5000);
      my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
      send_lci(
	       .my_id_i(eci_hreqid_t'(my_hreq_id)),
	       .my_dmask_i(my_dmask),
	       .my_ns_i(my_ns),
	       .my_addr_i(my_addr.flat)
	       );
      // no need to wait as when lci handshake
      // happens lcia (rsp_valid) handshake also
      // happens. 
      //wait(eci_rsp_valid_o & eci_rsp_ready_i);
      // Receive LCIA
      exp_resp_header = eci_cmd_defs::lcl_gen_lcia(.req_i(eci_req_pkt_i[0]));
      act_resp_header = eci_word_t'(eci_rsp_pkt_o[0]);
      if(act_resp_header.eci_word !== exp_resp_header.eci_word) begin
	 $error("Error: The response LCIA does not match expected LCIA");
	 $finish;
      end
      assert((eci_rsp_vc_o === VC_LCL_RESP_WO_DATA_E) |
	     (eci_rsp_vc_o === VC_LCL_RESP_WO_DATA_O))else
	$fatal(1,"LCIA not sent to correct VC.");
      assert(eci_rsp_size_o === 'd1) else
	$fatal(1,"LCIA size should be 1.");
      ##1; // wait for output valid signal to go low.
      assert(dcuTb.dcu1.cc_rom_inst.next_state_o == s1pUL__1) else 
	$fatal(1, "test_lci_lcia_ul expected state does not match actual");
      // CL should be locked here.
      ##10;
      test_unlock_locked_rs_inv(
			 .my_id_i(my_hreq_id),
			 .my_dmask_i(my_dmask),
			 .my_addr_i(my_addr.flat)
			 );
      assert(dcuTb.dcu1.cc_rom_inst.next_state_o == s1__1) else 
	$fatal(1, "Address is not invalid but should be.");
      $display("PASS: test_lci_lcia_ul\n");
   endtask //test_lci_lcia_ul

   task static test_unlock_locked_rs_inv(
				input eci_hreqid_t my_id_i,
				input eci_dmask_t my_dmask_i,
				input eci_cl_addr_t my_addr_i
				);
      $display("Test: test_unlock_locked_rs_inv");
      // CL should be locked here.
      // CL should be invalid in remote node.
      // send RR, it should be stalled.
      $display("Test: Stall due to locked");
      send_req_wo_data(
		       // casting down from 6 to 5 bits 
		       .my_id_i(eci_id_t'(my_id_i)),
		       .my_dmask_i(my_dmask_i),
		       .my_addr_i(my_addr_i),
		       .rldt_i(1'b0),
		       .rldi_i(1'b1),
		       .rldd_i(1'b0),
		       .rc2d_s_i(1'b0),
		       .rldx_i(1'b0)
		       );
      assert(dcuTb.dcu1.cc_rom_inst.next_action_o == STALL) else 
	$fatal(1, "Address is locked but incoming req_wod is not stalled.");
      $display("Pass: Stall due to locked");
      ##5;
      // unlock address. 
      send_ul(
	      .my_addr_i(my_addr_i)
	      );
      // now read should proceed.
      test_rr_rra(
		  .my_id_i(my_id_i),
		  .my_dmask_i(my_dmask_i),
		  .my_addr_i(my_addr_i),
		  .my_data_i('0)
		  );            
      $display("Pass: test_unlock_locked_rs_inv");
   endtask //test_unlock_locked_rs_inv

   task static test_unlock_locked_rs_shared(
				input eci_hreqid_t my_id_i,
				input eci_dmask_t my_dmask_i,
				input eci_cl_addr_t my_addr_i
				);
      $display("Test: test_unlock_locked_rs_shared");
      // CL should be locked here.
      // CL should be shared in remote node.
      // send RR, it should be stalled.
      $display("Test: Stall due to locked");
      // Issuing R23 (which should be blocked).
      send_req_wo_data(
		       // casting down from 6 to 5 bits 
		       .my_id_i(eci_id_t'(my_id_i)),
		       .my_dmask_i(my_dmask_i),
		       .my_addr_i(my_addr_i),
		       .rldt_i(1'b0),
		       .rldi_i(1'b0),
		       .rldd_i(1'b0),
		       .rc2d_s_i(1'b1),
		       .rldx_i(1'b0)
		       );
      assert(dcuTb.dcu1.cc_rom_inst.next_action_o == STALL) else 
	$fatal(1, "Address is locked but incoming req_wod is not stalled.");
      $display("Pass: Stall due to locked");
      ##5;
      // unlock address. 
      send_ul(
	      .my_addr_i(my_addr_i)
	      );
      // now read should proceed.
      test_r23_ra3(
		  .my_id_i(my_id_i),
		  .my_dmask_i(my_dmask_i),
		  .my_addr_i(my_addr_i)
		  );            
      $display("Pass: test_unlock_locked_rs_shared");
   endtask //test_unlock_locked_rs_shared
   
   task static test_nxm();
      // Tag should be all 1s.
      // any request to this address will cause nxm error.
      $display("test_nxm in progress");
      my_hreq_id = $urandom_range(0,63);
      my_dmask = '1;
      my_addr_rtg_casted = '0;
      my_addr_rtg_casted.set_within_dcu = $urandom_range(0,8091);
      my_addr_rtg_casted.dcu_id = 'd0; // should always be 0.
      my_addr_rtg_casted.tag = '1; // tag is all 1s, causing nxm.
      my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
      $display("Sending R13 for invalid tag causes NXM error.");
      send_req_wo_data(
		       // casting down from 6 to 5 bits 
		       .my_id_i(eci_id_t'(my_hreq_id)),
		       .my_dmask_i(my_dmask),
		       .my_addr_i(my_addr.flat),
		       .rldt_i(1'b0),
		       .rldi_i(1'b0),
		       .rldd_i(1'b0),
		       .rc2d_s_i(1'b0),
		       .rldx_i(1'b1)
		       );
      assert(dbg_err_code_o === ERR_TSU_NXM) else 
	$fatal(1,"Error ERR_TSU_NXM should be asserted");
      $display("Pass: test_nxm\n");
   endtask //test_nxm
   
   task static test_r12_ra2_r23_ra3_v31();
      $display("test_r12_ra2_r23_ra3_v31 in progress");
      // CL is in state 1
      // R12 pushes CL to state 2, RA2 response expected
      // V21 pushes CL back to state 1, no response is needed
      my_addr_rtg_casted = '0;
      my_addr_rtg_casted.set_within_dcu = 1; // Set is always 1.
      my_addr_rtg_casted.dcu_id = 'd0;
      my_addr_rtg_casted.tag = $urandom_range(100, 5000);
      my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
      my_data.scls[0] = 1;
      my_data.scls[1] = 2;
      my_data.scls[2] = 3;
      my_data.scls[3] = 4;
      my_hreq_id = eci_hreqid_t'('d6);
      my_dmask = '1;
      test_r12_ra2(
		   .my_id_i(my_hreq_id),
		   .my_dmask_i(my_dmask),
		   .my_addr_i(my_addr)
		   );
      ##5;
      
      // to clear out the transaction tables state.
      // issue r12 on a different CL. 
      test_r12_ra2(
		   .my_id_i('0),
		   .my_dmask_i('1),
		   .my_addr_i('0)
		   );
      ##5;
      send_v21(
	       .my_addr_i('0),
	       .my_dmask_i('1)
	       );
      ##5;
      
      // CL is in state 2
      // Issue R23 (RC2D_S) to bring it to state 3.
      // address is the same
      test_r23_ra3(
		   .my_id_i(my_hreq_id),
		   .my_dmask_i(my_dmask),
		   .my_addr_i(my_addr)
		   );
      ##5;
      // V31 to bring CL back to invalid.
      send_v31(
	       .my_addr_i(my_addr.flat),
	       .my_dmask_i(my_dmask),
	       .my_data_i('0)
	       );
      assert(dcuTb.dcu1.cc_rom_inst.next_state_o == s1__1) else 
	$fatal(1, "test_r12_ra2_r23_ra3_v31 failed.");

      $display("Pass: test_r12_ra2_r23_ra3_v31\n");
   endtask //test_r12_ra2_r23_ra3_v31

   task static test_protocol_stall();
      // Get a CL in state s1__2 and before downgrade issue 
      // an R12 for same addr and check stall.
      $display("Test: test_protocol_stall");
      my_hreq_id = 'd5;
      my_dmask = '1;
      my_addr_rtg_casted = '0;
      my_addr_rtg_casted.set_within_dcu = $urandom_range(0, 8092);
      my_addr_rtg_casted.dcu_id = 'd0; // DCU ID should be always 0.
      my_addr_rtg_casted.tag = $urandom_range(100, 5000);
      my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
      test_r12_ra2(
		   .my_id_i(my_hreq_id),
		   .my_dmask_i(my_dmask),
		   .my_addr_i(my_addr)
		   );
      send_req_wo_data(
		       // casting down from 6 to 5 bits 
		       .my_id_i(eci_id_t'(my_hreq_id)),
		       .my_dmask_i(my_dmask),
		       .my_addr_i(my_addr),
		       .rldt_i(1'b0),
		       .rldi_i(1'b0),
		       .rldd_i(1'b1),
		       .rc2d_s_i(1'b0),
		       .rldx_i(1'b0)
		       );
      assert(dcuTb.dcu1.cc_rom_inst.next_action_o == STALL) else 
	$fatal(1, "test_protocol_stall failed");
      $display("Pass: test_protocol_stall\n");
   endtask 

   task static test_r13_reset_r13_v31();
      $display("test_r13_reset_r13_v31 in progress");
      // CL is in state 1
      // R13 pushes CL to state 3, RA3 response expected
      // reset is asserted before bringing the CL to state 1
      //  reset should clear all contents of tag ram and so
      //  all cache lines should be effectively in state I
      // R13 is issued again and RA3 response is expected 
      my_hreq_id = 'd5;
      my_dmask = '1;
      my_addr_rtg_casted = '0;
      my_addr_rtg_casted.set_within_dcu = $urandom_range(0, 8092);
      my_addr_rtg_casted.dcu_id = 'd0; // DCU ID should be always 0.
      my_addr_rtg_casted.tag = $urandom_range(100, 5000);
      my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
      test_r13_ra3(
		   .my_id_i(my_hreq_id),
		   .my_dmask_i(my_dmask),
		   .my_addr_i(my_addr)
		   );
      // Assert reset
      reset_system();
      // R13 again should return RA3, since previous state have been reset.
      test_r13_ra3(
		   .my_id_i(my_hreq_id),
		   .my_dmask_i(my_dmask),
		   .my_addr_i(my_addr)
		   );
      // Sending V31 to bring this CL back to invalid.
      send_v31(
	       .my_addr_i(my_addr.flat),
	       .my_dmask_i(my_dmask),
	       .my_data_i('0)
	       );
      assert(dcuTb.dcu1.cc_rom_inst.next_state_o == s1__1) else 
	$fatal(1, "test_r13_ra3_v31 failed.");
      $display("test_r13_reset_r13_v31 passed");
   endtask //test_r13_reset_r13_v31

   task static test_stall_tsu_full();
      $display("Test: test_stall_tsu_full");
      // All 16 ways of set 1 are filled with
      // tags 1 to 16. 
      make_set1_full_r12();
      // issue r12 for set 1, tag 17 this will miss
      // in directory, since it needs a new slot in
      // RTG, it should stall the incoming request. 
      // There should be an induced eviction in progress.
      my_addr_rtg_casted = '0;
      my_addr_rtg_casted.set_within_dcu = 1; // Set is always 1.
      my_dmask = '1;
      my_addr_rtg_casted.tag = 17;
      my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
      my_hreq_id = eci_hreqid_t'(5); //random value.
      $display("Sending R12 cause TSU to get filled up");
      send_req_wo_data(
		       // casting down from 6 to 5 bits 
		       .my_id_i(eci_id_t'(my_hreq_id)),
		       .my_dmask_i(my_dmask),
		       .my_addr_i(my_addr.flat),
		       .rldt_i(1'b0),
		       .rldi_i(1'b0),
		       .rldd_i(1'b1),
		       .rc2d_s_i(1'b0),
		       .rldx_i(1'b0)
		       );
      assert(dbg_stall_rtg_full_o & dbg_eci_req_stalled_o & dbg_valid_o === 1'b1) else
	$fatal(1,"TSU full but request is not stalled");

      // send V21 on a previously upgraded CL
      // to create space in the RTG.
      // previously tags 1 to 16 were upgraded. 
      my_addr_rtg_casted = '0;
      my_addr_rtg_casted.set_within_dcu = 1; // Set is always 1.
      my_dmask = '1;
      my_addr_rtg_casted.tag = $urandom_range(1,16);
      my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
      my_hreq_id = eci_hreqid_t'(12); //random value.
      send_v21(
	       .my_addr_i(my_addr),
	       .my_dmask_i(my_dmask)
	       );

      // resend stalled request to occupy the
      // newly created spot.
      my_addr_rtg_casted = '0;
      my_addr_rtg_casted.set_within_dcu = 1; // Set is always 1.
      my_dmask = '1;
      my_addr_rtg_casted.tag = 17;
      my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
      my_hreq_id = eci_hreqid_t'(5); //random value.
      test_r12_ra2(
		   .my_id_i(my_hreq_id),
		   .my_dmask_i(my_dmask),
		   .my_addr_i(my_addr)
		   );
      $display("Pass: test_stall_tsu_full\n");
   endtask // test_err_tsu_full

   task static make_set1_full_r12();
      // Task to fill all 16 ways in set 1, all in shared state.
      // This is done by issuing R12 16 times for address with 
      // same set but different tag fields.
      // each R12 will fill 1 way in set 1.
      // Tags 1-16 occupy the ways and will hit if 
      // you use one of these tags.
      $display("make_set1_full_r12: Filling up all 16 ways of set 1 by issuing r12");
      my_addr_rtg_casted = '0;
      my_addr_rtg_casted.set_within_dcu = 1; // Set is always 1.
      my_dmask = '1;
      for( integer i=0; i<NUM_WAYS_PER_SET; i=i+1 ) begin
	 my_addr_rtg_casted.tag = i+1;
	 my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
	 my_hreq_id = eci_hreqid_t'(i+6); //random value.
	 test_r12_ra2(
		      .my_id_i(my_hreq_id),
		      .my_dmask_i(my_dmask),
		      .my_addr_i(my_addr)
		      );
      end 
      $display("make_set1_full_r12: 16 ways of set 1 is filled");
   endtask // make_set1_full_r12

   task static make_set1_empty_v21();
      // this is to be used in conjunction with make_set1_full_r12
      // v21 is sent for tags 1-16 in set 1.
      $display("make_set1_empty_v21: Emptying up all 16 ways of set 1 by issuing v21");
      my_addr_rtg_casted = '0;
      my_addr_rtg_casted.set_within_dcu = 1; // Set is always 1.
      my_dmask = '1;
      for( integer i=0; i<NUM_WAYS_PER_SET; i=i+1 ) begin
	 my_addr_rtg_casted.tag = i+1;
	 my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
	 my_hreq_id = eci_hreqid_t'(i+6); //random value.
	 send_v21(
		  .my_addr_i(my_addr),
		  .my_dmask_i(my_dmask)
		  );
      end 
      $display("make_set1_empty_v21: 16 ways of set 1 is emptied.");
   endtask //make_set1_empty_v21

   task static test_r13_ra3_v21_v32d_rr_rra();
      $display("Test: test_r13_ra3_v21_v32d_rr_rra");
      // CL is in state 1
      // R13 pushes CL to state 3, RA3 response expected
      my_addr_rtg_casted = '0;
      my_addr_rtg_casted.set_within_dcu = $urandom_range(0, 8092);
      my_addr_rtg_casted.dcu_id = 'd0; // DCU ID should be always 0.
      my_addr_rtg_casted.tag = $urandom_range(100, 5000);
      my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
      my_data.scls[0] = 5;
      my_data.scls[1] = 6;
      my_data.scls[2] = 7;
      my_data.scls[3] = 8;
      my_hreq_id = eci_hreqid_t'('d6);
      my_dmask = '1;
      test_r13_ra3(
		   .my_id_i(my_hreq_id),
		   .my_dmask_i(my_dmask),
		   .my_addr_i(my_addr.flat)
		   );
      // CL is in state 3
      // Issue V21 (VICS) to bring it down to intermediate state s1__1_V32
      // address is the same
      ##5;
      send_v21(
	       .my_addr_i(my_addr),
	       .my_dmask_i(my_dmask)
	       );
      // s1__1_V32
      if(dcuTb.dcu1.cc_rom_inst.next_state_o != s1__1_V32) begin
	 $fatal(1, "Expected state: s1__1_V32, actual state: %s", dcuTb.dcu1.cc_rom_inst.next_state_o);
      end
      // Issue V32d to bring it down to state s1__1
      send_v32d(
		.my_addr_i(my_addr.flat),
		.my_dmask_i(my_dmask),
		.my_data_i(my_data)
		);
      if(dcuTb.dcu1.cc_rom_inst.next_state_o != s1__1) begin
	 $fatal(1, "Expected state: s1__1, actual state: %s", dcuTb.dcu1.cc_rom_inst.next_state_o);
      end
      // now read to see data matches 
      test_rr_rra(
		  .my_id_i(my_hreq_id),
		  .my_dmask_i(my_dmask),
		  .my_addr_i(my_addr.flat),
		  .my_data_i(my_data)
		  );
      $display("Pass: test_r13_ra3_v21_v32d_rr_rra\n");
   endtask //test_r13_ra3_v21_v32d_rr_rra


   task static test_r13_ra3_v31();
      $display("Test: test_r13_ra3_v31");
      // CL is in state 1
      // R13 pushes CL to state 3, RA3 response expected
      // V31 pushes CL back to state 1, no response is needed
      my_addr_rtg_casted = '0;
      my_addr_rtg_casted.set_within_dcu = $urandom_range(0, 8092);
      my_addr_rtg_casted.dcu_id = 'd0; // DCU ID should be always 0.
      my_addr_rtg_casted.tag = $urandom_range(100, 5000);
      my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
      my_data.scls[0] = 1;
      my_data.scls[1] = 2;
      my_data.scls[2] = 3;
      my_data.scls[3] = 4;
      my_hreq_id = eci_hreqid_t'('d6);
      my_dmask = '1;
      test_r13_ra3(
		   .my_id_i(my_hreq_id),
		   .my_dmask_i(my_dmask),
		   .my_addr_i(my_addr.flat)
		   );
      // CL is in state 3
      // Issue V31 (VICD.N) to bring it down to state 1
      // address is the same
      ##5;
      send_v31(
	       .my_addr_i(my_addr.flat),
	       .my_dmask_i(my_dmask),
	       .my_data_i(my_data)
	       );
      assert(dcuTb.dcu1.cc_rom_inst.next_state_o === s1__1) else $fatal(1, "test_r13_ra3_v31 failed.");
      $display("Pass: test_r13_ra3_v31\n");
   endtask //test_r13_ra3_v31

   task static test_r13_ra3_v31d_rr_rra();
      $display("Test: test_r13_ra3_v31d_rr_rra");
      // CL is in state 1
      // R13 pushes CL to state 3, RA3 response expected
      // V31d writes new data into the CL and pushes CL to state 1
      // R12 reads the new data into the CL to check if data written matches
      // V21 brings it back to state 1
      my_hreq_id = 'd17;
      my_addr_rtg_casted = '0;
      my_addr_rtg_casted.set_within_dcu = $urandom_range(0, 8092);
      my_addr_rtg_casted.dcu_id = 'd0; // DCU ID should be always 0.
      my_addr_rtg_casted.tag = $urandom_range(100, 5000);
      my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
      my_data.scls[0] = 1;
      my_data.scls[1] = 2;
      my_data.scls[2] = 3;
      my_data.scls[3] = 4;
      my_hreq_id = eci_hreqid_t'('d6);
      my_dmask = '1;
      // CL is in state 3
      // Issue V31d (VICD) to bring it down to state 1
      // address is the same
      test_r13_ra3(
		   .my_id_i(my_hreq_id),
		   .my_dmask_i(my_dmask),
		   .my_addr_i(my_addr.flat)
		   );
      ##5;
      send_v31d(
		.my_addr_i(my_addr.flat),
		.my_dmask_i(my_dmask),
		.my_data_i(my_data)
		);
      assert(dcuTb.dcu1.cc_rom_inst.next_state_o === s1__1) else $fatal(1, "State not invalid after sending V31d\ntest_r13_ra3_v31d_rr_rra failed.");
      //$display("\texpected NS = s1__1, actual NS = %s", dcuTb.dcu1.cc_rom_inst.next_state_o.name());      
      test_rr_rra(
		  .my_id_i(my_hreq_id),
		  .my_dmask_i(my_dmask),
		  .my_addr_i(my_addr.flat),
		  .my_data_i(my_data)
		  );      
      $display("Test: test_r13_v31d_rr_rra passed\n");
   endtask //r13_ra3_v31d_rr_rra

   task static test_r12_ra2_v21();
      $display("Test: test_r12_ra2_v21 in progress.");
      // CL is in state 1
      // R12 pushes CL to state 2, RA2 response expected
      // V21 pushes CL back to state 1, no response is needed
      my_addr_rtg_casted = '0;
      my_addr_rtg_casted.set_within_dcu = $urandom_range(0, 8092);
      my_addr_rtg_casted.dcu_id = 'd0; // DCU ID should be always 0.
      my_addr_rtg_casted.tag = $urandom_range(100, 5000);
      my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
      my_data.scls[0] = 1;
      my_data.scls[1] = 2;
      my_data.scls[2] = 3;
      my_data.scls[3] = 4;
      my_hreq_id = eci_hreqid_t'('d6);
      my_dmask = '1;
      // CL is in state 3
      // Issue V21 (VICS) to bring it down to state 1
      // address is the same
      test_r12_ra2(
		   .my_id_i(my_hreq_id),
		   .my_dmask_i(my_dmask),
		   .my_addr_i(my_addr)
		   );
      ##5;
      send_v21(
	       .my_addr_i(my_addr),
	       .my_dmask_i(my_dmask)
	       );
      // if ths current state of the CCROM is not invalid then failed. 
      assert(dcuTb.dcu1.cc_rom_inst.next_state_o === s1__1) else $fatal(1, "test_r12_ra2_v21 failed.");
      //$display("\texpected NS = s1__1, actual NS = %s", dcuTb.dcu1.cc_rom_inst.next_state_o.name());
      $display("Pass: test_r12_ra2_v21\n");
   endtask // test_r12_ra2

   task static test_r23_ra3(
			    input eci_hreqid_t my_id_i,
			    input eci_dmask_t my_dmask_i,
			    input eci_cl_addr_t my_addr_i
			    );
      // Sends R23 and waits for RA3. When RA3 is recd, it is compared with expected.
      // RA3 should be without any data, R23 does not like PEMD, it should be PEMN.
      $display("Test: test_r23_ra3");
      send_req_wo_data(
		       // casting down from 6 to 5 bits 
		       .my_id_i(eci_id_t'(my_id_i)),
		       .my_dmask_i(my_dmask_i),
		       .my_addr_i(my_addr_i.flat),
		       .rldt_i(1'b0),
		       .rldi_i(1'b0),
		       .rldd_i(1'b0),
		       .rc2d_s_i(1'b1),
		       .rldx_i(1'b0)
		       );
      wait(eci_rsp_valid_o & eci_rsp_ready_i);
      // Receive PEMN (no data).
      exp_resp_header = eci_cmd_defs::eci_gen_pemd(.req_i(eci_req_pkt_i[0]), .pemn_i(1'b1));
      act_resp_header = eci_word_t'(eci_rsp_pkt_o[0]);
      if(act_resp_header.eci_word !== exp_resp_header.eci_word) begin
	 $error("Error: RC2D_S should respond with PEMN and that is not happeneing");
	 $finish;
      end
      if((eci_rsp_vc_o !== VC_RESP_WO_DATA_E) && (eci_rsp_vc_o !== VC_RESP_WO_DATA_O)) begin
	 $error("Error: PEMN should go in response without data VC %d. (instance %m)", eci_rsp_vc_o);
	 $finish;
      end
      ##1; // wait for output valid signal to go low.
      //$display("\tActual response header matches expected response header");
      $display("Pass: test_r23_ra3");
   endtask //test_r23_ra3
   
   task static test_r13_ra3(
			    input eci_hreqid_t my_id_i,
			    input eci_dmask_t my_dmask_i,
			    input eci_cl_addr_t my_addr_i
			    );
      $display("Test: test_r13_ra3");
      // Sends R13 and waits for RA3. When RA3 is recd, it is compared with expected.
      send_req_wo_data(
		       // casting down from 6 to 5 bits 
		       .my_id_i(eci_id_t'(my_id_i)),
		       .my_dmask_i(my_dmask_i),
		       .my_addr_i(my_addr_i.flat),
		       .rldt_i(1'b0),
		       .rldi_i(1'b0),
		       .rldd_i(1'b0),
		       .rc2d_s_i(1'b0),
		       .rldx_i(1'b1)
		       );
      wait(eci_rsp_valid_o & eci_rsp_ready_i);
      // Receive PEMD with data
      exp_resp_header = eci_cmd_defs::eci_gen_pemd(.req_i(eci_req_pkt_i[0]), .pemn_i(1'b0));
      act_resp_header = eci_word_t'(eci_rsp_pkt_o[0]);
      if(act_resp_header.eci_word !== exp_resp_header.eci_word) begin
	 $error("Error: RLDD should respond with PEMD and that is not happeneing");
	 $finish;
      end
      ##1; // wait for output valid signal to go low.
      //$display("\tExpected response header matches actual header");
      $display("Pass: test_r13_ra3");
   endtask
   
   task static test_r12_ra2(
			    input eci_hreqid_t my_id_i,
			    input eci_dmask_t my_dmask_i,
			    input eci_cl_addr_t my_addr_i
			    );
      // Sends R12 and waits for RA2. When RA2 is recd, it is compared with expected.
      $display("Test: test_r12_ra2");
      send_req_wo_data(
		       // casting down from 6 to 5 bits 
		       .my_id_i(eci_id_t'(my_id_i)),
		       .my_dmask_i(my_dmask_i),
		       .my_addr_i(my_addr_i.flat),
		       .rldt_i(1'b0),
		       .rldi_i(1'b0),
		       .rldd_i(1'b1),
		       .rc2d_s_i(1'b0),
		       .rldx_i(1'b0)
		       );
      wait(eci_rsp_valid_o & eci_rsp_ready_i);
      // Receive PSHA with data
      exp_resp_header = eci_cmd_defs::eci_gen_psha(.req_i(eci_req_pkt_i[0]));
      act_resp_header = eci_word_t'(eci_rsp_pkt_o[0]);
      if(act_resp_header.eci_word !== exp_resp_header.eci_word) begin
	 $error("Error: RLDD should respond with PEMD and that is not happeneing");
	 $finish;
      end
      ##1; // wait for output valid signal to go low.
      //$display("\tActual response header matches expected response header");
      $display("Pass: test_r12_ra2");
   endtask //test_r12_ra2

   task static test_rr_rra(
			   input eci_hreqid_t my_id_i,
			   input eci_dmask_t my_dmask_i,
			   input eci_cl_addr_t my_addr_i,
			   input eci_cl_data_t my_data_i
			   );
      $display("Test: test_rr_rra");
      // Note: Unless you define the address previously, this
      // task will not read the right address 
      // to check the read data
      send_req_wo_data(
		       // casting down from 6 to 5 bits 
		       .my_id_i(eci_id_t'(my_id_i)),
		       .my_dmask_i(my_dmask_i),
		       .my_addr_i(my_addr_i),
		       .rldt_i(1'b1),
		       .rldi_i(1'b0),
		       .rldd_i(1'b0),
		       .rc2d_s_i(1'b0),
		       .rldx_i(1'b0)
		       );
      wait(eci_rsp_valid_o & eci_rsp_ready_i);
      exp_resp_header = eci_cmd_defs::eci_gen_psha(.req_i(eci_req_pkt_i[0]));
      act_resp_header = eci_word_t'(eci_rsp_pkt_o[0]);
      if(act_resp_header.eci_word !== exp_resp_header.eci_word) begin
	 $error("Error: RLDT should respond with PSHA and that is not happeneing");
	 $finish;
      end
      // my_rd_data = eci_cl_data_t'(eci_rsp_pkt_o[ECI_PACKET_SIZE-1:1]);
      // if(my_rd_data !== my_data_i) begin
      // 	 $error("Error: data written %h does not match with data read %h (instance %m)", my_data_i, my_rd_data);
      // 	 $finish;
      // end
      ##1; // wait for output valid signal to go low.
      $display("Pass: test_rr_rra");
   endtask //test_rr_rra

   //---------------------------------------------------------------
   // Configure generic tasks to send specific events. 

   task static send_A11(
			input eci_word_t fwd_req_i
			);
      // HAKV
      // There is no data only header.
      $display("Sending A11");
      send_hak(
	       .fwd_req_i(fwd_req_i),
	       .my_data_i('0),
	       .vicdhi_i(1'b0),
	       .vicdhi_n_i(1'b0),
	       .hakd_i(1'b0),
	       .hakd_n_i(1'b0),
	       .hakn_s_i(1'b0),
	       .haki_i(1'b0),
	       .haks_i(1'b0),
	       .hakv_i(1'b1)
	       );
      ##10; // to avoid stall      
   endtask // send_A11
   
   task static send_A31d(
			 input eci_word_t fwd_req_i,
			 input eci_cl_data_t my_data_i
			 );
      // VICDHI
      $display("Sending A31d");
      send_hak(
	       .fwd_req_i(fwd_req_i),
	       .my_data_i(my_data_i),
	       .vicdhi_i(1'b1),
	       .vicdhi_n_i(1'b0),
	       .hakd_i(1'b0),
	       .hakd_n_i(1'b0),
	       .hakn_s_i(1'b0),
	       .haki_i(1'b0),
	       .haks_i(1'b0),
	       .hakv_i(1'b0)
	       );
      ##10; // to avoid stall 
   endtask // send_A31d

   task static send_A21(
			input eci_word_t fwd_req_i
			);
      // HAKD_N
      // There is no data only header.
      $display("Sending A21");
      send_hak(
	       .fwd_req_i(fwd_req_i),
	       .my_data_i('0),
	       .vicdhi_i(1'b0),
	       .vicdhi_n_i(1'b0),
	       .hakd_i(1'b0),
	       .hakd_n_i(1'b1),
	       .hakn_s_i(1'b0),
	       .haki_i(1'b0),
	       .haks_i(1'b0),
	       .hakv_i(1'b0)
	       );
      ##30; // to avoid stall      
   endtask // send_A21

   task static send_A32d(
			 input eci_word_t fwd_req_i,
			 input eci_cl_data_t my_data_i
			 );
      // HAKD
      $display("Sending A32d");
      send_hak(
	       .fwd_req_i(fwd_req_i),
	       .my_data_i(my_data_i),
	       .vicdhi_i(1'b0),
	       .vicdhi_n_i(1'b0),
	       .hakd_i(1'b1),
	       .hakd_n_i(1'b0),
	       .hakn_s_i(1'b0),
	       .haki_i(1'b0),
	       .haks_i(1'b0),
	       .hakv_i(1'b0)
	       );
      ##10; // to avoid stall 
   endtask // send_A32d

   task static send_A32(
			 input eci_word_t fwd_req_i,
			 input eci_cl_data_t my_data_i
			 );
      // HAKD.N or HAKN
      $display("Sending A32");
      send_hak(
	       .fwd_req_i(fwd_req_i),
	       .my_data_i(my_data_i),
	       .vicdhi_i(1'b0),
	       .vicdhi_n_i(1'b0),
	       .hakd_i(1'b0),
	       .hakd_n_i(1'b1),
	       .hakn_s_i(1'b0),
	       .haki_i(1'b0),
	       .haks_i(1'b0),
	       .hakv_i(1'b0)
	       );
      ##10; // to avoid stall 
   endtask // send_A32

   task static send_v31d(
			 input eci_address_t my_addr_i,
			 input eci_dmask_t my_dmask_i,
			 input eci_cl_data_t my_data_i
			 );
      $display("Sending V31d");
      send_vic(
	       // VICs have no transaction IDs 
	       .my_addr_i(my_addr_i),
	       .my_dmask_i(my_dmask_i),
	       .my_data_i(my_data_i),
	       .vics_i(1'b0),
	       .vicc_i(1'b0),
	       .vicd_i(1'b1),
	       .vicd_n_i(1'b0),
	       .vicc_n_i(1'b0)
	       );
      // CL is in state 1 now, read it to check data
      ##30; // to avoid stalls
   endtask // send_v31d

   task static send_v32d(
			 input eci_address_t my_addr_i,
			 input eci_dmask_t my_dmask_i,
			 input eci_cl_data_t my_data_i
			 );
      $display("Sending V32d");
      send_vic(
	       // VICs have no transaction IDs 
	       .my_addr_i(my_addr_i),
	       .my_dmask_i(my_dmask_i),
	       .my_data_i(my_data_i),
	       .vics_i(1'b0),
	       .vicc_i(1'b1),
	       .vicd_i(1'b0),
	       .vicd_n_i(1'b0),
	       .vicc_n_i(1'b0)
	       );
      ##30; //to avoid stall 
   endtask // send_v32d

   task static send_v32(
			 input eci_address_t my_addr_i,
			 input eci_dmask_t my_dmask_i,
			 input eci_cl_data_t my_data_i
			 );
      $display("Sending V32");
      send_vic(
	       // VICs have no transaction IDs 
	       .my_addr_i(my_addr_i),
	       .my_dmask_i(my_dmask_i),
	       .my_data_i(my_data_i),
	       .vics_i(1'b0),
	       .vicc_i(1'b0),
	       .vicd_i(1'b0),
	       .vicd_n_i(1'b0),
	       .vicc_n_i(1'b1)
	       );
      ##30; //to avoid stall 
   endtask // send_v32


   task static send_v31(
			input eci_address_t my_addr_i,
			input eci_dmask_t my_dmask_i,
			input eci_cl_data_t my_data_i
			);
      $display("Sending V31");
      send_vic(
	       // VICs have no transaction IDs 
	       .my_addr_i(my_addr_i),
	       .my_dmask_i(my_dmask_i),
	       .my_data_i(my_data_i),
	       .vics_i(1'b0),
	       .vicc_i(1'b0),
	       .vicd_i(1'b0),
	       .vicd_n_i(1'b1),
	       .vicc_n_i(1'b0)
	       );
      ##30; // to avoid stalls 
   endtask // send_v31

   task static send_v21(
			input eci_address_t my_addr_i,
			input eci_dmask_t my_dmask_i
			);
      $display("Sending V21");
      send_vic(
	       // VICs have no transaction IDs 
	       .my_addr_i(my_addr_i),
	       .my_dmask_i(my_dmask_i),
	       .my_data_i('0),
	       .vics_i(1'b1),
	       .vicc_i(1'b0),
	       .vicd_i(1'b0),
	       .vicd_n_i(1'b0),
	       .vicc_n_i(1'b0)
	       );
      ##30; // to avoid stalls 
   endtask // send_v21

   task static send_lc(
		       input 	   eci_hreqid_t my_id_i,
		       input 	   eci_dmask_t my_dmask_i,
		       input logic my_ns_i,
		       input 	   eci_address_t my_addr_i
		       );
      $display("Sending LC");
      send_lcl_mfwd(
		    .my_id_i(my_id_i),
		    .my_dmask_i(my_dmask_i),
		    .my_ns_i(my_ns_i),
		    .my_addr_i(my_addr_i),
		    .lc_i(1'b1),
		    .lci_i(1'b0)
		    );
      ##30; // to avoid stalls. 
   endtask //send_lc

   task static send_lci(
		       input 	   eci_hreqid_t my_id_i,
		       input 	   eci_dmask_t my_dmask_i,
		       input logic my_ns_i,
		       input 	   eci_address_t my_addr_i
		       );
      $display("Sending LCI");
      send_lcl_mfwd(
		    .my_id_i(my_id_i),
		    .my_dmask_i(my_dmask_i),
		    .my_ns_i(my_ns_i),
		    .my_addr_i(my_addr_i),
		    .lc_i(1'b0),
		    .lci_i(1'b1)
		    );
      ##30; // to avoid stalls. 
   endtask //send_lci

   task static send_ul(
		       input eci_address_t my_addr_i
		       );
      $display("Sending UL");
      send_lcl_ul(
		  .my_addr_i(my_addr_i)
		  );
      ##10;
   endtask //send_ul
   //---------------------------------------------------------------
   // Configurable tasks to send different events corresponding to a VC to DirC.
   // Generate and send  requests without datta
   // RLDT, RLDD, RLDI, RC2D_S 
   task send_req_wo_data(
			 input 	     eci_id_t my_id_i,
			 input 	     eci_dmask_t my_dmask_i,
			 input 	     eci_address_t my_addr_i,
			 // Note only 1 of the folloaing bits
			 // can be set 
			 input logic rldt_i,
			 input logic rldd_i,
			 input logic rldi_i,
			 input logic rc2d_s_i,
			 input logic rldx_i
		  );
      eci_word_t my_hdr;
      eci_cl_data_t my_data2send;
      eci_vc_size_t my_vc;
      logic [ECI_PACKET_SIZE_WIDTH-1:0] my_size;
      // Header 
      my_hdr = gen_req_wo_data(
			       .rreq_id_i(my_id_i),
			       .dmask_i(my_dmask_i),
			       .addr_i(my_addr_i),
			       .rldt_i(rldt_i),
			       .rldi_i(rldi_i),
			       .rldd_i(rldd_i),
			       .rc2d_s_i(rc2d_s_i),
			       .rldx_i(rldx_i)
			       );
      // Data 
      // Dont care about data for read
      my_data2send.flat = '0;
      // VC
      // request wo data - VC6/7
      // odd cl indices VC6, even VC7
      my_vc = '0;
      if(my_addr_i[ECI_CL_ADDR_LSB] == CL_IDX_ODD) begin
	 my_vc = eci_vc_size_t'('d6);
      end else begin
	 my_vc = eci_vc_size_t'('d7);
      end
      // Packet size 
      // req wo data has no data so size is 1
      my_size = ECI_PACKET_SIZE_WIDTH'('d1);
      // Send to dirc 
      send_cmd(
	       .my_hdr_i(my_hdr),
	       .my_data_i(my_data2send),
	       .my_vc_i(my_vc),
	       .my_size_i(my_size)
	       );
   endtask // send_req_wo_data

   // Send HAK commands to DirC 
   task static send_hak(
			input 	    eci_word_t fwd_req_i,
			input 	    eci_cl_data_t my_data_i,
			input logic vicdhi_i,
			input logic vicdhi_n_i,
			input logic hakd_i,
			input logic hakd_n_i,
			input logic hakn_s_i,
			input logic haki_i,
			input logic haks_i,
			input logic hakv_i
			);
      eci_word_t my_hdr;
      eci_cl_data_t my_data2send;
      eci_vc_size_t my_vc;
      logic [ECI_PACKET_SIZE_WIDTH-1:0] my_size;
      // Header
      my_hdr = gen_hak_hdr(
			   .fwd_req_i(fwd_req_i),
			   .vicdhi_i(vicdhi_i),
			   .vicdhi_n_i(vicdhi_n_i),
			   .hakd_i(hakd_i),
			   .hakd_n_i(hakd_n_i),
			   .hakn_s_i(hakn_s_i),
			   .haki_i(haki_i),
			   .haks_i(haks_i),
			   .hakv_i(hakv_i)
			   );
      // Data
      my_data2send.flat = '0;
      if(vicdhi_n_i | hakd_n_i | hakn_s_i | haks_i | hakv_i | haki_i) begin
	 // No data needs to be sent
	 // Response without data - VC 10,11
	 my_data2send.flat = '0;
	 my_size = ECI_PACKET_SIZE_WIDTH'('d1);
	 if(fwd_req_i.mfwd_generic.address[ECI_CL_ADDR_LSB] == CL_IDX_ODD) begin
	    my_vc = eci_vc_size_t'('d10);
	 end else begin
	    my_vc = eci_vc_size_t'('d11);
	 end
      end else begin
	 // Response with data VC 4,5
	 my_data2send = my_data_i;
	 my_size = get_pkt_size_from_dmask(.my_dmask_i(fwd_req_i.mfwd_generic.dmask));
	 if(fwd_req_i.mfwd_generic.address[ECI_CL_ADDR_LSB] == CL_IDX_ODD) begin
	    my_vc = eci_vc_size_t'('d4);
	 end else begin
	    my_vc = eci_vc_size_t'('d5);
	 end
      end
      // Send to dirc 
      send_cmd(
	       .my_hdr_i(my_hdr),
	       .my_data_i(my_data2send),
	       .my_vc_i(my_vc),
	       .my_size_i(my_size)
	       );
   endtask //send_hak

   // Send VIC
   task static send_vic(
			input eci_address_t my_addr_i,
			input eci_dmask_t my_dmask_i,
			input eci_cl_data_t my_data_i,
			input vics_i, //V21
			input vicc_i, //V32d
			input vicd_i, //V31d
			input vicd_n_i, //V31
			input vicc_n_i  //V32
			);
      eci_word_t my_hdr;
      eci_cl_data_t my_data2send;
      eci_vc_size_t my_vc;
      logic [ECI_PACKET_SIZE_WIDTH-1:0] my_size;
      // Header 
      my_hdr = gen_vic_hdr(
			   .my_addr_i(my_addr_i),
			   .my_dmask_i(my_dmask_i),
			   .vics_i(vics_i),
			   .vicc_i(vicc_i),
			   .vicd_i(vicd_i),
			   .vicd_n_i(vicd_n_i),
			   .vicc_n_i(vicc_n_i)
			   );
      // Data 
      my_data2send.flat = '0;
      if(vics_i | vicd_n_i | vicc_n_i) begin
	 // No data needed for VICS, VICD.N, VICC.N
	 my_data2send.flat = '0;
      end else begin
	 my_data2send = my_data_i;
      end
      // VC
      my_vc = '0;
      if(vics_i | vicd_n_i | vicc_n_i) begin
	 // resp wo data 10, 11
	 if(my_addr_i[ECI_CL_ADDR_LSB] == CL_IDX_ODD) begin
	    my_vc = eci_vc_size_t'('d10);
	 end else begin
	    my_vc = eci_vc_size_t'('d11);
	 end
      end else begin
	 // response with data VC4, 5 V32d, V31d
	 if(my_addr_i[ECI_CL_ADDR_LSB] == CL_IDX_ODD) begin
	    my_vc = eci_vc_size_t'('d4);
	 end else begin
	    my_vc = eci_vc_size_t'('d5);
	 end
      end
      // Packet size 
      my_size = '0;
      if(vics_i | vicc_n_i | vicd_n_i) begin
	 // Only header no data for V21, V32, V31
	 my_size = ECI_PACKET_SIZE_WIDTH'('d1);
      end else begin
	 my_size = get_pkt_size_from_dmask(.my_dmask_i(my_dmask_i));
      end
      // Send to dirc 
      send_cmd(
	       .my_hdr_i(my_hdr),
	       .my_data_i(my_data2send),
	       .my_vc_i(my_vc),
	       .my_size_i(my_size)
	       );
   endtask //send_vic

   task static send_lcl_mfwd(
			     input 	 eci_hreqid_t my_id_i,
			     input 	 eci_dmask_t my_dmask_i,
			     input logic my_ns_i,
			     input 	 eci_address_t my_addr_i,
			     // local clean.
			     input logic lc_i,
			     // local clean invalidate. 
			     input logic lci_i
			     );
      eci_word_t my_hdr;
      eci_cl_data_t my_data2send;
      eci_vc_size_t my_vc;
      logic [ECI_PACKET_SIZE_WIDTH-1:0] my_size;
      // generate header
      my_hdr = gen_lcl_mfwd_hdr(
				.my_id_i(my_id_i),
				.my_dmask_i(my_dmask_i),
				.my_ns_i(my_ns_i),
				.my_addr_i(my_addr_i),
				.lc_i(lc_i),
				.lci_i(lci_i)
				);
      // Data - no data to send.
      my_data2send.flat = '0;
      // VC -odd cl index to even VC and vice versa.
      if(my_addr_i[ECI_CL_ADDR_LSB] == CL_IDX_ODD) begin
	 my_vc = eci_vc_size_t'(VC_LCL_FWD_WO_DATA_E);
      end else begin
	 my_vc = eci_vc_size_t'(VC_LCL_FWD_WO_DATA_O);
      end
      // Packet size - always 1.
      my_size = ECI_PACKET_SIZE_WIDTH'('d1);
      // send to dcu.
      send_cmd(
	       .my_hdr_i(my_hdr),
	       .my_data_i(my_data2send),
	       .my_vc_i(my_vc),
	       .my_size_i(my_size)
	       );
   endtask //send_lcl_mfwd

   task static send_lcl_ul(
			   input eci_address_t my_addr_i
			   );
      eci_word_t my_hdr;
      eci_cl_data_t my_data2send;
      eci_vc_size_t my_vc;
      logic [ECI_PACKET_SIZE_WIDTH-1:0] my_size;
      my_hdr = gen_lcl_ul_hdr(
			      .my_addr_i(my_addr_i)
			      );
      // Data - no data to send.
      my_data2send.flat = '0;
      // VC -odd cl index to even VC and vice versa.
      if(my_addr_i[ECI_CL_ADDR_LSB] == CL_IDX_ODD) begin
	 my_vc = eci_vc_size_t'(VC_LCL_RESP_WO_DATA_E);
      end else begin
	 my_vc = eci_vc_size_t'(VC_LCL_RESP_WO_DATA_O);
      end
      // Packet size - always 1.
      my_size = ECI_PACKET_SIZE_WIDTH'('d1);
      // send to dcu.
      send_cmd(
	       .my_hdr_i(my_hdr),
	       .my_data_i(my_data2send),
	       .my_vc_i(my_vc),
	       .my_size_i(my_size)
	       );

   endtask //send_lcl_mrsp
   
   //---------------------------------------------------------------
   // Most basic task
   
   task static send_cmd
     (
      // header 
      input 				      eci_word_t my_hdr_i,
      // data 
      input 				      eci_cl_data_t my_data_i,
      // VC 
      input 				      eci_vc_size_t my_vc_i,
      input logic [ECI_PACKET_SIZE_WIDTH-1:0] my_size_i
      );
      // Generic task to send input ECI event to dirC.
      eci_req_pkt_i[0] = my_hdr_i;
      eci_req_pkt_i[ECI_PACKET_SIZE-1:1] = my_data_i.words;
      eci_req_vc_i = my_vc_i;
      eci_req_size_i = my_size_i;
      eci_req_valid_i = 1'b1;
      wait((eci_req_valid_i & eci_req_ready_o) | 
	   (eci_req_valid_i & eci_req_stall_o) | 
	   dbg_error_o);
      ##1;
      eci_req_valid_i = 1'b0;
   endtask // send_cmd


   //---------------------------------------------------------------
   // Functions to generate requests.
   // RLDT, RLDD, RLDI, RC2D_S - Req wo data.
   
   function automatic eci_word_t gen_req_wo_data
     (
      input 	  eci_id_t rreq_id_i,
      input 	  eci_dmask_t dmask_i,
      input 	  eci_address_t addr_i,
      input logic rldt_i,
      input logic rldi_i,
      input logic rldd_i,
      input logic rc2d_s_i,
      input logic rldx_i
      );
      eci_word_t my_hdr;
      my_hdr.eci_word = '0;
      if(rldt_i) begin
	 my_hdr.mreq_load.opcode = ECI_CMD_MREQ_RLDT;
      end
      if(rldi_i) begin
	 my_hdr.mreq_load.opcode = ECI_CMD_MREQ_RLDI;
      end      
      if(rldd_i) begin
	 my_hdr.mreq_load.opcode = ECI_CMD_MREQ_RLDD;
      end
      if(rc2d_s_i) begin
	 my_hdr.mreq_load.opcode = ECI_CMD_MREQ_RC2D_S;
      end
      if(rldx_i) begin
	 my_hdr.mreq_load.opcode = ECI_CMD_MREQ_RLDX;
      end
      my_hdr.mreq_load.rreq_id = rreq_id_i;
      my_hdr.mreq_load.dmask = dmask_i;
      my_hdr.mreq_load.address = addr_i;
      return(my_hdr);
   endfunction : gen_req_wo_data

   
   function automatic eci_word_t gen_hak_hdr
     (
      input 	  eci_word_t fwd_req_i,
      input logic vicdhi_i,
      input logic vicdhi_n_i,
      input logic hakd_i,
      input logic hakd_n_i,
      input logic hakn_s_i,
      input logic haki_i,
      input logic haks_i,
      input logic hakv_i
      );
      eci_word_t my_hdr;
      my_hdr.eci_word = '0;
      if(vicdhi_i | vicdhi_n_i) begin
	 my_hdr.mrsp_3to8.opcode = ECI_CMD_MRSP_VICDHI;
      end
      if(hakd_i | hakd_n_i) begin
	 my_hdr.mrsp_3to8.opcode = ECI_CMD_MRSP_HAKD;
      end
      if(hakn_s_i) begin
	 my_hdr.mrsp_3to8.opcode = ECI_CMD_MRSP_HAKN_S;
      end
      if(haki_i) begin
	 my_hdr.mrsp_3to8.opcode = ECI_CMD_MRSP_HAKI;
      end
      if(haks_i) begin
	 my_hdr.mrsp_3to8.opcode = ECI_CMD_MRSP_HAKS;
      end
      if(hakv_i) begin
	 my_hdr.mrsp_3to8.opcode = ECI_CMD_MRSP_HAKV;
      end
      my_hdr.mrsp_3to8.hreq_id = fwd_req_i.mfwd_generic.hreq_id;
      my_hdr.mrsp_3to8.dmask = fwd_req_i.mfwd_generic.dmask;
      if(vicdhi_n_i | hakd_n_i | hakn_s_i | haks_i | hakv_i | haki_i) begin
	 // A31, A21, A32, A22, A11, A11 - no data 
	 my_hdr.mrsp_3to8.dmask = '0;
      end
      my_hdr.mrsp_3to8.ns = fwd_req_i.mfwd_generic.ns;
      my_hdr.mrsp_3to8.address = fwd_req_i.mfwd_generic.address;
      return(my_hdr);
   endfunction : gen_hak_hdr
   
   
   function automatic eci_word_t gen_vic_hdr
     (
      input eci_address_t my_addr_i,
      input eci_dmask_t my_dmask_i,
      input vics_i, // V21
      input vicc_i, // V32d
      input vicd_i, // V31d
      input vicd_n_i, // V31
      input vicc_n_i // V32
      );
      eci_word_t my_hdr;
      my_hdr.eci_word = '0;
      if(vics_i) begin
	 my_hdr.mrsp_0to2.opcode = ECI_CMD_MRSP_VICS;
	 my_hdr.mrsp_0to2.dmask = '0;
      end
      if(vicd_n_i | vicd_i) begin
	 my_hdr.mrsp_0to2.opcode = ECI_CMD_MRSP_VICD;
	 if(vicd_n_i) begin
	    my_hdr.mrsp_0to2.dmask = '0;
	 end else begin
	    my_hdr.mrsp_0to2.dmask = my_dmask_i;
	 end
      end
      if(vicc_n_i | vicc_i) begin
	 my_hdr.mrsp_0to2.opcode = ECI_CMD_MRSP_VICC;
	 if(vicc_n_i) begin
	    my_hdr.mrsp_0to2.dmask = '0;
	 end else begin
	    my_hdr.mrsp_0to2.dmask = my_dmask_i;
	 end
      end
      my_hdr.mrsp_0to2.ns = 1'b1;
      my_hdr.mrsp_0to2.address = my_addr_i;
      return(my_hdr);
   endfunction : gen_vic_hdr

   function automatic eci_word_t gen_lcl_mfwd_hdr
     (
      input 	  eci_hreqid_t my_id_i,
      input 	  eci_dmask_t my_dmask_i,
      input logic my_ns_i,
      input 	  eci_address_t my_addr_i,
      // local clean.
      input logic lc_i,
      // local clean invalidate. 
      input logic lci_i
      );
      eci_word_t my_hdr;
      my_hdr.eci_word = '0;
      if(lc_i) begin
	 // generate LC header.
	 my_hdr.lcl_mfwd_generic.opcode = LCL_CMD_MFWD_CLEAN;
      end else begin
	 // generate LCI header.
	 my_hdr.lcl_mfwd_generic.opcode = LCL_CMD_MFWD_CLEAN_INV;
      end
      my_hdr.lcl_mfwd_generic.hreq_id = my_id_i;
      my_hdr.lcl_mfwd_generic.dmask = my_dmask_i;
      my_hdr.lcl_mfwd_generic.ns = my_ns_i;
      my_hdr.lcl_mfwd_generic.address = my_addr_i;
      return(my_hdr);
   endfunction : gen_lcl_mfwd_hdr

   function automatic eci_word_t gen_lcl_ul_hdr
     (
      input eci_address_t my_addr_i
      );
      eci_word_t my_hdr;
      my_hdr.eci_word = '0;
      my_hdr.lcl_unlock.opcode = LCL_CMD_MRSP_UNLOCK;
      my_hdr.lcl_unlock.address = my_addr_i;
      return(my_hdr);
   endfunction : gen_lcl_ul_hdr
   
endmodule
