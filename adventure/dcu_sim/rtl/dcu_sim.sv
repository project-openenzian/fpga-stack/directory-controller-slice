`ifndef DCU_SIM_SV
 `define DCU_SIM_SV

import eci_dcs_defs::*;

module dcu_sim #
  (
   parameter STORE_HDR_WIDTH = 64,
   parameter RETRIEVE_HDR_WIDTH = 64,
   parameter SIM_DELAY = 5
   )
  (
   input logic 				 clk,
   input logic 				 reset,
   input logic [DS_DCU_IDX_WIDTH-1:0] 	 req_dcu_idx_i,
   input logic [STORE_HDR_WIDTH-1:0] 	 req_hdr_i,
   input logic 				 req_valid_i,
   output logic 			 req_ready_o,
   output logic [DS_DCU_ID_WIDTH-1:0] 	 rsp_dcu_id_o,
   output logic [RETRIEVE_HDR_WIDTH-1:0] rsp_hdr_o,
   output logic 			 rsp_valid_o,
   input logic 				 rsp_ready_i
   );

   logic [63:0] 			 counter_reg = '0, counter_next;
   logic 				 en_vr;

   logic [DS_DCU_IDX_WIDTH-1:0] 	 p_req_dcu_idx_i;
   logic [STORE_HDR_WIDTH-1:0] 		 p_req_hdr_i;
   logic 				 p_req_valid_i;
   logic 				 p_req_ready_o;

   logic [DS_DCU_IDX_WIDTH-1:0] 	 p_req_dcu_idx_o;
   logic [STORE_HDR_WIDTH-1:0] 		 p_req_hdr_o;
   logic 				 p_req_valid_o;
   logic 				 p_req_ready_i;


   logic [DS_DCU_ID_WIDTH-1:0] 		 n_rsp_dcu_id_i;
   logic [RETRIEVE_HDR_WIDTH-1:0] 	 n_rsp_hdr_i;
   logic 				 n_rsp_valid_i;
   logic 				 n_rsp_ready_o;

   logic [DS_DCU_ID_WIDTH-1:0] 		 n_rsp_dcu_id_o;
   logic [RETRIEVE_HDR_WIDTH-1:0] 	 n_rsp_hdr_o;
   logic 				 n_rsp_valid_o;
   logic 				 n_rsp_ready_i;

   // Output assign
   always_comb begin : OUT_ASSIGN
      rsp_dcu_id_o = n_rsp_dcu_id_o;
      rsp_hdr_o = n_rsp_hdr_o;
      rsp_valid_o = n_rsp_valid_o;
      req_ready_o = p_req_ready_o;
   end : OUT_ASSIGN
   
   // Input pipeline stage.
   always_comb begin : FS_IP_ASSIGN
      p_req_dcu_idx_i = req_dcu_idx_i;
      p_req_hdr_i = req_hdr_i;
      p_req_valid_i = req_valid_i;
      p_req_ready_i = n_rsp_ready_o & en_vr;
   end : FS_IP_ASSIGN
   
   axis_pipeline_stage #
     (
      .DATA_WIDTH(DS_DCU_IDX_WIDTH +  STORE_HDR_WIDTH)
      )
   first_stage_pl1
     (
      .clk(clk),
      .reset(reset),
      .us_data({p_req_dcu_idx_i, p_req_hdr_i}),
      .us_valid(p_req_valid_i),
      .us_ready(p_req_ready_o),
      .ds_data({p_req_dcu_idx_o, p_req_hdr_o}),
      .ds_valid(p_req_valid_o),
      .ds_ready(p_req_ready_i)
      );

   // Connect this properly.
   // continue here
   always_comb begin : LS_IP_ASSIGN
      n_rsp_dcu_id_i = {p_req_dcu_idx_o, 1'b0};
      n_rsp_hdr_i = p_req_hdr_o;
      n_rsp_valid_i = p_req_valid_o & en_vr;
      n_rsp_ready_i = rsp_ready_i;
   end : LS_IP_ASSIGN
   axis_pipeline_stage #
     (
      .DATA_WIDTH(DS_DCU_ID_WIDTH +  RETRIEVE_HDR_WIDTH)
      )
   last_stage_pl1
     (
      .clk(clk),
      .reset(reset),
      .us_data({n_rsp_dcu_id_i, n_rsp_hdr_i}),
      .us_valid(n_rsp_valid_i),
      .us_ready(n_rsp_ready_o),
      .ds_data({n_rsp_dcu_id_o, n_rsp_hdr_o}),
      .ds_valid(n_rsp_valid_o),
      .ds_ready(n_rsp_ready_i)
      );

   // counter is initially at 0.
   // if input is valid, start counter.
   // keep counter running till SIM_DELAY 
   // count is reached. Once count is
   // reached, output becomes valid and
   // downstream ready is fed into input.
   // Counter is reset upon handshake in
   // request i/f.
   always_comb begin : CONTROLLER
      counter_next	= counter_reg;
      en_vr = 1'b0;
      
      if(counter_reg == SIM_DELAY) begin
	 en_vr = 1'b1;
      end
 
      if(p_req_valid_o & p_req_ready_i) begin
	 counter_next = '0;
      end else if(p_req_valid_o & ~en_vr) begin
	 counter_next = counter_reg + 1;
      end 
   end : CONTROLLER

   always_ff @(posedge clk) begin : REG_ASSIGN
      counter_reg	<= counter_next;
      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset 
	 counter_reg	<= '0;
      end
   end : REG_ASSIGN

endmodule //dcu_sim
`endif
