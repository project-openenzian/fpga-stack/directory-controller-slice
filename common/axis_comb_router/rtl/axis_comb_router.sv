/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-07-22
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */

`ifndef AXIS_COMB_ROUTER_SV
`define AXIS_COMB_ROUTER_SV

/*
 * Module Description:
 *  1 to N demultiplexer combinational logic. 
 *  1 input channel, N output channels.
 *  Input data is routed to appropriate output 
 *  channel based on SELECT signal. 
 * 
 *  Input and all N output channels have valid 
 *  ready flow control. 
 *
 * Input Output Description:
 *  Input data + select signal.
 *  Select signal identifies the one of N output 
 *  channels to route the data. 
 *  valid ready flow control. 
 * 
 *  Output data channels. 
 *  N output channels with valid ready flow control 
 *  one of the outputs will be valid at a time. 
 *
 * Architecture Description:
 *  The input data is broadcast to all output channels. 
 *  Only one output channel is valid depending on input 
 *  select signal. 
 *
 * Modifiable Parameters:
 *  DATA_WIDTH - width of data to be routed. 
 *  NUM_OUT - Number of output channels. 
 *
 * Non-modifiable Parameters:
 *  SEL_WIDTH should be $clog2(NUM_OUT).
 * 
 * Modules Used:
 *  None.
 *
 * Notes:
 *  Combinational logic, outputs are not registered.
 *  In input channel, ready waits for valid to be asserted. 
 *  In output channels, valid does not wait for ready to be asserted. 
 *
 */

module axis_comb_router #
  (
   parameter DATA_WIDTH = 64,
   parameter NUM_OUT = 4,
   parameter SEL_WIDTH = $clog2(NUM_OUT)
   )
   (
    // 1 Input data + select.
    input logic [DATA_WIDTH-1:0] 	       us_data_i,
    input logic [SEL_WIDTH-1:0] 	       us_sel_i,
    input logic 			       us_valid_i,
    output logic 			       us_ready_o,

    // NUM_OUT Output channels.
    // only 1 channel will be valid depending on
    // input select signal. 
    output logic [NUM_OUT-1:0][DATA_WIDTH-1:0] ds_data_o,
    output logic [NUM_OUT-1:0] 		       ds_valid_o,
    input logic [NUM_OUT-1:0] 		       ds_ready_i
    );

   initial begin
      if(SEL_WIDTH != $clog2(NUM_OUT)) begin
	 $error("Error: SEL_WIDTH should be clog2(NUM_OUT) (instance %m)");
	 $finish;
      end
   end
   
   logic [NUM_OUT-1:0] 			       ds_hs;
   logic 				       any_ds_hs;

   assign any_ds_hs = |ds_hs;

   //Output assign
   // if handshake happens in any output channel
   // this means input is consumed. 
   assign us_ready_o = any_ds_hs;
   
   // Only 1 output will be valid at a time,
   // broadcast input data to all outputs. 
   always_comb begin : DS_DATA_ASSIGN
      for( integer i=0; i<NUM_OUT; i=i+1 ) begin
	 ds_data_o[i] = us_data_i;
      end
   end : DS_DATA_ASSIGN

   // Only 1 output will be valid at a time
   // and it is the output selected by upstream.
   always_comb begin : DS_VALID_ASSIGN
      ds_valid_o = '0;
      ds_valid_o[us_sel_i] = us_valid_i;
   end : DS_VALID_ASSIGN

   // Only 1 handshake can happen at a time
   // and it will be in the downstream port which
   // was selected and assigned valid to.
   always_comb begin : ANY_DS_HS
      for( integer i=0; i<NUM_OUT; i=i+1 ) begin
	 ds_hs[i] = 1'b0;
	 if(ds_valid_o[i] & ds_ready_i[i]) begin
	    ds_hs[i] = 1'b1;
	 end
      end
   end : ANY_DS_HS
endmodule // axis_comb_router

`endif
