/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-11-16
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */

`ifndef ARB_4_ECIH_SV
 `define ARB_4_ECIH_SV

import eci_cmd_defs::*;

// Choose between 5 ECI inputs to pass to DCU.
// 
// The DCU waits for one of the input headers
// to be selected and its valid goes high.
// then it takes a number of cycles to identify
// if the header is accepted or stalled.
// if header is accepted, DCU asserts ready for
// 1 cycle.
// if header is not accepted, DCU asserts stall
// for 1 cycle.
//
// when ready is asserted, the selected input
// is consumed and new input is chosen.
//
// when stall is asserted, the selected input
// is not consumed yet but priority is rotated
// and new input is chosen.
//
// II = 2 because of the weird valid-ready and
// valid-stall handshakes.

module arb_4_ecih 
  (
   input logic 					clk,
   input logic 					reset,
   // Input ECI header 1
   input logic [ECI_WORD_WIDTH-1:0] 		eci_hdr1_i,
   input logic [ECI_PACKET_SIZE_WIDTH-1:0] 	eci_pkt1_size_i,
   input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	eci_pkt1_vc_i,
   input logic 					eci_pkt1_valid_i,
   output logic 				eci_pkt1_ready_o,
   output logic 				eci_pkt1_stall_o,
   // Input ECI header 2
   input logic [ECI_WORD_WIDTH-1:0] 		eci_hdr2_i,
   input logic [ECI_PACKET_SIZE_WIDTH-1:0] 	eci_pkt2_size_i,
   input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	eci_pkt2_vc_i,
   input logic 					eci_pkt2_valid_i,
   output logic 				eci_pkt2_ready_o,
   output logic 				eci_pkt2_stall_o,
   // Input ECI header 3
   input logic [ECI_WORD_WIDTH-1:0] 		eci_hdr3_i,
   input logic [ECI_PACKET_SIZE_WIDTH-1:0] 	eci_pkt3_size_i,
   input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	eci_pkt3_vc_i,
   input logic 					eci_pkt3_valid_i,
   output logic 				eci_pkt3_ready_o,
   output logic 				eci_pkt3_stall_o,
   // Input ECI header 4
   input logic [ECI_WORD_WIDTH-1:0] 		eci_hdr4_i,
   input logic [ECI_PACKET_SIZE_WIDTH-1:0] 	eci_pkt4_size_i,
   input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	eci_pkt4_vc_i,
   input logic 					eci_pkt4_valid_i,
   output logic 				eci_pkt4_ready_o,
   output logic 				eci_pkt4_stall_o,
   // Input ECI header 5
   input logic [ECI_WORD_WIDTH-1:0] 		eci_hdr5_i,
   input logic [ECI_PACKET_SIZE_WIDTH-1:0] 	eci_pkt5_size_i,
   input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	eci_pkt5_vc_i,
   input logic 					eci_pkt5_valid_i,
   output logic 				eci_pkt5_ready_o,
   output logic 				eci_pkt5_stall_o,
   // Output ECI header
   output logic [ECI_WORD_WIDTH-1:0] 		eci_hdr_o,
   output logic [ECI_PACKET_SIZE_WIDTH-1:0] 	eci_pkt_size_o,
   output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] eci_pkt_vc_o,
   output logic 				eci_pkt_valid_o,
   input logic 					eci_pkt_ready_i,
   input logic 					eci_pkt_stall_i
   );

   // Stage 0
   // 1 3-ip, 1 2-ip ARB.
   // Stage 1
   // 1 2-ip ARB.
   typedef struct packed {
      logic [ECI_WORD_WIDTH-1:0]        hdr;
      logic [ECI_PACKET_SIZE_WIDTH-1:0] size;
      logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] vc;
      logic				valid;
      logic				ready;
      logic 				stall;
   } eci_hdr_if_t;

   eci_hdr_if_t s0_pkt1_i, s0_pkt2_i, s0_pkt3_i, s0_pkt1_o;
   eci_hdr_if_t s0_pkt4_i, s0_pkt5_i, s0_pkt2_o;
   eci_hdr_if_t s1_pkt1_i, s1_pkt2_i, s1_pkt_o;

   always_comb begin : OP_ASSIGN
      eci_pkt1_ready_o = s0_pkt1_i.ready;
      eci_pkt1_stall_o = s0_pkt1_i.stall;
      eci_pkt2_ready_o = s0_pkt2_i.ready;
      eci_pkt2_stall_o = s0_pkt2_i.stall;
      eci_pkt3_ready_o = s0_pkt3_i.ready;
      eci_pkt3_stall_o = s0_pkt3_i.stall;
      eci_pkt4_ready_o = s0_pkt4_i.ready;
      eci_pkt4_stall_o = s0_pkt4_i.stall;
      eci_pkt5_ready_o = s0_pkt5_i.ready;
      eci_pkt5_stall_o = s0_pkt5_i.stall;
      eci_hdr_o	       = s1_pkt_o.hdr;
      eci_pkt_size_o   = s1_pkt_o.size;
      eci_pkt_vc_o     = s1_pkt_o.vc;
      eci_pkt_valid_o  = s1_pkt_o.valid; 
   end : OP_ASSIGN

   // Stage 0
   // 1 3-ip, 1 2-ip ARB.
   always_comb begin : S0_ARB_3_IP_ASSIGN
      s0_pkt1_i.hdr	= eci_hdr1_i;
      s0_pkt1_i.size	= eci_pkt1_size_i;
      s0_pkt1_i.vc	= eci_pkt1_vc_i;
      s0_pkt1_i.valid	= eci_pkt1_valid_i;
      s0_pkt2_i.hdr	= eci_hdr2_i;
      s0_pkt2_i.size	= eci_pkt2_size_i;
      s0_pkt2_i.vc	= eci_pkt2_vc_i;
      s0_pkt2_i.valid	= eci_pkt2_valid_i;
      s0_pkt3_i.hdr	= eci_hdr3_i;
      s0_pkt3_i.size	= eci_pkt3_size_i;
      s0_pkt3_i.vc	= eci_pkt3_vc_i;
      s0_pkt3_i.valid	= eci_pkt3_valid_i;
      s0_pkt1_o.ready	= s1_pkt1_i.ready;
      s0_pkt1_o.stall	= s1_pkt1_i.stall;
   end : S0_ARB_3_IP_ASSIGN
   arb_3_ecih
     arb_3ip_s0
       (
	.clk			(clk),
	.reset			(reset),
	.eci_hdr1_i		(s0_pkt1_i.hdr),
	.eci_pkt1_size_i	(s0_pkt1_i.size),
	.eci_pkt1_vc_i		(s0_pkt1_i.vc),
	.eci_pkt1_valid_i	(s0_pkt1_i.valid),
	.eci_pkt1_ready_o	(s0_pkt1_i.ready),
	.eci_pkt1_stall_o	(s0_pkt1_i.stall),
	.eci_hdr2_i		(s0_pkt2_i.hdr),
	.eci_pkt2_size_i	(s0_pkt2_i.size),
	.eci_pkt2_vc_i		(s0_pkt2_i.vc),
	.eci_pkt2_valid_i	(s0_pkt2_i.valid),
	.eci_pkt2_ready_o	(s0_pkt2_i.ready),
	.eci_pkt2_stall_o	(s0_pkt2_i.stall),
	.eci_hdr3_i		(s0_pkt3_i.hdr),
	.eci_pkt3_size_i	(s0_pkt3_i.size),
	.eci_pkt3_vc_i		(s0_pkt3_i.vc),
	.eci_pkt3_valid_i	(s0_pkt3_i.valid),
	.eci_pkt3_ready_o	(s0_pkt3_i.ready),
	.eci_pkt3_stall_o	(s0_pkt3_i.stall),
	.eci_hdr_o		(s0_pkt1_o.hdr),
	.eci_pkt_size_o		(s0_pkt1_o.size),
	.eci_pkt_vc_o		(s0_pkt1_o.vc),
	.eci_pkt_valid_o	(s0_pkt1_o.valid),
	.eci_pkt_ready_i	(s0_pkt1_o.ready),
	.eci_pkt_stall_i	(s0_pkt1_o.stall)
	);

   always_comb begin : S0_ARB_2_IP_ASSIGN
      s0_pkt4_i.hdr	= eci_hdr4_i;
      s0_pkt4_i.size	= eci_pkt4_size_i;
      s0_pkt4_i.vc	= eci_pkt4_vc_i;
      s0_pkt4_i.valid	= eci_pkt4_valid_i;
      s0_pkt5_i.hdr	= eci_hdr5_i;
      s0_pkt5_i.size	= eci_pkt5_size_i;
      s0_pkt5_i.vc	= eci_pkt5_vc_i;
      s0_pkt5_i.valid	= eci_pkt5_valid_i;
      s0_pkt2_o.ready	= s1_pkt2_i.ready;
      s0_pkt2_o.stall	= s1_pkt2_i.stall;
   end : S0_ARB_2_IP_ASSIGN
   arb_2_ecih
     arb_2ip_s0
       (
	.clk			(clk),
	.reset			(reset),
	.eci_hdr1_i		(s0_pkt4_i.hdr),
	.eci_pkt1_size_i	(s0_pkt4_i.size),
	.eci_pkt1_vc_i		(s0_pkt4_i.vc),
	.eci_pkt1_valid_i	(s0_pkt4_i.valid),
	.eci_pkt1_ready_o	(s0_pkt4_i.ready),
	.eci_pkt1_stall_o	(s0_pkt4_i.stall),
	.eci_hdr2_i		(s0_pkt5_i.hdr),
	.eci_pkt2_size_i	(s0_pkt5_i.size),
	.eci_pkt2_vc_i		(s0_pkt5_i.vc),
	.eci_pkt2_valid_i	(s0_pkt5_i.valid),
	.eci_pkt2_ready_o	(s0_pkt5_i.ready),
	.eci_pkt2_stall_o	(s0_pkt5_i.stall),
	.eci_hdr_o		(s0_pkt2_o.hdr),
	.eci_pkt_size_o		(s0_pkt2_o.size),
	.eci_pkt_vc_o		(s0_pkt2_o.vc),
	.eci_pkt_valid_o	(s0_pkt2_o.valid),
	.eci_pkt_ready_i	(s0_pkt2_o.ready),
	.eci_pkt_stall_i	(s0_pkt2_o.stall)
	);

   // Stage 1
   // 1 2-ip ARB.
   always_comb begin : S1_ARB_2_IP_ASSIGN
      s1_pkt1_i.hdr   = s0_pkt1_o.hdr;
      s1_pkt1_i.size  = s0_pkt1_o.size;
      s1_pkt1_i.vc    = s0_pkt1_o.vc;
      s1_pkt1_i.valid = s0_pkt1_o.valid;
      s1_pkt2_i.hdr   = s0_pkt2_o.hdr;
      s1_pkt2_i.size  = s0_pkt2_o.size;
      s1_pkt2_i.vc    = s0_pkt2_o.vc;
      s1_pkt2_i.valid = s0_pkt2_o.valid;
      s1_pkt_o.ready  = eci_pkt_ready_i;
      s1_pkt_o.stall  = eci_pkt_stall_i;
   end : S1_ARB_2_IP_ASSIGN
   arb_2_ecih
     arb_2ip_s1
       (
	.clk			(clk),
	.reset			(reset),
	.eci_hdr1_i		(s1_pkt1_i.hdr),
	.eci_pkt1_size_i	(s1_pkt1_i.size),
	.eci_pkt1_vc_i		(s1_pkt1_i.vc),
	.eci_pkt1_valid_i	(s1_pkt1_i.valid),
	.eci_pkt1_ready_o	(s1_pkt1_i.ready),
	.eci_pkt1_stall_o	(s1_pkt1_i.stall),
	.eci_hdr2_i		(s1_pkt2_i.hdr),
	.eci_pkt2_size_i	(s1_pkt2_i.size),
	.eci_pkt2_vc_i		(s1_pkt2_i.vc),
	.eci_pkt2_valid_i	(s1_pkt2_i.valid),
	.eci_pkt2_ready_o	(s1_pkt2_i.ready),
	.eci_pkt2_stall_o	(s1_pkt2_i.stall),
	.eci_hdr_o		(s1_pkt_o.hdr),
	.eci_pkt_size_o		(s1_pkt_o.size),
	.eci_pkt_vc_o		(s1_pkt_o.vc),
	.eci_pkt_valid_o	(s1_pkt_o.valid),
	.eci_pkt_ready_i	(s1_pkt_o.ready),
	.eci_pkt_stall_i	(s1_pkt_o.stall)
	);

endmodule // arb_4_ecih

`endif
