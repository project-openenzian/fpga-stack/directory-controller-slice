/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-07-27
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */

`ifndef DP_WR_REQ_SV
`define DP_WR_REQ_SV

import eci_cmd_defs::*;
import eci_dcs_defs::*;

module dp_wr_req #
  (
   parameter ID_WIDTH = 7,
   )  
   (
    input logic 			 clk,
    input logic 			 reset,
    // ECI wr req hdr+data i/f.
    input logic [ECI_WORD_WIDTH-1:0] 	 e_hdr_i,
    input logic [ECI_CL_WIDTH-1:0] 	 e_data_i,
    input logic 			 e_valid_i,
    output logic 			 e_ready_o,
    // Output ECI hdr i/f.
    output logic [ECI_WORD_WIDTH-1:0] 	 eh_hdr_o,
    output logic [DS_DCU_IDX_WIDTH-1:0]  eh_dcu_idx_o,
    output logic 			 eh_valid_o,
    input logic 			 eh_ready_i,
    // Input wr req i/f.
    input logic [MAX_DCU_ID_WIDTH-1:0] 	 wr_req_id_i,
    input logic [DS_ADDR_WIDTH-1:0] 	 wr_req_addr_i,
    input logic [ECI_CL_SIZE_BYTES-1:0]  wr_req_strb_i,
    input logic 			 wr_req_valid_i,
    output logic 			 wr_req_ready_o,
    // Output write descriptor + data i/f.
    output logic [MAX_DCU_ID_WIDTH-1:0]  wr_req_id_o, //7b
    output logic [DS_ADDR_WIDTH-1:0] 	 wr_req_addr_o, //38b 
    output logic [ECI_CL_SIZE_BYTES-1:0] wr_req_strb_o,
    output logic [ECI_CL_WIDTH-1:0] 	 wr_req_data_o,
    output logic 			 wr_req_valid_o,
    input logic 			 wr_req_ready_i
    );

   localparam DATA_STORE_HDR_WIDTH = MAX_DCU_ID_WIDTH + DS_ADDR_WIDTH + ECI_CL_SIZE_BYTES;
   
   
   typedef logic [DS_DCU_ID_WIDTH-1:0] 	    dcu_id_t;
   // DCU ID is split into two parts.
   // LSB - Odd/even offset.
   // MSB to LSB-1 - DCU IDX.
   // Check eci_dcs_defs for more information.
   typedef logic [DS_DCU_IDX_WIDTH-1:0]     dcu_idx_t;

   typedef union packed {
      logic [DATA_STORE_HDR_WIDTH-1:0] flat;
      struct packed {
	 logic [MAX_DCU_ID_WIDTH-1:0] id;
	 logic [DS_ADDR_WIDTH-1:0]    addr;
	 logic [ECI_CL_SIZE_BYTES-1:0] strb; 
      } parts;
   } data_st_hdr_t;
		    
   
   eci_word_t e_hdr_c;
   ds_cl_addr_t e_hdr_addr;
   eci_dmask_t e_hdr_dmask;
   dcu_id_t e_hdr_dcu_id;
   
   // Datapath Gate signals.
   dcu_id_t                              hd_dcu_id_i;
   logic [ECI_DMASK_WIDTH-1:0] 		 hd_dmask_i;
   logic [HDR_WIDTH-1:0] 		 hd_hdr_i;
   logic [ECI_CL_WIDTH-1:0] 		 hd_data_i;
   logic 				 hd_valid_i;
   logic 				 hd_ready_o;
   // header i/f.
   dcu_idx_t                             h_dcu_idx_o;
   logic [HDR_WIDTH-1:0] 		 h_hdr_o;
   logic 				 h_valid_o;
   logic 				 h_ready_i;
   // data i/f.
   dcu_idx_t                             d_dcu_idx_o;
   logic [ECI_CL_WIDTH-1:0] 		 d_data_o;
   logic 				 d_valid_o;
   logic 				 d_ready_i;
   // clear signal to free up dbuf idx.
   dcu_idx_t                             c_dcu_idx_i;
   logic 				 c_dcu_idx_valid_i;

   // dp_wr_ser signals
   dcu_idx_t                             ser_wr_dcu_idx_i;
   logic [CL_WIDTH-1:0] 		 ser_wr_data_i;
   logic 				 ser_wr_valid_i;
   logic 				 ser_wr_ready_o;
   // Serialized write output;
   // 1024 bits split into two 512 bit writes.
   // No flow control downstream.
   logic [DS_DBUF_WR_IDX_WIDTH-1:0] 	 ser_s_wr_addr_o;
   logic [CLB2_WIDTH-1:0] 		 ser_s_wr_data_o;
   logic 				 ser_s_wr_en_o;
   
   // Data store signals.
   data_st_hdr_t dp_ds_hdr;
   

   // Cast input header to eci_word_t to extract
   // dmask information. 
   assign e_hdr_c = eci_word_t'(e_hdr_i);
   assign e_hdr_dmask = e_hdr_c.generic_cmd.dmask;
   // Last 40 bits of header is ECI Cl address.
   // cast it to ds_cl_addr_t to extract DCU ID. 
   assign e_hdr_addr = ds_cl_addr_t'(e_hdr_i[ECI_ADDR_WIDTH-1:0]);
   assign e_hdr_dcu_id = e_hdr_addr.dcu_id;

   // Output assign
   always_comb begin : OUT_ASSIN
      e_ready_o = hd_ready_o;
      eh_hdr_o = h_hdr_o;
      eh_dcu_idx_o = h_dcu_idx_o;
      eh_valid_o = h_valid_o;
   end : OUT_ASSIN
   always_comb begin : ECI_WR_GATE_IP_ASSIGN
      hd_dcu_id_i = e_hdr_dcu_id;
      hd_dmask_i = e_hdr_dmask;
      hd_hdr_i = e_hdr_i;
      hd_data_i = e_data_i;
      hd_valid_i = e_valid_i;
      h_ready_i = eh_ready_i;
      d_ready_i = ser_wr_ready_o;
      c_dcu_idx_i = ;
      c_dcu_idx_valid_i = ;
   end : ECI_WR_GATE_IP_ASSIGN
   
   dp_gate #
     (
      .HDR_WIDTH(ECI_WORD_WIDTH),
      .GEN_MAP_ECID_TO_WRD(1)
      )
   eci_wr_gate
     (
      .clk(clk),
      .reset(reset),
      // Header info + data i/f.
      .hd_dcu_id_i(hd_dcu_id_i),
      .hd_dmask_i(hd_dmask_i),
      .hd_hdr_i(hd_hdr_i),
      .hd_data_i(hd_data_i),
      .hd_valid_i(hd_valid_i),
      .hd_ready_o(hd_ready_o),
      // Output hdr i/f.
      .h_dcu_idx_o(h_dcu_idx_o),
      .h_hdr_o(h_hdr_o),
      .h_valid_o(h_valid_o),
      .h_ready_i(h_ready_i),
      // Output data i/f.
      .d_dcu_idx_o(d_dcu_idx_o),
      .d_data_o(d_data_o),
      .d_valid_o(d_valid_o),
      .d_ready_i(d_ready_i),
      // Clear signal
      .c_dcu_idx_i(c_dcu_idx_i),
      .c_dcu_idx_valid_i(c_dcu_idx_valid_i)
      );

   always_comb begin : ECI_WR_SER_IP_ASSIGN
      ser_wr_dcu_idx_i = d_dcu_idx_o;
      ser_wr_data_i = d_data_o;
      ser_wr_valid_i = d_valid_o;
   end : ECI_WR_SER_IP_ASSIGN
   dp_wr_ser #
     (
      .CL_WIDTH(ECI_CL_WIDTH)
      )
   eci_wr_data_ser
     (
      .clk(clk),
      .reset(reset),
      .wr_dcu_idx_i(ser_wr_dcu_idx_i),
      .wr_data_i(ser_wr_data_i),
      .wr_valid_i(ser_wr_valid_i),
      .wr_ready_o(ser_wr_ready_o),
      .s_wr_addr_o(ser_s_wr_addr_o),
      .s_wr_data_o(ser_s_wr_data_o),
      .s_wr_en_o(ser_s_wr_en_o)
      );

   // Pipeline registers can be attached here.

   // Continue here to instantiate data store.
   
endmodule // dp_wr_req

`endif
