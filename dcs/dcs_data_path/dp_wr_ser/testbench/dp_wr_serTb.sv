import eci_dcs_defs::*;

module dp_wr_serTb();

   parameter CL_WIDTH = 1024;
   parameter CLB2_WIDTH = 512;

   //input output ports 
   //Input signals
   logic 			    clk;
   logic 			    reset;
   logic [DS_DCU_IDX_WIDTH-1:0]     wr_dcu_idx_i;
   logic [CL_WIDTH-1:0] 	    wr_data_i;
   logic 			    wr_valid_i;

   //Output signals
   logic 			    wr_ready_o;
   logic [DS_DBUF_WR_IDX_WIDTH-1:0] s_wr_addr_o;
   logic [CLB2_WIDTH-1:0] 	    s_wr_data_o;
   logic 			    s_wr_en_o;

   logic [CL_WIDTH-1:0] 	    wr_data;
   // pipeline stage
   // wr addr + write data + enable signal.
   localparam PS_DATA_WIDTH = DS_DBUF_WR_IDX_WIDTH+CLB2_WIDTH+1;
   logic [PS_DATA_WIDTH-1:0]     ps_data_i;
   logic [PS_DATA_WIDTH-1:0] 	 ps_data_o;

   // Memory signals.
   logic [DS_DBUF_WR_IDX_WIDTH-1:0] mem_wr_addr_i;
   logic [CLB2_WIDTH-1:0] 	    mem_wr_data_i;
   logic 			    mem_wr_en_i;
   logic [DS_DBUF_RD_IDX_WIDTH-1:0] mem_rd_addr_i;
   logic [CL_WIDTH-1:0] 	    mem_rd_data_o;

   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   dp_wr_ser dp_wr_ser1 (
			 .clk(clk),
			 .reset(reset),
			 .wr_dcu_idx_i(wr_dcu_idx_i),
			 .wr_data_i(wr_data_i),
			 .wr_valid_i(wr_valid_i),
			 .wr_ready_o(wr_ready_o),
			 .s_wr_addr_o(s_wr_addr_o),
			 .s_wr_data_o(s_wr_data_o),
			 .s_wr_en_o(s_wr_en_o)
			 );//instantiation completed

   // Pass write signals through
   // 5 pipeline stages before
   // writing to mem.
   assign ps_data_i = {s_wr_addr_o, s_wr_data_o, s_wr_en_o};
   pipeline_stage #
     (
      .NUM_STAGE(5),
      .DATA_WIDTH(PS_DATA_WIDTH)
      )
   pstages
     (
      .clk(clk),
      .data_i(ps_data_i),
      .data_o(ps_data_o)
      );

   // write to mem.
   assign mem_wr_en_i = ps_data_o[0];
   assign mem_wr_data_i = ps_data_o[CLB2_WIDTH:1];
   assign mem_wr_addr_i = ps_data_o[PS_DATA_WIDTH-1:CLB2_WIDTH+1];
   dp_mem #
     (
      .WR_NUM_REGS(DS_DBUF_DEPTH)
      )
   dp_mem1(
	   .clk(clk),
	   .wr_addr_i(mem_wr_addr_i),
	   .wr_data_i(mem_wr_data_i),
	   .wr_en_i(mem_wr_en_i),
	   .rd_addr_i(mem_rd_addr_i),
	   .rd_data_o(mem_rd_data_o)
	   );

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports 
      clk = '0;
      reset = '1;
      wr_dcu_idx_i = '0;
      wr_data_i = '0;
      wr_valid_i = '0;
      wr_data = '0;
      mem_rd_addr_i = '0;
      ##5;
      reset = 1'b0;
      ##1;
      // There are 32 1024 registers in
      // mem, write and read one by one.
      for( integer i=0; i<32; i=i+1 ) begin
	 wr_data = {512'($urandom_range(1,32)), 512'($urandom_range(1,32))};
	 wr_cl(.addr_i(i), .data_i(wr_data));
	 wait(mem_wr_en_i == 1'b1); // write has passed through pipeline stages.
	 ##2; // 2 cycles to write to memory.
	 rd_mem_2_compare(.addr_i(i), .exp_data_i(wr_data));
	 ##1;
      end 

      #500 $finish;
   end 

   task static wr_cl(
		     input logic [DS_DBUF_WR_IDX_WIDTH-1:0] addr_i,
		     input logic [CL_WIDTH-1:0] 	    data_i
		     );
      wr_dcu_idx_i = addr_i;
      wr_data_i = data_i;
      wr_valid_i = 1'b1;
      wait(wr_valid_i & wr_ready_o);
      ##1;
      wr_valid_i = 1'b0;
      #1;
   endtask //wr_cl

   task static rd_mem_2_compare(
				input logic [DS_DBUF_RD_IDX_WIDTH-1:0] addr_i,
				input logic [CL_WIDTH-1:0] 	       exp_data_i
				);
      mem_rd_addr_i = addr_i;
      #1;
      assert(mem_rd_data_o === exp_data_i) else 
	$fatal(1, "Reading memory failed for addr %d", addr_i);
   endtask //rd_mem

endmodule
