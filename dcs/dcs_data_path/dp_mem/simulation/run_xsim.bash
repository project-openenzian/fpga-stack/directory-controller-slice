#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
$XILINX_VIVADO/data/verilog/src/glbl.v \
../testbench/dp_memTb.sv \
../rtl/dp_mem.sv 

xelab -debug typical -incremental -L xpm worklib.dp_memTb worklib.glbl -s worklib.dp_memTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.dp_memTb 
xsim -R worklib.dp_memTb 
