#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
$XILINX_VIVADO/data/verilog/src/glbl.v \
../testbench/pipeline_stageTb.sv \
../rtl/pipeline_stage.sv 

xelab -debug typical -incremental -L xpm worklib.pipeline_stageTb worklib.glbl -s worklib.pipeline_stageTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.pipeline_stageTb 
xsim -R worklib.pipeline_stageTb 
