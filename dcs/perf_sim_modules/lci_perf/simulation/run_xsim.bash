#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
      $XILINX_VIVADO/data/verilog/src/glbl.v \
      ../rtl/eci_cmd_defs.sv \
      ../rtl/eci_dcs_defs.sv \
      ../testbench/lci_perfTb.sv \
      ../rtl/cli_tput_load_gen.sv \
      ../rtl/lci_perf.sv \
      ../rtl/axis_xpm_fifo.sv \
      ../rtl/cli_lat_load_gen.sv \
      ../rtl/perf_gen_seq_aliased.sv \
      ../rtl/axis_pipeline_stage.sv 

xelab -debug typical -incremental -L xpm worklib.lci_perfTb worklib.glbl -s worklib.lci_perfTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.lci_perfTb 
xsim -gui worklib.lci_perfTb 
