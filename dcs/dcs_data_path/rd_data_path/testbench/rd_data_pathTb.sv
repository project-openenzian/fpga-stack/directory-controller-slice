import eci_cmd_defs::*;
import eci_dcs_defs::*;

module rd_data_pathTb();


   //input output ports 
   //Input signals
   logic 						  clk;
   logic 						  reset;
   logic [MAX_DCU_ID_WIDTH-1:0] 			  rd_rsp_id_i;
   logic [ECI_CL_WIDTH-1:0] 				  rd_rsp_data_i;
   logic 						  rd_rsp_valid_i;
   logic 						  rd_rsp_ready_i;
   logic [ECI_WORD_WIDTH-1:0] 				  rsp_wd_hdr_i;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 			  rsp_wd_pkt_size_i;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 			  rsp_wd_pkt_vc_i;
   logic 						  rsp_wd_pkt_valid_i;
   logic 						  rsp_wd_pkt_ready_i;

   //Output signals
   logic 						  rd_rsp_ready_o;
   logic [DS_DCU_IDX_WIDTH-1:0] 			  rd_rsp_dcu_idx_o;
   logic [MAX_DCU_ID_WIDTH-1:0] 			  rd_rsp_id_o;
   logic 						  rd_rsp_valid_o;
   logic 						  rsp_wd_pkt_ready_o;
   logic [ECI_PACKET_SIZE-1:0][ECI_WORD_WIDTH-1:0] 	  rsp_wd_pkt_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 			  rsp_wd_pkt_size_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 			  rsp_wd_pkt_vc_o;
   logic 						  rsp_wd_pkt_valid_o;

   ds_cl_addr_t rsp_wd_addr_c;
   

   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   rd_data_path rd_data_path1 (
			       .clk(clk),
			       .reset(reset),
			       .rd_rsp_id_i(rd_rsp_id_i),
			       .rd_rsp_data_i(rd_rsp_data_i),
			       .rd_rsp_valid_i(rd_rsp_valid_i),
			       .rd_rsp_ready_o(rd_rsp_ready_o),
			       
			       .rd_rsp_dcu_idx_o(rd_rsp_dcu_idx_o),
			       .rd_rsp_id_o(rd_rsp_id_o),
			       .rd_rsp_valid_o(rd_rsp_valid_o),
			       .rd_rsp_ready_i(rd_rsp_ready_i),
			       
			       .rsp_wd_hdr_i(rsp_wd_hdr_i),
			       .rsp_wd_pkt_size_i(rsp_wd_pkt_size_i),
			       .rsp_wd_pkt_vc_i(rsp_wd_pkt_vc_i),
			       .rsp_wd_pkt_valid_i(rsp_wd_pkt_valid_i),
			       .rsp_wd_pkt_ready_o(rsp_wd_pkt_ready_o),
			       
			       .rsp_wd_pkt_o(rsp_wd_pkt_o),
			       .rsp_wd_pkt_size_o(rsp_wd_pkt_size_o),
			       .rsp_wd_pkt_vc_o(rsp_wd_pkt_vc_o),
			       .rsp_wd_pkt_valid_o(rsp_wd_pkt_valid_o),
			       .rsp_wd_pkt_ready_i(rsp_wd_pkt_ready_i)
			       );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports 
      clk = '0;
      reset = '1;
      rd_rsp_id_i = '0; 
      rd_rsp_data_i = '0;
      rd_rsp_valid_i = '0;
      rsp_wd_pkt_size_i = 'd17;
      rsp_wd_pkt_vc_i = 'd4;
      rsp_wd_pkt_ready_i = '0;
      ##5;
      reset = 1'b0;
      rsp_wd_pkt_ready_i = 1'b1;

      // These tests only check if the
      // signals are correctly generated.
      // function of dp_gen_path is tested in
      // dp_gen_path testbench.
      
      ##5;
      rd_rsp_id_i = 'd4; 
      rd_rsp_data_i = $urandom_range(100,200);
      rd_rsp_valid_i = 1'b1;
      wait(rd_rsp_valid_i & rd_rsp_ready_o);
      ##1;
      rd_rsp_valid_i = 1'b0;

      ##10; // sometime to reach steady state.

      assert(rsp_wd_pkt_o[0] === rsp_wd_hdr_i) else 
	$fatal(1, "Header is wrong.");

      assert(rsp_wd_pkt_o[ECI_PACKET_SIZE-1:1] === rd_rsp_data_i) else 
	$fatal(1,"Data is wrong.");
      
      assert(rsp_wd_pkt_size_o === rsp_wd_pkt_size_i) else
	$fatal(1,"Size is wrong.");

      assert(rsp_wd_pkt_vc_o === rsp_wd_pkt_vc_i) else
	$fatal(1,"VC is wrong.");

      $display("Success");
      
      #500 $finish;
   end

   always_comb begin : IN_ASSIGN
      rsp_wd_addr_c = '0;
      rsp_wd_addr_c.dcu_id = f_maxdcuid_2_dcu_id(rd_rsp_id_o);
      rsp_wd_hdr_i = '0;
      rsp_wd_hdr_i[ECI_ADDR_WIDTH-1:0] = rsp_wd_addr_c;
      rsp_wd_pkt_valid_i = rd_rsp_valid_o;
      rd_rsp_ready_i = rsp_wd_pkt_ready_o;
   end : IN_ASSIGN

endmodule
