
module axis_comb_priority_encTb();

   parameter DATA_WIDTH = 64;
   parameter NUM_IN = 4;
   localparam SEL_WIDTH = $clog2(NUM_IN);
   
   logic [SEL_WIDTH-1:0] 		     p_sel;


   bit clk;
   
   //input output ports 
   //Input signals
   logic [NUM_IN-1:0][DATA_WIDTH-1:0] us_data_i;
   logic [NUM_IN-1:0] 		      us_valid_i;
   logic 			      ds_ready_i;

   //Output signals
   logic [NUM_IN-1:0] 		      us_ready_o;
   logic [DATA_WIDTH-1:0] 	      ds_data_o;
   logic 			      ds_valid_o;


   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   axis_comb_priority_enc 
     axis_comb_priority_enc1 (
			      .us_id_i(),
			      .us_data_i(us_data_i),
			      .us_valid_i(us_valid_i),
			      .ds_ready_i(ds_ready_i),
			      .us_ready_o(us_ready_o),
			      .ds_id_o(),
			      .ds_data_o(ds_data_o),
			      .ds_valid_o(ds_valid_o)
			      );//instantiation completed 
   
   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();
      
      // Initialize Input Ports 
      us_data_i = '0;
      us_valid_i = '0;
      ds_ready_i = '0;
      for( integer i=0; i<NUM_IN; i=i+1 ) begin
	 us_data_i[i] = i+1;
      end
      ##5;
      ds_ready_i = 1'b1;
      ##5;
      us_valid_i = 4'b1001;
      ##1;
      us_valid_i = 4'b1000;
      ##1;
      us_valid_i = 4'b0000;
      
      
      #500 $finish;
   end

   always_comb begin : P_SEL_VAL
      p_sel = '0;
      // bit 0 need not be considered in loop
      // because it is selected by default.
      for( integer i=NUM_IN; i>0; i=i-1 ) begin
	 if(us_valid_i[i] == 1'b1) begin
	    p_sel = SEL_WIDTH'(i);
	 end
      end 
   end : P_SEL_VAL      
endmodule
