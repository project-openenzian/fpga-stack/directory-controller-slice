
import eci_cmd_defs::*;
import eci_dcs_defs::*;

module lci_perfTb();

   parameter LCI_LAT_TPUT = 1;
   parameter NUM_REQS_TO_WAIT = 10;
   parameter NUM_ADDR_TO_ISSUE = 100;
   parameter PERF_REGS_WIDTH = 32;
   parameter FIFO_DEPTH = 16;
   

   //input output ports 
   //Input signals
   logic 				 clk;
   logic 				 reset;
   logic 				 odd_wait_req_valid_i;
   logic 				 even_wait_req_valid_i;
   logic 				 lci_odd_idx_ready_i;
   logic [ECI_WORD_WIDTH-1:0] 		 lcia_odd_idx_hdr_i;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 lcia_odd_idx_size_i;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] lcia_odd_idx_vc_i;
   logic 				 lcia_odd_idx_valid_i;
   logic 				 ul_odd_idx_ready_i;
   logic 				 lci_even_idx_ready_i;
   logic [ECI_WORD_WIDTH-1:0] 		 lcia_even_idx_hdr_i;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 lcia_even_idx_size_i;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] lcia_even_idx_vc_i;
   logic 				 lcia_even_idx_valid_i;
   logic 				 ul_even_idx_ready_i;

   //Output signals
   logic [ECI_WORD_WIDTH-1:0] 		 lci_odd_idx_hdr_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 lci_odd_idx_size_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] lci_odd_idx_vc_o;
   logic 				 lci_odd_idx_valid_o;
   logic 				 lcia_odd_idx_ready_o;
   logic [ECI_WORD_WIDTH-1:0] 		 ul_odd_idx_hdr_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 ul_odd_idx_size_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] ul_odd_idx_vc_o;
   logic 				 ul_odd_idx_valid_o;
   logic [ECI_WORD_WIDTH-1:0] 		 lci_even_idx_hdr_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 lci_even_idx_size_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] lci_even_idx_vc_o;
   logic 				 lci_even_idx_valid_o;
   logic 				 lcia_even_idx_ready_o;
   logic [ECI_WORD_WIDTH-1:0] 		 ul_even_idx_hdr_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 ul_even_idx_size_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] ul_even_idx_vc_o;
   logic 				 ul_even_idx_valid_o;
   logic 				 done_o;
   


   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   lci_perf #
     (
      .LCI_LAT_TPUT(LCI_LAT_TPUT),
      .NUM_REQS_TO_WAIT(NUM_REQS_TO_WAIT),
      .NUM_ADDR_TO_ISSUE(NUM_ADDR_TO_ISSUE),
      .PERF_REGS_WIDTH(PERF_REGS_WIDTH),
      .FIFO_DEPTH(FIFO_DEPTH)
      )
   lci_perf1 (
	      .clk(clk),
	      .reset(reset),
	      .odd_wait_req_valid_i(odd_wait_req_valid_i),
	      .even_wait_req_valid_i(even_wait_req_valid_i),
	      .lci_odd_idx_ready_i(lci_odd_idx_ready_i),
	      .lcia_odd_idx_hdr_i(lcia_odd_idx_hdr_i),
	      .lcia_odd_idx_size_i(lcia_odd_idx_size_i),
	      .lcia_odd_idx_vc_i(lcia_odd_idx_vc_i),
	      .lcia_odd_idx_valid_i(lcia_odd_idx_valid_i),
	      .ul_odd_idx_ready_i(ul_odd_idx_ready_i),
	      .lci_even_idx_ready_i(lci_even_idx_ready_i),
	      .lcia_even_idx_hdr_i(lcia_even_idx_hdr_i),
	      .lcia_even_idx_size_i(lcia_even_idx_size_i),
	      .lcia_even_idx_vc_i(lcia_even_idx_vc_i),
	      .lcia_even_idx_valid_i(lcia_even_idx_valid_i),
	      .ul_even_idx_ready_i(ul_even_idx_ready_i),
	      .lci_odd_idx_hdr_o(lci_odd_idx_hdr_o),
	      .lci_odd_idx_size_o(lci_odd_idx_size_o),
	      .lci_odd_idx_vc_o(lci_odd_idx_vc_o),
	      .lci_odd_idx_valid_o(lci_odd_idx_valid_o),
	      .lcia_odd_idx_ready_o(lcia_odd_idx_ready_o),
	      .ul_odd_idx_hdr_o(ul_odd_idx_hdr_o),
	      .ul_odd_idx_size_o(ul_odd_idx_size_o),
	      .ul_odd_idx_vc_o(ul_odd_idx_vc_o),
	      .ul_odd_idx_valid_o(ul_odd_idx_valid_o),
	      .lci_even_idx_hdr_o(lci_even_idx_hdr_o),
	      .lci_even_idx_size_o(lci_even_idx_size_o),
	      .lci_even_idx_vc_o(lci_even_idx_vc_o),
	      .lci_even_idx_valid_o(lci_even_idx_valid_o),
	      .lcia_even_idx_ready_o(lcia_even_idx_ready_o),
	      .ul_even_idx_hdr_o(ul_even_idx_hdr_o),
	      .ul_even_idx_size_o(ul_even_idx_size_o),
	      .ul_even_idx_vc_o(ul_even_idx_vc_o),
	      .ul_even_idx_valid_o(ul_even_idx_valid_o),
	      .done_o(done_o)
	      );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports 
      clk = '0;
      reset = '1;
      odd_wait_req_valid_i = '0;
      even_wait_req_valid_i = '0;
      ul_odd_idx_ready_i = '0;
      ul_even_idx_ready_i = '0;
      //lci_odd_idx_ready_i = '0;
      //lcia_odd_idx_hdr_i = '0;
      //lcia_odd_idx_size_i = '0;
      //lcia_odd_idx_vc_i = '0;
      //lcia_odd_idx_valid_i = '0;
      //lci_even_idx_ready_i = '0;
      //lcia_even_idx_hdr_i = '0;
      //lcia_even_idx_size_i = '0;
      //lcia_even_idx_vc_i = '0;
      //lcia_even_idx_valid_i = '0;
      ##5;
      reset = 1'b0;
      odd_wait_req_valid_i = '1;
      even_wait_req_valid_i = '1;
      ul_odd_idx_ready_i = '1;
      ul_even_idx_ready_i = '1;
      wait(done_o);
      ##1;
      #500;
      $display("PASS: Odd ch issues odd LCI and odd Unlocks");
      $display("PASS: Even ch issues even LCI and even Unlocks");
      $display("Success: all tests pass");
      //$display("Number of Transactions: %0d", perf_num_tr_done_o);
      //$display("Number of cycles taken: %0d", perf_cyc_st_to_done_o);
      $finish;
   end

   always_comb begin : ODD_LCIA
      lcia_odd_idx_hdr_i = eci_cmd_defs::lcl_gen_lcia(lci_odd_idx_hdr_o);
      lcia_odd_idx_size_i = 'd1;
      lcia_odd_idx_vc_i = VC_LCL_RESP_WO_DATA_E; //18
      lcia_odd_idx_valid_i = lci_odd_idx_valid_o;
      lci_odd_idx_ready_i = lcia_odd_idx_ready_o;
   end : ODD_LCIA

   always_comb begin : EVEN_LCIA
      lcia_even_idx_hdr_i = eci_cmd_defs::lcl_gen_lcia(lci_even_idx_hdr_o);
      lcia_even_idx_size_i = 'd1;
      lcia_even_idx_vc_i = VC_LCL_RESP_WO_DATA_E; //18
      lcia_even_idx_valid_i = lci_even_idx_valid_o;
      lci_even_idx_ready_i = lcia_even_idx_ready_o;
   end : EVEN_LCIA

   // Test: all odd unlocks must have odd CL indices.
   always_ff @(posedge clk) begin : ASSERTIONS
      if(lci_odd_idx_valid_o & lci_odd_idx_ready_i) begin
	 if(lci_odd_idx_hdr_o[ECI_CL_ADDR_LSB] !== 1'b1) begin
	    $fatal(1, "LCI from odd ch does not have odd index.");
	 end
      end
      if(ul_odd_idx_valid_o & ul_odd_idx_ready_i) begin
	 if(ul_odd_idx_hdr_o[ECI_CL_ADDR_LSB] !== 1'b1) begin
	    $fatal(1, "Unlock from odd ch does not have odd index.");
	 end
      end
      if(lci_even_idx_valid_o & lci_even_idx_ready_i) begin
	 if(lci_even_idx_hdr_o[ECI_CL_ADDR_LSB] !== 1'b0) begin
	    $fatal(1, "LCI from even ch does not have even index.");
	 end
      end
      if(ul_even_idx_valid_o & ul_even_idx_ready_i) begin
	 if(ul_even_idx_hdr_o[ECI_CL_ADDR_LSB] !== 1'b0) begin
	    $fatal(1, "Unlock from even ch does not have even index.");
	 end
      end
   end : ASSERTIONS


endmodule
