
module simple_delay_moduleTb();

   parameter DATA_WIDTH = 64;
   parameter SIM_DELAY = 5;

   //input output ports 
   //Input signals
   logic 		  clk;
   logic 		  reset;
   logic [DATA_WIDTH-1:0] us_data_i;
   logic 		  us_valid_i;
   logic 		  ds_ready_i;

   //Output signals
   logic 		  us_ready_o;
   logic [DATA_WIDTH-1:0] ds_data_o;
   logic 		  ds_valid_o;


   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   simple_delay_module simple_delay_module1 (
					     .clk(clk),
					     .reset(reset),
					     .us_data_i(us_data_i),
					     .us_valid_i(us_valid_i),
					     .ds_ready_i(ds_ready_i),
					     .us_ready_o(us_ready_o),
					     .ds_data_o(ds_data_o),
					     .ds_valid_o(ds_valid_o)
					     );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports 
      clk = '0;
      reset = '1;
      //us_data_i = '0;
      us_valid_i = '0;
      ds_ready_i = '0;
      ##5;
      reset = 1'b0;
      
      ##5;
      us_valid_i = 1'b1;
      ds_ready_i = 1'b1;
      

      #500 $finish;
   end 

   always_ff @(posedge clk) begin : REG_ASSIGN
      if(us_valid_i & us_ready_o) begin
	 us_data_i <= $urandom_range(100,200);
      end
      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset 
	 us_data_i <= '0;
      end
   end : REG_ASSIGN
endmodule
