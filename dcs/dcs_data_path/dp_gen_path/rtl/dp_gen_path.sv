`ifndef DP_GEN_PATH_SV
`define DP_GEN_PATH_SV

import eci_cmd_defs::*;
import eci_dcs_defs::*;

//                       Store hdr only          Retrieve hdr only
//                       output I/F to DCUs      input I/F from DCUs
//                            ┌──►                ───┐
//                            │                      │ pipeline stage to ensure 
//                            │                      │ 2 512 beats complete before
//                            │                      │ retrieve req is recd.
//                ┌─────────┐ │          ┌─────────┐ │      ┌────────┐1024b
//                │         ├─┘          │         │ │   │  │        ├─────►
//          ─────►│         │            │ DP_WR   │ └───┼─►│ DP_    │ Retrieve
// Store(ST)Hdr   │ DP_GATE │            │ SER     │     │  │ STORE  │ Hdr + data
//       + Data   │         │  1024b     │         │        │        │ output I/F
//       i/F      │         ├───────────►│         │        │        │
//                │         │ Data to    │         ├──512──►┤        │
//                └─────────┘ Serialize  └─────────┘  x2    └────────┘
//                            into mem
//                                                   Optional
//                                                   pipeline
//                                                   registers
//
// Storing and Retrieving data.
//  * DP_STORE has enough memory to store 1 CL for each DCU in the slice.
//  * DP_GATE splits incoming hdr+data into separate hdr and data channels.
//  * hdr gets sent to DCU and data is stored in the DP_STORE corresponding
//    to the DCU_ID.
//  * eventually when DCU is completed with the header, it sends a
//    retrieve header to DP_STORE along with the DCU ID.
//  * The DCU_ID is used to index into the DP_STORE mem to retrieve data
//    and the retrieve header + data is then sent downstream.
// 
// DP_GATE
//  * Splits input hdr+data channel into separate hdr and data channels.
//  * Ensures each DCU has only 1 on-going data operation per data path.
//  * If incoming data is from ECI, there is an option to map ECI data
//    to its word address indicated by dmask.
//  * Sends header with DCU_IDX to the DCUS.
//  * Sends data with DCU_IDX to the DP_STORE. 
//
// DP_WR_SER
//  * Serializes 1024 bit write data into two cycles of 512 bits each.
//  * Done to ease routing.
//
// DP_STORE
//  * Instantiates memory to store and retrieve data corresponding to DCU_IDX.
//  * writes CL/2 bits each cycle.
//  * reads CL bits in a cycle.
//
// Pipeline stage to retrieve data.
//  * Since data gets written in 2 beats, this pipeline stage
//  * ensures retrieve does not happen before data gets written.

module dp_gen_path #
  (
   parameter STORE_HDR_WIDTH = 64,
   parameter RETRIEVE_HDR_WIDTH = 64,
   parameter GEN_MAP_ECID_TO_WRD = 1 // see DP_GATE.
   )
   (
    input logic 			  clk,
    input logic 			  reset,
    // Store i/f hdr+Data.
    // DCU ID from CL address (check eci_dcs_defs for more info).
    input logic [DS_DCU_ID_WIDTH-1:0] 	  st_dcu_id_i,
    // dmask not used if GEN_Map_ecid_to_wrd == 0
    input logic [ECI_DMASK_WIDTH-1:0] 	  st_dmask_i, 
    input logic [STORE_HDR_WIDTH-1:0] 	  st_hdr_i,
    input logic [ECI_CL_WIDTH-1:0] 	  st_data_i,
    input logic 			  st_valid_i,
    output logic 			  st_ready_o,
    // Output Store heaader i/f.
    // DCU_IDX from CL address. (not DCU_ID)
    // used for routing to DCU within slice. 
    output logic [DS_DCU_IDX_WIDTH-1:0]   sth_dcu_idx_o,
    output logic [STORE_HDR_WIDTH-1:0] 	  sth_hdr_o,
    output logic 			  sth_valid_o,
    input logic 			  sth_ready_i,
    // Input retrieve header i/f.
    // DCU ID that is returned by the DCU.
    // Used to get the DCU_IDX to index into dp_store.
    input logic [DS_DCU_ID_WIDTH-1:0] 	  rth_dcu_id_i,
    input logic [RETRIEVE_HDR_WIDTH-1:0]  rth_hdr_i,
    input logic 			  rth_valid_i,
    output logic 			  rth_ready_o,
    // Output retrieve hdr + data i/f.
    output logic [RETRIEVE_HDR_WIDTH-1:0] rt_hdr_o,
    output logic [ECI_CL_WIDTH-1:0] 	  rt_data_o,
    output logic 			  rt_valid_o,
    input logic 			  rt_ready_i
    );

   typedef logic [DS_DCU_ID_WIDTH-1:0] 	  dcu_id_t;
   // DCU ID is split into two parts.
   // LSB - Odd/even offset.
   // MSB to LSB-1 - DCU IDX.
   // Check eci_dcs_defs for more information.
   typedef logic [DS_DCU_IDX_WIDTH-1:0]     dcu_idx_t;
   typedef logic [DS_DBUF_WR_IDX_WIDTH-1:0] dbuf_wr_idx_t;
   typedef logic [STORE_HDR_WIDTH-1:0] 	    st_hdr_t;
   typedef logic [RETRIEVE_HDR_WIDTH-1:0]   ret_hdr_t;
   typedef logic [ECI_CL_WIDTH-1:0] 	    cl_data_t;
   typedef logic [(ECI_CL_WIDTH/2)-1:0]     clb2_data_t;

   // DP Gate signals.
   // Datapath Gate signals.
   dcu_id_t                              hd_dcu_id_i;
   eci_dmask_t 		                 hd_dmask_i;
   st_hdr_t 		                 hd_hdr_i;
   cl_data_t 		                 hd_data_i;
   logic 				 hd_valid_i;
   logic 				 hd_ready_o;
   // header i/f.
   dcu_idx_t                             h_dcu_idx_o;
   st_hdr_t 		                 h_hdr_o;
   logic 				 h_valid_o;
   logic 				 h_ready_i;
   // data i/f.
   dcu_idx_t                             d_dcu_idx_o;
   cl_data_t 		                 d_data_o;
   logic 				 d_valid_o;
   logic 				 d_ready_i;
   // clear signal to free up dbuf idx.
   dcu_idx_t                             c_dcu_idx_i;
   logic 				 c_dcu_idx_valid_i;

   // dp_wr_ser signals
   dcu_idx_t                             ser_wr_dcu_idx_i;
   cl_data_t 		                 ser_wr_data_i;
   logic 				 ser_wr_valid_i;
   logic 				 ser_wr_ready_o;
   // Serialized write output;
   // 1024 bits split into two 512 bit writes.
   // No flow control downstream.
   dbuf_wr_idx_t 	                 ser_s_wr_addr_o;
   clb2_data_t 	                         ser_s_wr_data_o;
   logic 				 ser_s_wr_en_o;

   // Data store signals. 
   dcu_id_t 		                 m_rd_dcu_id_i;
   ret_hdr_t 		                 m_rd_hdr_i;
   logic 				 m_rd_valid_i;
   logic 				 m_rd_ready_o;
   // Write to datapath mem i/f.
   // write is done in 2 512 bit beats
   dbuf_wr_idx_t 	                 m_wr_addr_i;
   clb2_data_t 		                 m_wr_data_i;
   logic 				 m_wr_en_i;
   // retrieve result i/f.
   // read is done in 1 1024 bit beat.
   ret_hdr_t 		                 m_ret_hdr_o;
   cl_data_t 		                 m_ret_data_o;
   logic 				 m_ret_valid_o;
   logic 				 m_ret_ready_i;
   // Clear signal when data is read.
   dcu_idx_t 	                         m_c_dcu_idx_o;
   logic 				 m_c_dcu_idx_valid_o;

   // Pipeline stage for Input retrieve hdr i/f.
   dcu_id_t                              p_rth_dcu_id_reg;
   ret_hdr_t                             p_rth_hdr_reg;
   logic 				 p_rth_valid_reg;
   logic 				 p_rth_ready;
   
   always_comb begin : OUT_ASSIGN
      st_ready_o = hd_ready_o;
      sth_dcu_idx_o = h_dcu_idx_o;
      sth_hdr_o = h_hdr_o;
      sth_valid_o = h_valid_o;
      rth_ready_o = p_rth_ready;
      rt_hdr_o = m_ret_hdr_o;
      rt_data_o = m_ret_data_o;
      rt_valid_o = m_ret_valid_o;
   end : OUT_ASSIGN

   // DP GATE
   always_comb begin : DP_GATE_ASSIGN
      hd_dcu_id_i = st_dcu_id_i;
      hd_dmask_i = st_dmask_i;
      hd_hdr_i = st_hdr_i;
      hd_data_i = st_data_i;
      hd_valid_i = st_valid_i;
      h_ready_i = sth_ready_i;
      d_ready_i = ser_wr_ready_o;
      c_dcu_idx_i = m_c_dcu_idx_o;
      c_dcu_idx_valid_i = m_c_dcu_idx_valid_o;
   end : DP_GATE_ASSIGN
   dp_gate #
     (
      .HDR_WIDTH(STORE_HDR_WIDTH),
      .GEN_MAP_ECID_TO_WRD(GEN_MAP_ECID_TO_WRD)
      )
   gen_wr_gate1
     (
      .clk(clk),
      .reset(reset),
      // Header info + data i/f.
      .hd_dcu_id_i(hd_dcu_id_i),
      .hd_dmask_i(hd_dmask_i),
      .hd_hdr_i(hd_hdr_i),
      .hd_data_i(hd_data_i),
      .hd_valid_i(hd_valid_i),
      .hd_ready_o(hd_ready_o),
      // Output hdr i/f.
      .h_dcu_idx_o(h_dcu_idx_o),
      .h_hdr_o(h_hdr_o),
      .h_valid_o(h_valid_o),
      .h_ready_i(h_ready_i),
      // Output data i/f.
      .d_dcu_idx_o(d_dcu_idx_o),
      .d_data_o(d_data_o),
      .d_valid_o(d_valid_o),
      .d_ready_i(d_ready_i),
      // Clear signal
      .c_dcu_idx_i(c_dcu_idx_i),
      .c_dcu_idx_valid_i(c_dcu_idx_valid_i)
      );

   // DP WR SER
   always_comb begin : WR_SER_IP_ASSIGN
      ser_wr_dcu_idx_i = d_dcu_idx_o;
      ser_wr_data_i = d_data_o;
      ser_wr_valid_i = d_valid_o;
   end : WR_SER_IP_ASSIGN
   dp_wr_ser #
     (
      .CL_WIDTH(ECI_CL_WIDTH)
      )
   gen_wr_data_ser1
     (
      .clk(clk),
      .reset(reset),
      .wr_dcu_idx_i(ser_wr_dcu_idx_i),
      .wr_data_i(ser_wr_data_i),
      .wr_valid_i(ser_wr_valid_i),
      .wr_ready_o(ser_wr_ready_o),
      .s_wr_addr_o(ser_s_wr_addr_o),
      .s_wr_data_o(ser_s_wr_data_o),
      .s_wr_en_o(ser_s_wr_en_o)
      );

   // Pipeline registers can be added here.
   // one valid ready pipeline is necessary at retrieve 
   // header i/f to avoid issues with reading data 
   // because data is written in 2 beats of 512 bits 
   // and read in 1 beat of 1024 bits.
   vr_pipe_stage #
     (
      .DATA_WIDTH(DS_DCU_ID_WIDTH + RETRIEVE_HDR_WIDTH),
      .NUM_STAGES(3)
       )
   ret_delay_pipe
     (
      .clk(clk),
      .reset(reset),
      .us_data({rth_dcu_id_i, rth_hdr_i}),
      .us_valid(rth_valid_i),
      .us_ready(p_rth_ready),
      .ds_data({p_rth_dcu_id_reg, p_rth_hdr_reg}),
      .ds_valid(p_rth_valid_reg),
      .ds_ready(m_rd_ready_o)
      );
   
   // assign p_rth_ready = ~p_rth_valid_reg | m_rd_ready_o;
   // always_ff @(posedge clk) begin : RTH_PIPE_STAGE
   //    if(p_rth_ready) begin
   // 	 p_rth_dcu_id_reg <= rth_dcu_id_i;
   // 	 p_rth_hdr_reg <= rth_hdr_i;
   // 	 p_rth_valid_reg <= rth_valid_i;
   //    end
   //    if( reset ) begin
   // 	 p_rth_valid_reg <= '0;
   //    end
   // end : RTH_PIPE_STAGE

   // Data store.
   always_comb begin : DP_STORE_IP_ASSIGN
      m_rd_dcu_id_i = p_rth_dcu_id_reg;
      m_rd_hdr_i = p_rth_hdr_reg;
      m_rd_valid_i = p_rth_valid_reg;
      m_wr_addr_i = ser_s_wr_addr_o;
      m_wr_data_i = ser_s_wr_data_o;
      m_wr_en_i = ser_s_wr_en_o;
      m_ret_ready_i = rt_ready_i;
   end : DP_STORE_IP_ASSIGN
   dp_data_store #
     (
      .HDR_WIDTH(RETRIEVE_HDR_WIDTH),
      .RD_DATA_WIDTH(ECI_CL_WIDTH)
      )
   gen_dp_store_1
     (
      .clk(clk),
      .reset(reset),
      // Retrieve req i/f.
      .rd_dcu_id_i(m_rd_dcu_id_i),
      .rd_hdr_i(m_rd_hdr_i),
      .rd_valid_i(m_rd_valid_i),
      .rd_ready_o(m_rd_ready_o),
      // Write to datapath mem i/f.
      .wr_addr_i(m_wr_addr_i),
      .wr_data_i(m_wr_data_i),
      .wr_en_i(m_wr_en_i),
      // Output retrieve result i/f.
      .ret_hdr_o(m_ret_hdr_o),
      .ret_data_o(m_ret_data_o),
      .ret_valid_o(m_ret_valid_o),
      .ret_ready_i(m_ret_ready_i),
      // Clear signal when data is read.
      .c_dcu_idx_o(m_c_dcu_idx_o),
      .c_dcu_idx_valid_o(m_c_dcu_idx_valid_o)
      );
   
endmodule // dp_gen_path

`endif
