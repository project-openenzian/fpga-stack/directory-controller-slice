`ifndef INTERFACE_BUS_ASSIGN_SV
`define INTERFACE_BUS_ASSIGN_SV

module interface_bus_assign ();

   typedef struct packed {
      logic [64-1:0] hdr;
      logic [5-1:0]  size;
      logic [5-1:0]  vc;
      logic 	     valid;
      logic 	     ready;
   } eci_hdr_if_t;
   
   eci_hdr_if_t [4:0] req_i, rsp_o;

   eci_hdr_if_t acr_i;
   eci_hdr_if_t acr_o [3:0];

   genvar i;
   generate
      for( i = 0; i < 5; i++) begin : SIMP_ASSIGN
	 always_comb begin : ASSIGN
	    rsp_o[i].hdr   = req_i[i].hdr;
	    rsp_o[i].size  = req_i[i].size;
	    rsp_o[i].vc    = req_i[i].vc;
	    rsp_o[i].valid = req_i[i].valid;
	    req_i[i].ready = rsp_o[i].ready;
	 end : ASSIGN
      end : SIMP_ASSIGN
   endgenerate


   initial begin
      for( integer i=0; i<5; i=i+1 ) begin
	 req_i[i].hdr = i*100;
	 req_i[i].size = i;
	 req_i[i].vc = i;
	 req_i[i].valid = 1'b1;
	 rsp_o[i].ready = 1'b1;
      end


      acr_i.hdr = $urandom_range(1,100);
      acr_i.size = $urandom_range(1,17);
      acr_i.vc = $urandom_range(2,11);
      acr_i.valid = 1'b1;
      acr_o.ready = '1;
      
      #500;
      $finish;
      
   end

   axis_comb_router #
     (
      .DATA_WIDTH(74),
      .NUM_OUT(4)
       )
   acr1
     (
      .us_data_i({acr_i.hdr, acr_i.size, acr_i.vc}),
      .us_sel_i('d2),
      .us_valid_i(acr_i.valid),
      .us_ready_o(acr_i.ready),
      .ds_data_o({acr_o.hdr, acr_o.size, acr_o.vc}),
      .ds_valid_o(acr_o.valid),
      .ds_ready_i(acr_o.ready),
      );
   
endmodule // interface_bus_assign

`endif
