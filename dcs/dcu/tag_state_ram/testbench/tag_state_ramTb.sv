// Note: when testing test following cases
// OP_REGISTER = 1'b1;
// OP_REGISTER = 1'b0;
module tag_state_ramTb();

   parameter logic [17:0] INVALID_STATE = '0;
   parameter logic [17:0] INVALID_TAG = '1;
   parameter logic 	  OP_REGISTER = 1'b1;
   

   localparam BRAM_ADDR_WIDTH  = 10;
   localparam BRAM_DATA_WIDTH  = 36;
   localparam BRAM_BYTE_WIDTH  = 9;
   localparam BRAM_WSTRB_WIDTH = BRAM_DATA_WIDTH/BRAM_BYTE_WIDTH;
   localparam WAY_ID_WIDTH = 2;
   localparam WAY_WIDTH = BRAM_DATA_WIDTH/2; //18
   
   localparam logic TAG = 1'b0;
   localparam logic STATE = ~TAG;
   localparam logic LO_REGION = 1'b0;
   localparam logic HI_REGION = ~LO_REGION;
   localparam logic ROW_WAY_0 = 1'b0;
   localparam logic ROW_WAY_1 = ~ROW_WAY_0;
   localparam logic ENABLE = 1'b1;
   localparam logic DISABLE = ~ENABLE;
   localparam logic HIT = 1'b1;
   localparam logic MISS = ~HIT;
   localparam logic FREE = 1'b1;
   localparam logic USED = ~FREE;
   localparam logic FULL = 1'b1;
   localparam logic NOT_FULL= ~FULL;
   localparam logic [BRAM_WSTRB_WIDTH-1:0] WE_ROW_WAY_0 = 4'b0011;
   localparam logic [BRAM_WSTRB_WIDTH-1:0] WE_ROW_WAY_1 = 4'b1100;
   localparam logic [BRAM_WSTRB_WIDTH-1:0] WE_DISABLE   = 4'b0000;
   
   


   //input output ports 
   //Input signals
   logic 		  clk;
   logic 		  reset;
   logic [17:0] 	  wr_tag_i;
   logic [17:0] 	  wr_stt_i;
   logic [7:0] 		  wr_set_id_i;
   logic [1:0] 		  wr_way_id_i;
   logic 		  wr_en_i;
   logic [7:0] 		  rd_tag_set_id_i;
   logic [17:0] 	  rd_tag_to_cmp_i;
   logic 		  rd_tag_en_i;
   logic [7:0] 		  rd_stt_set_id_i;
   logic [1:0] 		  rd_stt_way_id_i;
   logic 		  rd_stt_en_i;

   //Output signals
   logic [1:0] 		  rd_way_id_o;
   logic 		  hit_o;
   logic 		  set_id_full_o;
   logic 		  rd_tag_nxm_o;
   logic [19:0] 	  way_id_stt_o;
   logic 		  tsr_busy_o;


   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   tag_state_ram #
     (
      .OP_REGISTER(OP_REGISTER)
      )
   tag_state_ram1 (
		   .clk(clk),
		   .reset(reset),
		   // write tag and state. 
		   .wr_tag_i(wr_tag_i),
		   .wr_stt_i(wr_stt_i),
		   .wr_set_id_i(wr_set_id_i),
		   .wr_way_id_i(wr_way_id_i),
		   .wr_en_i(wr_en_i),
		   // match tag interface.
		   .rd_tag_set_id_i(rd_tag_set_id_i),
		   .rd_tag_to_cmp_i(rd_tag_to_cmp_i),
		   .rd_tag_en_i(rd_tag_en_i),
		   .rd_way_id_o(rd_way_id_o),
		   .hit_o(hit_o),
		   .set_id_full_o(set_id_full_o),
		   .rd_tag_nxm_o(rd_tag_nxm_o),
		   //  read state. 
		   .rd_stt_set_id_i(rd_stt_set_id_i),
		   .rd_stt_way_id_i(rd_stt_way_id_i),
		   .rd_stt_en_i(rd_stt_en_i),
		   .way_id_stt_o(way_id_stt_o),
		   .tsr_busy_o(tsr_busy_o)
		   );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports 
      clk = '0;
      reset = '1;
      wr_tag_i = '0;
      wr_stt_i = '0;
      wr_set_id_i = '0;
      wr_way_id_i = '0;
      wr_en_i = '0;
      rd_tag_set_id_i = '0;
      rd_tag_to_cmp_i = '0;
      rd_tag_en_i = '0;
      rd_stt_set_id_i = '0;
      rd_stt_way_id_i = '0;
      rd_stt_en_i = '0;
      ##5;
      reset = 1'b0;
      wait(tsr_busy_o == 1'b0);
      ##5;

      // Test 1 reading an empty state.
      // HI region, set id 128, row way 0.
      rd_stt(
	     .lo_hi_i(HI_REGION),
	     .set_id_i('d128),
	     .row_way_i(ROW_WAY_0)
	     );
      // The state returned should be zero and way ID should match what is sent.
      assert(way_id_stt_o[19:18] == {HI_REGION, ROW_WAY_0}) else $fatal(1,"Invalid state: Way does not match");
      assert(way_id_stt_o[17:0] == INVALID_STATE) else $fatal(1,"Returned state is not invalid.");
      ##5;

      // Write tag and state.
      // HI region, set 128, way 0.
      // Tag value: 1
      // State value: 100.
      wr_tag_state(
		   .tag_i('d1),
		   .stt_i('d100),
		   .lo_hi_i(HI_REGION),
		   .set_id_i('d128),
		   .row_way_i(ROW_WAY_0)
		   );
      ##5;
      // read back the state from previous address, and check if new state is written.
      rd_stt(
	     .lo_hi_i(HI_REGION),
	     .set_id_i('d128),
	     .row_way_i(ROW_WAY_0)
	     );
      assert(way_id_stt_o[19:18] == {HI_REGION, ROW_WAY_0}) else $fatal(1,"Way does not match 2");
      assert(way_id_stt_o[17:0] == 'd100) else $fatal(1,"Returned state is 100");
      ##5;

      // Write tag and state
      // HI region, set 128, way 1.
      // Tag value : 2
      // State value: 200
      wr_tag_state(
      		   .tag_i('d2),
		   .stt_i('d200),
		   .lo_hi_i(HI_REGION),
		   .set_id_i('d128),
		   .row_way_i(ROW_WAY_1)
		   );
      ##5;
      // write tag and state
      // LO region, set 128, way 0
      // Tag value: 3
      // state value: 300
      wr_tag_state(
      		   .tag_i('d3),
		   .stt_i('d300),
		   .lo_hi_i(LO_REGION),
		   .set_id_i('d128),
		   .row_way_i(ROW_WAY_0)
		   );
      ##5;
      // write tag and state
      // LO region, set 128, way 1
      // Tag value: 4
      // state value: 400
      wr_tag_state(
      		   .tag_i('d4),
		   .stt_i('d400),
		   .lo_hi_i(LO_REGION),
		   .set_id_i('d128),
		   .row_way_i(ROW_WAY_1)
		   );
      ##5;
      
      // Now we have the following
      // HI, set 128, way 0 - 1, 100 (tag, state).
      // HI, set 128, way 1 - 2, 200
      // LO, set 128, way 0 - 3, 300
      // LO, set 128, way 1 - 4, 400
      // Read the tags and check if they match.
      // Tag 1 should hit, way id should be 
      rd_tag(.set_id_i('d128), .tag_to_cmp_i('d1));
      assert_rd_tag_resp(
			 .hit_i(HIT),
			 .nxm_i(1'b0),
			 .full_i(FULL),
			 .way_id_i({HI_REGION, ROW_WAY_0})
			 );
      ##5;
      rd_tag(.set_id_i('d128), .tag_to_cmp_i('d2));
      assert_rd_tag_resp(
			 .hit_i(HIT),
			 .nxm_i(1'b0),
			 .full_i(FULL),
			 .way_id_i({HI_REGION, ROW_WAY_1})
			 );
      ##5;
      rd_tag(.set_id_i('d128), .tag_to_cmp_i('d3));
      assert_rd_tag_resp(
			 .hit_i(HIT),
			 .nxm_i(1'b0),
			 .full_i(FULL),
			 .way_id_i({LO_REGION, ROW_WAY_0})
			 );
      ##5;
      rd_tag(.set_id_i('d128), .tag_to_cmp_i('d4));
      assert_rd_tag_resp(
			 .hit_i(HIT),
			 .nxm_i(1'b0),
			 .full_i(FULL),
			 .way_id_i({LO_REGION, ROW_WAY_1})
			 );
      ##5;

      // Checking MISS.
      // make set 128 tag 3 as invalid
      // write tag and state
      // LO region, set 128, way 0
      // Tag value: 3
      // state value: INVALID_STATE
      wr_tag_state(
      		   .tag_i('d3),
		   .stt_i(INVALID_STATE),
		   .lo_hi_i(LO_REGION),
		   .set_id_i('d128),
		   .row_way_i(ROW_WAY_0)
		   );
      ##5;
      // Tag 3 has been invalidated.
      // LO_REGION, ROW_WAY_0 is now free and should be returned.
      // The set is not full anymore.
      rd_tag(.set_id_i('d128), .tag_to_cmp_i('d3));
      assert_rd_tag_resp(
			 .hit_i(MISS),
			 .nxm_i(1'b0),
			 .full_i(NOT_FULL),
			 .way_id_i({LO_REGION, ROW_WAY_0})
			 );
      ##5;
      // Read state to confirm.
      rd_stt(
	     .lo_hi_i(LO_REGION),
	     .set_id_i('d128),
	     .row_way_i(ROW_WAY_0)
	     );
      assert(way_id_stt_o[19:18] == {LO_REGION, ROW_WAY_0}) else $fatal(1,"Invalid state: Way does not match");
      assert(way_id_stt_o[17:0] == INVALID_STATE) else $fatal(1,"Returned state is not invalid.");
      ##5;

      // Check NXM/
      rd_tag(.set_id_i('d128), .tag_to_cmp_i(INVALID_TAG));
      assert(rd_tag_nxm_o == 1'b1) else $fatal(1,"Error: NXM should be 1 (instance %m)");


      // DCU operation.
      // read set_id_i 64 for tag 'd15
      // This should be a miss and an empty way is returned. 
      rd_tag(.set_id_i('d64), .tag_to_cmp_i('d15));
      assert_rd_tag_resp(
      			 .hit_i(MISS),
      			 .nxm_i(1'b0),
      			 .full_i(NOT_FULL),
      			 .way_id_i({HI_REGION, ROW_WAY_1})
      			 );
      ##5;
      // write new state to returned way.
      wr_tag_state(
      		   .tag_i('d15),
      		   .stt_i('d100),
      		   .lo_hi_i(HI_REGION),
      		   .row_way_i(ROW_WAY_1),
      		   .set_id_i('d64)
      		   );
      ##5;
      // read for tag again.
      rd_tag(.set_id_i('d64), .tag_to_cmp_i('d15));
      assert_rd_tag_resp(
      			 .hit_i(HIT),
      			 .nxm_i(1'b0),
      			 .full_i(NOT_FULL),
      			 .way_id_i({HI_REGION, ROW_WAY_1})
      			 );
      ##5;
      // read state.
      rd_stt(
      	     .lo_hi_i(HI_REGION),
      	     .set_id_i('d64),
      	     .row_way_i(ROW_WAY_1)
      	     );
      assert(way_id_stt_o[19:18] == {HI_REGION, ROW_WAY_1}) else $fatal(1,"Invalid state: Way does not match");
      assert(way_id_stt_o[17:0] == 'd100) else $fatal(1,"Returned state is not invalid.");
      ##5;

      $display("Tests: Completed, all pass.");
      #500 $finish;
   end

   task static wr_tag_state(
			    input logic [17:0] tag_i,
			    input logic [17:0] stt_i,
			    input logic        lo_hi_i,
			    input logic        row_way_i,
			    // {6 bit set, 2 bit set row}
			    input logic [7:0]  set_id_i
			    );
      automatic logic [1:0] my_way_id = gen_way_id(
						   .lo_hi_i(lo_hi_i),
						   .row_way_i(row_way_i)
						   );
      wr_tag_i = tag_i;
      wr_stt_i = stt_i;
      wr_set_id_i = set_id_i;
      wr_way_id_i = my_way_id;
      wr_en_i = 1'b1;
      ##1;
      wr_en_i = 1'b0;
      #1;
   endtask //wr_tag_state

   task static rd_tag(
		      input logic [7:0]  set_id_i,
		      input logic [17:0] tag_to_cmp_i
		      );
      rd_tag_set_id_i = set_id_i;
      rd_tag_to_cmp_i = tag_to_cmp_i;
      rd_tag_en_i = 1'b1;
      ##1;
      rd_tag_en_i = 1'b0;
      #1;
      wait(tsr_busy_o == 1'b0);
      #1;
   endtask // rd_tag

   task static rd_stt(
		      input logic 	lo_hi_i,
		      input logic [7:0] set_id_i,
		      input logic 	row_way_i
		      );
      automatic logic [1:0] my_way_id = gen_way_id(
						   .lo_hi_i(lo_hi_i),
						   .row_way_i(row_way_i)
						   );
      rd_stt_set_id_i = set_id_i;
      rd_stt_way_id_i = my_way_id;
      rd_stt_en_i = 1'b1;
      ##1;
      rd_stt_en_i = 1'b0;
      #1;
      wait(tsr_busy_o == 1'b0);
      #1;
   endtask //rd_stt

   function automatic [1:0] gen_way_id
     (
      input logic lo_hi_i,
      input logic row_way_i
      );
      return({lo_hi_i, row_way_i});
   endfunction : gen_way_id

   task static assert_rd_tag_resp(
				  input logic 	    hit_i,
				  input logic 	    nxm_i,
				  input logic 	    full_i,
				  input logic [1:0] way_id_i
				  );
      assert(hit_o == hit_i) else $fatal(1,"Error: Hit expected does not match actual.");
      assert(rd_tag_nxm_o == nxm_i) else $fatal(1,"Error: nxm expected does not match actual.");
      assert(set_id_full_o == full_i) else $fatal(1,"Error: full.");
      assert(rd_way_id_o == way_id_i) else $fatal(1,"Error: way id.");

   endtask //assert_rd_tag_resp
endmodule
