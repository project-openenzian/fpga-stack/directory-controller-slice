import eci_cmd_defs::*;

module arb_4_ecihTb();


   //input output ports 
   //Input signals
   logic 				    clk;
   logic 				    reset;
   logic [ECI_WORD_WIDTH-1:0] 		    eci_hdr1_i;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	    eci_pkt1_size_i;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0]    eci_pkt1_vc_i;
   logic 				    eci_pkt1_valid_i;
   logic [ECI_WORD_WIDTH-1:0] 		    eci_hdr2_i;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	    eci_pkt2_size_i;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0]    eci_pkt2_vc_i;
   logic 				    eci_pkt2_valid_i;
   logic [ECI_WORD_WIDTH-1:0] 		    eci_hdr3_i;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	    eci_pkt3_size_i;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0]    eci_pkt3_vc_i;
   logic 				    eci_pkt3_valid_i;
   logic [ECI_WORD_WIDTH-1:0] 		    eci_hdr4_i;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	    eci_pkt4_size_i;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0]    eci_pkt4_vc_i;
   logic 				    eci_pkt4_valid_i;
   logic [ECI_WORD_WIDTH-1:0] 		    eci_hdr5_i;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	    eci_pkt5_size_i;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0]    eci_pkt5_vc_i;
   logic 				    eci_pkt5_valid_i;
   logic 				    eci_pkt_ready_i;
   logic 				    eci_pkt_stall_i;

   //Output signals
   logic 				    eci_pkt1_ready_o;
   logic 				    eci_pkt1_stall_o;
   logic 				    eci_pkt2_ready_o;
   logic 				    eci_pkt2_stall_o;
   logic 				    eci_pkt3_ready_o;
   logic 				    eci_pkt3_stall_o;
   logic 				    eci_pkt4_ready_o;
   logic 				    eci_pkt4_stall_o;
   logic 				    eci_pkt5_ready_o;
   logic 				    eci_pkt5_stall_o;
   logic [ECI_WORD_WIDTH-1:0] 		    eci_hdr_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	    eci_pkt_size_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0]    eci_pkt_vc_o;
   logic 				    eci_pkt_valid_o;

   logic 				    eci_pkt_ready_reg = 1'b0, eci_pkt_ready_next;
   logic 				    eci_pkt_stall_reg = 1'b0, eci_pkt_stall_next;
   
   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   arb_4_ecih arb_4_ecih1 (
			   .clk(clk),
			   .reset(reset),
			   .eci_hdr1_i(eci_hdr1_i),
			   .eci_pkt1_size_i(eci_pkt1_size_i),
			   .eci_pkt1_vc_i(eci_pkt1_vc_i),
			   .eci_pkt1_valid_i(eci_pkt1_valid_i),
			   .eci_hdr2_i(eci_hdr2_i),
			   .eci_pkt2_size_i(eci_pkt2_size_i),
			   .eci_pkt2_vc_i(eci_pkt2_vc_i),
			   .eci_pkt2_valid_i(eci_pkt2_valid_i),
			   .eci_hdr3_i(eci_hdr3_i),
			   .eci_pkt3_size_i(eci_pkt3_size_i),
			   .eci_pkt3_vc_i(eci_pkt3_vc_i),
			   .eci_pkt3_valid_i(eci_pkt3_valid_i),
			   .eci_hdr4_i(eci_hdr4_i),
			   .eci_pkt4_size_i(eci_pkt4_size_i),
			   .eci_pkt4_vc_i(eci_pkt4_vc_i),
			   .eci_pkt4_valid_i(eci_pkt4_valid_i),
			   .eci_hdr5_i(eci_hdr5_i),
			   .eci_pkt5_size_i(eci_pkt5_size_i),
			   .eci_pkt5_vc_i(eci_pkt5_vc_i),
			   .eci_pkt5_valid_i(eci_pkt5_valid_i),
			   .eci_pkt_ready_i(eci_pkt_ready_i),
			   .eci_pkt_stall_i(eci_pkt_stall_i),
			   .eci_pkt1_ready_o(eci_pkt1_ready_o),
			   .eci_pkt1_stall_o(eci_pkt1_stall_o),
			   .eci_pkt2_ready_o(eci_pkt2_ready_o),
			   .eci_pkt2_stall_o(eci_pkt2_stall_o),
			   .eci_pkt3_ready_o(eci_pkt3_ready_o),
			   .eci_pkt3_stall_o(eci_pkt3_stall_o),
			   .eci_pkt4_ready_o(eci_pkt4_ready_o),
			   .eci_pkt4_stall_o(eci_pkt4_stall_o),
			   .eci_pkt5_ready_o(eci_pkt5_ready_o),
			   .eci_pkt5_stall_o(eci_pkt5_stall_o),
			   .eci_hdr_o(eci_hdr_o),
			   .eci_pkt_size_o(eci_pkt_size_o),
			   .eci_pkt_vc_o(eci_pkt_vc_o),
			   .eci_pkt_valid_o(eci_pkt_valid_o)
			   );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports 
      clk = '0;
      reset = '0;
      eci_hdr1_i = 'd1;
      eci_pkt1_size_i = 'd1;
      eci_pkt1_vc_i = 'd1;
      //eci_pkt1_valid_i = '1;
      eci_hdr2_i = 'd2;
      eci_pkt2_size_i = 'd2;
      eci_pkt2_vc_i = 'd2;
      //eci_pkt2_valid_i = '1;
      eci_hdr3_i = 'd3;
      eci_pkt3_size_i = 'd3;
      eci_pkt3_vc_i = 'd3;
      //eci_pkt3_valid_i = '1;
      eci_hdr4_i = 'd4;
      eci_pkt4_size_i = 'd4;
      eci_pkt4_vc_i = 'd4;
      //eci_pkt4_valid_i = '1;
      eci_hdr5_i = 'd5;
      eci_pkt5_size_i = 'd5;
      eci_pkt5_vc_i = 'd5;
      //eci_pkt4_valid_i = '1;
      eci_pkt_ready_i = '0;
      eci_pkt_stall_i = '0;
      ##5;
      reset = 1;
      ##5;
      reset = 1'b0;
      ##10;
      // pkt 1 would be selected.
      // stall packet 1.
      stall();
      // pkt 2 would be selected.
      // accept packet 2.
      ready();
      // stall pkt 3
      stall();
      // stall pkt 4
      stall();
      for(integer i = 0; i < 5; i=i+1) begin
	 stall();
      end
      for(integer i = 0; i < 2; i=i+1) begin
	 ready();
      end
      for(integer i = 0; i < 5; i=i+1) begin
	 stall();
      end
      reset_system();
      for(integer i=0; i<4; i = i+1) begin
	 ready();
      end
      #500 $finish;
   end

   always_ff @(posedge clk) begin : REG_ASSIGN
      if(eci_pkt1_valid_i & eci_pkt1_ready_o) begin
	 eci_pkt1_valid_i <= 1'b0;
      end
      if(eci_pkt2_valid_i & eci_pkt2_ready_o) begin
	 eci_pkt2_valid_i <= 1'b0;
      end
      if(eci_pkt3_valid_i & eci_pkt3_ready_o) begin
	 eci_pkt3_valid_i <= 1'b0;
      end
      if(eci_pkt4_valid_i & eci_pkt4_ready_o) begin
	 eci_pkt4_valid_i <= 1'b0;
      end
      if(eci_pkt5_valid_i & eci_pkt5_ready_o) begin
	 eci_pkt5_valid_i <= 1'b0;
      end
      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset 
	 eci_pkt1_valid_i <= '1;
	 eci_pkt2_valid_i <= '1;
	 eci_pkt3_valid_i <= '1;
	 eci_pkt4_valid_i <= '1;
	 eci_pkt5_valid_i <= '1;
      end
   end : REG_ASSIGN

   task static stall();
      wait(eci_pkt_valid_o == 1'b1);
      ##5;
      eci_pkt_stall_i = 1'b1;
      ##1;
      eci_pkt_stall_i = 1'b0;
   endtask //stall

   task static ready();
      wait(eci_pkt_valid_o);
      ##5;
      eci_pkt_ready_i = 1'b1;
      ##1;
      eci_pkt_ready_i = 1'b0;
   endtask //ready

   task static reset_system();
      reset = 1'b1;
      ##5;
      reset = 1'b0;
   endtask //reset_system
endmodule
