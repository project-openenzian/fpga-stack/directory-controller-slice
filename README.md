## Enzian Optimized DCU

For more information on the Enzian shell (of which this is a part),
see the wiki at: 

https://unlimited.ethz.ch/x/3nRjCg


``` 
dcu_tsu: DCU Tag and State unit (directory)
│  
└─── tag_state_ram: One BRAM storing tag and state.
     │  
     └─── ram_td: Dual port RAM.
```
