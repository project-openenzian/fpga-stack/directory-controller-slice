import eci_cmd_defs::*;
import eci_dcs_defs::*;

module cl_inv_loopbackTb();

   parameter NUM_IN_STAGES = 32;
   parameter PERF_REGS_WIDTH = 32;
   

   //input output ports 
   //Input signals
   logic 				 clk;
   logic 				 reset;
   logic [DS_ADDR_WIDTH-1:0] 		 rd_req_addr_i;
   logic 				 rd_req_valid_i;
   logic 				 lcl_fwd_wod_pkt_ready_i;
   logic [ECI_WORD_WIDTH-1:0] 		 lcl_rsp_wod_hdr_i;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 lcl_rsp_wod_pkt_size_i;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] lcl_rsp_wod_pkt_vc_i;
   logic 				 lcl_rsp_wod_pkt_valid_i;
   logic 				 lcl_rsp_wod_pkt_ready_i;

   //Output signals
   logic [ECI_WORD_WIDTH-1:0] 		 lcl_fwd_wod_hdr_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 lcl_fwd_wod_pkt_size_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] lcl_fwd_wod_pkt_vc_o;
   logic 				 lcl_fwd_wod_pkt_valid_o;
   logic 				 lcl_rsp_wod_pkt_ready_o;
   logic [ECI_WORD_WIDTH-1:0] 		 lcl_rsp_wod_hdr_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 lcl_rsp_wod_pkt_size_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] lcl_rsp_wod_pkt_vc_o;
   logic 				 lcl_rsp_wod_pkt_valid_o;
   logic [PERF_REGS_WIDTH-1:0] 		 perf_num_valid_rd_req_o;
   logic [PERF_REGS_WIDTH-1:0] 		 perf_num_lci_issued_o;
   
   eci_word_t lci_cmd;
   
   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   assign lci_cmd.eci_word = lcl_fwd_wod_hdr_o;
   
   cl_inv_loopback #
     (
      .NUM_IN_STAGES(NUM_IN_STAGES)
      )
   cl_inv_loopback1 (
		     .clk(clk),
		     .reset(reset),
		     // Sampled addr to be invalidated.
		     .rd_req_addr_i(rd_req_addr_i),
		     .rd_req_valid_i(rd_req_valid_i),
		     // LCI header sent output. 
		     .lcl_fwd_wod_hdr_o(lcl_fwd_wod_hdr_o),
		     .lcl_fwd_wod_pkt_size_o(lcl_fwd_wod_pkt_size_o),
		     .lcl_fwd_wod_pkt_vc_o(lcl_fwd_wod_pkt_vc_o),
		     .lcl_fwd_wod_pkt_valid_o(lcl_fwd_wod_pkt_valid_o),
		     .lcl_fwd_wod_pkt_ready_i(lcl_fwd_wod_pkt_ready_i),
		     // LCIA input. 
		     .lcl_rsp_wod_hdr_i(lcl_rsp_wod_hdr_i),
		     .lcl_rsp_wod_pkt_size_i(lcl_rsp_wod_pkt_size_i),
		     .lcl_rsp_wod_pkt_vc_i(lcl_rsp_wod_pkt_vc_i),
		     .lcl_rsp_wod_pkt_valid_i(lcl_rsp_wod_pkt_valid_i),
		     .lcl_rsp_wod_pkt_ready_o(lcl_rsp_wod_pkt_ready_o),
		     // Unlock output. 
		     .lcl_rsp_wod_hdr_o(lcl_rsp_wod_hdr_o),
		     .lcl_rsp_wod_pkt_size_o(lcl_rsp_wod_pkt_size_o),
		     .lcl_rsp_wod_pkt_vc_o(lcl_rsp_wod_pkt_vc_o),
		     .lcl_rsp_wod_pkt_valid_o(lcl_rsp_wod_pkt_valid_o),
		     .lcl_rsp_wod_pkt_ready_i(lcl_rsp_wod_pkt_ready_i),
		     //perf
		     .perf_num_valid_rd_req_o(perf_num_valid_rd_req_o),
		     .perf_num_lci_issued_o(perf_num_lci_issued_o)
		     );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports 
      clk = '0;
      reset = '1;
      //rd_req_addr_i = '0;
      //rd_req_valid_i = '0;
      //lcl_fwd_wod_pkt_ready_i = '0;
      //lcl_rsp_wod_hdr_i = '0;
      //lcl_rsp_wod_pkt_size_i = '0;
      //lcl_rsp_wod_pkt_vc_i = '0;
      //lcl_rsp_wod_pkt_valid_i = '0;
      lcl_rsp_wod_pkt_ready_i = '0;

      ##5;
      reset = 1'b0;
      lcl_rsp_wod_pkt_ready_i = 1'b1;

      #1000 $finish;
   end

   always_ff @(posedge clk) begin : REG_ASSIGN
      rd_req_addr_i <= rd_req_addr_i + 'd128;
      rd_req_valid_i <= 1;
      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset
	 rd_req_addr_i <= '0;
	 rd_req_valid_i <= '0;
      end
   end : REG_ASSIGN

   always_comb begin : LCIA_GEN
      lcl_rsp_wod_hdr_i = eci_cmd_defs::lcl_gen_lcia(
						     .req_i(lcl_fwd_wod_hdr_o)
						     );
      lcl_rsp_wod_pkt_size_i = 'd1;
      if(lcl_rsp_wod_hdr_o[ECI_CL_ADDR_LSB] == 1'b1) begin
	 // Odd cl index, even VC.
	 lcl_rsp_wod_pkt_vc_i = VC_LCL_RESP_WO_DATA_E;
      end else begin
	 lcl_rsp_wod_pkt_vc_i = VC_LCL_RESP_WO_DATA_O;
      end

      lcl_rsp_wod_pkt_valid_i = lcl_fwd_wod_pkt_valid_o;
      lcl_fwd_wod_pkt_ready_i = lcl_rsp_wod_pkt_ready_o;
   end : LCIA_GEN

   always_ff @(posedge clk) begin : ASSERTS
      if(lcl_fwd_wod_pkt_valid_o & lcl_fwd_wod_pkt_ready_i) begin
	 assert(lci_cmd.lcl_clean_inv.opcode === LCL_CMD_MFWD_CLEAN_INV) else
	   $fatal(1,"Fwd opcode is not clean invalidate.");
      end
   end : ASSERTS

endmodule
