
module dp_memTb();

   parameter WR_NUM_REGS = 64;
   parameter RD_NUM_REGS = WR_NUM_REGS/2; 
   parameter WR_IDX_WIDTH = $clog2(WR_NUM_REGS); 
   parameter RD_CL_IDX_WIDTH = $clog2(RD_NUM_REGS); 
   parameter RD_DATA_WIDTH = 1024;
   parameter WR_DATA_WIDTH = 512;

   //input output ports 
   //Input signals
   logic 			clk;
   logic [WR_IDX_WIDTH-1:0] 	wr_addr_i;
   logic [WR_DATA_WIDTH-1:0] 	wr_data_i;
   logic 			wr_en_i;
   logic [RD_CL_IDX_WIDTH-1:0] 	rd_addr_i;
   logic 			rd_en_i;
   

   //Output signals
   logic [RD_DATA_WIDTH-1:0] 	rd_data_o;
   logic [RD_DATA_WIDTH-1:0] 	wr_data; // full CL.
   
   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   dp_mem dp_mem1 (
		   .clk(clk),
		   .wr_addr_i(wr_addr_i),
		   .wr_data_i(wr_data_i),
		   .wr_en_i(wr_en_i),
		   .rd_addr_i(rd_addr_i),
		   .rd_en_i(rd_en_i),
		   .rd_data_o(rd_data_o)
		   );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports 
      clk = '0;
      wr_addr_i = '0;
      wr_data_i = '0;
      wr_en_i = '0;
      rd_addr_i = '0;
      rd_en_i = '0;
      ##5;

      for( integer i=0; i<RD_NUM_REGS; i=i+1 ) begin
	 wr_data = {512'((i*10)+1), 512'(i*10)};
	 wr_cl(.addr_i(i), .data_i(wr_data));
	 ##1;
	 rd_cl(.addr_i(i));
	 assert(rd_data_o === wr_data) else 
	   $fatal(1,"Rd data does not match: Expected: %d, Got %d.", wr_data, rd_data_o);
	 ##1;
      end
      $display("All tests completed");
      #500 $finish;
   end 

   // write CL in 2 beats.
   task static wr_cl(
		     input logic [RD_CL_IDX_WIDTH-1:0] addr_i,
		     input logic [RD_DATA_WIDTH-1:0] data_i
		     );
      wr_addr_i = {addr_i, 1'b0};
      wr_data_i = data_i[511:0];
      wr_en_i = 1'b1;
      ##1;
      wr_addr_i = {addr_i, 1'b1};
      wr_data_i = data_i[1023:512];
      wr_en_i = 1'b1;
      ##1;
      wr_en_i = 1'b0;
      #1;
   endtask //wr_cl

   task static rd_cl(
		     input logic [RD_CL_IDX_WIDTH-1:0] addr_i
		     );
      rd_addr_i = addr_i;
      rd_en_i = 1'b1;
      ##1;
      rd_en_i = 1'b0;
      #1;
   endtask //rd_cl
endmodule
