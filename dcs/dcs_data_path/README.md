## Generic Data Path. 
* dp_gen_path - top level.
  * dp_gate
    * map_ecid_to_wrd.
  * dp_wr_ser
  * dp_data_store
    * dp_mem

## Verification of generic data path.
* dp_gen_path/testbench/dp_gen_pathTb.sv
* Run dp_gen_path/simulation/run_xsim.bash
* Verifies only the generic data path,
individual testbenches available for each sub module.
  