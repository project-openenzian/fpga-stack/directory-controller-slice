`ifndef DIST_RAM_SV
`define DIST_RAM_SV

module dist_ram (
		 input logic 	       clk,
		 // Port A: write port.
		 input logic [5:0]     addra,
		 input logic [511:0]   dia,
		 input logic 	       ena,
		 input logic 	       wea,
		 // Port B: read port.
		 input logic [4:0]     addrb,
		 input logic 	       enb,
		 output logic [1023:0] dob
		 
		 );


   (* ram_style = "distributed" *) logic [511:0] ram [64:0];
   logic [511:0] 		       dob_lo, dob_hi;

   assign dob = {dob_hi, dob_lo};

   always_ff @(posedge clk) begin : WRITE_PORT_A
      if(ena) begin
	 if(wea) begin
	    ram[addra] <= dia;
	 end
      end
   end : WRITE_PORT_A

   always_ff @(posedge clk) begin : READ_PORT_B
      if(enb) begin
	 dob_lo <= ram[{addrb,1'b0}];
	 dob_hi <= ram[{addrb,1'b1}];
      end
   end : READ_PORT_B
endmodule // dist_ram

`endif
