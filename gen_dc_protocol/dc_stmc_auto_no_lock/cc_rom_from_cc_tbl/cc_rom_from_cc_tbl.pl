#!/usr/bin/perl

# Takes the ECI_CC.csv file as input and generates
# 1. eci_cc_defs.sv - contains the state and protocol definitions.
# 2. eci_cc_table.sv - system verilog ROM that contains the state transitions.
# Files are generated in the current working directory using the template files
#  eci_cc_table.tmpl, eci_cc_defs.tmpl

# Usage
# ./cc_rom_from_cc_tbl.pl /path/to/ECI_CC.csv

no autovivification;
use warnings;
use strict;
use Data::Dumper;
use Getopt::Long;
use POSIX qw(log10 ceil strftime);

if($#ARGV < 0){
    print("$0 /path/to/ECI_CC.csv\n");
    exit;
}
my $csv = $ARGV[0];
if((!-f $csv) || (! defined $csv)){
    print("Cannot find ECI_CC.csv file input\n");
}

my $eci_cc_defs_tmpl = "eci_cc_defs.tmpl";
my $eci_cc_defs_out = "eci_cc_defs.sv";
my $eci_cc_rom_tmpl = "eci_cc_table.tmpl";
my $eci_cc_rom_out = "eci_cc_table.sv";
my $states_t_hash;
my $states_prop_hash;
my $reqs_prop_hash;
my $action_t_hash;
my $req_t_hash;
my $ns_acn_hash;

my $repl_with_hash = {};

my $local_date = strftime "%b %e %Y", localtime;
&read_cc_csv($csv);
&get_state_properties();
&get_req_properties();
&print_cc_pkg($eci_cc_defs_tmpl, $eci_cc_defs_out);
&print_cc_rom($eci_cc_rom_tmpl, $eci_cc_rom_out);
print "ECI CC defs written to $eci_cc_defs_out\n";
print "ECI CC rom written to $eci_cc_rom_out\n";


sub read_cc_csv {
    my $f = $_[0];
    my $first_line_flag = 1;
    my @reqs = ();
    $reqs[0] = "UNKNOWN_REQ";
    $req_t_hash->{$reqs[0]} = 0;
    open(RD,"<$f") or die "Cannot open $f to read";
    while(<RD>){
	chomp;
	next if (/^\s*$/);
	my $line = $_;
	my @csv_e = split(/\s*,\s*/,$line);
	if($first_line_flag){
	    # Read all request types 
	    $first_line_flag = 0;
	    for(my $i=1; $i <= $#csv_e; $i++){
		$req_t_hash->{$csv_e[$i]} = 1;
		$reqs[$i] = $csv_e[$i];
	    }
	    # these are forwards from the FPGA 
	    # they are also requests but do not appear on the CC table
	    # adding them manually
	    $req_t_hash->{"F21"} = 1;
	    $req_t_hash->{"F31"} = 1;
	    $req_t_hash->{"F32"} = 1;
	    # no need to add these to the req array as they
	    # dont appear on the first row 
	    
	} else {
	    # read a line from CSV
	    # First element is the state
	    my $present_state = &rename_state($csv_e[0]);
	    my $present_req = $reqs[0]; #unknown 
	    $states_t_hash->{$present_state} = 1;
	    if($present_state eq &rename_state("1:1")){
		# Default enumeration value of 0 
		$states_t_hash->{$present_state} = 0;
	    }

	    #cycle through each element in table 
	    for(my $i=1; $i<=$#csv_e; $i++){
		# get the present request
		$present_req = $reqs[$i];
		
		# Read table element 
		my $enc_ns_action = $csv_e[$i];
		
		#default next values 
		my $next_state = $present_state;
		my $next_action = "NO_ACTION";

		# Determine next state and action
		if( $enc_ns_action =~ /stall/i){
		    $next_action = "STALL";
		} elsif( $enc_ns_action eq "X"){
		    $next_action = "NOT_ALLOWED";
		} else {
		    my @enc_e = split(/\s*;\s*/, $enc_ns_action);
		    $next_state = &rename_state($enc_e[0]);
		    
		    my $lcl_acn = $enc_e[1];
		    my $rmt_acn = $enc_e[2];
		    
		    if($lcl_acn eq "-:-" && $rmt_acn eq "-:-"){
			$next_action = "NO_ACTION";
		    } elsif ($lcl_acn eq "-:-"){
			$next_action = $rmt_acn;
		    } else {
			$next_action = $lcl_acn;
		    }
		    #reformat the next action 
		    $next_action =~ s/-//g;
		    $next_action =~ s/://g;
		    $next_action =~ s/\s+/_/g;
		    $next_action = uc($next_action);
		    # add to action types
		} #next state and action determined
		
		if($next_action eq "NO_ACTION"){
		    # 0 indicates the default enum value of 0
		    $action_t_hash->{$next_action} = 0;
		} else {
		    $action_t_hash->{$next_action} = 1;
		}

		# store next state and action for given present state and request 
		$ns_acn_hash->{$present_state}->{$present_req}->{"next_state"} = $next_state;
		$ns_acn_hash->{$present_state}->{$present_req}->{"next_action"} = $next_action;
	    } #cycle through each req for given ps
	    # To debug if it matches with the table 
	    # print "$present_state\n";
	    # foreach my $pr (sort keys %{$ns_acn_hash->{$present_state}}){
	    # 	print "$pr, $ns_acn_hash->{$present_state}->{$pr}->{next_state}, $ns_acn_hash->{$present_state}->{$pr}->{next_action}\n";
	    # }
	    # getc();
	} # completed read line from CSV
    }
    close(RD);
}

sub get_state_properties{
    foreach my $state (keys %{$states_t_hash}){
	# property - invalid 
	#1:1 is invalid
	if($state eq &rename_state("1:1")){
	    $states_prop_hash->{$state}->{invalid} = 1;
	} else {
	    $states_prop_hash->{$state}->{invalid} = 0;
	}

	#property evictable
	#1:2 and 1:3 are evictable
	if($state eq &rename_state("1:2") || $state eq &rename_state("1:3")){
	    $states_prop_hash->{$state}->{evictable} = 1;
	} else {
	    $states_prop_hash->{$state}->{evictable} = 0;
	}

	#property eviction in progress will be invalid soon 
	# anything with final state 1
	# indicated by ":1" string match
	my $st_c = &rename_state(":1");
	$st_c =~ s/^s//;
	if($state =~ /$st_c/){
	    $states_prop_hash->{$state}->{evcn_in_prgrs} = 1;
	} else {
	    $states_prop_hash->{$state}->{evcn_in_prgrs} = 0;
	}	
    }
}

sub get_req_properties {
    foreach my $req (keys %{$req_t_hash}){
	if($req eq "R12" || $req eq "R13" || $req eq "RR" || $req eq "RW" || $req eq "R23"){
	    $reqs_prop_hash->{$req}->{creates_new_rtg_entry} = 1;
	} else {
	    $reqs_prop_hash->{$req}->{creates_new_rtg_entry} = 0;
	}
	if( grep(/^$req$/, ("A11","A21","A22","A31d","A32d","A31","A32")) ){
	    $reqs_prop_hash->{$req}->{"rsp2req"} = 1;
	} else {
	    $reqs_prop_hash->{$req}->{"rsp2req"} = 0;
	}
    }
}

sub print_cc_pkg{
    my $cc_def_tmpl = shift;
    my $out_cc_def = shift;
    
    open(CC_DEF_TMPL, "<$cc_def_tmpl") or die "Cannot open $cc_def_tmpl\n";
    open(CC_DEF_OUT, ">$out_cc_def") or die "Cannot open $out_cc_def to write\n";
    while(<CC_DEF_TMPL>){
	chomp;
	my $line = $_;
	my $str_to_repl = "";
	my $repl_with = "";
	if($line =~ /__(\S+)__/){
	    $str_to_repl = $1;
	    # perl doesnt have case why???
	    if($str_to_repl eq "script"){
		$repl_with = $0;
	    } elsif( $str_to_repl eq "num_cc_req"){
		$repl_with = keys %{$req_t_hash};
	    } elsif( $str_to_repl eq "num_state"){
		$repl_with = keys %{$states_t_hash};
	    } elsif( $str_to_repl eq "num_action"){
		$repl_with = keys %{$action_t_hash};
	    } elsif ( $str_to_repl eq "enum_cc_req"){
		$repl_with = gen_enum_string($req_t_hash);
	    } elsif ( $str_to_repl eq "enum_action"){
	     	$repl_with = gen_enum_string($action_t_hash);
	    } elsif ( $str_to_repl eq "enum_state"){
	     	$repl_with = gen_enum_string($states_t_hash);
	    } elsif ( $str_to_repl eq "creates_new_rtg_entry"){
	     	$repl_with = get_prop_string($reqs_prop_hash, "creates_new_rtg_entry", 1);
	    } elsif ( $str_to_repl eq "evictable"){
	     	$repl_with = get_prop_string($states_prop_hash, "evictable", 1);
	    } elsif ( $str_to_repl eq "not_evcn_in_prgrs"){
	     	$repl_with = get_prop_string($states_prop_hash, "evcn_in_prgrs", 0);
	    } elsif ( $str_to_repl eq "invalid_state"){
		$repl_with = &rename_state("1:1");
	    } elsif ( $str_to_repl eq "date"){
		$repl_with = $local_date;
	    } elsif ( $str_to_repl eq "rsp2req"){
		$repl_with = get_prop_string($reqs_prop_hash, "rsp2req", 1);
	    } elsif($str_to_repl eq "_"){
		# part of danger symbol
		# ignore 
		$repl_with = "_____";
	    } else {
		print "CC DEF - $str_to_repl replacement not defined\n";
		exit;
	    }
	}
	$line =~ s/__${str_to_repl}__/$repl_with/;
	print CC_DEF_OUT "$line\n";
    }
    close(CC_DEF_OUT);
    close(CC_DEF_TMPL);
}

sub print_cc_rom{
    my $cc_rom_tmpl = shift;
    my $cc_rom_out = shift;
    open(CC_ROM_TMPL,"<$cc_rom_tmpl") or die "Cannot open $cc_rom_tmpl to read\n";
    open(CC_ROM_OUT, ">$cc_rom_out") or die "Cannot open $cc_rom_out to write\n";
    my $state_loop_flag = 0;
    my $req_loop_flag = 0;
    my @state_before = ();
    my @state_after = ();
    my @req_data = ();
    
    while(<CC_ROM_TMPL>){
	chomp;
	my $line = $_;
	my $str_to_repl = "";
	my $repl_with = "";
	my $print_ugly_text = 0;
	if($print_ugly_text == 0){
	    if($line =~ /__(\S+)__/){
		$str_to_repl = $1;
		# perl doesnt have case why???
		if($str_to_repl eq "script"){
		    $repl_with = $0;
		} elsif ( $str_to_repl eq "ugly_text"){
		    $print_ugly_text = 1;
		} elsif ($str_to_repl eq "init_state"){
		    $repl_with = &rename_state("1:1");
		} elsif ( $str_to_repl eq "date"){
		    $repl_with = $local_date;
		} elsif($str_to_repl eq "_"){
		    # part of danger symbol
		    # ignore 
		    $repl_with = "_____";
		} else {
		    print "CCROM $str_to_repl replacement not defined\n";
		    exit;
		}
		
		$line =~ s/__${str_to_repl}__/$repl_with/;
	    }
	}
	if($print_ugly_text){
	    foreach my $present_state (sort keys %{$states_t_hash}){
		print CC_ROM_OUT "$present_state: begin\n";
		print CC_ROM_OUT "case(curr_req_i)\n";
		foreach my $present_req (sort keys %{$req_t_hash}){
		    next if($present_req eq "UNKNOWN_REQ");
		    # ignore forwards as they are sent by FPGA and do not
		    # appear on the first row of the CC table
		    next if($present_req eq "F21");
		    next if($present_req eq "F31");
		    next if($present_req eq "F32");
		    if(!defined $ns_acn_hash->{$present_state}->{$present_req}){
			print "Error no event $present_state, $present_req\n";
			exit;
		    }
		    my $next_state = $ns_acn_hash->{$present_state}->{$present_req}->{"next_state"};
		    my $next_action = $ns_acn_hash->{$present_state}->{$present_req}->{"next_action"};
		    print CC_ROM_OUT "$present_req: begin\n";
		    print CC_ROM_OUT "next_state_reg <= $next_state\;\n";
		    print CC_ROM_OUT "next_action_reg <= $next_action\;\n";
		    print CC_ROM_OUT "end // $present_req\n";
		}
		print CC_ROM_OUT "default: begin\n";
		print CC_ROM_OUT "next_state_reg <= $present_state\;\n";
		print CC_ROM_OUT "next_action_reg <= NOT_ALLOWED\;\n";
		print CC_ROM_OUT "end\n";
		print CC_ROM_OUT "endcase // case (curr_req_i)\n";
		print CC_ROM_OUT "end // $present_state\n";
	    }
	    $print_ugly_text = 0;
	}
	if($print_ugly_text == 0){
	    print CC_ROM_OUT "$line\n";
	}
    }
    close(CC_ROM_OUT);
    close(CC_ROM_TMPL);
}
sub get_prop_string{
    my $prop_hash = shift;
    my $prop = shift;
    my $val = shift;
    my $prop_string = "";
    foreach my $k (sort keys %{$prop_hash}){
	if(!defined $prop_hash->{$k}->{$prop}){
	    print "Property $prop not found\n";
	    exit;
	}
	if($prop_hash->{$k}->{$prop} == $val){
	    $prop_string .= "\t$k,\n";
	}
    }
    chomp($prop_string);
    $prop_string =~ s/,$//;
    return($prop_string);
}

sub gen_enum_string{
    my $kv_h = shift;
    my $enum_string = "";
    #print the zero key first 
    foreach my $key (sort keys %{$kv_h}){
	if($kv_h->{$key} == 0){
	    $enum_string = "$key = 0,\n";
	    last;
	}
    }
    #print non zero keys
    foreach my $key (sort keys %{$kv_h}){
	if($kv_h->{$key} == 1){
	    $enum_string = $enum_string."\t$key,\n";
	}
    }
    $enum_string =~ s/,$//;
    chomp($enum_string);
    return($enum_string);
}


sub rename_state{
    my $state = shift;
    $state = "s".$state;
    $state =~ s/:/__/;
    return($state);
}

