#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
      $XILINX_VIVADO/data/verilog/src/glbl.v \
      ../rtl/eci_dcs_defs.sv \
      ../testbench/dcs_rr_arbTb.sv \
      ../rtl/dcs_rr_arb.sv \
      ../rtl/axis_comb_rr_arb.sv \
      ../rtl/axis_pipeline_stage.sv

xelab -debug typical -incremental -L xpm worklib.dcs_rr_arbTb worklib.glbl -s worklib.dcs_rr_arbTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.dcs_rr_arbTb 
xsim -gui worklib.dcs_rr_arbTb 
