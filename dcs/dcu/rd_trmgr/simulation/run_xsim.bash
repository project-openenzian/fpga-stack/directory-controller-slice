#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
$XILINX_VIVADO/data/verilog/src/glbl.v \
../testbench/rd_trmgrTb.sv \
../rtl/rd_trmgr.sv 

xelab -debug typical -incremental -L xpm worklib.rd_trmgrTb worklib.glbl -s worklib.rd_trmgrTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.rd_trmgrTb 
xsim -R worklib.rd_trmgrTb 
