import eci_cmd_defs::*;
import eci_dcs_defs::*;
import eci_dirc_defs::*; 
import eci_cc_defs::*;   

module dcs_dcusTb();

   parameter PERF_REGS_WIDTH = 64;
   parameter SYNTH_PERF_REGS = 0;

   //input output ports 
   //Input signals
   logic 			     clk;
   logic 			     reset;
   logic [ECI_WORD_WIDTH-1:0] 	     req_wod_hdr_i;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] req_wod_pkt_size_i;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] req_wod_pkt_vc_i;
   logic 				 req_wod_pkt_valid_i;
   logic [ECI_WORD_WIDTH-1:0] 		 rsp_wod_hdr_i;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 rsp_wod_pkt_size_i;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] rsp_wod_pkt_vc_i;
   logic 				 rsp_wod_pkt_valid_i;
   logic [ECI_WORD_WIDTH-1:0] 		 rsp_wd_hdr_i;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 rsp_wd_pkt_size_i;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] rsp_wd_pkt_vc_i;
   logic 				 rsp_wd_pkt_valid_i;
   logic 				 rsp_wod_pkt_ready_i;
   logic 				 rsp_wd_pkt_ready_i;
   logic 				 fwd_wod_pkt_ready_i;
   logic 				 lcl_rsp_wod_pkt_ready_i;
   logic [ECI_WORD_WIDTH-1:0] 		 lcl_fwd_wod_hdr_i;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 lcl_fwd_wod_pkt_size_i;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] lcl_fwd_wod_pkt_vc_i;
   logic 				 lcl_fwd_wod_pkt_valid_i;
   logic [ECI_WORD_WIDTH-1:0] 		 lcl_rsp_wod_hdr_i;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 lcl_rsp_wod_pkt_size_i;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] lcl_rsp_wod_pkt_vc_i;
   logic 				 lcl_rsp_wod_pkt_valid_i;


   
   logic 				 rd_req_ready_i;
   logic [MAX_DCU_ID_WIDTH-1:0] 	 rd_rsp_id_i;
   logic 				 rd_rsp_valid_i;
   logic 				 wr_req_ready_i;
   logic [MAX_DCU_ID_WIDTH-1:0] 	 wr_rsp_id_i;
   logic [1:0] 				 wr_rsp_bresp_i;
   logic 				 wr_rsp_valid_i;

   //Output signals
   logic 				 req_wod_pkt_ready_o;
   logic 				 rsp_wod_pkt_ready_o;
   logic 				 rsp_wd_pkt_ready_o;
   logic 				 lcl_fwd_wod_pkt_ready_o;
      logic 				 lcl_rsp_wod_pkt_ready_o;
   logic [ECI_WORD_WIDTH-1:0] 		 rsp_wod_hdr_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 rsp_wod_pkt_size_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] rsp_wod_pkt_vc_o;
   logic 				 rsp_wod_pkt_valid_o;
   logic [ECI_WORD_WIDTH-1:0] 		 rsp_wd_hdr_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 rsp_wd_pkt_size_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] rsp_wd_pkt_vc_o;
   logic 				 rsp_wd_pkt_valid_o;
   logic [ECI_WORD_WIDTH-1:0] 		 fwd_wod_hdr_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 fwd_wod_pkt_size_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] fwd_wod_pkt_vc_o;
   logic 				 fwd_wod_pkt_valid_o;
   logic [ECI_WORD_WIDTH-1:0] 		 lcl_rsp_wod_hdr_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 lcl_rsp_wod_pkt_size_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] lcl_rsp_wod_pkt_vc_o;
   logic 				 lcl_rsp_wod_pkt_valid_o;
   logic [MAX_DCU_ID_WIDTH-1:0] 	 rd_req_id_o;
   logic [DS_ADDR_WIDTH-1:0] 		 rd_req_addr_o;
   logic 				 rd_req_valid_o;
   logic 				 rd_rsp_ready_o;
   logic [MAX_DCU_ID_WIDTH-1:0] 	 wr_req_id_o;
   logic [DS_ADDR_WIDTH-1:0] 		 wr_req_addr_o;
   logic [ECI_CL_SIZE_BYTES-1:0] 	 wr_req_strb_o;
   logic 				 wr_req_valid_o;
   logic 				 wr_rsp_ready_o;

   logic [ECI_WORD_WIDTH-1:0] 		 curr_hdr;
   eci_word_t exp_resp_header;
   eci_word_t act_resp_header;
   eci_word_t fwd_store_header;
   eci_hreqid_t my_hreq_id;
   eci_dmask_t my_dmask;
   eci_cl_addr_t my_addr;
   eci_cl_data_t my_data;
   eci_cl_data_t my_rd_data;
   ds_cl_addr_t my_addr_rtg_casted;
   ds_cl_addr_t test_my_addr_c;

   assign test_my_addr_c = ds_cl_addr_t'(my_addr);

   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking // cb

   dcs_dcus dcs_dcus1(
		      .clk(clk),
		      .reset(reset),
		      .req_wod_hdr_i		(req_wod_hdr_i),
		      .req_wod_pkt_size_i	(req_wod_pkt_size_i),
		      .req_wod_pkt_vc_i		(req_wod_pkt_vc_i),
		      .req_wod_pkt_valid_i	(req_wod_pkt_valid_i),
		      .req_wod_pkt_ready_o	(req_wod_pkt_ready_o),
		      .rsp_wod_hdr_i		(rsp_wod_hdr_i),
		      .rsp_wod_pkt_size_i	(rsp_wod_pkt_size_i),
		      .rsp_wod_pkt_vc_i		(rsp_wod_pkt_vc_i),
		      .rsp_wod_pkt_valid_i	(rsp_wod_pkt_valid_i),
		      .rsp_wod_pkt_ready_o	(rsp_wod_pkt_ready_o),
		      .rsp_wd_hdr_i		(rsp_wd_hdr_i),
		      .rsp_wd_pkt_size_i	(rsp_wd_pkt_size_i),
		      .rsp_wd_pkt_vc_i		(rsp_wd_pkt_vc_i),
		      .rsp_wd_pkt_valid_i	(rsp_wd_pkt_valid_i),
		      .rsp_wd_pkt_ready_o	(rsp_wd_pkt_ready_o),
		      .lcl_fwd_wod_hdr_i	(lcl_fwd_wod_hdr_i),
		      .lcl_fwd_wod_pkt_size_i	(lcl_fwd_wod_pkt_size_i),
		      .lcl_fwd_wod_pkt_vc_i	(lcl_fwd_wod_pkt_vc_i),
		      .lcl_fwd_wod_pkt_valid_i	(lcl_fwd_wod_pkt_valid_i),
		      .lcl_fwd_wod_pkt_ready_o	(lcl_fwd_wod_pkt_ready_o),
		      .lcl_rsp_wod_hdr_i	(lcl_rsp_wod_hdr_i),
		      .lcl_rsp_wod_pkt_size_i	(lcl_rsp_wod_pkt_size_i),
		      .lcl_rsp_wod_pkt_vc_i	(lcl_rsp_wod_pkt_vc_i),
		      .lcl_rsp_wod_pkt_valid_i	(lcl_rsp_wod_pkt_valid_i),
		      .lcl_rsp_wod_pkt_ready_o	(lcl_rsp_wod_pkt_ready_o),
		      .rsp_wod_hdr_o		(rsp_wod_hdr_o),
		      .rsp_wod_pkt_size_o	(rsp_wod_pkt_size_o),
		      .rsp_wod_pkt_vc_o		(rsp_wod_pkt_vc_o),
		      .rsp_wod_pkt_valid_o	(rsp_wod_pkt_valid_o),
		      .rsp_wod_pkt_ready_i	(rsp_wod_pkt_ready_i),
		      .rsp_wd_hdr_o		(rsp_wd_hdr_o),
		      .rsp_wd_pkt_size_o	(rsp_wd_pkt_size_o),
		      .rsp_wd_pkt_vc_o		(rsp_wd_pkt_vc_o),
		      .rsp_wd_pkt_valid_o	(rsp_wd_pkt_valid_o),
		      .rsp_wd_pkt_ready_i	(rsp_wd_pkt_ready_i),
		      .fwd_wod_hdr_o		(fwd_wod_hdr_o),
		      .fwd_wod_pkt_size_o	(fwd_wod_pkt_size_o),
		      .fwd_wod_pkt_vc_o		(fwd_wod_pkt_vc_o),
		      .fwd_wod_pkt_valid_o	(fwd_wod_pkt_valid_o),
		      .fwd_wod_pkt_ready_i	(fwd_wod_pkt_ready_i),
		      .lcl_rsp_wod_hdr_o	(lcl_rsp_wod_hdr_o),
		      .lcl_rsp_wod_pkt_size_o	(lcl_rsp_wod_pkt_size_o),
		      .lcl_rsp_wod_pkt_vc_o	(lcl_rsp_wod_pkt_vc_o),
		      .lcl_rsp_wod_pkt_valid_o	(lcl_rsp_wod_pkt_valid_o),
		      .lcl_rsp_wod_pkt_ready_i	(lcl_rsp_wod_pkt_ready_i),
		      .rd_req_id_o		(rd_req_id_o),
		      .rd_req_addr_o		(rd_req_addr_o),
		      .rd_req_valid_o		(rd_req_valid_o),
		      .rd_req_ready_i		(rd_req_ready_i),
		      .rd_rsp_id_i		(rd_rsp_id_i),
		      .rd_rsp_valid_i		(rd_rsp_valid_i),
		      .rd_rsp_ready_o		(rd_rsp_ready_o),
		      .wr_req_id_o		(wr_req_id_o),
		      .wr_req_addr_o		(wr_req_addr_o),
		      .wr_req_strb_o		(wr_req_strb_o),
		      .wr_req_valid_o		(wr_req_valid_o),
		      .wr_req_ready_i		(wr_req_ready_i),
		      .wr_rsp_id_i		(wr_rsp_id_i),
		      .wr_rsp_bresp_i		(wr_rsp_bresp_i),
		      .wr_rsp_valid_i		(wr_rsp_valid_i),
		      .wr_rsp_ready_o		(wr_rsp_ready_o)
		      );
   
   // This DCS should always receive only receive
   // even CL indices.
   always_ff @(posedge clk) begin : TEST_CL_EVEN_ALWYAS
      assert(test_my_addr_c.dcu_id[0] === '0) else 
	$fatal(1, "Test case is wrong, only even CL indices are allowed.");
   end : TEST_CL_EVEN_ALWYAS

   // Ensure if the following signals are routed correctly.
   // 1. Input ECI header req.
   // 2. Output rd req.
   // 3. Input rd rsp.
   // 4. Output wr req.
   // 5. Input wr rsp.
   always_ff @(posedge clk) begin : TEST_DCU_IDX_ROUTING
      for( integer i=0; i<DS_NUM_DCU_IDX; i=i+1 ) begin

	 // ECI header handshake, ensure DCU_IDX in header
	 // matches DCU_IDX of the DCU where handshake happens. 
	 // req_wod
	 if(dcs_dcus1.dc_req_wod_i[i].valid & dcs_dcus1.dc_req_wod_i[i].ready) begin
	    assert( i === dcs_dcus1.dc_req_wod_i[i].hdr[(DS_DCU_ID_WIDTH+DS_BYTE_OFFSET_WIDTH-1):DS_BYTE_OFFSET_WIDTH+1]) else
	      $fatal(1,"REQ_WOD DCU IDX: Expected %0d vs Got %0d", 
		     i,
		     dcs_dcus1.dc_req_wod_i[i].hdr[(DS_DCU_ID_WIDTH+DS_BYTE_OFFSET_WIDTH-1):DS_BYTE_OFFSET_WIDTH+1]
		     );	    
	 end

	 // rsp_wod
	 if(dcs_dcus1.dc_rsp_wod_i[i].valid & dcs_dcus1.dc_rsp_wod_i[i].ready) begin
	    assert( i === dcs_dcus1.dc_rsp_wod_i[i].hdr[(DS_DCU_ID_WIDTH+DS_BYTE_OFFSET_WIDTH-1):DS_BYTE_OFFSET_WIDTH+1]) else
	      $fatal(1,"RSP_WOD DCU IDX: Expected %0d vs Got %0d", 
		     i,
		     dcs_dcus1.dc_rsp_wod_i[i].hdr[(DS_DCU_ID_WIDTH+DS_BYTE_OFFSET_WIDTH-1):DS_BYTE_OFFSET_WIDTH+1]
		     );	    
	 end

	 // rsp_wd
	 if(dcs_dcus1.dc_rsp_wd_hdr_i[i].valid & dcs_dcus1.dc_rsp_wd_hdr_i[i].ready) begin
	    assert( i === dcs_dcus1.dc_rsp_wd_hdr_i[i].hdr[(DS_DCU_ID_WIDTH+DS_BYTE_OFFSET_WIDTH-1):DS_BYTE_OFFSET_WIDTH+1]) else
	      $fatal(1,"RSP_WD DCU IDX: Expected %0d vs Got %0d", 
		     i,
		     dcs_dcus1.dc_rsp_wd_hdr_i[i].hdr[(DS_DCU_ID_WIDTH+DS_BYTE_OFFSET_WIDTH-1):DS_BYTE_OFFSET_WIDTH+1]
		     );	    
	 end

	 // lcl_fwd_wod
	 if(dcs_dcus1.dc_lcl_fwd_wod_i[i].valid & dcs_dcus1.dc_lcl_fwd_wod_i[i].ready) begin
	    assert( i === dcs_dcus1.dc_lcl_fwd_wod_i[i].hdr[(DS_DCU_ID_WIDTH+DS_BYTE_OFFSET_WIDTH-1):DS_BYTE_OFFSET_WIDTH+1]) else
	      $fatal(1,"LCL_FWD_WOD DCU IDX: Expected %0d vs Got %0d", 
		     i,
		     dcs_dcus1.dc_lcl_fwd_wod_i[i].hdr[(DS_DCU_ID_WIDTH+DS_BYTE_OFFSET_WIDTH-1):DS_BYTE_OFFSET_WIDTH+1]
		     );
	 end

	 // lcl_rsp_wod
	 if(dcs_dcus1.dc_lcl_rsp_wod_i[i].valid & dcs_dcus1.dc_lcl_rsp_wod_i[i].ready) begin
	    assert( i === dcs_dcus1.dc_lcl_rsp_wod_i[i].hdr[(DS_DCU_ID_WIDTH+DS_BYTE_OFFSET_WIDTH-1):DS_BYTE_OFFSET_WIDTH+1]) else
	      $fatal(1,"LCL_RSP_WOD DCU IDX: Expected %0d vs Got %0d", 
		     i,
		     dcs_dcus1.dc_lcl_rsp_wod_i[i].hdr[(DS_DCU_ID_WIDTH+DS_BYTE_OFFSET_WIDTH-1):DS_BYTE_OFFSET_WIDTH+1]
		     );
	 end
	 
	 // If handshake happens in rd req I/F of DCU 
	 // ensure the ID generated matches DCU ID of the DCU.
	 if(dcs_dcus1.dc_rd_req_ctrl_o[i].valid & dcs_dcus1.dc_rd_req_ctrl_o[i].ready) begin
	    assert(dcs_dcus1.dc_rd_req_ctrl_o[i].id === {i,1'b0}) else
	      $fatal(1,"Rd req ID: Expected %0d, Got %0d", 
		     {i,1'b0}, 
		     dcs_dcus1.dc_rd_req_ctrl_o[i].id);
	 end

	 // If handshake happens in rd rsp I/F of DCU,
	 // ensure the ID received matches the DCU ID of DCU.
	 if(dcs_dcus1.dc_rd_rsp_ctrl_i[i].valid & dcs_dcus1.dc_rd_rsp_ctrl_i[i].ready) begin
	    assert(dcs_dcus1.dc_rd_rsp_ctrl_i[i].id === {i,1'b0}) else
	      $fatal(1,"Rd rsp ID: Expected %0d, Got %0d",
		     {i,1'b0}, 
		     dcs_dcus1.dc_rd_rsp_ctrl_i[i].id
		     );
	 end

	 // if handshake happens in wr req I/F of DCU,
	 // ensure wr req ID matches DCU_ID.
	 if(dcs_dcus1.dc_wr_req_ctrl_o[i].valid & dcs_dcus1.dc_wr_req_ctrl_o[i].ready) begin
	    assert(dcs_dcus1.dc_wr_req_ctrl_o[i].id === {i,1'b0}) else
	      $fatal(1,"Wr req ID: Expected %0d, Got %0d",
		     {i,1'b0}, 
		     dcs_dcus1.dc_wr_req_ctrl_o[i].id
		     );
	 end

	 // If handshake happens in wr rsp I/F of DCU,
	 // ensure wr rsp ID matches DCU ID.
	 if(dcs_dcus1.dc_wr_rsp_ctrl_i[i].valid & dcs_dcus1.dc_wr_rsp_ctrl_i[i].ready) begin
	    assert(dcs_dcus1.dc_wr_rsp_ctrl_i[i].id === {i,1'b0}) else
	      $fatal(1,"Wr rsp ID: Expected %0d, Got %0d",
		     {i,1'b0}, 
		     dcs_dcus1.dc_wr_rsp_ctrl_i[i].id
		     );
	 end

	 // If handshake happens in ECI response I/F of a DCU,
	 // ensure the DCU IDX of rsp header matches DCU IDX
	 // of DCU issuing it.
	 if(dcs_dcus1.dc_eci_rsp_hdr_o[i].valid & dcs_dcus1.dc_eci_rsp_hdr_o[i].ready) begin
	    assert( i === dcs_dcus1.dc_eci_rsp_hdr_o[i].hdr[(DS_DCU_ID_WIDTH+DS_BYTE_OFFSET_WIDTH-1):DS_BYTE_OFFSET_WIDTH+1]) else
	      $fatal(1,"ECI RSP DCU IDX: Expected %0d vs Got %0d", 
		     i,
		     dcs_dcus1.dc_eci_rsp_hdr_o[i].hdr[(DS_DCU_ID_WIDTH+DS_BYTE_OFFSET_WIDTH-1):DS_BYTE_OFFSET_WIDTH+1]
		     );
	 end
	 
      end // for.      
   end : TEST_DCU_IDX_ROUTING

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports 
      clk = '0;
      reset = '0;
      req_wod_hdr_i = '0;
      req_wod_pkt_size_i = '0;
      req_wod_pkt_vc_i = '0;
      req_wod_pkt_valid_i = '0;
      rsp_wod_hdr_i = '0;
      rsp_wod_pkt_size_i = '0;
      rsp_wod_pkt_vc_i = '0;
      rsp_wod_pkt_valid_i = '0;
      rsp_wd_hdr_i = '0;
      rsp_wd_pkt_size_i = '0;
      rsp_wd_pkt_vc_i = '0;
      rsp_wd_pkt_valid_i = '0;
      lcl_fwd_wod_hdr_i = '0;
      lcl_fwd_wod_pkt_size_i = '0;
      lcl_fwd_wod_pkt_vc_i = '0;
      lcl_fwd_wod_pkt_valid_i = '0;
      lcl_rsp_wod_hdr_i = '0;
      lcl_rsp_wod_pkt_size_i = '0;
      lcl_rsp_wod_pkt_vc_i = '0;
      lcl_rsp_wod_pkt_valid_i = '0;
      rsp_wod_pkt_ready_i = '0;
      rsp_wd_pkt_ready_i = '0;
      fwd_wod_pkt_ready_i = '0;
      lcl_rsp_wod_pkt_ready_i = '0;
      
      my_hreq_id = '0;
      my_dmask = '0;
      my_addr = '0;
      my_data = '0;
      my_rd_data = '0;

      // Reset system, DCUs take a number of
      // cycles to clear its TSU before consuming
      // first request.
      reset_system();
      rsp_wod_pkt_ready_i = '1;
      rsp_wd_pkt_ready_i = '1;
      fwd_wod_pkt_ready_i = '1;
      lcl_rsp_wod_pkt_ready_i = '1;
      ##5;

      // Test to check if routing to and from
      // DCUs are as expected.
      // 
      // Upgrade one CL in each DCU to exclusive,
      // then downgrade with data. 
      // Upgrade causes read request and 
      // response on each DCU.
      // Upgrade also causes DCU to issue an ECI
      // header response. 
      // Downgrade causes write request and
      // response on each DCU.
      // Then we can test if the following signals are routed correctly.
      // 1. Input ECI header req.
      // 2. Output rd req.
      // 3. Input rd rsp.
      // 4. Output ECI header rsp.
      // 5. Output wr req.
      // 6. Input wr rsp.
      test_sweep_r13_ra3_v31d();
      test_sweep_r13_ra3_lci_a31d_lcia();
      
      // Tests in block TEST_DCU_IDX_ROUTING.
      $display("Pass: All incoming ECI events are for even CL indices");
      $display("Pass: Incoming ECI headers are routed to correct DCUs.");
      $display("Pass: Outgoing read requests from DCUS have IDs matching their DCU IDs.");
      $display("Pass: Incoming read responses to DCUs have IDs matching their DCU IDs.");
      $display("Pass: Outgoing write requests from DCUS have IDs matching their DCU IDs.");
      $display("Pass: Incoming write responses to DCUs have IDs matching their DCU IDs.");
      $display("Pass: Outing ECI rsps from DCUs are issued for correct DCU IDX");
      
      #500 $finish;
   end 

   word_addr_mem #
     (
      .ID_WIDTH(MAX_DCU_ID_WIDTH),
      .ADDR_WIDTH(10),
      .DATA_WIDTH(ECI_CL_WIDTH)
       )
   mem_inst_1
     (
      .clk(clk),
      .reset(reset),

      .rd_req_id_i(rd_req_id_o),
      .rd_req_addr_i(rd_req_addr_o[9:0]),
      .rd_req_valid_i(rd_req_valid_o),
      .rd_req_ready_o(rd_req_ready_i),
      
      .wr_req_id_i(wr_req_id_o),
      .wr_req_addr_i(wr_req_addr_o[9:0]),
      .wr_req_data_i('0), // No data to write.
      .wr_req_valid_i(wr_req_valid_o),
      .wr_req_ready_o(wr_req_ready_i),
      
      .rd_rsp_id_o(rd_rsp_id_i),
      .rd_rsp_data_o(), // Not connected
      .rd_rsp_valid_o(rd_rsp_valid_i),
      .rd_rsp_ready_i(rd_rsp_ready_o),
      
      .wr_rsp_id_o(wr_rsp_id_i),
      .wr_rsp_bresp_o(wr_rsp_bresp_i),
      .wr_rsp_valid_o(wr_rsp_valid_i),
      .wr_rsp_ready_i(wr_rsp_ready_o)
      );
   
   //---------------------------------------------------------------
   // Tasks and functions below

   task static reset_system();
      $display("Resetting system");
      ##5;
      reset = 1'b1;
      ##20;
      reset = 1'b0;
   endtask // reset_system

   task static test_sweep_r13_ra3_lci_a31d_lcia();
      $display("Test: test_sweep_r13_ra3_lci_a31d_lcia");
      my_addr_rtg_casted = '0;
      my_addr_rtg_casted.set_within_dcu = $urandom_range(0, 8092);
      my_addr_rtg_casted.dcu_id = 'd0; // starting DCU IDX is 0.
      my_addr_rtg_casted.tag = $urandom_range(100, 5000);
      my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
      my_hreq_id = eci_hreqid_t'($urandom_range(1,64));
      my_dmask = '1;
      my_data.scls[0] = 1;
      my_data.scls[1] = 2;
      my_data.scls[2] = 3;
      my_data.scls[3] = 4;
      repeat(DS_NUM_DCU_IDX) begin
	 test_r13_ra3(
		      .my_id_i(my_hreq_id),
		      .my_dmask_i(my_dmask),
		      .my_addr_i(my_addr.flat)
		      );

	 //my_addr_rtg_casted = '0;
	 // issue req to next DCU IDX within this slice.
	 // rest of the values in address remain same so V31d can be sent
	 // without keeping track fo the values. 
	 my_addr_rtg_casted.dcu_id = my_addr_rtg_casted.dcu_id + 2;
	 my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
	 my_hreq_id = eci_hreqid_t'($urandom_range(1,64));
      end

      ##5;
      // send LCI for all the CLs that were upgraded
      // to exclusive in the previous step.
      my_addr_rtg_casted.dcu_id = '0;
      my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
      my_hreq_id = eci_hreqid_t'($urandom_range(1,64));
      repeat(DS_NUM_DCU_IDX) begin
	 send_lci(
		  .my_id_i(my_hreq_id),
		  .my_ns_i(1'b1),
		  .my_addr_i(my_addr.flat),
		  .my_dmask_i(my_dmask)
		  );
	 
	 my_addr_rtg_casted.dcu_id = my_addr_rtg_casted.dcu_id + 2;
	 my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
	 my_hreq_id = eci_hreqid_t'($urandom_range(1,64));
      end

      // Send A31d
      my_addr_rtg_casted.dcu_id = '0;
      my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
      my_hreq_id = eci_hreqid_t'($urandom_range(1,64));
      repeat(DS_NUM_DCU_IDX) begin
	 fwd_store_header = eci_cmd_defs::eci_gen_fevx_eh(
							  .hreq_id_i(my_hreq_id),
							  .dmask_i(my_dmask),
							  .ns_i(1'b1),
							  .addr_i(my_addr.flat)
							  );
	 send_A31d(
		   .fwd_req_i(fwd_store_header),
		   .my_data_i('0) // dont care.
		   );
	 my_addr_rtg_casted.dcu_id = my_addr_rtg_casted.dcu_id + 2;
	 my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
	 my_hreq_id = eci_hreqid_t'($urandom_range(1,64));
      end

      // CLs should be locked here.
      // Unlock them.
      my_addr_rtg_casted.dcu_id = '0;
      my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
      repeat(DS_NUM_DCU_IDX) begin
	 send_ul(
		 .my_addr_i(my_addr.flat)
		 );
	 my_addr_rtg_casted.dcu_id = my_addr_rtg_casted.dcu_id + 2;
	 my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
      end

      // Upgrede to exclusive again.
      my_addr_rtg_casted = '0;
      my_addr_rtg_casted.set_within_dcu = $urandom_range(0, 8092);
      my_addr_rtg_casted.dcu_id = 'd0; // starting DCU IDX is 0.
      my_addr_rtg_casted.tag = $urandom_range(100, 5000);
      my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
      my_hreq_id = eci_hreqid_t'($urandom_range(1,64));
      my_dmask = '1;
      my_data.scls[0] = 1;
      my_data.scls[1] = 2;
      my_data.scls[2] = 3;
      my_data.scls[3] = 4;
      repeat(DS_NUM_DCU_IDX) begin
	 test_r13_ra3(
		      .my_id_i(my_hreq_id),
		      .my_dmask_i(my_dmask),
		      .my_addr_i(my_addr.flat)
		      );

	 //my_addr_rtg_casted = '0;
	 // issue req to next DCU IDX within this slice.
	 // rest of the values in address remain same so V31d can be sent
	 // without keeping track fo the values. 
	 my_addr_rtg_casted.dcu_id = my_addr_rtg_casted.dcu_id + 2;
	 my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
	 my_hreq_id = eci_hreqid_t'($urandom_range(1,64));
      end

      $display("Pass: test_sweep_r13_ra3_lci_a31d_lcia\n");
   endtask //test_sweep_r13_ra3_lci_a31d_lcia

   task static test_sweep_r13_ra3_v31d();
      $display("Test: test_sweep_r13_ra3_v31d");
      // DCU_ID = {DCU_IDX, ODD/EVEN}.
      // we always consider ODD/EVEN is 0.
      // So including the byte offset, we need offset of 256 bytes
      // to sweep through the DCUs. 
      my_addr_rtg_casted = '0;
      my_addr_rtg_casted.set_within_dcu = $urandom_range(0, 8092);
      my_addr_rtg_casted.dcu_id = 'd0; // starting DCU IDX is 0.
      my_addr_rtg_casted.tag = $urandom_range(100, 5000);
      my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
      my_hreq_id = eci_hreqid_t'($urandom_range(1,64));
      my_dmask = '1;
      my_data.scls[0] = 1;
      my_data.scls[1] = 2;
      my_data.scls[2] = 3;
      my_data.scls[3] = 4;
      repeat(DS_NUM_DCU_IDX) begin
	 test_r13_ra3(
		      .my_id_i(my_hreq_id),
		      .my_dmask_i(my_dmask),
		      .my_addr_i(my_addr.flat)
		      );

	 //my_addr_rtg_casted = '0;
	 // issue req to next DCU IDX within this slice.
	 // rest of the values in address remain same so V31d can be sent
	 // without keeping track fo the values. 
	 my_addr_rtg_casted.dcu_id = my_addr_rtg_casted.dcu_id + 2;
	 my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
	 my_hreq_id = eci_hreqid_t'($urandom_range(1,64));
      end

      ##5;
      // send V31d for all the CLs that were upgraded
      // to exclusive in the previous step.
      my_addr_rtg_casted.dcu_id = '0;
      my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
      repeat(DS_NUM_DCU_IDX) begin
	 send_v31d(
		   .my_addr_i(my_addr.flat),
		   .my_dmask_i(my_dmask),
		   .my_data_i(my_data)
		   );
	 
	 my_addr_rtg_casted.dcu_id = my_addr_rtg_casted.dcu_id + 2;
	 my_addr.flat = ECI_ADDR_WIDTH'(my_addr_rtg_casted);
      end
      $display("Pass: test_sweep_r13_ra3_v31d");
   endtask //test_sweep_r13_ra3_v31d

   task static test_r13_ra3(
			    input eci_hreqid_t my_id_i,
			    input eci_dmask_t my_dmask_i,
			    input eci_cl_addr_t my_addr_i
			    );
      $display("Test: test_r13_ra3");
      // Sends R13 and waits for RA3. When RA3 is recd, it is compared with expected.
      send_req_wo_data(
		       // casting down from 6 to 5 bits 
		       .my_id_i(eci_id_t'(my_id_i)),
		       .my_dmask_i(my_dmask_i),
		       .my_addr_i(my_addr_i.flat),
		       .rldt_i(1'b0),
		       .rldi_i(1'b0),
		       .rldd_i(1'b0),
		       .rc2d_s_i(1'b0),
		       .rldx_i(1'b1)
		       );
      wait(rsp_wd_pkt_valid_o & rsp_wd_pkt_ready_i);
      // Receive PEMD with data
      exp_resp_header = eci_cmd_defs::eci_gen_pemd(.req_i(curr_hdr), .pemn_i(1'b0));
      act_resp_header = eci_word_t'(rsp_wd_hdr_o);
      if(act_resp_header.eci_word !== exp_resp_header.eci_word) begin
	 $error("Error: RLDD should respond with PEMD and that is not happeneing");
	 ##100;
	 $finish;
      end
      ##1; // wait for output valid signal to go low.
      //$display("\tExpected response header matches actual header");
      $display("Pass: test_r13_ra3");
   endtask


   //---------------------------------------------------------------
   // Configure generic tasks to send specific events. 
   task static send_ul(
		       input eci_address_t my_addr_i
		       );
      $display("Sending UL");
      send_lcl_ul(
		  .my_addr_i(my_addr_i)
		  );
      ##10;
   endtask //send_ul

   task static send_lcl_ul(
			   input eci_address_t my_addr_i
			   );
      eci_word_t my_hdr;
      eci_cl_data_t my_data2send;
      eci_vc_size_t my_vc;
      logic [ECI_PACKET_SIZE_WIDTH-1:0] my_size;
      my_hdr = gen_lcl_ul_hdr(
			      .my_addr_i(my_addr_i)
			      );
      // Data - no data to send.
      my_data2send.flat = '0;
      // VC -odd cl index to even VC and vice versa.
      if(my_addr_i[ECI_CL_ADDR_LSB] == CL_IDX_ODD) begin
	 my_vc = eci_vc_size_t'(VC_LCL_RESP_WO_DATA_E);
      end else begin
	 my_vc = eci_vc_size_t'(VC_LCL_RESP_WO_DATA_O);
      end
      // Packet size - always 1.
      my_size = ECI_PACKET_SIZE_WIDTH'('d1);
      // send to dcu.
      send_cmd(
	       .my_hdr_i(my_hdr),
	       .my_data_i(my_data2send),
	       .my_vc_i(my_vc),
	       .my_size_i(my_size)
	       );
   endtask //send_lcl_mrsp

   task static send_A11(
			input eci_word_t fwd_req_i
			);
      // HAKV
      // There is no data only header.
      $display("Sending A11");
      send_hak(
	       .fwd_req_i(fwd_req_i),
	       .my_data_i('0),
	       .vicdhi_i(1'b0),
	       .vicdhi_n_i(1'b0),
	       .hakd_i(1'b0),
	       .hakd_n_i(1'b0),
	       .hakn_s_i(1'b0),
	       .haki_i(1'b0),
	       .haks_i(1'b0),
	       .hakv_i(1'b1)
	       );
      ##10; // to avoid stall      
   endtask // send_A21
   
   task static send_A31d(
			 input eci_word_t fwd_req_i,
			 input eci_cl_data_t my_data_i
			 );
      // VICDHI
      $display("Sending A31d");
      send_hak(
	       .fwd_req_i(fwd_req_i),
	       .my_data_i(my_data_i),
	       .vicdhi_i(1'b1),
	       .vicdhi_n_i(1'b0),
	       .hakd_i(1'b0),
	       .hakd_n_i(1'b0),
	       .hakn_s_i(1'b0),
	       .haki_i(1'b0),
	       .haks_i(1'b0),
	       .hakv_i(1'b0)
	       );
      ##10; // to avoid stall 
   endtask // send_A31d

   task static send_A21(
			input eci_word_t fwd_req_i
			);
      // HAKD_N
      // There is no data only header.
      $display("Sending A21");
      send_hak(
	       .fwd_req_i(fwd_req_i),
	       .my_data_i('0),
	       .vicdhi_i(1'b0),
	       .vicdhi_n_i(1'b0),
	       .hakd_i(1'b0),
	       .hakd_n_i(1'b1),
	       .hakn_s_i(1'b0),
	       .haki_i(1'b0),
	       .haks_i(1'b0),
	       .hakv_i(1'b0)
	       );
      ##10; // to avoid stall      
   endtask // send_A21


   task static send_v31d(
			 input eci_address_t my_addr_i,
			 input eci_dmask_t my_dmask_i,
			 input eci_cl_data_t my_data_i
			 );
      $display("Sending V31d");
      send_vic(
	       // VICs have no transaction IDs 
	       .my_addr_i(my_addr_i),
	       .my_dmask_i(my_dmask_i),
	       .my_data_i(my_data_i),
	       .vics_i(1'b0),
	       .vicc_i(1'b0),
	       .vicd_i(1'b1),
	       .vicd_n_i(1'b0),
	       .vicc_n_i(1'b0)
	       );
      // CL is in state 1 now, read it to check data
      ##10; // to avoid stalls
   endtask // send_v31d

   task static send_v32d(
			 input eci_address_t my_addr_i,
			 input eci_dmask_t my_dmask_i,
			 input eci_cl_data_t my_data_i
			 );
      $display("Sending V32d");
      send_vic(
	       // VICs have no transaction IDs 
	       .my_addr_i(my_addr_i),
	       .my_dmask_i(my_dmask_i),
	       .my_data_i(my_data_i),
	       .vics_i(1'b0),
	       .vicc_i(1'b1),
	       .vicd_i(1'b0),
	       .vicd_n_i(1'b0),
	       .vicc_n_i(1'b0)
	       );
      ##10; //to avoid stall 
   endtask // send_v32d


   task static send_v31(
			input eci_address_t my_addr_i,
			input eci_dmask_t my_dmask_i,
			input eci_cl_data_t my_data_i
			);
      $display("Sending V31");
      send_vic(
	       // VICs have no transaction IDs 
	       .my_addr_i(my_addr_i),
	       .my_dmask_i(my_dmask_i),
	       .my_data_i(my_data_i),
	       .vics_i(1'b0),
	       .vicc_i(1'b0),
	       .vicd_i(1'b0),
	       .vicd_n_i(1'b1),
	       .vicc_n_i(1'b0)
	       );
      ##10; // to avoid stalls 
   endtask // send_v31

   task static send_v21(
			input eci_address_t my_addr_i,
			input eci_dmask_t my_dmask_i
			);
      $display("Sending V21");
      send_vic(
	       // VICs have no transaction IDs 
	       .my_addr_i(my_addr_i),
	       .my_dmask_i(my_dmask_i),
	       .my_data_i('0),
	       .vics_i(1'b1),
	       .vicc_i(1'b0),
	       .vicd_i(1'b0),
	       .vicd_n_i(1'b0),
	       .vicc_n_i(1'b0)
	       );
      ##10; // to avoid stalls 
   endtask // send_v21

   
   //---------------------------------------------------------------
   // Configurable tasks to send different events corresponding to a VC to DirC.
   // Generate and send  requests without datta
   // RLDT, RLDD, RLDI, RC2D_S 
   task send_req_wo_data(
			 input 	     eci_id_t my_id_i,
			 input 	     eci_dmask_t my_dmask_i,
			 input 	     eci_address_t my_addr_i,
			 // Note only 1 of the folloaing bits
			 // can be set 
			 input logic rldt_i,
			 input logic rldd_i,
			 input logic rldi_i,
			 input logic rc2d_s_i,
			 input logic rldx_i
		  );
      eci_word_t my_hdr;
      eci_cl_data_t my_data2send;
      eci_vc_size_t my_vc;
      logic [ECI_PACKET_SIZE_WIDTH-1:0] my_size;
      // Header 
      my_hdr = gen_req_wo_data(
			       .rreq_id_i(my_id_i),
			       .dmask_i(my_dmask_i),
			       .addr_i(my_addr_i),
			       .rldt_i(rldt_i),
			       .rldi_i(rldi_i),
			       .rldd_i(rldd_i),
			       .rc2d_s_i(rc2d_s_i),
			       .rldx_i(rldx_i)
			       );
      // Data 
      // Dont care about data for read
      my_data2send.flat = '0;
      // VC
      // request wo data - VC6/7
      // odd cl indices VC6, even VC7
      my_vc = '0;
      if(my_addr_i[ECI_CL_ADDR_LSB] == CL_IDX_ODD) begin
	 my_vc = eci_vc_size_t'('d6);
      end else begin
	 my_vc = eci_vc_size_t'('d7);
      end
      // Packet size 
      // req wo data has no data so size is 1
      my_size = ECI_PACKET_SIZE_WIDTH'('d1);
      // Send to dirc 
      send_cmd(
	       .my_hdr_i(my_hdr),
	       .my_data_i(my_data2send),
	       .my_vc_i(my_vc),
	       .my_size_i(my_size)
	       );
   endtask // send_req_wo_data

   // Send HAK commands to DirC 
   task static send_hak(
			input 	    eci_word_t fwd_req_i,
			input 	    eci_cl_data_t my_data_i,
			input logic vicdhi_i,
			input logic vicdhi_n_i,
			input logic hakd_i,
			input logic hakd_n_i,
			input logic hakn_s_i,
			input logic haki_i,
			input logic haks_i,
			input logic hakv_i
			);
      eci_word_t my_hdr;
      eci_cl_data_t my_data2send;
      eci_vc_size_t my_vc;
      logic [ECI_PACKET_SIZE_WIDTH-1:0] my_size;
      // Header
      my_hdr = gen_hak_hdr(
			   .fwd_req_i(fwd_req_i),
			   .vicdhi_i(vicdhi_i),
			   .vicdhi_n_i(vicdhi_n_i),
			   .hakd_i(hakd_i),
			   .hakd_n_i(hakd_n_i),
			   .hakn_s_i(hakn_s_i),
			   .haki_i(haki_i),
			   .haks_i(haks_i),
			   .hakv_i(hakv_i)
			   );
      // Data
      my_data2send.flat = '0;
      if(vicdhi_n_i | hakd_n_i | hakn_s_i | haks_i | hakv_i | haki_i) begin
	 // No data needs to be sent
	 // Response without data - VC 10,11
	 my_data2send.flat = '0;
	 my_size = ECI_PACKET_SIZE_WIDTH'('d1);
	 if(fwd_req_i.mfwd_generic.address[ECI_CL_ADDR_LSB] == CL_IDX_ODD) begin
	    my_vc = eci_vc_size_t'('d10);
	 end else begin
	    my_vc = eci_vc_size_t'('d11);
	 end
      end else begin
	 // Response with data VC 4,5
	 my_data2send = my_data_i;
	 my_size = get_pkt_size_from_dmask(.my_dmask_i(fwd_req_i.mfwd_generic.dmask));
	 if(fwd_req_i.mfwd_generic.address[ECI_CL_ADDR_LSB] == CL_IDX_ODD) begin
	    my_vc = eci_vc_size_t'('d4);
	 end else begin
	    my_vc = eci_vc_size_t'('d5);
	 end
      end
      // Send to dirc 
      send_cmd(
	       .my_hdr_i(my_hdr),
	       .my_data_i(my_data2send),
	       .my_vc_i(my_vc),
	       .my_size_i(my_size)
	       );
   endtask //send_hak

   // Send VIC
   task static send_vic(
			input eci_address_t my_addr_i,
			input eci_dmask_t my_dmask_i,
			input eci_cl_data_t my_data_i,
			input vics_i, //V21
			input vicc_i, //V32d
			input vicd_i, //V31d
			input vicd_n_i, //V31
			input vicc_n_i  //V32
			);
      eci_word_t my_hdr;
      eci_cl_data_t my_data2send;
      eci_vc_size_t my_vc;
      logic [ECI_PACKET_SIZE_WIDTH-1:0] my_size;
      // Header 
      my_hdr = gen_vic_hdr(
			   .my_addr_i(my_addr_i),
			   .my_dmask_i(my_dmask_i),
			   .vics_i(vics_i),
			   .vicc_i(vicc_i),
			   .vicd_i(vicd_i),
			   .vicd_n_i(vicd_n_i),
			   .vicc_n_i(vicc_n_i)
			   );
      // Data 
      my_data2send.flat = '0;
      if(vics_i | vicd_n_i | vicc_n_i) begin
	 // No data needed for VICS, VICD.N, VICC.N
	 my_data2send.flat = '0;
      end else begin
	 my_data2send = my_data_i;
      end
      // VC
      my_vc = '0;
      if(vics_i | vicd_n_i | vicc_n_i) begin
	 // resp wo data 10, 11
	 if(my_addr_i[ECI_CL_ADDR_LSB] == CL_IDX_ODD) begin
	    my_vc = eci_vc_size_t'('d10);
	 end else begin
	    my_vc = eci_vc_size_t'('d11);
	 end
      end else begin
	 // response with data VC4, 5 V32d, V31d
	 if(my_addr_i[ECI_CL_ADDR_LSB] == CL_IDX_ODD) begin
	    my_vc = eci_vc_size_t'('d4);
	 end else begin
	    my_vc = eci_vc_size_t'('d5);
	 end
      end
      // Packet size 
      my_size = '0;
      if(vics_i | vicc_n_i | vicd_n_i) begin
	 // Only header no data for V21, V32, V31
	 my_size = ECI_PACKET_SIZE_WIDTH'('d1);
      end else begin
	 my_size = get_pkt_size_from_dmask(.my_dmask_i(my_dmask_i));
      end
      // Send to dirc 
      send_cmd(
	       .my_hdr_i(my_hdr),
	       .my_data_i(my_data2send),
	       .my_vc_i(my_vc),
	       .my_size_i(my_size)
	       );
   endtask //send_vic

   
   task static send_lc(
		       input 	   eci_hreqid_t my_id_i,
		       input 	   eci_dmask_t my_dmask_i,
		       input logic my_ns_i,
		       input 	   eci_address_t my_addr_i
		       );
      $display("Sending LC");
      send_lcl_mfwd(
		    .my_id_i(my_id_i),
		    .my_dmask_i(my_dmask_i),
		    .my_ns_i(my_ns_i),
		    .my_addr_i(my_addr_i),
		    .lc_i(1'b1),
		    .lci_i(1'b0)
		    );
   endtask //send_lc

   task static send_lci(
		       input 	   eci_hreqid_t my_id_i,
		       input 	   eci_dmask_t my_dmask_i,
		       input logic my_ns_i,
		       input 	   eci_address_t my_addr_i
		       );
      $display("Sending LCI");
      send_lcl_mfwd(
		    .my_id_i(my_id_i),
		    .my_dmask_i(my_dmask_i),
		    .my_ns_i(my_ns_i),
		    .my_addr_i(my_addr_i),
		    .lc_i(1'b0),
		    .lci_i(1'b1)
		    );
   endtask //send_lci


   task static send_lcl_mfwd(
			     input 	 eci_hreqid_t my_id_i,
			     input 	 eci_dmask_t my_dmask_i,
			     input logic my_ns_i,
			     input 	 eci_address_t my_addr_i,
			     // local clean.
			     input logic lc_i,
			     // local clean invalidate. 
			     input logic lci_i
			     );
      eci_word_t my_hdr;
      eci_cl_data_t my_data2send;
      eci_vc_size_t my_vc;
      logic [ECI_PACKET_SIZE_WIDTH-1:0] my_size;
      // generate header
      my_hdr = gen_lcl_mfwd_hdr(
				.my_id_i(my_id_i),
				.my_dmask_i(my_dmask_i),
				.my_ns_i(my_ns_i),
				.my_addr_i(my_addr_i),
				.lc_i(lc_i),
				.lci_i(lci_i)
				);
      // Data - no data to send.
      my_data2send.flat = '0;
      // VC -odd cl index to even VC and vice versa.
      if(my_addr_i[ECI_CL_ADDR_LSB] == CL_IDX_ODD) begin
	 my_vc = eci_vc_size_t'(VC_LCL_FWD_WO_DATA_E);
      end else begin
	 my_vc = eci_vc_size_t'(VC_LCL_FWD_WO_DATA_O);
      end
      // Packet size - always 1.
      my_size = ECI_PACKET_SIZE_WIDTH'('d1);
      // send to dcu.
      send_cmd(
	       .my_hdr_i(my_hdr),
	       .my_data_i(my_data2send),
	       .my_vc_i(my_vc),
	       .my_size_i(my_size)
	       );
   endtask //send_lcl_mfwd

   //---------------------------------------------------------------
   // Most basic task
   
   task static send_cmd
     (
      // header 
      input 				      eci_word_t my_hdr_i,
      // data 
      input 				      eci_cl_data_t my_data_i,
      // VC 
      input 				      eci_vc_size_t my_vc_i,
      input logic [ECI_PACKET_SIZE_WIDTH-1:0] my_size_i
      );
      // Generic task to send input ECI event to dirC.
      if((my_vc_i === VC_REQ_WO_DATA_E) | (my_vc_i === VC_REQ_WO_DATA_O)) begin
	 send_cmd_req_wod(
		      .my_hdr_i(my_hdr_i),
		      .my_vc_i(my_vc_i),
		      .my_size_i(my_size_i)
		      );
	 
      end else if ((my_vc_i === VC_RESP_WO_DATA_E) | (my_vc_i === VC_RESP_WO_DATA_O)) begin
	 send_cmd_rsp_wod(
		      .my_hdr_i(my_hdr_i),
		      .my_vc_i(my_vc_i),
		      .my_size_i(my_size_i)
		      );
	 
      end else if ((my_vc_i === VC_RESP_W_DATA_E) | (my_vc_i === VC_RESP_W_DATA_O)) begin
	 // DCS_DCUS do not accept data and is ignored. 
	 send_cmd_rsp_wd(
		     .my_hdr_i(my_hdr_i),
		     .my_data_i(my_data_i),
		     .my_vc_i(my_vc_i),
		     .my_size_i(my_size_i)
		     );
	 
      end else if ((my_vc_i === VC_LCL_FWD_WO_DATA_E) | (my_vc_i === VC_LCL_FWD_WO_DATA_O)) begin
	 send_cmd_lcl_fwd_wod(
			      .my_hdr_i(my_hdr_i),
			      .my_vc_i(my_vc_i),
			      .my_size_i(my_size_i)
			      );
      end else if ((my_vc_i === VC_LCL_RESP_WO_DATA_E) | (my_vc_i === VC_LCL_RESP_WO_DATA_O)) begin
	 send_cmd_lcl_rsp_wod(
			      .my_hdr_i(my_hdr_i),
			      .my_vc_i(my_vc_i),
			      .my_size_i(my_size_i)
			      );
      end
   endtask // send_cmd

   task static send_cmd_req_wod
     (
      // header 
      input 				      eci_word_t my_hdr_i,
      // no data because wod.
      // VC 
      input 				      eci_vc_size_t my_vc_i,
      input logic [ECI_PACKET_SIZE_WIDTH-1:0] my_size_i
      );
      // Generic task to send request without data to DCS.
      curr_hdr = my_hdr_i;
      req_wod_hdr_i = my_hdr_i;
      req_wod_pkt_size_i = my_size_i;
      req_wod_pkt_vc_i = my_vc_i;
      req_wod_pkt_valid_i = 1'b1;
      wait((req_wod_pkt_valid_i & req_wod_pkt_ready_o));
      ##1;
      req_wod_pkt_valid_i = 1'b0;
   endtask //send_req_wod

   task static send_cmd_rsp_wod
      (
      // header 
      input 				      eci_word_t my_hdr_i,
      // no data because wod.
      // VC 
      input 				      eci_vc_size_t my_vc_i,
      input logic [ECI_PACKET_SIZE_WIDTH-1:0] my_size_i
      );
      // Generic task to send response without data to DCS.
      curr_hdr = my_hdr_i;
      rsp_wod_hdr_i = my_hdr_i;
      rsp_wod_pkt_size_i = my_size_i;
      rsp_wod_pkt_vc_i = my_vc_i;
      rsp_wod_pkt_valid_i = 1'b1;
      wait((rsp_wod_pkt_valid_i & rsp_wod_pkt_ready_o));
      ##1;
      rsp_wod_pkt_valid_i = 1'b0;
   endtask //send_rsp_wod

   task static send_cmd_rsp_wd
     (
      // header 
      input 				      eci_word_t my_hdr_i,
      // DCS_DCUS do not accept data. 
      // data is ignored here.
      input 				      eci_cl_data_t my_data_i,
      // VC 
      input 				      eci_vc_size_t my_vc_i,
      input logic [ECI_PACKET_SIZE_WIDTH-1:0] my_size_i
      );
      // Generic task to send response with data to DCS.
      curr_hdr = my_hdr_i;
      rsp_wd_hdr_i = my_hdr_i;
      // data is ignored. 
      rsp_wd_pkt_size_i = my_size_i;
      rsp_wd_pkt_vc_i = my_vc_i;
      rsp_wd_pkt_valid_i = 1'b1;
      wait(rsp_wd_pkt_valid_i & rsp_wd_pkt_ready_o);
      ##1;
      rsp_wd_pkt_valid_i = 1'b0;
   endtask //send_rsp_wd

   task static send_cmd_lcl_fwd_wod
     (
      // header 
      input 				      eci_word_t my_hdr_i,
      // VC 
      input 				      eci_vc_size_t my_vc_i,
      input logic [ECI_PACKET_SIZE_WIDTH-1:0] my_size_i
      );
      // Generic task to send response with data to DCS.
      curr_hdr = my_hdr_i;
      lcl_fwd_wod_hdr_i = my_hdr_i;
      // data is ignored. 
      lcl_fwd_wod_pkt_size_i = my_size_i;
      lcl_fwd_wod_pkt_vc_i = my_vc_i;
      lcl_fwd_wod_pkt_valid_i = 1'b1;
      wait(lcl_fwd_wod_pkt_valid_i & lcl_fwd_wod_pkt_ready_o);
      ##1;
      lcl_fwd_wod_pkt_valid_i = 1'b0;
   endtask //send_rsp_wd

   task static send_cmd_lcl_rsp_wod
     (
      // header 
      input 				      eci_word_t my_hdr_i,
      // VC 
      input 				      eci_vc_size_t my_vc_i,
      input logic [ECI_PACKET_SIZE_WIDTH-1:0] my_size_i
      );
      // Generic task to send response with data to DCS.
      curr_hdr = my_hdr_i;
      lcl_rsp_wod_hdr_i = my_hdr_i;
      // data is ignored. 
      lcl_rsp_wod_pkt_size_i = my_size_i;
      lcl_rsp_wod_pkt_vc_i = my_vc_i;
      lcl_rsp_wod_pkt_valid_i = 1'b1;
      wait(lcl_rsp_wod_pkt_valid_i & lcl_rsp_wod_pkt_ready_o);
      ##1;
      lcl_rsp_wod_pkt_valid_i = 1'b0;
   endtask //send_rsp_wd

   //---------------------------------------------------------------
   // Functions to generate requests.
   // RLDT, RLDD, RLDI, RC2D_S - Req wo data.
   
   function automatic eci_word_t gen_req_wo_data
     (
      input 	  eci_id_t rreq_id_i,
      input 	  eci_dmask_t dmask_i,
      input 	  eci_address_t addr_i,
      input logic rldt_i,
      input logic rldi_i,
      input logic rldd_i,
      input logic rc2d_s_i,
      input logic rldx_i
      );
      eci_word_t my_hdr;
      my_hdr.eci_word = '0;
      if(rldt_i) begin
	 my_hdr.mreq_load.opcode = ECI_CMD_MREQ_RLDT;
      end
      if(rldi_i) begin
	 my_hdr.mreq_load.opcode = ECI_CMD_MREQ_RLDI;
      end      
      if(rldd_i) begin
	 my_hdr.mreq_load.opcode = ECI_CMD_MREQ_RLDD;
      end
      if(rc2d_s_i) begin
	 my_hdr.mreq_load.opcode = ECI_CMD_MREQ_RC2D_S;
      end
      if(rldx_i) begin
	 my_hdr.mreq_load.opcode = ECI_CMD_MREQ_RLDX;
      end
      my_hdr.mreq_load.rreq_id = rreq_id_i;
      my_hdr.mreq_load.dmask = dmask_i;
      my_hdr.mreq_load.address = addr_i;
      return(my_hdr);
   endfunction : gen_req_wo_data

   
   function automatic eci_word_t gen_hak_hdr
     (
      input 	  eci_word_t fwd_req_i,
      input logic vicdhi_i,
      input logic vicdhi_n_i,
      input logic hakd_i,
      input logic hakd_n_i,
      input logic hakn_s_i,
      input logic haki_i,
      input logic haks_i,
      input logic hakv_i
      );
      eci_word_t my_hdr;
      my_hdr.eci_word = '0;
      if(vicdhi_i | vicdhi_n_i) begin
	 my_hdr.mrsp_3to8.opcode = ECI_CMD_MRSP_VICDHI;
      end
      if(hakd_i | hakd_n_i) begin
	 my_hdr.mrsp_3to8.opcode = ECI_CMD_MRSP_HAKD;
      end
      if(hakn_s_i) begin
	 my_hdr.mrsp_3to8.opcode = ECI_CMD_MRSP_HAKN_S;
      end
      if(haki_i) begin
	 my_hdr.mrsp_3to8.opcode = ECI_CMD_MRSP_HAKI;
      end
      if(haks_i) begin
	 my_hdr.mrsp_3to8.opcode = ECI_CMD_MRSP_HAKS;
      end
      if(hakv_i) begin
	 my_hdr.mrsp_3to8.opcode = ECI_CMD_MRSP_HAKV;
      end
      my_hdr.mrsp_3to8.hreq_id = fwd_req_i.mfwd_generic.hreq_id;
      my_hdr.mrsp_3to8.dmask = fwd_req_i.mfwd_generic.dmask;
      if(vicdhi_n_i | hakd_n_i | hakn_s_i | haks_i | hakv_i | haki_i) begin
	 // A31, A21, A32, A22, A11, A11 - no data 
	 my_hdr.mrsp_3to8.dmask = '0;
      end
      my_hdr.mrsp_3to8.ns = fwd_req_i.mfwd_generic.ns;
      my_hdr.mrsp_3to8.address = fwd_req_i.mfwd_generic.address;
      return(my_hdr);
   endfunction : gen_hak_hdr
   
   
   function automatic eci_word_t gen_vic_hdr
     (
      input eci_address_t my_addr_i,
      input eci_dmask_t my_dmask_i,
      input vics_i, // V21
      input vicc_i, // V32d
      input vicd_i, // V31d
      input vicd_n_i, // V31
      input vicc_n_i // V32
      );
      eci_word_t my_hdr;
      my_hdr.eci_word = '0;
      if(vics_i) begin
	 my_hdr.mrsp_0to2.opcode = ECI_CMD_MRSP_VICS;
	 my_hdr.mrsp_0to2.dmask = '0;
      end
      if(vicd_n_i | vicd_i) begin
	 my_hdr.mrsp_0to2.opcode = ECI_CMD_MRSP_VICD;
	 if(vicd_n_i) begin
	    my_hdr.mrsp_0to2.dmask = '0;
	 end else begin
	    my_hdr.mrsp_0to2.dmask = my_dmask_i;
	 end
      end
      if(vicc_n_i | vicc_i) begin
	 my_hdr.mrsp_0to2.opcode = ECI_CMD_MRSP_VICC;
	 if(vicc_n_i) begin
	    my_hdr.mrsp_0to2.dmask = '0;
	 end else begin
	    my_hdr.mrsp_0to2.dmask = my_dmask_i;
	 end
      end
      my_hdr.mrsp_0to2.ns = 1'b1;
      my_hdr.mrsp_0to2.address = my_addr_i;
      return(my_hdr);
   endfunction : gen_vic_hdr

   function automatic eci_word_t gen_lcl_mfwd_hdr
     (
      input 	  eci_hreqid_t my_id_i,
      input 	  eci_dmask_t my_dmask_i,
      input logic my_ns_i,
      input 	  eci_address_t my_addr_i,
      // local clean.
      input logic lc_i,
      // local clean invalidate. 
      input logic lci_i
      );
      eci_word_t my_hdr;
      my_hdr.eci_word = '0;
      if(lc_i) begin
	 // generate LC header.
	 my_hdr.lcl_mfwd_generic.opcode = LCL_CMD_MFWD_CLEAN;
      end else begin
	 // generate LCI header.
	 my_hdr.lcl_mfwd_generic.opcode = LCL_CMD_MFWD_CLEAN_INV;
      end
      my_hdr.lcl_mfwd_generic.hreq_id = my_id_i;
      my_hdr.lcl_mfwd_generic.dmask = my_dmask_i;
      my_hdr.lcl_mfwd_generic.ns = my_ns_i;
      my_hdr.lcl_mfwd_generic.address = my_addr_i;
      return(my_hdr);
   endfunction : gen_lcl_mfwd_hdr

   function automatic eci_word_t gen_lcl_ul_hdr
     (
      input eci_address_t my_addr_i
      );
      eci_word_t my_hdr;
      my_hdr.eci_word = '0;
      my_hdr.lcl_unlock.opcode = LCL_CMD_MRSP_UNLOCK;
      my_hdr.lcl_unlock.address = my_addr_i;
      return(my_hdr);
   endfunction : gen_lcl_ul_hdr

endmodule
