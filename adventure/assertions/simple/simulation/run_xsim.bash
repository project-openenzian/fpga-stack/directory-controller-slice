#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
$XILINX_VIVADO/data/verilog/src/glbl.v \
../testbench/simpleTb.sv \
../rtl/simple.sv 

xelab -debug typical -incremental -L xpm worklib.simpleTb worklib.glbl -s worklib.simpleTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.simpleTb 
xsim -R worklib.simpleTb 
