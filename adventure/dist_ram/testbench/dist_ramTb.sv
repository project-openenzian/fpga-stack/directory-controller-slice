
module dist_ramTb();


   //input output ports 
   //Input signals
   logic 	      clk;
   logic [4:0] 	      addra;
   logic [511:0]      dia;
   logic 	      ena;
   logic 	      wea;
   logic [4:0] 	      addrb;
   logic 	      enb;

   //Output signals
   logic [511:0]      dob;


   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   dist_ram dist_ram1 (
		       .clk(clk),
		       .addra(addra),
		       .dia(dia),
		       .ena(ena),
		       .wea(wea),
		       .addrb(addrb),
		       .enb(enb),
		       .dob(dob)
		       );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports 
      clk = '0;
      addra = '0;
      dia = '0;
      ena = '0;
      wea = '0;
      addrb = '0;
      enb = '0;
      ##5;
      write(
	    .wr_addr_i(5'd0),
	    .wr_data_i(512'd64)
	    );
      write(
	    .wr_addr_i(5'd1),
	    .wr_data_i(512'd128)
	    );
      read(
	   .rd_addr_i(5'd0)
	   );
      read(
	   .rd_addr_i(5'd1)
	   );

      #500 $finish;
   end 

   task static write(
		     input logic [4:0] 	 wr_addr_i,
		     input logic [511:0] wr_data_i
		     );
      addra = wr_addr_i;
      dia = wr_data_i;
      ena = 1'b1;
      wea = 1'b1;
      ##1;
      ena = 1'b0;
      wea = 1'b0;
   endtask //write

   task static read(
		    input logic [4:0] rd_addr_i
		    );
      addrb = rd_addr_i;
      enb = 1'b1;
      ##1;
      enb = 1'b0;
      $display("Read value: %0d\n", dob);
   endtask //read
endmodule
