/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-09-16
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */


`ifndef DCS_SINGLE_DCU_SV
`define DCS_SINGLE_DCU_SV

import eci_cmd_defs::*;
import eci_dcs_defs::*;
import eci_dirc_defs::*; // DCU specific defs. 
import eci_cc_defs::*;   // CC specific defs to support dirc debug interface.

// Debug and performance counters are not currently
// exposed to software. This will eventually have
// to be connected to CPU.

module dcs_single_dcu #
  (
   parameter PERF_REGS_WIDTH = 64,
   parameter SYNTH_PERF_REGS = 0 // 0,1 
   )
   (
    input logic 					   clk,
    input logic 					   reset,
    
    // Input ECI header only events.
    // Request without data: VC 6 or 7.
    // Response without data: VC 10 or 11.
    input logic [ECI_WORD_WIDTH-1:0] 			   lo_wod_hdr_i,
    input logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   lo_wod_pkt_size_i,
    input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 		   lo_wod_pkt_vc_i,
    input logic 					   lo_wod_pkt_valid_i,
    output logic 					   lo_wod_pkt_ready_o,

    // Input ECI header + data events.
    // Response with data: VC 4 or 5.
    input logic [ECI_PACKET_SIZE-1:0][ECI_WORD_WIDTH-1:0]  hi_wd_pkt_i,
    input logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   hi_wd_pkt_size_i,
    input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 		   hi_wd_pkt_vc_i,
    input logic 					   hi_wd_pkt_valid_i,
    output logic 					   hi_wd_pkt_ready_o,

    // Output ECI header only events.
    // Response without data: VC 10 or 11.
    output logic [ECI_WORD_WIDTH-1:0] 			   lo_wod_hdr_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   lo_wod_pkt_size_o,
    output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 		   lo_wod_pkt_vc_o,
    output logic 					   lo_wod_pkt_valid_o,
    input logic 					   lo_wod_pkt_ready_i,

    // Output ECI header + data events.
    // Response with data: VC 4 or 5.
    output logic [ECI_PACKET_SIZE-1:0][ECI_WORD_WIDTH-1:0] hi_wd_pkt_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   hi_wd_pkt_size_o,
    output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 		   hi_wd_pkt_vc_o,
    output logic 					   hi_wd_pkt_valid_o,
    input logic 					   hi_wd_pkt_ready_i,

    // Output Read descriptors
    // Read descriptors: Request and response.
    output logic [MAX_DCU_ID_WIDTH-1:0] 		   rd_req_id_o,
    output logic [DS_ADDR_WIDTH-1:0] 			   rd_req_addr_o,
    output logic 					   rd_req_valid_o,
    input logic 					   rd_req_ready_i,

    input logic [MAX_DCU_ID_WIDTH-1:0] 			   rd_rsp_id_i,
    input logic [ECI_CL_WIDTH-1:0] 			   rd_rsp_data_i,
    input logic 					   rd_rsp_valid_i,
    output logic 					   rd_rsp_ready_o,

    // Write descriptors: Request and response.
    output logic [MAX_DCU_ID_WIDTH-1:0] 		   wr_req_id_o,
    output logic [DS_ADDR_WIDTH-1:0] 			   wr_req_addr_o,
    output logic [ECI_CL_WIDTH-1:0] 			   wr_req_data_o,
    output logic [ECI_CL_SIZE_BYTES-1:0] 		   wr_req_strb_o, 
    output logic 					   wr_req_valid_o,
    input logic 					   wr_req_ready_i,

    input logic [MAX_DCU_ID_WIDTH-1:0] 			   wr_rsp_id_i,
    input logic [1:0] 					   wr_rsp_bresp_i,
    input logic 					   wr_rsp_valid_i,
    output logic 					   wr_rsp_ready_o
    );

   localparam ECI_HDR_PKT_WIDTH = ECI_WORD_WIDTH + ECI_PACKET_SIZE_WIDTH + 
				  ECI_LCL_TOT_NUM_VCS_WIDTH;
   
   
   typedef struct packed {
      logic [ECI_WORD_WIDTH-1:0]        hdr;
      logic [ECI_PACKET_SIZE_WIDTH-1:0] size;
      logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] vc;
      logic				valid;
      logic				ready;
   } eci_hdr_if_t;

   typedef struct packed {
      logic [ECI_PACKET_SIZE-1:0][ECI_WORD_WIDTH-1:0] pkt;
      logic [ECI_PACKET_SIZE_WIDTH-1:0] 	      size;
      logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	      vc;
      logic					      valid;
      logic					      ready;
   } eci_pkt_if_t;

   typedef struct packed {
      logic [MAX_DCU_ID_WIDTH-1:0]		   id;
      logic [DS_ADDR_WIDTH-1:0]			   addr;
      logic					   valid;
      logic					   ready;
   } rd_req_ctrl_if_t;

   typedef struct packed {
      logic [MAX_DCU_ID_WIDTH-1:0] id;
      logic 			   valid;
      logic 			   ready;
   } rd_rsp_ctrl_if_t;

   typedef logic [ECI_CL_WIDTH-1:0] rd_rsp_data_if_t;

   typedef struct packed {
      logic [MAX_DCU_ID_WIDTH-1:0]  id;
      logic [DS_ADDR_WIDTH-1:0]     addr;
      logic [ECI_CL_SIZE_BYTES-1:0] strb; 
      logic 			    valid;
      logic 			    ready;
   } wr_req_ctrl_if_t;

   typedef logic [ECI_CL_WIDTH-1:0] wr_req_data_if_t;

   typedef struct packed {
      logic [MAX_DCU_ID_WIDTH-1:0]  id;
      logic [1:0] 		    bresp;
      logic 			    valid;
      logic 			    ready;
   } wr_rsp_ctrl_if_t;

   typedef struct packed {
      logic [PERF_REGS_WIDTH-1:0] no_req_pkt_rdy;
      logic [PERF_REGS_WIDTH-1:0] no_req_pkt_stl;
      logic [PERF_REGS_WIDTH-1:0] no_rsp_pkt_sent;
      logic [PERF_REGS_WIDTH-1:0] no_rd_req;
      logic [PERF_REGS_WIDTH-1:0] no_rd_rsp;
      logic [PERF_REGS_WIDTH-1:0] no_wr_req;
      logic [PERF_REGS_WIDTH-1:0] no_wr_rsp;
      logic [PERF_REGS_WIDTH-1:0] no_cyc_bw_req_vld_rdy;
   } perf_counters_if_t;

   typedef struct packed {
      // Debug interface.
      logic [ECI_WORD_WIDTH-1:0]        eci_req_hdr;
      logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] eci_req_vc;
      logic [ECI_PACKET_SIZE_WIDTH-1:0] eci_req_size;
      logic 				eci_req_ready;
      logic 				eci_req_stalled;
      logic [ECI_WORD_WIDTH-1:0] 	eci_rsp_hdr;
      logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] eci_rsp_vc;
      logic [ECI_PACKET_SIZE_WIDTH-1:0] eci_rsp_size;
      logic 				eci_rsp_valid;
      logic 				rtg_full;
      cc_req_t				req_cc_enc;
      logic 				rdda;
      logic 				wdda;
      cc_req_t				cc_active_req;
      cc_state_t			cc_present_state;
      cc_state_t			cc_next_state;
      cc_action_t			cc_next_action;
      dirc_err_t			err_code;
      logic 				error;
      logic 				valid;
   } dbg_if_t;

   typedef logic [DS_DCU_IDX_WIDTH-1:0] dcu_idx_t;
   typedef logic [DS_DCU_ID_WIDTH-1:0] 	dcu_id_t;

   // DCU specific signals.
   eci_hdr_if_t dc_req_wod_i, dc_rsp_wod_i, dc_rsp_wd_hdr_i, dc_eci_rsp_hdr_o;
   rd_req_ctrl_if_t dc_rd_req_o;
   rd_rsp_ctrl_if_t dc_rd_rsp_ctrl_i;
   wr_req_ctrl_if_t dc_wr_req_ctrl_o;
   wr_rsp_ctrl_if_t dc_wr_rsp_ctrl_i;
   perf_counters_if_t prf_o;
   dbg_if_t dbg_o;
   // Read data path specific signals.
   rd_rsp_ctrl_if_t rdp_rsp_mem_ctrl_i;
   rd_rsp_data_if_t rdp_rsp_mem_data_i;
   rd_rsp_ctrl_if_t rdp_rsp_dcu_ctrl_o;
   dcu_idx_t rdp_rsp_dcu_idx_o;
   eci_hdr_if_t rdp_rsp_wd_hdr_i;
   eci_pkt_if_t rdp_rsp_wd_pkt_o;
   // Write data path specific signals.
   eci_pkt_if_t wdp_rsp_wd_pkt_i;
   dcu_idx_t wdp_rsp_wd_dcu_idx_o;
   eci_hdr_if_t wdp_rsp_wd_hdr_o;
   wr_req_ctrl_if_t wdp_req_dcu_ctrl_i;
   wr_req_ctrl_if_t wdp_req_mem_ctrl_o;
   wr_req_data_if_t wdp_req_mem_data_o;
   // Lo hdr DCU idx extract signals.
   ds_cl_addr_t lo_wod_addr_c;
   dcu_idx_t 	lo_wod_dcu_idx;
   // ECI header priority arb specific signals
   logic [1:0][DS_DCU_IDX_WIDTH-1:0] ecih_parb_us_id_i;
   logic [1:0][ECI_HDR_PKT_WIDTH-1:0] ecih_parb_us_data_i;
   logic [1:0]			     ecih_parb_us_valid_i;
   logic [1:0]			     ecih_parb_us_ready_o;
   logic [DS_DCU_IDX_WIDTH-1:0]      ecih_parb_ds_id_o;
   eci_hdr_if_t ecih_parb_ds_o;
   // HI/LO to VC router signals.
   eci_hdr_if_t hlvr_ecih_i;
   eci_hdr_if_t hlvr_req_wod_o, hlvr_rsp_wod_o, hlvr_rsp_wd_hdr_o;
   // VC to HI/LO router signals.
   eci_hdr_if_t vhlr_ecih_i;
   eci_hdr_if_t vhlr_lo_wod_o, vhlr_hi_wd_hdr_o;
   
   always_comb begin : OUT_ASSIGN
      // Request without data: VC 6 or 7.
      // Response without data: VC 10 or 11.
      lo_wod_pkt_ready_o = ecih_parb_us_ready_o[1];
      // Response with data: VC 4 or 5.
      hi_wd_pkt_ready_o = wdp_rsp_wd_pkt_i.ready;
      // Output ECI header only events.
      // Response without data: VC 10 or 11.
      lo_wod_hdr_o = vhlr_lo_wod_o.hdr;
      lo_wod_pkt_size_o = vhlr_lo_wod_o.size;
      lo_wod_pkt_vc_o = vhlr_lo_wod_o.vc;
      lo_wod_pkt_valid_o = vhlr_lo_wod_o.valid;
      // Output ECI header + data events.
      // Response with data: VC 4 or 5.
      hi_wd_pkt_o = rdp_rsp_wd_pkt_o.pkt;
      hi_wd_pkt_size_o = rdp_rsp_wd_pkt_o.size;
      hi_wd_pkt_vc_o = rdp_rsp_wd_pkt_o.vc;
      hi_wd_pkt_valid_o = rdp_rsp_wd_pkt_o.valid;
      // Output Read descriptors
      // Read descriptors: Request and response.
      rd_req_id_o = dc_rd_req_o.id;
      rd_req_addr_o = dc_rd_req_o.addr;
      rd_req_valid_o = dc_rd_req_o.valid;
      // Read descriptors: Response. 
      rd_rsp_ready_o = rdp_rsp_mem_ctrl_i.ready;
      // Write descriptors: Request and response.
      wr_req_id_o = wdp_req_mem_ctrl_o.id;
      wr_req_addr_o = wdp_req_mem_ctrl_o.addr;
      wr_req_data_o = wdp_req_mem_data_o;
      wr_req_strb_o = wdp_req_mem_ctrl_o.strb;
      wr_req_valid_o = wdp_req_mem_ctrl_o.valid;
      // write descriptors: Response. 
      wr_rsp_ready_o =  dc_wr_rsp_ctrl_i.ready;
   end : OUT_ASSIGN

   // Get the DCU IDX from Lo ECI header.
   // Cast the CL byte address into DCS Cl addr type
   // to extract dcu_id and pass it to DCS function
   // to get the dcu idx. 
   always_comb begin : GET_LO_HDR_DCU_IDX
      lo_wod_addr_c = ds_cl_addr_t'(lo_wod_hdr_i[ECI_ADDR_WIDTH-1:0]);
      lo_wod_dcu_idx = f_dcuid_2_dcuidx(.dcu_id_i(lo_wod_addr_c.dcu_id));
   end : GET_LO_HDR_DCU_IDX

   // Hi and Lo headers are arbitered
   // to get one header every cycle.
   // Lo headers get 1 odd/even CL event every 3 cycles.
   // Hi headers get 1 odd/even CL event every 3 cycles.
   // Maximum at input we have 2 events every 3 cycles.
   // Module can choose 1 event every cycle which is
   // greater than input throughput.
   always_comb begin : ECIH_PARB_IP_ASSIGN
      // First priority to HI hdr. 
      ecih_parb_us_id_i[0] = wdp_rsp_wd_dcu_idx_o;
      ecih_parb_us_data_i[0] = {
				wdp_rsp_wd_hdr_o.hdr, 
				wdp_rsp_wd_hdr_o.size, 
				wdp_rsp_wd_hdr_o.vc
				};
      ecih_parb_us_valid_i[0] = wdp_rsp_wd_hdr_o.valid;
      // Second priority to LO hdr.
      ecih_parb_us_id_i[1] = lo_wod_dcu_idx;
      ecih_parb_us_data_i[1] = {
				lo_wod_hdr_i,
				lo_wod_pkt_size_i,
				lo_wod_pkt_vc_i
				};
      ecih_parb_us_valid_i[1] = lo_wod_pkt_valid_i;
      // Arbitrated ECI header.
      ecih_parb_ds_o.ready = hlvr_ecih_i.ready;
   end : ECIH_PARB_IP_ASSIGN
   axis_comb_priority_enc #
     (
      .ID_WIDTH(DS_DCU_IDX_WIDTH),
      .DATA_WIDTH(ECI_HDR_PKT_WIDTH),
      .NUM_IN(2)
       )
   ecih_priority_select1
     (
      // Hi, Lo HDRs input.
      // HI - index 0, LO index 1.
      .us_id_i(ecih_parb_us_id_i),
      .us_data_i(ecih_parb_us_data_i),
      .us_valid_i(ecih_parb_us_valid_i),
      .us_ready_o(ecih_parb_us_ready_o),
      // arbitrated ECI header.
      .ds_id_o(ecih_parb_ds_id_o), // Not connected as there is only 1 DCU. 
      .ds_data_o({
		  ecih_parb_ds_o.hdr,
		  ecih_parb_ds_o.size,
		  ecih_parb_ds_o.vc
		  }),
      .ds_valid_o(ecih_parb_ds_o.valid),
      .ds_ready_i(ecih_parb_ds_o.ready)
      );

   // Route arbitrated header to appropriate VC.
   // Output channels connected to DCU.
   always_comb begin : PARB_TO_VC_IP_ASSIGN
      // Input ECI header + VC number.
      hlvr_ecih_i.hdr = ecih_parb_ds_o.hdr;
      hlvr_ecih_i.size = ecih_parb_ds_o.size;
      hlvr_ecih_i.vc = ecih_parb_ds_o.vc;
      hlvr_ecih_i.valid = ecih_parb_ds_o.valid; 
      // Output ECI header for req without data: VC 6/7.
      hlvr_req_wod_o.ready = dc_req_wod_i.ready;
      // Output ECI header for rsp without data: VC 10/11
      hlvr_rsp_wod_o.ready = dc_rsp_wod_i.ready;
      // Output ECI header for rsp with data: VC 4/5.
      hlvr_rsp_wd_hdr_o.ready = dc_rsp_wd_hdr_i.ready;
   end : PARB_TO_VC_IP_ASSIGN
   hilo_vc_router
     parb_to_vc_rtr
       (
	.clk(clk),
	.reset(reset),
	// Input ECI header + VC number.
	.ecih_hdr_i		(hlvr_ecih_i.hdr),
	.ecih_pkt_size_i	(hlvr_ecih_i.size),
	.ecih_pkt_vc_i		(hlvr_ecih_i.vc),
	.ecih_pkt_valid_i	(hlvr_ecih_i.valid),
	.ecih_pkt_ready_o	(hlvr_ecih_i.ready),
	// Output ECI header for req without data: VC 6/7.
	.req_wod_hdr_o		(hlvr_req_wod_o.hdr),
	.req_wod_pkt_size_o	(hlvr_req_wod_o.size),
	.req_wod_pkt_vc_o	(hlvr_req_wod_o.vc),
	.req_wod_pkt_valid_o	(hlvr_req_wod_o.valid),
	.req_wod_pkt_ready_i	(hlvr_req_wod_o.ready),
	// Output ECI header for rsp without data: VC 10/11
	.rsp_wod_hdr_o		(hlvr_rsp_wod_o.hdr),
	.rsp_wod_pkt_size_o	(hlvr_rsp_wod_o.size),
	.rsp_wod_pkt_vc_o	(hlvr_rsp_wod_o.vc),
	.rsp_wod_pkt_valid_o	(hlvr_rsp_wod_o.valid),
	.rsp_wod_pkt_ready_i	(hlvr_rsp_wod_o.ready),
	// Output ECI header for rsp with data: VC 4/5.
	.rsp_wd_hdr_o		(hlvr_rsp_wd_hdr_o.hdr),
	.rsp_wd_pkt_size_o	(hlvr_rsp_wd_hdr_o.size),
	.rsp_wd_pkt_vc_o	(hlvr_rsp_wd_hdr_o.vc),
	.rsp_wd_pkt_valid_o	(hlvr_rsp_wd_hdr_o.valid),
	.rsp_wd_pkt_ready_i	(hlvr_rsp_wd_hdr_o.ready)
	);

   always_comb begin : DCS_DCU_IN_ASSIGN
      // Incoming req without data hdr (VC 6,7).
      dc_req_wod_i.hdr		= hlvr_req_wod_o.hdr;
      dc_req_wod_i.size		= hlvr_req_wod_o.size;
      dc_req_wod_i.vc		= hlvr_req_wod_o.vc;
      dc_req_wod_i.valid	= hlvr_req_wod_o.valid;
      // Incoming rsp without data hdr (VC 10,11).
      dc_rsp_wod_i.hdr		= hlvr_rsp_wod_o.hdr;
      dc_rsp_wod_i.size		= hlvr_rsp_wod_o.size;
      dc_rsp_wod_i.vc		= hlvr_rsp_wod_o.vc;
      dc_rsp_wod_i.valid	= hlvr_rsp_wod_o.valid;
      // Incoming rsp with data hdr (VC 4,5).
      dc_rsp_wd_hdr_i.hdr	= hlvr_rsp_wd_hdr_o.hdr;
      dc_rsp_wd_hdr_i.size	= hlvr_rsp_wd_hdr_o.size;
      dc_rsp_wd_hdr_i.vc	= hlvr_rsp_wd_hdr_o.vc;
      dc_rsp_wd_hdr_i.valid	= hlvr_rsp_wd_hdr_o.valid;
      // Outgoing ECI headers (no data).
      dc_eci_rsp_hdr_o.ready	= vhlr_ecih_i.ready;
      // Outgoing Read request.
      dc_rd_req_o.ready		= rd_req_ready_i;
      // Incoming Read response	(without data).
      dc_rd_rsp_ctrl_i.id	= rdp_rsp_dcu_ctrl_o.id;
      dc_rd_rsp_ctrl_i.valid	= rdp_rsp_dcu_ctrl_o.valid;
      // Outgoing write request (without data).
      dc_wr_req_ctrl_o.ready	= wdp_req_dcu_ctrl_i.ready;
      // Incoming write response.
      dc_wr_rsp_ctrl_i.id	= wr_rsp_id_i;
      dc_wr_rsp_ctrl_i.bresp	= wr_rsp_bresp_i;
      dc_wr_rsp_ctrl_i.valid	= wr_rsp_valid_i;
   end : DCS_DCU_IN_ASSIGN
   dcu_top #
     (
      .PERF_REGS_WIDTH(PERF_REGS_WIDTH),
      .SYNTH_PERF_REGS(SYNTH_PERF_REGS)
      )
   dcs_dcu_inst
     (
      .clk(clk),
      .reset(reset),
      // Incoming ECI headers (no data).
      // Incoming req without data hdr (VC 6,7).
      .req_wod_hdr_i			(dc_req_wod_i.hdr),
      .req_wod_pkt_size_i		(dc_req_wod_i.size),
      .req_wod_pkt_vc_i			(dc_req_wod_i.vc),
      .req_wod_pkt_valid_i		(dc_req_wod_i.valid),
      .req_wod_pkt_ready_o		(dc_req_wod_i.ready),
      // Incoming rsp without data hdr (VC 10,11).
      .rsp_wod_hdr_i			(dc_rsp_wod_i.hdr),
      .rsp_wod_pkt_size_i		(dc_rsp_wod_i.size),
      .rsp_wod_pkt_vc_i			(dc_rsp_wod_i.vc),
      .rsp_wod_pkt_valid_i		(dc_rsp_wod_i.valid),
      .rsp_wod_pkt_ready_o		(dc_rsp_wod_i.ready),
      // Incoming rsp with data hdr (VC 4,5).
      .rsp_wd_hdr_i			(dc_rsp_wd_hdr_i.hdr),
      .rsp_wd_pkt_size_i		(dc_rsp_wd_hdr_i.size),
      .rsp_wd_pkt_vc_i			(dc_rsp_wd_hdr_i.vc),
      .rsp_wd_pkt_valid_i		(dc_rsp_wd_hdr_i.valid),
      .rsp_wd_pkt_ready_o		(dc_rsp_wd_hdr_i.ready),
      // Outgoing ECI headers (no data).
      // Currently only rsp with data (VC 4,5).
      // and rsp without data hdr (VC 10,11).
      .eci_rsp_hdr_o			(dc_eci_rsp_hdr_o.hdr),
      .eci_rsp_size_o			(dc_eci_rsp_hdr_o.size),
      .eci_rsp_vc_o			(dc_eci_rsp_hdr_o.vc),
      .eci_rsp_valid_o			(dc_eci_rsp_hdr_o.valid),
      .eci_rsp_ready_i			(dc_eci_rsp_hdr_o.ready),
      // Read descriptors to byte addressable memory.
      // Outgoing Read request.
      .rd_req_id_o			(dc_rd_req_o.id),
      .rd_req_addr_o			(dc_rd_req_o.addr),
      .rd_req_valid_o			(dc_rd_req_o.valid),
      .rd_req_ready_i			(dc_rd_req_o.ready),
      // Incoming Read response	(without data).
      .rd_rsp_id_i			(dc_rd_rsp_ctrl_i.id),
      .rd_rsp_valid_i			(dc_rd_rsp_ctrl_i.valid),
      .rd_rsp_ready_o			(dc_rd_rsp_ctrl_i.ready),
      // write descriptors to byte addressable memory.
      // Outgoing write request (without data).
      .wr_req_id_o			(dc_wr_req_ctrl_o.id),
      .wr_req_addr_o			(dc_wr_req_ctrl_o.addr),
      .wr_req_strb_o			(dc_wr_req_ctrl_o.strb),
      .wr_req_valid_o			(dc_wr_req_ctrl_o.valid),
      .wr_req_ready_i			(dc_wr_req_ctrl_o.ready),
      // Incoming write response.
      .wr_rsp_id_i			(dc_wr_rsp_ctrl_i.id),
      .wr_rsp_bresp_i			(dc_wr_rsp_ctrl_i.bresp),
      .wr_rsp_valid_i			(dc_wr_rsp_ctrl_i.valid),
      .wr_rsp_ready_o			(dc_wr_rsp_ctrl_i.ready),
      // Performance counters. 
      .prf_no_req_pkt_rdy_o		(prf_o.no_req_pkt_rdy),        // Not connected. 	
      .prf_no_req_pkt_stl_o		(prf_o.no_req_pkt_stl),	       // Not connected. 	
      .prf_no_rsp_pkt_sent_o		(prf_o.no_rsp_pkt_sent),       // Not connected. 	
      .prf_no_rd_req_o			(prf_o.no_rd_req),	       // Not connected. 	
      .prf_no_rd_rsp_o			(prf_o.no_rd_rsp),	       // Not connected. 	
      .prf_no_wr_req_o			(prf_o.no_wr_req),	       // Not connected. 	
      .prf_no_wr_rsp_o			(prf_o.no_wr_rsp),	       // Not connected. 	
      .prf_no_cyc_bw_req_vld_rdy_o	(prf_o.no_cyc_bw_req_vld_rdy), // Not connected. 	
      // Debug signals. 
      .dbg_eci_req_hdr_o		(dbg_o.eci_req_hdr),           // Not connected. 	
      .dbg_eci_req_vc_o			(dbg_o.eci_req_vc),	       // Not connected. 	
      .dbg_eci_req_size_o		(dbg_o.eci_req_size),	       // Not connected. 	
      .dbg_eci_req_ready_o		(dbg_o.eci_req_ready),	       // Not connected. 	
      .dbg_eci_req_stalled_o		(dbg_o.eci_req_stalled),       // Not connected. 	
      .dbg_eci_rsp_hdr_o		(dbg_o.eci_rsp_hdr),	       // Not connected. 	
      .dbg_eci_rsp_vc_o			(dbg_o.eci_rsp_vc),	       // Not connected. 	
      .dbg_eci_rsp_size_o		(dbg_o.eci_rsp_size),	       // Not connected. 	
      .dbg_eci_rsp_valid_o		(dbg_o.eci_rsp_valid),	       // Not connected.
      .dbg_rtg_full_o                   (dbg_o.rtg_full),              // Not connected.
      .dbg_req_cc_enc_o			(dbg_o.req_cc_enc),	       // Not connected. 	
      .dbg_rdda_o			(dbg_o.rdda),		       // Not connected. 	
      .dbg_wdda_o			(dbg_o.wdda),		       // Not connected. 	
      .dbg_cc_active_req_o		(dbg_o.cc_active_req),	       // Not connected. 	
      .dbg_cc_present_state_o		(dbg_o.cc_present_state),      // Not connected. 	
      .dbg_cc_next_state_o		(dbg_o.cc_next_state),	       // Not connected. 	
      .dbg_cc_next_action_o		(dbg_o.cc_next_action),	       // Not connected. 	
      .dbg_err_code_o			(dbg_o.err_code),	       // Not connected. 	
      .dbg_error_o			(dbg_o.error),		       // Not connected. 	
      .dbg_valid_o			(dbg_o.valid)		       // Not connected. 	
      );

   // DCS header to TX HI/LO channels.
   // ECI header response from DCS is transmitted towards.
   // to HI/LO TX channels.
   // LO VC headers are directly connected to LO TX channel.
   // HI VC headers must retrieve data from read data path.
   always_comb begin : VC_HILO_RTR_IP_ASSIGN
      // Input ECI header + VC number.
      vhlr_ecih_i.hdr = dc_eci_rsp_hdr_o.hdr;
      vhlr_ecih_i.size = dc_eci_rsp_hdr_o.size;
      vhlr_ecih_i.vc = dc_eci_rsp_hdr_o.vc;
      vhlr_ecih_i.valid = dc_eci_rsp_hdr_o.valid; 
      // Output Lo channel.
      // VC: 6/7/8/9/10/11
      vhlr_lo_wod_o.ready = lo_wod_pkt_ready_i;
      // Output Hi channel
      // VC: 2/3/4/5
      vhlr_hi_wd_hdr_o.ready = rdp_rsp_wd_hdr_i.ready;
   end : VC_HILO_RTR_IP_ASSIGN
   vc_hilo_router
     vc_hilo_router1
       (
	.clk                    (clk),
	.reset                  (reset),
	// Input ECI header + VC number.
	.ecih_hdr_i		(vhlr_ecih_i.hdr),
	.ecih_pkt_size_i	(vhlr_ecih_i.size),
	.ecih_pkt_vc_i		(vhlr_ecih_i.vc),
	.ecih_pkt_valid_i	(vhlr_ecih_i.valid),
	.ecih_pkt_ready_o	(vhlr_ecih_i.ready),
	// Output Lo channel.
	// VC: 6/7/8/9/10/11
	.lo_wod_hdr_o		(vhlr_lo_wod_o.hdr),
	.lo_wod_pkt_size_o	(vhlr_lo_wod_o.size),
	.lo_wod_pkt_vc_o	(vhlr_lo_wod_o.vc),
	.lo_wod_pkt_valid_o	(vhlr_lo_wod_o.valid),
	.lo_wod_pkt_ready_i	(vhlr_lo_wod_o.ready),
	// Output Hi channel
	// VC: 2/3/4/5
	.hi_wd_hdr_o		(vhlr_hi_wd_hdr_o.hdr),
	.hi_wd_pkt_size_o	(vhlr_hi_wd_hdr_o.size),
	.hi_wd_pkt_vc_o		(vhlr_hi_wd_hdr_o.vc),
	.hi_wd_pkt_valid_o	(vhlr_hi_wd_hdr_o.valid),
	.hi_wd_pkt_ready_i	(vhlr_hi_wd_hdr_o.ready)
	);

   
   // Read from byte addressable mem data path.
   always_comb begin : RDP_IP_ASSIGN
      // Read resposne + data from byte addressable memory.
      rdp_rsp_mem_ctrl_i.id	= rd_rsp_id_i;
      rdp_rsp_mem_data_i	= rd_rsp_data_i;
      rdp_rsp_mem_ctrl_i.valid	= rd_rsp_valid_i;
      // read response only routed to DCU.
      rdp_rsp_dcu_ctrl_o.ready	= dc_rd_rsp_ctrl_i.ready;
      // VC 4,5 Input - header only from DCU
      rdp_rsp_wd_hdr_i.hdr	= vhlr_hi_wd_hdr_o.hdr;
      rdp_rsp_wd_hdr_i.size	= vhlr_hi_wd_hdr_o.size;
      rdp_rsp_wd_hdr_i.vc	= vhlr_hi_wd_hdr_o.vc;
      rdp_rsp_wd_hdr_i.valid	= vhlr_hi_wd_hdr_o.valid;
      // VC 4,5 Output header + data to TX to CPU.
      rdp_rsp_wd_pkt_o.ready	= hi_wd_pkt_ready_i;
   end : RDP_IP_ASSIGN
   rd_data_path 
     dcs_rd_data_path 
       (
	.clk			(clk),
	.reset			(reset),
	// Read resposne + data from byte addressable memory.
	.rd_rsp_id_i		(rdp_rsp_mem_ctrl_i.id),
	.rd_rsp_data_i		(rdp_rsp_mem_data_i),
	.rd_rsp_valid_i		(rdp_rsp_mem_ctrl_i.valid),
	.rd_rsp_ready_o		(rdp_rsp_mem_ctrl_i.ready),
	// read response only routed to DCU.
	.rd_rsp_dcu_idx_o	(rdp_rsp_dcu_idx_o), // Not connected since there is only 1 DCU.
	.rd_rsp_id_o		(rdp_rsp_dcu_ctrl_o.id),
	.rd_rsp_valid_o		(rdp_rsp_dcu_ctrl_o.valid),
	.rd_rsp_ready_i		(rdp_rsp_dcu_ctrl_o.ready),
	// VC 4,5 Input - header only from DCU
	.rsp_wd_hdr_i		(rdp_rsp_wd_hdr_i.hdr),
	.rsp_wd_pkt_size_i	(rdp_rsp_wd_hdr_i.size),
	.rsp_wd_pkt_vc_i	(rdp_rsp_wd_hdr_i.vc),
	.rsp_wd_pkt_valid_i	(rdp_rsp_wd_hdr_i.valid),
	.rsp_wd_pkt_ready_o	(rdp_rsp_wd_hdr_i.ready),
	// VC 4,5 Output header + data to TX to CPU.
	.rsp_wd_pkt_o		(rdp_rsp_wd_pkt_o.pkt),
	.rsp_wd_pkt_size_o	(rdp_rsp_wd_pkt_o.size),
	.rsp_wd_pkt_vc_o	(rdp_rsp_wd_pkt_o.vc),
	.rsp_wd_pkt_valid_o	(rdp_rsp_wd_pkt_o.valid),
	.rsp_wd_pkt_ready_i	(rdp_rsp_wd_pkt_o.ready)
	);

   // write to byte addressable mem data path.
   always_comb begin : WDP_IP_ASSIGN
      // ECI packet for response with data 
      // (VC 4 or 5). (header + data) from CPU. 
      wdp_rsp_wd_pkt_i.pkt	= hi_wd_pkt_i;
      wdp_rsp_wd_pkt_i.size	= hi_wd_pkt_size_i;
      wdp_rsp_wd_pkt_i.vc	= hi_wd_pkt_vc_i;
      wdp_rsp_wd_pkt_i.valid	= hi_wd_pkt_valid_i;
      // Output ECI header only for response with data
      // to be routed to the DCUs.
      wdp_rsp_wd_hdr_o.ready	= ecih_parb_us_ready_o[0];
      // Input Write descriptors: Request from DCUs. 
      // no data, only descriptor.
      wdp_req_dcu_ctrl_i.id	= dc_wr_req_ctrl_o.id;
      wdp_req_dcu_ctrl_i.addr	= dc_wr_req_ctrl_o.addr;
      wdp_req_dcu_ctrl_i.strb	= dc_wr_req_ctrl_o.strb;
      wdp_req_dcu_ctrl_i.valid	= dc_wr_req_ctrl_o.valid;
      // Output write request: descriptor + data i/f.
      // to byte addressable memory.
      wdp_req_mem_ctrl_o.ready	= wr_req_ready_i;
   end : WDP_IP_ASSIGN
   wr_data_path 
     dcs_wr_data_path 
       (
	.clk			(clk),
	.reset			(reset),
	// ECI packet for response with data 
	// (VC 4 or 5). (header + data) from CPU. 
	.rsp_wd_pkt_i		(wdp_rsp_wd_pkt_i.pkt),
	.rsp_wd_pkt_size_i	(wdp_rsp_wd_pkt_i.size),
	.rsp_wd_pkt_vc_i	(wdp_rsp_wd_pkt_i.vc),
	.rsp_wd_pkt_valid_i	(wdp_rsp_wd_pkt_i.valid),
	.rsp_wd_pkt_ready_o	(wdp_rsp_wd_pkt_i.ready),
	// Output ECI header only for response with data
	// to be routed to the DCUs.
	// DCU_IDX is for routing the ECI header, size, VC
	// to appropriate DCU.
	.rsp_wd_dcu_idx_o	(wdp_rsp_wd_dcu_idx_o),
	.rsp_wd_hdr_o		(wdp_rsp_wd_hdr_o.hdr),
	.rsp_wd_pkt_size_o	(wdp_rsp_wd_hdr_o.size),
	.rsp_wd_pkt_vc_o	(wdp_rsp_wd_hdr_o.vc),
	.rsp_wd_pkt_valid_o	(wdp_rsp_wd_hdr_o.valid),
	.rsp_wd_pkt_ready_i	(wdp_rsp_wd_hdr_o.ready),
	// Input Write descriptors: Request from DCUs. 
	// no data, only descriptor.
	.wr_req_id_i		(wdp_req_dcu_ctrl_i.id),
	.wr_req_addr_i		(wdp_req_dcu_ctrl_i.addr),
	.wr_req_strb_i		(wdp_req_dcu_ctrl_i.strb),
	.wr_req_valid_i		(wdp_req_dcu_ctrl_i.valid),
	.wr_req_ready_o		(wdp_req_dcu_ctrl_i.ready),
	// Output write request: descriptor + data i/f.
	// to byte addressable memory.
	.wr_req_id_o		(wdp_req_mem_ctrl_o.id),
	.wr_req_addr_o		(wdp_req_mem_ctrl_o.addr),
	.wr_req_strb_o		(wdp_req_mem_ctrl_o.strb),
	.wr_req_data_o		(wdp_req_mem_data_o),
	.wr_req_valid_o		(wdp_req_mem_ctrl_o.valid),
	.wr_req_ready_i		(wdp_req_mem_ctrl_o.ready)
	);
   
endmodule // dcs_single_dcu

`endif
