#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
      $XILINX_VIVADO/data/verilog/src/glbl.v \
      ../rtl/eci_cmd_defs.sv \
      ../testbench/dcu_to_vc_routerTb.sv \
      ../rtl/dcu_gen_ecih_vc_router.sv \
      ../rtl/axis_2_router.sv \
      ../rtl/dcu_to_vc_router.sv \
      ../rtl/axis_pipeline_stage.sv 

xelab -debug typical -incremental -L xpm worklib.dcu_to_vc_routerTb worklib.glbl -s worklib.dcu_to_vc_routerTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.dcu_to_vc_routerTb 
xsim -R worklib.dcu_to_vc_routerTb 
