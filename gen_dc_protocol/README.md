## Automated DC protocol state machine generation.
Generate the protocol state machine in CSV format
and convert it to CC-ROM HDL. The CC-ROM HDL contains
eci_cc_table.sv, which is the state machine ROM and
eci_cc_defs.sv, which contains package definitions
for the contents of eci_cc_table.sv

There are two flavors of the protocol
 * dc_stmc_auto_lock
   * Generates state machine where cachelines can be locked by FPGA.
   * This is the state machine that is currently in use.

 * dc_stmc_auto_no_lock
   * No locking capaibilites on the FPGA.

### Scripts description
 * eci_stmc_auto.pl
   * Generates the protocol state machine in CSV format.
 * cc_rom_from_cc_tbl.pl
   * Takes the protocol definition in CSV format and
   fills up two templates (eci_cc_defs.tmpl and eci_cc_table.tmpl)
   to generate eci_cc_defs.sv and eci_cc_table.sv

### Generating state machine
 * perl eci_stmc_auto.pl - Generates protocol state machine in CSV format (ECI_CC.csv).
 * perl cc_rom_from_cc_tbl /path/to/ECI_CC.csv
 