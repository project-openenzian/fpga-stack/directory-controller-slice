`ifndef CL_INV_LATENCY_SV
`define CL_INV_LATENCY_SV

module cl_inv_latency #
  (
   // odd cls 
   parameter PERF_REGS_WIDTH = 32,
   parameter ODD_OR_EVEN = 1,
   // 4MB worth of reads. 
   parameter NUM_REQ_TO_WAIT = 32768
   )
   (
    input logic 				 clk,
    input logic 				 reset,
    // Sampled addresses to invalidate.
    input logic [DS_ADDR_WIDTH-1:0] 		 rd_req_addr_i,
    input logic 				 rd_req_valid_i,
    // Output LCI VC 16 or 17
    output logic [ECI_WORD_WIDTH-1:0] 		 lcl_fwd_wod_hdr_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 lcl_fwd_wod_pkt_size_o,
    output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] lcl_fwd_wod_pkt_vc_o,
    output logic 				 lcl_fwd_wod_pkt_valid_o,
    input logic 				 lcl_fwd_wod_pkt_ready_i,
    // Input LCIA VC 18 or 19.
    input logic [ECI_WORD_WIDTH-1:0] 		 lcl_rsp_wod_hdr_i,
    input logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 lcl_rsp_wod_pkt_size_i,
    input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0]  lcl_rsp_wod_pkt_vc_i,
    input logic 				 lcl_rsp_wod_pkt_valid_i,
    output logic 				 lcl_rsp_wod_pkt_ready_o,
    // Output unlock VC 18 or 19.
    output logic [ECI_WORD_WIDTH-1:0] 		 lcl_rsp_wod_hdr_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 lcl_rsp_wod_pkt_size_o,
    output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] lcl_rsp_wod_pkt_vc_o,
    output logic 				 lcl_rsp_wod_pkt_valid_o,
    input logic 				 lcl_rsp_wod_pkt_ready_i
    );

   logic [PERF_REGS_WIDTH-1:0] 			 num_rd_elapsed_reg = 0;
   logic 					 cl_odd_even_reg = 0;

   enum {WAIT, RUN} state_reg, state_next;

   always_comb begin : CONTROLLER
      state_next = state_reg;
      case(state_reg)
	WAIT: begin
	   if(num_rd_elapsed_reg > NUM_REQ_TO_WAIT) begin
	      state_next = RUN;
	   end
	end
	RUN: begin
	   
	end
      endcase
   end : CONTROLLER

   always_ff @(posedge clk) begin : REG_ASSIGN
      state_reg <= state_next;
      if(rd_req_valid_i) begin
	 num_rd_elapsed_reg <= num_rd_elapsed_reg + 'd1;
	 cl_odd_even_reg <= rd_req_addr_i[ECI_CL_ADDR_LSB];
      end
      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset 
	 num_rd_elapsed_reg <= '0;
	 state_reg <= WAIT;
	 cl_odd_even_reg <= '0;
      end
   end : REG_ASSIGN

   
endmodule // cl_inv_latency

`endif
