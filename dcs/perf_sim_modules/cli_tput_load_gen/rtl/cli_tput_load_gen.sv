/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-11-24
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */


`ifndef CLI_TPUT_LOAD_GEN_SV
`define CLI_TPUT_LOAD_GEN_SV

import eci_cmd_defs::*;
import eci_dcs_defs::*;

// Module receives an aliased address to invalidte
// and generates LCI header to send to DCS.
// when response arrives, unlock is sent.
// Issues LCI as fast as possible to measure
// LCI Tput.
module cli_tput_load_gen #
  (
   parameter PERF_REGS_WIDTH = 32
   )
   (
    input logic 				 clk,
    input logic 				 reset,
    // Aliased address to invalidate.
    input logic [DS_ADDR_WIDTH-1:0] 		 al_addr_i,
    input logic 				 al_addr_valid_i,
    output logic 				 al_addr_ready_o,
    // Output LCI VC 16 or 17
    output logic [ECI_WORD_WIDTH-1:0] 		 lcl_fwd_wod_hdr_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 lcl_fwd_wod_pkt_size_o,
    output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] lcl_fwd_wod_pkt_vc_o,
    output logic 				 lcl_fwd_wod_pkt_valid_o,
    input logic 				 lcl_fwd_wod_pkt_ready_i,
    // Input LCIA VC 18 or 19.
    input logic [ECI_WORD_WIDTH-1:0] 		 lcl_rsp_wod_hdr_i,
    input logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 lcl_rsp_wod_pkt_size_i,
    input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0]  lcl_rsp_wod_pkt_vc_i,
    input logic 				 lcl_rsp_wod_pkt_valid_i,
    output logic 				 lcl_rsp_wod_pkt_ready_o,
    // Output unlock VC 18 or 19.
    output logic [ECI_WORD_WIDTH-1:0] 		 lcl_rsp_wod_hdr_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 lcl_rsp_wod_pkt_size_o,
    output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] lcl_rsp_wod_pkt_vc_o,
    output logic 				 lcl_rsp_wod_pkt_valid_o,
    input logic 				 lcl_rsp_wod_pkt_ready_i,
    // Indicate when a transaction is complete.
    output logic 				 tr_fin_valid_o
    );

   localparam PIPE_DATA_WIDTH = ECI_WORD_WIDTH + ECI_PACKET_SIZE_WIDTH + ECI_LCL_TOT_NUM_VCS_WIDTH;
   localparam CL_IDX_ODD = 1'b1;
   localparam CL_IDX_EVEN = 1'b0;

   typedef struct packed {
      logic [ECI_WORD_WIDTH-1:0]        hdr;
      logic [ECI_PACKET_SIZE_WIDTH-1:0] size;
      logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] vc;
      logic				valid;
      logic				ready;
   } eci_hdr_if_t;

   // output pipeline stage signals.
   logic [PIPE_DATA_WIDTH-1:0] 		lci_ps_us_data;
   logic 				lci_ps_us_valid;
   logic 				lci_ps_us_ready;
   logic [PIPE_DATA_WIDTH-1:0] 		lci_ps_ds_data;
   logic 				lci_ps_ds_valid;
   logic 				lci_ps_ds_ready;
   logic [ECI_WORD_WIDTH-1:0] 		lcia_ps_us_data;
   logic 				lcia_ps_us_valid;
   logic 				lcia_ps_us_ready;
   logic [ECI_WORD_WIDTH-1:0] 		lcia_ps_ds_data;
   logic 				lcia_ps_ds_valid;
   logic 				lcia_ps_ds_ready;
   logic [PIPE_DATA_WIDTH-1:0] 		ul_ps_us_data;
   logic 				ul_ps_us_valid;
   logic 				ul_ps_us_ready;
   logic [PIPE_DATA_WIDTH-1:0] 		ul_ps_ds_data;
   logic 				ul_ps_ds_valid;
   logic 				ul_ps_ds_ready;
   // Unlock i/f signals.
   eci_hdr_if_t ul_hdr;
   eci_hdr_if_t lci_hdr;
   
   always_comb begin : OUT_ASSIGN
      // Aliased addr i/f.
      al_addr_ready_o = lci_hdr.ready;
      // LCI
      {lcl_fwd_wod_hdr_o,
       lcl_fwd_wod_pkt_size_o,
       lcl_fwd_wod_pkt_vc_o}	= lci_ps_ds_data;
      lcl_fwd_wod_pkt_valid_o	= lci_ps_ds_valid;
      // LCIA
      lcl_rsp_wod_pkt_ready_o   = lcia_ps_us_ready;
      // Unlock.
      {lcl_rsp_wod_hdr_o,
       lcl_rsp_wod_pkt_size_o,
       lcl_rsp_wod_pkt_vc_o}	= ul_ps_ds_data;
      lcl_rsp_wod_pkt_valid_o	= ul_ps_ds_valid;
      // tr complete signal.
      // when lcia is received then one tr is complete.
      tr_fin_valid_o = lcia_ps_us_valid & lcia_ps_us_ready;
   end : OUT_ASSIGN

   // LCI header generation from
   // incoming aliased address. 
   always_comb begin : LCI_HDR_GEN
      lci_hdr.hdr = gen_lci_from_al_addr(
					 .al_addr_i(al_addr_i)
					 );
      lci_hdr.size = 'd1;
      if(al_addr_i[ECI_CL_ADDR_LSB] == CL_IDX_ODD) begin
	 lci_hdr.vc = VC_LCL_FWD_WO_DATA_E; //16
      end else begin
	 lci_hdr.vc = VC_LCL_FWD_WO_DATA_O; //17
      end
      lci_hdr.valid = al_addr_valid_i;
      lci_hdr.ready = lci_ps_us_ready;
   end : LCI_HDR_GEN

   // Output Pipeline stages.
   always_comb begin : LCI_PS_IP_ASSIGN
      lci_ps_us_data  = {
			 lci_hdr.hdr,
			 lci_hdr.size,
			 lci_hdr.vc
			 };
      lci_ps_us_valid = lci_hdr.valid;
      lci_ps_ds_ready = lcl_fwd_wod_pkt_ready_i;
   end : LCI_PS_IP_ASSIGN
   axis_pipeline_stage #
     (
      .DATA_WIDTH(PIPE_DATA_WIDTH)
      )
   lci_ps
     (
      .clk	(clk),
      .reset	(reset),
      .us_data	(lci_ps_us_data),
      .us_valid	(lci_ps_us_valid),
      .us_ready	(lci_ps_us_ready),
      .ds_data	(lci_ps_ds_data),
      .ds_valid	(lci_ps_ds_valid),
      .ds_ready	(lci_ps_ds_ready)
      );

   // pipeline stage for incoming lcia.
   // ignoring size and vc signals.
   always_comb begin : LCIA_PS_IP_ASSIGN
      lcia_ps_us_data  = lcl_rsp_wod_hdr_i;
      lcia_ps_us_valid = lcl_rsp_wod_pkt_valid_i;
      lcia_ps_ds_ready = ul_hdr.ready;
   end : LCIA_PS_IP_ASSIGN
   axis_pipeline_stage #
     (
      .DATA_WIDTH(ECI_WORD_WIDTH)
       )
   lcia_ps
     (
      .clk	(clk),
      .reset	(reset),
      .us_data	(lcia_ps_us_data),
      .us_valid	(lcia_ps_us_valid),
      .us_ready	(lcia_ps_us_ready),
      .ds_data	(lcia_ps_ds_data),
      .ds_valid	(lcia_ps_ds_valid),
      .ds_ready	(lcia_ps_ds_ready)
      );

   // Generate Unlock from LCIA header.
   always_comb begin : UL_HDR_GEN
      ul_hdr.hdr  = gen_ul_from_lcia(lcia_ps_ds_data);
      ul_hdr.size = 'd1;
      if(ul_hdr.hdr[ECI_CL_ADDR_LSB] == CL_IDX_ODD) begin
	 ul_hdr.vc = VC_LCL_RESP_WO_DATA_E; //18
      end else begin
	 ul_hdr.vc = VC_LCL_RESP_WO_DATA_O; //19
      end
      ul_hdr.valid = lcia_ps_ds_valid;
      ul_hdr.ready = ul_ps_us_ready;
   end : UL_HDR_GEN
   
   // Pipeline stage for outgoing unlock.
   always_comb begin : UL_PS_IP_ASSIGN
      ul_ps_us_data  = {
			ul_hdr.hdr,
			ul_hdr.size,
			ul_hdr.vc
			};
      ul_ps_us_valid = ul_hdr.valid;
      ul_ps_ds_ready = lcl_rsp_wod_pkt_ready_i;
   end : UL_PS_IP_ASSIGN
   axis_pipeline_stage #
     (
      .DATA_WIDTH(PIPE_DATA_WIDTH)
       )
   ul_ps
     (
      .clk	(clk),
      .reset	(reset),
      .us_data	(ul_ps_us_data),
      .us_valid	(ul_ps_us_valid),
      .us_ready	(ul_ps_us_ready),
      .ds_data	(ul_ps_ds_data),
      .ds_valid	(ul_ps_ds_valid),
      .ds_ready	(ul_ps_ds_ready)
      );

   function automatic [ECI_WORD_WIDTH-1:0] gen_ul_from_lcia
     (
      input logic [ECI_WORD_WIDTH-1:0] lcia_hdr_i
      );
      eci_word_t lcia_hdr_c;
      eci_word_t ul_hdr_c;
      lcia_hdr_c = eci_word_t'(lcia_hdr_i);
      ul_hdr_c.eci_word = '0;
      ul_hdr_c.lcl_unlock.opcode = LCL_CMD_MRSP_UNLOCK;
      ul_hdr_c.lcl_unlock.address = lcia_hdr_c.lcl_clean_inv_ack.address;
      return(ul_hdr_c.eci_word);
   endfunction : gen_ul_from_lcia

   function automatic [ECI_WORD_WIDTH-1:0] gen_lci_from_al_addr
     (
      input logic [DS_ADDR_WIDTH-1:0] al_addr_i
      );
      eci_word_t eci_cmd;
      eci_cmd.eci_word = '0;
      eci_cmd.lcl_clean_inv.opcode = LCL_CMD_MFWD_CLEAN_INV;
      eci_cmd.lcl_clean_inv.hreq_id = '0;
      eci_cmd.lcl_clean_inv.dmask = '1;
      eci_cmd.lcl_clean_inv.ns = 1'b1;
      eci_cmd.lcl_clean_inv.rnode = eci_nodeid_t'(ECI_FPGA_NODE_ID);
      eci_cmd.lcl_clean_inv.address[DS_ADDR_WIDTH-1:0] = al_addr_i;
      return(eci_cmd.eci_word);
   endfunction : gen_lci_from_al_addr

endmodule // cli_tput_load_gen

`endif
