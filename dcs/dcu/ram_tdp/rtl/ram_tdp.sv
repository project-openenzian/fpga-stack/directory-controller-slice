/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-05-24
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */

`ifndef RAM_TDP_SV
 `define RAM_TDP_SV

/*
 * Module Description:
 *  1 BRAM in 1kx36 true dual port configuration, write first mode. 
 *  9-bit byte instead of 8 (to allow for 18 bit tags and states). 
 *   * byte strobe is 36/9 = 4 bits wide. 
 *  Upon reset, all rows will be filled with RESET_VALUE.
 *  This takes 512 cycles and busy_o signal would be set high and inputs would be ignored.
 *
 * Input Output Description:
 *  Two ports: A, B
 *  addr - 10 bit address. 
 *  din  - 36 bit data. 
 *  en   - enable port. 
 *  we   - 4 bit write strobe. 
 *  dout - 36 bit read data. 
 *
 * Architecture Description:
 *  uses the xpm_memory_tdpram to insert a BRAM.
 *
 * Modifiable Parameters:
 *  RESET_VALUE: The value to be written into each row after reset. 
 *
 * Notes:
 *  II=1
 *  Take into account the busy_o signal when designing controller outside. 
 *
 */

module ram_tdp #
  (
   // value to write into all rows of BRAM upon reset.
   parameter logic [35:0] PORTA_RESET_VALUE = '1,
   parameter logic [35:0] PORTB_RESET_VALUE = '0
   )
   (
    input logic 	clk,
    input logic 	reset,
		
    // Port A read, write; write first.
    input logic [9:0] 	addra,
    input logic [35:0] 	dina,
    input logic 	ena,
    input logic [3:0] 	wea,
    output logic [35:0] douta,
    
    // Port B read, write; write first.
    input logic [9:0] 	addrb,
    input logic [35:0] 	dinb,
    input logic 	enb,
    input logic [3:0] 	web,
    output logic [35:0] doutb,
    
    // If 1, module is busy, reset in progress.
    output logic 	busy_o
    );

   // Upon reset, the module switches to CLEAR state and starts
   // filling up the BRAM with RESET_VALUE. After completion it switches
   // to NORMAL mode of execution.
   enum {CLEAR, NORMAL} state_reg = CLEAR, state_next;
   
   localparam ADDR_WIDTH = 10;
   localparam DATA_WIDTH = 36;
   localparam MEM_SIZE = (2**ADDR_WIDTH) * DATA_WIDTH;
   localparam BYTE_WRITE_WIDTH_A = 9; // Bits per byte for port A.
   localparam BYTE_WRITE_WIDTH_B = 9; // Bits per byte for port A.
   localparam WSTRB_WIDTH_A = DATA_WIDTH/BYTE_WRITE_WIDTH_A; // 
   localparam WSTRB_WIDTH_B = DATA_WIDTH/BYTE_WRITE_WIDTH_B;

   // Addres counter for resetting the data.
   // Two ports, each sweeping half the address space, 
   // so counter goes from 0 to 511. 
   logic [ADDR_WIDTH-2:0] clr_addr_reg = '0, clr_addr_next;

   // XPM signals. 
   logic [ADDR_WIDTH-1:0]    xpm_addra;
   logic [DATA_WIDTH-1:0]    xpm_dina;
   logic 		     xpm_ena;
   logic [WSTRB_WIDTH_A-1:0] xpm_wea;
   logic [DATA_WIDTH-1:0]    xpm_douta;
   logic [ADDR_WIDTH-1:0]    xpm_addrb;
   logic [DATA_WIDTH-1:0]    xpm_dinb;
   logic 		     xpm_enb;
   logic [WSTRB_WIDTH_B-1:0] xpm_web;
   logic [DATA_WIDTH-1:0]    xpm_doutb;   
   logic 		     xpm_regcea;
   logic 		     xpm_regceb;
   logic 		     xpm_sleep;

   assign busy_o = (state_reg == CLEAR) ? 1'b1 : 1'b0;
   assign douta = xpm_douta;
   assign doutb = xpm_doutb;
   
   assign xpm_regcea = 1'b1; // do not disable output register.
   assign xpm_regceb = 1'b1;
   assign xpm_sleep = 1'b0; // disable dynamic power saving feature.

   xpm_memory_tdpram #(
		       .ADDR_WIDTH_A(ADDR_WIDTH),//DECIMAL
		       .ADDR_WIDTH_B(ADDR_WIDTH),//DECIMAL
		       .AUTO_SLEEP_TIME(0),//DECIMAL
		       .BYTE_WRITE_WIDTH_A(BYTE_WRITE_WIDTH_A),//DECIMAL
		       .BYTE_WRITE_WIDTH_B(BYTE_WRITE_WIDTH_B),//DECIMAL
		       .CLOCKING_MODE("common_clock"),//String
		       .ECC_MODE("no_ecc"),//String
		       .MEMORY_INIT_FILE("none"),//String
		       .MEMORY_INIT_PARAM("0"),//String
		       .MEMORY_OPTIMIZATION("true"),//String
		       .MEMORY_PRIMITIVE("block"),//String // distributed, block, ultra
		       .MEMORY_SIZE(MEM_SIZE),//DECIMAL // 1kx32
		       .MESSAGE_CONTROL(0),//DECIMAL
		       .READ_DATA_WIDTH_A(DATA_WIDTH),//DECIMAL
		       .READ_DATA_WIDTH_B(DATA_WIDTH),//DECIMAL
		       .READ_LATENCY_A(1),//DECIMAL
		       .READ_LATENCY_B(1),//DECIMAL
		       .READ_RESET_VALUE_A("0"),//String
		       .READ_RESET_VALUE_B("0"),//String
		       .USE_EMBEDDED_CONSTRAINT(0),//DECIMAL
		       .USE_MEM_INIT(1),//DECIMAL
		       .WAKEUP_TIME("disable_sleep"),//String
		       .WRITE_DATA_WIDTH_A(DATA_WIDTH),//DECIMAL
		       .WRITE_DATA_WIDTH_B(DATA_WIDTH),//DECIMAL
		       .WRITE_MODE_A("write_first"),//String
		       .WRITE_MODE_B("write_first")//String
		       )
   xpm_memory_tdpram_inst(
			  .dbiterra(), // 1 bit output to indicate double bit error on port A. 
			  .dbiterrb(), // 1 bit output to indicate double bit error on port B
			  .douta(xpm_douta),       // data output for port A read operations. 
			  .doutb(xpm_doutb),       // data outpu for port B read operations. 
			  .sbiterra(), 
			  .sbiterrb(),
			  .addra(xpm_addra),
			  .addrb(xpm_addrb),
			  .clka(clk),
			  .clkb(clk),
			  .dina(xpm_dina),
			  .dinb(xpm_dinb),
			  .ena(xpm_ena),
			  .enb(xpm_enb),
			  .injectdbiterra(),
			  .injectdbiterrb(),
			  .injectsbiterra(),
			  .injectsbiterrb(),
			  .regcea(xpm_regcea),
			  .regceb(xpm_regceb),
			  .rsta(reset), // resets only the output registers and not the entire memory.
			  .rstb(reset),
			  .sleep(xpm_sleep),
			  .wea(xpm_wea),
			  .web(xpm_web)
			  );

   // When state is CLEAR, start sweeping through entire address space
   // through both ports, writing the default reset value into the BRAM.
   // Each port sweeps through half the rows. 
   // operation returns to normal after entire address space is swept.
   always_comb begin : CONTROLLER
      state_next = state_reg;
      clr_addr_next = clr_addr_reg;

      // default input to XPM - NORMAL mode. 
      xpm_addra = addra;
      xpm_dina = dina;
      xpm_ena =  ena;
      xpm_wea = wea;
      xpm_addrb = addrb;
      xpm_dinb = dinb;
      xpm_enb = enb;
      xpm_web= web;

      case(state_reg)
	CLEAR: begin
	   // Write RESET_VALUE into all rows of the BRAM. 
	   // Port A sweeps rows 0 to 511.
	   // Port B sweeps rows 512 to 1023.
	   if(clr_addr_reg == '1) begin
	      state_next = NORMAL;
	   end
	   clr_addr_next = clr_addr_reg + 1;

	   xpm_addra = {1'b0, clr_addr_reg};
	   xpm_dina = PORTA_RESET_VALUE;
	   xpm_ena =  1'b1;
	   xpm_wea = '1;
	   xpm_addrb = {1'b1, clr_addr_reg};;
	   xpm_dinb = PORTB_RESET_VALUE;
	   xpm_enb = 1'b1;
	   xpm_web= '1;
	end
	NORMAL: begin
	   xpm_addra = addra;
	   xpm_dina = dina;
	   xpm_ena =  ena;
	   xpm_wea = wea;
	   xpm_addrb = addrb;
	   xpm_dinb = dinb;
	   xpm_enb = enb;
	   xpm_web= web;
	end
      endcase
   end : CONTROLLER

   always_ff @(posedge clk) begin : REG_ASSIGN
      state_reg <= state_next;
      clr_addr_reg <= clr_addr_next;
      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset
	 state_reg <= CLEAR;
	 clr_addr_reg <= '0;
      end
   end : REG_ASSIGN
endmodule // ram_tdp
`endif
