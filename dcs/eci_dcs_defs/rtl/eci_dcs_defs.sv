`ifndef ECI_DCS_DEFS_SV
`define ECI_DCS_DEFS_SV

// DC Slice definitions.
// At a level higher than DCU definitions.

package eci_dcs_defs ;
   //  40b ECI Cl address is split into
   //
   // ┌────────────┬────────────────────┐
   // │ 2b Slice ID│ 38b Slice CLaddr   │
   // └────────────┴────────────────────┘
   //
   //  38b slice CL address is split into 
   // ┌────────────┬────────────┬──────────────┐
   // │   18b tag  │ 13b set idx│ 7b ByteOffset│
   // └────────────┴─────┬──────┴──────────────┘
   //                    │set idx is
   //                    ▼ split into
   //        ┌───────────────┬──────────┐
   //        │   6/7/8b      │ 7/6/5b   │
   //        │set within DCU │ DCU_ID   │
   //        └───────────────┴──────────┘
   //                             │ dcu id is
   //                             ▼ split into
   //                       ┌──────────┬────────────┐
   //                       │ 6/5/4b   │ 1b         │
   //                       │ DCU_IDX  │ O/E offset │
   //                       └──────────┴────────────┘
   //                                   Odd/Even

   // Modifiable parameters begin
   // Each DCU has a part of the directory.
   // This parameter specifies the number of sets (rows) per DCU.
   // 64  - 1 BRAM per DCU.
   // 128 - 2 BRAM per DCU.
   // 256 - 4 BRAM per DCU.
   parameter DS_NUM_SETS_PER_DCU = 128; // 64, 128, 256
   // add pipeline stage to output of TSR.
   // if needed to meet timing.
   // Note: for 64 sets/DCU additing this stage
   // will not help timing and only adversely impact performance.
   parameter logic DS_TSR_OP_REGISTER = 1'b1; // 1'b1/1'b0
   // Modifiable parameters end.

   // Do not modify anything below.
   parameter DS_BYTE_OFFSET_WIDTH = 7; // dont change.
   parameter DS_SET_IDX_WIDTH = 13; // dont change.
   parameter DS_TAG_WIDTH = 18; // dont change.
   parameter DS_SET_WN_DCU_WIDTH = $clog2(DS_NUM_SETS_PER_DCU); // 6,7,8
   parameter DS_DCU_ID_WIDTH = DS_SET_IDX_WIDTH - DS_SET_WN_DCU_WIDTH; // 7,6,5
   parameter DS_NUM_WAYS_PER_SET = 16; // dont change.

   typedef struct packed {
      logic [1:0] xb2; // 2b dont care.
      logic [DS_TAG_WIDTH-1:0] tag; //18b
      logic [DS_SET_WN_DCU_WIDTH-1:0] set_within_dcu; // 6/7/8b
      logic [DS_DCU_ID_WIDTH-1:0]     dcu_id; // 7/6/5b
      logic [DS_BYTE_OFFSET_WIDTH-1:0] byte_offset; // 7b
   } ds_cl_addr_t; // 40 bits.

   // Slice CL address is 38 bits (check figure above).
   parameter DS_ADDR_WIDTH = 38;
   parameter MAX_DCU_ID_WIDTH = 7; // max(DS_DCU_ID_WIDTH) = max(7,6,5) = 7.
   
   parameter DS_NUM_DCU = 2**DS_DCU_ID_WIDTH; //128, 64, 32
   parameter DS_NUM_DCU_IDX = DS_NUM_DCU/2;  //64, 32, 16
   parameter DS_DCU_IDX_WIDTH = $clog2(DS_NUM_DCU_IDX); //6,5,4
   
   // Data path.
   // Number of 512 bit registers needed to write
   // DS_NUM_DCU_IDX cache lines.
   // 1 1024 bit CL is written in 2 512 bit registers.
   // DS_NUM_DCU_IDX CLs are written into:
   parameter DS_DBUF_DEPTH = DS_NUM_DCU_IDX*2;        //128,64,32
   parameter DS_DBUF_WR_IDX_WIDTH = $clog2(DS_DBUF_DEPTH); //7,6,5
   // Always 1 CL is read from data buffer.
   // ie 2 512 bit registers are read in parallel and so
   // read index is 1 bit smaller than write index. 
   parameter DS_DBUF_RD_IDX_WIDTH = DS_DCU_IDX_WIDTH; //6,5,4

   function automatic [DS_DCU_IDX_WIDTH-1:0] f_dcuid_2_dcuidx
     (
      input logic [DS_DCU_ID_WIDTH-1:0] dcu_id_i
      );
      // Function to get DCU_IDX from DCU ID.
      // LSB of DCU ID chooses odd or even,
      // this LSB is removed to get DCU_IDX.
      return(dcu_id_i[DS_DCU_ID_WIDTH-1:1]);
   endfunction : f_dcuid_2_dcuidx

   function automatic [DS_DCU_ID_WIDTH-1:0] f_maxdcuid_2_dcu_id
     (
      input logic [MAX_DCU_ID_WIDTH-1:0] max_dcu_id_i
      );
      // MAX_DCU_ID_WIDTH is 7 bits but the actual
      // number of bits for DCU ID depends on configuration
      // (DS_DCU_ID_WIDTH). This function extracts necessary
      // bits from MAX_DCU_ID to get DCU_ID.
      return(max_dcu_id_i[DS_DCU_ID_WIDTH-1:0]);
   endfunction : f_maxdcuid_2_dcu_id
   
endpackage // eci_dcs_defs
   
`endif
