#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
      $XILINX_VIVADO/data/verilog/src/glbl.v \
      ../rtl/eci_cmd_defs.sv \
      ../testbench/dc_to_vc_routerTb.sv \
      ../rtl/dc_to_vc_router.sv \
      ../rtl/axis_pipeline_stage.sv 

xelab -debug typical -incremental -L xpm worklib.dc_to_vc_routerTb worklib.glbl -s worklib.dc_to_vc_routerTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.dc_to_vc_routerTb 
xsim -R worklib.dc_to_vc_routerTb 
