/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-08-25
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */

`ifndef SEQ_READ_LOAD_GEN_SV
`define SEQ_READ_LOAD_GEN_SV

// Given a starting CL index, number of CLs and stride,
// module issues that many RLDX to the DirC.
// It counts the responses and calculates throughput.
// if DirC sends forward downgrade, it sends an ack with data. (A31d).

import eci_cmd_defs::*;

module seq_read_load_gen #
  (
   // parameter to display throughput
   // values, prints tput after PRINT_EVERY_X_TR
   // responses received.
   parameter PRINT_EVERY_X_TR = 1000
   )
   (
    input logic 					   clk,
    input logic 					   reset,

    input logic [ECI_CL_INDEX_WIDTH-1:0] 		   st_cl_idx_i,
    input logic [63:0] 					   num_cls_i,
    input logic [63:0] 					   cl_idx_stride_i,
    input logic 					   tput_latency_bar_i,
    input logic 					   en_i,

    // Output ECI events to dcs.
    // VC: req_wod 6,7, [fwd_wod 8,9], rsp_wod 10,11.
    output logic [ECI_WORD_WIDTH-1:0] 			   lo_wod_hdr_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   lo_wod_pkt_size_o,
    output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 		   lo_wod_pkt_vc_o,
    output logic 					   lo_wod_pkt_valid_o,
    input logic 					   lo_wod_pkt_ready_i,

    // ECI packet for response with data. (VC 4 or 5). (header + data).
    output logic [ECI_PACKET_SIZE-1:0][ECI_WORD_WIDTH-1:0] hi_wd_pkt_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   hi_wd_pkt_size_o,
    output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 		   hi_wd_pkt_vc_o,
    output logic 					   hi_wd_pkt_valid_o,
    input logic 					   hi_wd_pkt_ready_i,

    // Incoming ECI events from dcs.
    // VC 10,11
    input logic [ECI_WORD_WIDTH-1:0] 			   lo_wod_hdr_i,
    input logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   lo_wod_pkt_size_i,
    input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 		   lo_wod_pkt_vc_i,
    input logic 					   lo_wod_pkt_valid_i,
    output logic 					   lo_wod_pkt_ready_o,

    // VC 5,4
    input logic [ECI_PACKET_SIZE-1:0][ECI_WORD_WIDTH-1:0]  hi_wd_pkt_i,
    input logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   hi_wd_pkt_size_i,
    input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 		   hi_wd_pkt_vc_i,
    input logic 					   hi_wd_pkt_valid_i,
    output logic 					   hi_wd_pkt_ready_o,

    // Performance counters.
    output logic [63:0] 				   tput_timer_o,
    output logic [63:0] 				   num_rsp_recd_o
    );

   
   enum  {IDLE, RUN, DONE} state_reg, state_next;
   logic [ECI_CL_INDEX_WIDTH-1:0] cl_idx_reg = '0, cl_idx_next;
   logic [63:0] 		  num_cls_reg = '0, num_cls_next;
   logic [63:0] 		  cl_idx_stride_reg = '0, cl_idx_stride_next;
   logic 			  tput_latency_bar_reg = '0, tput_latency_bar_next;
   logic [63:0] 		  num_req_sent_reg = '0, num_req_sent_next;
   logic [63:0] 		  num_rsp_recd_reg = '0, num_rsp_recd_next;
   
   // timers.
   logic [63:0] 		  tput_timer_reg = '0, tput_timer_next;
   logic 			  tput_timer_en_reg = '0, tput_timer_en_next;

   real 			  clock_period_ns = 3.106;
   real 			  tput_value_GBps = 0;
   
   
   eci_word_t curr_rldx_req;
   eci_cl_addr_t curr_cl_addr;
   

   // assign outputs.
   assign lo_wod_hdr_o = curr_rldx_req.eci_word;
   assign lo_wod_pkt_size_o = 'd1;
   assign lo_wod_pkt_vc_o = VC_REQ_WO_DATA_E;
   assign tput_timer_o = tput_timer_reg;
   assign num_rsp_recd_o = num_rsp_recd_reg;
   
   
   // Generate RLDX based on current address.
   always_comb begin : CURR_RLDX
      curr_cl_addr.parts.cl_index = cl_idx_reg;
      curr_cl_addr.parts.byte_offset = '0;
      curr_rldx_req = gen_req_wo_data(
				      .rreq_id_i('0),
				      .dmask_i('1),
				      .addr_i(curr_cl_addr.flat),
				      .rldt_i(1'b0),
				      .rldi_i(1'b0),
				      .rldd_i(1'b0),
				      .rc2d_s_i(1'b0),
				      .rldx_i(1'b1)
				      );
      
   end : CURR_RLDX

   // state machine to continuously issue RLDX.
   always_comb begin : CONTROLLER
      state_next                = state_reg;
      cl_idx_next		= cl_idx_reg;
      num_cls_next		= num_cls_reg;
      cl_idx_stride_next	= cl_idx_stride_reg;
      tput_latency_bar_next	= tput_latency_bar_reg;
      num_req_sent_next		= num_req_sent_reg;
      num_rsp_recd_next		= num_rsp_recd_reg;
      tput_timer_next		= tput_timer_reg;
      tput_timer_en_next = tput_timer_en_reg;

      lo_wod_pkt_valid_o = 1'b0;
      hi_wd_pkt_ready_o = 1'b0;

      
      case(state_reg)
	IDLE: begin
	   if(en_i) begin
	      state_next = RUN;
	      cl_idx_next = st_cl_idx_i;
	      num_cls_next = num_cls_i;
	      cl_idx_stride_next = cl_idx_stride_i;
	      tput_latency_bar_next = tput_latency_bar_i;
	      num_req_sent_next = '0;
	   end
	end
	RUN: begin
	   lo_wod_pkt_valid_o = 1'b1;
	   hi_wd_pkt_ready_o = 1'b1;
	   if(lo_wod_pkt_valid_o & lo_wod_pkt_ready_i) begin
	      cl_idx_next = cl_idx_reg + cl_idx_stride_i;
	      num_req_sent_next = num_req_sent_reg + 1;
	   end
	   if(num_req_sent_next == num_cls_reg) begin
	      state_next = DONE;
	   end
	end
	DONE: begin
	   // system has to be reset to start again.
	   hi_wd_pkt_ready_o = 1'b1;
	   if(num_rsp_recd_reg == num_cls_reg) begin
	      hi_wd_pkt_ready_o = 1'b0;
	   end
	   state_next = state_reg;
	end
      endcase

      // whenever the first response is recd, start the tput timer.
      // stop it when the number of responses recd matches the number of resp expected. 
      if(hi_wd_pkt_valid_i & hi_wd_pkt_ready_o) begin
	 num_rsp_recd_next = num_rsp_recd_reg + 1;
	 if(tput_timer_en_reg == 1'b0) begin
	    // First response recd, start timer. 
	    tput_timer_en_next = 1'b1;
	 end
	 if(num_rsp_recd_next ==  num_cls_reg) begin
	    tput_timer_en_next = 1'b0;
	 end
      end

      if((tput_timer_en_reg == 1'b1) | (tput_timer_en_next == 1'b1)) begin
	 tput_timer_next = tput_timer_reg + 1;
      end
   end : CONTROLLER

   
   always_ff @(posedge clk) begin : REG_ASSIGN
      state_reg <= state_next;
      cl_idx_reg			<= cl_idx_next;
      num_cls_reg		<= num_cls_next;
      cl_idx_stride_reg		<= cl_idx_stride_next;
      tput_latency_bar_reg	<= tput_latency_bar_next;
      num_req_sent_reg		<= num_req_sent_next;
      num_rsp_recd_reg		<= num_rsp_recd_next;
      tput_timer_reg		<= tput_timer_next;
      tput_timer_en_reg <= tput_timer_en_next;
      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset
	 state_reg <= IDLE;
	 cl_idx_reg		<= '0;
	 num_cls_reg		<= '0;
	 cl_idx_stride_reg	<= '0;
	 tput_latency_bar_reg <= '0;
	 num_req_sent_reg	<= '0;
	 num_rsp_recd_reg	<= '0;
	 tput_timer_reg	<= '0;
	 tput_timer_en_reg <= '0;
      end
   end : REG_ASSIGN

   always @(num_rsp_recd_o) begin
      if((num_rsp_recd_o % PRINT_EVERY_X_TR) == 0) begin
	 tput_value_GBps = real'(num_rsp_recd_o * 128) / (real'(tput_timer_o) * clock_period_ns);
	 $display("Clock period  = %g ns", clock_period_ns);
	 $display("Clock elapsed = %0d", tput_timer_o);
	 $display("Num resp recd = %0d", num_rsp_recd_o);
	 $display("Tput          = %g GBytes/s", tput_value_GBps);
      end
   end
   
   //---------------------------------------------------------------
   // Functions to generate requests.
   // RLDT, RLDD, RLDI, RC2D_S - Req wo data.
   function automatic eci_word_t gen_req_wo_data
     (
      input 	  eci_id_t rreq_id_i,
      input 	  eci_dmask_t dmask_i,
      input 	  eci_address_t addr_i,
      input logic rldt_i,
      input logic rldi_i,
      input logic rldd_i,
      input logic rc2d_s_i,
      input logic rldx_i
      );
      eci_word_t my_hdr;
      my_hdr.eci_word = '0;
      if(rldt_i) begin
	 my_hdr.mreq_load.opcode = ECI_CMD_MREQ_RLDT;
      end
      if(rldi_i) begin
	 my_hdr.mreq_load.opcode = ECI_CMD_MREQ_RLDI;
      end      
      if(rldd_i) begin
	 my_hdr.mreq_load.opcode = ECI_CMD_MREQ_RLDD;
      end
      if(rc2d_s_i) begin
	 my_hdr.mreq_load.opcode = ECI_CMD_MREQ_RC2D_S;
      end
      if(rldx_i) begin
	 my_hdr.mreq_load.opcode = ECI_CMD_MREQ_RLDX;
      end
      my_hdr.mreq_load.rreq_id = rreq_id_i;
      my_hdr.mreq_load.dmask = dmask_i;
      my_hdr.mreq_load.address = addr_i;
      return(my_hdr);
   endfunction : gen_req_wo_data
  
endmodule // seq_read_load_gen

`endif
