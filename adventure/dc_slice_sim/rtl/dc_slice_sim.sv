`ifndef DC_SLICE_SIM_SV
`define DC_SLICE_SIM_SV

module dc_slice_sim #
  (
   parameter STORE_HDR_WIDTH = 64,
   parameter RETRIEVE_HDR_WIDTH = 64,
   parameter SIM_DELAY = 5
   )
   (
    input logic 			  clk,
    input logic 			  reset,
    input logic [DS_DCU_IDX_WIDTH-1:0] 	  req_dcu_idx_i,
    input logic [STORE_HDR_WIDTH-1:0] 	  req_hdr_i,
    input logic 			  req_valid_i,
    output logic 			  req_ready_o,
    output logic [DS_DCU_ID_WIDTH-1:0] 	  rsp_dcu_id_o,
    output logic [RETRIEVE_HDR_WIDTH-1:0] rsp_hdr_o,
    output logic 			  rsp_valid_o,
    input logic 			  rsp_ready_i
    );

   logic [STORE_HDR_WIDTH-1:0] 		  hr_data_i;
   logic [DS_DCU_IDX_WIDTH-1:0] 	  hr_sel_i;
   logic 				  hr_valid_i;
   logic 				  hr_ready_o;

   logic [DS_NUM_DCU_IDX-1:0][STORE_HDR_WIDTH-1:0] hr_data_o;
   logic [DS_NUM_DCU_IDX-1:0] 			   hr_valid_o;
   logic [DS_NUM_DCU_IDX-1:0] 			   hr_ready_i;

   // DCU SIM signals. 
   logic [DS_NUM_DCU_IDX-1:0][STORE_HDR_WIDTH-1:0] dcu_req_hdr_i;
   logic [DS_NUM_DCU_IDX-1:0] 			   dcu_req_valid_i;
   logic [DS_NUM_DCU_IDX-1:0] 			   dcu_req_ready_o;

   logic [DS_NUM_DCU_IDX-1:0][DS_DCU_ID_WIDTH-1:0] dcu_rsp_dcu_id_o;
   logic [DS_NUM_DCU_IDX-1:0][RETRIEVE_HDR_WIDTH-1:0] dcu_rsp_hdr_o;
   logic [DS_NUM_DCU_IDX-1:0] 			      dcu_rsp_valid_o;
   logic [DS_NUM_DCU_IDX-1:0] 			      dcu_rsp_ready_i;

   logic [DS_NUM_DCU_IDX-1:0][DS_DCU_ID_WIDTH-1:0]    penc_id_i;
   logic [DS_NUM_DCU_IDX-1:0][RETRIEVE_HDR_WIDTH-1:0] penc_hdr_i;
   logic [DS_NUM_DCU_IDX-1:0] 			      penc_valid_i;
   logic [DS_NUM_DCU_IDX-1:0] 			      penc_ready_o;
   logic [DS_DCU_ID_WIDTH-1:0] 			      penc_id_o;
   logic [RETRIEVE_HDR_WIDTH-1:0] 		      penc_hdr_o;
   logic 					      penc_valid_o;
   logic 					      penc_ready_i;
   
   always_comb begin : OP_ASSIGN
      req_ready_o = hr_ready_o;
      rsp_dcu_id_o = penc_id_o;
      rsp_hdr_o = penc_hdr_o;
      rsp_valid_o = penc_valid_o;
   end : OP_ASSIGN

   always_comb begin : H_ROUTER_IP_ASSIGN
      hr_data_i = req_hdr_i;
      hr_sel_i = req_dcu_idx_i;
      hr_valid_i = req_valid_i;
      hr_ready_i = dcu_req_ready_o;
   end : H_ROUTER_IP_ASSIGN
   axis_comb_router #
     (
      .DATA_WIDTH(STORE_HDR_WIDTH),
      .NUM_OUT(DS_NUM_DCU_IDX)
      )
   hdr_dcu_router
     (
      .us_data_i(hr_data_i),
      .us_sel_i(hr_sel_i),
      .us_valid_i(hr_valid_i),
      .us_ready_o(hr_ready_o),
      .ds_data_o(hr_data_o),
      .ds_valid_o(hr_valid_o),
      .ds_ready_i(hr_ready_i)
      );

   genvar dcu_iter;
   generate
      for( dcu_iter = 0; dcu_iter < DS_NUM_DCU_IDX; dcu_iter=dcu_iter+1) begin : DCUS
	 always_comb begin : DCU_SIM_IP_ASSIGN
	    dcu_req_hdr_i = hr_data_o;
	    dcu_req_valid_i = hr_valid_o;
	    dcu_rsp_ready_i = penc_ready_o;
	 end : DCU_SIM_IP_ASSIGN
	 dcu_sim #
		      (
		       .STORE_HDR_WIDTH(STORE_HDR_WIDTH),
		       .RETRIEVE_HDR_WIDTH(RETRIEVE_HDR_WIDTH),
		       .SIM_DELAY($urandom_range(1,10))
		       )
	 dcu_sim_inst
		      (
		       .clk(clk),
		       .reset(reset),
		       .req_dcu_idx_i(DS_DCU_IDX_WIDTH'(dcu_iter)),
		       .req_hdr_i(dcu_req_hdr_i[dcu_iter]),
		       .req_valid_i(dcu_req_valid_i[dcu_iter]),
		       .req_ready_o(dcu_req_ready_o[dcu_iter]),
		       .rsp_dcu_id_o(dcu_rsp_dcu_id_o[dcu_iter]),
		       .rsp_hdr_o(dcu_rsp_hdr_o[dcu_iter]),
		       .rsp_valid_o(dcu_rsp_valid_o[dcu_iter]),
		       .rsp_ready_i(dcu_rsp_ready_i[dcu_iter])
		       );
	 
      end : DCUS
   endgenerate

   always_comb begin : PENC_IP_ASSIGN
      penc_id_i = dcu_rsp_dcu_id_o;
      penc_hdr_i = dcu_rsp_hdr_o;
      penc_valid_i = dcu_rsp_valid_o;
      penc_ready_i = rsp_ready_i;
   end : PENC_IP_ASSIGN
   axis_comb_priority_enc #
     (
      .ID_WIDTH(DS_DCU_ID_WIDTH),
      .DATA_WIDTH(RETRIEVE_HDR_WIDTH),
      .NUM_IN(DS_NUM_DCU_IDX)
       )
   dcu_rsp_penc_1
     (
      .us_id_i(penc_id_i),
      .us_data_i(penc_hdr_i),
      .us_valid_i(penc_valid_i),
      .us_ready_o(penc_ready_o),
      .ds_id_o(penc_id_o),
      .ds_data_o(penc_hdr_o),
      .ds_valid_o(penc_valid_o),
      .ds_ready_i(penc_ready_i)
      );
   
endmodule // dc_slice_sim

`endif
