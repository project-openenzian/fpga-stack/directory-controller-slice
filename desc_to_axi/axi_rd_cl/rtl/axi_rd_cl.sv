/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-02-02
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */


`ifndef AXI_RD_CL_SV
`define AXI_RD_CL_SV

/*
 * Module Description:
 *  Accepts read descriptor for 1 CL and 1 CL only (no sub cache lines) and 
 *  converts it to AXI read request. 
 *  When AXI read response is recd, it accumulates till CL is filled before 
 *  enabling downstream. 
 *
 * Input Output Description:
 *  input rd req desc channel -> primary AXI read request out channel.
 *  input AXI read response channel -> rd rsp desc out channel. 
 *
 *
 * Architecture Description:
 *  Read request and response channels are independent of each other. 
 *  The read req desc to AXI read req conversion is combinational.
 *  The read resp channel has a simple valid ready pipeline (Ready not registered but valid is).
 *  Each beat of AXI resp channel is stored in a "beat ram" till a full CL is formed. 
 *  
 *
 * Modifiable Parameters:
 *  DESC_ID_WIDTH, DESC_ADDR_WIDTH = read desc ID, address widths. 
 *  CL_WIDTH = size of CL
 *  AXI_DATA_WIDTH = width of AXI data bus, can be varied from 64 to 1024 in powers of 2.
 *
 *
 * Non-modifiable Parameters:
 *  None.
 * 
 * Modules Used:
 *  None.
 *
 * Notes:
 *  1. Reads 1 full CL always but this can be easily modified by taking in a dmask input 
 *     and generating the appropriate number of beats. 
 *  2. the rresp AXI signal is not used currently and can be used if required. 
 *  3. II = 1.
 *
 */


/////////////////////////////////////////////////////////////////////////
//                                                +                    //
//     CL   ^   ^ +                               |RD Req              //
//     CL   |   | | CL Valid                      |Desc ch     	       //
//     Width|   | v Ready                         v (valid ready)      //
// +------------------------------+    +--+-------+------+----+	       //
// |   +-------------------+      |    |                      |	       //
// |   |DW |DW |DW |DW |DW |RAM   |    |                      |	       //
// |   +-------------------+      |    |    READ_ADDR_CNTRLR  |	       //
// |                              |    |                      |	       //
// |    READ_DATA_CNTRLR          |    | (always req 1 CL)    |	       //
// |    (read 1 CL at a time)     |    |                      |	       //
// +--------------+---------------+    +----------^-----------+	       //
//                ^                               |		       //
//     DW data    | m_axi_r* channel              | m_axi_ar* channel  //
//     width      +                               v		       //
//                             NO DEPENDENCY			       //
//                             BETWEEN AXI CHANNELS		       //
/////////////////////////////////////////////////////////////////////////


module axi_rd_cl #
  (
   parameter DESC_ID_WIDTH   = 6,
   parameter DESC_ADDR_WIDTH = 40,
   parameter CL_WIDTH        = 1024,
   parameter AXI_ID_WIDTH    = DESC_ID_WIDTH,
   parameter AXI_ADDR_WIDTH  = DESC_ADDR_WIDTH,
   // can be varied between 64 and 1024 (powers of 2).
   parameter AXI_DATA_WIDTH = 512
   )
   (
    input logic 		      clk,
    input logic 		      reset,
   
    // Read desc input.
    input logic [DESC_ID_WIDTH-1:0]   rd_req_id_i,
    input logic [DESC_ADDR_WIDTH-1:0] rd_req_addr_i,
    input logic 		      rd_req_valid_i,
    output logic 		      rd_req_ready_o,

    // Read resp desc output.
    output logic [DESC_ID_WIDTH-1:0]  rd_rsp_id_o,
    output logic [CL_WIDTH-1:0]       rd_rsp_data_o,
    output logic 		      rd_rsp_valid_o,
    input logic 		      rd_rsp_ready_i,

    // AXI output primary I/F.
    output logic [AXI_ID_WIDTH-1:0]   p_axi_arid,
    output logic [AXI_ADDR_WIDTH-1:0] p_axi_araddr,
    output logic [7:0] 		      p_axi_arlen, 
    output logic [2:0] 		      p_axi_arsize, 
    output logic [1:0] 		      p_axi_arburst, // always incr
    output logic 		      p_axi_arlock,  // not used
    output logic [3:0] 		      p_axi_arcache, // not used
    output logic [2:0] 		      p_axi_arprot,  // not used
    output logic 		      p_axi_arvalid,
    input logic 		      p_axi_arready,
    input logic [AXI_ID_WIDTH-1:0]    p_axi_rid,
    input logic [AXI_DATA_WIDTH-1:0]  p_axi_rdata,
    input logic [1:0] 		      p_axi_rresp, // ignored.
    input logic 		      p_axi_rlast,
    input logic 		      p_axi_rvalid,
    output logic 		      p_axi_rready
   );

   // Master slave is ugly.
   // Primary replaces master, replica replaces slave.
   
   localparam BYTES_IN_CL = CL_WIDTH/8;
   localparam BYTES_IN_TRANSFER = AXI_DATA_WIDTH/8;
   // 2^AX_SIZE is number of bytes in a beat.
   localparam [2:0] AX_SIZE = $clog2(BYTES_IN_TRANSFER);
   localparam ALIGN_ADDR_LSB = $clog2(BYTES_IN_TRANSFER);
   // Auto increment address by replica after each beat.
   localparam [1:0] INCR_BURST_TYPE = 2'b01;
   // Number of beats per burst (count starts from 0) (2 beats for 512 data width).
   localparam [7:0] AX_LEN = (BYTES_IN_CL/BYTES_IN_TRANSFER) - 1;
   // Number of register required to store all the beats.
   localparam NUM_READ_REGS = AX_LEN + 1;
   // width of index into the RAM.
   localparam READ_REGS_IDX_WIDTH = $clog2(NUM_READ_REGS);
   localparam [1:0] AXI_OKAY = 2'b00;

   
   logic [DESC_ID_WIDTH-1:0] 	     rd_rsp_id_reg = '0, rd_rsp_id_next;
   logic 			     rd_rsp_valid_reg = '0, rd_rsp_valid_next;
   // Ram to store read data, one location per beat.
   logic [NUM_READ_REGS-1:0][AXI_DATA_WIDTH-1:0] rd_ram;
   logic [READ_REGS_IDX_WIDTH-1:0] 		 rd_ram_idx_reg = '0, rd_ram_idx_next;
   logic 					 rd_ram_wren;
   logic 					 acc_last;

   logic [AXI_ID_WIDTH-1:0] 			 p_axi_arid_reg;
   logic [AXI_ADDR_WIDTH-1:0] 			 p_axi_araddr_reg;
   logic [7:0] 					 p_axi_arlen_reg;
   logic [2:0] 					 p_axi_arsize_reg;
   logic [1:0] 					 p_axi_arburst_reg;
   logic 					 p_axi_arlock_reg;
   logic [3:0] 					 p_axi_arcache_reg;
   logic [2:0] 					 p_axi_arprot_reg;
   logic 					 p_axi_arvalid_reg;


   initial begin
      if(AXI_DATA_WIDTH < 64 || AXI_DATA_WIDTH > 1024) begin
	 // not a hard rule in AXI4 but AXI3 limits bursts to 16 beats, 
	 // 16 beats * 64 bit data = 1024 bits (1 CL)
	 $error("Error: AXI_DATA_WIDTH %0d cannot be less than 64 or greater than 1024 bits (instance %m)", AXI_DATA_WIDTH);
	 $finish;
      end
      if(!(BYTES_IN_TRANSFER inside {1, 2, 4, 8, 16, 32, 64, 128})) begin
	 // AXI data width cannot exeed 128 bytes
	 $error("Error: AXI_DATA_WIDTH %0d is not supported (instance %m)", AXI_DATA_WIDTH);
	 $finish;
      end
   end


   always_ff @(posedge clk) begin : AR_CH_ASSIGN
      if(rd_req_ready_o) begin
	 p_axi_arid_reg	        <= rd_req_id_i;
	 p_axi_araddr_reg[AXI_ADDR_WIDTH-1:ALIGN_ADDR_LSB] <= rd_req_addr_i[AXI_ADDR_WIDTH-1:ALIGN_ADDR_LSB];
	 p_axi_arlen_reg	<= AX_LEN;
	 p_axi_arsize_reg	<= AX_SIZE;
	 p_axi_arburst_reg	<= INCR_BURST_TYPE;
	 p_axi_arlock_reg	<= '0; // not used. 
	 p_axi_arcache_reg	<= '0; // not used. 
	 p_axi_arprot_reg	<= '0; // not used. 
	 p_axi_arvalid_reg      <= rd_req_valid_i;
      end

      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset 
	 p_axi_arid_reg		<= '0;
	 p_axi_araddr_reg	<= '0;
	 p_axi_arlen_reg	<= '0;
	 p_axi_arsize_reg	<= '0;
	 p_axi_arburst_reg	<= '0;
	 p_axi_arlock_reg	<= '0;
	 p_axi_arcache_reg	<= '0;
	 p_axi_arprot_reg	<= '0;
	 p_axi_arvalid_reg	<= '0;
      end
   end : AR_CH_ASSIGN

   // The output read request channel is not registered.
   // Aligning the address to number of bytes in data transfer. 
   always_comb begin : RD_ADDR_ASSIGN
      p_axi_arid	= p_axi_arid_reg;
      p_axi_araddr      = p_axi_araddr_reg;
      p_axi_arlen	= p_axi_arlen_reg;
      p_axi_arsize	= p_axi_arsize_reg;
      p_axi_arburst	= p_axi_arburst_reg;
      p_axi_arlock	= p_axi_arlock_reg;  // not used. 
      p_axi_arcache	= p_axi_arcache_reg; // not used. 
      p_axi_arprot	= p_axi_arprot_reg;  // not used. 
      p_axi_arvalid	= p_axi_arvalid_reg;
      rd_req_ready_o    = (~p_axi_arvalid | p_axi_arready);
   end : RD_ADDR_ASSIGN

   assign rd_rsp_id_o    = rd_rsp_id_reg;
   assign rd_rsp_data_o  = rd_ram;
   assign rd_rsp_valid_o = rd_rsp_valid_reg;
   assign p_axi_rready   = (~rd_rsp_valid_o) | rd_rsp_ready_i;
   assign rd_ram_wren    = p_axi_rready;
   assign acc_last       = ((rd_ram_idx_reg == AX_LEN) | p_axi_rlast);

   // Accumulate read response till all beats are recd into rd_ram. 
   // simple valid ready pipeline where downstream latches upstream whenever downstream is ready.
   // downstream is valid only when all beats are accumulated.
   // clear the index into the rd_ram when downstream handshake happens.
   
   always_comb begin : RD_DATA_CONTROLLER
      rd_rsp_id_next	= rd_rsp_id_reg;
      rd_rsp_valid_next = rd_rsp_valid_reg;
      rd_ram_idx_next	= rd_ram_idx_reg;
      if(p_axi_rready) begin
	 if(acc_last) begin
	    rd_rsp_id_next = p_axi_rid;
	    rd_rsp_valid_next = p_axi_rvalid;
	 end else begin
	    rd_rsp_valid_next = 1'b0;
	 end
	 if(p_axi_rvalid) begin
	    if(acc_last) begin
	       rd_ram_idx_next = '0;
	    end else begin
	       rd_ram_idx_next = rd_ram_idx_reg + 1;
	    end
	 end
      end
   end : RD_DATA_CONTROLLER

   always_ff @(posedge clk) begin : BEAT_RAM
      if(rd_ram_wren) begin
	 rd_ram[rd_ram_idx_reg] <= p_axi_rdata;
      end
   end : BEAT_RAM
   
   always_ff @(posedge clk) begin : REG_ASSIGN
      rd_rsp_id_reg	<= rd_rsp_id_next;
      rd_rsp_valid_reg	<= rd_rsp_valid_next;
      rd_ram_idx_reg	<= rd_ram_idx_next;
      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset 
	 rd_rsp_id_reg	  <= '0;
	 rd_rsp_valid_reg <= '0;
	 rd_ram_idx_reg   <= '0;
      end
   end : REG_ASSIGN
endmodule // axi_rd_cl
`endif
