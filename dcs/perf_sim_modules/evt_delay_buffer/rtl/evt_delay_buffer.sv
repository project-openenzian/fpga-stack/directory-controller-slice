/*
 * Systems Group, D-INFK, ETH Zurich
 *
 * Author  : A.Ramdas
 * Date    : 2022-01-25
 * Project : Enzian
 *
 */

`ifndef EVT_DELAY_BUFFER_SV
`define EVT_DELAY_BUFFER_SV

// Delay valid ready pipeline.
// Input packet is delayed by NUM_STAGES cycles.
// The first and last stages are registered to avoid timing
// issues in long pipelines.

module evt_delay_buffer #
  (
   parameter EVT_WIDTH = 64,
   parameter VC_WIDTH = 5,
   parameter SIZE_WIDTH = 5,
   parameter NUM_STAGES = 3
   )
   (
    input logic 		  clk,
    input logic 		  reset,
    
    input logic [EVT_WIDTH-1:0]   us_evt_i,
    input logic [VC_WIDTH-1:0] 	  us_vc_i,
    input logic [SIZE_WIDTH-1:0]  us_size_i,
    input logic 		  us_valid_i,
    output logic 		  us_ready_o,

    output logic [EVT_WIDTH-1:0]  ds_evt_o,
    output logic [VC_WIDTH-1:0]   ds_vc_o,
    output logic [SIZE_WIDTH-1:0] ds_size_o,
    output logic 		  ds_valid_o,
    input logic 		  ds_ready_i
    );

   // Number of stage accounting for first and last registered stages.
   localparam NUM_STAGES_IP_ACC = NUM_STAGES - 1;
   
   // Stage 0 is just input, actual pipeline stages start with 1.
   // this is done to avoid code duplication.
   logic [EVT_WIDTH-1:0] 		     stage_evt[NUM_STAGES_IP_ACC-1:0];
   logic [VC_WIDTH-1:0] 		     stage_vc[NUM_STAGES_IP_ACC-1:0];
   logic [SIZE_WIDTH-1:0] 		     stage_size[NUM_STAGES_IP_ACC-1:0];
   logic 				     stage_valid[NUM_STAGES_IP_ACC-1:0];
   logic 				     stage_ready[NUM_STAGES_IP_ACC-1:0];
   logic 				     stage_en[NUM_STAGES_IP_ACC-1:0];

   initial begin
      if(NUM_STAGES < 3) begin
	 $error("Error: NUM_STAGES cannot be less than 3 (instance %m)");
	 $finish;
      end
   end

   // First stage, registered valid ready signal.
   axis_pipeline_stage #
     (
      .DATA_WIDTH(EVT_WIDTH + VC_WIDTH + SIZE_WIDTH)
       )
   first_stage_inst
     (
      .clk(clk),
      .reset(reset),
      .us_data({us_size_i, us_vc_i, us_evt_i}),
      .us_valid(us_valid_i),
      .us_ready(us_ready_o),
      .ds_data({stage_size[0], stage_vc[0], stage_evt[0]}),
      .ds_valid(stage_valid[0]),
      .ds_ready(stage_ready[0])
      );

   // Last stage, registered valid ready signals.
   axis_pipeline_stage #
     (
      .DATA_WIDTH(EVT_WIDTH + VC_WIDTH + SIZE_WIDTH)
       )
   last_stage_inst
     (
      .clk(clk),
      .reset(reset),
      .us_data({stage_size[NUM_STAGES_IP_ACC-1], 
		stage_vc[NUM_STAGES_IP_ACC-1], 
		stage_evt[NUM_STAGES_IP_ACC-1]}),
      .us_valid(stage_valid[NUM_STAGES_IP_ACC-1]),
      .us_ready(stage_ready[NUM_STAGES_IP_ACC-1]),
      .ds_data({ds_size_o,ds_vc_o,ds_evt_o}),
      .ds_valid(ds_valid_o),
      .ds_ready(ds_ready_i)
      );

   // Valid ready pipeline with ready not registered. 
   genvar i;
   generate
      for( i = 1; i < NUM_STAGES_IP_ACC; i++) begin : VR_PIPELINE
	 
	 assign stage_en[i] = (~stage_valid[i] | stage_ready[i]);
	 assign stage_ready[i-1] = stage_en[i];
	 
	 always_ff @(posedge clk) begin : VR_PIPE_STAGE
	    if(stage_en[i]) begin
	       stage_evt[i] <= stage_evt[i-1];
	       stage_vc[i] <= stage_vc[i-1];
	       stage_size[i] <= stage_size[i-1];
	       stage_valid[i] <= stage_valid[i-1];
	    end 
	    if( reset ) begin
	       // Make sure unnecessary datapath registers are not reset 
	       //stage_evt[i] <= '0;
	       stage_vc[i] <= '0;
	       stage_size[i] <= '0;
	       stage_valid[i] <= '0;
	    end
	 end : VR_PIPE_STAGE
      end : VR_PIPELINE
   endgenerate

endmodule // evt_delay_buffer

`endif
