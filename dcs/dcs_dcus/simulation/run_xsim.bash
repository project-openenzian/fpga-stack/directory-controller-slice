#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
      $XILINX_VIVADO/data/verilog/src/glbl.v \
      ../rtl/eci_cc_defs.sv \
      ../rtl/eci_cmd_defs.sv \
      ../rtl/eci_dcs_defs.sv \
      ../rtl/eci_dirc_defs.sv \
      ../testbench/dcs_dcusTb.sv \
      ../testbench/word_addr_mem.sv \
      ../rtl/axis_comb_router.sv \
      ../rtl/axis_comb_rr_arb.sv \
      ../rtl/dcs_rr_arb.sv \
      ../rtl/wr_trmgr.sv \
      ../rtl/dcu_controller.sv \
      ../rtl/dc_to_vc_router.sv \
      ../rtl/arb_4_ecih.sv \
      ../rtl/arb_3_ecih.sv \
      ../rtl/arb_2_ecih.sv \
      ../rtl/dcu_tsu.sv \
      ../rtl/rd_trmgr.sv \
      ../rtl/eci_trmgr.sv \
      ../rtl/gen_out_header.sv \
      ../rtl/decode_eci_req.sv \
      ../rtl/eci_cc_table.sv \
      ../rtl/dcu.sv \
      ../rtl/axis_2_router.sv \
      ../rtl/dcu_gen_ecih_vc_router.sv \
      ../rtl/axis_comb_priority_enc.sv \
      ../rtl/dcu_top.sv \
      ../rtl/axis_pipeline_stage.sv \
      ../rtl/dcs_dcus.sv \
      ../rtl/tag_state_ram.sv \
      ../rtl/ram_tdp.sv 

xelab -debug typical -incremental -L xpm worklib.dcs_dcusTb worklib.glbl -s worklib.dcs_dcusTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.dcs_dcusTb 
xsim -R worklib.dcs_dcusTb 
