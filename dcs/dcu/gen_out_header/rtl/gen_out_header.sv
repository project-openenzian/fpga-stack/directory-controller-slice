`ifndef GEN_OUT_HEADER_SV
`define GEN_OUT_HEADER_SV

import eci_cmd_defs::*;
import eci_cc_defs::*;
import eci_dirc_defs::*;

// Module to generate header
// output is registered
module gen_out_header (
		       input logic 				clk,
		       input logic 				reset,
		       input logic 				en_i,
		       // action determines what inputs are used 
		       input 					cc_action_t cc_acn_i,
		       // This input is used to generate response headers
		       // for previously recd input eci requests 
		       input 					eci_word_t hdr_from_tr_tbl_i,
		       // indicates if read data is available for this action or not 
		       input logic 				rdda_sink_in_prgrs_i,
		       input logic 				wdda_sink_in_prgrs_i,
		       // These inputs are used to generate forward messages 
		       input 					eci_hreqid_t fwd_tr_id_i,
		       input 					eci_address_t fwd_cl_addr_i,
		       input 					eci_dmask_t fwd_dmask_i,
		       input logic 				fwd_ns_i,
		       output 					eci_word_t eci_hdr_o,
		       output 					eci_vc_size_t vc_o,
		       // header + data size 
		       output logic [ECI_PACKET_SIZE_WIDTH-1:0] pkt_size_o,
		       output logic 				eci_hdr_invalid_o
		       );

   // Registers for output 
   eci_word_t eci_hdr_reg = '0, eci_hdr_next;
   logic eci_hdr_invalid_reg = '0, eci_hdr_invalid_next;
   eci_vc_size_t vc_reg = '0, vc_next;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] pkt_size_reg = '0, pkt_size_next;
   // Local signals 
   logic [ECI_WORD_WIDTH-1:0] generated_hdr;
   logic 		      generated_hdr_invalid;
   eci_vc_size_t generated_vc;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] generated_pkt_size;

   // Assign registers to output 
   assign eci_hdr_o		= eci_hdr_reg;
   assign eci_hdr_invalid_o	= eci_hdr_invalid_reg;
   assign vc_o = vc_reg;
   assign pkt_size_o = pkt_size_reg;

   always_comb begin : CONTROLLER
      eci_hdr_next = eci_hdr_reg;
      eci_hdr_invalid_next = eci_hdr_invalid_reg;
      vc_next = vc_reg;
      pkt_size_next = pkt_size_reg;

      // Generate header based on CC action input 
      case(cc_acn_i)
	SEND_F31: begin
	   // FEVX_EH
	   generated_hdr_invalid = 1'b0;
	   // Memory forward wo data VC 8, 9
	   // Odd cl indices go to VC8, even cl indices to VC9
	   generated_hdr =  eci_cmd_defs::eci_gen_fevx_eh
			    (
			     .hreq_id_i(fwd_tr_id_i),
			     .dmask_i(fwd_dmask_i),
			     .ns_i(fwd_ns_i),
			     .addr_i(fwd_cl_addr_i)
			     );
	   // VC information 
	   if(fwd_cl_addr_i[ECI_CL_ADDR_LSB] == CL_IDX_ODD) begin
	      // CL index is odd, sent to VC8
	      generated_vc = eci_vc_size_t'(VC_FWD_WO_DATA_E);
	   end else begin
	      // CL index is even, sent to VC9
	      generated_vc = eci_vc_size_t'(VC_FWD_WO_DATA_O);
	   end
	   // size - only sending header 
	   generated_pkt_size = ECI_PACKET_SIZE_WIDTH'('d1);
	end

	SEND_F21: begin
	   // SINV_H
	   generated_hdr_invalid = 1'b0;
	   // Memory forward wo data VC 8, 9
	   // Odd cl indices go to VC8, even cl indices to VC9
	   generated_hdr =  eci_cmd_defs::eci_gen_sinv_h
			    (
			     .hreq_id_i(fwd_tr_id_i),
			     .dmask_i(fwd_dmask_i),
			     .ns_i(fwd_ns_i),
			     .addr_i(fwd_cl_addr_i)
			     );
	   // VC information 
	   if(fwd_cl_addr_i[ECI_CL_ADDR_LSB] == CL_IDX_ODD) begin
	      // CL index is odd, sent to VC8
	      generated_vc = eci_vc_size_t'(VC_FWD_WO_DATA_E);
	   end else begin
	      // CL index is even, sent to VC9
	      generated_vc = eci_vc_size_t'(VC_FWD_WO_DATA_O);
	   end
	   // size - only sending header 
	   generated_pkt_size = ECI_PACKET_SIZE_WIDTH'('d1);
	end

	SEND_F32: begin
	   //FLDRS_EH
	   generated_hdr_invalid = 1'b0;
	   // Memory forward wo data VC 8, 9
	   // Odd cl indices go to VC8, even cl indices to VC9
	   generated_hdr =  eci_cmd_defs::eci_gen_fldrs_eh
			    (
			     .hreq_id_i(fwd_tr_id_i),
			     .dmask_i(fwd_dmask_i),
			     .ns_i(fwd_ns_i),
			     .addr_i(fwd_cl_addr_i)
			     );
	   // VC information 
	   if(fwd_cl_addr_i[ECI_CL_ADDR_LSB] == CL_IDX_ODD) begin
	      // CL index is odd, sent to VC8
	      generated_vc = eci_vc_size_t'(VC_FWD_WO_DATA_E);
	   end else begin
	      // CL index is even, sent to VC9
	      generated_vc = eci_vc_size_t'(VC_FWD_WO_DATA_O);
	   end
	   // size - only sending header 
	   generated_pkt_size = ECI_PACKET_SIZE_WIDTH'('d1);
	end
	
	SEND_RA3: begin
	   // RA3 has two variants PEMD (with data), PEMN(without data)
	   // if there is an rdda sink in progress that means, there is
	   // data from AXI RD module so we have to send PEMD
	   // if there is no rdda sink in progress, there is no data
	   // send PEMN.
	   // Currently RC2D_S is set to not read memory, so response PEMN is generated.
	   // RLDX PEMD - response with data  VC4/5
	   // RC2D_S PEMN - response without data VC 10/11.
	   generated_hdr_invalid = 1'b0;
	   generated_hdr = eci_cmd_defs::eci_gen_pemd(
						      .req_i(hdr_from_tr_tbl_i), 
						      .pemn_i(~rdda_sink_in_prgrs_i)
						      );
	   if(rdda_sink_in_prgrs_i) begin
	      // Read data available
	      // read 1 CL always
	      generated_pkt_size = ECI_PACKET_SIZE_WIDTH'(ECI_PACKET_SIZE);
	   end else begin
	      // No read data, just header
	      generated_pkt_size = ECI_PACKET_SIZE_WIDTH'('d1);
	   end

	   if(hdr_from_tr_tbl_i.mreq_0to10.address[ECI_CL_ADDR_LSB] == CL_IDX_ODD) begin
	      if(rdda_sink_in_prgrs_i) begin
		 // odd index VC4 (resp with data).
		 generated_vc = eci_vc_size_t'('d4);
	      end else begin
		 // odd index VC10 (resp without data).
		 generated_vc = eci_vc_size_t'('d10);
	      end
	   end else begin
	      if(rdda_sink_in_prgrs_i) begin
		 // even index VC5 (resp with data).
		 generated_vc = eci_vc_size_t'('d5);
	      end else begin
		 // even index VC11 (resp without data).
		 generated_vc = eci_vc_size_t'('d11);
	      end
	   end
	end

	SEND_RA2: begin
	   // PSHA needs to be sent 
	   // has data always
	   // Response VCs - Response with data VC4/5
	   // Response for RLDI
	   generated_hdr_invalid = 1'b0;
	   generated_hdr = eci_cmd_defs::eci_gen_psha(.req_i(hdr_from_tr_tbl_i));
	   // RLDI always sends read data 
	   generated_pkt_size = ECI_PACKET_SIZE_WIDTH'(ECI_PACKET_SIZE);
	   // VC
	   if(hdr_from_tr_tbl_i.mreq_0to10.address[ECI_CL_ADDR_LSB] == CL_IDX_ODD) begin
	      // odd index VC4
	      generated_vc = eci_vc_size_t'('d4);
	   end else begin
	      // even index VC5
	      generated_vc = eci_vc_size_t'('d5);
	   end
	end

	SEND_RRA: begin
	   // PSHA needs to be sent
	   generated_hdr_invalid = 1'b0;
	   generated_hdr = eci_cmd_defs::eci_gen_psha(.req_i(hdr_from_tr_tbl_i));
	   // always 1 full CL is read
	   generated_pkt_size = ECI_PACKET_SIZE_WIDTH'(ECI_PACKET_SIZE);
	   // VC
	   if(hdr_from_tr_tbl_i.mreq_0to10.address[ECI_CL_ADDR_LSB] == CL_IDX_ODD) begin
	      // odd index VC4
	      generated_vc = eci_vc_size_t'('d4);
	   end else begin
	      // even index VC5
	      generated_vc = eci_vc_size_t'('d5);
	   end
	end

	SEND_LCIA: begin
	   generated_hdr_invalid = 1'b0;
	   generated_hdr = eci_cmd_defs::lcl_gen_lcia(.req_i(hdr_from_tr_tbl_i));
	   // Packet size is always 1 (no data).
	   generated_pkt_size = ECI_PACKET_SIZE_WIDTH'('d1);
	   // VC
	   if(hdr_from_tr_tbl_i.lcl_clean_inv.address[ECI_CL_ADDR_LSB] == CL_IDX_ODD) begin
	      generated_vc = VC_LCL_RESP_WO_DATA_E;
	   end else begin
	      generated_vc = VC_LCL_RESP_WO_DATA_O;
	   end
	end

	SEND_LCA: begin
	   generated_hdr_invalid = 1'b0;
	   generated_hdr = eci_cmd_defs::lcl_gen_lca(.req_i(hdr_from_tr_tbl_i));
	   // Packet size is always 1 (no data).
	   generated_pkt_size = ECI_PACKET_SIZE_WIDTH'('d1);
	   // VC
	   if(hdr_from_tr_tbl_i.lcl_clean.address[ECI_CL_ADDR_LSB] == CL_IDX_ODD) begin
	      generated_vc = VC_LCL_RESP_WO_DATA_E;
	   end else begin
	      generated_vc = VC_LCL_RESP_WO_DATA_O;
	   end
	end

	default: begin
	   generated_hdr_invalid = 1'b1;
	   generated_hdr = ECI_WORD_WIDTH'('0);
	   generated_pkt_size = '0;
	   generated_vc = '0;
	end
      endcase // case (cc_acn_i)

      // Latch next values to output registers 
      if(en_i) begin
	 eci_hdr_next = generated_hdr;
	 eci_hdr_invalid_next = generated_hdr_invalid;
	 vc_next = generated_vc;
	 pkt_size_next = generated_pkt_size;
	 if(generated_hdr_invalid == 1'b1) begin
	    //$error("Error: Cannot generate header for action %s (instance %m)", cc_acn_i.name());
	    //$finish;
	 end
      end
   end : CONTROLLER

   always_ff @(posedge clk) begin : REG_ASSIGN
      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset 
	 eci_hdr_reg	<= '0;
	 eci_hdr_invalid_reg <= '0;
	 vc_reg <= '0;
	 pkt_size_reg <= '0;
      end else begin
	 eci_hdr_reg <= eci_hdr_next;
	 eci_hdr_invalid_reg <= eci_hdr_invalid_next;
	 vc_reg <= vc_next;
	 pkt_size_reg <= pkt_size_next;
      end
   end : REG_ASSIGN
endmodule // gen_out_header
`endif
