#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
$XILINX_VIVADO/data/verilog/src/glbl.v \
../testbench/axis_xpm_fifoTb.sv \
../rtl/axis_xpm_fifo.sv 

xelab -debug typical -incremental -L xpm worklib.axis_xpm_fifoTb worklib.glbl -s worklib.axis_xpm_fifoTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.axis_xpm_fifoTb 
xsim -gui worklib.axis_xpm_fifoTb 
