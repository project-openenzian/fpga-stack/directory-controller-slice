#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
      $XILINX_VIVADO/data/verilog/src/glbl.v \
      ../rtl/eci_cmd_defs.sv \
      ../rtl/eci_dcs_defs.sv \
      ../testbench/rd_data_pathTb.sv \
      ../rtl/rd_data_path.sv \
      ../rtl/dp_data_store.sv \
      ../rtl/dp_gen_path.sv \
      ../rtl/map_ecid_to_wrd.sv \
      ../rtl/dp_gate.sv \
      ../rtl/dp_mem.sv \
      ../rtl/dp_wr_ser.sv 

xelab -debug typical -incremental -L xpm worklib.rd_data_pathTb worklib.glbl -s worklib.rd_data_pathTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.rd_data_pathTb 
xsim -R worklib.rd_data_pathTb 
