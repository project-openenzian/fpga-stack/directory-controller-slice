#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
      $XILINX_VIVADO/data/verilog/src/glbl.v \
      ../testbench/dcu_tsuTb.sv \
      ../rtl/dcu_tsu.sv \
      ../rtl/tag_state_ram.sv \
      ../rtl/ram_tdp.sv 

xelab -debug typical -incremental -L xpm worklib.dcu_tsuTb worklib.glbl -s worklib.dcu_tsuTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.dcu_tsuTb 
xsim -R worklib.dcu_tsuTb 
