#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
      $XILINX_VIVADO/data/verilog/src/glbl.v \
      ../rtl/eci_dcs_defs.sv \
      ../rtl/eci_cmd_defs.sv \
      ../testbench/dp_gen_pathTb.sv \
      ../rtl/dp_data_store.sv \
      ../rtl/dp_gen_path.sv \
      ../rtl/map_ecid_to_wrd.sv \
      ../rtl/dp_gate.sv \
      ../rtl/dp_mem.sv \
      ../testbench/dc_slice_sim.sv \
      ../testbench/dcu_sim.sv \
      ../testbench/axis_comb_priority_enc.sv \
      ../testbench/axis_comb_router.sv \
      ../testbench/axis_pipeline_stage.sv \
      ../rtl/dp_wr_ser.sv \
      ../testbench/simple_delay_module.sv \
      ../rtl/vr_pipe_stage.sv 

xelab -debug typical -incremental -L xpm worklib.dp_gen_pathTb worklib.glbl -s worklib.dp_gen_pathTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.dp_gen_pathTb 
xsim -R worklib.dp_gen_pathTb 
