import eci_cc_defs::*;

module eci_cc_tableTb();
   //input output ports 
   //Input signals
   logic clk;
   logic reset;
   
   logic en;
   cc_state_t curr_state_i;
   cc_req_t curr_req_i;
   logic ns_valid_o;
   logic ns_evictable_o;
   logic ns_evcn_in_prgrs_o;

   //Output signals
   cc_state_t next_state_o;
   cc_action_t next_action_o;


   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   eci_cc_table eci_cc_table1 (
			       .clk(clk),
			       .reset(reset),
			       .en(en),
			       .curr_state_i(curr_state_i),
			       .curr_req_i(curr_req_i),
			       .next_state_o(next_state_o),
			       .next_action_o(next_action_o),
			       .ns_valid_o(ns_valid_o),
			       .ns_evictable_o(ns_evictable_o),
			       .ns_evcn_in_prgrs_o(ns_evcn_in_prgrs_o)
			       );

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports 
      clk = '0;
      reset = 1'b1;
      en = '0;
      curr_state_i = s1__1;
      curr_req_i = R12;
      ##5;
      reset = 1'b0;
      ##5;
      en = 1'b1;
      ##1;
      en = 1'b0;
      ##5;

      // Test invalid scenario
      curr_state_i = s1__1;
      curr_req_i = R23;
      en = 1'b1;
      ##1;
      en = 1'b0;

      #500 $finish;
   end 

endmodule
