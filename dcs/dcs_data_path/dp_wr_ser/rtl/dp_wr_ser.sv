/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-07-27
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */

`ifndef DP_WR_SER_SV
`define DP_WR_SER_SV

import eci_dcs_defs::*;

// serialize input CL of 1024 bits into
// two 512 bit blocks to write to datapath mem.
// For a given DCU_IDX, 
//  lower 512 bits written to {DCU_IDX,0}
//  higher 512 bits written to {DCU_IDX,1}
// Done to ease routing.

module dp_wr_ser #
  (
   parameter CL_WIDTH = 1024,
   parameter CLB2_WIDTH = CL_WIDTH/2
   )
   (
    input logic 			    clk,
    input logic 			    reset,
    // Input cache line to write
    // along with DCU_IDX reg to write to.
    input logic [DS_DCU_IDX_WIDTH-1:0] 	    wr_dcu_idx_i,
    input logic [CL_WIDTH-1:0] 		    wr_data_i,
    input logic 			    wr_valid_i,
    output logic 			    wr_ready_o,
    // Serialized write output,
    // 1024 bits split into two 512 bit writes.
    // No flow control downstream.
    output logic [DS_DBUF_WR_IDX_WIDTH-1:0] s_wr_addr_o,
    output logic [CLB2_WIDTH-1:0] 	    s_wr_data_o,
    output logic 			    s_wr_en_o
    );

   enum {WR_LO,WR_HI} state_reg, state_next;
   
   always_comb begin : CONTROLLER
      state_next = state_reg;
      s_wr_addr_o = {wr_dcu_idx_i, 1'b0};
      s_wr_data_o = wr_data_i[CLB2_WIDTH-1:0]; //511:0
      s_wr_en_o = 1'b0;
      wr_ready_o = 1'b0;
      case(state_reg)
	WR_LO: begin
	   if(wr_valid_i == 1'b1) begin
	      s_wr_en_o = 1'b1;
	      state_next = WR_HI;
	   end
	end
	WR_HI: begin
	   s_wr_addr_o = {wr_dcu_idx_i, 1'b1};
	   s_wr_data_o = wr_data_i[CL_WIDTH-1:CLB2_WIDTH]; //1023:512
	   s_wr_en_o = 1'b1;
	   wr_ready_o = 1'b1;
	   state_next = WR_LO;
	end
      endcase
   end : CONTROLLER

   always_ff @(posedge clk) begin : REG_ASSIGN
      state_reg <= state_next;
      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset
	 state_reg <= WR_LO;
      end
   end : REG_ASSIGN

   
endmodule // dp_wr_ser

`endif
