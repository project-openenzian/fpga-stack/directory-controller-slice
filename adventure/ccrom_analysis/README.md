## Characteristics of CCROM

 * CCROM looks sparse (lots of Xs), seeing if it can be compacted.
   * Total number of entries vs number of Xs
   * For each state, what is number of Xs
   * For each event, what is number of Xs
   * Mark distribution of Xs 