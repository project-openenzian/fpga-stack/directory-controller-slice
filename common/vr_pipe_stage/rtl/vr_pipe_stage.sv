`ifndef VR_PIPE_STAGE_SV
`define VR_PIPE_STAGE_SV

module vr_pipe_stage #
  (
   parameter DATA_WIDTH = 32,
   parameter NUM_STAGES = 4
   )
   (
    input logic 		    clk,
    input logic 		    reset,
    // Incoming upstream data
    input logic [(DATA_WIDTH-1):0]  us_data,
    input logic 		    us_valid,
    output logic 		    us_ready,
    // Outgoing downstream data
    output logic [(DATA_WIDTH-1):0] ds_data,
    output logic 		    ds_valid,
    input logic 		    ds_ready
    );

   // Number of stage accounting for first and last registered stages.
   localparam NUM_STAGES_IP_ACC = NUM_STAGES - 1;
   
   // Stage 0 is just input, actual pipeline stages start with 1.
   // this is done to avoid code duplication.
   logic [DATA_WIDTH-1:0] 	    stage_data[NUM_STAGES_IP_ACC-1:0];
   logic 			    stage_valid[NUM_STAGES_IP_ACC-1:0];
   logic 			    stage_ready[NUM_STAGES_IP_ACC-1:0];
   logic 			    stage_en[NUM_STAGES_IP_ACC-1:0];

   initial begin
      if(NUM_STAGES < 3) begin
	 $error("Error: NUM_STAGES cannot be less than 3 (instance %m)");
	 $finish;
      end
   end

   // First stage, registered valid ready signal.
   axis_pipeline_stage #
     (
      .DATA_WIDTH(DATA_WIDTH)
       )
   first_stage_inst
     (
      .clk(clk),
      .reset(reset),
      .us_data(us_data),
      .us_valid(us_valid),
      .us_ready(us_ready),
      .ds_data(stage_data[0]),
      .ds_valid(stage_valid[0]),
      .ds_ready(stage_ready[0])
      );

   // Last stage, registered valid ready signals.
   axis_pipeline_stage #
     (
      .DATA_WIDTH(DATA_WIDTH)
       )
   last_stage_inst
     (
      .clk(clk),
      .reset(reset),
      .us_data(stage_data[NUM_STAGES_IP_ACC-1]),
      .us_valid(stage_valid[NUM_STAGES_IP_ACC-1]),
      .us_ready(stage_ready[NUM_STAGES_IP_ACC-1]),
      .ds_data(ds_data),
      .ds_valid(ds_valid),
      .ds_ready(ds_ready)
      );

   // Valid ready pipeline with ready not registered. 
   genvar i;
   generate
      for( i = 1; i < NUM_STAGES_IP_ACC; i++) begin : VR_PIPELINE
	 
	 assign stage_en[i] = (~stage_valid[i] | stage_ready[i]);
	 assign stage_ready[i-1] = stage_en[i];
	 
	 always_ff @(posedge clk) begin : VR_PIPE_STAGE
	    if(stage_en[i]) begin
	       stage_data[i] <= stage_data[i-1];
	       stage_valid[i] <= stage_valid[i-1];
	    end 
	    if( reset ) begin
	       // Make sure unnecessary datapath registers are not reset 
	       //stage_data[i] <= '0;
	       stage_valid[i] <= '0;
	    end
	 end : VR_PIPE_STAGE
      end : VR_PIPELINE
   endgenerate

endmodule // vr_pipe_stage

`endif
