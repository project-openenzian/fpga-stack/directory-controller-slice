import eci_cmd_defs::*;
import eci_dcs_defs::*;

module perf_gen_seq_aliasedTb();

   parameter NUM_REQS_TO_WAIT  = 10;
   parameter NUM_ADDR_TO_ISSUE = 20;
   parameter PERF_REGS_WIDTH   = 32;

   //input output ports
   //Input signals
   logic		       clk;
   logic		       reset;
   logic		       odd_wait_req_valid_i;
   logic		       even_wait_req_valid_i;
   logic		       odd_al_addr_ready_i;
   logic		       even_al_addr_ready_i;
   logic		       odd_wait_rsp_valid_i;
   logic		       even_wait_rsp_valid_i;

   //Output signals
   logic [DS_ADDR_WIDTH-1:0]   odd_al_addr_o;
   logic		       odd_al_addr_valid_o;
   logic [DS_ADDR_WIDTH-1:0]   even_al_addr_o;
   logic		       even_al_addr_valid_o;
   logic		       done_o;
   logic [PERF_REGS_WIDTH-1:0] perf_cyc_st_to_done_o;
   logic [PERF_REGS_WIDTH-1:0] perf_num_tr_done_o;
   
   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   perf_gen_seq_aliased #
     (
      .NUM_REQS_TO_WAIT(NUM_REQS_TO_WAIT),
      .NUM_ADDR_TO_ISSUE(NUM_ADDR_TO_ISSUE),
      .PERF_REGS_WIDTH(PERF_REGS_WIDTH)
      )
   perf_gen_seq_aliased1 
     (
      .clk(clk),
      .reset(reset),
      .odd_wait_req_valid_i(odd_wait_req_valid_i),
      .even_wait_req_valid_i(even_wait_req_valid_i),
      .odd_al_addr_o(odd_al_addr_o),
      .odd_al_addr_valid_o(odd_al_addr_valid_o),
      .odd_al_addr_ready_i(odd_al_addr_ready_i),
      .even_al_addr_o(even_al_addr_o),
      .even_al_addr_valid_o(even_al_addr_valid_o),
      .even_al_addr_ready_i(even_al_addr_ready_i),
      .odd_wait_rsp_valid_i(odd_wait_rsp_valid_i),
      .even_wait_rsp_valid_i(even_wait_rsp_valid_i),
      .done_o(done_o),
      .perf_cyc_st_to_done_o(perf_cyc_st_to_done_o),
      .perf_num_tr_done_o(perf_num_tr_done_o)
      );//instantiation completed

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();
      // Initialize Input Ports
      clk = '0;
      reset = '1;
      //odd_wait_req_valid_i = '0;
      //even_wait_req_valid_i = '0;
      odd_al_addr_ready_i = '0;
      even_al_addr_ready_i = '0;
      //odd_wait_rsp_valid_i = '0;
      //even_wait_rsp_valid_i = '0;
      ##5;
      reset = 1'b0;
      odd_al_addr_ready_i = '1;
      even_al_addr_ready_i = '1;
      #1000;
      $display("Success: all tests pass");
      $display("Number of Transactions: %0d", perf_num_tr_done_o);
      $display("Number of cycles taken: %0d", perf_cyc_st_to_done_o);
      $finish;
   end

   always_ff @(posedge clk) begin : WAIT_REQ
      odd_wait_req_valid_i <= 1'b1;
      even_wait_req_valid_i <= 1'b1;
      if( reset ) begin
	 odd_wait_req_valid_i <= 1'b0;
	 even_wait_req_valid_i <= 1'b0;
      end
   end : WAIT_REQ

   always_ff @(posedge clk) begin : RSP_VALID_C
      if(odd_al_addr_valid_o & odd_al_addr_ready_i) begin
	 odd_wait_rsp_valid_i <= 1'b1;
      end else begin
	 odd_wait_rsp_valid_i <= 1'b0;
      end
      if (even_al_addr_valid_o & even_al_addr_ready_i) begin
	 even_wait_rsp_valid_i <= 1'b1;
      end else begin
	 even_wait_rsp_valid_i <= 1'b0;
      end
      if( reset ) begin
	 odd_wait_rsp_valid_i <= 1'b0;
	 even_wait_rsp_valid_i <= 1'b0;
      end
   end : RSP_VALID_C

   // always make sure odd/even cl indices
   // are observed in correct channels. 
   always_ff @(posedge clk) begin : ODD_EVEN_ASSERT
      if(odd_al_addr_valid_o & odd_al_addr_ready_i) begin
	 if(odd_al_addr_o[ECI_CL_ADDR_LSB] !== 1'b1) begin
	    $fatal(1, "Odd cl idx not observed in odd out channel");
	 end
      end
      if(even_al_addr_valid_o & even_al_addr_ready_i) begin
	 if(even_al_addr_o[ECI_CL_ADDR_LSB] !== 1'b0) begin
	    $fatal(1, "Even cl idx not observed in even out channel");
	 end
      end
   end : ODD_EVEN_ASSERT

endmodule
