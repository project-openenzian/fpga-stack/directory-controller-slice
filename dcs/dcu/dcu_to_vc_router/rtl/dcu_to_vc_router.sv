/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-08-17
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */


`ifndef DCU_TO_VC_ROUTER_SV
`define DCU_TO_VC_ROUTER_SV

// Module takes an ECI header + VC number to
// route it to either rsp_wod or rsp_wd output I/F.
// 
// Module uses dcu_gen_ecih_vc_router module in the
// architecture shown below.
//
// dcu_gen_ecih_vc_router takes a VC boundary as a
// parameter.
// It compares the input VC to this boundary value
// and routes it to one of the two output channel.
// Any VC# that is greater than or equal to the VC
// boundary would be routed to output channel 1 and
// any VC# that is lower than VC boundary will be
// routed to output channel 0.


//                 ┌──────────┐
//         VC10,11 │          │
//  rsp_wod ◄──────┤ VC_B=10  │    Hdr from
//             Ch0 │          │    VC 6,7,8,9,10,11
//                 │ gen_ecih │◄─────┐
//                 │          │      │
//                 │          │      │      ┌──────────┐
//        X ◄──────┤          │      │      │          │◄────── ECI hdr
//             Ch1 │          │      └──────┤ VC_B=6   │        from DCU
//    VC 6,7,8,9   └──────────┘        Ch1  │          │
//                                          │ gen_ecih │◄────── VC# of
//                                          │          │        ECI hdr
//                                          │          │
//                                          │          │
//                 ┌───────────┐     ┌──────┤          │
//           VC 5,4│           │     │ Ch0  └──────────┘
//   rsp_wd ◄──────┤  VC_B=4   │     │
//            Ch1  │           │◄────┘
//                 │ gen_ecih  │    Hdr from
//                 │           │    VC 5,4,3,2
//           VC 3,2│           │
//        X ◄──────┤           │
//             Ch0 │           │
//                 └───────────┘
		      
import eci_cmd_defs::*;

module dcu_to_vc_router 
  (
   input logic 				    clk,
   input logic 				    reset,
   // ECI response from dcu.
   input logic [ECI_WORD_WIDTH-1:0] 	    eci_rsp_hdr_i,
   input logic [ECI_PACKET_SIZE_WIDTH-1:0]  eci_rsp_size_i,
   input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0]  eci_rsp_vc_i,
   input logic 				    eci_rsp_valid_i,
   output logic 			    eci_rsp_ready_o,
   // VC 10, 11 response without data (only header).
   output logic [ECI_WORD_WIDTH-1:0] 	    rsp_wod_hdr_o,
   output logic [ECI_PACKET_SIZE_WIDTH-1:0] rsp_wod_pkt_size_o,
   output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] rsp_wod_pkt_vc_o,
   output logic 			    rsp_wod_pkt_valid_o,
   input logic 				    rsp_wod_pkt_ready_i,

   // VC 5,4 response with data (only header).
   output logic [ECI_WORD_WIDTH-1:0] 	    rsp_wd_hdr_o,
   output logic [ECI_PACKET_SIZE_WIDTH-1:0] rsp_wd_pkt_size_o,
   output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] rsp_wd_pkt_vc_o,
   output logic 			    rsp_wd_pkt_valid_o,
   input logic 				    rsp_wd_pkt_ready_i
   );

   typedef struct packed {
      logic [ECI_WORD_WIDTH-1:0] 	    hdr;
      logic [ECI_PACKET_SIZE_WIDTH-1:0]     size;
      logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0]     vc;
      logic 				    valid;
      logic 				    ready;
   } eci_hdr_if_t;

   eci_hdr_if_t dc_i;
   eci_hdr_if_t vc_5to2_o, vc_6to11_o;
   eci_hdr_if_t vc_5to2_i, vc_5to4_o;
   eci_hdr_if_t vc_6to11_i, vc_10to11_o;

   always_comb begin : OP_ASSIGN
      // ECI response from dcu.
      eci_rsp_ready_o     = dc_i.ready;
      // VC 10, 11
      rsp_wod_hdr_o       = vc_10to11_o.hdr;
      rsp_wod_pkt_size_o  = vc_10to11_o.size;
      rsp_wod_pkt_vc_o    = vc_10to11_o.vc;
      rsp_wod_pkt_valid_o = vc_10to11_o.valid;
      // VC 5,4
      rsp_wd_hdr_o        = vc_5to4_o.hdr;
      rsp_wd_pkt_size_o   = vc_5to4_o.size;
      rsp_wd_pkt_vc_o     = vc_5to4_o.vc;
      rsp_wd_pkt_valid_o  = vc_5to4_o.valid;
   end : OP_ASSIGN
   
   // Route input ECI packet
   // VC boundary = 6 // route VCs 6 and above to ch1. 
   // Channel 0 routes to VCs 5,4,3,2
   // Channel 1 routes to VCs 6,7,8,9,10,11
   always_comb begin : GEHVCRTR_6_IP_ASSIGN
      // Input ECI packet from DCU.
      dc_i.hdr   = eci_rsp_hdr_i;
      dc_i.size  = eci_rsp_size_i;
      dc_i.vc    = eci_rsp_vc_i;
      dc_i.valid = eci_rsp_valid_i;
      // CH 0
      vc_5to2_o.ready = vc_5to2_i.ready;
      // CH 1
      vc_6to11_o.ready = vc_6to11_i.ready;
   end : GEHVCRTR_6_IP_ASSIGN
   dcu_gen_ecih_vc_router #
     (
      .VC_BOUNDARY(6) // route VCs 6 and above to ch1.
      )
   gehvcrtr_6
     (
      .clk		(clk),
      .reset		(reset),
      // Input ECI packet from dcu.
      .eci_hdr_i	(dc_i.hdr),
      .eci_pkt_size_i	(dc_i.size),
      .eci_pkt_vc_i	(dc_i.vc),
      .eci_pkt_valid_i	(dc_i.valid),
      .eci_pkt_ready_o	(dc_i.ready),
      // CH 0 Route to VCs 5,4,3,2
      .eci_hdr0_o	(vc_5to2_o.hdr), 
      .eci_pkt0_size_o	(vc_5to2_o.size), 
      .eci_pkt0_vc_o	(vc_5to2_o.vc), 
      .eci_pkt0_valid_o	(vc_5to2_o.valid), 
      .eci_pkt0_ready_i	(vc_5to2_o.ready),
      // CH 1 Route to VCs 6,7,8,9,10,11
      .eci_hdr1_o	(vc_6to11_o.hdr),
      .eci_pkt1_size_o	(vc_6to11_o.size),
      .eci_pkt1_vc_o	(vc_6to11_o.vc),
      .eci_pkt1_valid_o	(vc_6to11_o.valid),
      .eci_pkt1_ready_i	(vc_6to11_o.ready)
      );

   // Takes hdrs from VC 5 to 2 and splits based on
   // VC BOUNDARY = 4, route VC 4 and above to CH1.
   // Channel 0: 3,2
   // Channel 1: 5,4
   // Since we dont care about VC 3,2 it is not connected.
   always_comb begin : GEHVCRTR_4_IP_ASSIGN
      vc_5to2_i.hdr   = vc_5to2_o.hdr;
      vc_5to2_i.size  = vc_5to2_o.size;
      vc_5to2_i.vc    = vc_5to2_o.vc;
      vc_5to2_i.valid = vc_5to2_o.valid;
      vc_5to4_o.ready = rsp_wd_pkt_ready_i;
   end : GEHVCRTR_4_IP_ASSIGN
   dcu_gen_ecih_vc_router #
     (
      .VC_BOUNDARY(4) // route VCs 4 and above to ch1.
      )
   gehvcrtr_4
     (
      .clk		(clk),
      .reset		(reset),
      // Inputs from VC 5,4,3,2
      .eci_hdr_i	(vc_5to2_i.hdr),
      .eci_pkt_size_i	(vc_5to2_i.size),
      .eci_pkt_vc_i	(vc_5to2_i.vc),
      .eci_pkt_valid_i	(vc_5to2_i.valid),
      .eci_pkt_ready_o	(vc_5to2_i.ready),
      // CH 0 Route to VCs 3,2
      .eci_hdr0_o	(), // Not connected on purpose. 
      .eci_pkt0_size_o	(), // Not connected on purpose. 
      .eci_pkt0_vc_o	(), // Not connected on purpose. 
      .eci_pkt0_valid_o	(), // Not connected on purpose. 
      .eci_pkt0_ready_i	(1'b1), 
      // CH 1 Route to VCs 5,4
      .eci_hdr1_o	(vc_5to4_o.hdr),
      .eci_pkt1_size_o	(vc_5to4_o.size),
      .eci_pkt1_vc_o	(vc_5to4_o.vc),
      .eci_pkt1_valid_o	(vc_5to4_o.valid),
      .eci_pkt1_ready_i	(vc_5to4_o.ready)
      );

   // Takes hdrs from VC 6 to 11 and splits based on
   // VC BOUNDARY = 10, route VC 10 and above to CH1.
   // Channel 0: 6,7,8,9
   // Channel 1: 11,10
   // Since we dont care about VC 6,7,8,9 it is not connected.
   always_comb begin : GEHVCRTR_10_IP_ASSIGN
      vc_6to11_i.hdr    = vc_6to11_o.hdr; 
      vc_6to11_i.size   = vc_6to11_o.size; 
      vc_6to11_i.vc     = vc_6to11_o.vc; 
      vc_6to11_i.valid  = vc_6to11_o.valid;
      vc_10to11_o.ready = rsp_wod_pkt_ready_i;
   end : GEHVCRTR_10_IP_ASSIGN
   dcu_gen_ecih_vc_router #
     (
      .VC_BOUNDARY(10) // route VCs 10 and above to ch1.
      )
   gehvcrtr_10
     (
      .clk		(clk),
      .reset		(reset),
      // Inputs from VC 6,7,8,9,10,11
      .eci_hdr_i	(vc_6to11_i.hdr),
      .eci_pkt_size_i	(vc_6to11_i.size),
      .eci_pkt_vc_i	(vc_6to11_i.vc),
      .eci_pkt_valid_i	(vc_6to11_i.valid),
      .eci_pkt_ready_o	(vc_6to11_i.ready),
      // CH 0 Route to VCs 6,7,8,9
      .eci_hdr0_o	(), // Not connected on purpose. 
      .eci_pkt0_size_o	(), // Not connected on purpose. 
      .eci_pkt0_vc_o	(), // Not connected on purpose. 
      .eci_pkt0_valid_o	(), // Not connected on purpose. 
      .eci_pkt0_ready_i	(1'b1), 
      // CH 1 Route to VCs 10,11
      .eci_hdr1_o	(vc_10to11_o.hdr),
      .eci_pkt1_size_o	(vc_10to11_o.size),
      .eci_pkt1_vc_o	(vc_10to11_o.vc),
      .eci_pkt1_valid_o	(vc_10to11_o.valid),
      .eci_pkt1_ready_i	(vc_10to11_o.ready)
      );

endmodule // dcu_to_vc_router

`endif
