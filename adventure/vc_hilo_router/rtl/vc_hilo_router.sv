/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-09-15
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */

`ifndef VC_HILO_ROUTER_SV
`define VC_HILO_ROUTER_SV

import eci_cmd_defs::*;

// Module takes a ECI header+VC number
// to route the header to HI or LO output channels.
// VC: 2,3,4,5 go to HI channel.
// VC: 6,7,8,9,10,11 go to LO channel.
// Input is passed through a pipeline stage. 

module vc_hilo_router 
  (
   input logic 				    clk,
   input logic 				    reset,
   // Input ECI header + VC number.
   input logic [ECI_WORD_WIDTH-1:0] 	    ecih_hdr_i,
   input logic [ECI_PACKET_SIZE_WIDTH-1:0]  ecih_pkt_size_i,
   input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0]  ecih_pkt_vc_i,
   input logic 				    ecih_pkt_valid_i,
   output logic 			    ecih_pkt_ready_o,
   // Output Lo channel.
   // VC: 6/7/8/9/10/11
   output logic [ECI_WORD_WIDTH-1:0] 	    lo_wod_hdr_o,
   output logic [ECI_PACKET_SIZE_WIDTH-1:0] lo_wod_pkt_size_o,
   output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] lo_wod_pkt_vc_o,
   output logic 			    lo_wod_pkt_valid_o,
   input logic 				    lo_wod_pkt_ready_i,
   // Output Hi channel
   // VC: 2/3/4/5
   output logic [ECI_WORD_WIDTH-1:0] 	    hi_wd_hdr_o,
   output logic [ECI_PACKET_SIZE_WIDTH-1:0] hi_wd_pkt_size_o,
   output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] hi_wd_pkt_vc_o,
   output logic 			    hi_wd_pkt_valid_o,
   input logic 				    hi_wd_pkt_ready_i
   );

   localparam PIPE_DATA_WIDTH = ECI_WORD_WIDTH + ECI_PACKET_SIZE_WIDTH + ECI_LCL_TOT_NUM_VCS_WIDTH;
   typedef struct packed {
      logic [ECI_WORD_WIDTH-1:0] 	    hdr;
      logic [ECI_PACKET_SIZE_WIDTH-1:0]     size;
      logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0]     vc;
      logic 				    valid;
      logic 				    ready;
   } eci_hdr_if_t;
   
   eci_hdr_if_t ecih_p_i, ecih_p_o;
   logic 				    lo_hs, hi_hs;

   always_comb begin : MISC_ASSIGN
      lo_hs = lo_wod_pkt_valid_o & lo_wod_pkt_ready_i;
      hi_hs = hi_wd_pkt_valid_o & hi_wd_pkt_ready_i;
   end : MISC_ASSIGN
   
   always_comb begin : OUT_ASSIGN
      ecih_pkt_ready_o	 = ecih_p_i.ready;
      // Broadcast incoming ecih to all output channels.
      // only one of the output channels will be enabled.
      // Lo channel.
      lo_wod_hdr_o      = ecih_p_o.hdr;
      lo_wod_pkt_size_o = ecih_p_o.size;
      lo_wod_pkt_vc_o   = ecih_p_o.vc;
      // Hi channel
      hi_wd_hdr_o       = ecih_p_o.hdr;
      hi_wd_pkt_size_o  = ecih_p_o.size;
      hi_wd_pkt_vc_o    = ecih_p_o.vc;
   end : OUT_ASSIGN

   // Input eci header is passed through
   // a pipeline stage. Output of pipeline
   // stage is broadcast to both output channels.
   // but only one of the channels is enabled.
   // The output of pipeline stage is consumed
   // when a handshake happens in either of the
   // output channels. 
   always_comb begin : PIPE_IP_ASSIGN
      ecih_p_i.hdr = ecih_hdr_i;
      ecih_p_i.size = ecih_pkt_size_i;
      ecih_p_i.vc = ecih_pkt_vc_i;
      ecih_p_i.valid = ecih_pkt_valid_i;
      ecih_p_o.ready = lo_hs | hi_hs;
   end : PIPE_IP_ASSIGN

   axis_pipeline_stage #
     (
      .DATA_WIDTH(PIPE_DATA_WIDTH)
      )
   rtr_in_stage
     (
      .clk(clk),
      .reset(reset),
      .us_data({ecih_p_i.hdr, ecih_p_i.size, ecih_p_i.vc}),
      .us_valid(ecih_p_i.valid),
      .us_ready(ecih_p_i.ready),
      .ds_data({ecih_p_o.hdr, ecih_p_o.size, ecih_p_o.vc}),
      .ds_valid(ecih_p_o.valid),
      .ds_ready(ecih_p_o.ready)
      );

   always_comb begin : CONTROLLER
      lo_wod_pkt_valid_o = 1'b0;
      hi_wd_pkt_valid_o = 1'b0;
      if(ecih_p_o.vc < 'd6) begin
	 // Hi channel.
	 // VC 2,3,4,5
	 lo_wod_pkt_valid_o = 1'b0;
	 hi_wd_pkt_valid_o = ecih_p_o.valid;
      end else begin
	 // Lo channel.
	 // VC 6,7,8,9,10,11
	 lo_wod_pkt_valid_o = ecih_p_o.valid;
	 hi_wd_pkt_valid_o = 1'b0;
      end
   end : CONTROLLER
   
endmodule // vc_hilo_router

`endif
