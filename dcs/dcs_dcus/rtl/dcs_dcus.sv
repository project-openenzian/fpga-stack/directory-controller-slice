/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-09-16
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */

`ifndef DCS_DCUS_SV
`define DCS_DCUS_SV

import eci_cmd_defs::*;
import eci_dcs_defs::*;
import eci_dirc_defs::*; // DCU specific defs.
import eci_cc_defs::*;   // CC specific defs to support dirc debug interface.

// Module to instantiate DCUs.
// No data path here, only headers and control signals.

module dcs_dcus #
  (
   parameter PERF_REGS_WIDTH = 64,
   parameter SYNTH_PERF_REGS = 0
   )
   (
    input logic 				 clk,
    input logic 				 reset,

    // Input ECI events.
    // ECI packet for request without data. (VC 6 or 7) (only header).
    input logic [ECI_WORD_WIDTH-1:0] 		 req_wod_hdr_i,
    input logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 req_wod_pkt_size_i,
    input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0]  req_wod_pkt_vc_i,
    input logic 				 req_wod_pkt_valid_i,
    output logic 				 req_wod_pkt_ready_o,

    // ECI packet for response without data.(VC 10 or 11). (only header).
    input logic [ECI_WORD_WIDTH-1:0] 		 rsp_wod_hdr_i,
    input logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 rsp_wod_pkt_size_i,
    input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0]  rsp_wod_pkt_vc_i,
    input logic 				 rsp_wod_pkt_valid_i,
    output logic 				 rsp_wod_pkt_ready_o,

    // ECI packet for response with data. (VC 4 or 5). (only header).
    input logic [ECI_WORD_WIDTH-1:0] 		 rsp_wd_hdr_i,
    input logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 rsp_wd_pkt_size_i,
    input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0]  rsp_wd_pkt_vc_i,
    input logic 				 rsp_wd_pkt_valid_i,
    output logic 				 rsp_wd_pkt_ready_o,

    // LCL packet for clean, clean invalidates. (VC 16 or 17) (only header).
    input logic [ECI_WORD_WIDTH-1:0] 		 lcl_fwd_wod_hdr_i,
    input logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 lcl_fwd_wod_pkt_size_i,
    input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0]  lcl_fwd_wod_pkt_vc_i,
    input logic 				 lcl_fwd_wod_pkt_valid_i,
    output logic 				 lcl_fwd_wod_pkt_ready_o,

    // LCL packet for response without data (unlock). (VC 18 or 19) (only header).
    input logic [ECI_WORD_WIDTH-1:0] 		 lcl_rsp_wod_hdr_i,
    input logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 lcl_rsp_wod_pkt_size_i,
    input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0]  lcl_rsp_wod_pkt_vc_i,
    input logic 				 lcl_rsp_wod_pkt_valid_i,
    output logic 				 lcl_rsp_wod_pkt_ready_o,

    // Output ECI header only events.
    // Response without data: VC 10 or 11.
    output logic [ECI_WORD_WIDTH-1:0] 		 rsp_wod_hdr_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 rsp_wod_pkt_size_o,
    output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] rsp_wod_pkt_vc_o,
    output logic 				 rsp_wod_pkt_valid_o,
    input logic 				 rsp_wod_pkt_ready_i,

    // Output ECI header + data events but only header.
    // Response with data headers: VC 4 or 5. (only header).
    output logic [ECI_WORD_WIDTH-1:0] 		 rsp_wd_hdr_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 rsp_wd_pkt_size_o,
    output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] rsp_wd_pkt_vc_o,
    output logic 				 rsp_wd_pkt_valid_o,
    input logic 				 rsp_wd_pkt_ready_i,

    // Output ECI Fwd without data headers: VC 8 or 9.
    output logic [ECI_WORD_WIDTH-1:0] 		 fwd_wod_hdr_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 fwd_wod_pkt_size_o,
    output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] fwd_wod_pkt_vc_o,
    output logic 				 fwd_wod_pkt_valid_o,
    input logic 				 fwd_wod_pkt_ready_i,

    // Output lcl rsp without data headers: VC 18 or 19.
    output logic [ECI_WORD_WIDTH-1:0] 		 lcl_rsp_wod_hdr_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 lcl_rsp_wod_pkt_size_o,
    output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] lcl_rsp_wod_pkt_vc_o,
    output logic 				 lcl_rsp_wod_pkt_valid_o,
    input logic 				 lcl_rsp_wod_pkt_ready_i,

    // Output read descriptors
    // Read descriptors: Request.
    output logic [MAX_DCU_ID_WIDTH-1:0] 	 rd_req_id_o,
    output logic [DS_ADDR_WIDTH-1:0] 		 rd_req_addr_o,
    output logic 				 rd_req_valid_o,
    input logic 				 rd_req_ready_i,

    // Read descriptors: Response control
    // no data here.
    input logic [MAX_DCU_ID_WIDTH-1:0] 		 rd_rsp_id_i,
    input logic 				 rd_rsp_valid_i,
    output logic 				 rd_rsp_ready_o,

    // Output write descriptors
    // Write requests only control, no data here.
    output logic [MAX_DCU_ID_WIDTH-1:0] 	 wr_req_id_o,
    output logic [DS_ADDR_WIDTH-1:0] 		 wr_req_addr_o,
    output logic [ECI_CL_SIZE_BYTES-1:0] 	 wr_req_strb_o,
    output logic 				 wr_req_valid_o,
    input logic 				 wr_req_ready_i,

    // Write response.
    input logic [MAX_DCU_ID_WIDTH-1:0] 		 wr_rsp_id_i,
    input logic [1:0] 				 wr_rsp_bresp_i,
    input logic 				 wr_rsp_valid_i,
    output logic 				 wr_rsp_ready_o,

    // Tracing
    output logic		tracing_valid[2],
    output logic		tracing_error[2],
    output logic [39:0] tracing_cli[2],
    output logic [6:0]	tracing_state[2],
    output logic [3:0]	tracing_action[2],
    output logic [4:0]	tracing_request[2]
    );

   localparam ECI_HDR_PKT_WIDTH = ECI_WORD_WIDTH + ECI_PACKET_SIZE_WIDTH +
				  ECI_LCL_TOT_NUM_VCS_WIDTH;
   localparam WR_ADDR_STRB_WIDTH = DS_ADDR_WIDTH + ECI_CL_SIZE_BYTES;
   localparam RD_ID_ADDR_WIDTH = MAX_DCU_ID_WIDTH + DS_ADDR_WIDTH;
   localparam WR_ID_ADDR_STRB_WIDTH = MAX_DCU_ID_WIDTH + WR_ADDR_STRB_WIDTH;
   localparam WR_ID_BRESP_WIDTH = MAX_DCU_ID_WIDTH + 2;

   typedef struct packed {
      logic [ECI_WORD_WIDTH-1:0]        hdr;
      logic [ECI_PACKET_SIZE_WIDTH-1:0] size;
      logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] vc;
      logic				valid;
      logic				ready;
   } eci_hdr_if_t;

   typedef struct packed {
      logic [MAX_DCU_ID_WIDTH-1:0]		   id;
      logic [DS_ADDR_WIDTH-1:0]			   addr;
      logic					   valid;
      logic					   ready;
   } rd_req_ctrl_if_t;

   typedef struct packed {
      logic [MAX_DCU_ID_WIDTH-1:0] id;
      logic			   valid;
      logic			   ready;
   } rd_rsp_ctrl_if_t;

   typedef struct packed {
      logic [MAX_DCU_ID_WIDTH-1:0]  id;
      logic [DS_ADDR_WIDTH-1:0]     addr;
      logic [ECI_CL_SIZE_BYTES-1:0] strb;
      logic			    valid;
      logic			    ready;
   } wr_req_ctrl_if_t;

   typedef struct packed {
      logic [MAX_DCU_ID_WIDTH-1:0]  id;
      logic [1:0]		    bresp;
      logic			    valid;
      logic			    ready;
   } wr_rsp_ctrl_if_t;

   typedef struct packed {
      logic [PERF_REGS_WIDTH-1:0] no_req_pkt_rdy;
      logic [PERF_REGS_WIDTH-1:0] no_req_pkt_stl;
      logic [PERF_REGS_WIDTH-1:0] no_rsp_pkt_sent;
      logic [PERF_REGS_WIDTH-1:0] no_rd_req;
      logic [PERF_REGS_WIDTH-1:0] no_rd_rsp;
      logic [PERF_REGS_WIDTH-1:0] no_wr_req;
      logic [PERF_REGS_WIDTH-1:0] no_wr_rsp;
      logic [PERF_REGS_WIDTH-1:0] no_cyc_bw_req_vld_rdy;
   } perf_counters_if_t;

   typedef struct packed {
      // Debug interface.
      logic [ECI_WORD_WIDTH-1:0]        eci_req_hdr;
      logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] eci_req_vc;
      logic [ECI_PACKET_SIZE_WIDTH-1:0] eci_req_size;
      logic				eci_req_ready;
      logic				eci_req_stalled;
      logic [ECI_WORD_WIDTH-1:0]	eci_rsp_hdr;
      logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] eci_rsp_vc;
      logic [ECI_PACKET_SIZE_WIDTH-1:0] eci_rsp_size;
      logic				eci_rsp_valid;
      cc_req_t				req_cc_enc;
      logic				rdda;
      logic				wdda;
      logic 				stall_prot_o;
      logic 				stall_rd_busy_o;
      logic 				stall_wr_busy_o;
      logic 				stall_eci_tr_busy_o;
      logic 				stall_rtg_full_o;
      logic 				stall_wait_op_hs_o;
      cc_req_t				cc_active_req;
      cc_state_t			cc_present_state;
      cc_state_t			cc_next_state;
      cc_action_t			cc_next_action;
      dirc_err_t			err_code;
      logic				error;
      logic				valid;
   } dbg_if_t;

   typedef struct packed { // 56 bits
      logic [6:0]   padding; // to 64 bits
      logic			error;
      logic [39:0]  cli;
      logic [6:0]	state;
      logic [3:0]	action;
      logic [4:0]	request;
   } trace_if_t;

    logic [63:0] dc_trace_l1_muxed[15:0];
   typedef logic [DS_DCU_IDX_WIDTH-1:0] dcu_idx_t;
   typedef logic [DS_DCU_ID_WIDTH-1:0]	dcu_id_t;

   // DCU IDX from header signals.
   ds_cl_addr_t req_wod_addr_c, rsp_wod_addr_c, rsp_wd_addr_c, lcl_fwd_wod_addr_c, lcl_rsp_wod_addr_c;
   dcu_idx_t    req_wod_dcu_idx, rsp_wod_dcu_idx, rsp_wd_dcu_idx, lcl_fwd_wod_dcu_idx, lcl_rsp_wod_dcu_idx;
   // Pipeline stage signals for incoming channels.
   eci_hdr_if_t req_wod_p_i, req_wod_p_o;
   dcu_idx_t req_wod_dcu_idx_p_i, req_wod_dcu_idx_p_o;
   eci_hdr_if_t rsp_wod_p_i, rsp_wod_p_o;
   dcu_idx_t rsp_wod_dcu_idx_p_i, rsp_wod_dcu_idx_p_o;
   eci_hdr_if_t rsp_wd_p_i, rsp_wd_p_o;
   dcu_idx_t rsp_wd_dcu_idx_p_i, rsp_wd_dcu_idx_p_o;
   eci_hdr_if_t lcl_fwd_wod_p_i, lcl_fwd_wod_p_o;
   dcu_idx_t lcl_fwd_wod_dcu_idx_p_i, lcl_fwd_wod_dcu_idx_p_o;
   eci_hdr_if_t lcl_rsp_wod_p_i, lcl_rsp_wod_p_o;
   dcu_idx_t lcl_rsp_wod_dcu_idx_p_i, lcl_rsp_wod_dcu_idx_p_o;
   // Signals routing incoming channels to DCUs.
   eci_hdr_if_t req_wod_2_dcu_i;
   dcu_idx_t req_wod_2_dcu_idx_i;
   logic [DS_NUM_DCU_IDX-1:0][ECI_HDR_PKT_WIDTH-1:0] req_wod_2_dcu_hdr_o;
   logic [DS_NUM_DCU_IDX-1:0]			     req_wod_2_dcu_valid_o;
   logic [DS_NUM_DCU_IDX-1:0]			     req_wod_2_dcu_ready_i;
   eci_hdr_if_t rsp_wod_2_dcu_i;
   dcu_idx_t rsp_wod_2_dcu_idx_i;
   logic [DS_NUM_DCU_IDX-1:0][ECI_HDR_PKT_WIDTH-1:0] rsp_wod_2_dcu_hdr_o;
   logic [DS_NUM_DCU_IDX-1:0]			     rsp_wod_2_dcu_valid_o;
   logic [DS_NUM_DCU_IDX-1:0]			     rsp_wod_2_dcu_ready_i;
   eci_hdr_if_t rsp_wd_2_dcu_i;
   dcu_idx_t rsp_wd_2_dcu_idx_i;
   logic [DS_NUM_DCU_IDX-1:0][ECI_HDR_PKT_WIDTH-1:0] rsp_wd_2_dcu_hdr_o;
   logic [DS_NUM_DCU_IDX-1:0]			     rsp_wd_2_dcu_valid_o;
   logic [DS_NUM_DCU_IDX-1:0]			     rsp_wd_2_dcu_ready_i;
   eci_hdr_if_t lcl_fwd_wod_2_dcu_i;
   dcu_idx_t lcl_fwd_wod_2_dcu_idx_i;
   logic [DS_NUM_DCU_IDX-1:0][ECI_HDR_PKT_WIDTH-1:0] lcl_fwd_wod_2_dcu_hdr_o;
   logic [DS_NUM_DCU_IDX-1:0]			     lcl_fwd_wod_2_dcu_valid_o;
   logic [DS_NUM_DCU_IDX-1:0]			     lcl_fwd_wod_2_dcu_ready_i;
   eci_hdr_if_t lcl_rsp_wod_2_dcu_i;
   dcu_idx_t lcl_rsp_wod_2_dcu_idx_i;
   logic [DS_NUM_DCU_IDX-1:0][ECI_HDR_PKT_WIDTH-1:0] lcl_rsp_wod_2_dcu_hdr_o;
   logic [DS_NUM_DCU_IDX-1:0]			     lcl_rsp_wod_2_dcu_valid_o;
   logic [DS_NUM_DCU_IDX-1:0]			     lcl_rsp_wod_2_dcu_ready_i;
   // DCU specific signals.
   eci_hdr_if_t [DS_NUM_DCU_IDX-1:0] dc_req_wod_i, dc_rsp_wod_i, dc_rsp_wd_hdr_i, dc_lcl_fwd_wod_i, dc_lcl_rsp_wod_i;
   eci_hdr_if_t [DS_NUM_DCU_IDX-1:0] dc_eci_rsp_hdr_o;
   rd_req_ctrl_if_t [DS_NUM_DCU_IDX-1:0] dc_rd_req_ctrl_o;
   rd_rsp_ctrl_if_t [DS_NUM_DCU_IDX-1:0] dc_rd_rsp_ctrl_i;
   wr_req_ctrl_if_t [DS_NUM_DCU_IDX-1:0] dc_wr_req_ctrl_o;
   wr_rsp_ctrl_if_t [DS_NUM_DCU_IDX-1:0] dc_wr_rsp_ctrl_i;
   dbg_if_t [DS_NUM_DCU_IDX-1:0] dc_dbg_o;
   perf_counters_if_t [DS_NUM_DCU_IDX-1:0] dc_perf_o;

   trace_if_t [DS_NUM_DCU_IDX-1:0] dc_trace_l0_o;
   logic      [DS_NUM_DCU_IDX-1:0] dc_trace_l0_valid_o;
   logic      [DS_NUM_DCU_IDX-1:0] dc_trace_l0_ready_o;

   trace_if_t [DS_NUM_DCU_IDX-1:0] dc_trace_l1_o;
   logic      [DS_NUM_DCU_IDX-1:0] dc_trace_l1_valid_o;
   logic      [DS_NUM_DCU_IDX-1:0] dc_trace_l1_ready_o;

   trace_if_t [15:0] dc_trace_l2_o;
   logic      [15:0] dc_trace_l2_valid_o;
   logic      [15:0] dc_trace_l2_ready_o;

   logic [63:0] dc_trace_l1_data_o[32][8];
   logic [2:0] dc_trace_l1_size_o[32];
   logic [3:0] dc_trace_l1_vc_no_o[32];

   logic [63:0] dc_trace_l2_data_o[16][8];
   logic [2:0] dc_trace_l2_size_o[16];
   logic [3:0] dc_trace_l2_vc_no_o[16];

   // ECI response from DCU arbiter specific signals.
   // id is not used.
   logic [DS_NUM_DCU_IDX-1:0][ECI_HDR_PKT_WIDTH-1:0] eci_rsp_arb_data_i;
   logic [DS_NUM_DCU_IDX-1:0]			     eci_rsp_arb_valid_i;
   logic [DS_NUM_DCU_IDX-1:0]			     eci_rsp_arb_ready_o;
   logic [ECI_HDR_PKT_WIDTH-1:0]		     eci_rsp_arb_data_o;
   logic					     eci_rsp_arb_valid_o;
   logic					     eci_rsp_arb_ready_i;
   // Read requests from DCU arbiter specific signals.
   logic [DS_NUM_DCU_IDX-1:0][MAX_DCU_ID_WIDTH-1:0]  rd_req_arb_id_i;
   logic [DS_NUM_DCU_IDX-1:0][DS_ADDR_WIDTH-1:0]     rd_req_arb_data_i;
   logic [DS_NUM_DCU_IDX-1:0]			     rd_req_arb_valid_i;
   logic [DS_NUM_DCU_IDX-1:0]			     rd_req_arb_ready_o;
   logic [MAX_DCU_ID_WIDTH-1:0]			     rd_req_arb_id_o;
   logic [DS_ADDR_WIDTH-1:0]			     rd_req_arb_data_o;
   logic					     rd_req_arb_valid_o;
   logic					     rd_req_arb_ready_i;
   // Arbitrated rd req pipeline stage specific signals.
   logic [RD_ID_ADDR_WIDTH-1:0]			     rd_req_p_i;
   logic					     rd_req_p_valid_i;
   logic					     rd_req_p_ready_o;
   logic [RD_ID_ADDR_WIDTH-1:0]			     rd_req_p_o;
   logic					     rd_req_p_valid_o;
   logic					     rd_req_p_ready_i;
   // Write requests from DCU arbiter specific signals.
   logic [DS_NUM_DCU_IDX-1:0][MAX_DCU_ID_WIDTH-1:0]   wr_req_arb_id_i;
   logic [DS_NUM_DCU_IDX-1:0][WR_ADDR_STRB_WIDTH-1:0] wr_req_arb_data_i;
   logic [DS_NUM_DCU_IDX-1:0]			      wr_req_arb_valid_i;
   logic [DS_NUM_DCU_IDX-1:0]			      wr_req_arb_ready_o;
   logic [MAX_DCU_ID_WIDTH-1:0]			      wr_req_arb_id_o;
   logic [WR_ADDR_STRB_WIDTH-1:0]		      wr_req_arb_data_o;
   logic					      wr_req_arb_valid_o;
   logic					      wr_req_arb_ready_i;
   // Arbitrated wr req pipeline stage specific signals.
   logic [WR_ID_ADDR_STRB_WIDTH-1:0]		     wr_req_p_i;
   logic					     wr_req_p_valid_i;
   logic					     wr_req_p_ready_o;
   logic [WR_ID_ADDR_STRB_WIDTH-1:0]		     wr_req_p_o;
   logic					     wr_req_p_valid_o;
   logic					     wr_req_p_ready_i;
   // rd rsp router specific signals.
   logic [MAX_DCU_ID_WIDTH-1:0]			     rd_rsp_rtr_data_i;
   logic [DS_DCU_IDX_WIDTH-1:0]			     rd_rsp_rtr_sel_i;
   logic					     rd_rsp_rtr_valid_i;
   logic					     rd_rsp_rtr_ready_o;
   logic [DS_NUM_DCU_IDX-1:0][MAX_DCU_ID_WIDTH-1:0]  rd_rsp_rtr_data_o;
   logic [DS_NUM_DCU_IDX-1:0]			     rd_rsp_rtr_valid_o;
   logic [DS_NUM_DCU_IDX-1:0]			     rd_rsp_rtr_ready_i;
   logic [DS_DCU_ID_WIDTH-1:0]			     rd_rsp_rtr_dcu_id;
   logic [DS_DCU_IDX_WIDTH-1:0]			     rd_rsp_rtr_dcu_idx;
   // wr rsp router specific signals.
   logic [WR_ID_BRESP_WIDTH-1:0]		     wr_rsp_rtr_data_i;
   logic [DS_DCU_IDX_WIDTH-1:0]			     wr_rsp_rtr_sel_i;
   logic					     wr_rsp_rtr_valid_i;
   logic					     wr_rsp_rtr_ready_o;
   logic [DS_NUM_DCU_IDX-1:0][WR_ID_BRESP_WIDTH-1:0]  wr_rsp_rtr_data_o;
   logic [DS_NUM_DCU_IDX-1:0]			     wr_rsp_rtr_valid_o;
   logic [DS_NUM_DCU_IDX-1:0]			     wr_rsp_rtr_ready_i;
   logic [DS_DCU_ID_WIDTH-1:0]			     wr_rsp_rtr_dcu_id;
   logic [DS_DCU_IDX_WIDTH-1:0]			     wr_rsp_rtr_dcu_idx;
   // DC to VC router signals.
   eci_hdr_if_t dv_ecih_i;
   eci_hdr_if_t dv_rsp_wod_o, dv_rsp_wd_hdr_o, dv_fwd_wod_o, dv_lcl_rsp_wod_o;

   always_comb begin : OUT_ASSIGN
      // Request without data: VC 6 or 7.
      req_wod_pkt_ready_o = req_wod_p_i.ready;

      // Response without data: VC 10 or 11.
      rsp_wod_pkt_ready_o = rsp_wod_p_i.ready;

      // Response with data: VC 4 or 5.
      rsp_wd_pkt_ready_o = rsp_wd_p_i.ready;

      // lcl fwd without data: VC 16 or 17.
      lcl_fwd_wod_pkt_ready_o = lcl_fwd_wod_p_i.ready;

      // lcl rsp without data: VC 18 or 19.
      lcl_rsp_wod_pkt_ready_o = lcl_rsp_wod_p_i.ready;

      // Output ECI header only events.
      // Response without data: VC 10 or 11.
      rsp_wod_hdr_o		= dv_rsp_wod_o.hdr;
      rsp_wod_pkt_size_o	= dv_rsp_wod_o.size;
      rsp_wod_pkt_vc_o		= dv_rsp_wod_o.vc;
      rsp_wod_pkt_valid_o	= dv_rsp_wod_o.valid;

      // Output ECI header + data events but only header.
      // Response with data headers: VC 4 or 5.
      rsp_wd_hdr_o		= dv_rsp_wd_hdr_o.hdr;
      rsp_wd_pkt_size_o		= dv_rsp_wd_hdr_o.size;
      rsp_wd_pkt_vc_o		= dv_rsp_wd_hdr_o.vc;
      rsp_wd_pkt_valid_o	= dv_rsp_wd_hdr_o.valid;

      // ECI forward without data headers VC 8 or 9
      fwd_wod_hdr_o             = dv_fwd_wod_o.hdr;
      fwd_wod_pkt_size_o        = dv_fwd_wod_o.size;
      fwd_wod_pkt_vc_o          = dv_fwd_wod_o.vc;
      fwd_wod_pkt_valid_o       = dv_fwd_wod_o.valid;

      // LCL rsp without data headers. VC 18 or 19
      lcl_rsp_wod_hdr_o         = dv_lcl_rsp_wod_o.hdr;
      lcl_rsp_wod_pkt_size_o    = dv_lcl_rsp_wod_o.size;
      lcl_rsp_wod_pkt_vc_o      = dv_lcl_rsp_wod_o.vc;
      lcl_rsp_wod_pkt_valid_o   = dv_lcl_rsp_wod_o.valid;

      // Output read descriptors
      // Read descriptors: Request.
      {rd_req_id_o, rd_req_addr_o} = rd_req_p_o;
      rd_req_valid_o = rd_req_p_valid_o;

      // Read descriptors: Response control
      // no data here.
      rd_rsp_ready_o = rd_rsp_rtr_ready_o;

      // Output write descriptors
      // Write requests only control, no data here.
      {wr_req_id_o,
       wr_req_addr_o,
       wr_req_strb_o} = wr_req_p_o;
      wr_req_valid_o = wr_req_p_valid_o;

      // Write response.
      wr_rsp_ready_o = wr_rsp_rtr_ready_o;
   end : OUT_ASSIGN

   // Get the DCU IDX from ECI header.
   // Cast the CL byte address into DCS Cl addr type
   // to extract dcu_id and pass it to DCS function
   // to get the dcu idx.
   always_comb begin : GET_HDR_DCU_IDX
      req_wod_addr_c  = ds_cl_addr_t'(req_wod_hdr_i[ECI_ADDR_WIDTH-1:0]);
      req_wod_dcu_idx = f_dcuid_2_dcuidx(.dcu_id_i(req_wod_addr_c.dcu_id));
      rsp_wod_addr_c  = ds_cl_addr_t'(rsp_wod_hdr_i[ECI_ADDR_WIDTH-1:0]);
      rsp_wod_dcu_idx = f_dcuid_2_dcuidx(.dcu_id_i(rsp_wod_addr_c.dcu_id));
      rsp_wd_addr_c   = ds_cl_addr_t'(rsp_wd_hdr_i[ECI_ADDR_WIDTH-1:0]);
      rsp_wd_dcu_idx  = f_dcuid_2_dcuidx(.dcu_id_i(rsp_wd_addr_c.dcu_id));
      lcl_fwd_wod_addr_c = ds_cl_addr_t'(lcl_fwd_wod_hdr_i[ECI_ADDR_WIDTH-1:0]);
      lcl_fwd_wod_dcu_idx = f_dcuid_2_dcuidx(.dcu_id_i(lcl_fwd_wod_addr_c.dcu_id));
      lcl_rsp_wod_addr_c = ds_cl_addr_t'(lcl_rsp_wod_hdr_i[ECI_ADDR_WIDTH-1:0]);
      lcl_rsp_wod_dcu_idx = f_dcuid_2_dcuidx(.dcu_id_i(lcl_rsp_wod_addr_c.dcu_id));
   end : GET_HDR_DCU_IDX

   // Pipeline stage for each incoming channel.
   // req_wod header input pipeline stage.
   always_comb begin : REQ_WOD_PIPE_IP_ASSIGN
      req_wod_dcu_idx_p_i	= req_wod_dcu_idx;
      req_wod_p_i.hdr		= req_wod_hdr_i;
      req_wod_p_i.size		= req_wod_pkt_size_i;
      req_wod_p_i.vc		= req_wod_pkt_vc_i;
      req_wod_p_i.valid		= req_wod_pkt_valid_i;
      req_wod_p_o.ready		= req_wod_2_dcu_i.ready;
   end : REQ_WOD_PIPE_IP_ASSIGN
   axis_pipeline_stage #
     (
      .DATA_WIDTH(DS_DCU_IDX_WIDTH + ECI_HDR_PKT_WIDTH)
       )
   req_wod_ps1
     (
      .clk(clk),
      .reset(reset),
      .us_data({
		req_wod_dcu_idx_p_i,
		req_wod_p_i.hdr,
		req_wod_p_i.size,
		req_wod_p_i.vc
		}),
      .us_valid(req_wod_p_i.valid),
      .us_ready(req_wod_p_i.ready),
      .ds_data({
		req_wod_dcu_idx_p_o,
		req_wod_p_o.hdr,
		req_wod_p_o.size,
		req_wod_p_o.vc
		}),
      .ds_valid(req_wod_p_o.valid),
      .ds_ready(req_wod_p_o.ready)
      );

   // rsp_wod header input pipeline stage.
   always_comb begin : RSP_WOD_PIPE_IP_ASSIGN
      rsp_wod_dcu_idx_p_i	= rsp_wod_dcu_idx;
      rsp_wod_p_i.hdr		= rsp_wod_hdr_i;
      rsp_wod_p_i.size		= rsp_wod_pkt_size_i;
      rsp_wod_p_i.vc		= rsp_wod_pkt_vc_i;
      rsp_wod_p_i.valid		= rsp_wod_pkt_valid_i;
      rsp_wod_p_o.ready		= rsp_wod_2_dcu_i.ready;
   end : RSP_WOD_PIPE_IP_ASSIGN
   axis_pipeline_stage #
     (
      .DATA_WIDTH(DS_DCU_IDX_WIDTH + ECI_HDR_PKT_WIDTH)
       )
   rsp_wod_ps1
     (
      .clk(clk),
      .reset(reset),
      .us_data({
		rsp_wod_dcu_idx_p_i,
		rsp_wod_p_i.hdr,
		rsp_wod_p_i.size,
		rsp_wod_p_i.vc
		}),
      .us_valid(rsp_wod_p_i.valid),
      .us_ready(rsp_wod_p_i.ready),
      .ds_data({
		rsp_wod_dcu_idx_p_o,
		rsp_wod_p_o.hdr,
		rsp_wod_p_o.size,
		rsp_wod_p_o.vc
		}),
      .ds_valid(rsp_wod_p_o.valid),
      .ds_ready(rsp_wod_p_o.ready)
      );

   // rsp_wd header input pipeline stage.
   always_comb begin : RSP_WD_PIPE_IP_ASSIGN
      rsp_wd_dcu_idx_p_i	= rsp_wd_dcu_idx;
      rsp_wd_p_i.hdr		= rsp_wd_hdr_i;
      rsp_wd_p_i.size		= rsp_wd_pkt_size_i;
      rsp_wd_p_i.vc		= rsp_wd_pkt_vc_i;
      rsp_wd_p_i.valid		= rsp_wd_pkt_valid_i;
      rsp_wd_p_o.ready		= rsp_wd_2_dcu_i.ready;
   end : RSP_WD_PIPE_IP_ASSIGN
   axis_pipeline_stage #
     (
      .DATA_WIDTH(DS_DCU_IDX_WIDTH + ECI_HDR_PKT_WIDTH)
       )
   rsp_wd_ps1
     (
      .clk(clk),
      .reset(reset),
      .us_data({
		rsp_wd_dcu_idx_p_i,
		rsp_wd_p_i.hdr,
		rsp_wd_p_i.size,
		rsp_wd_p_i.vc
		}),
      .us_valid(rsp_wd_p_i.valid),
      .us_ready(rsp_wd_p_i.ready),
      .ds_data({
		rsp_wd_dcu_idx_p_o,
		rsp_wd_p_o.hdr,
		rsp_wd_p_o.size,
		rsp_wd_p_o.vc
		}),
      .ds_valid(rsp_wd_p_o.valid),
      .ds_ready(rsp_wd_p_o.ready)
      );

   // lcl_fwd_wod header input pipeline stage.
   always_comb begin : LCL_FWD_WOD_PIPE_IP_ASSIGN
      lcl_fwd_wod_dcu_idx_p_i	= lcl_fwd_wod_dcu_idx;
      lcl_fwd_wod_p_i.hdr	= lcl_fwd_wod_hdr_i;
      lcl_fwd_wod_p_i.size	= lcl_fwd_wod_pkt_size_i;
      lcl_fwd_wod_p_i.vc	= lcl_fwd_wod_pkt_vc_i;
      lcl_fwd_wod_p_i.valid	= lcl_fwd_wod_pkt_valid_i;
      lcl_fwd_wod_p_o.ready	= lcl_fwd_wod_2_dcu_i.ready;
   end : LCL_FWD_WOD_PIPE_IP_ASSIGN
   axis_pipeline_stage #
     (
      .DATA_WIDTH(DS_DCU_IDX_WIDTH + ECI_HDR_PKT_WIDTH)
       )
   lcl_fwd_wod_ps1
     (
      .clk(clk),
      .reset(reset),
      .us_data({
		lcl_fwd_wod_dcu_idx_p_i,
		lcl_fwd_wod_p_i.hdr,
		lcl_fwd_wod_p_i.size,
		lcl_fwd_wod_p_i.vc
		}),
      .us_valid(lcl_fwd_wod_p_i.valid),
      .us_ready(lcl_fwd_wod_p_i.ready),
      .ds_data({
		lcl_fwd_wod_dcu_idx_p_o,
		lcl_fwd_wod_p_o.hdr,
		lcl_fwd_wod_p_o.size,
		lcl_fwd_wod_p_o.vc
		}),
      .ds_valid(lcl_fwd_wod_p_o.valid),
      .ds_ready(lcl_fwd_wod_p_o.ready)
      );

   // lcl_rsp_wod header input pipeline stage.
   always_comb begin : LCL_RSP_WOD_PIPE_IP_ASSIGN
      lcl_rsp_wod_dcu_idx_p_i	= lcl_rsp_wod_dcu_idx;
      lcl_rsp_wod_p_i.hdr	= lcl_rsp_wod_hdr_i;
      lcl_rsp_wod_p_i.size	= lcl_rsp_wod_pkt_size_i;
      lcl_rsp_wod_p_i.vc	= lcl_rsp_wod_pkt_vc_i;
      lcl_rsp_wod_p_i.valid	= lcl_rsp_wod_pkt_valid_i;
      lcl_rsp_wod_p_o.ready	= lcl_rsp_wod_2_dcu_i.ready;
   end : LCL_RSP_WOD_PIPE_IP_ASSIGN
   axis_pipeline_stage #
     (
      .DATA_WIDTH(DS_DCU_IDX_WIDTH + ECI_HDR_PKT_WIDTH)
       )
   lcl_rsp_wod_ps1
     (
      .clk(clk),
      .reset(reset),
      .us_data({
		lcl_rsp_wod_dcu_idx_p_i,
		lcl_rsp_wod_p_i.hdr,
		lcl_rsp_wod_p_i.size,
		lcl_rsp_wod_p_i.vc
		}),
      .us_valid(lcl_rsp_wod_p_i.valid),
      .us_ready(lcl_rsp_wod_p_i.ready),
      .ds_data({
		lcl_rsp_wod_dcu_idx_p_o,
		lcl_rsp_wod_p_o.hdr,
		lcl_rsp_wod_p_o.size,
		lcl_rsp_wod_p_o.vc
		}),
      .ds_valid(lcl_rsp_wod_p_o.valid),
      .ds_ready(lcl_rsp_wod_p_o.ready)
      );

   // Route incoming channels to DCUs.
   // Route req_wod to DCUs.
   always_comb begin : REQWOD2DCU_IP_ASSIGN
      req_wod_2_dcu_i.hdr	= req_wod_p_o.hdr;
      req_wod_2_dcu_i.size	= req_wod_p_o.size;
      req_wod_2_dcu_i.vc	= req_wod_p_o.vc;
      req_wod_2_dcu_i.valid	= req_wod_p_o.valid;
      req_wod_2_dcu_idx_i	= req_wod_dcu_idx_p_o;
   end : REQWOD2DCU_IP_ASSIGN
   axis_comb_router #
     (
      .DATA_WIDTH(ECI_HDR_PKT_WIDTH),
      .NUM_OUT(DS_NUM_DCU_IDX)
       )
   req_wod_to_dcus_rtr_1
     (
      // ECI header input + DCU IDX.
      .us_data_i(
		 {
		  req_wod_2_dcu_i.hdr,
		  req_wod_2_dcu_i.size,
		  req_wod_2_dcu_i.vc
		  }
		 ),
      .us_sel_i(req_wod_2_dcu_idx_i),
      .us_valid_i(req_wod_2_dcu_i.valid),
      .us_ready_o(req_wod_2_dcu_i.ready),
      // ECI header output routed
      // based on DCU_IDX.
      // There are DS_NUM_DCU_IDX outputs here.
      .ds_data_o(req_wod_2_dcu_hdr_o),
      .ds_valid_o(req_wod_2_dcu_valid_o),
      .ds_ready_i(req_wod_2_dcu_ready_i)
      );

   // Route rsp_wod to DCUs.
   always_comb begin : RSPWOD2DCU_IP_ASSIGN
      rsp_wod_2_dcu_i.hdr	= rsp_wod_p_o.hdr;
      rsp_wod_2_dcu_i.size	= rsp_wod_p_o.size;
      rsp_wod_2_dcu_i.vc	= rsp_wod_p_o.vc;
      rsp_wod_2_dcu_i.valid	= rsp_wod_p_o.valid;
      rsp_wod_2_dcu_idx_i	= rsp_wod_dcu_idx_p_o;
   end : RSPWOD2DCU_IP_ASSIGN
   axis_comb_router #
     (
      .DATA_WIDTH(ECI_HDR_PKT_WIDTH),
      .NUM_OUT(DS_NUM_DCU_IDX)
       )
   rsp_wod_to_dcus_rtr_1
     (
      // ECI header input + DCU IDX.
      .us_data_i(
		 {
		  rsp_wod_2_dcu_i.hdr,
		  rsp_wod_2_dcu_i.size,
		  rsp_wod_2_dcu_i.vc
		  }
		 ),
      .us_sel_i(rsp_wod_2_dcu_idx_i),
      .us_valid_i(rsp_wod_2_dcu_i.valid),
      .us_ready_o(rsp_wod_2_dcu_i.ready),
      // ECI header output routed
      // based on DCU_IDX.
      // There are DS_NUM_DCU_IDX outputs here.
      .ds_data_o(rsp_wod_2_dcu_hdr_o),
      .ds_valid_o(rsp_wod_2_dcu_valid_o),
      .ds_ready_i(rsp_wod_2_dcu_ready_i)
      );

   // Route rsp_wd to DCUs.
   always_comb begin : RSPWD2DCU_IP_ASSIGN
      rsp_wd_2_dcu_i.hdr	= rsp_wd_p_o.hdr;
      rsp_wd_2_dcu_i.size	= rsp_wd_p_o.size;
      rsp_wd_2_dcu_i.vc		= rsp_wd_p_o.vc;
      rsp_wd_2_dcu_i.valid	= rsp_wd_p_o.valid;
      rsp_wd_2_dcu_idx_i	= rsp_wd_dcu_idx_p_o;
   end : RSPWD2DCU_IP_ASSIGN
   axis_comb_router #
     (
      .DATA_WIDTH(ECI_HDR_PKT_WIDTH),
      .NUM_OUT(DS_NUM_DCU_IDX)
       )
   rsp_wd_to_dcus_rtr_1
     (
      // ECI header input + DCU IDX.
      .us_data_i(
		 {
		  rsp_wd_2_dcu_i.hdr,
		  rsp_wd_2_dcu_i.size,
		  rsp_wd_2_dcu_i.vc
		  }
		 ),
      .us_sel_i(rsp_wd_2_dcu_idx_i),
      .us_valid_i(rsp_wd_2_dcu_i.valid),
      .us_ready_o(rsp_wd_2_dcu_i.ready),
      // ECI header output routed
      // based on DCU_IDX.
      // There are DS_NUM_DCU_IDX outputs here.
      .ds_data_o(rsp_wd_2_dcu_hdr_o),
      .ds_valid_o(rsp_wd_2_dcu_valid_o),
      .ds_ready_i(rsp_wd_2_dcu_ready_i)
      );

   // Route lcl_fwd_wod to DCUs.
   always_comb begin : LCL_FWDWOD2DCU_IP_ASSIGN
      lcl_fwd_wod_2_dcu_i.hdr	= lcl_fwd_wod_p_o.hdr;
      lcl_fwd_wod_2_dcu_i.size	= lcl_fwd_wod_p_o.size;
      lcl_fwd_wod_2_dcu_i.vc	= lcl_fwd_wod_p_o.vc;
      lcl_fwd_wod_2_dcu_i.valid	= lcl_fwd_wod_p_o.valid;
      lcl_fwd_wod_2_dcu_idx_i	= lcl_fwd_wod_dcu_idx_p_o;
   end : LCL_FWDWOD2DCU_IP_ASSIGN
   axis_comb_router #
     (
      .DATA_WIDTH(ECI_HDR_PKT_WIDTH),
      .NUM_OUT(DS_NUM_DCU_IDX)
       )
   lcl_fwd_wod_to_dcus_rtr_1
     (
      // ECI header input + DCU IDX.
      .us_data_i(
		 {
		  lcl_fwd_wod_2_dcu_i.hdr,
		  lcl_fwd_wod_2_dcu_i.size,
		  lcl_fwd_wod_2_dcu_i.vc
		  }
		 ),
      .us_sel_i(lcl_fwd_wod_2_dcu_idx_i),
      .us_valid_i(lcl_fwd_wod_2_dcu_i.valid),
      .us_ready_o(lcl_fwd_wod_2_dcu_i.ready),
      // ECI header output routed
      // based on DCU_IDX.
      // There are DS_NUM_DCU_IDX outputs here.
      .ds_data_o(lcl_fwd_wod_2_dcu_hdr_o),
      .ds_valid_o(lcl_fwd_wod_2_dcu_valid_o),
      .ds_ready_i(lcl_fwd_wod_2_dcu_ready_i)
      );

   // Route lcl_rsp_wod to DCUs.
   always_comb begin : LCL_RSPWOD2DCU_IP_ASSIGN
      lcl_rsp_wod_2_dcu_i.hdr	= lcl_rsp_wod_p_o.hdr;
      lcl_rsp_wod_2_dcu_i.size	= lcl_rsp_wod_p_o.size;
      lcl_rsp_wod_2_dcu_i.vc	= lcl_rsp_wod_p_o.vc;
      lcl_rsp_wod_2_dcu_i.valid	= lcl_rsp_wod_p_o.valid;
      lcl_rsp_wod_2_dcu_idx_i	= lcl_rsp_wod_dcu_idx_p_o;
   end : LCL_RSPWOD2DCU_IP_ASSIGN
   axis_comb_router #
     (
      .DATA_WIDTH(ECI_HDR_PKT_WIDTH),
      .NUM_OUT(DS_NUM_DCU_IDX)
       )
   lcl_rsp_wod_to_dcus_rtr_1
     (
      // ECI header input + DCU IDX.
      .us_data_i(
		 {
		  lcl_rsp_wod_2_dcu_i.hdr,
		  lcl_rsp_wod_2_dcu_i.size,
		  lcl_rsp_wod_2_dcu_i.vc
		  }
		 ),
      .us_sel_i(lcl_rsp_wod_2_dcu_idx_i),
      .us_valid_i(lcl_rsp_wod_2_dcu_i.valid),
      .us_ready_o(lcl_rsp_wod_2_dcu_i.ready),
      // ECI header output routed
      // based on DCU_IDX.
      // There are DS_NUM_DCU_IDX outputs here.
      .ds_data_o(lcl_rsp_wod_2_dcu_hdr_o),
      .ds_valid_o(lcl_rsp_wod_2_dcu_valid_o),
      .ds_ready_i(lcl_rsp_wod_2_dcu_ready_i)
      );

   genvar dcu_iter;
   generate
      for( dcu_iter = 0; dcu_iter < DS_NUM_DCU_IDX; dcu_iter++) begin : DCUS
	 always_comb begin : ROUTER_READY_ASSIGN
	    req_wod_2_dcu_ready_i[dcu_iter] = dc_req_wod_i[dcu_iter].ready;
	    rsp_wod_2_dcu_ready_i[dcu_iter] = dc_rsp_wod_i[dcu_iter].ready;
	    rsp_wd_2_dcu_ready_i[dcu_iter] = dc_rsp_wd_hdr_i[dcu_iter].ready;
	    lcl_fwd_wod_2_dcu_ready_i[dcu_iter] = dc_lcl_fwd_wod_i[dcu_iter].ready;
	    lcl_rsp_wod_2_dcu_ready_i[dcu_iter] = dc_lcl_rsp_wod_i[dcu_iter].ready;
	 end : ROUTER_READY_ASSIGN

	 always_comb begin : DCU_INST_IP_ASSIGN
	    { dc_req_wod_i[dcu_iter].hdr,
	      dc_req_wod_i[dcu_iter].size,
	      dc_req_wod_i[dcu_iter].vc } = req_wod_2_dcu_hdr_o[dcu_iter];
	    dc_req_wod_i[dcu_iter].valid = req_wod_2_dcu_valid_o[dcu_iter];

	    { dc_rsp_wod_i[dcu_iter].hdr,
	      dc_rsp_wod_i[dcu_iter].size,
	      dc_rsp_wod_i[dcu_iter].vc } = rsp_wod_2_dcu_hdr_o[dcu_iter];
	    dc_rsp_wod_i[dcu_iter].valid = rsp_wod_2_dcu_valid_o[dcu_iter];

	    { dc_rsp_wd_hdr_i[dcu_iter].hdr,
	      dc_rsp_wd_hdr_i[dcu_iter].size,
	      dc_rsp_wd_hdr_i[dcu_iter].vc } = rsp_wd_2_dcu_hdr_o[dcu_iter];
	    dc_rsp_wd_hdr_i[dcu_iter].valid = rsp_wd_2_dcu_valid_o[dcu_iter];

	    { dc_lcl_fwd_wod_i[dcu_iter].hdr,
	      dc_lcl_fwd_wod_i[dcu_iter].size,
	      dc_lcl_fwd_wod_i[dcu_iter].vc } = lcl_fwd_wod_2_dcu_hdr_o[dcu_iter];
	    dc_lcl_fwd_wod_i[dcu_iter].valid = lcl_fwd_wod_2_dcu_valid_o[dcu_iter];

	    { dc_lcl_rsp_wod_i[dcu_iter].hdr,
	      dc_lcl_rsp_wod_i[dcu_iter].size,
	      dc_lcl_rsp_wod_i[dcu_iter].vc } = lcl_rsp_wod_2_dcu_hdr_o[dcu_iter];
	    dc_lcl_rsp_wod_i[dcu_iter].valid = lcl_rsp_wod_2_dcu_valid_o[dcu_iter];

	    dc_eci_rsp_hdr_o[dcu_iter].ready	= eci_rsp_arb_ready_o[dcu_iter];

	    dc_rd_req_ctrl_o[dcu_iter].ready	= rd_req_arb_ready_o[dcu_iter];
	    dc_rd_rsp_ctrl_i[dcu_iter].id	= rd_rsp_rtr_data_o[dcu_iter];
	    dc_rd_rsp_ctrl_i[dcu_iter].valid	= rd_rsp_rtr_valid_o[dcu_iter];

	    dc_wr_req_ctrl_o[dcu_iter].ready	= wr_req_arb_ready_o[dcu_iter];
	    {dc_wr_rsp_ctrl_i[dcu_iter].id,
	     dc_wr_rsp_ctrl_i[dcu_iter].bresp}  = wr_rsp_rtr_data_o[dcu_iter];
	    dc_wr_rsp_ctrl_i[dcu_iter].valid	= wr_rsp_rtr_valid_o[dcu_iter];
	 end : DCU_INST_IP_ASSIGN
	 dcu_top #
	   (
	    .PERF_REGS_WIDTH(PERF_REGS_WIDTH),
	    .SYNTH_PERF_REGS(SYNTH_PERF_REGS)
	     )
	 dcu_inst
	   (
	    .clk(clk),
	    .reset(reset),
	    // Input ECI events.
	    // ECI packet for request without data. (VC 6 or 7) (only header).
	    .req_wod_hdr_i		(dc_req_wod_i[dcu_iter].hdr),
	    .req_wod_pkt_size_i		(dc_req_wod_i[dcu_iter].size),
	    .req_wod_pkt_vc_i		(dc_req_wod_i[dcu_iter].vc),
	    .req_wod_pkt_valid_i	(dc_req_wod_i[dcu_iter].valid),
	    .req_wod_pkt_ready_o	(dc_req_wod_i[dcu_iter].ready),
	    // ECI packet for response without data.(VC 10 or 11). (only header).
	    .rsp_wod_hdr_i		(dc_rsp_wod_i[dcu_iter].hdr),
	    .rsp_wod_pkt_size_i		(dc_rsp_wod_i[dcu_iter].size),
	    .rsp_wod_pkt_vc_i		(dc_rsp_wod_i[dcu_iter].vc),
	    .rsp_wod_pkt_valid_i	(dc_rsp_wod_i[dcu_iter].valid),
	    .rsp_wod_pkt_ready_o	(dc_rsp_wod_i[dcu_iter].ready),
	    // ECI packet for response with data. (VC 4 or 5). (only header).
	    .rsp_wd_hdr_i	(dc_rsp_wd_hdr_i[dcu_iter].hdr),
	    .rsp_wd_pkt_size_i	(dc_rsp_wd_hdr_i[dcu_iter].size),
	    .rsp_wd_pkt_vc_i	(dc_rsp_wd_hdr_i[dcu_iter].vc),
	    .rsp_wd_pkt_valid_i	(dc_rsp_wd_hdr_i[dcu_iter].valid),
	    .rsp_wd_pkt_ready_o	(dc_rsp_wd_hdr_i[dcu_iter].ready),
	    // LCL packet for clean, clean invalidates (VC 16,17)
	    .lcl_fwd_wod_hdr_i		(dc_lcl_fwd_wod_i[dcu_iter].hdr),
	    .lcl_fwd_wod_pkt_size_i	(dc_lcl_fwd_wod_i[dcu_iter].size),
	    .lcl_fwd_wod_pkt_vc_i	(dc_lcl_fwd_wod_i[dcu_iter].vc),
	    .lcl_fwd_wod_pkt_valid_i	(dc_lcl_fwd_wod_i[dcu_iter].valid),
	    .lcl_fwd_wod_pkt_ready_o	(dc_lcl_fwd_wod_i[dcu_iter].ready),
	    // LCL packet for response without data (unlocks) (VC 18,19)
	    .lcl_rsp_wod_hdr_i		(dc_lcl_rsp_wod_i[dcu_iter].hdr),
	    .lcl_rsp_wod_pkt_size_i	(dc_lcl_rsp_wod_i[dcu_iter].size),
	    .lcl_rsp_wod_pkt_vc_i	(dc_lcl_rsp_wod_i[dcu_iter].vc),
	    .lcl_rsp_wod_pkt_valid_i	(dc_lcl_rsp_wod_i[dcu_iter].valid),
	    .lcl_rsp_wod_pkt_ready_o	(dc_lcl_rsp_wod_i[dcu_iter].ready),
	    // Output ECI events, only header.
	    // VC # indicates VC to route to.
	    .eci_rsp_hdr_o	(dc_eci_rsp_hdr_o[dcu_iter].hdr),
	    .eci_rsp_size_o	(dc_eci_rsp_hdr_o[dcu_iter].size),
	    .eci_rsp_vc_o	(dc_eci_rsp_hdr_o[dcu_iter].vc),
	    .eci_rsp_valid_o	(dc_eci_rsp_hdr_o[dcu_iter].valid),
	    .eci_rsp_ready_i	(dc_eci_rsp_hdr_o[dcu_iter].ready),
	    // Output Read descriptors
	    // Read descriptors: Request and response.
	    // no data, only descriptor.
	    .rd_req_id_o	(dc_rd_req_ctrl_o[dcu_iter].id),
	    .rd_req_addr_o	(dc_rd_req_ctrl_o[dcu_iter].addr),
	    .rd_req_valid_o	(dc_rd_req_ctrl_o[dcu_iter].valid),
	    .rd_req_ready_i	(dc_rd_req_ctrl_o[dcu_iter].ready),
	    .rd_rsp_id_i	(dc_rd_rsp_ctrl_i[dcu_iter].id),
	    .rd_rsp_valid_i	(dc_rd_rsp_ctrl_i[dcu_iter].valid),
	    .rd_rsp_ready_o	(dc_rd_rsp_ctrl_i[dcu_iter].ready),
	    // Write descriptors: Request and response.
	    // no data, only descriptor.
	    .wr_req_id_o	(dc_wr_req_ctrl_o[dcu_iter].id),
	    .wr_req_addr_o	(dc_wr_req_ctrl_o[dcu_iter].addr),
	    .wr_req_strb_o	(dc_wr_req_ctrl_o[dcu_iter].strb),
	    .wr_req_valid_o	(dc_wr_req_ctrl_o[dcu_iter].valid),
	    .wr_req_ready_i	(dc_wr_req_ctrl_o[dcu_iter].ready),
	    .wr_rsp_id_i	(dc_wr_rsp_ctrl_i[dcu_iter].id),
	    .wr_rsp_bresp_i	(dc_wr_rsp_ctrl_i[dcu_iter].bresp),
	    .wr_rsp_valid_i	(dc_wr_rsp_ctrl_i[dcu_iter].valid),
	    .wr_rsp_ready_o	(dc_wr_rsp_ctrl_i[dcu_iter].ready),
	    // Performance Counters.
	    .prf_no_req_pkt_rdy_o	 (dc_perf_o[dcu_iter].no_req_pkt_rdy),
	    .prf_no_req_pkt_stl_o	 (dc_perf_o[dcu_iter].no_req_pkt_stl),
	    .prf_no_rsp_pkt_sent_o	 (dc_perf_o[dcu_iter].no_rsp_pkt_sent),
	    .prf_no_rd_req_o		 (dc_perf_o[dcu_iter].no_rd_req),
	    .prf_no_rd_rsp_o		 (dc_perf_o[dcu_iter].no_rd_rsp),
	    .prf_no_wr_req_o		 (dc_perf_o[dcu_iter].no_wr_req),
	    .prf_no_wr_rsp_o		 (dc_perf_o[dcu_iter].no_wr_rsp),
	    .prf_no_cyc_bw_req_vld_rdy_o (dc_perf_o[dcu_iter].no_cyc_bw_req_vld_rdy),
	    // Debug interface.
	    .dbg_eci_req_hdr_o		(dc_dbg_o[dcu_iter].eci_req_hdr),
	    .dbg_eci_req_vc_o		(dc_dbg_o[dcu_iter].eci_req_vc),
	    .dbg_eci_req_size_o		(dc_dbg_o[dcu_iter].eci_req_size),
	    .dbg_eci_req_ready_o	(dc_dbg_o[dcu_iter].eci_req_ready),
	    .dbg_eci_req_stalled_o	(dc_dbg_o[dcu_iter].eci_req_stalled),
	    .dbg_eci_rsp_hdr_o		(dc_dbg_o[dcu_iter].eci_rsp_hdr),
	    .dbg_eci_rsp_vc_o		(dc_dbg_o[dcu_iter].eci_rsp_vc),
	    .dbg_eci_rsp_size_o		(dc_dbg_o[dcu_iter].eci_rsp_size),
	    .dbg_eci_rsp_valid_o	(dc_dbg_o[dcu_iter].eci_rsp_valid),
	    .dbg_req_cc_enc_o		(dc_dbg_o[dcu_iter].req_cc_enc),
	    .dbg_rdda_o			(dc_dbg_o[dcu_iter].rdda),
	    .dbg_wdda_o			(dc_dbg_o[dcu_iter].wdda),
	    .dbg_stall_prot_o		(dc_dbg_o[dcu_iter].stall_prot_o),
	    .dbg_stall_rd_busy_o	(dc_dbg_o[dcu_iter].stall_rd_busy_o),
	    .dbg_stall_wr_busy_o	(dc_dbg_o[dcu_iter].stall_wr_busy_o),
	    .dbg_stall_eci_tr_busy_o    (dc_dbg_o[dcu_iter].stall_eci_tr_busy_o),
	    .dbg_stall_rtg_full_o	(dc_dbg_o[dcu_iter].stall_rtg_full_o),
	    .dbg_stall_wait_op_hs_o     (dc_dbg_o[dcu_iter].stall_wait_op_hs_o),
	    .dbg_cc_active_req_o	(dc_dbg_o[dcu_iter].cc_active_req),
	    .dbg_cc_present_state_o	(dc_dbg_o[dcu_iter].cc_present_state),
	    .dbg_cc_next_state_o	(dc_dbg_o[dcu_iter].cc_next_state),
	    .dbg_cc_next_action_o	(dc_dbg_o[dcu_iter].cc_next_action),
	    .dbg_err_code_o		(dc_dbg_o[dcu_iter].err_code),
	    .dbg_error_o		(dc_dbg_o[dcu_iter].error),
	    .dbg_valid_o		(dc_dbg_o[dcu_iter].valid)
	    );

	 always_comb begin : ECIR_RD_WR_ARB_ASSIGN
	    // ECI resposne from DCU to arb.
	    eci_rsp_arb_data_i[dcu_iter] = {
					    dc_eci_rsp_hdr_o[dcu_iter].hdr,
					    dc_eci_rsp_hdr_o[dcu_iter].size,
					    dc_eci_rsp_hdr_o[dcu_iter].vc
					    };
	    eci_rsp_arb_valid_i[dcu_iter] = dc_eci_rsp_hdr_o[dcu_iter].valid;


	    // Read req DCU to arb.
	    rd_req_arb_id_i[dcu_iter]    = dc_rd_req_ctrl_o[dcu_iter].id;
	    rd_req_arb_data_i[dcu_iter]  = dc_rd_req_ctrl_o[dcu_iter].addr;
	    rd_req_arb_valid_i[dcu_iter] = dc_rd_req_ctrl_o[dcu_iter].valid;

	    // Write req DCU to arb.
	    wr_req_arb_id_i[dcu_iter]    = dc_wr_req_ctrl_o[dcu_iter].id;
	    wr_req_arb_data_i[dcu_iter]  = {
					    dc_wr_req_ctrl_o[dcu_iter].addr,
					    dc_wr_req_ctrl_o[dcu_iter].strb
					    };
	    wr_req_arb_valid_i[dcu_iter] = dc_wr_req_ctrl_o[dcu_iter].valid;

	    // Read rsp RTR to DCU.
	    rd_rsp_rtr_ready_i[dcu_iter] = dc_rd_rsp_ctrl_i[dcu_iter].ready;

	    // Wr rsp rtr to DCU.
	    wr_rsp_rtr_ready_i[dcu_iter] = dc_wr_rsp_ctrl_i[dcu_iter].ready;

        dc_trace_l0_valid_o[dcu_iter] = dc_dbg_o[dcu_iter].valid;
        dc_trace_l0_o[dcu_iter].error = dc_dbg_o[dcu_iter].error;
        dc_trace_l0_o[dcu_iter].cli = {(dc_dbg_o[dcu_iter].eci_req_ready ? dc_dbg_o[dcu_iter].eci_req_hdr[39:7] : dc_dbg_o[dcu_iter].eci_rsp_hdr[39:7]), 7'b0000000};
        dc_trace_l0_o[dcu_iter].state = dc_dbg_o[dcu_iter].cc_next_state;
        dc_trace_l0_o[dcu_iter].action = dc_dbg_o[dcu_iter].cc_next_action;
        dc_trace_l0_o[dcu_iter].request = dc_dbg_o[dcu_iter].cc_active_req;
        dc_trace_l0_o[dcu_iter].padding = 9'bxxxxxxxxx;
	 end : ECIR_RD_WR_ARB_ASSIGN
        // Second buffer (the first one is in the muxliplexer), so we can store up to 4 values
        eci_channel_buffer inst_dcu_trace_input_buffer
        (
            clk,
            {{64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx}, dc_trace_l0_o[dcu_iter],
            3'b000, 4'bxxxx, dc_trace_l0_valid_o[dcu_iter]},
            dc_trace_l0_ready_o[dcu_iter],
            {{dc_trace_l1_data_o[dcu_iter][7], dc_trace_l1_data_o[dcu_iter][6], dc_trace_l1_data_o[dcu_iter][5], dc_trace_l1_data_o[dcu_iter][4],
              dc_trace_l1_data_o[dcu_iter][3], dc_trace_l1_data_o[dcu_iter][2], dc_trace_l1_data_o[dcu_iter][1], dc_trace_l1_data_o[dcu_iter][0],
              dc_trace_l1_o[dcu_iter]},
              dc_trace_l1_size_o[dcu_iter], dc_trace_l1_vc_no_o[dcu_iter], dc_trace_l1_valid_o[dcu_iter]},
            dc_trace_l1_ready_o[dcu_iter]
        );
      end : DCUS
   endgenerate

    generate
        // one muxer per 16 DCUs
        for( dcu_iter = 0; dcu_iter < 2; dcu_iter++) begin : TRACE_MUX_L1
                eci_channel_muxer #(16,1,0) inst_dcu_trace_mux_l1
                (
                    clk,
                    {{64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, dc_trace_l1_o[16*dcu_iter+15],
                    3'b000, 4'bxxxx, dc_trace_l1_valid_o[16*dcu_iter+15]},
                    {64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, dc_trace_l1_o[16*dcu_iter+14],
                    3'b000, 4'bxxxx, dc_trace_l1_valid_o[16*dcu_iter+14]},
                    {64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, dc_trace_l1_o[16*dcu_iter+13],
                    3'b000, 4'bxxxx, dc_trace_l1_valid_o[16*dcu_iter+13]},
                    {64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, dc_trace_l1_o[16*dcu_iter+12],
                    3'b000, 4'bxxxx, dc_trace_l1_valid_o[16*dcu_iter+12]},
                    {64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, dc_trace_l1_o[16*dcu_iter+11],
                    3'b000, 4'bxxxx, dc_trace_l1_valid_o[16*dcu_iter+11]},
                    {64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, dc_trace_l1_o[16*dcu_iter+10],
                    3'b000, 4'bxxxx, dc_trace_l1_valid_o[16*dcu_iter+10]},
                    {64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, dc_trace_l1_o[16*dcu_iter+9],
                    3'b000, 4'bxxxx, dc_trace_l1_valid_o[16*dcu_iter+9]},
                    {64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, dc_trace_l1_o[16*dcu_iter+8],
                    3'b000, 4'bxxxx, dc_trace_l1_valid_o[16*dcu_iter+8]},
                    {64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, dc_trace_l1_o[16*dcu_iter+7],
                    3'b000, 4'bxxxx, dc_trace_l1_valid_o[16*dcu_iter+7]},
                    {64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, dc_trace_l1_o[16*dcu_iter+6],
                    3'b000, 4'bxxxx, dc_trace_l1_valid_o[16*dcu_iter+6]},
                    {64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, dc_trace_l1_o[16*dcu_iter+5],
                    3'b000, 4'bxxxx, dc_trace_l1_valid_o[16*dcu_iter+5]},
                    {64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, dc_trace_l1_o[16*dcu_iter+4],
                    3'b000, 4'bxxxx, dc_trace_l1_valid_o[16*dcu_iter+4]},
                    {64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, dc_trace_l1_o[16*dcu_iter+3],
                    3'b000, 4'bxxxx, dc_trace_l1_valid_o[16*dcu_iter+3]},
                    {64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, dc_trace_l1_o[16*dcu_iter+2],
                    3'b000, 4'bxxxx, dc_trace_l1_valid_o[16*dcu_iter+2]},
                    {64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, dc_trace_l1_o[16*dcu_iter+1],
                    3'b000, 4'bxxxx, dc_trace_l1_valid_o[16*dcu_iter+1]},
                    {64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, 64'hxxxxxxxxxxxxxxxx, dc_trace_l1_o[16*dcu_iter],
                    3'b000, 4'bxxxx, dc_trace_l1_valid_o[16*dcu_iter]}},
                    {dc_trace_l1_ready_o[16*dcu_iter+15], dc_trace_l1_ready_o[16*dcu_iter+14], dc_trace_l1_ready_o[16*dcu_iter+13], dc_trace_l1_ready_o[16*dcu_iter+12],
                     dc_trace_l1_ready_o[16*dcu_iter+11], dc_trace_l1_ready_o[16*dcu_iter+10], dc_trace_l1_ready_o[16*dcu_iter+9], dc_trace_l1_ready_o[16*dcu_iter+8],
                     dc_trace_l1_ready_o[16*dcu_iter+7], dc_trace_l1_ready_o[16*dcu_iter+6], dc_trace_l1_ready_o[16*dcu_iter+5], dc_trace_l1_ready_o[16*dcu_iter+4],
                     dc_trace_l1_ready_o[16*dcu_iter+3], dc_trace_l1_ready_o[16*dcu_iter+2], dc_trace_l1_ready_o[16*dcu_iter+1], dc_trace_l1_ready_o[16*dcu_iter]},
                    {{dc_trace_l2_data_o[dcu_iter][7], dc_trace_l2_data_o[dcu_iter][6], dc_trace_l2_data_o[dcu_iter][5], dc_trace_l2_data_o[dcu_iter][4],
                      dc_trace_l2_data_o[dcu_iter][3], dc_trace_l2_data_o[dcu_iter][2], dc_trace_l2_data_o[dcu_iter][1], dc_trace_l2_data_o[dcu_iter][0],
                      dc_trace_l2_o[dcu_iter]},
                      dc_trace_l2_size_o[dcu_iter], dc_trace_l2_vc_no_o[dcu_iter], dc_trace_l2_valid_o[dcu_iter]},
                    1'b1
                );
            always_comb begin
                tracing_valid[dcu_iter] = dc_trace_l2_valid_o[dcu_iter];
                tracing_error[dcu_iter] = dc_trace_l2_o[dcu_iter].error;
                tracing_cli[dcu_iter] = dc_trace_l2_o[dcu_iter].cli;
                tracing_state[dcu_iter] = dc_trace_l2_o[dcu_iter].state;
                tracing_action[dcu_iter] = dc_trace_l2_o[dcu_iter].action;
                tracing_request[dcu_iter] = dc_trace_l2_o[dcu_iter].request;
            end
        end : TRACE_MUX_L1
    endgenerate

   // Select 1 ECI rsp from DS_NUM_DCU_IDX DCUs.
   // ID is not used.
   // Data is ECI hdr, size vc
   always_comb begin : DCU_ECI_RSP_ARB_IP_ASSIGN
      eci_rsp_arb_ready_i = dv_ecih_i.ready;
   end : DCU_ECI_RSP_ARB_IP_ASSIGN
   dcs_rr_arb #
     (
      .ID_WIDTH(1), //not used.
      .DATA_WIDTH(ECI_HDR_PKT_WIDTH)
      //.NUM_IN(DS_NUM_DCU_IDX)
       )
   dcu_eci_rsp_rr_arb1
     (
      .clk(clk),
      .reset(reset),
      // DS_NUM_DCU_IDX ins.
      .us_id_i('0), // Not used.
      .us_data_i(eci_rsp_arb_data_i),
      .us_valid_i(eci_rsp_arb_valid_i),
      .us_ready_o(eci_rsp_arb_ready_o),
      // one out.
      .ds_id_o(), // Not connected.
      .ds_data_o(eci_rsp_arb_data_o),
      .ds_valid_o(eci_rsp_arb_valid_o),
      .ds_ready_i(eci_rsp_arb_ready_i)
      );

   // DCS to VC channels.
   // ECI header response from DCS is transmitted
   // towards TX VC channels.
   always_comb begin : DCS_VC_RTR_IP_ASSIGN
      {dv_ecih_i.hdr,
       dv_ecih_i.size,
       dv_ecih_i.vc}		= eci_rsp_arb_data_o;
      dv_ecih_i.valid		= eci_rsp_arb_valid_o;
      dv_rsp_wod_o.ready	= rsp_wod_pkt_ready_i;
      dv_rsp_wd_hdr_o.ready	= rsp_wd_pkt_ready_i;
      dv_fwd_wod_o.ready	= fwd_wod_pkt_ready_i;
      dv_lcl_rsp_wod_o.ready	= lcl_rsp_wod_pkt_ready_i;
   end : DCS_VC_RTR_IP_ASSIGN
   dc_to_vc_router
     dcs_vc_rtr1
       (
	.clk				(clk),
	.reset				(reset),
	// Input ECI header + VC number.
	.ecih_hdr_i			(dv_ecih_i.hdr),
	.ecih_pkt_size_i		(dv_ecih_i.size),
	.ecih_pkt_vc_i			(dv_ecih_i.vc),
	.ecih_pkt_valid_i		(dv_ecih_i.valid),
	.ecih_pkt_ready_o		(dv_ecih_i.ready),
	// Output rsp_wod channel.
	.rsp_wod_hdr_o			(dv_rsp_wod_o.hdr),
	.rsp_wod_pkt_size_o		(dv_rsp_wod_o.size),
	.rsp_wod_pkt_vc_o		(dv_rsp_wod_o.vc),
	.rsp_wod_pkt_valid_o		(dv_rsp_wod_o.valid),
	.rsp_wod_pkt_ready_i		(dv_rsp_wod_o.ready),
	// Output rsp_wd channel.
	.rsp_wd_hdr_o			(dv_rsp_wd_hdr_o.hdr),
	.rsp_wd_pkt_size_o		(dv_rsp_wd_hdr_o.size),
	.rsp_wd_pkt_vc_o		(dv_rsp_wd_hdr_o.vc),
	.rsp_wd_pkt_valid_o		(dv_rsp_wd_hdr_o.valid),
	.rsp_wd_pkt_ready_i		(dv_rsp_wd_hdr_o.ready),
	// Output fwd_wod channel.
	.fwd_wod_hdr_o			(dv_fwd_wod_o.hdr),
	.fwd_wod_pkt_size_o		(dv_fwd_wod_o.size),
	.fwd_wod_pkt_vc_o		(dv_fwd_wod_o.vc),
	.fwd_wod_pkt_valid_o		(dv_fwd_wod_o.valid),
	.fwd_wod_pkt_ready_i		(dv_fwd_wod_o.ready),
	// output lcl_rsp_wod channel.
	.lcl_rsp_wod_hdr_o		(dv_lcl_rsp_wod_o.hdr),
	.lcl_rsp_wod_pkt_size_o		(dv_lcl_rsp_wod_o.size),
	.lcl_rsp_wod_pkt_vc_o		(dv_lcl_rsp_wod_o.vc),
	.lcl_rsp_wod_pkt_valid_o	(dv_lcl_rsp_wod_o.valid),
	.lcl_rsp_wod_pkt_ready_i	(dv_lcl_rsp_wod_o.ready)
	);

   // Select 1 read request from
   // DS_NUM_DCU_IDX DCUs.
   // ID is rd req ID.
   // Data is rd addr.
   always_comb begin : RD_REQ_ARB_IP_ASSIGN
      rd_req_arb_ready_i = rd_req_p_ready_o;
   end : RD_REQ_ARB_IP_ASSIGN
   dcs_rr_arb #
     (
      .ID_WIDTH(MAX_DCU_ID_WIDTH),
      .DATA_WIDTH(DS_ADDR_WIDTH)
      //.NUM_IN(DS_NUM_DCU_IDX)
       )
   dcu_rd_req_pri_arb1
     (
      .clk(clk),
      .reset(reset),
      // DS_NUM_DCU_IDX ins.
      .us_id_i(rd_req_arb_id_i),
      .us_data_i(rd_req_arb_data_i),
      .us_valid_i(rd_req_arb_valid_i),
      .us_ready_o(rd_req_arb_ready_o),
      // one out.
      .ds_id_o(rd_req_arb_id_o),
      .ds_data_o(rd_req_arb_data_o),
      .ds_valid_o(rd_req_arb_valid_o),
      .ds_ready_i(rd_req_arb_ready_i)
      );

   // pass arbitrated rd req through pipeline stage.
   // ID + address has to be passed.
   // Output of this pipeline stage is connected
   // to the read request output of this module.
   always_comb begin : RD_REQ_PIPE_IP_ASSIGN
      rd_req_p_i = {rd_req_arb_id_o, rd_req_arb_data_o};
      rd_req_p_valid_i = rd_req_arb_valid_o;
      rd_req_p_ready_i = rd_req_ready_i;
   end : RD_REQ_PIPE_IP_ASSIGN
   axis_pipeline_stage #
     (
      .DATA_WIDTH(RD_ID_ADDR_WIDTH)
       )
   rd_req_arb_pipe1
     (
      .clk(clk),
      .reset(reset),
      .us_data(rd_req_p_i),
      .us_valid(rd_req_p_valid_i),
      .us_ready(rd_req_p_ready_o),
      .ds_data(rd_req_p_o),
      .ds_valid(rd_req_p_valid_o),
      .ds_ready(rd_req_p_ready_i)
      );

   // Select 1 write request from
   // DS_NUM_DCU_IDX DCUs.
   // ID is wr req ID.
   // Data is wr addr and strobe.
   always_comb begin : WR_REQ_ARB_IP_ASSIGN
      wr_req_arb_ready_i = wr_req_p_ready_o;
   end : WR_REQ_ARB_IP_ASSIGN
   dcs_rr_arb #
     (
      .ID_WIDTH(MAX_DCU_ID_WIDTH),
      .DATA_WIDTH(WR_ADDR_STRB_WIDTH)
      //.NUM_IN(DS_NUM_DCU_IDX)
       )
   dcu_wr_req_pri_arb1
     (
      .clk(clk),
      .reset(reset),
      // DS_NUM_DCU_IDX ins.
      .us_id_i(wr_req_arb_id_i),
      .us_data_i(wr_req_arb_data_i),
      .us_valid_i(wr_req_arb_valid_i),
      .us_ready_o(wr_req_arb_ready_o),
      // one out.
      .ds_id_o(wr_req_arb_id_o),
      .ds_data_o(wr_req_arb_data_o),
      .ds_valid_o(wr_req_arb_valid_o),
      .ds_ready_i(wr_req_arb_ready_i)
      );

   // pass arbitrated wr req through pipeline stage.
   // ID + address has to be passed.
   // Output of pipeline stage is connected to write
   // request output of this module.
   always_comb begin : WR_REQ_PIPE_IP_ASSIGN
      wr_req_p_i = {wr_req_arb_id_o, wr_req_arb_data_o};
      wr_req_p_valid_i = wr_req_arb_valid_o;
      wr_req_p_ready_i = wr_req_ready_i;
   end : WR_REQ_PIPE_IP_ASSIGN
   axis_pipeline_stage #
     (
      .DATA_WIDTH(WR_ID_ADDR_STRB_WIDTH)
       )
   wr_req_arb_pipe1
     (
      .clk(clk),
      .reset(reset),
      .us_data(wr_req_p_i),
      .us_valid(wr_req_p_valid_i),
      .us_ready(wr_req_p_ready_o),
      .ds_data(wr_req_p_o),
      .ds_valid(wr_req_p_valid_o),
      .ds_ready(wr_req_p_ready_i)
      );

   // Read response control input to this module is
   // to be routed to appropriate DCU.
   // Read response ID is only signal to be routed
   // to DS_NUM_DCU_IDX DCUs.
   always_comb begin : RD_RSP_RTR_IP_ASSIGN
      rd_rsp_rtr_data_i = rd_rsp_id_i;
      rd_rsp_rtr_dcu_id = f_maxdcuid_2_dcu_id(.max_dcu_id_i(rd_rsp_id_i));
      rd_rsp_rtr_dcu_idx = f_dcuid_2_dcuidx(.dcu_id_i(rd_rsp_rtr_dcu_id));
      rd_rsp_rtr_sel_i = rd_rsp_rtr_dcu_idx;
      rd_rsp_rtr_valid_i = rd_rsp_valid_i;
   end : RD_RSP_RTR_IP_ASSIGN
   axis_comb_router #
     (
      .DATA_WIDTH(MAX_DCU_ID_WIDTH),
      .NUM_OUT(DS_NUM_DCU_IDX)
       )
   rd_rsp_2_dcu_rtr
     (
      .us_data_i(rd_rsp_rtr_data_i),
      .us_sel_i(rd_rsp_rtr_sel_i),
      .us_valid_i(rd_rsp_rtr_valid_i),
      .us_ready_o(rd_rsp_rtr_ready_o),
      .ds_data_o(rd_rsp_rtr_data_o),
      .ds_valid_o(rd_rsp_rtr_valid_o),
      .ds_ready_i(rd_rsp_rtr_ready_i)
      );

   // Write response control signals input to this
   // module has to be routed to DS_NUM_DCU_IDX DCUs.
   // write rsp ID and BRESP has to be routed.
   always_comb begin : WR_RSP_RTR_IP_ASSIGN
      wr_rsp_rtr_data_i = {wr_rsp_id_i, wr_rsp_bresp_i};
      wr_rsp_rtr_dcu_id = f_maxdcuid_2_dcu_id(.max_dcu_id_i(wr_rsp_id_i));
      wr_rsp_rtr_dcu_idx = f_dcuid_2_dcuidx(.dcu_id_i(wr_rsp_rtr_dcu_id));
      wr_rsp_rtr_sel_i = wr_rsp_rtr_dcu_idx;
      wr_rsp_rtr_valid_i = wr_rsp_valid_i;
   end : WR_RSP_RTR_IP_ASSIGN

   axis_comb_router #
     (
      .DATA_WIDTH(WR_ID_BRESP_WIDTH),
      .NUM_OUT(DS_NUM_DCU_IDX)
       )
   wr_rsp_2_dcu_rtr
     (
      .us_data_i(wr_rsp_rtr_data_i),
      .us_sel_i(wr_rsp_rtr_sel_i),
      .us_valid_i(wr_rsp_rtr_valid_i),
      .us_ready_o(wr_rsp_rtr_ready_o),
      .ds_data_o(wr_rsp_rtr_data_o),
      .ds_valid_o(wr_rsp_rtr_valid_o),
      .ds_ready_i(wr_rsp_rtr_ready_i)
      );
endmodule // dcs_dcus

`endif
