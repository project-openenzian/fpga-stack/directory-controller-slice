
module rd_trmgrTb();

   parameter ID_WIDTH = 7;
   parameter META_DATA_WIDTH = 64;
   parameter ADDR_WIDTH = 38;

   //input output ports 
   //Input signals
   logic 		       clk;
   logic 		       reset;
   logic [ID_WIDTH-1:0]        desc_id_i;
   logic [META_DATA_WIDTH-1:0] desc_meta_i;
   logic [ADDR_WIDTH-1:0]      desc_addr_i;
   logic 		       desc_valid_i;
   logic 		       req_ready_i;
   logic [ID_WIDTH-1:0]        rsp_id_i;
   logic 		       rsp_valid_i;
   logic 		       desc_ready_i;

   //Output signals
   logic 		       desc_ready_o;
   logic [ID_WIDTH-1:0]        req_id_o;
   logic [ADDR_WIDTH-1:0]      req_addr_o;
   logic 		       req_valid_o;
   logic 		       rsp_ready_o;
   logic [ID_WIDTH-1:0]        desc_id_o;
   logic [META_DATA_WIDTH-1:0] desc_meta_o;
   logic 		       desc_valid_o;


   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   rd_trmgr #(
	      .ID_WIDTH(ID_WIDTH),
	      .META_DATA_WIDTH(META_DATA_WIDTH),
	      .ADDR_WIDTH(ADDR_WIDTH)
	      )
   rd_trmgr1 (
	      .clk(clk),
	      .reset(reset),
	      .desc_id_i(desc_id_i),
	      .desc_meta_i(desc_meta_i),
	      .desc_addr_i(desc_addr_i),
	      .desc_valid_i(desc_valid_i),
	      .desc_ready_o(desc_ready_o),
      
	      .req_id_o(req_id_o),
	      .req_addr_o(req_addr_o),
	      .req_valid_o(req_valid_o),
	      .req_ready_i(req_ready_i),
      
	      .rsp_id_i(rsp_id_i),
	      .rsp_valid_i(rsp_valid_i),
	      .rsp_ready_o(rsp_ready_o),
      
	      .desc_id_o(desc_id_o),
	      .desc_meta_o(desc_meta_o),
	      .desc_valid_o(desc_valid_o),
	      .desc_ready_i(desc_ready_i)
	      );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports 
      clk = '0;
      reset = '1;
      desc_id_i = '0;
      desc_meta_i = '0;
      desc_addr_i = '0;
      desc_valid_i = '0;
      req_ready_i = '0;
      rsp_id_i = '0;
      rsp_valid_i = '0;
      desc_ready_i = '0;

      ##5;
      reset = 1'b0;

      ##5;
      
      // Send descriptor
      desc_id_i = 'd10;
      desc_meta_i = 'd100;
      desc_addr_i = 'd45;
      desc_valid_i = 1'b1;
      desc_ready_i = 1'b1;
      req_ready_i = 1'b1;
      wait(desc_valid_i & desc_ready_o);
      #1;
      
      assert(req_id_o == desc_id_i) else $error("Req ID does not match");
      assert(req_addr_o == desc_addr_i) else $error("Req addr does not match.");
      // Should be in req accept mode.
      // should not be ready to accept response yet. 
      assert(rsp_ready_o == 1'b0) else $error("Should not be ready to accept response yet.");
      assert(desc_valid_o == 1'b0) else $error("Should not be issuing any new responses yet.");
      ##1;
      desc_valid_i = 1'b0;

      ##5;
      // should be in rsp_accept mode.
      // should not be ready to receive new descriptor requests.
      assert(desc_ready_o == 1'b0) else $error("Error: Should not be ready to accept new input descriptors");
      assert(req_valid_o == 1'b0) else $error("Error: Should not be issuing any read requests here.");

      // send read response.
      rsp_id_i = desc_id_i;
      rsp_valid_i = 1'b1;
      wait(rsp_valid_i & rsp_ready_o);
      #1;
      
      assert(desc_id_o == desc_id_i) else $error("Error: DescID in o/p does not match i/p(instance %m)");
      assert(desc_meta_o == desc_meta_i) else $error("Error: Desc Meta in o/p does not match i/p");
      ##1;
      rsp_valid_i = 1'b0;
      
      #500 $finish;
      $display("All tests completed, look for error messages");
      
   end 

endmodule
