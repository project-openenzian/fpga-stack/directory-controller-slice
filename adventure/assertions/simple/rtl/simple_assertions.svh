property dcu_id_check;
   @(posedge clk) disable iff (reset)
   wr_en_i |-> dcu_id_i == DCU_ID;
endproperty // dcu_id_check
prop_dcu_id_check: assert property (dcu_id_check);
