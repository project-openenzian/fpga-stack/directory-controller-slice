
module axis_tree_routerTb();

   parameter DATA_WIDTH = 64;
   parameter NUM_OUT = 4;
   parameter SEL_WIDTH = $clog2(NUM_OUT);

   //input output ports 
   //Input signals
   logic 			       clk;
   logic 			       reset;
   logic [DATA_WIDTH-1:0] 	       us_data_i;
   logic [SEL_WIDTH-1:0] 	       us_sel_ch_i;
   logic 			       us_valid_i;
   logic [NUM_OUT-1:0] 		       ds_ready_i;

   //Output signals
   logic 			       us_ready_o;
   logic [NUM_OUT-1:0][DATA_WIDTH-1:0] ds_data_o;
   logic [NUM_OUT-1:0] 		       ds_valid_o;


   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   axis_tree_router 
     axis_tree_router1 (
			.clk(clk),
			.reset(reset),
			.us_data_i(us_data_i),
			.us_sel_ch_i(us_sel_ch_i),
			.us_valid_i(us_valid_i),
			.ds_ready_i(ds_ready_i),
			.us_ready_o(us_ready_o),
			.ds_data_o(ds_data_o),
			.ds_valid_o(ds_valid_o)
			);//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports 
      clk = '0;
      reset = '1;
      ds_ready_i = '0;
      ##5;
      reset = 1'b0;
      ##5;
      ds_ready_i = '1;
      #500 $finish;
   end

   always_ff @(posedge clk) begin : REG_ASSIGN
      if(~us_valid_i) begin
	 us_valid_i <= 1'b1;
	 us_data_i <= us_data_i + 1;
      end

      if(us_valid_i & us_ready_o) begin
	 us_data_i <= us_data_i + 1;
	 us_sel_ch_i <= us_sel_ch_i + 1;
      end

      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset
	 us_valid_i <= 1'b0;
	 us_data_i <= '0;
	 us_sel_ch_i <= '0;
	 

      end
   end : REG_ASSIGN

endmodule
