import eci_cmd_defs::*;
import eci_dcs_defs::*;

module dcu_sim_wrTb();

   parameter SIM_DELAY = 5;

   //input output ports 
   //Input signals
   logic 			    clk;
   logic 			    reset;
   logic [ECI_WORD_WIDTH-1:0] 	    rsp_wd_hdr_i;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] rsp_wd_pkt_size_i;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] rsp_wd_pkt_vc_i;
   logic 			     rsp_wd_pkt_valid_i;
   logic 			     wr_req_ready_i;

   //Output signals
   logic 			     rsp_wd_pkt_ready_o;
   logic [MAX_DCU_ID_WIDTH-1:0]      wr_req_id_o;
   logic [DS_ADDR_WIDTH-1:0] 	     wr_req_addr_o;
   logic [ECI_CL_SIZE_BYTES-1:0]     wr_req_strb_o;
   logic 			     wr_req_valid_o;

   eci_word_t rsp_wd_hdr_c;
   

   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   dcu_sim_wr dcu_sim_wr1 (
			   .clk(clk),
			   .reset(reset),
			   .rsp_wd_hdr_i(rsp_wd_hdr_i),
			   .rsp_wd_pkt_size_i(rsp_wd_pkt_size_i),
			   .rsp_wd_pkt_vc_i(rsp_wd_pkt_vc_i),
			   .rsp_wd_pkt_valid_i(rsp_wd_pkt_valid_i),
			   .rsp_wd_pkt_ready_o(rsp_wd_pkt_ready_o),
			   .wr_req_id_o(wr_req_id_o),
			   .wr_req_addr_o(wr_req_addr_o),
			   .wr_req_strb_o(wr_req_strb_o),
			   .wr_req_valid_o(wr_req_valid_o),
			   .wr_req_ready_i(wr_req_ready_i)
			   );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports 
      clk = '0;
      reset = '1;
      rsp_wd_hdr_c.eci_word = '0;
      rsp_wd_pkt_size_i = '0;
      rsp_wd_pkt_vc_i = '0;
      rsp_wd_pkt_valid_i = '0;
      wr_req_ready_i = '0;
      ##5;
      reset = 1'b0;
      ##5;
      rsp_wd_pkt_size_i = 'd17;
      rsp_wd_pkt_vc_i = 'd4;
      rsp_wd_pkt_valid_i = 1'b1;
      wr_req_ready_i = 1'b1;
      #500 $finish;
   end 

   always_comb begin : CONTROLLER
      rsp_wd_hdr_i = rsp_wd_hdr_c.eci_word;
      
   end : CONTROLLER

   always_ff @(posedge clk) begin : IP_ASSIGN
      if(rsp_wd_pkt_valid_i & rsp_wd_pkt_ready_o) begin
	 rsp_wd_hdr_c <= gen_vic_hdr(
				    .my_addr_i($urandom_range(100,2000)),
				    .my_dmask_i('1),
				    .vics_i(1'b0),
				    .vicc_i(1'b1),
				    .vicd_i(1'b0),
				    .vicd_n_i(1'b0),
				    .vicc_n_i(1'b0)
				    );
	 
      end
      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset 
	 rsp_wd_hdr_c.eci_word <= gen_vic_hdr(
					      .my_addr_i('0),
					      .my_dmask_i('1),
					      .vics_i(1'b0),
					      .vicc_i(1'b1),
					      .vicd_i(1'b0),
					      .vicd_n_i(1'b0),
					      .vicc_n_i(1'b0)
					      );
      end
   end : IP_ASSIGN


   function automatic eci_word_t gen_vic_hdr
     (
      input eci_address_t my_addr_i,
      input eci_dmask_t my_dmask_i,
      input vics_i, // V21
      input vicc_i, // V32d
      input vicd_i, // V31d
      input vicd_n_i, // V31
      input vicc_n_i // V32
      );
      eci_word_t my_hdr;
      my_hdr.eci_word = '0;
      if(vics_i) begin
	 my_hdr.mrsp_0to2.opcode = ECI_CMD_MRSP_VICS;
	 my_hdr.mrsp_0to2.dmask = '0;
      end
      if(vicd_n_i | vicd_i) begin
	 my_hdr.mrsp_0to2.opcode = ECI_CMD_MRSP_VICD;
	 if(vicd_n_i) begin
	    my_hdr.mrsp_0to2.dmask = '0;
	 end else begin
	    my_hdr.mrsp_0to2.dmask = my_dmask_i;
	 end
      end
      if(vicc_n_i | vicc_i) begin
	 my_hdr.mrsp_0to2.opcode = ECI_CMD_MRSP_VICC;
	 if(vicc_n_i) begin
	    my_hdr.mrsp_0to2.dmask = '0;
	 end else begin
	    my_hdr.mrsp_0to2.dmask = my_dmask_i;
	 end
      end
      my_hdr.mrsp_0to2.ns = 1'b1;
      my_hdr.mrsp_0to2.address = my_addr_i;
      return(my_hdr);
   endfunction : gen_vic_hdr

endmodule
