/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-06-27
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */


`ifndef WR_TRMGR_SV
`define WR_TRMGR_SV

module wr_trmgr #
  (
   parameter ID_WIDTH = 7,
   parameter META_DATA_WIDTH = 64,
   parameter ADDR_WIDTH = 38,
   parameter DATA_WIDTH = 1024,
   parameter DMASK_WIDTH = 4,
   parameter DATA_STRB_WIDTH = (DATA_WIDTH/8)
   )
   (
    input logic 		       clk,
    input logic 		       reset,

    // desc ID + meta data + addr + data to write. 
    input logic [ID_WIDTH-1:0] 	       desc_id_i,
    input logic [META_DATA_WIDTH-1:0]  desc_meta_i,
    input logic [ADDR_WIDTH-1:0]       desc_addr_i,
    input logic [DMASK_WIDTH-1:0]      desc_dmask_i,
    //input logic [DATA_WIDTH-1:0]       desc_data_i,
    input logic 		       desc_valid_i,
    output logic 		       desc_ready_o,

    // Write request downstream.
    // request ID is different from desc ID.
    output logic [ID_WIDTH-1:0]        req_id_o,
    output logic [ADDR_WIDTH-1:0]      req_addr_o,
    //output logic [DATA_WIDTH-1:0]      req_data_o,
    output logic [DATA_STRB_WIDTH-1:0] req_strb_o,
    output logic 		       req_valid_o,
    input logic 		       req_ready_i,

    // Write response from downstream.
    input logic [ID_WIDTH-1:0] 	       rsp_id_i,
    input logic [1:0] 		       rsp_bresp_i,
    input logic 		       rsp_valid_i,
    output logic 		       rsp_ready_o,

    // Output ECI ID, header and bresp corresponding to write response.
    output logic [ID_WIDTH-1:0]        desc_id_o,
    output logic [META_DATA_WIDTH-1:0] desc_meta_o,
    output logic [1:0] 		       desc_bresp_o,
    output logic 		       desc_valid_o,
    input logic 		       desc_ready_i
    );

   localparam NUM_SCLS = DMASK_WIDTH;
   localparam SCL_WIDTH = 256;
   localparam SCL_BSTRB_WIDTH = SCL_WIDTH/8;
   
   enum {REQ_ACCEPT, RSP_ACCEPT} state_reg, state_next;

   logic [NUM_SCLS-1:0][SCL_BSTRB_WIDTH-1:0] scl_byte_strobe;
   
   // register to store meta data.
   logic [META_DATA_WIDTH-1:0] desc_meta_reg = '0, desc_meta_next;

   // Output write request interface.
   // Wiring the desc_i i/f directly to the req i/f.
   // The request ID is always DCU id.
   // Flow control is enabled in these i/fs only 
   // when new requests can be accepted.
   assign req_id_o = desc_id_i;
   assign req_addr_o = desc_addr_i;
   assign req_strb_o = scl_byte_strobe;
   assign req_valid_o = (state_reg == REQ_ACCEPT) ? desc_valid_i : 1'b0;
   assign desc_ready_o = (state_reg == REQ_ACCEPT) ? req_ready_i : 1'b0;

   // Output read response dec i/f.
   // Retrieve stored ID and ECI Hdr (there is only 1 on-going transaction).
   // State machine determines when this is accepted. 
   assign desc_id_o = rsp_id_i;
   assign desc_meta_o = desc_meta_reg;
   assign desc_bresp_o = rsp_bresp_i;
   assign desc_valid_o = (state_reg == RSP_ACCEPT) ? rsp_valid_i : 1'b0;
   assign rsp_ready_o = (state_reg == RSP_ACCEPT) ? desc_ready_i : 1'b0;

   // Latch the desc ID and meta data (ECI hdr)
   // whenever a handshake happens in the desc i/f.
   always_comb begin : IP_REG
      desc_meta_next	= desc_meta_reg;
      if(desc_valid_i & desc_ready_o) begin
	 desc_meta_next = desc_meta_i;
      end
      // sets scl_byte_strobe
      set_wr_byte_strobe(
			 .wr_slot_0(desc_dmask_i[0]),
			 .wr_slot_1(desc_dmask_i[1]),
			 .wr_slot_2(desc_dmask_i[2]),
			 .wr_slot_3(desc_dmask_i[3])
			 );
   end : IP_REG

   // State machine:
   // Only 1 transaction can be on-going at a time.
   // Wait to accept request, when request is accepted
   // go to accepting response.
   // Once response is accepted go back to accepting
   // requests. 
   always_comb begin : CONTROLLER
      state_next = state_reg;
      case(state_reg)
	REQ_ACCEPT: begin
	  if(desc_valid_i & desc_ready_o) begin
	     state_next = RSP_ACCEPT;
	  end
	end
	RSP_ACCEPT: begin
	   if(rsp_valid_i & rsp_ready_o) begin
	      state_next = REQ_ACCEPT;
	   end
	end
      endcase
   end : CONTROLLER

   always_ff @(posedge clk) begin : REG_ASSIGN
      desc_meta_reg <= desc_meta_next;
      state_reg     <= state_next;
      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset 
	 state_reg     <= REQ_ACCEPT;
      end
   end : REG_ASSIGN

   task static set_wr_byte_strobe(
				  input logic wr_slot_0,
				  input logic wr_slot_1, 
				  input logic wr_slot_2, 
				  input logic wr_slot_3 
				  );
      // assign byte strobe values for each write slot.
      // all bytes within a write slot will have the same strobe value. 
      scl_byte_strobe[0] = {SCL_BSTRB_WIDTH{wr_slot_0}};
      scl_byte_strobe[1] = {SCL_BSTRB_WIDTH{wr_slot_1}};
      scl_byte_strobe[2] = {SCL_BSTRB_WIDTH{wr_slot_2}};
      scl_byte_strobe[3] = {SCL_BSTRB_WIDTH{wr_slot_3}};
   endtask //set_wr_byte_strobe
endmodule // wr_trmgr

`endif
