#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
$XILINX_VIVADO/data/verilog/src/glbl.v \
      ../testbench/interface_bus_assignTb.sv \
      ../rtl/interface_bus_assign.sv \
      ../../../common/axis_comb_router/rtl/axis_comb_router.sv


xelab -debug typical -incremental -L xpm worklib.interface_bus_assignTb worklib.glbl -s worklib.interface_bus_assignTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.interface_bus_assignTb 
xsim -gui worklib.interface_bus_assignTb 
