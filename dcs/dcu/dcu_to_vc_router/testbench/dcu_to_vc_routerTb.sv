import eci_cmd_defs::*;

module dcu_to_vc_routerTb();

   //input output ports 
   //Input signals
   logic 				    clk;
   logic 				    reset;
   logic [ECI_WORD_WIDTH-1:0] 		    eci_rsp_hdr_i;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	    eci_rsp_size_i;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	    eci_rsp_vc_i;
   logic 				    eci_rsp_valid_i;
   logic 				    rsp_wod_pkt_ready_i;
   logic 				    rsp_wd_pkt_ready_i;

   //Output signals
   logic 				    eci_rsp_ready_o;
   logic [ECI_WORD_WIDTH-1:0] 		    rsp_wod_hdr_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	    rsp_wod_pkt_size_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	    rsp_wod_pkt_vc_o;
   logic 				    rsp_wod_pkt_valid_o;
   logic [ECI_WORD_WIDTH-1:0] 		    rsp_wd_hdr_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	    rsp_wd_pkt_size_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	    rsp_wd_pkt_vc_o;
   logic 				    rsp_wd_pkt_valid_o;

   localparam WOD_Q_WIDTH = (ECI_LCL_TOT_NUM_VCS_WIDTH + ECI_PACKET_SIZE_WIDTH + ECI_WORD_WIDTH);
   localparam WD_Q_WIDTH = WOD_Q_WIDTH;
   
   logic [WOD_Q_WIDTH-1:0] 				exp_rsp_wod_queue[$]; //unbounded.
   logic [WD_Q_WIDTH-1:0] 				exp_rsp_wd_queue[$];  //unbounded.
   logic [WOD_Q_WIDTH-1:0] 				act_rsp_wod_queue[$]; //unbounded.
   logic [WD_Q_WIDTH-1:0] 				act_rsp_wd_queue[$];  //unbounded.


   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   dcu_to_vc_router dcu_to_vc_router1 (
				       .clk(clk),
				       .reset(reset),
				       .eci_rsp_hdr_i(eci_rsp_hdr_i),
				       .eci_rsp_size_i(eci_rsp_size_i),
				       .eci_rsp_vc_i(eci_rsp_vc_i),
				       .eci_rsp_valid_i(eci_rsp_valid_i),
				       .rsp_wod_pkt_ready_i(rsp_wod_pkt_ready_i),
				       .rsp_wd_pkt_ready_i(rsp_wd_pkt_ready_i),
				       .eci_rsp_ready_o(eci_rsp_ready_o),
				       .rsp_wod_hdr_o(rsp_wod_hdr_o),
				       .rsp_wod_pkt_size_o(rsp_wod_pkt_size_o),
				       .rsp_wod_pkt_vc_o(rsp_wod_pkt_vc_o),
				       .rsp_wod_pkt_valid_o(rsp_wod_pkt_valid_o),
				       .rsp_wd_hdr_o(rsp_wd_hdr_o),
				       .rsp_wd_pkt_size_o(rsp_wd_pkt_size_o),
				       .rsp_wd_pkt_vc_o(rsp_wd_pkt_vc_o),
				       .rsp_wd_pkt_valid_o(rsp_wd_pkt_valid_o)
				       );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports 
      clk = '0;
      reset = '1;
      eci_rsp_hdr_i = '0;
      eci_rsp_size_i = '0;
      eci_rsp_vc_i = '0;
      eci_rsp_valid_i = '0;
      rsp_wod_pkt_ready_i = '0;
      rsp_wd_pkt_ready_i = '0;

      ##5;
      reset = 1'b0;
      ##1;

      rsp_wod_pkt_ready_i = '1;
      rsp_wd_pkt_ready_i = '1;

      // Tests
      // Send a number of different eci response packets
      // capture expected and actual values in queues.
      // once all transactions are completed, compare
      // expected and actual responses.
      // rsp_wod and rsp_wd are the only valid inputs
      // fwd_wod should be ignored by the module. 

      send_ip_rsp_wod();
      send_ip_rsp_wod();
      send_ip_fwd_wod();
      send_ip_rsp_wd();
      send_ip_rsp_wd();
      send_ip_fwd_wod();
      send_ip_rsp_wod();
      send_ip_rsp_wod();
      send_ip_fwd_wod();
      send_ip_rsp_wd();
      send_ip_rsp_wd();
      send_ip_fwd_wod();
      send_ip_rsp_wod();
      send_ip_rsp_wod();
      send_ip_fwd_wod();
      send_ip_rsp_wd();
      send_ip_rsp_wd();
      send_ip_fwd_wod();

      eci_rsp_valid_i = 1'b0;

      ##5;

      // Compare results for rsp_wod
      foreach(act_rsp_wod_queue[i]) begin
	 if(exp_rsp_wod_queue[i] !== act_rsp_wod_queue[i]) begin
	    $fatal(1, "Rsp Wod %0d: Expected %0d does not match Actual %0d", 
		   i, exp_rsp_wod_queue[i], act_rsp_wod_queue[i]);
	 end else begin
	    $display("Rsp Wod %0d Success: Expected %0d, Actual %0d", 
		     i, exp_rsp_wod_queue[i], act_rsp_wod_queue[i]);
	 end
      end

      // Compare results for rsp_wd
      foreach(act_rsp_wd_queue[i]) begin
	 if(exp_rsp_wd_queue[i] !== act_rsp_wd_queue[i]) begin
	    $fatal(1, "Rsp Wd %0d: Expected %0d does not match Actual %0d", 
		   i, exp_rsp_wd_queue[i], act_rsp_wd_queue[i]);
	 end else begin
	    $display("Rsp Wd %0d Success: Expected %0d, Actual %0d", 
		   i, exp_rsp_wd_queue[i], act_rsp_wd_queue[i]);
	 end
      end

      $display("Success: All tests pass");
      #500 $finish;
   end 

   always_ff @(posedge clk) begin : REG_ASSIGN
      if(rsp_wod_pkt_valid_o & rsp_wod_pkt_ready_i) begin
	 act_rsp_wod_queue.push_back({rsp_wod_hdr_o, rsp_wod_pkt_size_o, rsp_wod_pkt_vc_o});
      end
      if(rsp_wd_pkt_valid_o & rsp_wd_pkt_ready_i) begin
	 act_rsp_wd_queue.push_back({rsp_wd_hdr_o, rsp_wd_pkt_size_o, rsp_wd_pkt_vc_o});
      end
      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset 

      end
   end : REG_ASSIGN
   
   // Tasks.
   task static send_ip_rsp_wod();
      eci_rsp_hdr_i = $urandom_range(100,500);
      eci_rsp_vc_i = $urandom_range(10,11);
      eci_rsp_size_i = 'd1;
      eci_rsp_valid_i = '1;
      wait(eci_rsp_valid_i & eci_rsp_ready_o);
      exp_rsp_wod_queue.push_back({eci_rsp_hdr_i, eci_rsp_size_i, eci_rsp_vc_i});
      ##1;
   endtask //send_ip_rsp_wod

   task static send_ip_fwd_wod();
      // this would just be discarded
      eci_rsp_hdr_i = $urandom_range(100,500);
      eci_rsp_vc_i = $urandom_range(8,9);
      eci_rsp_size_i = 'd1;
      eci_rsp_valid_i = '1;
      wait(eci_rsp_valid_i & eci_rsp_ready_o);
      ##1;
   endtask //send_ip_fwd_wod
   
   task static send_ip_rsp_wd();
      eci_rsp_hdr_i = $urandom_range(100,500);
      eci_rsp_vc_i = $urandom_range(4,5);
      eci_rsp_size_i = $urandom_range(1, 17);
      eci_rsp_valid_i = '1;
      wait(eci_rsp_valid_i & eci_rsp_ready_o);
      exp_rsp_wd_queue.push_back({eci_rsp_hdr_i, eci_rsp_size_i, eci_rsp_vc_i});
      ##1;
   endtask //send_ip_rsp_wod

endmodule
