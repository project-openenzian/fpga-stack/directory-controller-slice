`ifndef DCU_SIM_WR_SV
`define DCU_SIM_WR_SV

import eci_cmd_defs::*;
import eci_dcs_defs::*;

module dcu_sim_wr #
  (
   parameter SIM_DELAY = 5
   )
   (
    input logic 			    clk,
    input logic 			    reset,
    // Input ECI response with data only header. 
    input logic [ECI_WORD_WIDTH-1:0] 	    rsp_wd_hdr_i,
    input logic [ECI_PACKET_SIZE_WIDTH-1:0] rsp_wd_pkt_size_i,
    input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] rsp_wd_pkt_vc_i,
    input logic 			    rsp_wd_pkt_valid_i,
    output logic 			    rsp_wd_pkt_ready_o,
    // Output write descriptor, no data only descriptor.
    output logic [MAX_DCU_ID_WIDTH-1:0]     wr_req_id_o, //7b
    output logic [DS_ADDR_WIDTH-1:0] 	    wr_req_addr_o, //38b
    output logic [ECI_CL_SIZE_BYTES-1:0]    wr_req_strb_o,
    output logic 			    wr_req_valid_o,
    input logic 			    wr_req_ready_i
    );

   localparam NUM_SCLS = ECI_DMASK_WIDTH;
   localparam SCL_WIDTH = 256;
   localparam SCL_BSTRB_WIDTH = SCL_WIDTH/8;

   eci_word_t rsp_wd_hdr_c;
   logic [ECI_ADDR_WIDTH-1:0] rsp_wd_addr;
   ds_cl_addr_t ds_cl_addr_c;
   logic [NUM_SCLS-1:0][SCL_BSTRB_WIDTH-1:0] scl_byte_strobe;
   eci_dmask_t rsp_wd_dmask;
   logic [63:0] 			     counter_reg = '0, counter_next;
   logic 				     en_vr;

   always_comb begin : CAST_ECI_HDR
      rsp_wd_hdr_c = eci_word_t'(rsp_wd_hdr_i);
      rsp_wd_addr = rsp_wd_hdr_i[ECI_ADDR_WIDTH-1:0];
      ds_cl_addr_c = ds_cl_addr_t'(rsp_wd_addr);
      rsp_wd_dmask = rsp_wd_hdr_c.generic_cmd.dmask;
      // sets scl_byte_strobe
      set_wr_byte_strobe(
			 .wr_slot_0(rsp_wd_dmask[0]),
			 .wr_slot_1(rsp_wd_dmask[1]),
			 .wr_slot_2(rsp_wd_dmask[2]),
			 .wr_slot_3(rsp_wd_dmask[3])
			 );
   end : CAST_ECI_HDR
   
   always_comb begin : OP_ASSIGN
      wr_req_id_o = '0;
      wr_req_id_o[DS_DCU_ID_WIDTH-1:0] = ds_cl_addr_c.dcu_id;
      wr_req_addr_o = rsp_wd_addr;
      wr_req_strb_o = scl_byte_strobe;
      wr_req_valid_o = rsp_wd_pkt_valid_i & en_vr;
      rsp_wd_pkt_ready_o = wr_req_ready_i & en_vr;
   end : OP_ASSIGN

   // counter is initially at 0.
   // if input is valid, start counter.
   // keep counter running till SIM_DELAY 
   // count is reached. Once count is
   // reached, output becomes valid and
   // downstream ready is fed into input.
   // Counter is reset upon handshake in
   // request i/f.
   always_comb begin : CONTROLLER
      counter_next = counter_reg;
      en_vr = 1'b0;
      
      if(counter_reg == SIM_DELAY) begin
	 en_vr = 1'b1;
      end
 
      if(rsp_wd_pkt_valid_i & rsp_wd_pkt_ready_o) begin
	 counter_next = '0;
      end else if(rsp_wd_pkt_valid_i & ~en_vr) begin
	 counter_next = counter_reg + 1;
      end 
   end : CONTROLLER

   always_ff @(posedge clk) begin : REG_ASSIGN
      counter_reg	<= counter_next;
      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset 
	 counter_reg	<= '0;
      end
   end : REG_ASSIGN


   task static set_wr_byte_strobe(
				  input logic wr_slot_0,
				  input logic wr_slot_1, 
				  input logic wr_slot_2, 
				  input logic wr_slot_3 
				  );
      // assign byte strobe values for each write slot.
      // all bytes within a write slot will have the same strobe value. 
      scl_byte_strobe[0] = {SCL_BSTRB_WIDTH{wr_slot_0}};
      scl_byte_strobe[1] = {SCL_BSTRB_WIDTH{wr_slot_1}};
      scl_byte_strobe[2] = {SCL_BSTRB_WIDTH{wr_slot_2}};
      scl_byte_strobe[3] = {SCL_BSTRB_WIDTH{wr_slot_3}};
   endtask //set_wr_byte_strobe

endmodule // dcu_sim_wr

`endif
