#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
      $XILINX_VIVADO/data/verilog/src/glbl.v \
      ../testbench/eci_dcs_defs.sv \
      ../testbench/pipeline_stage.sv \
      ../testbench/dp_wr_serTb.sv \
      ../testbench/dp_mem.sv \
      ../rtl/dp_wr_ser.sv 

xelab -debug typical -incremental -L xpm worklib.dp_wr_serTb worklib.glbl -s worklib.dp_wr_serTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.dp_wr_serTb 
xsim -gui worklib.dp_wr_serTb 
