/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-11-16
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */

`ifndef DC_TO_VC_ROUTER_SV
`define DC_TO_VC_ROUTER_SV

import eci_cmd_defs::*;

// Module takes ECI header + VC number
// to route to appropriate VC.
// currently only supports 3 output VCs.
//  rsp_wod - VC 10/11
//  rsp_wd  - VC 4/5
//  fwd_wod - VC 8/9

module dc_to_vc_router 
  (
   input logic 				    clk,
   input logic 				    reset,
   // Input ECI header + VC number.
   input logic [ECI_WORD_WIDTH-1:0] 	    ecih_hdr_i,
   input logic [ECI_PACKET_SIZE_WIDTH-1:0]  ecih_pkt_size_i,
   input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0]  ecih_pkt_vc_i,
   input logic 				    ecih_pkt_valid_i,
   output logic 			    ecih_pkt_ready_o,
   // Output channels.
   // Response without data: VC 10 or 11.
   output logic [ECI_WORD_WIDTH-1:0] 	    rsp_wod_hdr_o,
   output logic [ECI_PACKET_SIZE_WIDTH-1:0] rsp_wod_pkt_size_o,
   output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] rsp_wod_pkt_vc_o,
   output logic 			    rsp_wod_pkt_valid_o,
   input logic 				    rsp_wod_pkt_ready_i,

   // Response with data headers: VC 4 or 5. (only header).
   output logic [ECI_WORD_WIDTH-1:0] 	    rsp_wd_hdr_o,
   output logic [ECI_PACKET_SIZE_WIDTH-1:0] rsp_wd_pkt_size_o,
   output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] rsp_wd_pkt_vc_o,
   output logic 			    rsp_wd_pkt_valid_o,
   input logic 				    rsp_wd_pkt_ready_i,

   // Fwd without data headers: VC 8 or 9.
   output logic [ECI_WORD_WIDTH-1:0] 	    fwd_wod_hdr_o,
   output logic [ECI_PACKET_SIZE_WIDTH-1:0] fwd_wod_pkt_size_o,
   output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] fwd_wod_pkt_vc_o,
   output logic 			    fwd_wod_pkt_valid_o,
   input logic 				    fwd_wod_pkt_ready_i,

   // local rsp without data headers: VC 18 or 19.
   output logic [ECI_WORD_WIDTH-1:0] 	    lcl_rsp_wod_hdr_o,
   output logic [ECI_PACKET_SIZE_WIDTH-1:0] lcl_rsp_wod_pkt_size_o,
   output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] lcl_rsp_wod_pkt_vc_o,
   output logic 			    lcl_rsp_wod_pkt_valid_o,
   input logic 				    lcl_rsp_wod_pkt_ready_i
   );

   localparam PIPE_DATA_WIDTH = ECI_WORD_WIDTH + ECI_PACKET_SIZE_WIDTH + ECI_LCL_TOT_NUM_VCS_WIDTH;
   typedef struct packed {
      logic [ECI_WORD_WIDTH-1:0] 	    hdr;
      logic [ECI_PACKET_SIZE_WIDTH-1:0]     size;
      logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0]     vc;
      logic 				    valid;
      logic 				    ready;
   } eci_hdr_if_t;
   
   eci_hdr_if_t ecih_p_i, ecih_p_o;
   logic rsp_wod_hs, rsp_wd_hs, fwd_wod_hs, lcl_rsp_wod_hs;

   always_comb begin : MISC_ASSIGN
      rsp_wod_hs = rsp_wod_pkt_valid_o & rsp_wod_pkt_ready_i;
      rsp_wd_hs  = rsp_wd_pkt_valid_o & rsp_wd_pkt_ready_i;
      fwd_wod_hs = fwd_wod_pkt_valid_o & fwd_wod_pkt_ready_i;
      lcl_rsp_wod_hs = lcl_rsp_wod_pkt_valid_o & lcl_rsp_wod_pkt_ready_i;
   end : MISC_ASSIGN

   always_comb begin : OUT_ASSIGN
      ecih_pkt_ready_o	 = ecih_p_i.ready;
      // Broadcast incoming ecih to all output channels.
      // only one of the output channels will be enabled.
      // rsp_wod channel.
      rsp_wod_hdr_o      = ecih_p_o.hdr;
      rsp_wod_pkt_size_o = ecih_p_o.size;
      rsp_wod_pkt_vc_o   = ecih_p_o.vc;
      // rsp_wd channel.
      rsp_wd_hdr_o       = ecih_p_o.hdr;
      rsp_wd_pkt_size_o  = ecih_p_o.size;
      rsp_wd_pkt_vc_o    = ecih_p_o.vc;
      // fwd_wod channel.
      fwd_wod_hdr_o      = ecih_p_o.hdr;
      fwd_wod_pkt_size_o = ecih_p_o.size;
      fwd_wod_pkt_vc_o   = ecih_p_o.vc;
      // lcl_rsp_wod channel.
      lcl_rsp_wod_hdr_o      = ecih_p_o.hdr;
      lcl_rsp_wod_pkt_size_o = ecih_p_o.size;
      lcl_rsp_wod_pkt_vc_o   = ecih_p_o.vc;
   end : OUT_ASSIGN
   
   // Input eci header is passed through
   // a pipeline stage. Output of pipeline
   // stage is broadcast to both output channels.
   // but only one of the channels is enabled.
   // The output of pipeline stage is consumed
   // when a handshake happens in either of the
   // output channels. 
   always_comb begin : PIPE_IP_ASSIGN
      ecih_p_i.hdr   = ecih_hdr_i;
      ecih_p_i.size  = ecih_pkt_size_i;
      ecih_p_i.vc    = ecih_pkt_vc_i;
      ecih_p_i.valid = ecih_pkt_valid_i;
      ecih_p_o.ready = rsp_wod_hs | rsp_wd_hs | fwd_wod_hs | lcl_rsp_wod_hs;
   end : PIPE_IP_ASSIGN
   axis_pipeline_stage #
     (
      .DATA_WIDTH(PIPE_DATA_WIDTH)
      )
   in_pipe_stage_1
     (
      .clk(clk),
      .reset(reset),
      .us_data({ecih_p_i.hdr, ecih_p_i.size, ecih_p_i.vc}),
      .us_valid(ecih_p_i.valid),
      .us_ready(ecih_p_i.ready),
      .ds_data({ecih_p_o.hdr, ecih_p_o.size, ecih_p_o.vc}),
      .ds_valid(ecih_p_o.valid),
      .ds_ready(ecih_p_o.ready)
      );

   // The output channel that will be valid
   // depends on the VC number. 
   always_comb begin : CONTROLLER
      rsp_wod_pkt_valid_o = 1'b0;
      rsp_wd_pkt_valid_o  = 1'b0;
      fwd_wod_pkt_valid_o = 1'b0;
      lcl_rsp_wod_pkt_valid_o = 1'b0;
      if(ecih_p_o.vc < 'd6) begin
	 // VC [2,3],4,5
	 // rsp_wd.
	 rsp_wod_pkt_valid_o = 1'b0;
	 rsp_wd_pkt_valid_o  = ecih_p_o.valid;
	 fwd_wod_pkt_valid_o = 1'b0;
	 lcl_rsp_wod_pkt_valid_o = 1'b0;
      end else if (ecih_p_o.vc < 'd10) begin
	 // VC [6,7],8,9
	 // fwd_wod.
	 rsp_wod_pkt_valid_o = 1'b0;
	 rsp_wd_pkt_valid_o  = 1'b0;
	 fwd_wod_pkt_valid_o = ecih_p_o.valid;
	 lcl_rsp_wod_pkt_valid_o = 1'b0;
      end else if(ecih_p_o.vc < 'd18)begin
	 // VC 10,11,[12,13,14,15,16,17]
	 // rsp_wod.
	 rsp_wod_pkt_valid_o = ecih_p_o.valid;
	 rsp_wd_pkt_valid_o  = 1'b0;
	 fwd_wod_pkt_valid_o = 1'b0;
	 lcl_rsp_wod_pkt_valid_o = 1'b0;
      end else begin
	 // VC 18,19
	 // lcl_rsp_wod.
	 rsp_wod_pkt_valid_o = 1'b0;
	 rsp_wd_pkt_valid_o  = 1'b0;
	 fwd_wod_pkt_valid_o = 1'b0;
	 lcl_rsp_wod_pkt_valid_o = ecih_p_o.valid;
      end
   end : CONTROLLER

endmodule // dc_to_vc_router

`endif
