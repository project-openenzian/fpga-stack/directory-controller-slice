#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
$XILINX_VIVADO/data/verilog/src/glbl.v \
../testbench/simple_delay_moduleTb.sv \
../rtl/simple_delay_module.sv 

xelab -debug typical -incremental -L xpm worklib.simple_delay_moduleTb worklib.glbl -s worklib.simple_delay_moduleTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.simple_delay_moduleTb 
xsim -gui worklib.simple_delay_moduleTb 
