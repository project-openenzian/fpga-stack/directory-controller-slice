#!/usr/bin/perl

no autovivification;
use warnings;
use strict;
use Data::Dumper;
use Getopt::Long;

my @seed_eqns = (
    #Remote state, Local req, next home state, home action, next remote state 
    # Note 1pR and 1pW are both same stable state 1
    # except one does read action and other write action 
    "1, LR, 1pLRA, RDD, 1",
    "1, LW, 1pLWA, WDD, 1",
    "1, LC, 1, -:Send LCA, 1",
    "1, LCI, 1, -:Send LCIA, 1",
    #Induced clean invalidate has no local response
    "1, ICI, 1, -:-, 1",
    "2, LR, 1pLRA, RDD, 2",
    "2, LC, 1, -:Send LCA, 2",
    "2, LW, 1pW, -:Send F21, 1_A21",
    "2, LCI, 1pCI, -:Send F21, 1_A21",
    "2, ICI, 1pICI, -:Send F21, 1_A21",
    "3, LR, 1pR, -:Send F32, 2_A32d",
    "3, LC, 1pC, -:Send F32, 2_A32d",
    "3, LW, 1pW, -:Send F31, 1_A31d",
    "3, LCI, 1pCI, -:Send F31, 1_A31d",
    "3, ICI, 1pICI, -:Send F31, 1_A31d",
    );

#remote events
my @rmt_evts = (
    # Remote requests, response description
    #event, type, remote action"
    "R12, request, RDD",
    "R13, request, RDD",
    "R23, request, RDD",
    "A32, ack_resp, -:-",
    "A32d, ack_resp, WDD:-",
    "A31, ack_resp, -:-",
    "A31d, ack_resp, WDD:-",
    "A22, ack_resp, -:-",
    "A21, ack_resp, -:-",
    "A11, ack_resp, -:-",
    "V32d, ev_resp, WDD:-",
    "V32, ev_resp, -:-",
    "V31d, ev_resp, WDD:-",
    "V31, ev_resp, -:-",
    "V21, ev_resp, -:-",
    "RR, request, RDD",
    "RW, request, WDD",
    );

my @lcl_evts = (
    #event, type, action"
    "LR, request, RDD",
    "LC, request, -:Send LCA",
    "LW, request, WDD",
    "LCI, request, -:Send LCIA",
    "ICI, request, -:-",
    "RDDA, response, -:-",
    "WDDA, response, -:-",
    );

my @lcl_actions_list = (
    #home intermediate state, effective state, home transition to, associated local req 
    #read
    "1pR, 2, 1pLRA, LR",
    "1pR, 1, 1pLRA, LR",
    # Clean 
    "1pC, 2, 1, LC",
    "1pC, 1, 1, LC",
    #write
    "1pW, 1, 1pLWA, LW",
    #clean invalidate
    "1pCI, 1, 1, LCI",
    #induced clean invalidate
    "1pICI, 1, 1, ICI",
    );

# Note: when remotee is waiting for a response, it cannot issue another request for the CL
# for example if remote requested R12 before receiving RA2, it cannot issue R13/R23/RR/RW
my @rs_eqns = (
    "1, RR, 1pRRA",   # next state is 1 pending RRA
    "1pRRA, RDDA, 1", # until response is sent, I cannot receive another request for same CL from RMT
    "1, RW, 1pRWA",
    "1pRWA, WDDA, 1",
    "1, R12, 2pRA2",  # next state is 2 pending RA2
    "2pRA2, RDDA, 2", # THX req to goto state 2, cannot issue RR, RW, R13 till a response is received
    "1, R13, 3pRA3",  # next state is 3 pending RA3  
    "3pRA3, RDDA, 3",
    "2, R23, 3",
    "2, V21, 1",
    "2, V21, RR, 1pRRA",
    "2, V21, RW, 1pRWA",
    "2, V21, R12, 2pRA2",
    "2, V21, R13, 3pRA3",
    "3, V31, 1",
    "3, V31, RR, 1pRRA",
    "3, V31, RW, 1pRWA",
    "3, V31, R12, 2pRA2",
    "3, V31, R13, 3pRA3",
    "3, V31d, WDDA, 1",
    "3, V31d, WDDA, RR, 1pRRA",
    "3, V31d, WDDA, RW, 1pRWA",
    "3, V31d, WDDA, R12, 2pRA2",
    "3, V31d, WDDA, R13, 3pRA3",
    "3, V32, 2",
    "3, V32, R23, 3",
    "3, V32, V21, 1",
    "3, V32, V21, RR, 1pRRA",
    "3, V32, V21, RW, 1pRWA",
    "3, V32, V21, R12, 2pRA2",
    "3, V32, V21, R13, 3pRA3",
    "3, V32d, WDDA, 2",
    "3, V32d, WDDA, R23, 3",
    "3, V32d, WDDA, V21, 1",
    "3, V32d, WDDA, V21, RR, 1pRRA",
    "3, V32d, WDDA, V21, RW, 1pRWA",
    "3, V32d, WDDA, V21, R12, 2pRA2",
    "3, V32d, WDDA, V21, R13, 3pRA3",
    "2_A32d, A32, 2",
    "2_A32d, A32, R23, 3",
    "2_A32d, A32, V21, 1",
    "2_A32d, A32, V21, RR, 1pRRA",
    "2_A32d, A32, V21, RW, 1pRWA",
    "2_A32d, A32, V21, R12, 2pRA2",
    "2_A32d, A32, V21, R13, 3pRA3",
    "2_A32d, A32d, WDDA, 2",
    "2_A32d, A32d, WDDA, R23, 3",
    "2_A32d, A32d, WDDA, V21, 1",
    "2_A32d, A32d, WDDA, V21, RR, 1pRRA",
    "2_A32d, A32d, WDDA, V21, RW, 1pRWA",
    "2_A32d, A32d, WDDA, V21, R12, 2pRA2",
    "2_A32d, A32d, WDDA, V21, R13, 3pRA3",
    "2_A32d, V32, A22, 2",
    "2_A32d, V32, A22, R23, 3",
    "2_A32d, V32, A22, V21, 1",
    "2_A32d, V32, A22, V21, RR, 1pRRA",
    "2_A32d, V32, A22, V21, RW, 1pRWA",
    "2_A32d, V32, A22, V21, R12, 2pRA2",
    "2_A32d, V32, A22, V21, R13, 3pRA3",
    "2_A32d, V32, A22, R23, 3",
    "2_A32d, V32d, WDDA, A22, 2",
    "2_A32d, V32d, WDDA, A22, R23, 3",
    "2_A32d, V32d, WDDA, A22, V21, 1",
    "2_A32d, V32d, WDDA, A22, V21, RR, 1pRRA",
    "2_A32d, V32d, WDDA, A22, V21, RW, 1pRWA",
    "2_A32d, V32d, WDDA, A22, V21, R12, 2pRA2",
    "2_A32d, V32d, WDDA, A22, V21, R13, 3pRA3",
    "2_A32d, V32d, WDDA, A22, R23, 3",
    "2_A32d, V31, A11, 1",
    "2_A32d, V31, A11, RR, 1pRRA",
    "2_A32d, V31, A11, RW, 1pRWA",
    "2_A32d, V31, A11, R12, 2pRA2",
    "2_A32d, V31, A11, R13, 3pRA3",
    "2_A32d, V31d, WDDA, A11, 1",
    "2_A32d, V31d, WDDA, A11, RR, 1pRRA",
    "2_A32d, V31d, WDDA, A11, RW, 1pRWA",
    "2_A32d, V31d, WDDA, A11, R12, 2pRA2",
    "2_A32d, V31d, WDDA, A11, R13, 3pRA3",
    "2_A32d, V32, V21, A11, 1",
    "2_A32d, V32, V21, A11, RR, 1pRRA",
    "2_A32d, V32, V21, A11, RW, 1pRWA",
    "2_A32d, V32, V21, A11, R12, 2pRA2",
    "2_A32d, V32, V21, A11, R13, 3pRA3",
    "2_A32d, V32d, WDDA, V21, A11, 1",
    "2_A32d, V32d, WDDA, V21, A11, RR, 1pRRA",
    "2_A32d, V32d, WDDA, V21, A11, RW, 1pRWA",
    "2_A32d, V32d, WDDA, V21, A11, R12, 2pRA2",
    "2_A32d, V32d, WDDA, V21, A11, R13, 3pRA3",
    "1_A21, A21, 1",
    "1_A21, A21, RR, 1pRRA",
    "1_A21, A21, RW, 1pRWA",
    "1_A21, A21, R12, 2pRA2",
    "1_A21, A21, R13, 3pRA3",
    "1_A21, A21, R23, 3pRA3",
    "1_A21, V21, A11, 1",
    "1_A21, V21, A11, RR, 1pRRA",
    "1_A21, V21, A11, RW, 1pRWA",
    "1_A21, V21, A11, R12, 2pRA2",
    "1_A21, V21, A11, R13, 3pRA3",
    "1_A31d, A31, 1",
    "1_A31d, A31, RR, 1pRRA",
    "1_A31d, A31, RW, 1pRWA",
    "1_A31d, A31, R12, 2pRA2",
    "1_A31d, A31, R13, 3pRA3",
    "1_A31d, A31d, WDDA, 1",
    "1_A31d, A31d, WDDA, RR, 1pRRA",
    "1_A31d, A31d, WDDA, RW, 1pRWA",
    "1_A31d, A31d, WDDA, R12, 2pRA2",
    "1_A31d, A31d, WDDA, R13, 3pRA3",
    "1_A31d, V32, A21, 1",
    "1_A31d, V32, A21, RR, 1pRRA",
    "1_A31d, V32, A21, RW, 1pRWA",
    "1_A31d, V32, A21, R12, 2pRA2",
    "1_A31d, V32, A21, R13, 3pRA3",
    "1_A31d, V32, A21, R23, 3pRA3",
    "1_A31d, V32d, WDDA, A21, 1",
    "1_A31d, V32d, WDDA, A21, RR, 1pRRA",
    "1_A31d, V32d, WDDA, A21, RW, 1pRWA",
    "1_A31d, V32d, WDDA, A21, R12, 2pRA2",
    "1_A31d, V32d, WDDA, A21, R13, 3pRA3",
    "1_A31d, V32d, WDDA, A21, R23, 3pRA3",
    "1_A31d, V31, A11, 1",
    "1_A31d, V31, A11, RR, 1pRRA",
    "1_A31d, V31, A11, RW, 1pRWA",
    "1_A31d, V31, A11, R12, 2pRA2",
    "1_A31d, V31, A11, R13, 3pRA3",
    "1_A31d, V31d, WDDA, A11, 1",
    "1_A31d, V31d, WDDA, A11, RR, 1pRRA",
    "1_A31d, V31d, WDDA, A11, RW, 1pRWA",
    "1_A31d, V31d, WDDA, A11, R12, 2pRA2",
    "1_A31d, V31d, WDDA, A11, R13, 3pRA3",
    "1_A31d, V32, V21, A11, 1",
    "1_A31d, V32, V21, A11, RR, 1pRRA",
    "1_A31d, V32, V21, A11, RW, 1pRWA",
    "1_A31d, V32, V21, A11, R12, 2pRA2",
    "1_A31d, V32, V21, A11, R13, 3pRA3",
    "1_A31d, V32d, WDDA, V21, A11, 1",
    "1_A31d, V32d, WDDA, V21, A11, RR, 1pRRA",
    "1_A31d, V32d, WDDA, V21, A11, RW, 1pRWA",
    "1_A31d, V32d, WDDA, V21, A11, R12, 2pRA2",
    "1_A31d, V32d, WDDA, V21, A11, R13, 3pRA3",
    );

# Some actions to remote depend on local responses 
my $pre_def_rmt_acns = {};
$pre_def_rmt_acns->{"1pRRA"}->{"RDDA"} = "Send RRA";
$pre_def_rmt_acns->{"1pRWA"}->{"WDDA"} = "Send RWA";
$pre_def_rmt_acns->{"2pRA2"}->{"RDDA"} = "Send RA2";
$pre_def_rmt_acns->{"3pRA3"}->{"RDDA"} = "Send RA3";
$pre_def_rmt_acns->{"2"}->{"R23"} = "Send RA3";

my $pre_def_lcl_acns = {};
$pre_def_lcl_acns->{"1pLRA"}->{"RDDA"}->{"action"}     = "Send LRA";
$pre_def_lcl_acns->{"1pLRA"}->{"RDDA"}->{"next_state"} = "1";
$pre_def_lcl_acns->{"1pLWA"}->{"WDDA"}->{"action"}     = "Send LWA";
$pre_def_lcl_acns->{"1pLWA"}->{"WDDA"}->{"next_state"} = "1";


# Algorithm
# if remote is effectively in 2 then read, clean is allowed
# if remote is effectively in 1 then read, write, clean, clean invalidate allowed
# home state - affected only by local requests (from seed))
# stable - intermediate transition only as described in seed
# intermediate - stable transition when effective state is reached

my $global_stall = "Stall";
my $debug = 1;
my $state_mc_csv = "ECI_CC.csv";

# seed equations are stored here
my $seed_hash = {};
# has remote events + actions 
my $rmt_events_hash = {};
# has local events 
my $lcl_events_hash = {};
# has local actions 
my $lcl_actions_hash = {};
# what lcl event is allowed in which effective remote state
my $lcl_evt_access_ers = {};
# result of solving remote eqns 
my $rmt_states_hash = {};
# for remote intermediate state, holds the effective state 
my $effective_rs = {};

&read_seed_eqns(\@seed_eqns, $seed_hash);
&read_evt_types(\@rmt_evts, $rmt_events_hash);
&read_evt_types(\@lcl_evts, $lcl_events_hash);
&read_lcl_actions(\@lcl_actions_list, $lcl_events_hash, $lcl_actions_hash);
&get_lcl_evt_access_ers($lcl_actions_hash, $lcl_evt_access_ers);
&read_state_eqns(\@rs_eqns, $rmt_states_hash, $rmt_events_hash);
&get_effective_rmt_states($rmt_states_hash, $effective_rs);
&get_combined_state_transitions($rmt_states_hash, 
				$rmt_events_hash,
				$lcl_events_hash,
				$seed_hash,
				$lcl_actions_hash,
				$lcl_evt_access_ers,
				$state_mc_csv);

print "ECI CC Csv file - $state_mc_csv\n";

sub get_combined_state_transitions{
    # Combine home and remote state
    # and iterate through all operations
    # also prints the CSV file 
    my $r_states_hash = shift;
    my $r_events_hash = shift;
    my $l_events_hash = shift;
    my $seed_h = shift;
    my $l_actions_hash = shift;
    my $l_evt_access_ers = shift;
    my $out_csv = shift;
    
    my $allowed_states = {};
    
    # initial stable states {HS, RS}
    $allowed_states->{"1"}->{"1"} = 0;
    $allowed_states->{"1"}->{"2"} = 0;
    $allowed_states->{"1"}->{"3"} = 0;

    #Get all possible events in the system
    my @all_evts;
    # local events 
    foreach my $evt (sort keys %{$l_events_hash}){
	push(@all_evts, $evt);
    }
    # remote events 
    foreach my $revt (sort keys %{$r_events_hash}){
	push(@all_evts, $revt);
    }

    # Print header 
    my $header = "Current State HS:RS, ";
    open(my $fh, ">",$out_csv) or die "cannot open $out_csv to write\n";
    foreach my $ae (@all_evts){
	$header = $header."$ae, ";
    }
    &print_to_csv($fh, $header);
    
    while(1){
	# get the next HS, RS combination to be filled out 
	my ($not_fin, $hs, $rs) = &hs_rs_not_completed($allowed_states);
	if($not_fin == 1){
	    my $csv_line = "$hs:$rs, ";
	    #iterate thrhough all events
	    foreach my $op (@all_evts){
		my $hs_ns;
		my $rs_ns;
		# action based on local events 
		my $lcl_action;
		# action based on remote events 
		my $rmt_action;
		my $this_stall = 0;
		my $this_impossible = 0;

		#remote state and action
		# rs_ns = (stable + local op) ? seed_eqns : rmt_state_eqns
		if(defined $seed_h->{$rs}->{$op}){
		    # seed equations determine the next remote state
		    # unless presently home is reading/writing
		    # $op is a local request  
		    # action is not related to a remote event
		    if(defined $pre_def_lcl_acns->{$hs}){
			# home is in the process of reading/writing
			# stall op, no change to  remote state 
			$rs_ns = $rs;
			$rmt_action = "-:-";
			$this_stall = 1;
			&print_state_transition("rs_ns", $rs, $op, $rs_ns, $rmt_action, "Stall: Home is in $hs, waiting for local rd/wr response");
		    } else {
			# home is not in the proccess of reading/writing
			# change remote state based on seed equations and proceed
			$rs_ns = $seed_h->{$rs}->{$op}->{"rs_ns"};
			$rmt_action = "-:-";
			&print_state_transition("rs_ns", $rs, $op, $rs_ns, $rmt_action, "Seed");
		    }
		} elsif(defined $r_states_hash->{$rs}->{$op}){
		    # rs_ns defined by set of remote eqns
		    # this implies $op is a remote event 
		    $rs_ns = $r_states_hash->{$rs}->{$op}->{"next_state"};
		    $rmt_action = $r_states_hash->{$rs}->{$op}->{"action"};
		    if($rs_ns eq $global_stall){
			# remote is waiting for some responses and it has stalled a request
			# only remote requests are stalled by rmt eqns, $op is a remote request 
			$this_stall = 1;
			$rs_ns = $rs;
			$rmt_action = "-:-";
		    } else {
			# this remote event is not stalled, $op can be remote req/resp 
			# if home is in the process of doing a local read or write, remote requests are to be stalled
			# remote responses (evict/ack) can continue
			if(defined $pre_def_lcl_acns->{$hs}){
			    # home is in the middle of a local read/write
			    if($r_events_hash->{$op}->{"type"} eq "request"){
				# $op is remote request, stall it
				$this_stall = 1;
				$rs_ns = $rs;
				$rmt_action = "-:-";
			    }
			}
		    }
		    &print_state_transition("rs_ns", $rs, $op, $rs_ns, $rmt_action, "rmt eqns");
		} else {
		    # situation not potssible - rs + op not defined in seed/remote equations
		    # unless op is a local event
		    if(!defined $l_events_hash->{$op}){
			# $op is remote event, so not possible
			# example we cannot receive a V21 when in rs = 1, rmt eqns forbid it
			$this_impossible = 1;
		    } else {
			# it is a local event but could be a local response
			if($l_events_hash->{$op}->{type} eq "response"){
			    # a local event but it is a local response
			    # op + lcl response not  defined in  seed/remote eqns so it should not be possible
			    # ex When a read req is made, we cannot get a write response
			    if(!defined $pre_def_lcl_acns->{$hs}->{$op}){
				$this_impossible = 1;
			    }
			}
		    }
		    $rs_ns = $rs;
		    $rmt_action = "-:-";
		    if($this_impossible){
			&print_state_transition("rs_ns", $rs, $op, $rs_ns, $rmt_action, "Impossible: not in rmt eqns  or unexpected local resp");
		    } else {
			&print_state_transition("rs_ns", $rs, $op, $rs_ns, $rmt_action, "other");
		    }
		}

		# Effective remote state, remote next state 
		my $eff_rs_ns = &get_effective_state($rs_ns);
		my $eff_rs = &get_effective_state($rs);

		# Home state 
		if(defined $seed_h->{$rs}->{$op}){
		    # remote state + local request dictates next home state and local action
		    # unless a local read/write is already in progress
		    # RS is stable state, $op is a local request
		    if(defined $pre_def_lcl_acns->{$hs}){
			# home is in middle of read/write and receives a local request, stall it
			$hs_ns = $hs;
			$lcl_action = "-:-";
			$this_stall = 1;
			&print_state_transition("hs_ns", $hs, $op, $hs_ns, $lcl_action, "Stall: recd local req when home is in middle of read/write");
		    } else {
			$hs_ns = $seed_h->{$rs}->{$op}->{"hs_ns"};
			$lcl_action = $seed_h->{$rs}->{$op}->{"h_action"};
			&print_state_transition("hs_ns", $hs, $op, $hs_ns, $lcl_action, "Seed");
		    }
		} else {
		    #RS is intermediate or $op is not a local request
		    if(defined $l_actions_hash->{$hs}){
			#HS is intermediate (1pR, 1pC, 1pW, 1pCI)
			if (defined $l_actions_hash->{$hs}->{$eff_rs_ns}) {
			    #HS is intermediate and RS is in effective state this cycle to take action
			    $hs_ns = $l_actions_hash->{$hs}->{$eff_rs_ns}->{"next_state"};
			    $lcl_action = $l_actions_hash->{$hs}->{$eff_rs_ns}->{"action"};
			    &print_state_transition("hs_ns", $hs, $op, $hs_ns, $lcl_action, "effective state");
			} else {
			    #HS is intermediate and RS is not in effective state to take action
			    # stall if local request
			    if(defined $l_events_hash->{$op}){
				if($l_events_hash->{$op}->{type} eq "request"){
				    # local request, stall
				    $this_stall = 1;
				}
			    }
			    $hs_ns = $hs;
			    $lcl_action = "-:-";
			    if($this_stall){
				&print_state_transition("hs_ns", $hs, $op, $hs_ns, $lcl_action, "stalled local req");
			    } else {
				&print_state_transition("hs_ns", $hs, $op, $hs_ns, $lcl_action, "recd. lcl resp, no action");
			    }
			}
		    }  else {
			#HS is (1/1pLRA/1pLWA), RS is intermediate
			if(defined $l_events_hash->{$op}){
			    # local event received 
			    # check if local event is a request / response 
			    my $is_lcl_req = 0;
			    if($l_events_hash->{$op}->{type} eq "request"){
				$is_lcl_req = 1;
			    }

			    if($is_lcl_req){
				# received a local request, $op is a local request 
				# local req can be stalled/allowed based on effective remote state  when home is not in the  middle of read/write
				if(defined  $pre_def_lcl_acns->{$hs}){
				    # home is in middle of read/write so stall local request
				    # hs - 1pLRA, 1pLWA
				    $hs_ns = $hs;
				    $lcl_action = "-:-";
				    $this_stall = 1;
				    &print_state_transition("hs_ns", $hs, $op, $hs_ns, $lcl_action, "Stall: recd local req when home is in the middle of rd/wr");
				} else {
				    # recieved local request when home is not in the middle of rd/wr (hs = 1)
				    # allow local request based on effective remote state
				    if(defined $l_evt_access_ers->{$op}->{$eff_rs}){
					# local event can happen, use seed equation to determine what is the action and next state
					$hs_ns = $seed_h->{$eff_rs}->{$op}->{"hs_ns"};
					$lcl_action = $seed_h->{$eff_rs}->{$op}->{"h_action"};
					&print_state_transition("hs_ns", $hs, $op, $hs_ns, $lcl_action, "lcl req access matches effective remote state $eff_rs");
				    } else {
					#RS is not in effective state to take action so stall local reqs
					$this_stall = 1;
					$hs_ns = $hs;
					$lcl_action = "-:-";
					&print_state_transition("hs_ns", $hs, $op, $hs_ns, $lcl_action, "stalled remote not ready $eff_rs");
				    }
				}
			    } else {
				# hs can be 1/1pLRA/1pLWA
				# received a local  response
				# Ack for RDD or WDD (RDDA/WDDA)
				if(defined $pre_def_lcl_acns->{$hs}){
				    # home is waiting for response for read or write (1pLRA/1pLWA)
				    # $op can be RDDA/WDDA, when waiting for read response we cannot get a write response
				    if(defined $pre_def_lcl_acns->{$hs}->{$op}){
					#$op is acceptable response for current state
					# ie a read response is recd when waiting for read resp
					$hs_ns = $pre_def_lcl_acns->{$hs}->{$op}->{"next_state"};
					$lcl_action = $pre_def_lcl_acns->{$hs}->{$op}->{"action"};
					&print_state_transition("hs_ns", $hs, $op, $hs_ns, $lcl_action, "expected local response recd");
				    } else {
					# $op is not an acceptable response for current state
					# ie a wr resp is recd when waiting for rd resp which is not possible
					$hs_ns = $hs;
					$lcl_action = "-:-";
					$this_impossible = 1;
					&print_state_transition("hs_ns", $hs, $op, $hs_ns, $lcl_action, "Impossible: local resp does not match expected resp");
				    }
				} else {
				    # hs is 1 cannot receive a local response, impossible
				    # can receieve local response 
				    $hs_ns = $hs;
				    $lcl_action = "-:-";
				    &print_state_transition("hs_ns", $hs, $op, $hs_ns, $lcl_action, "Impossible: cannot receive local response when no  local requests aare issued");
				}
			    }
			} else {
			    # did not receive a local event + remote does not affect local state
			    # do nothing
			    $hs_ns = $hs;
			    $lcl_action = "-:-";
			    &print_state_transition("hs_ns", $hs, $op, $hs_ns, $lcl_action, "do nothing");
			}
		    }
		}
		

		&print_combine_transition($hs, $rs, $op, $hs_ns, $rs_ns, $lcl_action, $rmt_action, $this_impossible, $this_stall);
		if(!$this_stall & !$this_impossible){
		    if(! defined $allowed_states->{$hs_ns}->{$rs_ns}){
			$allowed_states->{$hs_ns}->{$rs_ns} = 0;
		    }
		}
		if($this_impossible){
		    $csv_line = $csv_line . "X, ";
		} elsif($this_stall){
		    $csv_line = $csv_line . "$global_stall, ";
		} else {
		    $csv_line = $csv_line . "$hs_ns:$rs_ns;$rmt_action;$lcl_action, ";
		}
	    } # loop through all events
	    # completed this set of HS and RS
	    $allowed_states->{$hs}->{$rs} = 1;
	    #getc();
	    &print_to_csv($fh, $csv_line);
	} else {
	    last;
	}
    }
    close($fh);
    return;
}



sub get_lcl_evt_access_ers {
    # get the effective remote states (ers)
    # allowed for each local event 
    my $l_actions_hash = shift;
    my $op_access_hash = shift;
    foreach my $inter_hs (keys %{$l_actions_hash}){
	foreach my $eff_rs (keys %{$l_actions_hash->{$inter_hs}}){
	    my $event = $l_actions_hash->{$inter_hs}->{$eff_rs}->{"event"};
	    $op_access_hash->{$event}->{$eff_rs} = 1;
	}
    }
    return;
}

sub hs_rs_not_completed{
    # check if this hs, rs combination has been evaluated for all states 
    my $allowed_states = shift;
    foreach my $hs (sort keys %{$allowed_states}){
	foreach my $rs (sort keys %{$allowed_states->{$hs}}){
	    if($allowed_states->{$hs}->{$rs} == 0){
		return(1, $hs, $rs);
	    }
	}
    }
    return 0;
}

sub get_effective_rmt_states{
    # get a hash of effective remote states for all remote intermediate states 
    my $states_hash = shift;
    my $eff_rs = shift;
    foreach my $rs (keys %{$states_hash}){
	$eff_rs->{$rs} = &get_effective_state($rs);
    }
    return;
}

#ugly need to find better way 
sub get_effective_state{
    # Identify the effective remote state 
    my $rmt_state = shift;
    chomp $rmt_state;
    if($rmt_state =~ /_(\S+)d/){
	#waiting for data so effective state is 3
	#takes highest priority 
	return "3";
    } elsif($rmt_state =~ /_(A|V)3/){
	#_V31, _A31 _V32, _A32 are effectively in state 3
	return "3";
    } elsif($rmt_state =~ /WDDA/){
	# waitting for write acknowledge, effective state is  3
	return "3";
    } elsif($rmt_state =~ /_A(\d)(\d)/){
	# _A21 means going from 2 to 1 upon receiving A21
	# state is 2 till A21 is received
	return $1;
    } elsif($rmt_state =~ /^\d+$/){
	return $rmt_state;
    } elsif($rmt_state =~ /^(\d+)_/){
	return $1;
    } else {
	print "WARN: Unable to find effective state for $rmt_state\n";
	#exit;
	return $rmt_state;
    }
    return;
}

sub read_seed_eqns {
    # Read the seed equations and generate a hash
    # HS_NS, home action = F(RS, lcl_req)

    # ex $VAR1 = {
    #      '2 (remote state) ' => {
    #               'LW (local req) ' => {
    #                         'hs_ns' => '1pW_A21',
    #                         'h_action' => '-:Send F21'
    #                       }};
    
    my @s_eqns = @{$_[0]};
    my $seed_hash = $_[1];
    foreach my $se (@s_eqns){
	my @se_elts = split(/\s*,\s*/,$se);
	my $rs = $se_elts[0];
	my $lcl_req = $se_elts[1];
	my $hs_ns = $se_elts[2];
	my $hs_action = $se_elts[3];
	my $rs_ns = $se_elts[4];
	$seed_hash->{$rs}->{$lcl_req}->{hs_ns} = $hs_ns;
	$seed_hash->{$rs}->{$lcl_req}->{h_action} = $hs_action;
	$seed_hash->{$rs}->{$lcl_req}->{rs_ns} = $rs_ns;
    }
    return;
}

sub read_evt_types{
    # read remote events, types and associated actiions
    # 'A32d' => {
    #                   'action' => 'WDD:-',
    #                   'type' => 'ack_resp'
    #                 },
    my @evts = @{$_[0]};
    my $evt_hash = $_[1];
    foreach my $re (@evts){
	my @relts = split(/\s*,\s*/, $re);
	my $req = $relts[0];
	my $type = $relts[1];
	my $act  = $relts[2];
	$evt_hash->{$req}->{action} = $act;
	$evt_hash->{$req}->{type} = $type;
    }
    return;
}

sub read_lcl_actions{
    # create a hash that has expected local actions
    # under effective remote states 
    # '1pW' => {
    #                  '1' => {
    #                           'action' => 'WDD:Send LWA',
    #                           'next_state' => '1',
    #                           'event => 'LW'
    #                         }
    my @lcl_list = @{$_[0]};
    my $l_events_hash = $_[1];
    my $l_actions_hash = $_[2];
    
    # format -     "1pR, 2, 1, LR",
    foreach my $line (@lcl_list){
	chomp $line;
	my @lelts = split(/\s*,\s*/, $line);
	my $inter_hs	= $lelts[0];
	my $eff_rs	= $lelts[1];
	my $hs_final	= $lelts[2];
	my $evt         = $lelts[3];
	my $action  = "";
	$l_actions_hash->{$inter_hs}->{$eff_rs}->{"next_state"} = $hs_final;
	$action = $l_events_hash->{$evt}->{"action"};
	if($action eq ""){
	    print "Unable to find action for event $evt\n";
	    exit;
	}
	$l_actions_hash->{$inter_hs}->{$eff_rs}->{"action"} = $action;
	$l_actions_hash->{$inter_hs}->{$eff_rs}->{"event"} = $evt;
    }
    return;
}

sub read_state_eqns {
    # Get all permutations of state events
    # to calculate the states hash 
    my @eqns = @{$_[0]};
    my $states_hash = $_[1];
    my $actions_hash = $_[2];;
    
    foreach my $equation (@eqns){
	chomp $equation;
	my @eq_elts = split(/\s*,\s*/,$equation);
	print_eqn(@eq_elts);
	if($#eq_elts == 2){
	    # no permutation of messages necessary
	    # only 1 possible way 
	    &fill_state_n($actions_hash, $states_hash, \@eq_elts);
	} else {
	    # many permutations of responses possible
	    # generating equations for all possible permutations
	    my @num_perms;
	    for(my $i=0; $i <= $#eq_elts-2; $i++){
		$num_perms[$i] = $i+1;
	    }

	    my @eqn_perm;
	    foreach (&permute(@num_perms) ){
		# Adding elements from eq_elts to permutation array 
		$eqn_perm[0] = $eq_elts[0];
		for(my $i=0; $i<=$#num_perms; $i++){
		    # only need to permute teh responses
		    # and not the initiial and final states 
		    $eqn_perm[$i+1] = $eq_elts[@$_[$i]];
		}
		$eqn_perm[$#eq_elts] = $eq_elts[$#eq_elts];

		my $filter_this = &filter_this_permutation(\@eqn_perm);
		#eqn_perm  now has current permutation of responses
		#solving it unless it is filtered
		if( ! $filter_this){
		    &fill_state_n($actions_hash, $states_hash, \@eqn_perm);
		}
	    }
	}
    }
}


sub filter_this_permutation{
    my @eq_elts = @{$_[0]};
    my $filter_this = 0;

    # Filter if WDDA comes before V31d. V32d, A31d, A32d in the list 
    for( my $i=1; $i < $#eq_elts; $i++){
	my $e = $eq_elts[$i];;
	if($e =~ /V31d/){
	    $filter_this = 1;
	} elsif ($e =~ /V32d/){
	    $filter_this = 1;
	} elsif ($e =~ /A31d/){
	    $filter_this = 1;
	} elsif ($e =~ /A32d/){
	    $filter_this = 1;
	} else {
	    $filter_this = $filter_this;
	}
	
	if($e =~ /WDDA/){
	    $filter_this = 0;
	}
    }

    if($filter_this){
	&pretty_print_eqn(\@eq_elts, "Not possible, WDDA appears before requesting WDD, filtered out");
    }

    return $filter_this;
}

sub permute {
    # All possible permutations for given numbers
    return ([]) unless (@_);
    return map {
		 my @cdr = @_;
		 my $car = splice @cdr, $_, 1;
		 map { [$car, @$_]; } &permute(@cdr);
	       } 0 .. $#_;
}

sub fill_state_n {
    # equation solve 
    my $actions_hash = $_[0];
    my $states_hash = $_[1];
    my @eq_elts = @{$_[2]};
    
    &pretty_print_eqn(\@eq_elts);

    my $curr_state  = $eq_elts[0];
    my $resp1       = $eq_elts[1];
    my $final_state = $eq_elts[$#eq_elts];
    
    if($#eq_elts == 2){
	# only 1 response 
	&fill_state_1($actions_hash, $states_hash, $curr_state, $resp1, $final_state);
    } else {
	my $next_state;
	my $action;
	my @eq_sub_elts;

	if(defined $states_hash->{$curr_state}->{$resp1}->{"next_state"}){
	    # Previously decoded the intermediate state for
	    # curr_state receving resp1
	    $next_state = $states_hash->{$curr_state}->{$resp1}->{"next_state"};
	    $action = $states_hash->{$curr_state}->{$resp1}->{"action"};
	    &print_transition($states_hash, $curr_state, $resp1, "known");
	    if( $next_state eq $global_stall){
		# curr_state + resp = stall
		# no need to delve deeper into this equation 
		return;
	    }
	    
	} else {
	    # curr_state + resp1 not encountered previoulsy
	    # if resp1 is a request, stall as there are responses expected to arrive
	    # if resp1 iss not a request, create a new state
	    &print_transition($states_hash, $curr_state, $resp1, "?");

	    # stall if encountering a request before receiving all
	    # expected responses
	    if( (defined $actions_hash->{$resp1}->{type}) && 
		($actions_hash->{$resp1}->{type} eq "request")){
		# encountered a request, stall
		# no need to dig deeper into the equation
		$states_hash->{$curr_state}->{$resp1}->{"next_state"} = $global_stall;
		$states_hash->{$curr_state}->{$resp1}->{"action"} = "-:-";
		&print_transition($states_hash, $curr_state, $resp1, "req recd when resp in transit");
		return;
		
	    } else {
		# Encountered a response not a request
		# create a new state and add it to the list
		# Action :
		#   For home depends on final home state transitioning to
		#   For remote depends on response received in ECI
		$next_state = &create_new_intermediate_state(\@eq_elts);

		# remote action depends on response received
		if(! defined $actions_hash->{$resp1}){
		    $action = "-:-";
		} else {
		    $action = $actions_hash->{$resp1}->{"action"};
		}
		# a new remote state created can potentially affect the home state
	    }
	    $states_hash->{$curr_state}->{$resp1}->{"next_state"} = $next_state;
	    $states_hash->{$curr_state}->{$resp1}->{"action"} = $action;
	    &print_transition($states_hash, $curr_state, $resp1, "create");
	}
	# write the created new state into the hash
	# no need to check for overwrites since it is a brand new state 
	#$states_hash->{$curr_state}->{$resp1}->{"next_state"} = $next_state;
	#$states_hash->{$curr_state}->{$resp1}->{"action"} = $action;
	#&print_transition($states_hash, $curr_state, $resp1, "");
	# Now we have reduced the number of responses 
	# (substituted known states or created new states)
	# solving further
	$eq_sub_elts[0] = $next_state;
	for(my $i=2; $i<=$#eq_elts; $i++){
	    $eq_sub_elts[$i-1] = $eq_elts[$i];
	}
	fill_state_n($actions_hash, $states_hash, \@eq_sub_elts);
    }
    return;
}


sub fill_state_1{
    # fill states for equations with 1 response
    # assuming all checks passed previously no error checking done here

    # Leanrs eqns of format 
    # curr_state + resp1 = final_state
    # actions for home depend on final state
    # actions for remote node depend on resp1
    my $rmt_events_hash = shift;
    my $states_hash = shift;
    my $curr_state = shift;
    my $resp1 = shift;
    my $final_state = shift;

    my $next_state;
    my $action;

    # Filling RS remote states
    $next_state = $final_state;
    $action = "-:-";

    if (defined $pre_def_rmt_acns->{$curr_state}->{$resp1}){
	# Action depend on present state, gets highest preceedence. 
	$action = $pre_def_rmt_acns->{$curr_state}->{$resp1};
    } elsif(defined $rmt_events_hash->{$resp1}){
	# Actions depend on response recieved
	$action = $rmt_events_hash->{$resp1}->{action};
    }
    
    &warn_overwrite($states_hash, $curr_state, $resp1, $next_state, $action);
    $states_hash->{$curr_state}->{$resp1}->{"next_state"} = $next_state;
    $states_hash->{$curr_state}->{$resp1}->{"action"} = $action;

    # For printing 
    &print_transition($states_hash, $curr_state, $resp1, "infer");
    #getc();
}

sub warn_overwrite {
    #States should not be overwritten with values different from
    #previously written values in states_hash
    my $states_hash = shift;
    my $state = shift;
    my $resp = shift;
    my $new_state = shift;
    my $action = shift;
    if(defined $states_hash->{$state}->{$resp}->{next_state}){
	my $orig_state = $states_hash->{$state}->{$resp}->{next_state};
	if($orig_state ne $new_state){
	    print "Warning: overwriting next state for $state, $resp from $orig_state to $new_state\n";
	    exit(1);
	}
    }
    if(defined $states_hash->{$state}->{$resp}->{"action"}){
	my $orig_action = $states_hash->{$state}->{$resp}->{"action"};
	if($orig_action ne $action){
	    print "Warning: overwriting action for $state, $resp from $orig_action to $action\n";
	    exit(1);
	}
	
    }
}

sub create_new_intermediate_state {
    # Create intermediate state for a 
    # state + resp = unknown    
    my @elts = @{$_[0]};
    my $new_is = "$elts[$#elts]"."_";
    for(my $i=2; $i<$#elts; $i++){
	$new_is = $new_is . "$elts[$i]" . "_";
    }
    $new_is =~ s/_+$//;
    return $new_is;
}

#--------- Print Functions -----------
sub print_transition{
    #print transition message 
    my $states_hash = shift;
    my $curr_state = shift;
    my $resp1 = shift;
    my $how = shift;

    return if (! $debug);
    
    my $next_state = $states_hash->{$curr_state}->{$resp1}->{"next_state"};
    my $action = $states_hash->{$curr_state}->{$resp1}->{"action"};
    if(defined $next_state){
	print("\t$curr_state + $resp1 = $next_state ($how); ");
	if($next_state eq $global_stall){
	    print "\n";
	} else {
	    print("Action: $action\n");
	    if(defined $states_hash->{$curr_state}->{$resp1}->{"next_home_state"}){
		my $hs_ns = $states_hash->{$curr_state}->{$resp1}->{"next_home_state"};
		my $h_action = $states_hash->{$curr_state}->{$resp1}->{"home_action"};
		print "\t Transitiion home to $hs_ns";
		print "Home action: $h_action\n";
	    }
	}
    } else {
	print "\t$curr_state + $resp1 = ?\n";
    }
    return;
}

sub print_state_transition {
    my $pre = shift;
    my $st = shift;
    my $op = shift;
    my $ns = shift;
    my $act = shift;
    my $how = shift;
    return if(!$debug);
    print "$pre: $st + $op = $ns $act ($how)\n";
}

sub print_combine_transition{
    my $hs = shift;
    my $rs = shift;
    my $op = shift;
    my $hs_ns = shift;
    my $rs_ns = shift;
    my $laction = shift;
    my $raction = shift;
    my $this_impossible = shift;
    my $this_stall = shift;
    return if(!$debug);
    if($this_impossible){
	print "$hs:$rs + $op = X\n";
    } elsif( $this_stall){
	print "$hs:$rs + $op = $global_stall $raction $laction\n";
    } else {
	print "$hs:$rs + $op = $hs_ns:$rs_ns $raction $laction\n";
    }
    return;
}

sub pretty_print_eqn{
    my @eq_elts = @{$_[0]};
    my $opt_text =  $_[1];
    $opt_text = "" if(! defined $opt_text);;
    my $line = "";
    return if(!$debug);
    for(my $i=0; $i < $#eq_elts-1; $i++){
	$line = $line."(";
    }
    $line = $line. $eq_elts[0];
    for(my $i=1; $i < $#eq_elts; $i++){
	$line = $line." + $eq_elts[$i])";
    }
    $line = $line. " = $eq_elts[$#eq_elts]\n";
    print $line;
    print "\t $opt_text\n" if($opt_text ne "");
    return;
}

sub print_eqn{
    my @eq_elts = @_;
    return if (!$debug);
    my $line = "Eqn: ";
    foreach my $e (@eq_elts){
	$line = $line . "$e + ";
    }
    $line =~ s/\s*\+\s*$//;
    $line =~ s/\s*\+\s*(\S+)$/ = $1/;
    print "\n$line\n";
    print "-------------------------\n";
}

sub print_to_csv {
    #print to file handle 
    my $fh = shift;
    my $line = shift;
    $line =~ s/\,\s*$//;
    print $fh "$line\n";
    return;
}
