/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-08-25
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */

`ifndef SEQ_READ_LOAD_GEN_SV
`define SEQ_READ_LOAD_GEN_SV

// Given a starting CL index, number of CLs and stride,
// module issues that many RLDX to the DirC.
// It counts the responses and calculates throughput.
// if DirC sends forward downgrade, it sends an ack with data. (A31d).

import eci_cmd_defs::*;

module seq_read_load_gen #
  (
   // parameter to display throughput
   // values, prints tput after PRINT_EVERY_X_TR
   // responses received.
   parameter PRINT_EVERY_X_TR = 1000
   )
   (
    input logic 					   clk,
    input logic 					   reset,

    input logic [ECI_CL_INDEX_WIDTH-1:0] 		   st_cl_idx_i,
    input logic [63:0] 					   num_cls_i,
    input logic [63:0] 					   cl_idx_stride_i,
    input logic 					   tput_latency_bar_i,
    input logic 					   en_i,
   
    // Output ECI events to dirc.
    // ECI packet for request without data. (VC 6 or 7) (only header).
    output logic [ECI_WORD_WIDTH-1:0] 			   req_wod_hdr_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   req_wod_pkt_size_o,
    output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 		   req_wod_pkt_vc_o,
    output logic 					   req_wod_pkt_valid_o,
    input logic 					   req_wod_pkt_ready_i,

    // ECI packet for response without data.(VC 10 or 11). (only header).
    output logic [ECI_WORD_WIDTH-1:0] 			   rsp_wod_hdr_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   rsp_wod_pkt_size_o,
    output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 		   rsp_wod_pkt_vc_o,
    output logic 					   rsp_wod_pkt_valid_o,
    input logic 					   rsp_wod_pkt_ready_i,

    // ECI packet for response with data. (VC 4 or 5). (header + data).
    output logic [ECI_PACKET_SIZE-1:0][ECI_WORD_WIDTH-1:0] rsp_wd_pkt_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   rsp_wd_pkt_size_o,
    output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 		   rsp_wd_pkt_vc_o,
    output logic 					   rsp_wd_pkt_valid_o,
    input logic 					   rsp_wd_pkt_ready_i,

    // Input events from dirc. 
    // FWD - VC 8,9
    input logic [ECI_WORD_WIDTH-1:0] 			   fwd_wod_hdr_i,
    input logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   fwd_wod_pkt_size_i,
    input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 		   fwd_wod_pkt_vc_i,
    input logic 					   fwd_wod_pkt_valid_i,
    output logic 					   fwd_wod_pkt_ready_o,

    // VC 10,11
    input logic [ECI_WORD_WIDTH-1:0] 			   rsp_wod_hdr_i,
    input logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   rsp_wod_pkt_size_i,
    input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 		   rsp_wod_pkt_vc_i,
    input logic 					   rsp_wod_pkt_valid_i,
    output logic 					   rsp_wod_pkt_ready_o,

    // VC 5,4
    input logic [ECI_PACKET_SIZE-1:0][ECI_WORD_WIDTH-1:0]  rsp_wd_pkt_i,
    input logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   rsp_wd_pkt_size_i,
    input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 		   rsp_wd_pkt_vc_i,
    input logic 					   rsp_wd_pkt_valid_i,
    output logic 					   rsp_wd_pkt_ready_o,

    // Performance counters.
    output logic [63:0] 				   tput_timer_o,
    output logic [63:0] 				   num_rsp_recd_o
    );

   
   enum  {IDLE, RUN, DONE} state_reg, state_next;
   logic [ECI_CL_INDEX_WIDTH-1:0] cl_idx_reg = '0, cl_idx_next;
   logic [63:0] 		  num_cls_reg = '0, num_cls_next;
   logic [63:0] 		  cl_idx_stride_reg = '0, cl_idx_stride_next;
   logic 			  tput_latency_bar_reg = '0, tput_latency_bar_next;
   logic [63:0] 		  num_req_sent_reg = '0, num_req_sent_next;
   logic [63:0] 		  num_rsp_recd_reg = '0, num_rsp_recd_next;
   
   // timers.
   logic [63:0] 		  tput_timer_reg = '0, tput_timer_next;
   logic 			  tput_timer_en_reg = '0, tput_timer_en_next;

   real 			  clock_period_ns = 3.106;
   real 			  tput_value_GBps = 0;
   
   
   eci_word_t curr_rldx_req;
   eci_cl_addr_t curr_cl_addr;
   eci_word_t fwd_wod_hdr_c;
   

   // assign outputs.
   assign req_wod_hdr_o = curr_rldx_req.eci_word;
   assign req_wod_pkt_size_o = 'd1;
   assign req_wod_pkt_vc_o = VC_REQ_WO_DATA_E;
   assign tput_timer_o = tput_timer_reg;
   assign num_rsp_recd_o = num_rsp_recd_reg;
   assign fwd_wod_hdr_c = eci_word_t'(fwd_wod_hdr_i);
   
   
   // Generate RLDX based on current address.
   always_comb begin : CURR_RLDX
      curr_cl_addr.parts.cl_index = cl_idx_reg;
      curr_cl_addr.parts.byte_offset = '0;
      curr_rldx_req = gen_req_wo_data(
				      .rreq_id_i('0),
				      .dmask_i('1),
				      .addr_i(curr_cl_addr.flat),
				      .rldt_i(1'b0),
				      .rldi_i(1'b0),
				      .rldd_i(1'b0),
				      .rc2d_s_i(1'b0),
				      .rldx_i(1'b1)
				      );
      
   end : CURR_RLDX

   // state machine to continuously issue RLDX.
   always_comb begin : CONTROLLER
      state_next                = state_reg;
      cl_idx_next		= cl_idx_reg;
      num_cls_next		= num_cls_reg;
      cl_idx_stride_next	= cl_idx_stride_reg;
      tput_latency_bar_next	= tput_latency_bar_reg;
      num_req_sent_next		= num_req_sent_reg;
      num_rsp_recd_next		= num_rsp_recd_reg;
      tput_timer_next		= tput_timer_reg;
      tput_timer_en_next = tput_timer_en_reg;

      req_wod_pkt_valid_o = 1'b0;
      rsp_wd_pkt_ready_o = 1'b0;
      rsp_wod_pkt_ready_o = 1'b0;
      
      case(state_reg)
	IDLE: begin
	   if(en_i) begin
	      state_next = RUN;
	      cl_idx_next = st_cl_idx_i;
	      num_cls_next = num_cls_i;
	      cl_idx_stride_next = cl_idx_stride_i;
	      tput_latency_bar_next = tput_latency_bar_i;
	      num_req_sent_next = '0;
	   end
	end
	RUN: begin
	   req_wod_pkt_valid_o = 1'b1;
	   rsp_wd_pkt_ready_o = 1'b1;
	   if(req_wod_pkt_valid_o & req_wod_pkt_ready_i) begin
	      cl_idx_next = cl_idx_reg + cl_idx_stride_i;
	      num_req_sent_next = num_req_sent_reg + 1;
	   end
	   if(num_req_sent_next == num_cls_reg) begin
	      state_next = DONE;
	   end
	end
	DONE: begin
	   // system has to be reset to start again.
	   rsp_wd_pkt_ready_o = 1'b1;
	   if(num_rsp_recd_reg == num_cls_reg) begin
	      rsp_wd_pkt_ready_o = 1'b0;
	   end
	   state_next = state_reg;
	end
      endcase

      // whenever the first response is recd, start the tput timer.
      // stop it when the number of responses recd matches the number of resp expected. 
      if(rsp_wd_pkt_valid_i & rsp_wd_pkt_ready_o) begin
	 num_rsp_recd_next = num_rsp_recd_reg + 1;
	 if(tput_timer_en_reg == 1'b0) begin
	    // First response recd, start timer. 
	    tput_timer_en_next = 1'b1;
	 end
	 if(num_rsp_recd_next ==  num_cls_reg) begin
	    tput_timer_en_next = 1'b0;
	 end
      end

      if((tput_timer_en_reg == 1'b1) | (tput_timer_en_next == 1'b1)) begin
	 tput_timer_next = tput_timer_reg + 1;
      end
   end : CONTROLLER

   // Send A31d for F31.
   always_comb begin : SEND_FWD_ACK
      // A31d header. 
      rsp_wd_pkt_o[0] = gen_hak_hdr(
				    .fwd_req_i(fwd_wod_hdr_c),
				    .vicdhi_i(1'b1),
				    .vicdhi_n_i(1'b0),
				    .hakd_i(1'b0),
				    .hakd_n_i(1'b0),
				    .hakn_s_i(1'b0),
				    .haki_i(1'b0),
				    .haks_i(1'b0),
				    .hakv_i(1'b0)
				    );
      // sending same dirty data back for every downgrade request. 
      rsp_wd_pkt_o[ECI_PACKET_SIZE-1:1] = 'd1024;
      rsp_wd_pkt_size_o = get_pkt_size_from_dmask(.my_dmask_i(fwd_wod_hdr_c.mfwd_generic.dmask));
      if(fwd_wod_hdr_c.mfwd_generic.address[ECI_CL_ADDR_LSB] == CL_IDX_ODD) begin
	 rsp_wd_pkt_vc_o = eci_vc_size_t'(VC_RESP_W_DATA_E);
      end else begin
	 rsp_wd_pkt_vc_o = eci_vc_size_t'(VC_RESP_W_DATA_O);
      end
      rsp_wd_pkt_valid_o = fwd_wod_pkt_valid_i;
      fwd_wod_pkt_ready_o = rsp_wd_pkt_ready_i;
   end : SEND_FWD_ACK
   
   always_ff @(posedge clk) begin : REG_ASSIGN
      state_reg <= state_next;
      cl_idx_reg			<= cl_idx_next;
      num_cls_reg		<= num_cls_next;
      cl_idx_stride_reg		<= cl_idx_stride_next;
      tput_latency_bar_reg	<= tput_latency_bar_next;
      num_req_sent_reg		<= num_req_sent_next;
      num_rsp_recd_reg		<= num_rsp_recd_next;
      tput_timer_reg		<= tput_timer_next;
      tput_timer_en_reg <= tput_timer_en_next;
      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset
	 state_reg <= IDLE;
	 cl_idx_reg		<= '0;
	 num_cls_reg		<= '0;
	 cl_idx_stride_reg	<= '0;
	 tput_latency_bar_reg <= '0;
	 num_req_sent_reg	<= '0;
	 num_rsp_recd_reg	<= '0;
	 tput_timer_reg	<= '0;
	 tput_timer_en_reg <= '0;
      end
   end : REG_ASSIGN

   always @(num_rsp_recd_o) begin
      if((num_rsp_recd_o % PRINT_EVERY_X_TR) == 0) begin
	 tput_value_GBps = real'(num_rsp_recd_o * 128) / (real'(tput_timer_o) * clock_period_ns);
	 $display("Clock period  = %g ns", clock_period_ns);
	 $display("Clock elapsed = %0d", tput_timer_o);
	 $display("Num resp recd = %0d", num_rsp_recd_o);
	 $display("Tput          = %g GBytes/s", tput_value_GBps);
      end
   end
   
   //---------------------------------------------------------------
   // Functions to generate requests.
   // RLDT, RLDD, RLDI, RC2D_S - Req wo data.
   function automatic eci_word_t gen_req_wo_data
     (
      input 	  eci_id_t rreq_id_i,
      input 	  eci_dmask_t dmask_i,
      input 	  eci_address_t addr_i,
      input logic rldt_i,
      input logic rldi_i,
      input logic rldd_i,
      input logic rc2d_s_i,
      input logic rldx_i
      );
      eci_word_t my_hdr;
      my_hdr.eci_word = '0;
      if(rldt_i) begin
	 my_hdr.mreq_load.opcode = ECI_CMD_MREQ_RLDT;
      end
      if(rldi_i) begin
	 my_hdr.mreq_load.opcode = ECI_CMD_MREQ_RLDI;
      end      
      if(rldd_i) begin
	 my_hdr.mreq_load.opcode = ECI_CMD_MREQ_RLDD;
      end
      if(rc2d_s_i) begin
	 my_hdr.mreq_load.opcode = ECI_CMD_MREQ_RC2D_S;
      end
      if(rldx_i) begin
	 my_hdr.mreq_load.opcode = ECI_CMD_MREQ_RLDX;
      end
      my_hdr.mreq_load.rreq_id = rreq_id_i;
      my_hdr.mreq_load.dmask = dmask_i;
      my_hdr.mreq_load.address = addr_i;
      return(my_hdr);
   endfunction : gen_req_wo_data

   
   function automatic eci_word_t gen_hak_hdr
     (
      input 	  eci_word_t fwd_req_i,
      input logic vicdhi_i,
      input logic vicdhi_n_i,
      input logic hakd_i,
      input logic hakd_n_i,
      input logic hakn_s_i,
      input logic haki_i,
      input logic haks_i,
      input logic hakv_i
      );
      eci_word_t my_hdr;
      my_hdr.eci_word = '0;
      if(vicdhi_i | vicdhi_n_i) begin
	 my_hdr.mrsp_3to8.opcode = ECI_CMD_MRSP_VICDHI;
      end
      if(hakd_i | hakd_n_i) begin
	 my_hdr.mrsp_3to8.opcode = ECI_CMD_MRSP_HAKD;
      end
      if(hakn_s_i) begin
	 my_hdr.mrsp_3to8.opcode = ECI_CMD_MRSP_HAKN_S;
      end
      if(haki_i) begin
	 my_hdr.mrsp_3to8.opcode = ECI_CMD_MRSP_HAKI;
      end
      if(haks_i) begin
	 my_hdr.mrsp_3to8.opcode = ECI_CMD_MRSP_HAKS;
      end
      if(hakv_i) begin
	 my_hdr.mrsp_3to8.opcode = ECI_CMD_MRSP_HAKV;
      end
      my_hdr.mrsp_3to8.hreq_id = fwd_req_i.mfwd_generic.hreq_id;
      my_hdr.mrsp_3to8.dmask = fwd_req_i.mfwd_generic.dmask;
      if(vicdhi_n_i | hakd_n_i | hakn_s_i | haks_i | hakv_i | haki_i) begin
	 // A31, A21, A32, A22, A11, A11 - no data 
	 my_hdr.mrsp_3to8.dmask = '0;
      end
      my_hdr.mrsp_3to8.ns = fwd_req_i.mfwd_generic.ns;
      my_hdr.mrsp_3to8.address = fwd_req_i.mfwd_generic.address;
      return(my_hdr);
   endfunction : gen_hak_hdr
   
   
   function automatic eci_word_t gen_vic_hdr
     (
      input eci_address_t my_addr_i,
      input eci_dmask_t my_dmask_i,
      input vics_i, // V21
      input vicc_i, // V32d
      input vicd_i, // V31d
      input vicd_n_i, // V31
      input vicc_n_i // V32
      );
      eci_word_t my_hdr;
      my_hdr.eci_word = '0;
      if(vics_i) begin
	 my_hdr.mrsp_0to2.opcode = ECI_CMD_MRSP_VICS;
	 my_hdr.mrsp_0to2.dmask = '0;
      end
      if(vicd_n_i | vicd_i) begin
	 my_hdr.mrsp_0to2.opcode = ECI_CMD_MRSP_VICD;
	 if(vicd_n_i) begin
	    my_hdr.mrsp_0to2.dmask = '0;
	 end else begin
	    my_hdr.mrsp_0to2.dmask = my_dmask_i;
	 end
      end
      if(vicc_n_i | vicc_i) begin
	 my_hdr.mrsp_0to2.opcode = ECI_CMD_MRSP_VICC;
	 if(vicc_n_i) begin
	    my_hdr.mrsp_0to2.dmask = '0;
	 end else begin
	    my_hdr.mrsp_0to2.dmask = my_dmask_i;
	 end
      end
      my_hdr.mrsp_0to2.ns = 1'b1;
      my_hdr.mrsp_0to2.address = my_addr_i;
      return(my_hdr);
   endfunction : gen_vic_hdr

endmodule // seq_read_load_gen

`endif
