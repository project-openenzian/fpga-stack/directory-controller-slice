/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-07-27
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */

`ifndef DP_DATA_STORE_SV
`define DP_DATA_STORE_SV

/*
 * Module Description:
 * Store and Retrieve data corresponding to a DCU_IDX.
 * Each DCU_IDX in a DC_SLICE has a slot in a buffer to 
 * store and retrieve data corresponding to it. 
 * Whenever a data corresponding to DCU_IDX is retrieved,
 * the DCU_IDX is signaled to enable the gate keeping module 
 * to allow new transactions. 
 * 
 * The data width to be stored is half of CL size. 
 * The data width to be retrieved is of CL size. 
 *
 * Input Output Description:
 * Input Retrieve I/F.
 * * DCU_ID corresponding to DCU_IDX that needs to be 
 *   retrieved along with the header. 
 * 
 * Store I/F.
 * * write address to CL/2 bits of data.
 * * write CL/2 data. 
 * * valid ready flow control. 
 * 
 * Output hdr+data I/F.
 * * Hdr + retrieved data with valid ready flow control. 
 * * Retrieve CL bits of data at a time. 
 * 
 * Clear DCU_IDX I/F.
 * * DCU_IDX that was retrieved + valid signal. (always ready). 
 * 
 *
 * Architecture Description:
 *  Instantiates a buffer to read at granularity of CL and 
 *  write at a granularity of CL/2. 
 *
 * Modifiable Parameters:
 *  HDR_WIDTH - width of header information. 
 *  RD_DATA_WIDTH - width of data to be read. 
 *
 * Non-modifiable Parameters:
 * WR_DATA_WIDTH - always equal to RD_DATA_WIDTH/2.
 * 
 * Modules Used:
 *  dp_mem. 
 *
 * Notes:
 *  Reading is combinational. 
 *  Write takes 1 cycle. 
 *  Distributed memory. 
 *
 */

import eci_dcs_defs::*;

module dp_data_store #
  (
   parameter HDR_WIDTH = 64,
   parameter RD_DATA_WIDTH = 1024,
   parameter WR_DATA_WIDTH = RD_DATA_WIDTH/2
   )
   (
    input logic 			   clk,
    input logic 			   reset,
    // Retrieve req i/f.
    input logic [DS_DCU_ID_WIDTH-1:0] 	   rd_dcu_id_i,
    input logic [HDR_WIDTH-1:0] 	   rd_hdr_i,
    input logic 			   rd_valid_i,
    output logic 			   rd_ready_o,
    // Write to datapath mem i/f.
    input logic [DS_DBUF_WR_IDX_WIDTH-1:0] wr_addr_i,
    input logic [WR_DATA_WIDTH-1:0] 	   wr_data_i,
    input logic 			   wr_en_i,
    // Output retrieve result i/f.
    output logic [HDR_WIDTH-1:0] 	   ret_hdr_o,
    output logic [RD_DATA_WIDTH-1:0] 	   ret_data_o,
    output logic 			   ret_valid_o,
    input logic 			   ret_ready_i,
    // Clear signal when data is read.
    output logic [DS_DCU_IDX_WIDTH-1:0]    c_dcu_idx_o,
    output logic 			   c_dcu_idx_valid_o
    );

   // Memory is read using dcu_idx which is derived
   // from dcu_id.
   // Each memory write takes 1 cycle and only half
   // of data that will eventually be read is written
   // in a cycle. So 2 cycles total before reading
   // can be done.
   // Once retrieved data handshake happens, a clear
   // signal is issued to the gate to free up dcu_idx.
   
   logic [DS_DCU_IDX_WIDTH-1:0] 	   rd_dcu_idx;
   
   // Memory signals.
   logic [DS_DBUF_WR_IDX_WIDTH-1:0] mem_wr_addr_i;
   logic [WR_DATA_WIDTH-1:0] 	    mem_wr_data_i;
   logic 			    mem_wr_en_i;
   logic [DS_DBUF_RD_IDX_WIDTH-1:0] mem_rd_addr_i;
   logic 			    mem_rd_en_i;
   logic [RD_DATA_WIDTH-1:0] 	    mem_rd_data_o;
   logic 			    ret_en;
   

   // Register outputs. 
   logic [HDR_WIDTH-1:0] 	    ret_hdr_reg		= '0, ret_hdr_next;
   logic 			    ret_valid_reg	= '0, ret_valid_next;
   logic [DS_DCU_IDX_WIDTH-1:0]     c_dcu_idx_reg	= '0, c_dcu_idx_next;
   logic 			    c_dcu_idx_valid_reg = '0, c_dcu_idx_valid_next;

   assign ret_en = ~ret_valid_o | ret_ready_i;
   
   // Output assign.
   assign rd_ready_o = ret_en;
   assign ret_hdr_o		= ret_hdr_reg;
   assign ret_data_o		= mem_rd_data_o;
   assign ret_valid_o		= ret_valid_reg;
   assign c_dcu_idx_o		= c_dcu_idx_reg;
   assign c_dcu_idx_valid_o	= ret_valid_o & ret_ready_i;

   initial begin
      if(WR_DATA_WIDTH !== RD_DATA_WIDTH/2) begin
	 $error("Error: WR_DATA_WIDTH should be half of RD_DATA_WIDTH (instance %m)");
	 $finish;
      end
      if(DS_DCU_IDX_WIDTH !== DS_DBUF_RD_IDX_WIDTH) begin
	 $error("Error: DS_DCU_IDX_WIDTH should be same as DS_DBUF_RD_IDX_WIDTH (instance %m)");
	 $finish;
      end
   end

   always_comb begin : RD_CONTROLLER
      ret_hdr_next = ret_hdr_reg;
      ret_valid_next = ret_valid_reg;
      c_dcu_idx_next = c_dcu_idx_reg;
      if(ret_en) begin
	 ret_hdr_next = rd_hdr_i;
	 ret_valid_next = rd_valid_i;
	 c_dcu_idx_next = rd_dcu_idx;
      end
   end : RD_CONTROLLER
   
   assign rd_dcu_idx = f_dcuid_2_dcuidx(.dcu_id_i(rd_dcu_id_i));

   // Read takes 1 cycle.
   // Write takes 1 cycle.
   always_comb begin : DP_MEM_IP_ASSIGN
      mem_wr_addr_i = wr_addr_i;
      mem_wr_data_i = wr_data_i;
      mem_wr_en_i = wr_en_i;
      mem_rd_addr_i = DS_DBUF_RD_IDX_WIDTH'(rd_dcu_idx);
      mem_rd_en_i = ret_en;
   end : DP_MEM_IP_ASSIGN
   dp_mem #
     (
      .WR_NUM_REGS(DS_DBUF_DEPTH),
      .RD_DATA_WIDTH(RD_DATA_WIDTH),
      .WR_DATA_WIDTH(WR_DATA_WIDTH)
       )
   dp_store
     (
      .clk(clk),
      .wr_addr_i(mem_wr_addr_i),
      .wr_data_i(mem_wr_data_i),
      .wr_en_i(mem_wr_en_i),
      .rd_addr_i(mem_rd_addr_i),
      .rd_en_i(mem_rd_en_i),
      .rd_data_o(mem_rd_data_o)
      );

   always_ff @(posedge clk) begin : REG_ASSIGN
      ret_hdr_reg		<= ret_hdr_next;
      ret_valid_reg		<= ret_valid_next;
      c_dcu_idx_reg		<= c_dcu_idx_next;
      c_dcu_idx_valid_reg	<= c_dcu_idx_valid_next;
      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset 
	 //ret_hdr_reg		<= '0;
	 ret_valid_reg	<= '0;
	 //c_dcu_idx_reg	<= '0;
	 c_dcu_idx_valid_reg <= '0;
      end
   end : REG_ASSIGN
endmodule // dp_data_store
`endif
