/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-01-27
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */

`ifndef AXIS_2_ROUTER_SV
`define AXIS_2_ROUTER_SV

module axis_2_router #
  (
   parameter DATA_WIDTH = 64
   )
   (
    input logic 		  clk,
    input logic 		  reset,
    // Input (upstream) channel.
    input logic [DATA_WIDTH-1:0]  us_data_i,
    // 1 selects down channel, 0 selects up channel. 
    input logic 		  us_sel_down_upbar_i,
    input logic 		  us_valid_i,
    output logic 		  us_ready_o,
    // Output (downstream) up channel.
    output logic [DATA_WIDTH-1:0] ds_up_data_o,
    output logic 		  ds_up_valid_o,
    input logic 		  ds_up_ready_i,
    // Output (downstream) down channel.
    output logic [DATA_WIDTH-1:0] ds_down_data_o,
    output logic 		  ds_down_valid_o,
    input logic 		  ds_down_ready_i
    );
   
   typedef struct packed {
      // format - logic abc;
      logic [DATA_WIDTH-1:0] data;
      logic 		     sel_down_upbar;
      logic 		     valid;
      logic 		     ready;
   } axis_t;

   axis_t pipe_stage_str_i, pipe_stage_str_o;
   logic  us_hs, ds_up_hs, ds_down_hs;
   
   // Output assign.
   // Since only one output channel will be valid at a time, duplicate the same set of signals
   // to both channels. 
   assign ds_up_data_o = pipe_stage_str_o.data;
   assign ds_down_data_o = pipe_stage_str_o.data;

   // Signals assign. 
   assign us_hs      = us_valid_i & us_ready_o;
   assign ds_up_hs   = ds_up_valid_o & ds_up_ready_i;
   assign ds_down_hs = ds_down_valid_o & ds_down_ready_i;
   
   // The input upstream channel feeds into a pipeline stage of II = 1.
   // This is done to make chaining of multiple of these units together as it
   // registers the output ready and valid signals.
   //
   // Only one of the downstream channels can be valid at a time and when
   // a downstream handshake happens it will happen only on the valid channel.
   // so the output of this pipeline stage is consumed when a handshake happens
   // either of the output channels. 
   assign pipe_stage_str_i.data = us_data_i;
   assign pipe_stage_str_i.sel_down_upbar = us_sel_down_upbar_i;
   assign pipe_stage_str_i.valid = us_valid_i;
   assign us_ready_o = pipe_stage_str_i.ready;
   assign pipe_stage_str_o.ready = ds_up_hs | ds_down_hs;   
   axis_pipeline_stage #
     (
      // DATA + up down select signal.
      .DATA_WIDTH(DATA_WIDTH + 1)
      )
   rtr_in_stage
     (
      .clk(clk),
      .reset(reset),
      .us_data({ pipe_stage_str_i.sel_down_upbar, 
		 pipe_stage_str_i.data }),
      .us_valid(pipe_stage_str_i.valid),
      .us_ready(pipe_stage_str_i.ready),
      .ds_data({ pipe_stage_str_o.sel_down_upbar,
		 pipe_stage_str_o.data }),
      .ds_valid(pipe_stage_str_o.valid),
      .ds_ready(pipe_stage_str_o.ready)
      );

   // downstream up/down channel valid controller.
   // When upstream handshake happens, the data would be registered in the
   // pipeline stage next cycle along with the select signal.
   // Based on this select signal enable only one of the output channels.
   always_comb begin : CONTROLLER
      ds_up_valid_o = 1'b0;
      ds_down_valid_o = 1'b0;
      if(pipe_stage_str_o.sel_down_upbar == 1'b1) begin
	 // select down channel.
	 ds_up_valid_o = 1'b0;
	 ds_down_valid_o = pipe_stage_str_o.valid;
      end else begin
	 // select up channel.
	 ds_up_valid_o = pipe_stage_str_o.valid;
	 ds_down_valid_o = 1'b0;	 
      end
   end : CONTROLLER

endmodule // axis_2_router
`endif
