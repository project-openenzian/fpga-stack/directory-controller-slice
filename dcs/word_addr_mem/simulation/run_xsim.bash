#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
      $XILINX_VIVADO/data/verilog/src/glbl.v \
      ../testbench/word_addr_memTb.sv \
      ../rtl/word_addr_mem.sv 

xelab -debug typical -incremental -L xpm worklib.word_addr_memTb worklib.glbl -s worklib.word_addr_memTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.word_addr_memTb 
xsim -R worklib.word_addr_memTb 
