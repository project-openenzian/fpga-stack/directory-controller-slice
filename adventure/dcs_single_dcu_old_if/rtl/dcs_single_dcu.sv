`ifndef DCS_SINGLE_DCU_SV
`define DCS_SINGLE_DCU_SV

import eci_cmd_defs::*;
import eci_dcs_defs::*;
import eci_dirc_defs::*; // DCU specific defs. 
import eci_cc_defs::*;   // CC specific defs to support dirc debug interface.

// Debug and performance counters are not currently
// exposed to software. This will eventually have
// to be connected to 
module dcs_single_dcu #
  (
   parameter PERF_REGS_WIDTH = 64,
   parameter SYNTH_PERF_REGS = 0   //0, 1
   )
   (
    input logic 					   clk,
    input logic 					   reset,
   
    // Input ECI events.
    // ECI packet for request without data. (VC 6 or 7) (only header).
    input logic [ECI_WORD_WIDTH-1:0] 			   req_wod_hdr_i,
    input logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   req_wod_pkt_size_i,
    input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 		   req_wod_pkt_vc_i,
    input logic 					   req_wod_pkt_valid_i,
    output logic 					   req_wod_pkt_ready_o,

    // ECI packet for response without data.(VC 10 or 11). (only header).
    input logic [ECI_WORD_WIDTH-1:0] 			   rsp_wod_hdr_i,
    input logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   rsp_wod_pkt_size_i,
    input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 		   rsp_wod_pkt_vc_i,
    input logic 					   rsp_wod_pkt_valid_i,
    output logic 					   rsp_wod_pkt_ready_o,

    // ECI packet for response with data. (VC 4 or 5). (header + data).
    input logic [ECI_PACKET_SIZE-1:0][ECI_WORD_WIDTH-1:0]  rsp_wd_pkt_i,
    input logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   rsp_wd_pkt_size_i,
    input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 		   rsp_wd_pkt_vc_i,
    input logic 					   rsp_wd_pkt_valid_i,
    output logic 					   rsp_wd_pkt_ready_o,

    // Output ECI events. (rsp without data, rsp with data).
    // VC 10,11
    output logic [ECI_WORD_WIDTH-1:0] 			   rsp_wod_hdr_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   rsp_wod_pkt_size_o,
    output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 		   rsp_wod_pkt_vc_o,
    output logic 					   rsp_wod_pkt_valid_o,
    input logic 					   rsp_wod_pkt_ready_i,

    // VC 5,4
    output logic [ECI_PACKET_SIZE-1:0][ECI_WORD_WIDTH-1:0] rsp_wd_pkt_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   rsp_wd_pkt_size_o,
    output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 		   rsp_wd_pkt_vc_o,
    output logic 					   rsp_wd_pkt_valid_o,
    input logic 					   rsp_wd_pkt_ready_i,

    // Output Read descriptors
    // Read descriptors: Request and response.
    output logic [MAX_DCU_ID_WIDTH-1:0] 		   rd_req_id_o,
    output logic [DS_ADDR_WIDTH-1:0] 			   rd_req_addr_o,
    output logic 					   rd_req_valid_o,
    input logic 					   rd_req_ready_i,

    input logic [MAX_DCU_ID_WIDTH-1:0] 			   rd_rsp_id_i,
    input logic [ECI_CL_WIDTH-1:0] 			   rd_rsp_data_i,
    input logic 					   rd_rsp_valid_i,
    output logic 					   rd_rsp_ready_o,

    // Write descriptors: Request and response.
    output logic [MAX_DCU_ID_WIDTH-1:0] 		   wr_req_id_o,
    output logic [DS_ADDR_WIDTH-1:0] 			   wr_req_addr_o,
    output logic [ECI_CL_WIDTH-1:0] 			   wr_req_data_o,
    output logic [ECI_CL_SIZE_BYTES-1:0] 		   wr_req_strb_o, 
    output logic 					   wr_req_valid_o,
    input logic 					   wr_req_ready_i,

    input logic [MAX_DCU_ID_WIDTH-1:0] 			   wr_rsp_id_i,
    input logic [1:0] 					   wr_rsp_bresp_i,
    input logic 					   wr_rsp_valid_i,
    output logic 					   wr_rsp_ready_o
    );

   typedef struct packed {
      logic [ECI_WORD_WIDTH-1:0]        hdr;
      logic [ECI_PACKET_SIZE_WIDTH-1:0] size;
      logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] vc;
      logic				valid;
      logic				ready;
   } eci_hdr_if_t;

   typedef struct packed {
      logic [ECI_PACKET_SIZE-1:0][ECI_WORD_WIDTH-1:0] pkt;
      logic [ECI_PACKET_SIZE_WIDTH-1:0] 	      size;
      logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	      vc;
      logic					      valid;
      logic					      ready;
   } eci_pkt_if_t;

   typedef struct packed {
      logic [MAX_DCU_ID_WIDTH-1:0]		   id;
      logic [DS_ADDR_WIDTH-1:0]			   addr;
      logic					   valid;
      logic					   ready;
   } rd_req_ctrl_if_t;

   typedef struct packed {
      logic [MAX_DCU_ID_WIDTH-1:0] id;
      logic 			   valid;
      logic 			   ready;
   } rd_rsp_ctrl_if_t;

   typedef logic [ECI_CL_WIDTH-1:0] rd_rsp_data_if_t;

   typedef struct packed {
      logic [MAX_DCU_ID_WIDTH-1:0]  id;
      logic [DS_ADDR_WIDTH-1:0]     addr;
      logic [ECI_CL_SIZE_BYTES-1:0] strb; 
      logic 			    valid;
      logic 			    ready;
   } wr_req_ctrl_if_t;

   typedef logic [ECI_CL_WIDTH-1:0] wr_req_data_if_t;

   typedef struct packed {
      logic [MAX_DCU_ID_WIDTH-1:0]  id;
      logic [1:0] 		    bresp;
      logic 			    valid;
      logic 			    ready;
   } wr_rsp_ctrl_if_t;

   typedef struct packed {
      logic [PERF_REGS_WIDTH-1:0] no_req_pkt_rdy;
      logic [PERF_REGS_WIDTH-1:0] no_req_pkt_stl;
      logic [PERF_REGS_WIDTH-1:0] no_rsp_pkt_sent;
      logic [PERF_REGS_WIDTH-1:0] no_rd_req;
      logic [PERF_REGS_WIDTH-1:0] no_rd_rsp;
      logic [PERF_REGS_WIDTH-1:0] no_wr_req;
      logic [PERF_REGS_WIDTH-1:0] no_wr_rsp;
      logic [PERF_REGS_WIDTH-1:0] no_cyc_bw_req_vld_rdy;
   } perf_counters_if_t;

   typedef struct packed {
      // Debug interface.
      logic [ECI_WORD_WIDTH-1:0]        eci_req_hdr;
      logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] eci_req_vc;
      logic [ECI_PACKET_SIZE_WIDTH-1:0] eci_req_size;
      logic 				eci_req_ready;
      logic 				eci_req_stalled;
      logic [ECI_WORD_WIDTH-1:0] 	eci_rsp_hdr;
      logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] eci_rsp_vc;
      logic [ECI_PACKET_SIZE_WIDTH-1:0] eci_rsp_size;
      logic 				eci_rsp_valid;
      cc_req_t				req_cc_enc;
      logic 				rdda;
      logic 				wdda;
      cc_req_t				cc_active_req;
      cc_state_t			cc_present_state;
      cc_state_t			cc_next_state;
      cc_action_t			cc_next_action;
      dirc_err_t			err_code;
      logic 				error;
      logic 				valid;
   } dbg_if_t;
   
   // DCU specific signals.
   eci_hdr_if_t dc_req_wod_i, dc_rsp_wod_i, dc_rsp_wd_hdr_i, dc_eci_rsp_hdr_o;
   rd_req_ctrl_if_t dc_rd_req_o;
   rd_rsp_ctrl_if_t dc_rd_rsp_ctrl_i;
   wr_req_ctrl_if_t dc_wr_req_ctrl_o;
   wr_rsp_ctrl_if_t dc_wr_rsp_ctrl_i;
   perf_counters_if_t prf_o;
   dbg_if_t dbg_o;
   // Read data path specific signals.
   rd_rsp_ctrl_if_t rdp_rsp_mem_ctrl_i;
   rd_rsp_data_if_t rdp_rsp_mem_data_i;
   rd_rsp_ctrl_if_t rdp_rsp_dcu_ctrl_o;
   eci_hdr_if_t rdp_rsp_wd_hdr_i;
   eci_pkt_if_t rdp_rsp_wd_pkt_o;
   // Write data path specific signals.
   eci_pkt_if_t wdp_rsp_wd_pkt_i;
   eci_hdr_if_t wdp_rsp_wd_hdr_o;
   wr_req_ctrl_if_t wdp_req_dcu_ctrl_i;
   wr_req_ctrl_if_t wdp_req_mem_ctrl_o;
   wr_req_data_if_t wdp_req_mem_data_o;
   // DCU to VC router specific signals.
   eci_hdr_if_t d2v_eci_rsp_hdr_i;
   eci_hdr_if_t d2v_rsp_wod_hdr_o;
   eci_hdr_if_t d2v_rsp_wd_hdr_o;

   always_comb begin : OUT_ASSIGN
      // ECI packet for request without data. (VC 6 or 7) (only header).
      req_wod_pkt_ready_o	= dc_req_wod_i.ready;
      // ECI packet for response without data.(VC 10 or 11). (only header).
      rsp_wod_pkt_ready_o	= dc_rsp_wod_i.ready;
      // ECI packet for response with data. (VC 4 or 5). (header + data).
      rsp_wd_pkt_ready_o	= wdp_rsp_wd_pkt_i.ready;
      // Output ECI events. (rsp without data, rsp with data).
      // VC 10,11
      rsp_wod_hdr_o		= d2v_rsp_wod_hdr_o.hdr;
      rsp_wod_pkt_size_o	= d2v_rsp_wod_hdr_o.size;
      rsp_wod_pkt_vc_o		= d2v_rsp_wod_hdr_o.vc;
      rsp_wod_pkt_valid_o	= d2v_rsp_wod_hdr_o.valid;
      // VC 5,4
      rsp_wd_pkt_o		= rdp_rsp_wd_pkt_o.pkt;
      rsp_wd_pkt_size_o		= rdp_rsp_wd_pkt_o.size;
      rsp_wd_pkt_vc_o		= rdp_rsp_wd_pkt_o.vc;
      rsp_wd_pkt_valid_o	= rdp_rsp_wd_pkt_o.valid;
      // Output Read descriptors
      // Read descriptors: Request.
      rd_req_id_o		= dc_rd_req_o.id;
      rd_req_addr_o		= dc_rd_req_o.addr;
      rd_req_valid_o		= dc_rd_req_o.valid;
      // Read descriptors: Response. 
      rd_rsp_ready_o		= rdp_rsp_mem_ctrl_i.ready;
      // Write descriptors: Request.
      wr_req_id_o		= wdp_req_mem_ctrl_o.id;
      wr_req_addr_o		= wdp_req_mem_ctrl_o.addr;
      wr_req_data_o		= wdp_req_mem_data_o;
      wr_req_strb_o		= wdp_req_mem_ctrl_o.strb; 
      wr_req_valid_o		= wdp_req_mem_ctrl_o.valid;
      // write descriptors: Response. 
      wr_rsp_ready_o		= dc_wr_rsp_ctrl_i.ready;
   end : OUT_ASSIGN
   
   always_comb begin : DCS_DCU_IN_ASSIGN
      // Incoming req without data hdr (VC 6,7).
      dc_req_wod_i.hdr		= req_wod_hdr_i;
      dc_req_wod_i.size		= req_wod_pkt_size_i;
      dc_req_wod_i.vc		= req_wod_pkt_vc_i;
      dc_req_wod_i.valid	= req_wod_pkt_valid_i;
      // Incoming rsp without data hdr (VC 10,11).
      dc_rsp_wod_i.hdr		= rsp_wod_hdr_i;
      dc_rsp_wod_i.size		= rsp_wod_pkt_size_i;
      dc_rsp_wod_i.vc		= rsp_wod_pkt_vc_i;
      dc_rsp_wod_i.valid	= rsp_wod_pkt_valid_i;
      // Incoming rsp with data hdr (VC 4,5).
      dc_rsp_wd_hdr_i.hdr	= wdp_rsp_wd_hdr_o.hdr;
      dc_rsp_wd_hdr_i.size	= wdp_rsp_wd_hdr_o.size;
      dc_rsp_wd_hdr_i.vc	= wdp_rsp_wd_hdr_o.vc;
      dc_rsp_wd_hdr_i.valid	= wdp_rsp_wd_hdr_o.valid;
      // Outgoing ECI headers (no data).
      dc_eci_rsp_hdr_o.ready	= d2v_eci_rsp_hdr_i.ready;
      // Outgoing Read request.
      dc_rd_req_o.ready		= rd_req_ready_i;
      // Incoming Read response	(without data).
      dc_rd_rsp_ctrl_i.id	= rdp_rsp_dcu_ctrl_o.id;
      dc_rd_rsp_ctrl_i.valid	= rdp_rsp_dcu_ctrl_o.valid;
      // Outgoing write request (without data).
      dc_wr_req_ctrl_o.ready	= wdp_req_dcu_ctrl_i.ready;
      // Incoming write response.
      dc_wr_rsp_ctrl_i.id	= wr_rsp_id_i;
      dc_wr_rsp_ctrl_i.bresp	= wr_rsp_bresp_i;
      dc_wr_rsp_ctrl_i.valid	= wr_rsp_valid_i;
   end : DCS_DCU_IN_ASSIGN
   dcu_top #
     (
      .PERF_REGS_WIDTH(PERF_REGS_WIDTH),
      .SYNTH_PERF_REGS(SYNTH_PERF_REGS)
       )
   dcs_dcu_inst
     (
      .clk(clk),
      .reset(reset),
      // Incoming ECI headers (no data).
      // Incoming req without data hdr (VC 6,7).
      .req_wod_hdr_i			(dc_req_wod_i.hdr),
      .req_wod_pkt_size_i		(dc_req_wod_i.size),
      .req_wod_pkt_vc_i			(dc_req_wod_i.vc),
      .req_wod_pkt_valid_i		(dc_req_wod_i.valid),
      .req_wod_pkt_ready_o		(dc_req_wod_i.ready),
      // Incoming rsp without data hdr (VC 10,11).
      .rsp_wod_hdr_i			(dc_rsp_wod_i.hdr),
      .rsp_wod_pkt_size_i		(dc_rsp_wod_i.size),
      .rsp_wod_pkt_vc_i			(dc_rsp_wod_i.vc),
      .rsp_wod_pkt_valid_i		(dc_rsp_wod_i.valid),
      .rsp_wod_pkt_ready_o		(dc_rsp_wod_i.ready),
      // Incoming rsp with data hdr (VC 4,5).
      .rsp_wd_hdr_i			(dc_rsp_wd_hdr_i.hdr),
      .rsp_wd_pkt_size_i		(dc_rsp_wd_hdr_i.size),
      .rsp_wd_pkt_vc_i			(dc_rsp_wd_hdr_i.vc),
      .rsp_wd_pkt_valid_i		(dc_rsp_wd_hdr_i.valid),
      .rsp_wd_pkt_ready_o		(dc_rsp_wd_hdr_i.ready),
      // Outgoing ECI headers (no data).
      // Currently only rsp with data (VC 4,5).
      // and rsp without data hdr (VC 10,11).
      .eci_rsp_hdr_o			(dc_eci_rsp_hdr_o.hdr),
      .eci_rsp_size_o			(dc_eci_rsp_hdr_o.size),
      .eci_rsp_vc_o			(dc_eci_rsp_hdr_o.vc),
      .eci_rsp_valid_o			(dc_eci_rsp_hdr_o.valid),
      .eci_rsp_ready_i			(dc_eci_rsp_hdr_o.ready),
      // Read descriptors to byte addressable memory.
      // Outgoing Read request.
      .rd_req_id_o			(dc_rd_req_o.id),
      .rd_req_addr_o			(dc_rd_req_o.addr),
      .rd_req_valid_o			(dc_rd_req_o.valid),
      .rd_req_ready_i			(dc_rd_req_o.ready),
      // Incoming Read response	(without data).
      .rd_rsp_id_i			(dc_rd_rsp_ctrl_i.id),
      .rd_rsp_valid_i			(dc_rd_rsp_ctrl_i.valid),
      .rd_rsp_ready_o			(dc_rd_rsp_ctrl_i.ready),
      // write descriptors to byte addressable memory.
      // Outgoing write request (without data).
      .wr_req_id_o			(dc_wr_req_ctrl_o.id),
      .wr_req_addr_o			(dc_wr_req_ctrl_o.addr),
      .wr_req_strb_o			(dc_wr_req_ctrl_o.strb),
      .wr_req_valid_o			(dc_wr_req_ctrl_o.valid),
      .wr_req_ready_i			(dc_wr_req_ctrl_o.ready),
      // Incoming write response.
      .wr_rsp_id_i			(dc_wr_rsp_ctrl_i.id),
      .wr_rsp_bresp_i			(dc_wr_rsp_ctrl_i.bresp),
      .wr_rsp_valid_i			(dc_wr_rsp_ctrl_i.valid),
      .wr_rsp_ready_o			(dc_wr_rsp_ctrl_i.ready),
      // Performance counters. 
      .prf_no_req_pkt_rdy_o		(prf_o.no_req_pkt_rdy),
      .prf_no_req_pkt_stl_o		(prf_o.no_req_pkt_stl),
      .prf_no_rsp_pkt_sent_o		(prf_o.no_rsp_pkt_sent),
      .prf_no_rd_req_o			(prf_o.no_rd_req),
      .prf_no_rd_rsp_o			(prf_o.no_rd_rsp),
      .prf_no_wr_req_o			(prf_o.no_wr_req),
      .prf_no_wr_rsp_o			(prf_o.no_wr_rsp),
      .prf_no_cyc_bw_req_vld_rdy_o	(prf_o.no_cyc_bw_req_vld_rdy),
      // Debug signals. 
      .dbg_eci_req_hdr_o		(dbg_o.eci_req_hdr),
      .dbg_eci_req_vc_o			(dbg_o.eci_req_vc),
      .dbg_eci_req_size_o		(dbg_o.eci_req_size),
      .dbg_eci_req_ready_o		(dbg_o.eci_req_ready),
      .dbg_eci_req_stalled_o		(dbg_o.eci_req_stalled),
      .dbg_eci_rsp_hdr_o		(dbg_o.eci_rsp_hdr),
      .dbg_eci_rsp_vc_o			(dbg_o.eci_rsp_vc),
      .dbg_eci_rsp_size_o		(dbg_o.eci_rsp_size),
      .dbg_eci_rsp_valid_o		(dbg_o.eci_rsp_valid),
      .dbg_req_cc_enc_o			(dbg_o.req_cc_enc),
      .dbg_rdda_o			(dbg_o.rdda),
      .dbg_wdda_o			(dbg_o.wdda),
      .dbg_cc_active_req_o		(dbg_o.cc_active_req),
      .dbg_cc_present_state_o		(dbg_o.cc_present_state),
      .dbg_cc_next_state_o		(dbg_o.cc_next_state),
      .dbg_cc_next_action_o		(dbg_o.cc_next_action),
      .dbg_err_code_o			(dbg_o.err_code),
      .dbg_error_o			(dbg_o.error),
      .dbg_valid_o			(dbg_o.valid)
      );

   // DCU ECI header to TX VCs.
   always_comb begin : D2V_RTR_IP_ASSIGN
      // ECI response from dcu.
      d2v_eci_rsp_hdr_i.hdr   = dc_eci_rsp_hdr_o.hdr;
      d2v_eci_rsp_hdr_i.size  = dc_eci_rsp_hdr_o.size;
      d2v_eci_rsp_hdr_i.vc    = dc_eci_rsp_hdr_o.vc;
      d2v_eci_rsp_hdr_i.valid = dc_eci_rsp_hdr_o.valid;
      // VC 10, 11 response without data (only header).
      d2v_rsp_wod_hdr_o.ready = rsp_wod_pkt_ready_i;
      // VC 5,4 response with data (only header).
      d2v_rsp_wd_hdr_o.ready  = rdp_rsp_wd_hdr_i.ready;
   end : D2V_RTR_IP_ASSIGN
   dcu_to_vc_router
     d2v_rtr_inst
       (
	.clk(clk),
	.reset			(reset),
	// ECI response from dcu.
	.eci_rsp_hdr_i		(d2v_eci_rsp_hdr_i.hdr),
	.eci_rsp_size_i		(d2v_eci_rsp_hdr_i.size),
	.eci_rsp_vc_i		(d2v_eci_rsp_hdr_i.vc),
	.eci_rsp_valid_i	(d2v_eci_rsp_hdr_i.valid),
	.eci_rsp_ready_o	(d2v_eci_rsp_hdr_i.ready),
	// VC 10, 11 response without data (only header).
	.rsp_wod_hdr_o		(d2v_rsp_wod_hdr_o.hdr),
	.rsp_wod_pkt_size_o	(d2v_rsp_wod_hdr_o.size),
	.rsp_wod_pkt_vc_o	(d2v_rsp_wod_hdr_o.vc),
	.rsp_wod_pkt_valid_o	(d2v_rsp_wod_hdr_o.valid),
	.rsp_wod_pkt_ready_i	(d2v_rsp_wod_hdr_o.ready),
	// VC 5,4 response with data (only header).
	.rsp_wd_hdr_o		(d2v_rsp_wd_hdr_o.hdr),
	.rsp_wd_pkt_size_o	(d2v_rsp_wd_hdr_o.size),
	.rsp_wd_pkt_vc_o	(d2v_rsp_wd_hdr_o.vc),
	.rsp_wd_pkt_valid_o	(d2v_rsp_wd_hdr_o.valid),
	.rsp_wd_pkt_ready_i	(d2v_rsp_wd_hdr_o.ready)
	);
   
   // Read from byte addressable mem data path.
   always_comb begin : RDP_IP_ASSIGN
      // Read resposne + data from byte addressable memory.
      rdp_rsp_mem_ctrl_i.id	= rd_rsp_id_i;
      rdp_rsp_mem_data_i	= rd_rsp_data_i;
      rdp_rsp_mem_ctrl_i.valid	= rd_rsp_valid_i;
      // read response only routed to DCU.
      rdp_rsp_dcu_ctrl_o.ready	= dc_rd_rsp_ctrl_i.ready;
      // VC 4,5 Input - header only from DCU
      rdp_rsp_wd_hdr_i.hdr	= d2v_rsp_wd_hdr_o.hdr;
      rdp_rsp_wd_hdr_i.size	= d2v_rsp_wd_hdr_o.size;
      rdp_rsp_wd_hdr_i.vc	= d2v_rsp_wd_hdr_o.vc;
      rdp_rsp_wd_hdr_i.valid	= d2v_rsp_wd_hdr_o.valid;
      // VC 4,5 Output header + data to TX to CPU.
      rdp_rsp_wd_pkt_o.ready	= rsp_wd_pkt_ready_i;
   end : RDP_IP_ASSIGN
   rd_data_path 
     dcs_rd_data_path 
       (
	.clk			(clk),
	.reset			(reset),
	// Read resposne + data from byte addressable memory.
	.rd_rsp_id_i		(rdp_rsp_mem_ctrl_i.id),
	.rd_rsp_data_i		(rdp_rsp_mem_data_i),
	.rd_rsp_valid_i		(rdp_rsp_mem_ctrl_i.valid),
	.rd_rsp_ready_o		(rdp_rsp_mem_ctrl_i.ready),
	// read response only routed to DCU.
	.rd_rsp_dcu_idx_o	(), // Not connected since there is only 1 DCU.
	.rd_rsp_id_o		(rdp_rsp_dcu_ctrl_o.id),
	.rd_rsp_valid_o		(rdp_rsp_dcu_ctrl_o.valid),
	.rd_rsp_ready_i		(rdp_rsp_dcu_ctrl_o.ready),
	// VC 4,5 Input - header only from DCU
	.rsp_wd_hdr_i		(rdp_rsp_wd_hdr_i.hdr),
	.rsp_wd_pkt_size_i	(rdp_rsp_wd_hdr_i.size),
	.rsp_wd_pkt_vc_i	(rdp_rsp_wd_hdr_i.vc),
	.rsp_wd_pkt_valid_i	(rdp_rsp_wd_hdr_i.valid),
	.rsp_wd_pkt_ready_o	(rdp_rsp_wd_hdr_i.ready),
	// VC 4,5 Output header + data to TX to CPU.
	.rsp_wd_pkt_o		(rdp_rsp_wd_pkt_o.pkt),
	.rsp_wd_pkt_size_o	(rdp_rsp_wd_pkt_o.size),
	.rsp_wd_pkt_vc_o	(rdp_rsp_wd_pkt_o.vc),
	.rsp_wd_pkt_valid_o	(rdp_rsp_wd_pkt_o.valid),
	.rsp_wd_pkt_ready_i	(rdp_rsp_wd_pkt_o.ready)
	);

   // write to byte addressable mem data path.
   always_comb begin : WDP_IP_ASSIGN
      // ECI packet for response with data 
      // (VC 4 or 5). (header + data) from CPU. 
      wdp_rsp_wd_pkt_i.pkt	= rsp_wd_pkt_i;
      wdp_rsp_wd_pkt_i.size	= rsp_wd_pkt_size_i;
      wdp_rsp_wd_pkt_i.vc	= rsp_wd_pkt_vc_i;
      wdp_rsp_wd_pkt_i.valid	= rsp_wd_pkt_valid_i;
      // Output ECI header only for response with data
      // to be routed to the DCUs.
      wdp_rsp_wd_hdr_o.ready	= dc_rsp_wd_hdr_i.ready;
      // Input Write descriptors: Request from DCUs. 
      // no data, only descriptor.
      wdp_req_dcu_ctrl_i.id	= dc_wr_req_ctrl_o.id;
      wdp_req_dcu_ctrl_i.addr	= dc_wr_req_ctrl_o.addr;
      wdp_req_dcu_ctrl_i.strb	= dc_wr_req_ctrl_o.strb;
      wdp_req_dcu_ctrl_i.valid	= dc_wr_req_ctrl_o.valid;
      // Output write request: descriptor + data i/f.
      // to byte addressable memory.
      wdp_req_mem_ctrl_o.ready	= wr_req_ready_i;
   end : WDP_IP_ASSIGN
   wr_data_path 
     dcs_wr_data_path 
       (
	.clk			(clk),
	.reset			(reset),
	// ECI packet for response with data 
	// (VC 4 or 5). (header + data) from CPU. 
	.rsp_wd_pkt_i		(wdp_rsp_wd_pkt_i.pkt),
	.rsp_wd_pkt_size_i	(wdp_rsp_wd_pkt_i.size),
	.rsp_wd_pkt_vc_i	(wdp_rsp_wd_pkt_i.vc),
	.rsp_wd_pkt_valid_i	(wdp_rsp_wd_pkt_i.valid),
	.rsp_wd_pkt_ready_o	(wdp_rsp_wd_pkt_i.ready),
	// Output ECI header only for response with data
	// to be routed to the DCUs.
	// DCU_IDX is for routing the ECI header, size, VC
	// to appropriate DCU.
	.rsp_wd_dcu_idx_o	(), // Not connected since there is only 1 DCU.
	.rsp_wd_hdr_o		(wdp_rsp_wd_hdr_o.hdr),
	.rsp_wd_pkt_size_o	(wdp_rsp_wd_hdr_o.size),
	.rsp_wd_pkt_vc_o	(wdp_rsp_wd_hdr_o.vc),
	.rsp_wd_pkt_valid_o	(wdp_rsp_wd_hdr_o.valid),
	.rsp_wd_pkt_ready_i	(wdp_rsp_wd_hdr_o.ready),
	// Input Write descriptors: Request from DCUs. 
	// no data, only descriptor.
	.wr_req_id_i		(wdp_req_dcu_ctrl_i.id),
	.wr_req_addr_i		(wdp_req_dcu_ctrl_i.addr),
	.wr_req_strb_i		(wdp_req_dcu_ctrl_i.strb),
	.wr_req_valid_i		(wdp_req_dcu_ctrl_i.valid),
	.wr_req_ready_o		(wdp_req_dcu_ctrl_i.ready),
	// Output write request: descriptor + data i/f.
	// to byte addressable memory.
	.wr_req_id_o		(wdp_req_mem_ctrl_o.id),
	.wr_req_addr_o		(wdp_req_mem_ctrl_o.addr),
	.wr_req_strb_o		(wdp_req_mem_ctrl_o.strb),
	.wr_req_data_o		(wdp_req_mem_data_o),
	.wr_req_valid_o		(wdp_req_mem_ctrl_o.valid),
	.wr_req_ready_i		(wdp_req_mem_ctrl_o.ready)
	);

endmodule // dcs_single_dcu

`endif
