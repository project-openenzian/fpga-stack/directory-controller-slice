#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
      $XILINX_VIVADO/data/verilog/src/glbl.v \
      ../rtl/eci_cmd_defs.sv \
      ../rtl/eci_cc_defs.sv \
      ../rtl/eci_dcs_defs.sv \
      ../rtl/eci_dirc_defs.sv \
      ../rtl/wr_trmgr.sv \
      ../rtl/dcu_controller.sv \
      ../rtl/dcu_tsu.sv \
      ../rtl/rd_trmgr.sv \
      ../rtl/gen_out_header.sv \
      ../rtl/decode_eci_req.sv \
      ../rtl/eci_cc_table.sv \
      ../rtl/dcu.sv \
      ../rtl/axis_2_router.sv \
      ../rtl/axis_comb_priority_enc.sv \
      ../rtl/axis_comb_rr_arb.sv \
      ../rtl/dcs_rr_arb.sv \
      ../rtl/map_ecid_to_wrd.sv \
      ../rtl/arb_4_ecih.sv \
      ../rtl/arb_3_ecih.sv \
      ../rtl/arb_2_ecih.sv \
      ../rtl/dcu_top.sv \
      ../rtl/axis_pipeline_stage.sv \
      ../rtl/tag_state_ram.sv \
      ../rtl/dcs.sv \
      ../rtl/ram_tdp.sv \
      ../rtl/dp_mem.sv \
      ../rtl/dp_wr_ser.sv \
      ../rtl/dp_gate.sv \
      ../rtl/wr_data_path.sv \
      ../rtl/dp_gen_path.sv \
      ../rtl/dp_data_store.sv \
      ../rtl/rd_data_path.sv \
      ../rtl/dcs_dcus.sv \
      ../rtl/axis_comb_router.sv \
      ../testbench/word_addr_mem.sv \
      ../rtl/dc_to_vc_router.sv \
      ../rtl/eci_trmgr.sv \
      ../rtl/vr_pipe_stage.sv \
      ../cli_lat_sim_tb/perf_simTb.sv \
      ../../perf_sim_modules/seq_read_load_gen/rtl/seq_read_load_gen.sv \
      ../../perf_sim_modules/evt_delay_buffer/rtl/evt_delay_buffer.sv \
      ../../perf_sim_modules/cli_lat_load_gen/rtl/cli_lat_load_gen.sv \
      ../../../common/axis_xpm_fifo/rtl/axis_xpm_fifo.sv \
      ../../../common/vr_pipe_stage/rtl/vr_pipe_stage.sv 

xelab -debug typical -incremental -L xpm worklib.perf_simTb worklib.glbl -s worklib.perf_simTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.perf_simTb 
xsim -gui worklib.perf_simTb 
