`ifndef ECI_DIRC_DEFS_SV
 `define ECI_DIRC_DEFS_SV
package eci_dirc_defs;

   parameter logic CL_IDX_ODD = 1'b1;
   parameter logic CL_IDX_EVEN = 1'b0;
   // Types of events (message class)
   // E means even (Odd Cl indices going to Even VCs will fall here)
   // O means odd (even CL indices that go to Odd VCs will fall here)
   // Remember
   //   E - means odd CL indices or even VC
   //   O - means even CL indices or odd VC 
   // IMPORTANT: DO NOT CONFUSE THEM WITH VC NUMBERS
   // THESE ARE MESSAGE CLASSES AND NOT VC NUMBERS
   typedef enum {
		 REQ_WO_DATA_E = 0,
		 REQ_WO_DATA_O = 1,
		 REQ_W_DATA_E = 2,
		 REQ_W_DATA_O = 3,
		 RESP_WO_DATA_E = 4,
		 RESP_WO_DATA_O = 5,
		 RESP_W_DATA_E = 6,
		 RESP_W_DATA_O = 7,
		 FWD_WO_DATA_E = 8,
		 FWD_WO_DATA_O = 9,
		 AXI_REFILL_RESP = 10,
		 AXI_EVICT_RESP = 11,
		 UNKNOWN_EVENT = 12
		 } dirc_event_t;
   
   // Typedefs
   // states within the directory controller
   // mark state upoon reset to all 0s 
   typedef enum {
		 WAIT_RTG_BUSY = 0,
		 CHECK_IN_EVENT,
		 READ_RTG_BUSY,
		 READ_RTG, 
		 READ_CC_ROM,
		 WAIT_OP_HS_ST,
		 ERROR_OUT_LOUD,
		 DBG_HALT} dirc_state_t;

   // Error encoding
   // DEV errors require dirc to be enhanced
   typedef enum logic [3:0]{
			    ERR_NO_ERROR = 0,
			    ERR_ECI_EVT_NOT_DECODED,
			    ERR_CC_EVT_NOT_ALLOWED,
			    // Requires dirc to be enhanced 
			    ERR_DEV_ACTION_NOT_DEFINED,
			    ERR_WRITE_0_DMASK,
			    // Trying to access a non accessible memory
			    // address. 
			    ERR_TSU_NXM
			    } dirc_err_t;

endpackage //eci_dirc_defs
`endif
