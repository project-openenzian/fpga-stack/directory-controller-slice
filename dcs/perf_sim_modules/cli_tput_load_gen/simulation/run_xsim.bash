#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
      $XILINX_VIVADO/data/verilog/src/glbl.v \
      ../rtl/eci_cmd_defs.sv \
      ../rtl/eci_dcs_defs.sv \
      ../../perf_gen_seq_aliased/rtl/perf_gen_seq_aliased.sv \
      ../testbench/cli_tput_load_genTb.sv \
      ../rtl/cli_tput_load_gen.sv \
      ../rtl/axis_pipeline_stage.sv 

xelab -debug typical -incremental -L xpm worklib.cli_tput_load_genTb worklib.glbl -s worklib.cli_tput_load_genTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.cli_tput_load_genTb 
xsim -R worklib.cli_tput_load_genTb 
