#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
$XILINX_VIVADO/data/verilog/src/glbl.v \
../testbench/ram_tdpTb.sv \
../rtl/ram_tdp.sv 

xelab -debug typical -incremental -L xpm worklib.ram_tdpTb worklib.glbl -s worklib.ram_tdpTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.ram_tdpTb 
xsim -gui worklib.ram_tdpTb 
