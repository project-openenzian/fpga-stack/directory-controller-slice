#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
$XILINX_VIVADO/data/verilog/src/glbl.v \
../testbench/eci_trmgrTb.sv \
../rtl/eci_trmgr.sv 

xelab -debug typical -incremental -L xpm worklib.eci_trmgrTb worklib.glbl -s worklib.eci_trmgrTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.eci_trmgrTb 
xsim -gui worklib.eci_trmgrTb 
