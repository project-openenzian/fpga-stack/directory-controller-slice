import eci_cmd_defs::*;
import eci_dcs_defs::*;

module wr_data_pathTb();

   //input output ports 
   //Input signals
   logic						 clk;
   logic						 reset;
   logic [ECI_PACKET_SIZE-1:0][ECI_WORD_WIDTH-1:0] 	 rsp_wd_pkt_i;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 			 rsp_wd_pkt_size_i;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 			 rsp_wd_pkt_vc_i;
   logic						 rsp_wd_pkt_valid_i;
   logic						 rsp_wd_pkt_ready_i;
   logic [MAX_DCU_ID_WIDTH-1:0] 			 wr_req_id_i;
   logic [DS_ADDR_WIDTH-1:0] 				 wr_req_addr_i;
   logic [ECI_CL_SIZE_BYTES-1:0] 			 wr_req_strb_i;
   logic						 wr_req_valid_i;
   logic						 wr_req_ready_i;

   //Output signals
   logic						 rsp_wd_pkt_ready_o;
   logic [DS_DCU_IDX_WIDTH-1:0] 			 rsp_wd_dcu_idx_o;
   logic [ECI_WORD_WIDTH-1:0] 				 rsp_wd_hdr_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 			 rsp_wd_pkt_size_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 			 rsp_wd_pkt_vc_o;
   logic						 rsp_wd_pkt_valid_o;
   logic						 wr_req_ready_o;
   logic [MAX_DCU_ID_WIDTH-1:0] 			 wr_req_id_o;
   logic [DS_ADDR_WIDTH-1:0] 				 wr_req_addr_o;
   logic [ECI_CL_SIZE_BYTES-1:0] 			 wr_req_strb_o;
   logic [ECI_CL_WIDTH-1:0] 				 wr_req_data_o;
   logic						 wr_req_valid_o;

   eci_word_t rsp_wd_hdr_c;
   logic [ECI_CL_WIDTH-1:0] 				 my_data;
   ds_cl_addr_t my_addr;

   
   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   wr_data_path wr_data_path1 (
			       .clk(clk),
			       .reset(reset),
			       
			       .rsp_wd_pkt_i(rsp_wd_pkt_i),
			       .rsp_wd_pkt_size_i(rsp_wd_pkt_size_i),
			       .rsp_wd_pkt_vc_i(rsp_wd_pkt_vc_i),
			       .rsp_wd_pkt_valid_i(rsp_wd_pkt_valid_i),
			       .rsp_wd_pkt_ready_o(rsp_wd_pkt_ready_o),

			       .rsp_wd_dcu_idx_o(rsp_wd_dcu_idx_o),
			       .rsp_wd_hdr_o(rsp_wd_hdr_o),
			       .rsp_wd_pkt_size_o(rsp_wd_pkt_size_o),
			       .rsp_wd_pkt_vc_o(rsp_wd_pkt_vc_o),
			       .rsp_wd_pkt_valid_o(rsp_wd_pkt_valid_o),
			       .rsp_wd_pkt_ready_i(rsp_wd_pkt_ready_i),
			       
			       .wr_req_id_i(wr_req_id_i),
			       .wr_req_addr_i(wr_req_addr_i),
			       .wr_req_strb_i(wr_req_strb_i),
			       .wr_req_valid_i(wr_req_valid_i),
			       .wr_req_ready_o(wr_req_ready_o),
			       
			       .wr_req_id_o(wr_req_id_o),
			       .wr_req_addr_o(wr_req_addr_o),
			       .wr_req_strb_o(wr_req_strb_o),
			       .wr_req_data_o(wr_req_data_o),
			       .wr_req_valid_o(wr_req_valid_o),
			       .wr_req_ready_i(wr_req_ready_i)
			       );//instantiation completed 

   always_comb begin : IP_ASSIGN
      rsp_wd_pkt_i[0] = rsp_wd_hdr_c.eci_word;
      rsp_wd_pkt_i[ECI_PACKET_SIZE-1:1] = my_data;

      // Retrieve IF.
      wr_req_id_i = '0;
      wr_req_id_i[DS_DCU_ID_WIDTH-1:0] = {rsp_wd_dcu_idx_o, 1'b0};
      wr_req_addr_i = rsp_wd_hdr_o[ECI_ADDR_WIDTH-1:0];
      wr_req_strb_i = '1;
      wr_req_valid_i = rsp_wd_pkt_valid_o;
      rsp_wd_pkt_ready_i = wr_req_ready_o;
   end : IP_ASSIGN
   
   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();


      // Tests.
      // Not verifying the datapath only if the signals
      // are correctly translated.
      // data path verified in dp_gen_path.

      // Initialize Input Ports 
      clk = '0;
      reset = '1;
      //rsp_wd_pkt_i = '0;
      rsp_wd_hdr_c.eci_word = '0;
      my_addr = '0;
      my_data = '0;
      rsp_wd_pkt_size_i = '0;
      rsp_wd_pkt_vc_i = '0;
      rsp_wd_pkt_valid_i = '0;

      wr_req_ready_i = '0;

      ##5;
      reset = 1'b0;
      
      wr_req_ready_i = 1'b1;
      
      ##5;
      
      my_addr = '0;
      my_addr.xb2 = '0;
      my_addr.tag = $urandom_range(100,2000);
      my_addr.set_within_dcu = $urandom_range(1,8091);
      my_addr.dcu_id = 'd4; // Give only even values. 
      my_addr.byte_offset = '0;
      
	
      rsp_wd_hdr_c.eci_word = '0;
      rsp_wd_hdr_c.generic_cmd.dmask = '1;
      rsp_wd_hdr_c[ECI_WORD_WIDTH-1:0] = my_addr;
      //my_data = my_addr * 2;
      my_data = '1;
      rsp_wd_pkt_size_i = 'd17;
      rsp_wd_pkt_vc_i = 'd4;
      rsp_wd_pkt_valid_i = 1'b1;
      wait(rsp_wd_pkt_valid_i & rsp_wd_pkt_ready_o);
      ##1;
      rsp_wd_pkt_valid_i = 1'b0;

      ##10; // sufficient time has passed. 
      
      // assert
      assert(rsp_wd_dcu_idx_o === my_addr.dcu_id[DS_DCU_ID_WIDTH-1:1]) else 
	$fatal(1,"Index is wrong");
      
      assert(rsp_wd_hdr_o === rsp_wd_hdr_c.eci_word) else 
	$fatal(1, "Header is wrong");

      assert(wr_req_id_o === my_addr.dcu_id) else 
	$fatal(1, "wr req ID is wrong.");

      assert(wr_req_addr_o === rsp_wd_hdr_c[ECI_WORD_WIDTH-1:0]) else 
	$fatal(1, "wr req address is wrong.");
      
      //assert(wr_req_data_o === wr_req_addr_o * 2) else 
	//$fatal(1, "Data is wrong.");

      $display("Success.");
      
      #500 $finish;
   end // initial begin


endmodule
