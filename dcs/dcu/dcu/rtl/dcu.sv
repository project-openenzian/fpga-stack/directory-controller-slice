/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-06-28
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */

`ifndef DCU_SV
`define DCU_SV

import eci_cmd_defs::*;  // ECI specific defs.
import eci_dcs_defs::*;  // DCS defs at a higher level than DCU.
import eci_dirc_defs::*; // DCU specific defs.
import eci_cc_defs::*;   // CC specific defs.

// The CL indices are 40 bits but we are
// choosing a smaller address range with 38 bits per DCU slice.
// Address range:
// each DCU is responsible for portion of 38 bit address space so
// output descriptor addresses from DCU is 38 bits wide.
// DCU ID width:
// Depending on number of sets per DCU,
// the DCU ID varies between 5,6 and 7.
// choosing the maximum value of 7. 
module dcu #
  (
   parameter PERF_REGS_WIDTH = 64,
   parameter SYNTH_PERF_REGS = 0
   )
   (
    input logic 			     clk,
    input logic 			     reset,
    // Input ECI header.
    input logic [ECI_WORD_WIDTH-1:0] 	     eci_req_hdr_i,
    input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0]  eci_req_vc_i,
    input logic [ECI_PACKET_SIZE_WIDTH-1:0]  eci_req_size_i,
    input logic 			     eci_req_valid_i,
    output logic 			     eci_req_ready_o,
    output logic 			     eci_req_stall_o,

    // Output ECI header.
    output logic [ECI_WORD_WIDTH-1:0] 	     eci_rsp_hdr_o,
    output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] eci_rsp_vc_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] eci_rsp_size_o,
    output logic 			     eci_rsp_valid_o,
    input logic 			     eci_rsp_ready_i,

    // Read descriptors: request, response.
    output logic [MAX_DCU_ID_WIDTH-1:0]      rd_req_id_o,
    output logic [DS_ADDR_WIDTH-1:0] 	     rd_req_addr_o,
    output logic 			     rd_req_valid_o,
    input logic 			     rd_req_ready_i,

    input logic [MAX_DCU_ID_WIDTH-1:0] 	     rd_rsp_id_i,
    input logic 			     rd_rsp_valid_i,
    output logic 			     rd_rsp_ready_o,

    // Write descriptors: request, response.
    output logic [MAX_DCU_ID_WIDTH-1:0]      wr_req_id_o,
    output logic [DS_ADDR_WIDTH-1:0] 	     wr_req_addr_o,
    output logic [ECI_CL_SIZE_BYTES-1:0]     wr_req_strb_o, 
    output logic 			     wr_req_valid_o,
    input logic 			     wr_req_ready_i,

    input logic [MAX_DCU_ID_WIDTH-1:0] 	     wr_rsp_id_i,
    input logic [1:0] 			     wr_rsp_bresp_i,
    input logic 			     wr_rsp_valid_i,
    output logic 			     wr_rsp_ready_o,

    // Performance counters.
    output logic [PERF_REGS_WIDTH-1:0] 	     prf_no_req_pkt_rdy_o,
    output logic [PERF_REGS_WIDTH-1:0] 	     prf_no_req_pkt_stl_o,
    output logic [PERF_REGS_WIDTH-1:0] 	     prf_no_rsp_pkt_sent_o,
    output logic [PERF_REGS_WIDTH-1:0] 	     prf_no_rd_req_o,
    output logic [PERF_REGS_WIDTH-1:0] 	     prf_no_rd_rsp_o,
    output logic [PERF_REGS_WIDTH-1:0] 	     prf_no_wr_req_o,
    output logic [PERF_REGS_WIDTH-1:0] 	     prf_no_wr_rsp_o,
    output logic [PERF_REGS_WIDTH-1:0] 	     prf_no_cyc_bw_req_vld_rdy_o,

    // Debug interface
    output logic [ECI_WORD_WIDTH-1:0] 	     dbg_eci_req_hdr_o,
    output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] dbg_eci_req_vc_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] dbg_eci_req_size_o,
    output logic 			     dbg_eci_req_ready_o,
    output logic 			     dbg_eci_req_stalled_o,
    output logic [ECI_WORD_WIDTH-1:0] 	     dbg_eci_rsp_hdr_o,
    output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] dbg_eci_rsp_vc_o,
    output logic [ECI_PACKET_SIZE_WIDTH-1:0] dbg_eci_rsp_size_o,
    output logic 			     dbg_eci_rsp_valid_o,
    output 				     cc_req_t dbg_req_cc_enc_o,
    output logic 			     dbg_rdda_o,
    output logic 			     dbg_wdda_o,
    output logic 			     dbg_stall_prot_o,
    output logic 			     dbg_stall_rd_busy_o,
    output logic 			     dbg_stall_wr_busy_o,
    output logic 			     dbg_stall_eci_tr_busy_o,
    output logic 			     dbg_stall_rtg_full_o,
    output logic 			     dbg_stall_wait_op_hs_o,
    output 				     cc_req_t dbg_cc_active_req_o,
    output 				     cc_state_t dbg_cc_present_state_o,
    output 				     cc_state_t dbg_cc_next_state_o,
    output 				     cc_action_t dbg_cc_next_action_o,
    output 				     dirc_err_t dbg_err_code_o,
    output logic 			     dbg_error_o,
    output logic 			     dbg_valid_o
    );

   // Tag State Unit.
   //  40b ECI Cl address is split into
   //
   // ┌────────────┬────────────────────┐
   // │ 2b Slice ID│ 38b Slice CLaddr   │
   // └────────────┴────────────────────┘
   //
   //  38b slice CL address is split into 
   // ┌────────────┬────────────┬──────────────┐
   // │   18b tag  │ 13b set idx│ 7b ByteOffset│
   // └────────────┴─────┬──────┴──────────────┘
   //                    │set idx is
   //                    ▼ split into
   //        ┌───────────────┬──────────┐
   //        │   6/7/8b      │ 7/6/5b   │
   //        │set within DCU │ DCU_ID   │
   //        └───────────────┴──────────┘
   localparam NUM_SETS_PER_DCU = DS_NUM_SETS_PER_DCU; // 64, 128, 256
   localparam SET_IDX_WIDTH = DS_SET_IDX_WIDTH;
   localparam TSU_BYTE_OFFSET_WIDTH = $clog2(ECI_CL_SIZE_BYTES); //7
   // Size of a way data entry in the TSU.
   localparam TSU_WAY_DATA_WIDTH = DS_TAG_WIDTH;
   // Set within DCU. 64=6, 128=7, 256=8
   localparam TSU_SET_IDX_WIDTH = $clog2(NUM_SETS_PER_DCU); //6/7/8b
   localparam DCU_ID_WIDTH = SET_IDX_WIDTH - TSU_SET_IDX_WIDTH; //7/6/5b
   localparam TSU_TAG_WIDTH = TSU_WAY_DATA_WIDTH; //18
   localparam TSU_STT_WIDTH = TSU_WAY_DATA_WIDTH; //18
   // ways.
   localparam NUM_WAYS_PER_SET = DS_NUM_WAYS_PER_SET; //16
   localparam TSU_NUM_WAYS = NUM_WAYS_PER_SET; // 16
   localparam TSU_WAY_IDX_WIDTH = $clog2(TSU_NUM_WAYS); // 4
   // Descriptors.
   localparam DESC_ID_WIDTH = MAX_DCU_ID_WIDTH; //max(DCU_ID_WIDTH)
   localparam DCU_ADDR_WIDTH = DS_ADDR_WIDTH; //2^38 is address range per DCU. 
   
   typedef ds_cl_addr_t tsu_cl_addr_t;
   
   // TSU views the 40 bit ECI CL address as:
   //typedef struct packed {
   //   logic [1:0] xb2; // 2b dont care.
   //   logic [TSU_TAG_WIDTH-1:0] tag; //18b
   //   logic [TSU_SET_IDX_WIDTH-1:0] set_within_dcu; //6/7/8b
   //   logic [DCU_ID_WIDTH-1:0] 	    dcu_id; //7/6/5b
   //   logic [TSU_BYTE_OFFSET_WIDTH-1:0] byte_offset; //7b
   //} tsu_cl_addr_t; //40b
   
   // Decoder signals.
   logic [ECI_WORD_WIDTH-1:0] 	     dec_req_hdr_i;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] dec_req_vc_i;
   eci_hreqid_t                      dec_req_trid_o;
   eci_address_t                     dec_req_addr_o;
   eci_dmask_t                       dec_req_dmask_o;
   logic 			     dec_req_cl_idx_oe_o;
   cc_req_t                          dec_req_cc_enc_o;
   logic 			     dec_req_invalid_o;
   logic 			     dec_req_crts_rtg_ntry_o;
   logic 			     dec_rsp_2_prv_snt_req_o;
   tsu_cl_addr_t                     dec_req_addr_tsu_c;

   // TSU signals.
   logic [TSU_TAG_WIDTH-1:0] 	 tsu_wr_tag_i;
   logic [TSU_STT_WIDTH-1:0] 	 tsu_wr_stt_i;
   logic [TSU_SET_IDX_WIDTH-1:0] tsu_wr_set_i;
   logic [TSU_WAY_IDX_WIDTH-1:0] tsu_wr_way_i;
   logic 			 tsu_wr_en_i;
   logic [TSU_SET_IDX_WIDTH-1:0] tsu_rd_tag_set_i;
   logic [TSU_TAG_WIDTH-1:0] 	 tsu_rd_tag_to_cmp_i;
   logic 			 tsu_rd_tag_en_i;
   logic [TSU_STT_WIDTH-1:0] 	 tsu_rd_tag_stt_o;
   logic [TSU_WAY_IDX_WIDTH-1:0] tsu_rd_tag_way_o;
   logic 			 tsu_rd_tag_hit_o;
   logic 			 tsu_rd_tag_set_full_o;
   logic 			 tsu_rd_tag_nxm_o;
   logic 			 tsu_busy_o;

   // CCROM signals
   logic       ccr_en_i;
   cc_state_t  ccr_curr_state_i;
   cc_req_t    ccr_curr_req_i;
   cc_state_t  ccr_next_state_o;
   cc_action_t ccr_next_action_o;
   cc_req_t    ccr_curr_active_req_o;
   logic       ccr_ns_valid_o;
   logic       ccr_ns_evictable_o;
   logic       ccr_ns_evcn_in_prgrs_o;

   // RD_TRMGR signals.
   logic [DESC_ID_WIDTH-1:0]   tr_rd_desc_id_i;
   logic [ECI_WORD_WIDTH-1:0]  tr_rd_desc_meta_i;
   logic [DCU_ADDR_WIDTH-1:0]  tr_rd_desc_addr_i;
   logic 		       tr_rd_desc_valid_i;
   logic 		       tr_rd_desc_ready_o;
   logic [DESC_ID_WIDTH-1:0]   tr_rd_req_id_o;
   logic [DCU_ADDR_WIDTH-1:0]  tr_rd_req_addr_o;
   logic 		       tr_rd_req_valid_o;
   logic 		       tr_rd_req_ready_i;
   logic [DESC_ID_WIDTH-1:0]   tr_rd_rsp_id_i;
   logic 		       tr_rd_rsp_valid_i;
   logic 		       tr_rd_rsp_ready_o;
   logic [DESC_ID_WIDTH-1:0]   tr_rd_desc_id_o;
   logic [ECI_WORD_WIDTH-1:0]  tr_rd_desc_meta_o;
   logic 		       tr_rd_desc_valid_o;
   logic 		       tr_rd_desc_ready_i;
   tsu_cl_addr_t               tr_rd_rsp_addr_c;
   logic [ECI_WORD_WIDTH-1:0]  tr_rd_rsp_eci_hdr;

   // WR_TRMGR signals.
   logic [DESC_ID_WIDTH-1:0]     tr_wr_desc_id_i;
   logic [ECI_WORD_WIDTH-1:0] 	 tr_wr_desc_meta_i;
   logic [DCU_ADDR_WIDTH-1:0] 	 tr_wr_desc_addr_i;
   logic [ECI_DMASK_WIDTH-1:0] 	 tr_wr_desc_dmask_i;
   logic 			 tr_wr_desc_valid_i;
   logic 			 tr_wr_desc_ready_o;
   logic [DESC_ID_WIDTH-1:0] 	 tr_wr_req_id_o;
   logic [DCU_ADDR_WIDTH-1:0] 	 tr_wr_req_addr_o;
   logic [ECI_CL_SIZE_BYTES-1:0] tr_wr_req_strb_o;
   logic 			 tr_wr_req_valid_o;
   logic 			 tr_wr_req_ready_i;
   logic [DESC_ID_WIDTH-1:0] 	 tr_wr_rsp_id_i;
   logic [1:0] 			 tr_wr_rsp_bresp_i;
   logic 			 tr_wr_rsp_valid_i;
   logic 			 tr_wr_rsp_ready_o;
   logic [DESC_ID_WIDTH-1:0] 	 tr_wr_desc_id_o;
   logic [ECI_WORD_WIDTH-1:0] 	 tr_wr_desc_meta_o;
   logic [1:0] 			 tr_wr_desc_bresp_o;
   logic 			 tr_wr_desc_valid_o;
   logic 			 tr_wr_desc_ready_i;
   tsu_cl_addr_t                 tr_wr_rsp_addr_c;
   logic [ECI_WORD_WIDTH-1:0] 	 tr_wr_rsp_eci_hdr;

   // ECI_TRMGR signals.
   logic [ECI_WORD_WIDTH-1:0] 	 tr_eci_alloc_data_i;
   logic 			 tr_eci_alloc_en_i;
   logic 			 tr_eci_free_en_i;
   logic [ECI_WORD_WIDTH-1:0] 	 tr_eci_alloc_data_o;
   logic 			 tr_eci_alloc_valid_o;
   
   // Gen out header.
   logic 			 goh_en_i;
   cc_action_t                   goh_cc_acn_i;
   eci_word_t                    goh_hdr_from_tr_tbl_i;
   logic 			 goh_rdda_sink_in_prgrs_i;
   logic 			 goh_wdda_sink_in_prgrs_i;
   eci_hreqid_t                  goh_fwd_tr_id_i;
   eci_address_t                 goh_fwd_cl_addr_i;
   eci_dmask_t                   goh_fwd_dmask_i;
   logic 			 goh_fwd_ns_i;
   eci_word_t                    goh_eci_hdr_o;
   eci_vc_size_t                 goh_vc_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] goh_pkt_size_o;
   logic 			     goh_eci_hdr_invalid_o;

   // Controller signals.
   logic        dc_eci_req_valid_i;
   logic 	dc_eci_req_ready_o;
   logic 	dc_eci_req_stall_o;
   logic 	dc_eci_evt_crts_rtg_ntry_i;
   logic 	dc_eci_rsp_2_prv_snt_req_i;
   logic 	dc_eci_rsp_valid_o;
   logic 	dc_eci_rsp_ready_i;
   logic 	dc_dec_req_invalid_i;
   logic 	dc_rtg_busy_i;
   logic 	dc_rtg_rd_en_o;
   logic 	dc_rtg_wr_en_o;
   logic 	dc_rtg_hit_i;
   logic 	dc_rtg_full_i;
   logic 	dc_rtg_nxm_i;
   logic 	dc_cc_sel_inv_ps_o;
   logic 	dc_cc_rom_en_o;
   cc_action_t  dc_cc_next_action_i;
   cc_req_t     dc_cc_curr_req_i;
   logic 	dc_tr_rd_req_valid_o;
   logic 	dc_tr_rd_req_ready_i;
   logic 	dc_tr_rd_rsp_valid_i;
   logic 	dc_tr_rd_rsp_ready_o;
   logic 	dc_tr_wr_req_valid_o;
   logic 	dc_tr_wr_req_ready_i;
   logic 	dc_tr_wr_rsp_valid_i;
   logic 	dc_tr_wr_rsp_ready_o;
   logic 	dc_tr_eci_full_i;
   logic 	dc_tr_eci_alloc_en_o;
   logic 	dc_tr_eci_free_en_o;
   logic 	dc_goh_en_o;
   logic 	dc_ctxt_rdda_o;
   logic 	dc_ctxt_wdda_o;
   logic 	dc_ctxt_ecitr_o;
   logic 	dc_stall_prot_o;
   logic 	dc_stall_rd_busy_o;
   logic 	dc_stall_wr_busy_o;
   logic 	dc_stall_eci_tr_busy_o;
   logic 	dc_stall_rtg_full_o;
   logic 	dc_stall_wait_op_hs_o;
   dirc_state_t dc_dirc_state_o;
   dirc_err_t   dc_error_code_o;
   logic 	dc_error_o;
   logic 	dc_dbg_latch_en;
   
   always_comb begin : OUT_ASSIGN
      // ECI outputs.
      eci_req_ready_o = dc_eci_req_ready_o;
      eci_req_stall_o = dc_eci_req_stall_o;
      eci_rsp_hdr_o   = goh_eci_hdr_o;
      eci_rsp_vc_o    = goh_vc_o;
      eci_rsp_size_o  = goh_pkt_size_o;
      eci_rsp_valid_o = dc_eci_rsp_valid_o;
      // Read desc outputs.
      rd_req_id_o = tr_rd_req_id_o;
      rd_req_addr_o = tr_rd_req_addr_o;
      rd_req_valid_o = tr_rd_req_valid_o;
      rd_rsp_ready_o = tr_rd_rsp_ready_o;
      // write desc outputs.
      wr_req_id_o    = tr_wr_req_id_o;    
      wr_req_addr_o  = tr_wr_req_addr_o;  
      wr_req_strb_o  = tr_wr_req_strb_o;
      wr_req_valid_o = tr_wr_req_valid_o; 
      wr_rsp_ready_o = tr_wr_rsp_ready_o;
   end : OUT_ASSIGN

   // Decode Incoming ECI event.
   always_comb begin : DEC_IP_ASSIGN
      dec_req_hdr_i = eci_req_hdr_i;
      dec_req_vc_i = eci_req_vc_i;
      dec_req_addr_tsu_c = tsu_cl_addr_t'(dec_req_addr_o);
   end : DEC_IP_ASSIGN
   
   decode_eci_req
     dec_eci_req_inst1
       (
	.clk				(clk),
	.reset				(reset),
	// Input ECI event.
	.eci_req_hdr_i			(dec_req_hdr_i),
	.eci_req_vc_i			(dec_req_vc_i),
	// Decoded output.
	.eci_req_trid_o			(dec_req_trid_o),
	.eci_req_addr_o			(dec_req_addr_o),
	.eci_req_dmask_o		(dec_req_dmask_o),
	.eci_req_cl_idx_oe_o		(dec_req_cl_idx_oe_o),
	.eci_req_cc_enc_o		(dec_req_cc_enc_o),
	.eci_req_invalid_o		(dec_req_invalid_o),
	.eci_req_crts_rtg_ntry_o	(dec_req_crts_rtg_ntry_o),
	.eci_rsp_2_prv_snt_req_o	(dec_rsp_2_prv_snt_req_o)	
	);

   // TSU - Tag state unit aka directory.
   // Directory stores and retrieves present
   // state of a cacheline.
   //
   // Reading the TSU:
   //  Given the set and tag, the TSU
   //  checks all ways of the set to find
   //  the state for the given tag.
   //
   //  Upon hit, the TSU returns the
   //  state and way of the matching tag.
   //
   //  Upon miss, if set is not full,
   //  the TSU returns a free way incase
   //  a new tag has to be added. The
   //  state returned is not a valid state.
   //  it cannot be expected to be S1__1(invalid).
   //
   //  Upon miss, if set is full
   //  this should never occur if an
   //  entry is to be added to the TSU.
   //  the controller will identify this
   //  and error out.
   //
   //  This TSU does not support induced
   //  evictions to make space in the TSU.
   //
   // Writing to TSU:
   //  Given set and way, a tag and
   //  state can be written in 1 cycle. 
   //
   // Input Assign:
   //
   // read state and tag:
   //  The set and tag to read depends on
   //  context (see write state and tag)
   //
   //  read enable is from dcu_controller.
   // write state and tag:
   //  The set and tag to write to depends
   //  on the context, whether the current
   //  transaction being handled is rd rsp,
   //  wr rsp or incoming eci event.
   //
   //  The state to write is always from
   //  the CCROM. TSU has 18 bit state but
   //  CCROM might return lesser number of
   //  bits so write only that.
   //
   //  The way to write is always from
   //  the previous read of TSU.
   //  TSU returns hit way upon hit and
   //  free way upon miss.
   //
   //  write enable is from dcu_controller.
   
   always_comb begin : TSU_IP_ASSIGN
      if(dc_ctxt_rdda_o) begin
	 tsu_rd_tag_set_i = tr_rd_rsp_addr_c.set_within_dcu;
	 tsu_rd_tag_to_cmp_i = tr_rd_rsp_addr_c.tag;
	 tsu_wr_set_i = tr_rd_rsp_addr_c.set_within_dcu;
	 tsu_wr_tag_i = tr_rd_rsp_addr_c.tag;
      end else if (dc_ctxt_wdda_o) begin
	 tsu_rd_tag_set_i = tr_wr_rsp_addr_c.set_within_dcu;
	 tsu_rd_tag_to_cmp_i = tr_wr_rsp_addr_c.tag;
	 tsu_wr_set_i = tr_wr_rsp_addr_c.set_within_dcu;
	 tsu_wr_tag_i = tr_wr_rsp_addr_c.tag;
      end else begin
	 tsu_rd_tag_set_i = dec_req_addr_tsu_c.set_within_dcu;
	 tsu_rd_tag_to_cmp_i = dec_req_addr_tsu_c.tag;
	 tsu_wr_set_i = dec_req_addr_tsu_c.set_within_dcu;
	 tsu_wr_tag_i = dec_req_addr_tsu_c.tag;
      end
      tsu_rd_tag_en_i = dc_rtg_rd_en_o;
      tsu_wr_en_i = dc_rtg_wr_en_o;
      //18 bits wide, write only necessary bits. 
      tsu_wr_stt_i = '0; 
      tsu_wr_stt_i[NUM_STATE_WIDTH-1:0] = ccr_next_state_o;
      tsu_wr_way_i = tsu_rd_tag_way_o;
   end : TSU_IP_ASSIGN
   
   dcu_tsu #
     (
      .NUM_SETS_PER_DCU(NUM_SETS_PER_DCU),
      .TSR_OP_REGISTER(DS_TSR_OP_REGISTER)
      )
   tsu1
     (
      .clk(clk),
      .reset(reset),
      // write tag and state.
      .wr_tag_i(tsu_wr_tag_i),
      .wr_stt_i(tsu_wr_stt_i),
      .wr_set_i(tsu_wr_set_i),
      .wr_way_i(tsu_wr_way_i),
      .wr_en_i(tsu_wr_en_i),
      // read tag and state.
      .rd_tag_set_i(tsu_rd_tag_set_i),
      .rd_tag_to_cmp_i(tsu_rd_tag_to_cmp_i),
      .rd_tag_en_i(tsu_rd_tag_en_i),
      .rd_tag_stt_o(tsu_rd_tag_stt_o),
      .rd_tag_way_o(tsu_rd_tag_way_o),
      .rd_tag_hit_o(tsu_rd_tag_hit_o),
      .rd_tag_set_full_o(tsu_rd_tag_set_full_o),
      .rd_tag_nxm_o(tsu_rd_tag_nxm_o),
      // tsu is busy, 
      // all inputs ignored.
      .tsu_busy_o(tsu_busy_o)
      );

   // CC ROM - Given present state and event 
   // CCROM prescribes the next state and 
   // action to be performed.
   //
   // Controller determines what is the present state 
   // and present event and when to enable.
   //   Present state to CCROM 
   //     choose between invalid and state value returned by RTG.
   //   Present event to CCROM
   //     if controller picks a read response to handle, 
   //     the present event is RDDA.
   //
   //  When enabled, Given present state and event, 
   //  it provides the next state and action to be 
   //  performed. it also gives properties of the next state 
   //  that will be useful later.
   //  The properties returned are
   //   1. Is the next state Invalid (I of MOESI).
   //   2. Does the next state indicate the CL is evictable?
   //   3. Does the next state indicate an eviction in progress?
   //
   //   currently dcu does not induce evictions. 
   //   The evictable and eviction in progress properties
   //   are useful when inducing evictions through dcu.
   //
   // Note: state returned by TSU is 18 bits,
   // different from what CCROM expects, needs casting.
   assign ccr_en_i = dc_cc_rom_en_o;
   assign ccr_curr_state_i = (dc_cc_sel_inv_ps_o == 1'b1) ? 
			     cc_state_t'(s1__1) : 
			     cc_state_t'(tsu_rd_tag_stt_o);
   // Present event to CCROM.
   always_comb begin : CC_REQ_MUX
      if(dc_ctxt_rdda_o) begin
	 // handling a read response.
	 ccr_curr_req_i = cc_req_t'(RDDA);
      end else if (dc_ctxt_wdda_o) begin
	 // handling a write response.
	 ccr_curr_req_i = cc_req_t'(WDDA);
      end else begin
	 // event is got from input ECI event. 
	 ccr_curr_req_i = cc_req_t'(dec_req_cc_enc_o);
      end
   end : CC_REQ_MUX

   eci_cc_table
     cc_rom_inst
     (
      .clk(clk),
      .reset(reset),
      // Input present state + event. 
      .en(ccr_en_i),
      .curr_state_i(ccr_curr_state_i),
      .curr_req_i(ccr_curr_req_i),
      // Output next state and action.
      .next_state_o(ccr_next_state_o),
      .next_action_o(ccr_next_action_o),
      .curr_active_req_o(ccr_curr_active_req_o),
      .ns_valid_o(ccr_ns_valid_o),
      .ns_evictable_o(ccr_ns_evictable_o),
      .ns_evcn_in_prgrs_o(ccr_ns_evcn_in_prgrs_o)
      );
   
   // Read transaction manager:
   //  For a read operation, Store ECI header
   //  start a read transaction by issuing read
   //  request descriptors.
   //
   //  The ID for request descriptors is the
   //  DCU ID so that can be used for routing
   //  response back to this DCU. 
   //
   //  when read response is received, retrieve the
   //  stored ECI header to complete the transaction.
   //
   //  Only 1 on-going transaction per DCU at a time.
   //  current granularity of read is cacheline note
   //  sub cache line.
   //
   // Assigning Inputs:
   //  tr_rd_desc_id_i is 7 bits but
   //  dec_req_addr_tsu_c.dcu_id can be 5/6/7 bits so
   //  fill remaining bits of tr_rd_desc_id_i with 0s.
   //
   //  The meta data, addr is always from 
   //  input eci header as read transactions 
   //  are only generated by incoming ECI events.
   //
   //  rd req,rsp i/f is connected to output of DCU.

   always_comb begin : RT_TRMGR_IP_ASSIGN
      tr_rd_desc_id_i = '0;
      tr_rd_desc_id_i[DCU_ID_WIDTH-1:0] = dec_req_addr_tsu_c.dcu_id;
      tr_rd_desc_meta_i = eci_req_hdr_i;
      tr_rd_desc_addr_i = dec_req_addr_o;
      tr_rd_desc_valid_i = dc_tr_rd_req_valid_o;
      tr_rd_req_ready_i  = rd_req_ready_i;
      tr_rd_rsp_id_i     = rd_rsp_id_i;
      tr_rd_rsp_valid_i  = rd_rsp_valid_i;
      tr_rd_desc_ready_i = dc_tr_rd_rsp_ready_o;
      tr_rd_rsp_addr_c = tsu_cl_addr_t'(tr_rd_desc_meta_o[ECI_ADDR_WIDTH-1:0]);
      tr_rd_rsp_eci_hdr = tr_rd_desc_meta_o;
   end : RT_TRMGR_IP_ASSIGN
   
   rd_trmgr #
     (
      .ID_WIDTH(DESC_ID_WIDTH),
      .META_DATA_WIDTH(ECI_WORD_WIDTH),
      .ADDR_WIDTH(DCU_ADDR_WIDTH)
      )
   rd_trmgr_inst
     (
      .clk(clk),
      .reset(reset),
      // Desc req i/f.
      .desc_id_i(tr_rd_desc_id_i),
      .desc_meta_i(tr_rd_desc_meta_i),
      .desc_addr_i(tr_rd_desc_addr_i),
      .desc_valid_i(tr_rd_desc_valid_i),
      .desc_ready_o(tr_rd_desc_ready_o),
      // Op rd req i/f
      .req_id_o(tr_rd_req_id_o),
      .req_addr_o(tr_rd_req_addr_o),
      .req_valid_o(tr_rd_req_valid_o),
      .req_ready_i(tr_rd_req_ready_i),
      // ip rd rsp i/f
      .rsp_id_i(tr_rd_rsp_id_i),
      .rsp_valid_i(tr_rd_rsp_valid_i),
      .rsp_ready_o(tr_rd_rsp_ready_o),
      // Desc rsp i/f.
      .desc_id_o(tr_rd_desc_id_o),
      .desc_meta_o(tr_rd_desc_meta_o),
      .desc_valid_o(tr_rd_desc_valid_o),
      .desc_ready_i(tr_rd_desc_ready_i)
      );

   // Write transaction manager.
   //   For write operation, store ECI header and
   //   start a write request transaction by
   //   issuing write descriptors.
   //
   //   The ID for write descriptor is the
   //   DCU ID so that can be used for
   //   routing response back to this DCU.
   //
   //   when write response is recd, retrieve
   //   the stored ECI header to complete transaction.
   //
   //   only 1 on-going transaction per DCU at
   //   a time. Granularity of write is at
   //   sub cacheline level. 
   //
   //   flow control managed by dcu_controller.
   //
   // Assigning Inputs:
   //  tr_wr_desc_id_i is 7 bits but
   //  dec_req_addr_tsu_c.dcu_id can be 5/6/7 bits
   //  fill remaining bits of tr_wr_desc_id_i with 0s.
   //
   //  The meta data, addr is always from 
   //  input eci header as read transactions 
   //  are only generated by incoming ECI events.
   //
   //  wr req,rsp i/f is connected to output of DCU.

   always_comb begin : WR_TRMGR_IP_ASSIGN
      tr_wr_desc_id_i = '0;
      tr_wr_desc_id_i[DCU_ID_WIDTH-1:0] = dec_req_addr_tsu_c.dcu_id;
      tr_wr_desc_meta_i = eci_req_hdr_i;
      tr_wr_desc_addr_i = dec_req_addr_o;
      tr_wr_desc_dmask_i = dec_req_dmask_o;
      tr_wr_desc_valid_i = dc_tr_wr_req_valid_o;
      tr_wr_req_ready_i = wr_req_ready_i;
      tr_wr_rsp_id_i = wr_rsp_id_i;
      tr_wr_rsp_bresp_i = wr_rsp_bresp_i;
      tr_wr_rsp_valid_i = wr_rsp_valid_i;
      tr_wr_desc_ready_i = dc_tr_wr_rsp_ready_o;
      tr_wr_rsp_addr_c = tsu_cl_addr_t'(tr_wr_desc_meta_o[ECI_ADDR_WIDTH-1:0]);
      tr_wr_rsp_eci_hdr = tr_wr_desc_meta_o;
   end : WR_TRMGR_IP_ASSIGN
   
   wr_trmgr #
     (
      .ID_WIDTH(DESC_ID_WIDTH),
      .META_DATA_WIDTH(ECI_WORD_WIDTH),
      .ADDR_WIDTH(DCU_ADDR_WIDTH),
      .DATA_WIDTH(ECI_CL_WIDTH),
      .DMASK_WIDTH(ECI_DMASK_WIDTH)
      )
   wr_trmgr_inst
     (
      .clk(clk),
      .reset(reset),
      // Desc req i/f.
      .desc_id_i(tr_wr_desc_id_i),
      .desc_meta_i(tr_wr_desc_meta_i),
      .desc_addr_i(tr_wr_desc_addr_i),
      .desc_dmask_i(tr_wr_desc_dmask_i),
      .desc_valid_i(tr_wr_desc_valid_i),
      .desc_ready_o(tr_wr_desc_ready_o),
      // Op wr req i/f.
      .req_id_o(tr_wr_req_id_o),
      .req_addr_o(tr_wr_req_addr_o),
      .req_strb_o(tr_wr_req_strb_o),
      .req_valid_o(tr_wr_req_valid_o),
      .req_ready_i(tr_wr_req_ready_i),
      // ip rd rsp i/f.
      .rsp_id_i(tr_wr_rsp_id_i),
      .rsp_bresp_i(tr_wr_rsp_bresp_i),
      .rsp_valid_i(tr_wr_rsp_valid_i),
      .rsp_ready_o(tr_wr_rsp_ready_o),
      // Desc rsp i/f.
      .desc_id_o(tr_wr_desc_id_o),
      .desc_meta_o(tr_wr_desc_meta_o),
      .desc_bresp_o(tr_wr_desc_bresp_o),
      .desc_valid_o(tr_wr_desc_valid_o),
      .desc_ready_i(tr_wr_desc_ready_i)
      );

   // ECI transaction manager.
   // When issuing forward downgrades, store
   // the original event that causes the downgrade
   // and mark transaction as on-going.
   // Only 1 forward downgrade transaction allowed
   // per DCU.
   // when transaction is completed, dcu_controller
   // marks transaction as completed and frees
   // resources.
   always_comb begin : ECI_TRMGR_IP_ASSIGN
      tr_eci_alloc_data_i = eci_req_hdr_i;
      tr_eci_alloc_en_i = dc_tr_eci_alloc_en_o;
      tr_eci_free_en_i = dc_tr_eci_free_en_o;
   end : ECI_TRMGR_IP_ASSIGN
   eci_trmgr #
     (
      .DATA_WIDTH(ECI_WORD_WIDTH)
      )
   eci_trmgr1
     (
      .clk(clk),
      .reset(reset),
      .alloc_data_i(tr_eci_alloc_data_i),
      .alloc_en_i(tr_eci_alloc_en_i),
      .free_en_i(tr_eci_free_en_i),
      .alloc_data_o(tr_eci_alloc_data_o),
      .alloc_valid_o(tr_eci_alloc_valid_o)
      );
   
   // Uncomment when supporting forward downgrades
   // // ECI transaction manager. 
   // // and invalidates.
   // // ECI transaction manager for FPGA 
   // // initiated ECI events.
   // // Keeps track of which ECI IDs have 
   // // an on-going transaction.
   // // whenever a new request is to be sent 
   // // by the FPGA, it pics a free ID from here to launch.
   // assign tr_eci_alloc_en_i = dc_tr_eci_alloc_en_o;
   // // whenever a response to previously sent request is recieved,
   // // it will be sunk and the transaction ID would be freed. 
   // assign tr_eci_allocated_id_i = dec_req_trid_o;
   // assign tr_eci_free_en_i = dc_tr_eci_free_en_o;
   // eci_trmgr #
   //   (
   //    .ID_WIDTH(ECI_HREQID_WIDTH)
   //    )
   // eci_trmgr1
   //   (
   //    .clk		(clk),
   //    .reset		(reset),
   //    .alloc_en_i	(tr_eci_alloc_en_i),
   //    .allocated_id_i	(tr_eci_allocated_id_i),
   //    .free_en_i	(tr_eci_free_en_i),
   //    .free_id_o	(tr_eci_free_id_o),
   //    .full_o		(tr_eci_full_o)
   //    );

   
   // Generate output header.
   // The present action is what is 
   // determined by the CCROM.
   // The header from transaction table is 
   // for sinking read/write responses.
   //  this header is got from the rd/wr transaction 
   //  manager if rdda/wdda is in progress.
   //  otherwise get this data from the input header.
   //
   // Assigning Inputs:
   //  This module is enabled by dcu_controller.
   //  Action from CCROM and context from 
   //  dcu_controller specify header
   //  to be sent.
   //  For forward signals:
   //   transaction ID is the DCU ID.
   //   address is the address from input 
   //   or from eci_trmgr as decided by dcu_controller.
   //  There are no induced evictions for
   //  resource maintenance. 

   always_comb begin : GOH_IP_ASSIGN
      goh_en_i = dc_goh_en_o;
      goh_cc_acn_i = cc_action_t'(ccr_next_action_o);
      goh_rdda_sink_in_prgrs_i = dc_ctxt_rdda_o;
      goh_wdda_sink_in_prgrs_i = dc_ctxt_wdda_o;
      if(dc_ctxt_ecitr_o) begin
	 goh_hdr_from_tr_tbl_i = tr_eci_alloc_data_o;
      end else if(dc_ctxt_wdda_o) begin
	 goh_hdr_from_tr_tbl_i = tr_wr_rsp_eci_hdr;
      end else if (dc_ctxt_rdda_o) begin
	 goh_hdr_from_tr_tbl_i = tr_rd_rsp_eci_hdr;
      end else begin
	 goh_hdr_from_tr_tbl_i = eci_req_hdr_i;
      end
      goh_fwd_cl_addr_i = dec_req_addr_o;
      goh_fwd_dmask_i = dec_req_dmask_o;
      goh_fwd_ns_i = 1'b1; // NS is hardcoded to 1.
   end : GOH_IP_ASSIGN

   // Assigning DCU ID as transaction ID for
   // forward requests. 
   // DCU ID WIDTH can be 5/6/7
   // HREQ ID WIDTH is 6.
   generate
      if(DCU_ID_WIDTH < ECI_HREQID_WIDTH) begin
	 always_comb begin : GOH_FWD_GEN_1
	    // DCU ID width is 5, prepend with 0s.
	    goh_fwd_tr_id_i = '0;
	    goh_fwd_tr_id_i[DCU_ID_WIDTH-1:0] = dec_req_addr_tsu_c.dcu_id;
	 end : GOH_FWD_GEN_1
      end else begin
	 always_comb begin : GOH_FWD_GEN_2
	    // DCU ID width is 6 or 7, choose lower 6 bits. 
	    goh_fwd_tr_id_i = '0;
	    goh_fwd_tr_id_i = dec_req_addr_tsu_c.dcu_id[ECI_HREQID_WIDTH-1:0];
	 end : GOH_FWD_GEN_2
      end
   endgenerate
   
   gen_out_header
     gen_out_h_inst
       (
	.clk			(clk),
	.reset			(reset),
	.en_i			(goh_en_i),
	.cc_acn_i		(goh_cc_acn_i),
	.hdr_from_tr_tbl_i	(goh_hdr_from_tr_tbl_i),
	.rdda_sink_in_prgrs_i	(goh_rdda_sink_in_prgrs_i),
	.wdda_sink_in_prgrs_i	(goh_wdda_sink_in_prgrs_i),
	.fwd_tr_id_i		(goh_fwd_tr_id_i),
	.fwd_cl_addr_i		(goh_fwd_cl_addr_i),
	.fwd_dmask_i            (goh_fwd_dmask_i),
	.fwd_ns_i               (goh_fwd_ns_i),
	.eci_hdr_o		(goh_eci_hdr_o),
	.vc_o			(goh_vc_o),
	.pkt_size_o		(goh_pkt_size_o),
	.eci_hdr_invalid_o	(goh_eci_hdr_invalid_o)
	);

   // Controller orchestrating everything.
   always_comb begin : CONTROLLER_IP_ASSIGN
      dc_eci_req_valid_i		= eci_req_valid_i;
      dc_eci_evt_crts_rtg_ntry_i	= dec_req_crts_rtg_ntry_o;
      dc_eci_rsp_2_prv_snt_req_i	= dec_rsp_2_prv_snt_req_o;
      dc_eci_rsp_ready_i		= eci_rsp_ready_i;
      dc_dec_req_invalid_i		= dec_req_invalid_o;
      dc_rtg_busy_i			= tsu_busy_o;
      dc_rtg_hit_i			= tsu_rd_tag_hit_o;
      dc_rtg_full_i			= tsu_rd_tag_set_full_o;
      dc_rtg_nxm_i			= tsu_rd_tag_nxm_o;
      dc_cc_next_action_i		= cc_action_t'(ccr_next_action_o);
      dc_cc_curr_req_i			= cc_req_t'(ccr_curr_active_req_o);
      dc_tr_rd_req_ready_i		= tr_rd_desc_ready_o;
      dc_tr_rd_rsp_valid_i		= tr_rd_desc_valid_o;
      dc_tr_wr_req_ready_i		= tr_wr_desc_ready_o;
      dc_tr_wr_rsp_valid_i		= tr_wr_desc_valid_o;
      dc_tr_eci_full_i			= tr_eci_alloc_valid_o;
   end : CONTROLLER_IP_ASSIGN
   
   dcu_controller
     dcu_controller_inst
       (
	.clk(clk),
	.reset(reset),

	// ECI input event.
	.eci_req_valid_i(dc_eci_req_valid_i),
	.eci_req_ready_o(dc_eci_req_ready_o),
	.eci_req_stall_o(dc_eci_req_stall_o),
	.eci_evt_crts_rtg_ntry_i(dc_eci_evt_crts_rtg_ntry_i),
	.eci_rsp_2_prv_snt_req_i(dc_eci_rsp_2_prv_snt_req_i),

	// ECI output event.
	.eci_rsp_valid_o(dc_eci_rsp_valid_o),
	.eci_rsp_ready_i(dc_eci_rsp_ready_i),

	// Decoder signals.
	.dec_req_invalid_i(dc_dec_req_invalid_i),
	
	// RTG signals.
	.rtg_busy_i(dc_rtg_busy_i),
	.rtg_rd_en_o(dc_rtg_rd_en_o),
	.rtg_wr_en_o(dc_rtg_wr_en_o),
	.rtg_hit_i(dc_rtg_hit_i),
	.rtg_full_i(dc_rtg_full_i),
	.rtg_nxm_i(dc_rtg_nxm_i),

	// MUX select signals to CCROM.
	.cc_sel_inv_ps_o(dc_cc_sel_inv_ps_o),
	.cc_rom_en_o(dc_cc_rom_en_o),
	.cc_next_action_from_table_i(dc_cc_next_action_i),
	.cc_curr_req_i(dc_cc_curr_req_i),

	// RD TRMGR signals.
	.tr_rd_req_valid_o(dc_tr_rd_req_valid_o),
	.tr_rd_req_ready_i(dc_tr_rd_req_ready_i),
	.tr_rd_rsp_valid_i(dc_tr_rd_rsp_valid_i),
	.tr_rd_rsp_ready_o(dc_tr_rd_rsp_ready_o),

	// ECI TRMGR signals.
	.tr_eci_full_i(dc_tr_eci_full_i),
	.tr_eci_alloc_en_o(dc_tr_eci_alloc_en_o),
	.tr_eci_free_en_o(dc_tr_eci_free_en_o),

	// WR TRMGR signals.
	.tr_wr_req_valid_o(dc_tr_wr_req_valid_o),
	.tr_wr_req_ready_i(dc_tr_wr_req_ready_i),
	.tr_wr_rsp_valid_i(dc_tr_wr_rsp_valid_i),
	.tr_wr_rsp_ready_o(dc_tr_wr_rsp_ready_o),

	// Generate output header signals.
	.goh_en_o(dc_goh_en_o),

	// Context signals.
	.ctxt_rdda_o(dc_ctxt_rdda_o),
	.ctxt_wdda_o(dc_ctxt_wdda_o),
	.ctxt_ecitr_o(dc_ctxt_ecitr_o),

	// Stall reasons.
	.stall_prot_o(dc_stall_prot_o),
	.stall_rd_busy_o(dc_stall_rd_busy_o),
	.stall_wr_busy_o(dc_stall_wr_busy_o),
	.stall_eci_tr_busy_o(dc_stall_eci_tr_busy_o),
	.stall_rtg_full_o(dc_stall_rtg_full_o),
	.stall_wait_op_hs_o(dc_stall_wait_op_hs_o),
	
	// Misc signals: errors, debug etc.
	.dirc_state_o(dc_dirc_state_o),
	.error_code_o(dc_error_code_o),
	.error_o(dc_error_o),
	.dbg_latch_en_o(dc_dbg_latch_en)
	);


   // Debug signal assignment 
   always_comb begin : DBG_ASSIGN
      // Request signals 
      if(dc_ctxt_rdda_o | dc_ctxt_wdda_o) begin
	 // The event that caused the RDD, WDD is got from the transaction table.
	 // The VC number and size are not stored so they are all 0s (invalid).
	 if(dc_ctxt_rdda_o) begin
	    dbg_eci_req_hdr_o = tr_rd_rsp_eci_hdr;
	 end else begin
	    dbg_eci_req_hdr_o = tr_wr_rsp_eci_hdr;
	 end
	 dbg_eci_req_vc_o = '0;
	 dbg_eci_req_size_o = '0;
      end else begin
	 // the active event that is being processed
	 dbg_eci_req_hdr_o = eci_req_hdr_i;
	 dbg_eci_req_vc_o = eci_req_vc_i;
	 dbg_eci_req_size_o = eci_req_size_i;
      end
      dbg_eci_req_ready_o = eci_req_ready_o;
      dbg_eci_req_stalled_o = eci_req_stall_o;
      // Response signals
      dbg_eci_rsp_hdr_o = eci_rsp_hdr_o;
      dbg_eci_rsp_vc_o = eci_rsp_vc_o;
      dbg_eci_rsp_size_o = eci_rsp_size_o;
      dbg_eci_rsp_valid_o = eci_rsp_valid_o;
      // Internal dirc context 
      dbg_req_cc_enc_o = dec_req_cc_enc_o;
      dbg_rdda_o = dc_ctxt_rdda_o;
      dbg_wdda_o = dc_ctxt_wdda_o;
      // Stall reasons.
      dbg_stall_prot_o = dc_stall_prot_o;
      dbg_stall_rd_busy_o = dc_stall_rd_busy_o;
      dbg_stall_wr_busy_o = dc_stall_wr_busy_o;
      dbg_stall_eci_tr_busy_o = dc_stall_eci_tr_busy_o;
      dbg_stall_rtg_full_o = dc_stall_rtg_full_o;
      dbg_stall_wait_op_hs_o = dc_stall_wait_op_hs_o;
      // CC signals
      dbg_cc_active_req_o = ccr_curr_active_req_o;
      dbg_cc_present_state_o = ccr_curr_state_i;
      dbg_cc_next_state_o = ccr_next_state_o ;
      dbg_cc_next_action_o = ccr_next_action_o;
      // Debug error codes. 
      dbg_error_o = dc_error_o;
      dbg_err_code_o = dc_error_code_o;
      // Enable latching signal when handshake happens in input request
      // or output response 
      dbg_valid_o = (eci_req_valid_i & eci_req_ready_o) | (eci_req_valid_i & eci_req_stall_o) | (eci_rsp_valid_o & eci_rsp_ready_i) | dc_dbg_latch_en;
   end : DBG_ASSIGN


   // Performance counters.
   // if SYNTH_PERF_REGS is 1 then synthesize performance registers else dont bother. 
   generate
      if(SYNTH_PERF_REGS == 1) begin
	 always_ff @(posedge clk) begin : PERF_COUNTERS
	    if(eci_req_valid_i & eci_req_ready_o) begin
	       prf_no_req_pkt_rdy_o <= prf_no_req_pkt_rdy_o + 64'd1;
	    end
	    if(eci_req_valid_i & eci_req_stall_o) begin
	       prf_no_req_pkt_stl_o <= prf_no_req_pkt_stl_o + 64'd1;
	    end
	    if(eci_rsp_valid_o & eci_rsp_ready_i) begin
	       prf_no_rsp_pkt_sent_o <= prf_no_rsp_pkt_sent_o + 64'd1;
	    end
	    if(rd_req_valid_o & rd_req_ready_i) begin
	       prf_no_rd_req_o <= prf_no_rd_req_o + 64'd1;
	    end
	    if(rd_rsp_valid_i & rd_rsp_ready_o) begin
	       prf_no_rd_rsp_o <= prf_no_rd_rsp_o + 64'd1;
	    end
	    if(wr_req_valid_o & wr_req_ready_i) begin
	       prf_no_wr_req_o <= prf_no_wr_req_o + 64'd1;
	    end
	    if(wr_rsp_valid_i & wr_rsp_ready_o) begin
	       prf_no_wr_rsp_o <= prf_no_wr_rsp_o + 64'd1;
	    end
	    if(eci_req_valid_i) begin
	       if(eci_req_ready_o) begin
		  prf_no_cyc_bw_req_vld_rdy_o <= '0;
	       end else begin
		  prf_no_cyc_bw_req_vld_rdy_o <= prf_no_cyc_bw_req_vld_rdy_o + 64'd1;
	       end
	    end
	    if( reset ) begin
	       // Make sure unnecessary datapath registers are not reset 
	       prf_no_req_pkt_rdy_o <= '0;
	       prf_no_req_pkt_stl_o <= '0;
	       prf_no_rsp_pkt_sent_o <= '0;
	       prf_no_rd_req_o <= '0;
	       prf_no_rd_rsp_o <= '0;
	       prf_no_wr_req_o <= '0;
	       prf_no_wr_rsp_o <= '0;
	       prf_no_cyc_bw_req_vld_rdy_o <= '0;
	    end
	 end : PERF_COUNTERS
      end else begin
	 always_comb begin : NO_PERF_REGS
	    prf_no_req_pkt_rdy_o = '0;
	    prf_no_req_pkt_stl_o = '0;
	    prf_no_rsp_pkt_sent_o = '0;
	    prf_no_rd_req_o = '0;
	    prf_no_rd_rsp_o = '0;
	    prf_no_wr_req_o = '0;
	    prf_no_wr_rsp_o = '0;
	    prf_no_cyc_bw_req_vld_rdy_o = '0;
	 end : NO_PERF_REGS
      end
   endgenerate
endmodule // dcu

`endif
