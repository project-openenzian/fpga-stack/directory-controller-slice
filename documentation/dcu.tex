\section{Directory Controller Unit (DCU)}
\label{sec:dcu}

\subsubsection{Introduction}
\label{sec:dcu_intro}

% What is DCU.
The \gls{dcu} is responsible for providing coherent
access to a pre-defined subset of the \gls{fpga}
address space. The \gls{dcu} sits in the control-path
of the \gls{dcs} and deals with coherence transaction
headers and memory transaction control signals. It
also determines the order in which these events 
are handled. Each \gls{dcu} implements a portion
of the directory which stores the present state
of a \gls{cl}. It also implements the coherence
protocol which takes a coherence event and
present state of a \gls{cl} to specify the next state
and action to be performed.

% Basic operation of DCU.
Performance of the \gls{dcs} is achieved by running
multiple \glspl{dcu} in parallel. Hence the design
of a \gls{dcu} is simple, un-pipelined and optimized
for resources. The basic operation of a \gls{dcu} is as
follows: Of the multiple incoming channels, the
\gls{dcu} chooses one event to handle at a time.
The event gets decoded to identify the type of event
and the \gls{cl} address. The address is then looked
up in the directory to get the present state of
the \gls{cl}. The event type and present state
are passed on to the coherence protocol which
provides the next state and action to be performed.
The \gls{dcu} finally performs the action and
updates the directory with the new state.
There are six types of actions performed by the \gls{dcu}:
\textbf{no-action} (just updating the directory),
initiating \textbf{read} transactions with memory,
initiating \textbf{write} transactions with memory,
initiating \textbf{forward-downgrade} transactions with CPU,
issuing \textbf{responses headers} to CPU/\gls{fpga}
initiated coherence transactions,
and \textbf{delaying} an incoming coherence header to be
retried later. The basic operation for an event is
\emph{atomic}; All steps in the basic operation are
completed for an event before the next event is chosen.
The basic operation is also \emph{non-blocking}; Once
a basic operation is completed, a new event \textbf{will}
be chosen by the \gls{dcu} to handle.  

The interface to the \gls{dcu} is shown in \autoref{fig:dcu_if}.
In addition to the memory control interface, it has
one incoming and one outgoing channel to exchange coherence
headers with all \glspl{vc} (both \gls{eci} and local).

% DCU Interface.
\begin{figure}[htbp]
  \centering
  \includegraphics[scale=0.5]{figures/dcu_if}
  \caption{DCU Interface}
  \label{fig:dcu_if}
\end{figure}


% deadlock avoidance.
The \gls{dcu} has finite resources and has to
deal with resource contention. The directory
and buffers in memory hierarchy are examples
of finite resources available to the \gls{dcu}.
As such there is a possibility that a \gls{dcu}
is unable to handle the chosen event at that
point in time. Since resources get freed
only when \gls{dcu} handles coherence events,
deadlocks can occur if the \gls{dcu} stalls
waiting for a resource. For example, consider
the scenario where a \gls{dcu} wants to issue
a read request but the memory is not ready to
accept new requests. If the \gls{dcu} stalls
waiting for the memory to accept its request,
it will not be able to sink read responses
that might free up memory resources to handle
new requests. \emph{Thus the \gls{dcu} should
  never stall and be able to skip an
  event that cannot be handled, to try a different
  event}.
Furthermore, the last message in a chain of
dependencies, the response events in this case,
always free up internal resources and retire
transactions. For example, handling a response
to a memory request allows the \gls{dcu}
to initate a new memory transaction. \emph{It
  is the responsibility of the coherence protocol
  to always sink response events and the \gls{dcu}
  prioritizes response channels to aggressively free
  resources.}

That said, the current \gls{dcu} can potentially
deadlock for two incorrect assumptions: Stalling
when unable to issue an outgoing coherence header
and having only one outgoing channel for all \glspl{vc}.
Stalling is an obvious reason for deadlocks as
described in the previous section. Having only
one outgoing channel for all \glspl{vc} can lead
to headers of different classes stuck behind
each other. Future design changes should reconsider
the assumptions. Although, due to the CPU's resilient
credit-based flow control, we have never run into
a deadlock (even under heavy loads) in practice.

% Coherece protocol
The \gls{dcu} also implements the coherence protocol
that, given a coherence event and present-state
of a \gls{cl}, determines the next state and
action to be performed.
Although the CPU implements a full MOESI protocol,
the \gls{dcu} is free to implement a subset of
the protocol that is suitable to its needs. For example,
the \emph{owned} (O) state is required for inter-cache
transfers and since there is no cache on the FPGA
the \gls{dcu} can implement a MESI variant and
still operate seamlessly with the CPU's protocol.
Furthermore, the application requirements can
be used to fine-tune the protocol. For instance,
in an application where only the CPU accesses
\gls{fpga} memory, the \gls{dcu} does not have
to support interactions with coherence controllers
on the FPGA. Thus sevaral variants of the protocol
are possible and it is beneficial to have an
architecture that accomodates this.

%dcu interface
% arbitration logic 
% deadlock avoidance
% skip coherence message

% dcu interface.
\subsubsection{DCU Interface with DCS}
\label{sec:dcu_dcs_if}
The interface to the \gls{dcu} is shown in \autoref{fig:dcu_if}.
In addition to the memory control interface, it has
one incoming and one outgoing channel to exchange
coherence headers with all \glspl{vc} (both \gls{eci}
and local).

\begin{figure}[htbp]
  \centering
  \includegraphics[scale=0.5]{figures/dcu_if}
  \caption{DCU Interface}
  \label{fig:dcu_if}
\end{figure}

% arbitration logic. 
Since there is only one incoming interface for all
\glspl{vc}, arbitration is required between various
\glspl{vc} of the \gls{dcs}. Once a \gls{vc} is
chosen, the \gls{dcu} peeks at the header at the
top of the channel to determine if it can be handled.
The \gls{dcu} would then pop the header off the
\gls{vc} if it can be currently handled, or switch
to a different \gls{vc} if not. This arbitration is
performed by a round-robin arbiter as shown in
\autoref{fig:dcu_top}, with a special skip signal
to switch \glspl{vc} without popping. It is to be
noted that skipping a \gls{vc} would cause head-of-line
blocking in the skipped channel which might affect
performance.

On the outgoing \gls{vc} interface, the \gls{dcu}
issues coherence headers (no data), with the
\gls{vc} number determining the exact \gls{vc} to
which the header has to be routed to.

Finally memory transactions are initiated and retired through
the memory control interface. \emph{To keep the design simple,
  each \gls{dcu} has one outstanding read and one outstanding
  write transaction at a time}. This means that a
\gls{dcu} issues a second memory request only after
the response for the first memory request is received.
The outstanding memory transaction is always tagged
with the \gls{dcu-id} to facilitate routing of responses. 

\begin{figure}[htbp]
  \centering
  \includegraphics[scale=0.5]{figures/dcu_top}
  \caption{DCU top instantiating a DCU}
  \label{fig:dcu_top}
\end{figure}

All incoming and outgoing channels are registered
using pipeline stages. These buffers isolate the \gls{dcu}
and reduce head-of-line blocking on the \gls{dcs}
\glspl{vc} by holding coherence headers as they are
being handled. 

\subsubsection{DCU Operation}
\label{sec:dcu_op}
The basic operation of \gls{dcu} described in
\autoref{sec:dcu_intro} gives us an idea of the
components required in the \gls{dcu}: an
event decoder, a directory to store and retrieve
\gls{cl} state information, a protocol look-up
table, individual components for each action to
be performed and a controller that orchestrates
everything. These components are shown in
\autoref{fig:dcu_arch}

\begin{figure}[htbp]
  \centering
  \includegraphics[scale=1]{figures/dcu_arch}
  \caption{DCU Architecture}
  \label{fig:dcu_arch}
\end{figure}


The event decoder takes in the coherence header
and uses the opcode and \gls{vc} number to identify
the coherence event and uses this information
to extract its bit-fields including the transaction
ID, dmask and \gls{cl} address.

In addition to the decoded coherence message,
there can be response events from memory
transactions waiting to be processed. The
\gls{dcu} prioritizes these responses over
the coherence message (for reasons discussed in
\autoref{sec:dcu_intro}) to select one coherence
event and pass its \gls{cl} address to the directory. 

The set and tag information from the chosen \gls{cl}
address is passed to the directory (also called
\gls{tsu}). The set information is used to index
into the directory and the tag information is
compared with tags in the ways to check if there
is a match. If a match is found, the directory
retrieves the present-state of the \gls{cl}
along with the way in which the hit occurs.
Not finding a matching tag means the
\gls{cl} is not cached anywhere in the system
and directory returns the present-state as
\emph{Invalid}(I). The way information returned by
the directory in the case of a hit, would be used
later when updating the state of the \gls{cl} in the
directory. 

The present-state of the \gls{cl} and the chosen
coherence event are then looked up the coherence
protocol table (\gls{cc-rom}) to get the next-state
and action to be performed. The action is then
coordinated by the controller. Having the coherence
protocol as a look-up table allows us to switch
coherence protocols without changing the rest of
the design. 

For actions that require initiating a new
transaction, the controller invokes one of three
\glspl{tm}: read \gls{tm}, write \gls{tm} and
\gls{eci} \gls{tm}. Each \gls{tm} stores the
original coherence header that caused the
transaction and initiates a single outstanding
transaction. The \glspl{tm} also indicates to
the controller when it is busy so the controller
can delay all events that require this
resource. Once the transaction is retired
(response is received), the \gls{tm} retrieves
the stored header, generates a completion
event and is now ready to issue a new transaction.
The completion event is then eventually handled
by the \gls{dcu} as dictated by the coherence
protocol. It is to be noted, that all transactions
initated by the \glspl{tm} are always tagged with
the \gls{dcu-id} to facilitate routing of the
remaining chain of coherence events. This also
helps decouple coherence transactions from memory
transactions as they have different IDs.

Coherence headers have to be issued when initiating
a forward-downgrade or responding to a previously
initiated coherence transaction. These headers
are generated by an encoder module enabled by
the controller. The controller identifies the exact
coherence message to be sent based on the action
prescribed by the coherence protocol. It also
provides information required to generate the header
such as coherence transaction ID and \gls{cl}
address. The encoder generates the header along
with the \gls{vc} number and issues it via the
outgoing channel.

Finally, for all actions except delaying a coherence
event, the directory gets updated with the next-state
defined by the coherence protocol. The directory
gets updated with the present-state when an event
is to be retried later. 

\subsubsection{Protocol Scenarios}
\label{sec:dcu_prot_pathway}
In this section, we see how the \gls{dcu} handles
specific coherence transactions. The scenarios
described here are for the default specification of
the coherence protocol and can vary if the protocol
specification changes.

The first scenario is when CPU issues a request to
upgrade a \gls{cl} from \emph{Invalid} (I) to either
\emph{Shared} (S) or \emph{Exclusive} (E). In this
scenario, the \gls{dcs} needs to send a acknowledge
with data. The \gls{dcs} stores the request header
and initiates a read transaction. Eventually when
the read response is received, 


When the request header is received by the
\gls{dcu}, it stores the header and initiates a read
transaction. Eventually when the read response is
receieved, the \gls{dcu} uses information from the
stored header to generate the protocol specified
response header. The response header and data are
then issued to the CPU by the \gls{dcs}.


The second scenario is when CPU issues a request to
upgrade a \gls{cl} from \emph{Shared} (S) to
\emph{Exclusive} (E). In this scenario, the
\gls{dcs} needs to send an acknowledge without
data. When the request header is receieved by
the \gls{dcu}, it uses the information from
the header to generate the protocol-specified
response header. The response header is then
issued to the CPU by the \gls{dcs} and the
transaction is retired. 

The third scenario is voluntary downgrades.
Voluntery downgrades without data (downgrade
from \emph{Shared}(S) to \emph{Invalid} (I)
do not require any action to be performed by
the \gls{dcs}. The directory gets updated with
the protocol specified next-state for this
\gls{cl} and the transaction is retired.




from
\emph{Shared} (S) to \emph{Invalid} (I). These
downgrade messages do not have any data. The
\gls{dcu} only has to update the directory with
the next-state for this \gls{cl}.

The fourth scenario is voluntary downgrades from





The second scenario is when CPU issues a request
to upgrade a \gls{cl} from


The
\gls{dcu} stores the incoming header and initiates
a read transaction. Eventually when the read response
is received, the \gls{dcu} uses information from
stored header to generate the protocol specified response
and issues it to the CPU. 

In this scenario, the
\gls{dcu} stores the coherence header and initates a
read transaction. Eventually when the read response
is received, the \gls{dcu} uses the information in
the stored header to generate the protocol specified
response header and issues it to the CPU.

The second scenario is when CPU issues a request to
upgrade a \gls{cl} from \emph{Shared} (S) to
\emph{Exclusive} (E). 



The simplest scenario is for voluntary downgrades
without data from the CPU. In this case, no action
has to be performed other than updating the
directory with the protocol defined next-state.

For voluntary downgrades with data, 


The first pathway is for coherence events that
do not require any action to be performed. 

The first pathway is when an incoming coherence
event does not require any action to be performed.
In the default version of the protocol, this happens
for voluntary downgrades from the CPU without data
and for write responses from memory. Updating the
state of the \gls{cl} in the directory with the
protocol-specified next state completes this path.

The second pathway is when an incoming coherence
header causes an outgoing coherence message without
data (no memory transactions initiated). In the
default version of the protocol, this happens when
the CPU upgrades a \gls{cl} from \emph{Shared} (S)
to \emph{Exclusive} (E) or when the \gls{fpga} coherence
controller cleans (or clean-invalidates) an already
clean (or invalid) \gls{cl}. In this pathway, the
protocol specifies the exact message to be sent and
bit-fields from the incoming header are used to
fill in  the response header. Once the response is
sent, the directory is updated with the next-state
and the pathway is completed.

The third pathway is for coherence events that
initiate a memory transaction. Memory read transactions
are initiated when the CPU issues a request to upgrade a
\gls{cl} from \emph{Invalid} (I) to either \emph{Shared}
(S) or \emph{Exclusive} (E). Memory write transactions
are initiated for voluntary-downgrades or forward-downgrade
responses with dirty data. In this pathway, the \gls{dcu}
stores the causal header and initiates a memory transaction.
The pathway is complete when the memory request is issued
and the directory is updated with the next-state.

The fourth pathway is for memory response events that
cause an outgoing coherence message. In the default
version of the protocol, rea




. These requests require a response with data and
the protocol determines the exact message to be sent. 
When the coherence header is received, the \gls{dcu}
stores the header, initiates a read transaction and
updates the state of the \gls{cl} in the directory.
When a read response is received, the causal header
is retrieved and its contents are used to the generate
the response message specified by the protocol. The
response message is then sent to the CPU and updates
are made to the directory to reflect the new state
of the \gls{cl}.

The second pathway is for coherence requests from
the CPU to upgrade a \gls{cl} from \emph{Shared} (S)
to \emph{Exclusive}. Since the CPU already has the
\gls{cl} data in its cache, the \gls{dcu} only has
to send an acknowledge response without data. The
protocol specifies the response message to send and
the request coherence header is used to fill in
the bit-fields of the response header. Once the
response is sent, the \gls{cl} state in the directory
is updated to exclusive. 



% TBD
% flexibility of coherence protocol and local interface.
% given this flexibility we assume the version of coherence
% protocol that provides 

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
