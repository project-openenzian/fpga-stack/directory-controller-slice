/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-07-14
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */

`ifndef PIPELINE_STAGE_SV
`define PIPELINE_STAGE_SV

// Pipeline stages without flow control.
// No reset on pipeline registers. 
module pipeline_stage #
  (
   parameter NUM_STAGE = 5,
   parameter DATA_WIDTH = 64
   )
   (
    input logic 		  clk,
    input logic [DATA_WIDTH-1:0]  data_i,
    output logic [DATA_WIDTH-1:0] data_o
    );

   // if NUM_STAGE is 5,
   // stage 0 is input stage.
   // stage 1,2,3,4,5 are registered stages.
   // so adjusting the number of stages to 6.
   // stage 5 is output stage. 
   localparam NUM_STAGE_IP_ADG = NUM_STAGE+1;
   
   logic [DATA_WIDTH-1:0] 	  stage_reg[NUM_STAGE_IP_ADG-1:0];
   assign stage_reg[0] = data_i;
   assign data_o = stage_reg[NUM_STAGE_IP_ADG-1];
   
   genvar i;
   generate
      for( i = NUM_STAGE_IP_ADG-1; i > 0; i--) begin : PIPE_INST
	 always_ff @(posedge clk) begin : REG_ASSIGN
	    stage_reg[i] <= stage_reg[i-1];
	 end : REG_ASSIGN
      end : PIPE_INST
   endgenerate
endmodule // pipeline_stage

`endif
