/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-06-24
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */


`ifndef RD_TRMGR_SV
`define RD_TRMGR_SV


/*
 * Module Description:
 * Read request and response handling.
 *  when read request is issued, ECI HDR
 *  is stored.
 *  when read response is recd, the stored
 *  ECI Hdr are retrieved.
 *
 * There can be only 1 on-going read transaction at
 * a time. So first issue request then wait for response
 * before issuing the next request.
 * 
 * The issued read request will always have the DCU ID
 * as the request ID. Note: this is not the ECI ID.
 * 
 * Read data does not pass through the tr mgr.
 * Only header passes through.
 * data is always 128 bytes, cannot read lesser. 
 *
 * Input Output Description:
 *  Input Descriptor I/F:
 *   Recieve ECI ID, Address and ECI header.
 *  Output Read Req I/F:
 *   Send read request doewnstream.
 *   Note: request ID is always DCU ID. 
 *  Input Read Response I/F
 *   Read response with just ID (DCU ID).
 *   Data is not passed through here. 
 *  Output Descriptor I/F.
 *   Retrieved ECI ID, ECI header that
 *   corresponds to the response. 
 *
 * Architecture Description:
 *  State machine switches from ready to accept 
 *  request descriptor to ready to accept response. 
 *  
 *  Valid ready signals are not registered and 
 *  are just wired across. 
 * 
 *
 * Modifiable Parameters:
 *  DCU ID: Make sure each DCU has unique ID 
 *  this is necessary for routing. 
 *
 * Non-modifiable Parameters:
 *  None.
 * 
 * Modules Used:
 *  None.
 *
 * Notes:
 *  Make sure DCU_ID is unique for each DCU.
 *
 */

module rd_trmgr #
  (
   parameter ID_WIDTH = 7,
   parameter META_DATA_WIDTH = 64,
   parameter ADDR_WIDTH = 38
   )
   (
    input logic 		       clk,
    input logic 		       reset,
    // DCU_ID + meta data (ECI hdr) 
    // + addr to read 
    input logic [ID_WIDTH-1:0] 	       desc_id_i,
    input logic [META_DATA_WIDTH-1:0]  desc_meta_i,
    input logic [ADDR_WIDTH-1:0]       desc_addr_i,
    input logic 		       desc_valid_i,
    output logic 		       desc_ready_o,

    // Read request.
    // request ID (DCU_ID), addr to read from.
    // data is always 128 bytes, cannot read lesser. 
    output logic [ID_WIDTH-1:0]        req_id_o,
    output logic [ADDR_WIDTH-1:0]      req_addr_o,
    output logic 		       req_valid_o,
    input logic 		       req_ready_i,

    // Read response.
    // data is not routed through DCUs.
    input logic [ID_WIDTH-1:0] 	       rsp_id_i,
    //input logic [DATA_WIDTH-1:0]       rsp_data_i,
    input logic 		       rsp_valid_i,
    output logic 		       rsp_ready_o,

    // Output DCU_ID and header corresponding to response.
    output logic [ID_WIDTH-1:0]        desc_id_o,
    output logic [META_DATA_WIDTH-1:0] desc_meta_o,
    //output logic [DATA_WIDTH-1:0]      desc_data_o,
    output logic 		       desc_valid_o,
    input logic 		       desc_ready_i
    );

   enum {REQ_ACCEPT, RSP_ACCEPT} state_reg, state_next;

   // registers to store meta data and id.
   logic [META_DATA_WIDTH-1:0] desc_meta_reg = '0, desc_meta_next;
   
   // Output read request interface.
   // Wiring the desc_i i/f directly to the req i/f.
   // The request ID is always DCU id.
   // Flow control is enabled in these i/fs only 
   // when new requests can be accepted.
   assign req_id_o = desc_id_i;
   assign req_addr_o = desc_addr_i;
   assign req_valid_o = (state_reg == REQ_ACCEPT) ? desc_valid_i : 1'b0;
   assign desc_ready_o = (state_reg == REQ_ACCEPT) ? req_ready_i : 1'b0;

   // Output read response dec i/f.
   // Retrieve ECI Hdr (there is only 1 on-going transaction).
   // State machine determines when this is accepted. 
   assign desc_id_o = rsp_id_i;
   assign desc_meta_o = desc_meta_reg;
   assign desc_valid_o = (state_reg == RSP_ACCEPT) ? rsp_valid_i : 1'b0;
   assign rsp_ready_o = (state_reg == RSP_ACCEPT) ? desc_ready_i : 1'b0;

   // Latch the desc meta data (ECI hdr)
   // whenever a handshake happens in the desc i/f.
   always_comb begin : IP_REG
      desc_meta_next	= desc_meta_reg;
      if(desc_valid_i & desc_ready_o) begin
	 desc_meta_next = desc_meta_i;
      end
   end : IP_REG

   // State machine:
   // Only 1 transaction can be on-going at a time.
   // Wait to accept request, when request is accepted
   // go to accepting response.
   // Once response is accepted go back to accepting
   // requests. 
   always_comb begin : CONTROLLER
      state_next = state_reg;
      case(state_reg)
	REQ_ACCEPT: begin
	  if(desc_valid_i & desc_ready_o) begin
	     state_next = RSP_ACCEPT;
	  end
	end
	RSP_ACCEPT: begin
	   if(rsp_valid_i & rsp_ready_o) begin
	      state_next = REQ_ACCEPT;
	   end
	end
      endcase
   end : CONTROLLER

   always_ff @(posedge clk) begin : REG_ASSIGN
      desc_meta_reg <= desc_meta_next;
      state_reg     <= state_next;
      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset 
	 state_reg     <= REQ_ACCEPT;
      end
   end : REG_ASSIGN

endmodule // rd_trmgr
`endif
