
\section{Introduction}
\label{sec:introduction}
% coherence controllers,
% coherence messages,
% memory events.
Coherence controllers refer to controllers that
exchange coherence messages with each other.
For example, the \gls{llc} on the CPU and
\gls{dc} on the \gls{fpga} are coherence controllers.
Coherence controllers on the CPU interact with
the \gls{dc} through \gls{eci} and controllers
on the \gls{fpga} have a local interface on the
\gls{dc} to interact with it. In addition to other
coherence controllers, the \gls{dc} also deals
with memory events, that is, issuing read/write
requests to memory and receiving responses.
Coherence messages and memory events are together
refered to as coherence events. 

% message classes and virtual channels. 
Coherence messages are classified into message
classes with each class having its own \gls{vc}
in order to avoid message-level deadlocks due
to messages of different classes blocking
each other. \glspl{vc} are essentially
\glspl{fifo} managed by credit-based flow
control in \gls{eci} and by valid-ready
flow control on the \gls{fpga}. Each \gls{vc}
has a number that can be used to identify
the message class. Depending on the message
class, a coherence message either contains
a 64-bit coherence header and 128-byte
payload or only a 64-bit coherence header.

All coherence headers have the following
bit-fields in common: a 5-bit opcode, a
5/6-bit transaction ID, a 4-bit dmask
(data mask) and a 40-bit \gls{cl} byte address.
The opcode and the \gls{vc} number are used
to uniquely identify a coherence message and 
dmask is used to identify the size of the payload.
In addition to these, there are also message
specific bit-fields for different messages.

Coherence controllers can initiate coherence
transactions on a \gls{cl},
which are a chain of coherence messages towards
a specific goal. For example, the \gls{dc}
can initiate a transaction with the CPU
to clean-invalidate a \gls{cl} to which the
CPU will respond. Each transaction is
associated with a \gls{cl} address and is tagged
with a transaction ID. All coherence messages
in a transaction will have the same transaction
ID and there can be multiple outstanding transactions
on a \gls{cl}.

In addition to coherence transactions, there are
also memory transactions on a \gls{cl} made up of read
request-response and write request-response pairs.
These transactions are also tagged with an ID
which has no relation to a coherence transaction ID.

% request vs response events. 
Both coherence and memory transactions form
a chain of dependency of events, beginning
with a request and retiring with a response.
This classifies coherence events further
into request events and response events.

\begin{itemize}
\item \colorbox{pink}{Request, response, forward VCs.}
\item \colorbox{pink}{read request, response channel.}
\item \colorbox{pink}{write request, response channel.}
\item \colorbox{pink}{request and response events.}
\item \colorbox{pink}{Coherence protocol and local interface are flexible.}
\item \colorbox{pink}{Assumptions on the coherence protocol (R23 does not require read)}
\item \colorbox{pink}{Assumptions on local interface and actions provided.}
\end{itemize}


\section{Directory Controller}
\label{sec:dc}
The \gls{dc} on the \gls{fpga} provides coherent
access to the \gls{fpga} attached memory. 

\section{Directory Controller Architecture}
\label{sec:dc_arch}
The \gls{llc} of CPU is 16-way set-associative and has
a capacity of 16MB. This is 128K \glspl{cl}.
In order to avoid conflict misses and round trip
invalidations, the \gls{dc} should be
able to hold the states of atleast 128K \glspl{cl}. These
states are held in the directory of the \gls{dc}.

Thus the directory of the \gls{dc} can be viewed as a giant
table where each set (row) has 16 ways (columns) and each
cell stores the state of 1 \gls{cl}. There are ${2^{13}}$
sets to store states of all 128K \glspl{cl}. The \gls{cl}
address defines the set into which the state of the \gls{cl}
would be stored and the \gls{dc} is free to place it in any of the
ways available in this set. A tag is stored along with the state
in a way to identify the \gls{cl} to which the state corresponds
to. This is shown in \autoref{dirc_gen_table}.

\begin{figure}[htbp]
  \centering
  \includegraphics[scale=0.5]{figures/dirc_gen_table.png}
  \caption{Directory of \gls{dc}}
  \label{dirc_gen_table}
\end{figure}


\subsection{Cache Line Address}
\label{sec:cl_address}
The \gls{dc} exposes 256GiB of coherent address space
on the \gls{fpga}. Addressing this space would require
38 bit \gls{cl} byte address. \gls{eci} allows
for a maximum of 40 bit address of which lower 38 bits are used
to address the byte addressable memory.

The 38-bit \gls{cl} byte address is further split into
\begin{itemize}
\item 7-bit byte offset.
\item 13-bit set index.
\item 18-bit tag.
\end{itemize}
A \gls{cl} can store 128 bytes, the 7-bit byte offset indicates
the byte addressed within a \gls{cl}.
The 13-bit set index identifies which of the ${2^{13}}$ sets corresponds
to this \gls{cl}. The remaining 18-bits are tag information that is stored
in the directory along with the state in a way to identify the \gls{cl}
corresponding to the state.

In the current configuration, a state can be 18 bits wide. This along with
an 18-bit tag is stored in each way of a given set, making each way in a set
to be 36 bits wide as seen in \autoref{dc_table_addr}.

\begin{figure}[htbp]
  \centering
  \includegraphics[scale=0.5]{figures/dc_table_addr.png}
  \caption{\gls{cl} address to directory table}
  \label{dc_table_addr}
\end{figure}

\subsection{Directory Controller Unit}
\label{sec:directory_controller_unit}
The state transitions on a given \gls{cl} is indepenedent of
the state transitions on other \glspl{cl} within the directory
protocol. This enables us to split the directory across multiple
\glspl{dcu} with each \gls{dcu} having a disjoint
subset of the sets and combined together holds all sets in the directory.

In current implementation, the number of sets within a \gls{dcu}
can be configured between 64/128/256 sets-per-\gls{dcu}. Thus to hold all
${2^{13}}$ sets we would need 128/64/32 \glspl{dcu} depending on the number
of sets-per-\gls{dcu}.

Thus the 13-bit set index is further split into 7/6/5-bit \emph{\gls{dcu-id}} that
chooses the \gls{dcu} corresponding to this set and 6/7/8-bit set-within-\gls{dcu}
to identify the set within a given \gls{dcu} as seen in \autoref{dcu_addr}.

\begin{figure}[htbp]
  \centering
  \includegraphics[scale=0.5]{figures/dcu_addr.png}
  \caption{\Gls{cl} address to directory table}
  \label{dcu_addr}
\end{figure}

Furthermore, \gls{eci} automatically splits the FPGA address space into
odd and even \gls{cl} indices. Thus to take advantage of this,
the \glspl{dcu} are organized onto odd and even \glspl{dcs} in a \gls{dc} tile, each containing
one half of the total number of \glspl{dcu} required in the system (ie each
\gls{dcs} contains 64/32/16 \glspl{dcu}). The \gls{dcu-id} in this case is split
into 1-bit odd/even \gls{dcs} identifier and remaining 6/5/4b to identify
the \emph{\gls{dcu-idx}} within a \gls{dcs} as seen in\autoref{dcs_addr}.

\begin{figure}[htbp]
  \centering
  \includegraphics[scale=0.5]{figures/dcs_addr.png}
  \caption{\gls{dc} Tile and slices}
  \label{dcs_addr}
\end{figure}

Summarizing, the \gls{dc} is made up of a single \gls{dc} tile and has
2 \glspl{dcs}, one for odd CL indices and
other for even CL indices. A \gls{dc} tile accounts for 256 GiB of the
FPGA address space and has a 16-way set-associative directory, with ${2^{13}}$
sets, big enough to match the caching capacity of the CPU. Each
\gls{dcs} has a number of \glspl{dcu}, with each \gls{dcu} responsible for
a disjoint sub-set of the 256 GiB address space. 

\subsection{Directory Resource Maintenance}
One implication of having no caching capability on
the \gls{fpga} is that  coherence operations on
the \gls{fpga} cannot create an entry in the directory.
Only the CPU can add entries to the directory and
operators on the \gls{fpga} can read or modify them.
By matching the set-associativity of the directory with
that of the CPU's cache, the \gls{dc} relies on the
CPU to maintain the limited directory resource. For
example, when a set in the CPU's cache is full, the
set in the \gls{dc}'s directory is also full. If the
CPU wants to cache a new \gls{cl} to this set, it would
have to free up a way in the set by evicting a previously
cached \gls{cl} from its cache. This eviction would also free up
a way in directory's set thereby maintaining the directory.
Although this design choice simplifies \gls{dc} design,
it limits the operations that can be carried out by the
\gls{fpga} to clean and clean-invalidations. Additional
features like locking a \gls{cl} requires the \gls{fpga}
to create entries in the directory and the \gls{dc}
would have to perform resource maintenance. These
will be discussed in 
\textbf{\textcolor{violet}{\colorbox{pink}{limitations section}}}.




\subsection{Non Existant Memory of \gls{dc} Tile}
\label{sec:dc_tile_nxm}
A \gls{dc} tile accounts for 256 GiB of the FPGA address space and has
a directory big enough to match the caching capacity of the CPU.
In order to optimize on the performance of the directory, a portion
of the address space where the 18-bit tag is all 1s is considered
to be non-existant. Why we need this ``non-existant'' region within
the 256 GiB of address region is described in \autoref{sec:why_tsr_nxm}.
Thus we have a 1 MiB region in the end of the 256 GiB region that
is considered to be non-existant as shown in \autoref{dc_tile_nxm}.
Any access to this region would result in an error. 

\begin{figure}[htbp]
  \centering
  \includegraphics[scale=0.5]{figures/dc_tile_nxm.png}
  \caption{\gls{dc} Tile Non existant memory}
  \label{dc_tile_nxm}
\end{figure}


\subsection{Notes on expanding to 40-bit address space}
\label{sec:dc_exp}
\gls{eci} offers a 40-bit (1 TiB) byte address space for homing on the FPGA. The main
reason it is reduced to 38-bits here is to do with how tags and state
are stored in the directory. In the current implementation, tags and
states are stored in a tag-state-ram (TSR) implemented using a BRAM.
This implementation restricts the tags to be atmost 18 bits wide instead
of 20-bit tags required for a 40-bit address space.

This leaves us with two options, either modifying the TSR to allow
for 20-bit tags or have additional \gls{dc} tiles with a much bigger
directory than what is required to satisfy the ${2^{13}}$ sets
caching capacity of the CPU. 
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
