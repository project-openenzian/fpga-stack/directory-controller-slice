#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
$XILINX_VIVADO/data/verilog/src/glbl.v \
../testbench/dist_ramTb.sv \
../rtl/dist_ram.sv 

xelab -debug typical -incremental -L xpm worklib.dist_ramTb worklib.glbl -s worklib.dist_ramTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.dist_ramTb 
xsim -R worklib.dist_ramTb 
