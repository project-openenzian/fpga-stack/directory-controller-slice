#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
      $XILINX_VIVADO/data/verilog/src/glbl.v \
      ../testbench/axis_2_routerTb.sv \
      ../rtl/axis_2_router.sv \
      ../rtl/axis_pipeline_stage.sv 

xelab -debug typical -incremental -L xpm worklib.axis_2_routerTb worklib.glbl -s worklib.axis_2_routerTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.axis_2_routerTb 
xsim -gui worklib.axis_2_routerTb 
