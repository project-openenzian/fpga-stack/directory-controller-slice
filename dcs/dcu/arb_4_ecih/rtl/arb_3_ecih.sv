`ifndef ARB_3_ECIH_SV
`define ARB_3_ECIH_SV

import eci_cmd_defs::*;

module arb_3_ecih 
  (
   input logic 					clk,
   input logic 					reset,
   // Input ECI header 1
   input logic [ECI_WORD_WIDTH-1:0] 		eci_hdr1_i,
   input logic [ECI_PACKET_SIZE_WIDTH-1:0] 	eci_pkt1_size_i,
   input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	eci_pkt1_vc_i,
   input logic 					eci_pkt1_valid_i,
   output logic 				eci_pkt1_ready_o,
   output logic 				eci_pkt1_stall_o,
   // Input ECI header 2
   input logic [ECI_WORD_WIDTH-1:0] 		eci_hdr2_i,
   input logic [ECI_PACKET_SIZE_WIDTH-1:0] 	eci_pkt2_size_i,
   input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	eci_pkt2_vc_i,
   input logic 					eci_pkt2_valid_i,
   output logic 				eci_pkt2_ready_o,
   output logic 				eci_pkt2_stall_o,
   // Input ECI header 3
   input logic [ECI_WORD_WIDTH-1:0] 		eci_hdr3_i,
   input logic [ECI_PACKET_SIZE_WIDTH-1:0] 	eci_pkt3_size_i,
   input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	eci_pkt3_vc_i,
   input logic 					eci_pkt3_valid_i,
   output logic 				eci_pkt3_ready_o,
   output logic 				eci_pkt3_stall_o,
   // Output ECI header
   output logic [ECI_WORD_WIDTH-1:0] 		eci_hdr_o,
   output logic [ECI_PACKET_SIZE_WIDTH-1:0] 	eci_pkt_size_o,
   output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] eci_pkt_vc_o,
   output logic 				eci_pkt_valid_o,
   input logic 					eci_pkt_ready_i,
   input logic 					eci_pkt_stall_i
   );

   logic 				    eci_pkt1_ready_reg	= '0, eci_pkt1_ready_next;
   logic 				    eci_pkt1_stall_reg	= '0, eci_pkt1_stall_next;
   logic 				    eci_pkt2_ready_reg	= '0, eci_pkt2_ready_next;
   logic 				    eci_pkt2_stall_reg	= '0, eci_pkt2_stall_next;
   logic 				    eci_pkt3_ready_reg	= '0, eci_pkt3_ready_next;
   logic 				    eci_pkt3_stall_reg	= '0, eci_pkt3_stall_next;
   logic [ECI_WORD_WIDTH-1:0] 		    eci_hdr_reg		= '0, eci_hdr_next;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	    eci_pkt_size_reg	= '0, eci_pkt_size_next;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0]    eci_pkt_vc_reg	= '0, eci_pkt_vc_next;
   logic 				    eci_pkt_valid_reg = '0, eci_pkt_valid_next;

   enum {WAIT_FOR_IP, WAIT_FOR_MUX, WAIT_FOR_OP, WAIT_FOR_IP_HS} state_reg = WAIT_FOR_IP, state_next;
   enum {CHOOSE_1_ST, CHOOSE_2_ST, CHOOSE_3_ST} priority_reg = CHOOSE_1_ST, priority_next;
   enum {IN1_CHOSEN, IN2_CHOSEN, IN3_CHOSEN} chosen_in_reg = IN1_CHOSEN, chosen_in_next;

   assign eci_pkt1_ready_o	= eci_pkt1_ready_reg;
   assign eci_pkt1_stall_o	= eci_pkt1_stall_reg;
   assign eci_pkt2_ready_o	= eci_pkt2_ready_reg;
   assign eci_pkt2_stall_o	= eci_pkt2_stall_reg;
   assign eci_pkt3_ready_o	= eci_pkt3_ready_reg;
   assign eci_pkt3_stall_o	= eci_pkt3_stall_reg;
   assign eci_hdr_o		= eci_hdr_reg;
   assign eci_pkt_size_o	= eci_pkt_size_reg;
   assign eci_pkt_vc_o		= eci_pkt_vc_reg;
   assign eci_pkt_valid_o = eci_pkt_valid_reg;

   // MUX
   always_comb begin : DATA_MUX
      eci_hdr_next = eci_hdr_reg;
      eci_pkt_size_next = eci_pkt_size_reg;
      eci_pkt_vc_next = eci_pkt_vc_reg;
      case(chosen_in_reg)
	IN1_CHOSEN: begin
	   eci_hdr_next = eci_hdr1_i;
	   eci_pkt_size_next = eci_pkt1_size_i;
	   eci_pkt_vc_next = eci_pkt1_vc_i;
	end
	IN2_CHOSEN: begin
	   eci_hdr_next = eci_hdr2_i;
	   eci_pkt_size_next = eci_pkt2_size_i;
	   eci_pkt_vc_next = eci_pkt2_vc_i;
	end
	IN3_CHOSEN: begin
	   eci_hdr_next = eci_hdr3_i;
	   eci_pkt_size_next = eci_pkt3_size_i;
	   eci_pkt_vc_next = eci_pkt3_vc_i;
	end
	default: begin
	   eci_hdr_next = eci_hdr_reg;
	   eci_pkt_size_next = eci_pkt_size_reg;
	   eci_pkt_vc_next = eci_pkt_vc_reg;
	end
      endcase
   end : DATA_MUX

   always_comb begin : CONTROLLER
      state_next = state_reg;
      priority_next = priority_reg;
      chosen_in_next = chosen_in_reg;
      eci_pkt1_ready_next = eci_pkt1_ready_reg;
      eci_pkt1_stall_next = eci_pkt1_stall_reg;
      eci_pkt2_ready_next = eci_pkt2_ready_reg;
      eci_pkt2_stall_next = eci_pkt2_stall_reg;
      eci_pkt3_ready_next = eci_pkt3_ready_reg;
      eci_pkt3_stall_next = eci_pkt3_stall_reg;
      eci_pkt_valid_next = eci_pkt_valid_reg;
      case(state_reg)
	WAIT_FOR_IP: begin
	   case(priority_reg)
	     CHOOSE_1_ST: begin
		// Priority given to input 1 
		if(eci_pkt1_valid_i) begin
		   chosen_in_next = IN1_CHOSEN;
		   state_next = WAIT_FOR_MUX;
		end else if (eci_pkt2_valid_i) begin
		   chosen_in_next = IN2_CHOSEN;
		   state_next = WAIT_FOR_MUX;
		end else if (eci_pkt3_valid_i) begin
		   chosen_in_next = IN3_CHOSEN;
		   state_next = WAIT_FOR_MUX;
		end else begin
		   chosen_in_next = chosen_in_reg;
		   state_next = state_reg;
		end
	     end
	     CHOOSE_2_ST: begin
		// Priority given to input 2 
		if(eci_pkt2_valid_i) begin
		   chosen_in_next = IN2_CHOSEN;
		   state_next = WAIT_FOR_MUX;
		end else if (eci_pkt3_valid_i) begin
		   chosen_in_next = IN3_CHOSEN;
		   state_next = WAIT_FOR_MUX;
		end else if (eci_pkt1_valid_i) begin
		   chosen_in_next = IN1_CHOSEN;
		   state_next = WAIT_FOR_MUX;
		end else begin
		   chosen_in_next = chosen_in_reg;
		   state_next = state_reg;
		end
	     end
	     CHOOSE_3_ST: begin
		// Priority given to input 3 
		if(eci_pkt3_valid_i) begin
		   chosen_in_next = IN3_CHOSEN;
		   state_next = WAIT_FOR_MUX;
		end else if (eci_pkt1_valid_i) begin
		   chosen_in_next = IN1_CHOSEN;
		   state_next = WAIT_FOR_MUX;
		end else if (eci_pkt2_valid_i) begin
		   chosen_in_next = IN2_CHOSEN;
		   state_next = WAIT_FOR_MUX;
		end else begin
		   chosen_in_next = chosen_in_reg;
		   state_next = state_reg;
		end
	     end
	     default: begin
		chosen_in_next = chosen_in_reg;
		state_next = state_reg;
	     end
	   endcase
	end

	WAIT_FOR_MUX: begin
	   state_next = WAIT_FOR_OP;
	   eci_pkt_valid_next = 1'b1;
	end

	WAIT_FOR_OP: begin
	   if(eci_pkt_ready_i | eci_pkt_stall_i) begin
	      eci_pkt_valid_next = 1'b0;
	      state_next = WAIT_FOR_IP_HS;
	      case(chosen_in_reg)
		IN1_CHOSEN: begin
		   eci_pkt1_ready_next = eci_pkt_ready_i;
		   eci_pkt2_ready_next = 1'b0;
		   eci_pkt3_ready_next = 1'b0;
		   eci_pkt1_stall_next = eci_pkt_stall_i;
		   eci_pkt2_stall_next = 1'b0;
		   eci_pkt3_stall_next = 1'b0;
		end
		IN2_CHOSEN: begin
		   eci_pkt1_ready_next = 1'b0;
		   eci_pkt2_ready_next = eci_pkt_ready_i;
		   eci_pkt3_ready_next = 1'b0;
		   eci_pkt1_stall_next = 1'b0;
		   eci_pkt2_stall_next = eci_pkt_stall_i;
		   eci_pkt3_stall_next = 1'b0;
		end
		IN3_CHOSEN: begin
		   eci_pkt1_ready_next = 1'b0;
		   eci_pkt2_ready_next = 1'b0;
		   eci_pkt3_ready_next = eci_pkt_ready_i;
		   eci_pkt1_stall_next = 1'b0;
		   eci_pkt2_stall_next = 1'b0;
		   eci_pkt3_stall_next = eci_pkt_stall_i;
		end
		default: begin
		   eci_pkt1_ready_next = eci_pkt1_ready_reg;
		   eci_pkt2_ready_next = eci_pkt2_ready_reg;
		   eci_pkt3_ready_next = eci_pkt3_ready_reg;
		   eci_pkt1_stall_next = eci_pkt1_stall_reg;
		   eci_pkt2_stall_next = eci_pkt2_stall_reg;
		   eci_pkt3_stall_next = eci_pkt3_stall_reg;
		end
	      endcase // case (chosen_in_reg)
	      // Cycle priority 
	      case(priority_reg)
		CHOOSE_1_ST: begin
		   priority_next = CHOOSE_2_ST;
		end
		CHOOSE_2_ST: begin
		   priority_next = CHOOSE_3_ST;
		end
		CHOOSE_3_ST: begin
		   priority_next = CHOOSE_1_ST;
		end
		default: begin
		   priority_next = priority_reg;
		end
	      endcase // case (priority_reg)
	   end
	end

	WAIT_FOR_IP_HS: begin
	   if(eci_pkt1_valid_i & eci_pkt1_ready_o) begin
	      eci_pkt1_ready_next = 1'b0;
	      state_next = WAIT_FOR_IP;
	   end
	   if (eci_pkt1_valid_i & eci_pkt1_stall_o) begin
	      eci_pkt1_stall_next = 1'b0;
	      state_next = WAIT_FOR_IP;
	   end 
	   if (eci_pkt2_valid_i & eci_pkt2_ready_o) begin
	      eci_pkt2_ready_next = 1'b0;
	      state_next = WAIT_FOR_IP;
	   end 
	   if (eci_pkt2_valid_i & eci_pkt2_stall_o) begin
	      eci_pkt2_stall_next = 1'b0;
	      state_next = WAIT_FOR_IP;
	   end
	   if(eci_pkt3_valid_i & eci_pkt3_ready_o) begin
	      eci_pkt3_ready_next = 1'b0;
	      state_next = WAIT_FOR_IP;
	   end
	   if (eci_pkt3_valid_i & eci_pkt3_stall_o) begin
	      eci_pkt3_stall_next = 1'b0;
	      state_next = WAIT_FOR_IP;
	   end 
	end
      endcase // case (state_reg)
   end : CONTROLLER

   always_ff @(posedge clk) begin : REG_ASSIGN
      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset
	 state_reg <= WAIT_FOR_IP;
	 priority_reg <= CHOOSE_1_ST;
	 chosen_in_reg <= IN1_CHOSEN;
	 eci_pkt1_ready_reg	<= '0;
	 eci_pkt1_stall_reg	<= '0;
	 eci_pkt2_ready_reg	<= '0;
	 eci_pkt2_stall_reg	<= '0;
	 eci_pkt3_ready_reg	<= '0;
	 eci_pkt3_stall_reg	<= '0;
	 //eci_hdr_reg		<= '0;
	 eci_pkt_size_reg	<= '0;
	 eci_pkt_vc_reg	<= '0;
	 eci_pkt_valid_reg <= '0;
      end else begin
	 state_reg <= state_next;
	 priority_reg <= priority_next;
	 chosen_in_reg <= chosen_in_next;
	 eci_pkt1_ready_reg	<= eci_pkt1_ready_next;
	 eci_pkt1_stall_reg	<= eci_pkt1_stall_next;
	 eci_pkt2_ready_reg	<= eci_pkt2_ready_next;
	 eci_pkt2_stall_reg	<= eci_pkt2_stall_next;
	 eci_pkt3_ready_reg	<= eci_pkt3_ready_next;
	 eci_pkt3_stall_reg	<= eci_pkt3_stall_next;
	 eci_pkt_size_reg	<= eci_pkt_size_next;
	 eci_pkt_vc_reg		<= eci_pkt_vc_next;
	 eci_pkt_valid_reg <= eci_pkt_valid_next;
      end
      // No reset oon data path 
      eci_hdr_reg <= eci_hdr_next;
   end : REG_ASSIGN
endmodule // arb_3_ecih
`endif
