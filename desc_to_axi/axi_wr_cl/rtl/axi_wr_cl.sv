/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-02-02
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */


`ifndef AXI_WR_CL_SV
`define AXI_WR_CL_SV

/*
 * This module converts a write req desc into AXI requests. 
 * Always set to write 1 CL, will write 2 beats. The bytestrobes will be honored. 
 * 
 * II=2, Not II=1 because the directory controller is not II=1 and will not issue 
 * events at a faster rate. 
 * 
 */

module axi_wr_cl #
  (
   parameter DESC_ID_WIDTH = 6,
   parameter DESC_ADDR_WIDTH = 40,
   // size of cache line. dont change. 
   parameter DESC_DATA_WIDTH = 1024,
   parameter DESC_STRB_WIDTH = DESC_DATA_WIDTH/8,
   
   parameter AXI_ID_WIDTH   = DESC_ID_WIDTH,
   parameter AXI_ADDR_WIDTH = DESC_ADDR_WIDTH,
   // Do not change axi data wdith. 
   parameter AXI_DATA_WIDTH = 512,
   parameter AXI_STRB_WIDTH = AXI_DATA_WIDTH/8
   )
   (
    input logic 		      clk,
    input logic 		      reset,
    
    input logic [DESC_ID_WIDTH-1:0]   wr_req_id_i,
    input logic [DESC_ADDR_WIDTH-1:0] wr_req_addr_i,
    input logic [DESC_DATA_WIDTH-1:0] wr_req_data_i,
    input logic [DESC_STRB_WIDTH-1:0] wr_req_strb_i,
    input logic 		      wr_req_valid_i,
    output logic 		      wr_req_ready_o,

    output logic [DESC_ID_WIDTH-1:0]  wr_rsp_id_o,
    output logic [1:0] 		      wr_rsp_bresp_o,
    output logic 		      wr_rsp_valid_o,
    input logic 		      wr_rsp_ready_i,


    // AXI primary interface
    output logic [AXI_ID_WIDTH-1:0]   p_axi_awid,
    output logic [AXI_ADDR_WIDTH-1:0] p_axi_awaddr,
    output logic [7:0] 		      p_axi_awlen,
    output logic [2:0] 		      p_axi_awsize,
    output logic [1:0] 		      p_axi_awburst,
    output logic 		      p_axi_awlock,
    output logic [3:0] 		      p_axi_awcache,
    output logic [2:0] 		      p_axi_awprot,
    output logic 		      p_axi_awvalid,
    input logic 		      p_axi_awready,
    output logic [AXI_DATA_WIDTH-1:0] p_axi_wdata,
    output logic [AXI_STRB_WIDTH-1:0] p_axi_wstrb,
    output logic 		      p_axi_wlast,
    output logic 		      p_axi_wvalid,
    input logic 		      p_axi_wready,
    input logic [AXI_ID_WIDTH-1:0]    p_axi_bid,
    input logic [1:0] 		      p_axi_bresp,
    input logic 		      p_axi_bvalid,
    output logic 		      p_axi_bready
    );

   // Number of bytes per beat encoded, same as width of databus in bytes.
   localparam AW_SIZE = $clog2(AXI_DATA_WIDTH/8);
   localparam ALIGN_ADDR_LSB = $clog2(AXI_DATA_WIDTH/8);
   // awlen + 1 indicates number of beats.
   // always 2 beats irrespective of write strobes.  
   localparam AW_LEN = 1;
   // INCR burst type always
   localparam AXI_INCR = 2'b01;   

   logic 			      wr_req_ready_reg	= '0, wr_req_ready_next;
   logic [DESC_ID_WIDTH-1:0] 	      wr_rsp_id_reg	= '0, wr_rsp_id_next;
   logic [1:0] 			      wr_rsp_bresp_reg	= '0, wr_rsp_bresp_next;
   logic 			      wr_rsp_valid_reg	= '0, wr_rsp_valid_next;
   logic [AXI_ID_WIDTH-1:0] 	      p_axi_awid_reg	= '0, p_axi_awid_next;
   logic [AXI_ADDR_WIDTH-1:0] 	      p_axi_awaddr_reg	= '0, p_axi_awaddr_next;
   logic [7:0] 			      p_axi_awlen_reg	= '0, p_axi_awlen_next;
   logic [2:0] 			      p_axi_awsize_reg	= '0, p_axi_awsize_next;
   logic [1:0] 			      p_axi_awburst_reg = '0, p_axi_awburst_next;
   logic 			      p_axi_awlock_reg	= '0, p_axi_awlock_next;
   logic [3:0] 			      p_axi_awcache_reg = '0, p_axi_awcache_next;
   logic [2:0] 			      p_axi_awprot_reg	= '0, p_axi_awprot_next;
   logic 			      p_axi_awvalid_reg = '0, p_axi_awvalid_next;
   logic [AXI_DATA_WIDTH-1:0] 	      p_axi_wdata_reg	= '0, p_axi_wdata_next;
   logic [AXI_STRB_WIDTH-1:0] 	      p_axi_wstrb_reg	= '0, p_axi_wstrb_next;
   logic 			      p_axi_wlast_reg	= '0, p_axi_wlast_next;
   logic 			      p_axi_wvalid_reg  = '0, p_axi_wvalid_next;
   logic 			      p_axi_bready_reg  = '0, p_axi_bready_next;
   logic [AXI_DATA_WIDTH-1:0] 	      wdata_exp_reg = '0, wdata_exp_next;
   logic 			      wdata_exp_valid_reg = '0, wdata_exp_valid_next;
   logic [AXI_STRB_WIDTH-1:0] 	      wstrb_exp_reg = '0, wstrb_exp_next;
      
   logic 			      wr_req_hs, aw_hs, w_hs, b_hs, wr_rsp_hs;

   initial begin
      if(DESC_DATA_WIDTH != 1024) begin
	 $error("Error: DESC_DATA_WIDTH has to be 1024 (instance %m)");
	 $finish;
      end
      if(AXI_DATA_WIDTH != 512) begin
	 $error("Error: AXI_DATA_WIDTH has to be 512 (instance %m)");
	 $finish;
      end
   end

   assign wr_req_ready_o	= wr_req_ready_reg;
   assign wr_rsp_id_o		= wr_rsp_id_reg;
   assign wr_rsp_bresp_o	= wr_rsp_bresp_reg;
   assign wr_rsp_valid_o	= wr_rsp_valid_reg;
   assign p_axi_awid		= p_axi_awid_reg;
   assign p_axi_awaddr		= p_axi_awaddr_reg;
   assign p_axi_awlen		= p_axi_awlen_reg;
   assign p_axi_awsize		= p_axi_awsize_reg;
   assign p_axi_awburst		= p_axi_awburst_reg;
   assign p_axi_awlock		= p_axi_awlock_reg;
   assign p_axi_awcache		= p_axi_awcache_reg;
   assign p_axi_awprot		= p_axi_awprot_reg;
   assign p_axi_awvalid		= p_axi_awvalid_reg;
   assign p_axi_wdata		= p_axi_wdata_reg;
   assign p_axi_wstrb		= p_axi_wstrb_reg;
   assign p_axi_wlast		= p_axi_wlast_reg;
   assign p_axi_wvalid		= p_axi_wvalid_reg;
   assign p_axi_bready          = p_axi_bready_reg;

   assign wr_req_hs = wr_req_valid_i & wr_req_ready_o;
   assign aw_hs = p_axi_awvalid & p_axi_awready;
   assign w_hs = p_axi_wvalid & p_axi_wready;
   assign b_hs = p_axi_bvalid & p_axi_bready;
   assign wr_rsp_hs = wr_rsp_valid_o & wr_rsp_ready_i;

   // when downstream AXI aw and wchannels are not valid, ready to accept a new write descriptor.
   // when handshake happens in write desc interface, the id, address are bottom half of the data
   // are latched to output AXI regsiters. The top half of data and strobe are latched onto
   // expansion registers and aw channel is valid.
   // when handshake happens in awvalid channel, the wchannel is now make valid.
   // when handshake happens in wchannel, if there is data in expansion register, it is sent
   // to the output. wvalid is still high.
   // after second handshake happens, the write transaction is complete and a new
   // transaction can be started. 
   always_comb begin : AW_W_CH_CONTROLLER
      wr_req_ready_next		= wr_req_ready_reg;
      p_axi_awid_next		= p_axi_awid_reg;
      p_axi_awaddr_next		= p_axi_awaddr_reg;
      p_axi_awlen_next		= p_axi_awlen_reg;
      p_axi_awsize_next		= p_axi_awsize_reg;
      p_axi_awburst_next	= p_axi_awburst_reg;
      p_axi_awlock_next		= p_axi_awlock_reg;
      p_axi_awcache_next	= p_axi_awcache_reg;
      p_axi_awprot_next		= p_axi_awprot_reg;
      p_axi_awvalid_next	= p_axi_awvalid_reg;
      p_axi_wdata_next		= p_axi_wdata_reg;
      p_axi_wstrb_next		= p_axi_wstrb_reg;
      p_axi_wlast_next		= p_axi_wlast_reg;
      p_axi_wvalid_next = p_axi_wvalid_reg;
      wdata_exp_next = wdata_exp_reg;
      wdata_exp_valid_next = wdata_exp_valid_reg;
      wstrb_exp_next = wstrb_exp_reg;
      

      if(~p_axi_awvalid & ~wr_req_hs & ~p_axi_wvalid) begin
	 wr_req_ready_next = 1'b1;
      end else if ( wr_req_hs ) begin
	 wr_req_ready_next = 1'b0;
	 p_axi_awid_next       = wr_req_id_i;
	 // Aligning address to number of bytes per beat. 
	 p_axi_awaddr_next     = '0;
	 p_axi_awaddr_next[AXI_ADDR_WIDTH-1:ALIGN_ADDR_LSB] = wr_req_addr_i[AXI_ADDR_WIDTH-1:ALIGN_ADDR_LSB];
	 p_axi_awlen_next      = AW_LEN;
	 p_axi_awsize_next     = AW_SIZE;
	 p_axi_awburst_next    = AXI_INCR;
	 p_axi_awlock_next     = '0;
	 p_axi_awcache_next    = '0;
	 p_axi_awprot_next     = '0;
	 p_axi_awvalid_next    =  1'b1;
	 p_axi_wdata_next      = wr_req_data_i[AXI_DATA_WIDTH-1:0];
	 p_axi_wstrb_next      = wr_req_strb_i[AXI_STRB_WIDTH-1:0];
	 wdata_exp_next        = wr_req_data_i[DESC_DATA_WIDTH-1:AXI_DATA_WIDTH];
	 wstrb_exp_next        = wr_req_strb_i[DESC_STRB_WIDTH-1:AXI_STRB_WIDTH];
	 wdata_exp_valid_next  = 1'b1;
	 p_axi_wlast_next      = 1'b0;
	 p_axi_wvalid_next     = 1'b0;
      end else if ( aw_hs ) begin
	 p_axi_awvalid_next = 1'b0;
	 p_axi_wvalid_next = 1'b1;
      end else if (w_hs) begin
	 if(wdata_exp_valid_reg) begin
	    p_axi_wdata_next = wdata_exp_reg;
	    p_axi_wstrb_next = wstrb_exp_reg;
	    p_axi_wlast_next = 1'b1;
	    p_axi_wvalid_next = 1'b1;
	    wdata_exp_valid_next = 1'b0;
	 end else begin
	    p_axi_wlast_next = 1'b0;
	    p_axi_wvalid_next = 1'b0;
	    wr_req_ready_next = 1'b1;
	 end
      end
   end : AW_W_CH_CONTROLLER

   always_comb begin : BRESP_CH_CONTROLLER
      wr_rsp_id_next = wr_rsp_id_reg;
      wr_rsp_bresp_next	= wr_rsp_bresp_reg;
      wr_rsp_valid_next	= wr_rsp_valid_reg;
      p_axi_bready_next = p_axi_bready_reg;

      if(~wr_rsp_valid_o & ~b_hs) begin
	 p_axi_bready_next = 1'b1;
      end else if(b_hs) begin
	 wr_rsp_id_next = p_axi_bid;
	 wr_rsp_bresp_next = p_axi_bresp;
	 wr_rsp_valid_next = 1'b1;
	 p_axi_bready_next = 1'b0;
      end else if (wr_rsp_hs) begin
	 wr_rsp_valid_next = 1'b0;
	 p_axi_bready_next = 1'b1;
      end
   end : BRESP_CH_CONTROLLER

   always_ff @(posedge clk) begin : REG_ASSIGN
      wr_req_ready_reg	<= wr_req_ready_next;
      wr_rsp_id_reg	<= wr_rsp_id_next;
      wr_rsp_bresp_reg	<= wr_rsp_bresp_next;
      wr_rsp_valid_reg	<= wr_rsp_valid_next;
      p_axi_awid_reg	<= p_axi_awid_next;
      p_axi_awaddr_reg	<= p_axi_awaddr_next;
      p_axi_awlen_reg	<= p_axi_awlen_next;
      p_axi_awsize_reg	<= p_axi_awsize_next;
      p_axi_awburst_reg <= p_axi_awburst_next;
      p_axi_awlock_reg	<= p_axi_awlock_next;
      p_axi_awcache_reg <= p_axi_awcache_next;
      p_axi_awprot_reg	<= p_axi_awprot_next;
      p_axi_awvalid_reg <= p_axi_awvalid_next;
      p_axi_wdata_reg	<= p_axi_wdata_next;
      p_axi_wstrb_reg	<= p_axi_wstrb_next;
      p_axi_wlast_reg	<= p_axi_wlast_next;
      p_axi_wvalid_reg	<= p_axi_wvalid_next;
      p_axi_bready_reg	<= p_axi_bready_next;
      wdata_exp_reg <= wdata_exp_next;
      wdata_exp_valid_reg <= wdata_exp_valid_next;
      wstrb_exp_reg <= wstrb_exp_next;

      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset 
	 wr_req_ready_reg	<= '0;
	 wr_rsp_id_reg	<= '0;
	 wr_rsp_bresp_reg	<= '0;
	 wr_rsp_valid_reg	<= '0;
	 p_axi_awid_reg	<= '0;
	 p_axi_awaddr_reg	<= '0;
	 p_axi_awlen_reg	<= '0;
	 p_axi_awsize_reg	<= '0;
	 p_axi_awburst_reg	<= '0;
	 p_axi_awlock_reg	<= '0;
	 p_axi_awcache_reg	<= '0;
	 p_axi_awprot_reg	<= '0;
	 p_axi_awvalid_reg	<= '0;
	 //p_axi_wdata_reg	<= '0;
	 p_axi_wstrb_reg	<= '0;
	 p_axi_wlast_reg	<= '0;
	 p_axi_wvalid_reg	<= '0;
	 p_axi_bready_reg	<= '0;
	 //wdata_exp_reg <= '0;
	 wdata_exp_valid_reg <= '0;
	 wstrb_exp_reg <= '0;
      end
   end : REG_ASSIGN
   
endmodule // axi_wr_cl

`endif
